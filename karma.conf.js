const envIsDev = require('yargs').argv.build === 'dev';
const preprocessors = envIsDev ? ['webpack', 'sourcemap'] : ['webpack'];
const webpackConfig = envIsDev ? require('./webpack.config.dev.js') : require('./webpack.config.build.js');

webpackConfig.devtool = 'inline-source-map';
delete webpackConfig.entry;
delete webpackConfig.output;
webpackConfig.module.loaders[0].query.plugins = ['lodash', 'istanbul'];
delete webpackConfig.module.loaders[0].query.extra;

webpackConfig.module.loaders[1].loader = 'style!css?modules&importLoaders=1&localIdentName=[name]-[local]!postcss!sass';

module.exports = function(config) {
  config.set({
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    files: [
      'node_modules/babel-polyfill/dist/polyfill.js',
      'tests/index.js'
    ],
    preprocessors: {
      'tests/index.js': preprocessors
    },
    reporters: ['progress', 'kjhtml', 'coverage', 'coveralls'],
    singleRun: (envIsDev ? false : true),
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true
    },
    coverageReporter: {
      reporters: [
        { type: 'text-summary' },
        { type : 'lcov' }
      ]
    },
    browserNoActivityTimeout: 100000
  });
};
