/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const childProcess = require('child_process');

const lastCommitHash = childProcess.execSync('git rev-parse HEAD').toString().trim();

const definePlugin = new webpack.DefinePlugin({
	'window.LAST_COMMIT_HASH': `"${lastCommitHash}"`
});

console.log('\x1b[32m%s\x1b[0m', '----- Building Publishing-UI in PROD -----');

module.exports = {
  entry: [
    'babel-polyfill',
    path.join(__dirname, '/app/scripts/main')
  ],
  output: {
    path: path.join(__dirname, 'build', 'publishing'),
    filename: 'scripts/bundle.js'
  },
  resolve: {
    root: [
      path.join(__dirname, '/app/scripts')
    ],
    fallback: [
      path.join(__dirname, '/node_modules')
    ],
    alias: {
      'bootstrap_sass': 'bootstrap-sass',
      'rad': '@spredfast/rad/dist',
      'reactLibStyles': '@spredfast/react-lib/styles',
      'styles': path.join(__dirname, '/app/styles'),
      'socialMediaPreview': path.join(__dirname, 'node_modules/@spredfast/social-media-preview'),
      'scripts': path.join(__dirname, '/app/scripts'),
      'tests': path.join(__dirname, 'tests'),
      'images': path.join(__dirname, '/app/styles/images'),
      'fixtures': path.join(__dirname, '/app/fixtures')
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /\/node_modules\//,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-0'],
          plugins: [
            'transform-runtime',
            'transform-decorators-legacy',
            'transform-react-constant-elements',
            'lodash'
          ]
        },
        cacheDirectory: false
      },
      {
        test: /\.scss$/,
        include: [
          path.join(__dirname, 'app/styles/modules')
        ],
        loader: 'style!css?modules&importLoaders=1!postcss!sass'
      },
      {
        test: /\.scss$/,
        exclude: [
          path.join(__dirname, 'app/styles/modules')
        ],
        loader: 'style!css!postcss!sass'
      },
      {
        test: /\.css$/,
        loader: 'style!css!postcss'
      },
      {
        test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/,
        include: [
          path.join(__dirname, 'node_modules/react-widgets/lib')
        ],
        loader: 'url-loader?mimetype=application/font-woff'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9].[0-9].[0-9])?$/,
        include: [
          path.join(__dirname, 'node_modules/react-widgets/lib')
        ],
        loader: 'file-loader?name=[name].[ext]'
      },
      {
        test: /\.(woff|woff2|ttf|eot|svg|png|gif|jpg)$/,
        loader: 'file-loader?name=assets/[name]-[hash].[ext]'
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        drop_console: true
      }
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new webpack.optimize.DedupePlugin(),
    definePlugin
  ],
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },
	devtool: 'source-map',
  postcss: function() {
    return [autoprefixer({
      browsers: [
        'last 3 versions',
        '> 1%',
        'ie >= 10'
      ],
      cascade: false
    })];
  }
};
