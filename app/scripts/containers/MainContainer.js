/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { messageTitleChanged, saveMessage } from 'actions/MessageActions';
import { updateDragStateIfNecessary, cancelCloseButtonClicked } from 'actions/UIStateActions';
import { initializeAppData } from 'actions/EnvironmentActions';
import { messageCanPublishSelector, topLevelValidation, messageCanSaveDraftSelector, canUseMultichannelPubs } from 'selectors/ValidationSelector';
import { messageHasId, getCompanyId, getCampaignId, getCancelButtonVerbage } from 'selectors/RouterSelector';
import { messageTitleSelector,
  scheduledDateTimeSelector,
  previewVisible,
  hasEditAccess,
  getCurrentPlan } from 'reducers/index';

import Main from 'components/Main';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

function mapStateToProps(state: rootState): Object {
  const { message } = state;
  return {
    eventId: message.eventId,
    messageTitle: messageTitleSelector(state),
    scheduledDatetime: scheduledDateTimeSelector(state),
    canPublish: messageCanPublishSelector(state),
    canSaveDraft: messageCanSaveDraftSelector(state),
    validation: topLevelValidation(state),
    messageHasId: messageHasId(state),
    companyId: getCompanyId(state),
    campaignId: getCampaignId(state),
    cancelVerbage: getCancelButtonVerbage(state),
    previewVisible: previewVisible(state),
    hasEditAccess: hasEditAccess(state),
    currentPlan: getCurrentPlan(state),
    canUseMultichannelPubs: canUseMultichannelPubs(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onTitleChange: bindActionCreators(messageTitleChanged, dispatch),
    onScheduleClicked: bindActionCreators(saveMessage, dispatch),
    dragStateChanged: bindActionCreators(updateDragStateIfNecessary, dispatch),
    onCancelClicked: bindActionCreators(cancelCloseButtonClicked, dispatch),
    initializeAppData: bindActionCreators(initializeAppData, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
