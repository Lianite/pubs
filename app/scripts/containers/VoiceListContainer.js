/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import VoiceList from 'components/VoiceList';
import { fetchVoicesLoading,
  getCurrentAccountChannelFilter,
  voiceSelectorOpen,
  getCurrentAccountSelectionFilter,
  searchOnVoices,
  getDarkPostStatus,
  featureFlags} from 'reducers/index';
import {
  getFilteredCredentials,
  getTotalFilteredCredentialCount,
  getTotalCredentialCount,
  checkedVoiceTree,
  totalCheckedCredentialsInCurrentView
} from 'selectors/AccountsSelectors';
import { resendValidAssigneesRequest } from 'actions/AssigneeActions';
import { voiceChecked,
  quickSelectAllOrNone,
  updateFilterChannels,
  updateFilterSelection,
  searchOnVoicesToggle
} from 'actions/AccountsActions';
import { targetingSet as facebookTargetingSet } from 'selectors/FacebookSelector';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

function mapStateToProps(state: rootState): Object {
  return {
    voices: getFilteredCredentials(state),
    checkedVoices: checkedVoiceTree(state),
    filteredAccountCount: getTotalFilteredCredentialCount(state),
    totalAccountCount: getTotalCredentialCount(state),
    checkedCount: totalCheckedCredentialsInCurrentView(state),
    isOpen: voiceSelectorOpen(state),
    voicesLoading: fetchVoicesLoading(state),
    currentAccountChannelFilter: getCurrentAccountChannelFilter(state),
    currentAccountSelectionFilter: getCurrentAccountSelectionFilter(state),
    facebookTargetingSet: facebookTargetingSet(state),
    searchOnVoices: searchOnVoices(state),
    darkPostStatus: getDarkPostStatus(state),
    featureFlags: featureFlags(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    voiceChecked: bindActionCreators(voiceChecked, dispatch),
    updateFilterChannels: bindActionCreators(updateFilterChannels, dispatch),
    updateFilterSelection: bindActionCreators(updateFilterSelection, dispatch),
    quickSelectAllVoices: bindActionCreators(quickSelectAllOrNone, dispatch),
    resendValidAssigneesRequest: bindActionCreators(resendValidAssigneesRequest, dispatch),
    searchOnVoicesToggle: bindActionCreators(searchOnVoicesToggle, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VoiceList);
