/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import OptionsSideBar from 'components/OptionsSideBar';
import { addLinkPreview } from 'actions/LinkPreviewActions';
import { toggleSidebar, closeSidebarIfOpen } from 'actions/UIStateActions';
import { updateSingleUrlProperty } from 'actions/LinksActions';
import { updateLinkTagVariableForAllNetworks } from 'actions/LinkTagsActions';
import { hasError as facebookHasError } from 'selectors/FacebookSelector';
import { sidebarOpen, activePage, urlProperties, selectedNetworks } from 'reducers/index';
import { selectedLinkPreviewUrl } from 'selectors/LinkPreviewSelector';
import {
  linkTagsByLink,
  linkTagsEnable } from 'selectors/LinkTagsSelector';
import { linkPreviewUrls } from 'selectors/LinksSelector';
import { canAddAttachmentByType } from 'selectors/AttachedMediaSelector';

function mapStateToProps(state: rootState) {
  return {
    sidebarOpen: sidebarOpen(state),
    activePage: activePage(state),
    urls: linkPreviewUrls(state),
    urlProperties: urlProperties(state),
    activeNetworks: selectedNetworks(state),
    selectedLinkPreviewUrl: selectedLinkPreviewUrl(state),
    linkTagsByLink: linkTagsByLink(state),
    linkTagsEnable: linkTagsEnable(state),
    canAddAttachmentByType: canAddAttachmentByType(state),
    facebookHasError: facebookHasError(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onSidebarTabClicked: bindActionCreators(toggleSidebar, dispatch),
    onSidebarClosed: bindActionCreators(closeSidebarIfOpen, dispatch),
    onChangeLinkProperty: bindActionCreators(updateSingleUrlProperty, dispatch),
    addLinkPreview: bindActionCreators(addLinkPreview, dispatch),
    updateLinkTagVariable: bindActionCreators(updateLinkTagVariableForAllNetworks, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OptionsSideBar);
