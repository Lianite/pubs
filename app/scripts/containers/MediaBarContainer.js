/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import MediaBar from 'components/MediaBar';

import { videoThumbnailFrameChanged } from 'actions/MediaActions';

import type { Dispatch } from 'redux';

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onVideoThumbnailFrameChanged: bindActionCreators(videoThumbnailFrameChanged, dispatch)
  };
}

export default connect(
  undefined, //no mapStateToProps
  mapDispatchToProps
)(MediaBar);
