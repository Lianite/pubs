/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  messageEditorState,
  totalNetworkCount,
  disableApplyToAll,
  getLinkRegexString,
  getMentionsQueryList,
  getMentionsQueryUiState,
  getMentionsQueryStatus,
  hasVideoBeenRequested,
  getCustomVideoTitle,
  getCurrentMentionOffset,
  getNetworkMentionsList
} from 'reducers/index';
import { makeGetValidationForNetwork } from 'selectors/ValidationSelector';
import { sendUpdatedMessageBody, applyToAllChannelsWithNotification } from 'actions/MessageActions';
import { updateMentions, closeMentionsQuery } from 'actions/MentionsActions';
import { updateUrlList } from 'actions/LinksActions';
import { setCustomVideoTitle } from 'actions/MediaActions';

import Message from 'components/Message';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

const makeMapStateToProps = (state, props) => {
  const getValidationForNetwork = makeGetValidationForNetwork(props.network);
  const mapStateToProps = (state: rootState, props) => {
    return {
      messageEditorState: messageEditorState(state, props.network),
      disableApplyToAll: disableApplyToAll(state, props.network),
      numberOfNetworks: totalNetworkCount(state),
      validation: getValidationForNetwork(state, props.network),
      linkRegexString: getLinkRegexString(state),
      mentionsQueryList: getMentionsQueryList(state),
      getMentionsQueryUiState: getMentionsQueryUiState(state, props.network),
      loadMentionsQueryStatus: getMentionsQueryStatus(state),
      hasVideoBeenRequested: hasVideoBeenRequested(state),
      customVideoTitle: getCustomVideoTitle(state),
      currentMentionOffset: getCurrentMentionOffset(state),
      networkMentionList: getNetworkMentionsList(state, props.network)
    };
  };
  return mapStateToProps;
};

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onChange: bindActionCreators(sendUpdatedMessageBody, dispatch),
    onBlur: bindActionCreators(updateUrlList, dispatch),
    applyToAllChannels: bindActionCreators(applyToAllChannelsWithNotification, dispatch),
    updateMentions: bindActionCreators(updateMentions, dispatch),
    closeMentionsQueryDialog: bindActionCreators(closeMentionsQuery, dispatch),
    setCustomVideoTitle: bindActionCreators(setCustomVideoTitle, dispatch)
  };
}

export default connect(
  makeMapStateToProps,
  mapDispatchToProps
)(Message);
