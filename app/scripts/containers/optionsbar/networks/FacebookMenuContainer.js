/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FacebookMenu from 'components/optionsbar/networks/FacebookMenu';
import {
  canDarkPost,
  canTarget,
  targetingSet,
  invalidTargetingAccounts,
  invalidDarkPostAccounts,
  selectedLanguages,
  languageOptions,
  locationOptions,
  includedLocationsByCountry,
  targetingIsEnabled
} from 'selectors/FacebookSelector';
import {
  fetchFacebookTargetingLanguages,
  addFacebookTargetingLanguage,
  removeFacebookTargetingLanguage,
  fetchFacebookTargetingLocations,
  addFacebookTargetingIncludedLocation,
  removeFacebookTargetingIncludedLocation,
  removeFacebookTargetingIncludedCountry,
  clearTargeting,
  setDarkPostVisibility,
  selectGenderTargeting
} from 'actions/FacebookActions';
import {
  getDarkPostStatus,
  facebookTargetingSelectedGender
} from 'reducers/index';

import type { rootState } from 'reducers/index';
import type { Dispatch } from 'redux';

const mapStateToProps = (state: rootState) => ({
  targetingSelectedLanguages: selectedLanguages(state),
  targetingLanguageOptions: languageOptions(state),
  invalidTargetingAccounts: invalidTargetingAccounts(state),
  targetingSet: targetingSet(state),
  targetingLocationOptions: locationOptions(state),
  targetingSelectedLocations: includedLocationsByCountry(state),
  darkPostStatus: getDarkPostStatus(state),
  canTarget: canTarget(state),
  canDarkPost: canDarkPost(state),
  invalidDarkPostAccounts: invalidDarkPostAccounts(state),
  targetingIsEnabled: targetingIsEnabled(state),
  targetingSelectedGender: facebookTargetingSelectedGender(state)
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onLanguageTargetingInputChange: bindActionCreators(fetchFacebookTargetingLanguages, dispatch),
  addTargetingLanguage: bindActionCreators(addFacebookTargetingLanguage, dispatch),
  removeTargetingLanguage: bindActionCreators(removeFacebookTargetingLanguage, dispatch),
  clearTargeting: bindActionCreators(clearTargeting, dispatch),
  onLocationTargetingInputChange: bindActionCreators(fetchFacebookTargetingLocations, dispatch),
  addTargetingIncludedLocation: bindActionCreators(addFacebookTargetingIncludedLocation, dispatch),
  removeTargetingIncludedLocation: bindActionCreators(removeFacebookTargetingIncludedLocation, dispatch),
  removeTargetingIncludedCountry: bindActionCreators(removeFacebookTargetingIncludedCountry, dispatch),
  setDarkPostVisibility: bindActionCreators(setDarkPostVisibility, dispatch),
  selectGenderTargeting: bindActionCreators(selectGenderTargeting, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FacebookMenu);
