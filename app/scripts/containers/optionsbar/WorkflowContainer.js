/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import WorkflowContextMenu from 'components/optionsbar/WorkflowContextMenu';
import { updateCurrentAssignee, resendValidAssigneesRequest } from 'actions/AssigneeActions';
import { getCurrentAssignee } from 'reducers/index';
import { getAssignees } from 'selectors/AssigneeSelectors';
import { getUnavailableVoices } from 'selectors/AccountsSelectors';

function mapStateToProps(state: rootState) {
  return {
    currentAssignee: getCurrentAssignee(state),
    unavailableVoices: getUnavailableVoices(state),
    assignees: getAssignees(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onSelectAssignee: bindActionCreators(updateCurrentAssignee, dispatch),
    resendValidAssigneesRequest: bindActionCreators(resendValidAssigneesRequest, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkflowContextMenu);
