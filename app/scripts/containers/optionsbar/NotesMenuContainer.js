/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NotesMenu from 'components/optionsbar/NotesMenu';
import { addNote,
  currentNoteTextChanged,
  createNoteForSavedMessage } from 'actions/NotesActions';
import { getUserTimezone,
  getUserFirstName,
  getUserLastName,
  getUserAvatar,
  messageIsSaved,
  currentNoteText,
  savingNote } from 'reducers/index';
import { activitiesForDisplay } from 'selectors/ActivitiesSelectors';
import { notesForDisplay } from 'selectors/NotesSelector';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

const mapStateToProps = (state: rootState) => {
  return {
    notes: notesForDisplay(state),
    activities: activitiesForDisplay(state),
    userTimezone: getUserTimezone(state),
    userFirstName: getUserFirstName(state),
    userLastName: getUserLastName(state),
    userAvatar: getUserAvatar(state),
    messageIsSaved: messageIsSaved(state),
    initialNoteText: currentNoteText(state),
    disableInput: savingNote(state)
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    createPendingNote: bindActionCreators(addNote, dispatch),
    createNoteForSavedMessage:bindActionCreators(createNoteForSavedMessage, dispatch),
    onCurrentNoteTextChange: bindActionCreators(currentNoteTextChanged, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotesMenu);
