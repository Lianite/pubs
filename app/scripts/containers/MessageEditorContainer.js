/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateFilterText,
  voiceChecked,
  removeCredentialsForSelectedNetwork,
  updateExclusiveFilterChannel,
  updateFilterSelection } from 'actions/AccountsActions';
import { toggleVoiceSelectorIfNecessary } from 'actions/UIStateActions';
import {
  sendUploadMediaRequest,
  revertToDefault,
  uploadImageCanceled,
  uploadVideoCanceled,
  removeUploadedVideo,
  uploadVideoThumb,
  removeOrCancelCustomThumb } from 'actions/MediaActions';
import MessageEditor from 'components/MessageEditor';
import {
  canShowLinkPreview
} from 'selectors/LinkPreviewSelector';
import {
  linkPreview
} from 'selectors/LinkPreviewSelectorWithValidation';
import {
  attachedImage,
  attachedVideo,
  videoUploadNetworks,
  mediaStatus } from 'selectors/AttachedMediaSelector';
import {
  removeLinkPreview,
  editLinkPreview,
  cycleLinkPreviewImageCarouselForward,
  cycleLinkPreviewImageCarouselBackward,
  uploadCustomThumb,
  cancelLinkPreviewCustomUpload,
  clearLinkPreviewCustomUploadError,
  cancelLinkPreviewScrape,
  syncLinkPreview } from 'actions/LinkPreviewActions';

import { resendValidAssigneesRequest } from 'actions/AssigneeActions';

import {
  accountFilterText,
  checkedCredentialsSelector,
  messageEditorStateMap,
  currentlyDraggingMedia,
  getCurrentAccountChannelFilter,
  voiceSelectorOpen } from 'reducers/index';

import type { Dispatch } from 'redux';

function mapStateToProps(state) {
  const _canShowLinkPreview = canShowLinkPreview(state);
  return {
    messageEditorStateMap: messageEditorStateMap(state),
    selectedAccountList: checkedCredentialsSelector(state),
    filterText: accountFilterText(state),
    currentlyDraggingMedia: currentlyDraggingMedia(state),
    image: attachedImage(state),
    video: attachedVideo(state),
    videoUploadNetworks: videoUploadNetworks(state),
    mediaStatus: mediaStatus(state),
    linkPreview: linkPreview(state),
    canShowLinkPreview: _canShowLinkPreview,
    canShowMediaBar: !_canShowLinkPreview,
    currentAccountChannelFilter: getCurrentAccountChannelFilter(state),
    voiceSelectorOpen: voiceSelectorOpen(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    voiceDropdownToggle: bindActionCreators(toggleVoiceSelectorIfNecessary, dispatch),
    removeUploadedVideo: bindActionCreators(removeUploadedVideo, dispatch),
    removeAttachment: bindActionCreators(revertToDefault, dispatch),
    removeLinkPreview: bindActionCreators(removeLinkPreview, dispatch),
    editLinkPreview: bindActionCreators(editLinkPreview, dispatch),
    removeSelectedAccount: bindActionCreators(voiceChecked, dispatch),
    removeSelectedAccountsPerNetwork: bindActionCreators(removeCredentialsForSelectedNetwork, dispatch),
    resendValidAssigneesRequest: bindActionCreators(resendValidAssigneesRequest, dispatch),
    updateFilterText: bindActionCreators(updateFilterText, dispatch),
    updateExclusiveFilterChannel: bindActionCreators(updateExclusiveFilterChannel, dispatch),
    updateFilterSelection: bindActionCreators(updateFilterSelection, dispatch),
    onDrop: bindActionCreators(sendUploadMediaRequest, dispatch),
    onCustomLinkPreviewDropped: bindActionCreators(uploadCustomThumb, dispatch),
    cancelImageUpload: bindActionCreators(uploadImageCanceled, dispatch),
    cancelVideoUpload: bindActionCreators(uploadVideoCanceled, dispatch),
    cycleThumbnailForward: bindActionCreators(cycleLinkPreviewImageCarouselForward, dispatch),
    cycleThumbnailBackward: bindActionCreators(cycleLinkPreviewImageCarouselBackward, dispatch),
    onCancelUpload: bindActionCreators(cancelLinkPreviewCustomUpload, dispatch),
    onAcknowledgeError: bindActionCreators(clearLinkPreviewCustomUploadError, dispatch),
    onScrapeCancel: bindActionCreators(cancelLinkPreviewScrape, dispatch),
    onRefreshClicked: bindActionCreators(syncLinkPreview, dispatch),
    onDropCustomVideoThumb: bindActionCreators(uploadVideoThumb, dispatch),
    removeOrCancelCustomThumb: bindActionCreators(removeOrCancelCustomThumb, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageEditor);
