/* @flow */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import postPreview from 'selectors/PostPreviewSelector';
import { updatePreviewVisibility } from 'actions/UIStateActions';
import { previewVisible } from 'reducers/index';

import MessageBoxHeader from 'components/MessageBoxHeader';

import type { rootState } from 'reducers/index';
import type { Dispatch } from 'redux';

const mapStateToProps = (state: rootState, props) => {
  return {
    postPreview: postPreview(state, props),
    previewVisible: previewVisible(state)
  };
};

const mapDispatchToProps = (dispatch: Dispatch, props) => {
  return {
    onPreviewVisibilityChange: bindActionCreators(updatePreviewVisibility, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageBoxHeader);
