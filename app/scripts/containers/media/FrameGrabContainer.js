/* @flow */
import { connect } from 'react-redux';

import {
  videoThumbnails,
  videoThumbnailToDisplay
} from 'reducers/index';

import FrameGrab from 'components/media/FrameGrab';

import type { rootState } from 'reducers/index';

function mapStateToProps(state: rootState): Object {
  return {
    possibleThumbnails: videoThumbnails(state),
    initialThumbnail: videoThumbnailToDisplay(state)
  };
}

export default connect(
  mapStateToProps,
  undefined //no mapDispatchToProps
)(FrameGrab);
