/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import BasicsContextMenu from 'components/optionsbar/BasicsContextMenu';
import { updateCurrentPlan } from 'actions/PlanActions';
import {
  getCurrentPlan,
  getPlans,
  getCurrentLabelText
} from 'reducers/index';
import { canCreateLabel } from 'selectors/EnvironmentSelector';
import { getAvailableLabelsWithoutSelectedLabels, getLabelErrorGroup, labelsWithErrorsFirst } from 'selectors/LabelsSelector';
import { fetchLabelsAction, updateLabelText, addLabel, removeLabelFromMessage } from 'actions/LabelActions';

function mapStateToProps(state: rootState) {
  return {
    currentPlan: getCurrentPlan(state),
    plans: getPlans(state),
    selectedLabels: labelsWithErrorsFirst(state),
    availableLabels: getAvailableLabelsWithoutSelectedLabels(state),
    canCreateLabels: canCreateLabel(state),
    currentLabelText: getCurrentLabelText(state),
    labelErrorGroup: getLabelErrorGroup(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onSelectPlan: bindActionCreators(updateCurrentPlan, dispatch),
    onRemoveLabel: bindActionCreators(removeLabelFromMessage, dispatch),
    addLabel: bindActionCreators(addLabel, dispatch),
    updateLabelText: bindActionCreators(updateLabelText, dispatch),
    fetchLabelsAction: bindActionCreators(fetchLabelsAction, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BasicsContextMenu);
