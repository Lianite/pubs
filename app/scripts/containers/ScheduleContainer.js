/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setScheduledTime } from 'actions/MessageActions';
import MessageScheduler from 'components/MessageScheduler';
import { loadingMessageHasCompleted,
  scheduledDateTimeSelector,
  getUserTimezone,
  savingMessageHasCompleted } from 'reducers/index';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

function mapStateToProps(state: rootState): Object {
  const userTimezone = getUserTimezone(state);
  return {
    scheduledDatetime: scheduledDateTimeSelector(state),
    userTimezone: userTimezone,
    loadingMessageHasCompleted: loadingMessageHasCompleted(state),
    savingMessageHasCompleted: savingMessageHasCompleted(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    setScheduledTime: bindActionCreators(setScheduledTime, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageScheduler);
