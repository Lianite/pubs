/* @flow */
import * as HttpService from 'services/HttpService';

import type { VideoThumbnailType } from 'adapters/types';

export type PostSocialImageResponse = {
  data: {
    assetID: ?number,
    assetVersion: ?any,
    displayName: string,
    fullImage: string,
    id: number,
    image: string,
    imageHeight: number,
    imageWidth: number,
    imageSizeAfterResizing: number,
    mimeType: string,
    spredfastSummary: ?string,
    spredfastThumbnailUrls: Array<string>,
    spredfastTitle: string,
    summary: ?string,
    type: string
  };
  nextActions: {};
  uri: string;
  permissions: Array<any>;
  verbs: Array<any>;
};

export type PostSocialThumbnailAttachmentResponse = {
  data: {
    id: ?string,
    uri: ?string
  };
  nextActions: {};
  uri: string;
  permissions: Array<any>;
  verbs: Array<any>;
};

export type GetSessionNonceToken = {
  data: string;
  nextActions: {};
  uri: string;
  permissions: Array<any>;
  verbs: Array<any>;
};

type UploadedVideoType = {
  displayName: string,
  duration: number,
  id: number,
  size: number,
  summary: string,
  type: string,
  videoStatus: string,
  videoThumbnail: {},
  targetService: string
};

export type PostSocialVideoResponse = {
  socialVideos: Array<UploadedVideoType>,
  videoKey: string
};

export type TranscodingProgressResponse = {
  [videoId:string]: number
};

type VideoTranscodingResults = {
  videoId: number,
  videoPreviewUrl: string,
  videoStatus: string,
  size: number,
  thumbnails: Array<VideoThumbnailType>
};

export type TranscodingResultsResponse = Array<VideoTranscodingResults>;

export const postSocialImage = function(companyId: string, campaignId: string, file: File): Promise<PostSocialImageResponse> {
  return HttpService.postMultipartFile({
    url: `/ws/company/${companyId}/campaign/${campaignId}/message/socialImage/`,
    file
  });
};

export const postSocialThumbnailAttachment = function(companyId: string, campaignId: string, file: File): Promise<PostSocialThumbnailAttachmentResponse> {
  return HttpService.postMultipartFile({
    url: `/ws/company/${companyId}/campaign/${campaignId}/message/socialThumbnailAttachment/`,
    file
  });
};

export const getSessionNonceToken = function(companyId: string): Promise<GetSessionNonceToken> {
  return HttpService.post({
    url: `/ws/company/${companyId}/session/nonce`,
    body: {}
  });
};

export const postSocialVideo = function(companyId: string, campaignId: string, sessionNonceToken: string, file: File, progressHandler: ?Function, networks: Array<string>): Promise<PostSocialVideoResponse> {
  const headers = {
    'X-Session-Nonce': sessionNonceToken
  };
  const services = networks.length && `&service=${networks.join('&service=')}` || '';
  const handlers = {
    'progress': progressHandler
  };
  return HttpService.postMultipartFile({
    url: `/ws/multichannel/company/${companyId}/campaign/${campaignId}/message/socialVideo?fileType=${encodeURI(file.type)}&fileSize=${file.size}${services}`,
    file,
    handlers,
    headers,
    endpointPrefix: 'upload-',
    port: '8443'
  });
};

export const copySocialVideo = function(companyId: string, campaignId: string, videoKey: string, networks: Array<string>): Promise<PostSocialVideoResponse> {
  const services = networks.length && `&service=${networks.join('&service=')}` || '';
  return HttpService.post({
    url: `/ws/multichannel/company/${companyId}/campaign/${campaignId}/message/socialVideo/copy?videoKey=${videoKey}${services}`,
    body: {}
  });
};

export const getTranscodingProgress = function(companyId: string, campaignId: string, videoKey: string): Promise<TranscodingProgressResponse> {
  return HttpService.get({
    url: `/ws/multichannel/company/${companyId}/campaign/${campaignId}/message/socialVideo/transcodingProgress/?videoKey=${videoKey}`
  });
};

export const getTranscodingResults = function(companyId: string, campaignId: string, videoKey: string): Promise<TranscodingResultsResponse> {
  return HttpService.get({
    url: `/ws/multichannel/company/${companyId}/campaign/${campaignId}/message/socialVideo/transcodingResults/?videoKey=${videoKey}`
  });
};
