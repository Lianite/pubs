/* @flow */
import * as HttpService from 'services/HttpService';

export const checkLock = function(companyId: string, initiativeId: string, messageId: string): Promise<bool> {
  return HttpService.get({
    url: `/ws/company/${companyId}/campaign/${initiativeId}/message/messageGroup/${messageId}/checkLock`
  });
};

export const unlock = function(companyId: string, initiativeId: string, messageId: string, body: Object = {}): Promise<bool> {
  return HttpService.post({
    url: `/ws/company/${companyId}/campaign/${initiativeId}/message/messageGroup/${messageId}/unlock`,
    body
  });
};
