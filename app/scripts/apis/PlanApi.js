/* @flow */
import * as HttpService from 'services/HttpService';

export type PlanData = {
  id: string,
  name: string,
  currentStatus: string,
  status: string,
  companyId: number,
  campaignId: number,
  campaignName: string,
  color: string,
  planCountersDTO:{
    draftMessageCount: number,
    scheduledMessageCount: number,
    waitingApprovalMessageCount: number,
    publishedMessageCount: number,
    totalEngagementMessageCount: number
  }
};

export type GetPlanDataResponse = {
  data: Array<PlanData>,
  paging:{
    pageNumber: number,
    totalPages: number,
    pageSize: number,
    totalRecords: number
  }
};

export const getPlansData = function(companyId: string, campaignId: string): Promise<GetPlanDataResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/planner/plan?campaignIds%5B%5D=${campaignId}&excludePlanStatuses=COMPLETED&loadAll=true&pageSize=100&pageNumber=1`
  });
};
