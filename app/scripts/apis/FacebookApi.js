/* @flow */
import * as HttpService from 'services/HttpService';

import type { TargetingEntity } from 'adapters/types';

export type GetTargetingAutocompleteResponse = {
    data: {
        targetingResults: Array<TargetingEntity>
    }
};

export type TargetingQueryParam = {
  credentialUniqueIds: Array<string>,
  companyId: string,
  campaignId: string,
  query: string
};

export const getLanguageAutocomplete = function({
  credentialUniqueIds,
  companyId,
  campaignId,
  query = ''
}: TargetingQueryParam
): Promise<GetTargetingAutocompleteResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/campaign/${campaignId}/targeting/autocomplete/?targetingType=LOCALE&service=FACEBOOK&credentialUniqueIds%5B%5D=${credentialUniqueIds.join(',')}&startsWith=${query}`
  });
};

export const getLocationAutocomplete = function({
    credentialUniqueIds,
    companyId,
    campaignId,
    query = ''
  } : TargetingQueryParam
): Promise<GetTargetingAutocompleteResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/campaign/${campaignId}/targeting/autocomplete/?targetingType=GEOLOCATION&service=FACEBOOK&credentialUniqueIds%5B%5D=${credentialUniqueIds.join(',')}&startsWith=${query}`
  });
};
