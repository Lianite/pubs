/* @flow */
import * as HttpService from 'services/HttpService';
import type { UrlLinkTagVariableValues } from 'records/LinkTagsRecords';
import {
  getCurrentEnvironment } from 'utils/ApiUtils';
import {
  LinkTags,
  EnvironmentConstants
} from 'constants/ApplicationConstants';

const endpoints = {
  [EnvironmentConstants.PROD]: `//${LinkTags.PROD_HOSTNAME}`,
  [EnvironmentConstants.DEV]: `//${LinkTags.DEV_HOSTNAME}`,
  [EnvironmentConstants.STAGING]: `//${LinkTags.STAGE_HOSTNAME}`,
  [EnvironmentConstants.TEST]: `//${LinkTags.DEV_HOSTNAME}`
};

export const getLinkTagsEndpoint = function() {
  return endpoints[getCurrentEnvironment()];
};

export const createLinkTag = function(url: string) {
  return HttpService.post({
    url: '/tags',
    body: {url},
    headers: {
      'Spredfast-CSRF-Token': null
    },
    endpoint: getLinkTagsEndpoint()
  });
};

export const saveLinkTag = function(linkTagVariableValues: UrlLinkTagVariableValues, tagId: number) {
  return HttpService.put({
    url: `/tags/${tagId}`,
    body: linkTagVariableValues.toJS(),
    headers: {
      'Spredfast-CSRF-Token': null
    },
    endpoint: getLinkTagsEndpoint()
  });
};