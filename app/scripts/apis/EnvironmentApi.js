/* @flow */
import * as HttpService from 'services/HttpService';
import Promise from 'bluebird';

export type GetCsrfTokenResponse = {
  csrfToken: string
};
export const getCsrfToken = function(companyId: string): Promise<GetCsrfTokenResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/session/csrfToken/`
  });
};

export type GetUserDataResponse = {
  data: {
    id: number,
    timezone: {
      abbreviation: string;
      name: string;
      offset: string;
    },
    role: {
      publishingAccessLevel: {
        key: string
      };
      name: string;
      privileges: Array<string>;
    },
    firstName: string,
    lastName: string,
    thumb: string,
    directoryUserId: number,
    email: string
  };
  nextActions: {};
  uri: string;
  permissions: Array<any>;
  verbs: Array<any>;
};

export type GetCompanyDataResponse = {
  data: {
    companyMetadata: {
      contentLabelCreationRestricted: bool,
      salesforceEnabled: bool,
      shortenLinksByDefault: bool
    },
    directoryId: number,
    enabledFeatures: Array<string>,
    id: number,
    mediaConfigDTO: {
      imageAllowedMimeTypes: Array<string>
    },
    name: string,
    publishingConfiguration: {
      smartPublishingPoliciesEnabled: bool,
      smartPublishingSuggestionsEnabled: bool
    },
    socialInboxConfig: {
      salesforceEnabled: bool,
      socialCareWorkflowEnabled: bool,
      streamRefreshSeconds: number,
      twitterFeedbackCardsEnabled: bool
    }
  };
  nextActions: {};
  permissions: Array<any>;
  uri: string;
  verbs: Array<any>;
};


export const getUserData = function(companyId: string): Promise<GetUserDataResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/session/currentUser/`
  });
};

export const getCompanyData = function(companyId: string): Promise<GetCompanyDataResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/`
  });
};

export type GetLastCampaignResponse = {
  campaignId: number
};

export const getLastCampaign = function(): Promise<GetLastCampaignResponse> {
  return HttpService.get({
    url: '/user/get-last-campaign-id/'
  });
};

export type GetCapabilitiesResponse = {
  data:Object
};

export const getCapabilities = function(companyId: string): Promise<GetCapabilitiesResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/publishingCapability/`
  });
};

export type GetCompanyConfigurationResponse = {
  businessHourTimezone: {
    abbreviation: string,
    name: string,
    offset: string
  },
  businessHours: Array<any>,
  contentLabelCreationRestricted: bool,
  id: number,
  initiativeScopingLabels: bool,
  prioritizeOpenGraphDescriptionInLinkPreviews: bool,
  prioritizeOpenGraphImagesInLinkPreviews: bool,
  sfTrackingIdFormat: string,
  twitter: {
    defaultRetweetType: string,
    maxSearchStreamsPerAccount: number,
    maxUserStreamsPerAccount: number,
    supportedRetweetTypes: Array<string>
  }
};

export const getCompanyConfiguration = function(companyId: string): Promise<GetCompanyConfigurationResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/configuration`
  }).then(res => res.data);
};
