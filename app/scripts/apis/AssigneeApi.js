/* @flow */
import * as HttpService from 'services/HttpService';
import type { AssigneeData } from 'adapters/types';
import type { GetVoicesResponse } from 'apis/AccountsApi';

export type GetAssigneeResponse = {
  data: Array<AssigneeData>
};

export const getAllAssignees = function(companyId: string, campaignId: string): Promise<GetAssigneeResponse> {
  return HttpService.get({url:`/ws/company/${companyId}/userForFilter?campaignIds=${campaignId}&pageSize=-1`});
};

export const getValidAssignees = function(companyId: string, campaignId: string, voices: Array<number>): Promise<Array<AssigneeData>> {
  let voiceIDs = '';
  if (voices.length === 1) {
    voiceIDs = `${voices.pop()}`;
  } else if ( voices.length > 1) {
    voiceIDs = `${voices.pop()}`;
    voices.forEach((voiceId) => {
      voiceIDs = voiceIDs + `&voiceIDs=${voiceId}`;
    });
  }

  return HttpService.get({
    url:`/ws/multichannel/company/${companyId}/campaign/${campaignId}/voices/assignableUsers?voiceIDs=${voiceIDs}`
  });
};

export const getAssigneeVoices = function(companyId: string, campaignId: string, userId: number, networks: Array<string>): Promise<GetVoicesResponse> {
  const services = networks.length && `&services=${networks.join('&services=')}` || '';
  return HttpService.get({
    url: `/ws/company/${companyId}/expandedVoice/?campaignId=${campaignId}${services}&userId=${userId}`
  });
};
