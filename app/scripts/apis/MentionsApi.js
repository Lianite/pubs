/* @flow */
import * as HttpService from 'services/HttpService';
import _ from 'lodash';

import type { MentionQueryResponse } from 'types/MentionsTypes';

export const queryForMentions = function(companyId: string, initiativeId:string, service: string, query: string, credentialUniqueId: string = '', date: number = _.now()): Promise<MentionQueryResponse> {
  return HttpService.get({
    url: `/ws/company/${companyId}/campaign/${initiativeId}/mentions?q=${encodeURIComponent(query)}&service=${service}&credentialUniqueId=${credentialUniqueId}&_=${date}`
  });
};
