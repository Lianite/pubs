/* @flow */
import * as HttpService from 'services/HttpService';
import { getCopyApiString } from 'utils/ApiUtils';

import type { MessageObject } from 'adapters/types';

export type LinkPreviewResponse = {
  linkUrl: string;
  description: string;
  caption: string;
  title: string;
  sourceMedia: Array<Object>;
  previewLinkValidationNeeded: bool
};

export const getLinkPreview = function(companyId: string, campaignId: string, url: string): Promise<LinkPreviewResponse> {
  const encodedUrl = encodeURIComponent(url);
  return HttpService.get({
    url: `/ws/company/${companyId}/campaign/${campaignId}/linkPreview/?url=${encodedUrl}&forceFetch=false`
  });
};

export const createMessage = function(companyId: string, initiativeId: string, body: MessageObject, isCopy: bool): Promise<MessageObject> {
  const copy = getCopyApiString(isCopy);
  const url = `/ws/multichannel/company/${companyId}/campaign/${initiativeId}/message${copy}`;
  return HttpService.post({url, body});
};

export const editMessage = function(companyId: string, initiativeId: string, eventId: string, body: MessageObject): Promise<MessageObject> {
  const url = `/ws/multichannel/company/${companyId}/campaign/${initiativeId}/message/${eventId}`;
  return HttpService.put({url, body});
};

export const readMessage = function(companyId: string, initiativeId: string, eventId: string, isCopy: bool): Promise<MessageObject> {
  const copy = getCopyApiString(isCopy);
  return HttpService.get({
    url: `/ws/multichannel/company/${companyId}/campaign/${initiativeId}/message${copy}/${eventId}`
  });
};
