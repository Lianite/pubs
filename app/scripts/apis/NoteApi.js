/* @flow */
import * as HttpService from 'services/HttpService';
import type { Note } from 'adapters/types';

export const createNote = function(companyId: string, campaignId: string, messageId: string, note: string): Promise<Note> {
  return HttpService.post({
    url: `/ws/multichannel/company/${companyId}/campaign/${campaignId}/message/note?messageId=${messageId}&note=${encodeURIComponent(note)}`,
    body: {}
  });
};
