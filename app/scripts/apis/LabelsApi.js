/* @flow */
import * as HttpService from 'services/HttpService';
import { DatetimeConstants } from 'constants/ApplicationConstants';

export const getLabels = function(companyId: string, initiativeId: string, searchString: string = '') {
  let requestUrl = `/ws/company/${companyId}/campaign/${initiativeId}/contentLabel?`;

  if (searchString && searchString.length > 0) {
    requestUrl += 'title=' + searchString + '&';
  }

  requestUrl += '_=' + Math.floor(Date.now() / DatetimeConstants.MS_IN_SEC);
  return HttpService.get({url: requestUrl})
    .then((res) => {
      return res.data.map((label) => {
        return label.data;
      });
    });
};
