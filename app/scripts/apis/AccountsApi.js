/* @flow */
import * as HttpService from 'services/HttpService';

type CredentialData = {
  data: {
    accountType: string,
    companyId: number,
    id: number,
    name: string,
    service: string,
    socialNetworkId: string,
    targetingSupported: bool,
    uniqueId: string,
    voiceId: number,
    organicVisibilitySupported: bool
  };
  nextActions: {};
  uri: string,
  permissions: Array<any>,
  verbs: Array<any>
};

export type VoiceData = {
  id: number,
  name: string,
  credentials: Array<CredentialData>
};

export type VoiceResponse = {
  data: VoiceData,
  nextActions: {},
  uri: string,
  permissions: Array<any>,
  verbs: Array<any>
};

export type GetVoicesResponse = Array<VoiceResponse>;

export const getVoices = function(companyId: string, initiativeId: string, networks: Array<string>): Promise<GetVoicesResponse> {
  const services = networks.length && `&services=${networks.join('&services=')}` || '';
  return HttpService.get({
    url: `/ws/company/${companyId}/expandedVoice/?campaignId=${initiativeId}${services}&socialObjectType=NOTE`
  });
};
