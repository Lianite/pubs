/* @flow */

import { EnvironmentConstants } from 'constants/ApplicationConstants';

export default {
  [EnvironmentConstants.PROD]: {
    googleplus: false,
    linkTags: false,
    facebookTargeting: false
  },
  [EnvironmentConstants.STAGING]: {
    googleplus: true,
    linkTags: true,
    facebookTargeting: true
  },
  [EnvironmentConstants.DEV]: {
    googleplus: true,
    linkTags: true,
    facebookTargeting: true
  },
  [EnvironmentConstants.TEST]: {
    googleplus: true, //Don't turn this off in test or tests will probably break
    linkTags: true,
    facebookTargeting: true
  }
};
