import React, {
  Component
} from 'react';
import ReactDOM from 'react-dom';
import { ReduxRouter } from 'redux-router';
import { Provider } from 'react-redux';
import createStore from './createStore';
import Promise from 'bluebird';
import { SegmentProductConstants } from 'constants/SegmentConstants';
import { initSegmentService } from '@spredfast/sf-segment-js';
import { getConversationsEnviromentDetails } from 'utils/ApiUtils';
import { ReduxModal } from '@spredfast/react-lib';
import 'babel-polyfill';

//styles
import 'rad/styles/_fonts.scss';
import 'rad/styles/_rad-icons-bootstrap.scss';
import 'styles/main.scss';
import 'react-widgets/lib/scss/react-widgets.scss';
import 'styles/icons/partials/_publishingIcons.scss';

import '@spredfast/react-lib/styles/Modal.scss';

import routes from 'routes';

window.PublishingApp = window.PublishingApp || {};

Promise.config({ cancellation: true });

initSegmentService({
  environment: getConversationsEnviromentDetails().environmentUrl,
  product: SegmentProductConstants.MULTICHANNEL_PUBLISHING,
  segmentKey: window.PublishingApp.segmentKey
});

if (window.LAST_COMMIT_HASH) {
  window.PublishingApp.commitHash = window.LAST_COMMIT_HASH;
} else {
  window.PublishingApp.commitHash = 'dev';
}

const store = createStore();

export default class PublishingDialogApp extends Component {
  static displayName = 'PublishingDialogApp';

  render() {
    return (
      <Provider store={store}>
        <ReduxRouter>
          { routes }
        </ReduxRouter>
      </Provider>
    );
  }
}

ReactDOM.render(
  <PublishingDialogApp />
, document.getElementById('spredfast-publishing-ui-container'));

ReactDOM.render(
  <Provider store={store}>
    <ReduxModal.Host />
  </Provider>
, document.getElementById('modals'));

document.body.style.visibility = 'visible';
