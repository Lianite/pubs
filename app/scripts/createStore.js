import { unstable_batchedUpdates as batchedUpdates } from 'react-dom';
import { batchedSubscribe } from 'redux-batched-subscribe';
import { createStore, applyMiddleware, compose } from 'redux';

import { reduxReactRouter } from 'redux-router';
import { createHistory } from 'history';

import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';
import rootReducer from 'reducers/RootReducer';
import rootSaga from 'sagas/rootSaga';

// here we implement a bit of an expanded createStore and pass it our reducers
// as well as some helpful middleware.

export default function() {
  const sagaMiddleware = createSagaMiddleware();

  const finalCreateStore = compose(
    applyMiddleware(
      thunk,
      sagaMiddleware
    ),
    reduxReactRouter({
      createHistory
    }),
    batchedSubscribe(batchedUpdates)
    // the following line is middleware that is used to kick off the redux
    // chrome plugin for debugging redux state, uncomment to use it!
    //
    // , window.devToolsExtension ? window.devToolsExtension() : undefined
  )(createStore);

  const store = finalCreateStore(rootReducer);
  sagaMiddleware.run(rootSaga, store.dispatch);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
