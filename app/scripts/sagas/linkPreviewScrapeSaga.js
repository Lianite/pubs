/* @flow */
import { put, cancel, fork, select, take } from 'redux-saga/effects';
import { cancellableCall } from 'sagas/effects';
import { LinkPreviewActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { getLinkPreview } from 'apis/MessageApi';
import { scrapingLinkPreview, updateLinkPreview, linkScrapeSuccessful } from 'actions/LinkPreviewActions';
import { OrderedSet as ImmutableOrderedSet } from 'immutable';
import type { Action } from 'actions/types';
import { convertToMediaRecord } from 'adapters/LinkPreviewAdapter';
import { extractUrlFromSourceMedia } from 'utils/LinkUtils';
import { canAddAttachmentByType } from 'selectors/AttachedMediaSelector';

function* beginLinkScrape(action: Action<String>):any {
  const url = action.payload;
  try {
    const compCampIds = yield select(getCompanyCampaignFromRoute);

    if (compCampIds) {
      yield put(scrapingLinkPreview());
      const res = yield cancellableCall(
        getLinkPreview,
        compCampIds.companyId, compCampIds.campaignId, url
      );

      if (res) {
        yield put(linkScrapeSuccessful());

        const thumbnailUrl = res && res.sourceMedia && res.sourceMedia[0] && res.sourceMedia[0].src;

        let linkPreview = {
          ...res,
          linkUrl: url,
          thumbnailUrl,
          currentlyUploadingMedia: false,
          customUploadedMedia: ImmutableOrderedSet(),
          sourceMedia: ImmutableOrderedSet(convertToMediaRecord(extractUrlFromSourceMedia(res.sourceMedia)))
        };

        yield put(updateLinkPreview(linkPreview));
      }
    }
  } catch (error) {
    yield put(updateLinkPreview({
      linkUrl: url,
      caption: url
    }));
  }
}

export default function* watchAddLinkPreviewHistory(): Generator<any, any, any> {
  /* eslint-disable no-constant-condition */
  while (true) {
    const action = yield take(LinkPreviewActionConstants.ADD_LINK_PREVIEW);
    const canAdd = yield select(canAddAttachmentByType);

    if ((canAdd || {}).linkPreview) {
      const linkScrapeTask = yield fork(beginLinkScrape, action);

      const finishAction = yield take(action =>
        action.type === LinkPreviewActionConstants.CANCEL_LINK_SCRAPE || action.type === LinkPreviewActionConstants.LINK_SCRAPE_SUCCESSFUL || action.type === MessageActionConstants.CLEAR_MESSAGE
      );
      if (finishAction && (finishAction.type === LinkPreviewActionConstants.CANCEL_LINK_SCRAPE || finishAction.type === MessageActionConstants.CLEAR_MESSAGE)) {
        yield cancel(linkScrapeTask);
      }
    }
  }
  /* eslint-enable no-constant-condition */
}
