/* @flow */
import { call } from 'redux-saga/effects';
import { CANCEL } from 'redux-saga';

export function* cancellableCall(fn: Function, ...args: Array<any>): Generator<any, any, any> {

  return yield call(() => {
    const promise = fn(...args);

    promise[CANCEL] = () => {
      promise.cancel();
    };

    return promise;
  });
}
