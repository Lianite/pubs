/* @flow */
import { put, cancel, fork, select, take } from 'redux-saga/effects';
import { cancellableCall } from 'sagas/effects';
import { MediaActionConstants } from 'constants/ActionConstants';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { postSocialThumbnailAttachment } from 'apis/MediaApi';
import { requestUploadVideoThumb, uploadVideoThumbSucceeded, uploadVideoThumbFailed } from 'actions/MediaActions';
import _ from 'lodash';
import type { Action } from 'actions/types';

function* uploadVideoThumb(action: Action<Array<File>>):any {
  const files = [action.payload];
  try {
    const compCampIds = yield select(getCompanyCampaignFromRoute);

    if (compCampIds) {
      yield put(requestUploadVideoThumb());
      const res = yield cancellableCall(
        postSocialThumbnailAttachment,
        compCampIds.companyId, compCampIds.campaignId, files[0]
      );
      const id = _.get(res, 'data.id', '');
      const uri = _.get(res, 'data.uri', '');
      if (uri && id) {
        yield put(uploadVideoThumbSucceeded({id, uri}));
      } else {
        throw new Error('Could not upload image');
      }
    }
  } catch (error) {
    yield put(uploadVideoThumbFailed(error.message));
  }
}

export default function* watchUploadVideoThumbHistory(): Generator<any, any, any> {
  /* eslint-disable no-constant-condition */
  while (true) {
    const action = yield take(MediaActionConstants.UPLOAD_VIDEO_THUMB);
    const uploadVideoThumbTask = yield fork(uploadVideoThumb, action);

    const finishAction = yield take(action =>
      action.type === MediaActionConstants.UPLOAD_VIDEO_THUMB_CANCELED || action.type === MediaActionConstants.UPLOAD_VIDEO_THUMB_SUCCEEDED || action.type === MediaActionConstants.UPLOAD_VIDEO_THUMB_FAILED
    );
    if (finishAction && finishAction.type === MediaActionConstants.UPLOAD_VIDEO_THUMB_CANCELED) {
      yield cancel(uploadVideoThumbTask);
    }
  }
  /* eslint-enable no-constant-condition */
}
