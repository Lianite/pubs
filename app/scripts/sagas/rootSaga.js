/* @flow */
import watchUploadLinkPreviewThumbHistory from 'sagas/uploadLinkPreviewThumbSaga';
import {
    watchUploadImagesHistory,
    watchUploadVideoHistory
} from 'sagas/uploadMediaSaga';
import watchAddLinkPreviewHistory from 'sagas/linkPreviewScrapeSaga';
import watchUploadVideoThumbHistory from 'sagas/uploadVideoCustomThumbnailSaga';

export default function* rootSaga(dispatch: Function): Generator<any, any, any> {
  yield [
    watchUploadImagesHistory(),
    watchUploadVideoHistory(dispatch),
    watchUploadLinkPreviewThumbHistory(),
    watchAddLinkPreviewHistory(),
    watchUploadVideoThumbHistory()
  ];
}
