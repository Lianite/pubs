/* @flow */
import { put, cancel, fork, select, take } from 'redux-saga/effects';
import { cancellableCall } from 'sagas/effects';
import { LinkPreviewActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { linkPreviewCustomUploadedMedia } from 'reducers/index';
import { postSocialImage } from 'apis/MediaApi';
import { requestUploadCustomThumb, uploadCustomThumbSucceeded, uploadCustomThumbFailed, editLinkPreview } from 'actions/LinkPreviewActions';
import { LinkPreviewMaxCustomThumbs } from 'constants/ApplicationConstants';
import { LinkPreviewMediaRecord } from 'records/LinkPreviewRecords';
import type { OrderedSet as ImmutableOrderedSetType } from 'immutable';
import type { Action } from 'actions/types';

const updateCustomThumbSet = (customThumbs: any, id:?number, src:?string):ImmutableOrderedSetType<LinkPreviewMediaRecord> => {
  let customThumbsToReturn = customThumbs;
  const possibleNewEntry = new LinkPreviewMediaRecord({id, src});
  //First, check if the existing id/src pair is in the list,
  if (id && src
    && !customThumbs.has(possibleNewEntry)) {
    //If we already have the max, replace the first and move it to the end otherwise, push
    if (customThumbs.size === LinkPreviewMaxCustomThumbs) {
      const firstItem = customThumbs.first();
      customThumbsToReturn = customThumbs.remove(firstItem).add(possibleNewEntry);
    } else {
      customThumbsToReturn = customThumbs.add(possibleNewEntry);
    }
  }

  return customThumbsToReturn;
};

function* uploadLinkPreviewThumb(action: Action<Array<File>>):any {
  const files = [action.payload];
  try {
    const compCampIds = yield select(getCompanyCampaignFromRoute);

    if (compCampIds) {
      yield put(requestUploadCustomThumb());
      const res = yield cancellableCall(
        postSocialImage,
        compCampIds.companyId, compCampIds.campaignId, files[0]
      );

      if (res) {
        yield put(uploadCustomThumbSucceeded());

        const updatedCustomThumbSet = updateCustomThumbSet(yield select(linkPreviewCustomUploadedMedia), res.data.id, res.data.image);
        yield put(editLinkPreview({
          customUploadedMedia: updatedCustomThumbSet,
          thumbnailUrl: res.data.image
        }));
      }
    }
  } catch (error) {
    yield put(uploadCustomThumbFailed(error.message));
  }
}

export default function* watchUploadLinkPreviewThumbHistory(): Generator<any, any, any> {
  /* eslint-disable no-constant-condition */
  while (true) {
    const action = yield take(LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB);
    const uploadLinkPreviewThumbTask = yield fork(uploadLinkPreviewThumb, action);

    const finishAction = yield take(action =>
      action.type === LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_CANCELED ||
      action.type === LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_SUCCEEDED ||
      action.type === LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_FAILED ||
      action.type === MessageActionConstants.CLEAR_MESSAGE
    );
    if (finishAction && (finishAction.type === LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_CANCELED || finishAction.type === MessageActionConstants.CLEAR_MESSAGE)) {
      yield cancel(uploadLinkPreviewThumbTask);
    }
  }
  /* eslint-enable no-constant-condition */
}
