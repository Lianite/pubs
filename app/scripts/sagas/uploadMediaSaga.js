/* @flow */
import _ from 'lodash';
import { put, cancel, fork, select, take, call } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {
  uploadImageSucceeded,
  uploadImageFailed,
  requestUploadImage,
  requestUploadVideo,
  uploadVideoProgress,
  uploadVideoSucceeded,
  requestCopyVideo,
  copyVideoSucceeded,
  uploadVideoFailed,
  requestTranscodeVideoProgress,
  updateTranscodeVideoProgress,
  transcodeVideoSucceeded,
  requestTranscodeVideoResults,
  transcodeVideoResultsSucceeded } from 'actions/MediaActions';
import { videoUploadNetworks } from 'selectors/AttachedMediaSelector';
import { cancellableCall } from 'sagas/effects';
import {
  postSocialImage,
  postSocialVideo,
  copySocialVideo,
  getSessionNonceToken,
  getTranscodingProgress,
  getTranscodingResults } from 'apis/MediaApi';
import { MediaActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { RequestStatus } from 'constants/ApplicationConstants';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { imageCanBeUploaded, videoKey, videoUploadStatus } from 'reducers/index';

import type { Action } from 'actions/types';

function* uploadImageMedia(action: Action<Array<File>>) {
  const files = action.payload;
  try {
    const canUpload = yield select(imageCanBeUploaded);
    const compCampIds = yield select(getCompanyCampaignFromRoute);
    if (canUpload && compCampIds) {
      yield put(requestUploadImage());
      const res = yield cancellableCall(
        postSocialImage,
        compCampIds.companyId, compCampIds.campaignId, files[0]
      );

      if (res) {
        yield put(uploadImageSucceeded({
          id: res.data.id,
          fullUrl: res.data.fullImage,
          thumbUrl: res.data.image,
          filesize: res.data.imageSizeAfterResizing,
          mimeType: res.data.mimeType,
          imageWidth: res.data.imageWidth,
          imageHeight: res.data.imageHeight
        }));
      }
    }
  } catch (error) {
    yield put(uploadImageFailed(error.message));
  }
}

export function* watchUploadImagesHistory(): Generator<any, any, any> {
  /* eslint-disable no-constant-condition */
  while (true) {
    const action = yield take(MediaActionConstants.UPLOAD_IMAGE);
    const uploadMediaTask = yield fork(uploadImageMedia, action);
    const finishAction = yield take(action =>
      action.type === MediaActionConstants.UPLOAD_IMAGE_CANCELED || action.type === MediaActionConstants.UPLOAD_IMAGE_SUCCEEDED || action.type === MediaActionConstants.UPLOAD_IMAGE_FAILED || action.type === MessageActionConstants.CLEAR_MESSAGE
    );
    if (finishAction && (finishAction.type === MediaActionConstants.UPLOAD_IMAGE_CANCELED || MessageActionConstants.CLEAR_MESSAGE)) {
      yield cancel(uploadMediaTask);
    }
  }
  /* eslint-enable no-constant-condition */
}

function* watchTranscodeProgress() {
  try {
    const uploadedVideoKey = yield select(videoKey);
    const uploadStatus = yield select(videoUploadStatus);
    const compCampIds = yield select(getCompanyCampaignFromRoute);
    if (uploadStatus === RequestStatus.STATUS_LOADED && uploadedVideoKey && compCampIds) {
      yield put(requestTranscodeVideoProgress());

      let transcodingProgress = yield call(getTranscodingProgress, compCampIds.companyId, compCampIds.campaignId, uploadedVideoKey);
      yield put(updateTranscodeVideoProgress(transcodingProgress));
      while (_.values(transcodingProgress).some(value => value < 100)) {
        yield delay(2500);
        transcodingProgress = yield call(getTranscodingProgress, compCampIds.companyId, compCampIds.campaignId, uploadedVideoKey);
        yield put(updateTranscodeVideoProgress(transcodingProgress));
      }

      yield put(transcodeVideoSucceeded());
      yield put(requestTranscodeVideoResults());

      const videoResult = yield call(getTranscodingResults, compCampIds.companyId, compCampIds.campaignId, uploadedVideoKey);
      if (!(videoResult)) {
        throw new Error('unable to get transcoding video results');
      }
      yield put(transcodeVideoResultsSucceeded(videoResult));
    }
  } catch (error) {
    yield put(uploadVideoFailed(error.message));
  }
}

function* uploadVideo(files: Array<File>, dispatch: Function) {
  try {
    const canUpload = yield select(imageCanBeUploaded);
    const compCampIds = yield select(getCompanyCampaignFromRoute);
    const networks = yield select(videoUploadNetworks);

    if (compCampIds && canUpload) {
      let uploadedVideoKey = '';

      yield put(requestUploadVideo(files[0]));

      const sessionNonceToken = yield call(getSessionNonceToken, compCampIds.companyId);
      if (!(sessionNonceToken && sessionNonceToken.data)) {
        throw new Error('unable to get session nonce token');
      }

      const onUploadProgress = (e): void => {
        dispatch(uploadVideoProgress(e.percent));
      };

      const uploadVideoResponse = yield cancellableCall(postSocialVideo, compCampIds.companyId, compCampIds.campaignId, sessionNonceToken.data, files[0], onUploadProgress, networks);
      const uploadedVideo = uploadVideoResponse && uploadVideoResponse.socialVideos && uploadVideoResponse.socialVideos.length;
      uploadedVideoKey = uploadVideoResponse && uploadVideoResponse.videoKey;
      if (!(uploadedVideo && uploadedVideoKey)) {
        throw new Error('unable to upload a video');
      }

      yield put(uploadVideoSucceeded(uploadVideoResponse));
    }

  } catch (error) {
    yield put(uploadVideoFailed(error.message));
  }
}

function* copyVideo(newNetworks: Array<string>) {
  try {
    const existingVideoKey = yield select(videoKey);
    const compCampIds = yield select(getCompanyCampaignFromRoute);

    if (compCampIds && existingVideoKey) {
      const uploadedVideoKey = existingVideoKey;

      yield put(requestCopyVideo());

      const copyVideoResponse = yield cancellableCall(copySocialVideo, compCampIds.companyId, compCampIds.campaignId, uploadedVideoKey, newNetworks);
      if (!copyVideoResponse || copyVideoResponse.videoKey !== uploadedVideoKey) {
        throw new Error('Video key returned does not match expected');
      }

      yield put(copyVideoSucceeded(copyVideoResponse));
    }
  } catch (error) {
    yield put(uploadVideoFailed(error.message));
  }
}

const GateReturnType = {
  SUCCESS: 'SUCCESS',
  FAILED: 'FAILED',
  CANCELED: 'CANCELED',
  REPROCESS: 'REPROCESS'
};

function* gateAfterFork(successActionType: string, taskToCancel: any, singleLoop: ?bool, taskToCancelOnReprocess:?any) {
  let returnValue = {
    type: GateReturnType.SUCCESS,
    action: {}
  };
  let continueToGate = true;

  while (continueToGate) {
    continueToGate = false;

    const gateAction = yield take(action =>
      action.type === successActionType ||
      action.type === MediaActionConstants.UPLOAD_VIDEO_CANCELED ||
      action.type === MediaActionConstants.UPLOAD_VIDEO_FAILED ||
      action.type === MediaActionConstants.REPROCESS_VIDEO ||
      action.type === MessageActionConstants.CLEAR_MESSAGE
    );

    if (gateAction && (gateAction.type === MediaActionConstants.UPLOAD_VIDEO_CANCELED || gateAction.type === MessageActionConstants.CLEAR_MESSAGE)) {
      returnValue.type = GateReturnType.CANCELED;
      //cancel the associated task
      if (taskToCancel) {
        yield cancel(taskToCancel);
      }
    } else if (gateAction && gateAction.type === MediaActionConstants.UPLOAD_VIDEO_FAILED) {
      returnValue.type = GateReturnType.FAILED;
    } else if (gateAction && gateAction.type === MediaActionConstants.REPROCESS_VIDEO) {
      returnValue.type = GateReturnType.REPROCESS;
      returnValue.action = gateAction;
      if (taskToCancelOnReprocess) {
        yield cancel(taskToCancelOnReprocess);
        taskToCancelOnReprocess = null;
      }
      if (!singleLoop) {
        continueToGate = true;
      }
    }
  }
  return returnValue;
}

export function* watchUploadVideoHistory(dispatch: Function): Generator<any, any, any> {
  /* eslint-disable no-constant-condition */
  while (true) {
    try {
      const action = yield take(action => action.type === MediaActionConstants.UPLOAD_VIDEO || action.type === MediaActionConstants.REPROCESS_VIDEO);
      if (action) {
        let newNetworks = action.payload;
        let taskToCancel;
        let successActionType;
        let shouldCopy = false;

        //UPLOAD
        if (action.type === MediaActionConstants.UPLOAD_VIDEO) {
          taskToCancel = yield fork(uploadVideo, action.payload, dispatch);
          successActionType = MediaActionConstants.UPLOAD_VIDEO_SUCCEEDED;

          const callResult = yield call(gateAfterFork, successActionType, taskToCancel);
          let callResultSent = callResult ? callResult.type : GateReturnType.FAILED;
          let callResultActionPayload = callResult ? callResult.action.payload : [];

          if (callResultSent === GateReturnType.FAILED || callResultSent === GateReturnType.CANCELED) {
            throw new Error();
          } else if (callResultSent === GateReturnType.REPROCESS) {
            shouldCopy = true;
            newNetworks = callResultActionPayload;
          }
        }

        //COPY AND TRANSCODE
        let keepCopyingAndTranscoding = true;
        while (keepCopyingAndTranscoding) {
          keepCopyingAndTranscoding = false;
          //COPY
          if (action.type === MediaActionConstants.REPROCESS_VIDEO || shouldCopy) {
            let keepCopying = true;
            while (keepCopying) {
              keepCopying = false;
              taskToCancel = yield fork(copyVideo, newNetworks);
              successActionType = MediaActionConstants.COPY_VIDEO_SUCCEEDED;

              const callResult = yield call(gateAfterFork, successActionType, taskToCancel);
              let callResultSent = callResult ? callResult.type : GateReturnType.FAILED;
              let callResultActionPayload = callResult ? callResult.action.payload : [];
              if (callResultSent === GateReturnType.FAILED || callResultSent === GateReturnType.CANCELED) {
                throw new Error();
              } else if (callResultSent === GateReturnType.REPROCESS) {
                keepCopying = true;
                newNetworks = callResultActionPayload;
              }
            }
          }

          //TRANSCODE
          const transcodeTask = yield fork(watchTranscodeProgress);

          const callResult = yield call(gateAfterFork, MediaActionConstants.TRANSCODE_VIDEO_SUCCEEDED, transcodeTask, true/*single gate loop*/, transcodeTask);
          let callResultSent = callResult ? callResult.type : GateReturnType.FAILED;
          let callResultActionPayload = callResult ? callResult.action.payload : [];
          if (callResultSent === GateReturnType.FAILED || callResultSent === GateReturnType.CANCELED) {
            throw new Error();
          } else if (callResultSent === GateReturnType.REPROCESS) {
            keepCopyingAndTranscoding = true;
            shouldCopy = true;
            newNetworks = callResultActionPayload;
          }
        } //end copy / transcode loop
      } //if action
    } catch (error) {
      //no-op, we just want to loop back
    }
  }
  /* eslint-enable no-constant-condition */
};
