/* @flow */
import { createAction } from 'redux-actions';
import { UIStateActionConstants } from 'constants/ActionConstants';
import { voiceSelectorOpen, currentlyDraggingMedia, eventIdSelector } from 'reducers/index';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import { closeEmbedded } from 'utils/EmbeddedUtils';
import { resetFullMessage } from 'actions/IndexActions';
import { checkLock, unlock } from 'apis/UIStateApi';
import {
  messageHasEventId,
  messageHasCopyId,
  getEventId,
  getCopyId } from 'selectors/RouterSelector';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import _ from 'lodash';

export const openSidebar  = createAction(UIStateActionConstants.OPEN_SIDEBAR);
export const closeSidebar  = createAction(UIStateActionConstants.CLOSE_SIDEBAR);
export const voiceDropdownToggle = createAction(UIStateActionConstants.TOGGLE_VOICE_SELECTOR);
export const dragStateChanged = createAction(UIStateActionConstants.DRAG_STATE_CHANGED);
export const clearUIState = createAction(UIStateActionConstants.CLEAR_UI_STATE);
export const previewVisibilityChanged = createAction(UIStateActionConstants.PREVIEW_VISIBILITY_CHANGED);

export const toggleSidebar = (sidebarOpen: bool, currentActivePage: string, requestedActivePage: string) => {
  return (dispatch: Dispatch): void  => {
    if (!sidebarOpen || (currentActivePage !== requestedActivePage)) {
      dispatch(openSidebar(requestedActivePage));
    } else if (sidebarOpen && (currentActivePage === requestedActivePage)) {
      dispatch(closeSidebar());
    }
  };
};

export const closeSidebarIfOpen = (sidebarOpen: bool) => {
  return (dispatch: Dispatch): void => {
    if (sidebarOpen) {
      dispatch(closeSidebar());
    }
  };
};

export const toggleVoiceSelectorIfNecessary = (action: ?string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    let shouldSend: bool = false;
    let currentVoiceSelectorOpen = voiceSelectorOpen(getState());

    if (action && (action === 'focus' && !currentVoiceSelectorOpen) ) {
      shouldSend = true;
    } else if (action && (action === 'blur' && currentVoiceSelectorOpen) ) {
      shouldSend = true;
    } else if (!action) {
      shouldSend = true;
    }

    if (shouldSend) {
      return dispatch(voiceDropdownToggle(action));
    }
  };
};

export const updateDragStateIfNecessary = (isDragging: bool) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const currentDragging = currentlyDraggingMedia(getState());
    if (isDragging !== currentDragging) {
      return dispatch(dragStateChanged(isDragging));
    }
  };
};

const closeOrClear = (dispatch: Dispatch) => {
  const intentClosed = closeEmbedded();
  if (!intentClosed) {
    return dispatch(resetFullMessage());
  }
};

export const cancelCloseButtonClicked = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const messageId = _.toString((messageHasEventId(getState()) && getEventId(getState())) || (messageHasCopyId(getState()) && getCopyId(getState())) || -1);
    const compCampIds = getCompanyCampaignFromRoute(getState());
    if (eventIdSelector(getState())) {
      return checkLock(compCampIds.companyId, compCampIds.campaignId, messageId)
        .then((res) => {
          if (res) {
            return unlock(compCampIds.companyId, compCampIds.campaignId, messageId)
              .then((res) => {
                closeOrClear(dispatch);
              })
              .catch((res) => {
                closeOrClear(dispatch);
              });
          } else {
            closeOrClear(dispatch);
          }
        })
        .catch((res) => {
          closeOrClear(dispatch);
        });
    } else {
      closeOrClear(dispatch);
    }
  };
};

export const updatePreviewVisibility = (previewIsVisible: bool) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return dispatch(previewVisibilityChanged(previewIsVisible));
  };
};
