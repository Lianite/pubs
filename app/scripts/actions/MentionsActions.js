/* @flow */
import { createAction } from 'redux-actions';
import { EditorState, Entity } from 'draft-js';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { MentionsActionConstants } from 'constants/ActionConstants';
import { getCurrentMentionsList, getMentionsQueryUiState } from 'reducers/index';
import { getFirstNetworkCheckedCredentialUniqueId } from 'selectors/AccountsSelectors';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import * as MentionsApi from 'apis/MentionsApi';
import { MentionsRecord } from 'types/MentionsTypes.js';
import _ from 'lodash';
import { MENTIONS_QUERY_DEBOUNCE } from 'constants/UILayoutConstants';

import type { SupportedNetworksType } from 'constants/ApplicationConstants';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

const MENTION_DRAFT_ENTITY_TYPE = 'MENTION';

const characterInMention = (character) => {
  const entityKey = character.getEntity();
  return entityKey !== null && Entity.get(entityKey).get('type') === MENTION_DRAFT_ENTITY_TYPE;
};

/**
 * Checks to see if:
 * a.) a network has been added
 * b.) the number of mentions within a network has changed
 * c.) the number of networks and mentions within networks are the same checks to make
 *     sure those lists of networks have changed
 *
 * If any of the above criteria has been met, the mentions have changed, and will be communicated
 * to the state.
 */
const mentionsHaveChanged = (oldMentions: ImmutableMap<string, ImmutableList<MentionsRecord>>, newMentions: ImmutableMap<string, ImmutableList<MentionsRecord>>): bool => {
  let mentionsHaveChangedFlag = false;
  if (oldMentions.size === newMentions.size) {
    oldMentions.forEach((network, networkKey) => {
      if (newMentions.get(networkKey) && network.size === newMentions.get(networkKey).size) {
        network.forEach((mention, mentionKey) => {
          if (mention.profileId !== newMentions.get(networkKey).get(mentionKey).profileId) {
            mentionsHaveChangedFlag = true;
            return false;
          }
        });
      } else {
        mentionsHaveChangedFlag = true;
        return false;
      }
    });
    return mentionsHaveChangedFlag;
  } else {
    return true;
  }
};

export const _updateMentions = createAction(MentionsActionConstants.UPDATE_MENTIONS);
export const _openMentionsQuery = createAction(MentionsActionConstants.OPEN_MENTIONS_QUERY);
export const _closeMentionsQuery = createAction(MentionsActionConstants.CLOSE_MENTIONS_QUERY);

export const requestMentionsQueryCompleted = createAction(MentionsActionConstants.MENTIONS_RECEIVED);
export const requestMentionsQueryFailed = createAction(MentionsActionConstants.MENTIONS_DATA_ERROR);
export const requestMentionsQuery = createAction(MentionsActionConstants.REQUEST_MENTIONS_DATA);
export const recordCurrentMentionOffset = createAction(MentionsActionConstants.CURRENT_MENTION_OFFSET);

export const openMentionsQuery = (network: SupportedNetworksType) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const isMentionsOpen = getMentionsQueryUiState(getState(), network);
    if (!isMentionsOpen) {
      return dispatch(_openMentionsQuery(network));
    }
  };
};

export const closeMentionsQuery = (network: SupportedNetworksType) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const isMentionsOpen = getMentionsQueryUiState(getState(), network);
    if (isMentionsOpen) {
      return dispatch(_closeMentionsQuery(network));
    }
  };
};

export const sendOutQuery = (network: string, mentionToQueryFor: string, offset: string, state: rootState, date?: number) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const compCampIds = getCompanyCampaignFromRoute(state);
    // TODO: Change this to a switch or something as more networks get added
    let facebookUniqueId = getFirstNetworkCheckedCredentialUniqueId(getState(), network);
    return MentionsApi.queryForMentions(compCampIds.companyId, compCampIds.campaignId, network, mentionToQueryFor, facebookUniqueId, date)
    .then(res => {
      let mentionsRecordArray: Array<MentionsRecord> = [];
      res.data.forEach((mention) => {
        mention.offsetKey = offset;
        mentionsRecordArray.push(new MentionsRecord(mention));
      });

      dispatch(requestMentionsQueryCompleted(mentionsRecordArray));
      dispatch(openMentionsQuery(network));
    }).catch(err => {
      dispatch(requestMentionsQueryFailed(err));
      dispatch(openMentionsQuery(network)); //to show the error
    });
  };
};

const debounceQueryCall = _.debounce( (dispatch, network, mentionToQueryFor, offset, state) => {
  return dispatch(sendOutQuery(network, mentionToQueryFor, offset, state));
}, MENTIONS_QUERY_DEBOUNCE);

export const queryForMentions = (mentionToQueryFor: string, offset: string, network: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    dispatch(requestMentionsQuery());
    debounceQueryCall(dispatch, network, mentionToQueryFor, offset, getState());
  };
};

/* *
 * Takes in the EditorStateMap and pulls out all of the mention information from them
 * so that it can be saved into the store. This gets called each time the editor is
 * updated
* */
export const updateMentions = (editorStateMap: EditorState) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();

    let mentions = ImmutableMap();
    let currentMentionsList = getCurrentMentionsList(state);

    editorStateMap.keySeq().toArray().forEach((key) => {
      let editorState = editorStateMap.get(key);
      let networkMentions = ImmutableList();

      let offsetBeforeBlock = 0;
      editorState
        .getCurrentContent()
        .getBlocksAsArray()
        .forEach((block) => {
          block.findEntityRanges(characterInMention, (start, end) => {
            const mention = Entity.get(block.getEntityAt(start)).getData().mention;
            networkMentions = networkMentions.push(mention.withMutations(updatedMention => {
              updatedMention.set('offsetBegin', offsetBeforeBlock + start)
                .set('offsetEnd', offsetBeforeBlock + end);
            }));
          });
          offsetBeforeBlock += block.getLength() + 1; // include the \n
        });
      mentions = mentions.set(key, networkMentions);
    });

    if (mentionsHaveChanged(currentMentionsList, mentions)) {
      return dispatch(_updateMentions(mentions));
    }

    return;
  };
};
