/* @flow */
import _ from 'lodash';
import { createAction } from 'redux-actions';
import { LinkTagsActionConstants } from 'constants/ActionConstants';
import * as LinkTagsApi from 'apis/LinkTagsApi';
import {
    linkTagsUrls,
    checkedCredentialsSelector,
    getSelectedLabels
} from 'reducers/index';
import {
  linkTagsReadyToBeSaved,
  linkTagsEnable,
  getMessageTitleByNetwork
} from 'selectors/LinkTagsSelector';
import {
  DefaultNetwork,
  LinkTags } from 'constants/ApplicationConstants';

import {
  List as ImmutableList,
  Map as ImmutableMap } from 'immutable';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import type { Map as ImmutableMapType } from 'immutable';
import { UrlLinkTagVariableValues } from 'records/LinkTagsRecords';

export const createLinkTagStart = createAction(LinkTagsActionConstants.CREATE_LINK_TAG_START);
export const createLinkTagCompleted = createAction(LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED);
export const createLinkTagFailed = createAction(LinkTagsActionConstants.CREATE_LINK_TAG_FAILED);

export const updateLinkTagVariable = createAction(LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE);

export const saveLinkTagCompleted = createAction(LinkTagsActionConstants.SAVE_LINK_TAG_COMPLETED);
export const saveLinkTagFailed = createAction(LinkTagsActionConstants.SAVE_LINK_TAG_FAILED);
export const saveLinkTagStart = createAction(LinkTagsActionConstants.SAVE_LINK_TAG_START);

export const updateLinkTagVariableValues = createAction(LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE_VALUES);

const getErrorFromResponse = response => {
  return _.get(response, 'response.body.errors[0].code');
};

function getLinkTagByLinkAndNetwork(state, link, network) {
  return linkTagsUrls(state).getIn([link, network]);
}

function getTagVariableByType(linkTag, type) {
  return linkTag.get('tagVariables').find(tagVariable => tagVariable.get('type') === type);
}

function walkLinkTags(linkTagsUrls, cb) {
  linkTagsUrls.forEach((linkTagPerNetwork, link) => {
    linkTagPerNetwork.forEach((linkTag, network) => cb(link, network, linkTag));
  });
}

type UpdateTagVariableValueProps = {
  link: string,
  network: string
};

function updateSingleValueTagVariableValue({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
    if (linkTag) {
      const singleValueTagVariable = getTagVariableByType(linkTag, LinkTags.SINGLE_VALUE_TAG_VARIABLE_TYPE);
      if (singleValueTagVariable) {
        const tagVariableId = singleValueTagVariable.get('id');
        const tagVariableValue = [{
          value: singleValueTagVariable.getIn(['variableDescription', 'value'])
        }];
        dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
      }
    }
  };
}

function updateSfTrackingIdTagVariableValue({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
    if (linkTag) {
      const sfTrackingIdTagVariable = getTagVariableByType(linkTag, LinkTags.SPREDFAST_TRACKING_ID_TAG_VARIABLE_TYPE);
      if (sfTrackingIdTagVariable) {
        const tagVariableId = sfTrackingIdTagVariable.get('id');
        const tagVariableValue = [{value: LinkTags.SF_TRACKING_ID_TOKEN}];
        dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
      }
    }
  };
}

function updateDatetimeTagVariableValue({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
    if (linkTag) {
      const datetimeTagVariable = getTagVariableByType(linkTag, LinkTags.DATETIME_TAG_VARIABLE_TYPE);
      if (datetimeTagVariable) {
        const tagVariableId = datetimeTagVariable.get('id');
        const tagVariableValue = [{value: LinkTags.DATETIME_TOKEN}];
        dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
      }
    }
  };
}

function updateChannelTagVariableValue({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    if (network !== DefaultNetwork) {
      const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
      if (linkTag) {
        const channelTagVariable = getTagVariableByType(linkTag, LinkTags.CHANNEL_TAG_VARIABLE_TYPE);
        if (channelTagVariable) {
          const tagVariableId = channelTagVariable.get('id');
          const channelValue = channelTagVariable.getIn(['variableDescription', network]) || network;
          const tagVariableValue = [{value: channelValue}];
          dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
        }
      }
    }
  };
}

function updateSocialAccountTagVariable({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
    if (linkTag) {
      const accountTagVariable = getTagVariableByType(linkTag, LinkTags.ACCOUNT_TAG_VARIABLE_TYPE);
      if (accountTagVariable) {
        const tagVariableId = accountTagVariable.get('id');
        const accounts = checkedCredentialsSelector(getState());
        const tagVariableValue = accounts
          .filter(account => account.socialNetwork === network)
          .map(account => ({
            accountUniqueId: account.uniqueId,
            value: accountTagVariable.getIn(['variableDescription', account.uniqueId], account.name)
          })).toList();

        if (!tagVariableValue.isEmpty()) {
          dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
        }
      }
    }
  };
}

function updateLabelsTagVariable({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
    if (linkTag) {
      const labelsTagVariable = getTagVariableByType(linkTag, LinkTags.LABELS_TAG_VARIABLE_TYPE);
      if (labelsTagVariable) {
        const tagVariableId = labelsTagVariable.get('id');
        const labels = getSelectedLabels(getState());
        const tagVariableValue = labels.map(label => ImmutableMap({value: label.get('title')}));
        dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
      }
    }
  };
}

function updateTitleTagVariable({link, network} : UpdateTagVariableValueProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTag = getLinkTagByLinkAndNetwork(getState(), link, network);
    if (linkTag) {
      const titleTagVariable = getTagVariableByType(linkTag, LinkTags.TITLE_TAG_VARIABLE_TYPE);
      if (titleTagVariable) {
        const tagVariableId = titleTagVariable.get('id');
        const title = getMessageTitleByNetwork(getState()).get(network);
        const tagVariableValue = ImmutableList([ImmutableMap({value: title})]);
        dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
      }
    }
  };
}

export function updateSocialAccountTagVariables() {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    if (linkTagsEnable(state)) {
      walkLinkTags(linkTagsUrls(getState()), (link, network, linkTag) => {
        dispatch(updateSocialAccountTagVariable({link, network}));
      });
      dispatch(saveLinkTags());
    }
  };
}

export function updateLabelsTagVariables() {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    if (linkTagsEnable(state)) {
      walkLinkTags(linkTagsUrls(getState()), (link, network, linkTag) => {
        dispatch(updateLabelsTagVariable({link, network}));
      });
      dispatch(saveLinkTags());
    }
  };
}

export function updateTitleTagVariables() {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    if (linkTagsEnable(state)) {
      walkLinkTags(linkTagsUrls(getState()), (link, network, linkTag) => {
        dispatch(updateTitleTagVariable({link, network}));
      });
      dispatch(saveLinkTags());
    }
  };
}

type CopyLinkTagValuesProps = {
  link: string,
  network: string
};

function copyLinkTagVariableValuesFromExistingLinkTag({link, network} : CopyLinkTagValuesProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const createdLinkTagsUrls = linkTagsUrls(getState());
    const linkTagsPerNetwork = createdLinkTagsUrls.get(link);
    if (linkTagsPerNetwork) {
      const linkTags = linkTagsPerNetwork.find((linkTag, linkTagNetwork) => linkTagNetwork !== network);
      if (linkTags) {
        const newValues = linkTags.get('variableValues');
        dispatch(updateLinkTagVariableValues({link, network, newValues}));
      }
    }
  };
};

type InitializeLinkTagValuesProps = {
  link: string,
  network: string
};

function initializeLinkTagVariableValues({link, network} : InitializeLinkTagValuesProps) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    dispatch(copyLinkTagVariableValuesFromExistingLinkTag({link, network}));
    dispatch(updateChannelTagVariableValue({link, network}));
    dispatch(updateSfTrackingIdTagVariableValue({link, network}));
    dispatch(updateDatetimeTagVariableValue({link, network}));
    dispatch(updateSocialAccountTagVariable({link, network}));
    dispatch(updateLabelsTagVariable({link, network}));
    dispatch(updateTitleTagVariables({link, network}));
    dispatch(updateSingleValueTagVariableValue({link, network}));
    dispatch(saveLinkTags());
  };
};

export function createLinkTag(link: string, network: string) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    dispatch(createLinkTagStart({link, network}));
    return LinkTagsApi.createLinkTag(link)
      .then(response => {
        dispatch(createLinkTagCompleted({linkTag: response, link, network}));
        dispatch(initializeLinkTagVariableValues({link, network}));
      })
      .catch(response => dispatch(createLinkTagFailed({error: getErrorFromResponse(response), link, network})));
  };
};

export function createLinkTagsFromUrls(urlsByNetworks: ImmutableMapType<string, ImmutableList<string>>) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    if (linkTagsEnable(state)) {
      const createdLinkTagsUrls = linkTagsUrls(state);
      urlsByNetworks.forEach((links, network) =>
        links
          .filter(link => !createdLinkTagsUrls.getIn([link, network]))
          .forEach(link => dispatch(createLinkTag(link, network)))
      );
    }
  };
};

export function saveLinkTags() {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const linkTagsUrlList = linkTagsReadyToBeSaved(getState());

    linkTagsUrlList.forEach(linkTagsToSave => {
      const link = linkTagsToSave.get('link');
      const network = linkTagsToSave.get('network');
      const linkTagVariableValues = new UrlLinkTagVariableValues({
        url: linkTagsToSave.getIn(['linkTag', 'url']),
        variableValues: linkTagsToSave.getIn(['linkTag', 'variableValues'])
      });

      dispatch(saveLinkTagStart({link, network}));

      LinkTagsApi.saveLinkTag(linkTagVariableValues, linkTagsToSave.getIn(['linkTag', 'id']))
        .then(() => dispatch(saveLinkTagCompleted({link, network})))
        .catch(response => dispatch(saveLinkTagFailed({error: getErrorFromResponse(response), link, network})));
    });
  };
};

export function updateLinkTagVariableForAllNetworks(link: string, tagVariableId: number, tagVariableValue: Array<Object>) {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    if (linkTagsEnable(state)) {
      linkTagsUrls(state).get(link).forEach((linkTag, network) => {
        dispatch(updateLinkTagVariable({link, network, tagVariableId, tagVariableValue}));
      });
      dispatch(saveLinkTags());
    }
  };
};
