/* @flow */
import { createAction } from 'redux-actions';
import { LabelActionConstants } from 'constants/ActionConstants';
import { updateLabelsTagVariables } from 'actions/LinkTagsActions';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

import { labelExists } from 'selectors/LabelsSelector';

import * as LabelsApi from 'apis/LabelsApi';

import { getCompanyId, getCampaignId } from 'selectors/RouterSelector';

export const addLabelToMessage  = createAction(LabelActionConstants.ADD_LABEL);
export const removeLabel  = createAction(LabelActionConstants.REMOVE_LABEL);
export const labelsLoaded = createAction(LabelActionConstants.LABELS_LOADED);
export const readContentLabelsCompleted = createAction(LabelActionConstants.LABELS_READ_COMPLETE);
export const addNewLabelToAvailableLabels = createAction(LabelActionConstants.APPEND_NEW_LABEL);
export const updateLabelText = createAction(LabelActionConstants.UPDATE_LABEL_TEXT);

export const removeLabelFromMessage = (labelName:string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    dispatch(removeLabel(labelName));
    dispatch(updateLabelsTagVariables());
  };
};

export const addLabel = (labelName: any) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    const labelExistsSelector = labelExists(labelName);

    const existingLabel = labelExistsSelector(state);

    if (labelName.title) {
      dispatch(addLabelToMessage(labelName));
    } else if (existingLabel === undefined) {
      dispatch(addLabelToMessage({title: labelName}));
    } else {
      dispatch(addLabelToMessage(existingLabel.toJS()));
    }
    dispatch(updateLabelsTagVariables());
  };
};

export const fetchLabels = (dispatch: Dispatch, getState: () => rootState, searchString?: string) => {
  const state = getState();
  return LabelsApi.getLabels(getCompanyId(state), getCampaignId(state), searchString)
    .then(res => {
      dispatch(labelsLoaded(res));
      return res;
    });
};
export const fetchLabelsAction = (searchString?: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return fetchLabels(dispatch, getState, searchString);
  };
};
