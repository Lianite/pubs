/* @flow */
import { createAction } from 'redux-actions';
import { AccountsActionConstants } from 'constants/ActionConstants';
import * as AccountsApi from 'apis/AccountsApi';
import { updateMessageBodyAfterCredentialSelection } from 'actions/MessageActions';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { canTarget as canFacebookTarget, canDarkPost } from 'selectors/FacebookSelector';
import { accountFilterText, hasCurrentAccountChannelFilter, featureFlags } from 'reducers/index';
import {
  fetchFacebookTargetingLanguages,
  clearTargeting as clearFacebookTargeting,
  setDarkPostVisibility,
  clearFacebookData
} from 'actions/FacebookActions';
import { SupportedNetworks, getAllSupportedNetworks } from 'constants/ApplicationConstants';
import { Map as ImmutableMap } from 'immutable';
import {
  buildVoicesCheckedState,
  checkAllVoices,
  uncheckAllVoices } from 'adapters/AccountsAdapter';
import { updateSocialAccountTagVariables } from 'actions/LinkTagsActions';

import type { rootState } from 'reducers/index';
import type { Dispatch } from 'redux';

export const fetchVoicesStarted  = createAction(AccountsActionConstants.FETCH_VOICES_REQUEST_STARTED);
export const fetchVoicesSuccessful  = createAction(AccountsActionConstants.FETCH_VOICES);
export const fetchVoicesFailed  = createAction(AccountsActionConstants.FETCH_VOICES_REQUEST_FAILED);
export const voiceCredentialCheckToggle = createAction(AccountsActionConstants.VOICE_CRED_TOGGLE);
export const quickSelectAllVoices = createAction(AccountsActionConstants.QUICK_CREDENTIAL_SELECT);
export const removeNetworkVoices = createAction(AccountsActionConstants.REMOVE_NETWORK_VOICES);
export const updateFilterText = createAction(AccountsActionConstants.UPDATE_FILTER_TEXT);
export const updateFilterChannels = createAction(AccountsActionConstants.UPDATE_FILTER_CHANNELS);
export const updateExclusiveFilterChannel = createAction(AccountsActionConstants.UPDATE_EXCLUSIVE_FILTER_CHANNEL);
export const updateFilterSelection = createAction(AccountsActionConstants.UPDATE_FILTER_SELECTION);
export const searchOnVoicesToggle = createAction(AccountsActionConstants.SEARCH_ON_VOICES_TOGGLE);

export const fetchVoicesRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());
  const flags = featureFlags(getState());
  dispatch(fetchVoicesStarted());

  return AccountsApi.getVoices(compCampIds.companyId, compCampIds.campaignId, getAllSupportedNetworks(flags))
  .then(res => {
    return dispatch(fetchVoicesSuccessful(res));
  });
};

export const fetchVoices = (searchStringParam?: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return fetchVoicesRequest(dispatch, getState);
  };
};

export const voiceChecked = (voiceId: number, credentialId?: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const credentialsChecked = buildVoicesCheckedState(voiceId, credentialId, getState());
    dispatch(voiceCredentialCheckToggle(credentialsChecked));
    dispatch(updateSocialAccountTagVariables());
    updateMessageBodyAfterCredentialSelection(getState, dispatch);
    dispatch(fetchFacebookTargetingLanguages());
    if (!canFacebookTarget(getState())) {
      dispatch(clearFacebookTargeting());
    }
    if (!canDarkPost(getState())) {
      dispatch(setDarkPostVisibility(false));
    }

  };
};

export const quickSelectAllOrNone = (selectAll: bool) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    let voicesToSelect;
    if (selectAll) {
      voicesToSelect = checkAllVoices(getState());
    } else if (accountFilterText(getState()) && !hasCurrentAccountChannelFilter(getState())) {
      voicesToSelect = ImmutableMap();
    } else {
      voicesToSelect = uncheckAllVoices(getState());
      clearFacebookData(dispatch, getState);
    }
    dispatch(quickSelectAllVoices(voicesToSelect));
    dispatch(updateSocialAccountTagVariables());
    updateMessageBodyAfterCredentialSelection(getState, dispatch);
    dispatch(fetchFacebookTargetingLanguages());
  };
};

export const removeCredentialsForSelectedNetwork = (network: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    dispatch(removeNetworkVoices(network));
    dispatch(updateSocialAccountTagVariables());
    updateMessageBodyAfterCredentialSelection(getState, dispatch);
    dispatch(fetchFacebookTargetingLanguages());
    if (network === SupportedNetworks.FACEBOOK) {
      clearFacebookData(dispatch, getState);
    }
  };
};
