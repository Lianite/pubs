/* @flow */
import { createAction } from 'redux-actions';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { LinksActionConstants } from 'constants/ActionConstants';
import {
  urlsMap as urlsMapSelector,
  urlProperties,
  messageEditorStateMap,
  getCompanyData,
  linkPreviewLinkUrl
  } from 'reducers/index';
import { linkPreviewUrlsListCurrentlyVisible, selectedLinkPreviewUrl } from 'selectors/LinkPreviewSelector';
import {
  List as ImmutableList,
  Set as ImmutableSet
 } from 'immutable';
import {
 identifyUrls,
 getTotalUrlList
} from 'utils/LinkUtils.js';
import { getPlainTextFromEditorState } from 'utils/MessageUtils';
import { syncLinkPreview } from 'actions/LinkPreviewActions';
import { createLinkTagsFromUrls } from 'actions/LinkTagsActions';
import { linkTagsEnable } from 'selectors/LinkTagsSelector';
import _ from 'lodash';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

export const updateFoundUrls = createAction(LinksActionConstants.UPDATE_FOUND_URLS);
export const updateUrlProperties = createAction(LinksActionConstants.UPDATE_URL_PROPERTIES);

export const calculateLinkPreviewUrl = (previousLinkPreviewUrls:ImmutableList<string>, newLinkPreviewUrls: ImmutableList<string>, currentLinkPreviewUrl: string): any => {

  const firstUrlAndNoLinkPreviewUrl = (previousLinkPreviewUrls.size === 0 && newLinkPreviewUrls.size !== 0 && !currentLinkPreviewUrl);
  const urlsSizeUnchanged = (newLinkPreviewUrls.size === previousLinkPreviewUrls.size);
  //We only care about updating the link preview if the current link preview is in the old list
  // otherwise, it means the current link preview was removed from the list some time before,
  // and we shouldn't update
  const currentLinkPreviewUrlInPreviousUrls = previousLinkPreviewUrls.contains(currentLinkPreviewUrl);

  let updatedLinkPreviewUrl = currentLinkPreviewUrl;

  if (firstUrlAndNoLinkPreviewUrl) {
    //just dispatch with the first url
    if (!newLinkPreviewUrls.isEmpty()) {
      updatedLinkPreviewUrl = newLinkPreviewUrls.first();
    }
  } else if (urlsSizeUnchanged && currentLinkPreviewUrlInPreviousUrls) {
    //look at the indeces where the link preview used to live, and also check for
    // differences between the two lists
    let numberOfDifferences = 0;
    previousLinkPreviewUrls.forEach((url:string, index: number) => {
      if (url === currentLinkPreviewUrl) {
        //only attempt to update the current link preview if it's the first difference we encounter
        if (updatedLinkPreviewUrl === currentLinkPreviewUrl) {

          //Check if the url has actually been changed, and make sure it's a change
          // we want to update (i.e. the new or previous urls should be a substring of the other)
          const currNewLinkPreviewUrl = newLinkPreviewUrls.get(index);
          const valuesDifferent = currNewLinkPreviewUrl !== currentLinkPreviewUrl;
          const isSubstring = currentLinkPreviewUrl.indexOf(currNewLinkPreviewUrl) !== -1 || currNewLinkPreviewUrl.indexOf(currentLinkPreviewUrl) !== -1;

          if (valuesDifferent && isSubstring) {
            updatedLinkPreviewUrl = newLinkPreviewUrls.get(index);
          }
        }
      }

      if (url !== newLinkPreviewUrls.get(index)) {
        numberOfDifferences++;
      }
    });

    //If we had multiple differences, we don't want to update the link
    // preview url as this most likely means the user pasted in a bunch of
    // new links
    if (numberOfDifferences !== 1) {
      updatedLinkPreviewUrl = currentLinkPreviewUrl;
    }
  }

  return updatedLinkPreviewUrl;
};

export const updateUrlList = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    const oldUrlProperties = urlProperties(state);
    let newUrlProperties = urlProperties(state);
    const oldUrls = urlsMapSelector(state);
    let newUrls = oldUrls;
    const previousLinkPreviewUrls = linkPreviewUrlsListCurrentlyVisible(state);
    const messageEditors = messageEditorStateMap(state);
    const currentLinkPreviewUrl = selectedLinkPreviewUrl(state);

    newUrlProperties.keySeq().toArray().forEach(url => {
      const urlWithEmptyAssociatedNetworks = newUrlProperties.get(url).set('associatedNetworks', ImmutableSet());
      newUrlProperties = newUrlProperties.set(url, urlWithEmptyAssociatedNetworks);
    });

    messageEditors.keySeq().toArray().forEach((key) => {
      let foundUrls = identifyUrls(getPlainTextFromEditorState(messageEditors.get(key)));
      newUrls = newUrls.set(key, ImmutableList(foundUrls));

      foundUrls.forEach((url) => {
        if (!newUrlProperties.get(url)) {
          newUrlProperties = newUrlProperties.set(url, new UrlPropertiesRecord({
            associatedNetworks: ImmutableSet(),
            shortenOnPublish: getCompanyData(state).companyMetadata.shortenLinksByDefault,
            addLinkTags: linkTagsEnable(state)
          }));
        }
        const newAssociatedNetworks = newUrlProperties.getIn([url, 'associatedNetworks'], ImmutableSet()).add(key);
        newUrlProperties = newUrlProperties.mergeIn([url, 'associatedNetworks'], newAssociatedNetworks);
      });
    });

    let newUrlList = getTotalUrlList(newUrls);
    const linkPreviewUrl = linkPreviewLinkUrl(state);
    if (linkPreviewUrl) {
      newUrlList = newUrlList.concat(linkPreviewUrl);
    }
    newUrlProperties = newUrlProperties.filter((props, url) => (newUrlList.indexOf(url) !== -1));

    //NOTE: Need to find a way to do this that satisfies flow...
    if (!_.isEqual(oldUrlProperties.toJS(), newUrlProperties.toJS())) {
      dispatch(updateUrlProperties(newUrlProperties));
    }

    if (!_.isEqual(oldUrls, newUrls)) {
      dispatch(updateFoundUrls(newUrls));
      dispatch(createLinkTagsFromUrls(newUrls));

      const newLinkPreviewUrls = linkPreviewUrlsListCurrentlyVisible(getState());
      const newLinkPreview = calculateLinkPreviewUrl(previousLinkPreviewUrls, newLinkPreviewUrls, currentLinkPreviewUrl);
      if (newLinkPreview !== currentLinkPreviewUrl) {
        dispatch(syncLinkPreview(newLinkPreview));
      }

    }
  };
};

export const updateSingleUrlProperty = (url: string, updatedProperty: string, value: any) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    const newUrlProperties = urlProperties(state).mergeIn([url, updatedProperty], value);
    return dispatch(updateUrlProperties(newUrlProperties));
  };
};
