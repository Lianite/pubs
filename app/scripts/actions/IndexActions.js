/* @flow */
import type { Dispatch } from 'redux';
import { clearMessage } from 'actions/MessageActions';
import { fetchLabelsAction } from 'actions/LabelActions';

export const resetFullMessage = () => {
  return (dispatch: Dispatch): void  => {
    dispatch(clearMessage());
    dispatch(fetchLabelsAction());
  };
};
