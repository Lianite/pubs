/* @flow */
import { createAction } from 'redux-actions';
import { NotesActionConstants } from 'constants/ActionConstants';
import * as NoteApi from 'apis/NoteApi';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { noteSaveFailedNotification } from 'utils/NotificationUtils';
import { createNoteFromPayload } from 'adapters/NotesAdapter';
import { addNotification } from '@spredfast/react-lib/lib/growler-notification/redux/actions';

import type { rootState } from 'reducers/index';
import type { Dispatch } from 'redux';

export const readNotesCompleted = createAction(NotesActionConstants.READ_NOTES_COMPLETED);

export const saveNoteRequestStarted = createAction(NotesActionConstants.SAVE_NOTE_REQUEST_STARTED);

export const saveNoteRequestCompleted = createAction(NotesActionConstants.SAVE_NOTE_REQUEST_COMPLETED);

export const addNote = createAction(NotesActionConstants.ADD_NOTE);

export const currentNoteTextChanged = createAction(NotesActionConstants.CURRENT_NOTE_TEXT_CHANGED);

export const createNoteForSavedMessage = (note: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const { eventId, companyId, campaignId } = getCompanyCampaignFromRoute(getState());
    dispatch(saveNoteRequestStarted());
    return NoteApi.createNote(companyId, campaignId, eventId, note)
      .then(res => {
        const note = createNoteFromPayload(res);
        dispatch(addNote(note));
        dispatch(saveNoteRequestCompleted());
      })
      .catch(err => {
        dispatch(addNotification(noteSaveFailedNotification()));
        dispatch(saveNoteRequestCompleted());
      });
  };
};
