/* @flow */
import { createAction } from 'redux-actions';
import { FacebookActionConstants } from 'constants/ActionConstants';
import { getLanguageAutocomplete, getLocationAutocomplete } from 'apis/FacebookApi';
import { getCompanyId, getCampaignId } from 'selectors/RouterSelector';
import { credentialUniqueIds as credentialUniqueIdsSelector } from 'selectors/CredentialsSelector';
import { canTarget } from 'selectors/FacebookSelector';
import { FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS } from 'constants/UITextConstants';
import { GENDER_TARGETING_TYPES } from 'constants/ApplicationConstants';
import Promise from 'bluebird';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

export const addFacebookTargetingLanguage = createAction(FacebookActionConstants.ADD_TARGETING_LANGUAGE);

export const removeFacebookTargetingLanguage = createAction(FacebookActionConstants.REMOVE_TARGETING_LANGUAGE);

export const readFacebookCompleted = createAction(FacebookActionConstants.READ_FACEBOOK_COMPLETED);

export const fetchFacebookTargetingLanguagesCompleted = createAction(FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LANGUAGES_COMPLETED);

export const fetchFacebookTargetingLocationsCompleted = createAction(FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LOCATIONS_COMPLETED);

export const addFacebookTargetingIncludedLocation = createAction(FacebookActionConstants.ADD_TARGETING_INCLUDED_LOCATION);

export const removeFacebookTargetingIncludedLocation = createAction(FacebookActionConstants.REMOVE_TARGETING_INCLUDED_LOCATION);

export const removeFacebookTargetingIncludedCountry = createAction(FacebookActionConstants.REMOVE_TARGETING_INCLUDED_COUNTRY);

export const clearTargeting = createAction(FacebookActionConstants.CLEAR_TARGETING);

export const setDarkPostVisibility = createAction(FacebookActionConstants.SET_DARK_POST_VISIBILITY);

export const clearDarkPostVisibility = createAction(FacebookActionConstants.CLEAR_DARK_POST_VISIBILITY);

export const setGenderTargeting = createAction(FacebookActionConstants.SET_GENDER_TARGETING);

export const clearGenderTargeting = createAction(FacebookActionConstants.CLEAR_GENDER_TARGETING);

export const selectGenderTargeting = (genderOption: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    if (genderOption === FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS.All) {
      return dispatch(clearGenderTargeting());
    } else {
      return dispatch(setGenderTargeting(GENDER_TARGETING_TYPES[genderOption]));
    }
  };
};

export const clearFacebookData = (dispatch: Dispatch, getState: () => rootState) => {
  dispatch(clearDarkPostVisibility());
  return dispatch(clearTargeting());
};

export const fetchFacebookTargetingLanguages = (query: string = '') => (dispatch: Dispatch, getState: () => rootState) => {
  const state = getState();
  const canTargetFacebook = canTarget(state);

  if (canTargetFacebook) {
    const credentialUniqueIds = credentialUniqueIdsSelector(state);
    const companyId = getCompanyId(state);
    const campaignId = getCampaignId(state);

    return getLanguageAutocomplete({
      credentialUniqueIds,
      companyId,
      campaignId,
      query
    })
        .then((response) => {
          dispatch(fetchFacebookTargetingLanguagesCompleted(response.data.targetingResults || []));
        });
  }

  return Promise.resolve();
};

export const fetchFacebookTargetingLocations = (query: string) => (dispatch: Dispatch, getState: () => rootState) => {
  const state = getState();
  const canTargetFacebook = canTarget(state);

  if (canTargetFacebook) {
    const credentialUniqueIds = credentialUniqueIdsSelector(state);
    const companyId = getCompanyId(state);
    const campaignId = getCampaignId(state);

    if (query && query.length) {
      return getLocationAutocomplete({
        credentialUniqueIds,
        companyId,
        campaignId,
        query
      })
          .then((response) => {
            dispatch(fetchFacebookTargetingLocationsCompleted(response.data.targetingResults || []));
          })
          .catch((e: Error) => e);
    } else {
      return dispatch(fetchFacebookTargetingLocationsCompleted([]));
    }
  }

  return Promise.resolve();
};
