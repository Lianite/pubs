/* @flow */
import { createAction } from 'redux-actions';
import { getCsrfToken, getUserData, getLastCampaign, getCapabilities, getCompanyData, getCompanyConfiguration } from 'apis/EnvironmentApi';
import { sendAllAssigneesRequest, sendValidAssigneesRequest, createCurrentAssignee, fetchAssigneesVoices } from 'actions/AssigneeActions';
import { currentPathname, currentQueryParams, getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { EnvironmentActionConstants } from 'constants/ActionConstants';
import { PermissionConstants } from 'constants/ApplicationConstants';
import { identify } from '@spredfast/sf-segment-js';
import { sendPlanDataRequest } from 'actions/PlanActions';
import { readMessage } from 'actions/MessageActions';
import { fetchLabels } from 'actions/LabelActions';
import { fetchFacebookTargetingLanguages } from 'actions/FacebookActions';
import { fetchVoicesRequest } from 'actions/AccountsActions';
import { push as pushHistory } from 'redux-router';
import setupSentry from 'services/SentryService';
import Promise from 'bluebird';
import _ from 'lodash';

import type { GetCompanyDataResponse, GetUserDataResponse } from 'apis/EnvironmentApi';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

export const requestCsrfToken = createAction(EnvironmentActionConstants.REQUEST_CSRF_TOKEN);
export const csrfTokenReceived = createAction(EnvironmentActionConstants.CSRF_TOKEN_RECEIVED);
export const csrfTokenError = createAction(EnvironmentActionConstants.CSRF_TOKEN_ERROR);
export const requestUserData = createAction(EnvironmentActionConstants.REQUEST_USER_DATA);
export const userDataReceived = createAction(EnvironmentActionConstants.USER_DATA_RECEIVED);
export const userDataError = createAction(EnvironmentActionConstants.USER_DATA_ERROR);
export const requestCampaignId = createAction(EnvironmentActionConstants.REQUEST_CAMPAIGN_ID);
export const campaignIdReceived = createAction(EnvironmentActionConstants.CAMPAIGN_ID_RECEIVED);
export const campaignIdError = createAction(EnvironmentActionConstants.CAMPAIGN_ID_ERROR);
export const requestCapabilities = createAction(EnvironmentActionConstants.REQUEST_CAPABILITIES);
export const capabilitiesReceived = createAction(EnvironmentActionConstants.CAPABILITIES_RECEIVED);
export const capabilitiesError = createAction(EnvironmentActionConstants.CAPABILITIES_ERROR);
export const clearEnvironment = createAction(EnvironmentActionConstants.CLEAR_ENVIRONMENT);
export const setUnscheduledDraftDatetime = createAction(EnvironmentActionConstants.SET_UNSCHEDULED_DRAFT_DATETIME);
export const requestCompanyData = createAction(EnvironmentActionConstants.REQUEST_COMPANY_DATA);
export const companyReceived = createAction(EnvironmentActionConstants.COMPANY_RECEIVED);
export const companyDataError = createAction(EnvironmentActionConstants.COMPANY_DATA_ERROR);

export const userCanEditPermission = createAction(EnvironmentActionConstants.USER_CAN_EDIT_PERMISSION);
export const multiChannelFeatureFlagReceived = createAction(EnvironmentActionConstants.FEATURES_RECEIVED);

export const setConfiguration = createAction(EnvironmentActionConstants.SET_CONFIGURATION);

const sendCsrfTokenRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());
  dispatch(requestCsrfToken());

  return getCsrfToken(compCampIds.companyId)
    .then(res => {
      if (res.csrfToken) {
        dispatch(csrfTokenReceived(res.csrfToken));
        return res;
      } else {
        throw new Error('csrfToken was not found on the payload for getCsrfToken');
      }
    })
    .catch((error: Error) => {
      dispatch(csrfTokenError(error.message));
      return error;
    });
};
export const sendCsrfTokenRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendCsrfTokenRequest(dispatch, getState);
  };
};

const sendUserDataRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());

  dispatch(requestUserData());

  return getUserData(compCampIds.companyId)
    .then(res => {
      const publishingAccessLevel = _.get(res, ['data', 'role', 'publishingAccessLevel', 'key']);
      const userCanView = (
        publishingAccessLevel !== PermissionConstants.NONE_PUBLISHING_ACCESS_KEY &&
        publishingAccessLevel !== PermissionConstants.VIEW_PUBLISHING_ACCESS_KEY
      );
      // If the user does not have view access, we should turn off edit access right away
      if (!userCanView) {
        dispatch(userCanEditPermission(false));
      }
      if (res.data.id && res.data.timezone) {
        const payload = {
          id: res.data.id,
          timezone: {
            abbreviation: res.data.timezone.abbreviation,
            name: res.data.timezone.name,
            offset: parseInt(res.data.timezone.offset, 10)
          },
          viewAccess: userCanView,
          role: {
            name: res.data.role.name,
            privileges: res.data.role.privileges
          },
          firstName: res.data.firstName,
          lastName: res.data.lastName,
          avatar: res.data.thumb
        };
        dispatch(userDataReceived(payload));
        return res;
      } else {
        throw new Error('The required user data was not found on the payload for getUserData');
      }
    })
    .catch((error: Error) => {
      dispatch(userDataError(error.message));
      return error;
    });
};
export const sendUserDataRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendUserDataRequest(dispatch, getState);
  };
};

const sendCampaignIdRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const currentPath = currentPathname(getState());
  const queryParams = currentQueryParams(getState());
  dispatch(requestCampaignId());

  return getLastCampaign()
    .then(res => {
      if (res.campaignId) {
        dispatch(campaignIdReceived());
        //Update the Route

        dispatch(pushHistory(`${currentPath}/campaign/${res.campaignId}${queryParams}`));
        //If we received a campagin ID, then we also need to request voices.
        return res;
      } else {
        throw new Error('campaignId was not found on the payload for getLastCampaign');
      }
    })
    .catch((error: Error) => {
      dispatch(campaignIdError(error.message));
      return error;
    });
};
export const sendCampaignIdRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendCampaignIdRequest(dispatch, getState);
  };
};

const sendCompanyRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());

  dispatch(requestCompanyData());

  return getCompanyData(compCampIds.companyId)
    .then(res => {
      if (res.data.id) {
        const multiChannelEnabled = _.includes(res.data.enabledFeatures, 'publishing_multichannel');
        const payload = {
          companyMetadata: {
            contentLabelCreationRestricted: res.data.companyMetadata.contentLabelCreationRestricted,
            salesforceEnabled: res.data.companyMetadata.salesforceEnabled,
            shortenLinksByDefault: res.data.companyMetadata.shortenLinksByDefault
          },
          mediaConfigDTO: {
            imageAllowedMimeTypes: res.data.mediaConfigDTO
          },
          name: res.data.name,
          enabledFeatures: res.data.enabledFeatures
        };
        dispatch(multiChannelFeatureFlagReceived(multiChannelEnabled));
        dispatch(companyReceived(payload));
        return res;
      } else {
        throw new Error('The required company data was not found on the payload for getCompanyData');
      }
    })
    .catch((error: Error) => {
      dispatch(companyDataError(error.message));
      return error;
    });
};

export const sendCompanyRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendCompanyRequest(dispatch, getState);
  };
};

const sendCapabilitiesRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());

  dispatch(requestCapabilities());

  return getCapabilities(compCampIds.companyId)
    .then(res => {
      if (res.data) {
        dispatch(capabilitiesReceived(res.data));
        return res;
      } else {
        throw new Error('data was not found on the payload for getCapabilities');
      }
    })
    .catch((error: Error) => {
      dispatch(capabilitiesError(error.message));
      return error;
    });
};
export const sendCapabilitiesRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendCapabilitiesRequest(dispatch, getState);
  };
};

const sendCompanyConfigurationRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());

  return getCompanyConfiguration(compCampIds.companyId)
    .then(res => {
      dispatch(setConfiguration(res));
      return res;
    });
};
export const sendCompanyConfigurationRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendCompanyConfigurationRequest(dispatch, getState);
  };
};

const indentifySegment = (companyRes: GetCompanyDataResponse, userResponse: GetUserDataResponse, campaignId: string) => {
  identify({
    userDirectoryId: userResponse.data.directoryUserId,
    companyDirectoryId: companyRes.data.directoryId,
    companyId: companyRes.data.id,
    companyName: companyRes.data.name,
    email: userResponse.data.email,
    firstName: userResponse.data.firstName,
    lastName: userResponse.data.lastName,
    initiativeId: campaignId
  });
};

export const initializeAppData = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const compCampIds = getCompanyCampaignFromRoute(getState());
    let initPromises = {};

    initPromises.csrfTokenRequest = sendCsrfTokenRequest(dispatch, getState);
    initPromises.userDataRequest = sendUserDataRequest(dispatch, getState);
    initPromises.companyRequest = sendCompanyRequest(dispatch, getState);
    initPromises.capabilitiesRequest = sendCapabilitiesRequest(dispatch, getState);
    initPromises.planDataRequest = sendPlanDataRequest(dispatch, getState);
    initPromises.companyConfigurationRequest = sendCompanyConfigurationRequest(dispatch, getState);
    if (!compCampIds.campaignId) {
      initPromises.campaignIdRequest = sendCampaignIdRequest(dispatch, getState);
    }

    /**
     * Upon completion of fetching a initiative id, fetch labels and accounts for the initiative
     */
    if (initPromises.campaignIdRequest) {
      initPromises.campaignIdRequest
        .then(() => {
          initPromises.allAssigneesRequest = sendAllAssigneesRequest(dispatch, getState);
          initPromises.fetchedLabels = fetchLabels(dispatch, getState);
          initPromises.fetchFacebookTargetingLanguages = dispatch(fetchFacebookTargetingLanguages());
          initPromises.fetchVoices = fetchVoicesRequest(dispatch, getState);
          initPromises.validAssigneesRequest = sendValidAssigneesRequest(dispatch, getState);
          initPromises.validAssigneesRequest
            .then(() => {
              initPromises.fetchAssigneesAccounts = dispatch(fetchAssigneesVoices());
            });
        });
    } else {
      initPromises.allAssigneesRequest = sendAllAssigneesRequest(dispatch, getState);
      initPromises.fetchedLabels = fetchLabels(dispatch, getState);
      initPromises.fetchVoices = fetchVoicesRequest(dispatch, getState);
      initPromises.validAssigneesRequest = sendValidAssigneesRequest(dispatch, getState);
      initPromises.validAssigneesRequest
        .then(() => {
          initPromises.fetchAssigneesAccounts = dispatch(fetchAssigneesVoices());
        });
    }

    /**
     * Upon completion of fetching the user data, read the message
     */
    initPromises.userDataRequest
      .then(() => {
        initPromises.message = readMessage(dispatch, getState);

        Promise.all([initPromises.message, initPromises.campaignIdRequest])
          .then(() => {
            initPromises.fetchFacebookTargetingLanguages = dispatch(fetchFacebookTargetingLanguages());
          });
      });

    Promise.all([initPromises.companyRequest, initPromises.userDataRequest])
    .then(res => {
      const companyRes = res[0];
      const userResponse = res[1];
      if (!compCampIds.campaignId) {
        initPromises.campaignIdRequest
          .then((campaignRes) => {
            if (campaignRes.campaignId) {
              indentifySegment(companyRes, userResponse, campaignRes.campaignId);
            }
          }
        );
      } else {
        indentifySegment(companyRes, userResponse, compCampIds.campaignId);
      }
      setupSentry({
        userId: userResponse.data.directoryUserId,
        groupId: companyRes.data.directoryId,
        email: userResponse.data.email,
        firstName: userResponse.data.firstName,
        lastName: userResponse.data.lastName
      });
      dispatch(createCurrentAssignee(getState));
    });
  };
};
