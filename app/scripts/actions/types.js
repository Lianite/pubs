/* @flow */
export type NoArgAction = { (): Function };
export type Action<T> = { payload: T };
