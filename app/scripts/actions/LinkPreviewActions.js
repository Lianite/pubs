/* @flow */
import { createAction } from 'redux-actions';
import { LinkPreviewActionConstants } from 'constants/ActionConstants';
import { CarouselDirections } from 'constants/UIStateConstants';
import {
  linkPreviewInfo,
  linkPreviewWasRemoved,
  getPossibleLinkPreviewThumbnails
} from 'selectors/LinkPreviewSelector';
import {
  getCompanyId,
  getCampaignId
} from 'selectors/RouterSelector';
import _ from 'lodash';
import { getLinkPreview } from 'apis/MessageApi';
import { OrderedSet as ImmutableOrderedSet } from 'immutable';
import { convertToMediaRecord } from 'adapters/LinkPreviewAdapter';
import { extractUrlFromSourceMedia } from 'utils/LinkUtils';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

export const addLinkPreview = createAction(LinkPreviewActionConstants.ADD_LINK_PREVIEW);
export const linkScrapeSuccessful = createAction(LinkPreviewActionConstants.LINK_SCRAPE_SUCCESSFUL);
export const updateLinkPreview = createAction(LinkPreviewActionConstants.UPDATE_LINK_PREVIEW);
export const updateLinkPreviewImages = createAction(LinkPreviewActionConstants.UPDATE_LINK_PREVIEW_IMAGES);
export const removeLinkPreview = createAction(LinkPreviewActionConstants.REMOVE_LINK_PREVIEW);
export const updateImageThumbnail = createAction(LinkPreviewActionConstants.UPDATE_IMAGE_THUMBNAIL);
export const editLinkPreview = createAction(LinkPreviewActionConstants.EDIT_LINK_PREVIEW);
export const scrapingLinkPreview = createAction(LinkPreviewActionConstants.SCRAPING_LINK_PREVIEW);
export const requestUploadCustomThumb = createAction(LinkPreviewActionConstants.REQUEST_UPLOAD_CUSTOM_THUMB);
export const uploadCustomThumbSucceeded = createAction(LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_SUCCEEDED);
export const uploadCustomThumbFailed = createAction(LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_FAILED);
export const uploadCustomThumb = createAction(LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB);
export const cancelLinkPreviewCustomUpload = createAction(LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_CANCELED);
export const cancelLinkPreviewScrape = createAction(LinkPreviewActionConstants.CANCEL_LINK_SCRAPE);
export const readLinkPreviewCompleted = createAction(LinkPreviewActionConstants.READ_LINK_PREVIEW_COMPLETED);
export const clearLinkPreviewCustomUploadError = createAction(LinkPreviewActionConstants.CLEAR_ERROR);

export const rescrapeLinkPreviewOnMessageLoad = (linkPreviewData: Object) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    return getLinkPreview(getCompanyId(state), getCampaignId(state), linkPreviewData.linkUrl).then(res => {

      let customUploadedMedia = linkPreviewData.thumbnailUrl;
      if (res.sourceMedia && _.some(res.sourceMedia, (sourceMediaUrl: string) => {
        return sourceMediaUrl === customUploadedMedia;
      })) {
        customUploadedMedia = '';
      }

      const linkPreviewPayloadWithNewScrape = {
        sourceMedia: ImmutableOrderedSet(convertToMediaRecord(extractUrlFromSourceMedia(res.sourceMedia))),
        customUploadedMedia: customUploadedMedia ? ImmutableOrderedSet(convertToMediaRecord([customUploadedMedia])) : ImmutableOrderedSet()
      };

      return dispatch(updateLinkPreviewImages(linkPreviewPayloadWithNewScrape));
    }).catch(error => {
      return dispatch(updateLinkPreviewImages({}));
    });
  };
};

export const cycleLinkPreviewImageCarousel = (direction:string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const carouselList = getPossibleLinkPreviewThumbnails(getState());
    const currentImage = _.get(linkPreviewInfo(getState()), 'thumbnailUrl', '');

    let currentIndex = carouselList.indexOf(currentImage);
    if (direction === CarouselDirections.FORWARD) {
      currentIndex++;
    } else {
      currentIndex--;
    }

    let newValidIndex = currentIndex;

    if (currentIndex < 0) {
      newValidIndex = currentIndex + carouselList.length;
    } else if (currentIndex >= carouselList.length) {
      newValidIndex = currentIndex - carouselList.length;
    }

    return dispatch(updateImageThumbnail(carouselList[newValidIndex]));
  };
};

export const cycleLinkPreviewImageCarouselForward = () => {
  return (dispatch: Dispatch) => {
    return dispatch(cycleLinkPreviewImageCarousel(CarouselDirections.FORWARD));
  };
};

export const cycleLinkPreviewImageCarouselBackward = () => {
  return (dispatch: Dispatch) => {
    return dispatch(cycleLinkPreviewImageCarousel(CarouselDirections.BACKWARD));
  };
};

export const syncLinkPreview = (newLinkPreview: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    if (!linkPreviewWasRemoved(state)) {
      dispatch(cancelLinkPreviewCustomUpload());
      dispatch(addLinkPreview(newLinkPreview));
    }
  };
};
