/* @flow */
import React from 'react';
import { createAction } from 'redux-actions';
import { Set as ImmutableSet, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import * as MessageApi from 'apis/MessageApi';
import { readImagesCompleted, readVideosCompleted, reprocessVideo } from 'actions/MediaActions';
import { rescrapeLinkPreviewOnMessageLoad, readLinkPreviewCompleted } from 'actions/LinkPreviewActions';
import { readNotesCompleted } from 'actions/NotesActions';
import { readFacebookCompleted } from 'actions/FacebookActions';
import { updateFoundUrls, updateUrlProperties } from 'actions/LinksActions';
import { userCanEditPermission } from 'actions/EnvironmentActions';
import { updateCurrentAssignee, createCurrentAssignee } from 'actions/AssigneeActions';
import { voiceCredentialCheckToggle } from 'actions/AccountsActions';
import { updateCurrentPlan } from 'actions/PlanActions';
import { createMessageObject, createMessageStateFromPayload } from 'adapters/MessageAdapter';
import { getCompanyCampaignFromRoute } from 'selectors/RouterSelector';
import { messageEditorStateMap,
  messageCurrentlySavingSelector,
  getUserTimezone,
  scheduledDateTimeSelector,
  selectedNetworks,
  urlsMap,
  urlProperties,
  getPlans,
  videoUploadStatus,
  firstVideo,
  messageTitleSelector,
  linkPreviewLinkUrl } from 'reducers/index';
import { selectedNetworksFromVoices } from 'selectors/AccountsSelectors';
import { readContentLabelsCompleted } from 'actions/LabelActions';
import { videoUploadNetworks } from 'selectors/AttachedMediaSelector';
import {
  messageHasEventId,
  messageHasCopyId,
  getEventId,
  getCopyId} from 'selectors/RouterSelector';
import {getCompositeDecorator} from 'decorators/EditorDecorator';
import { EditorState, ContentState } from 'draft-js';
import { buildRegexpFromArray,
  getTotalUrlList,
  isPositionInsideAnUrl } from 'utils/LinkUtils.js';
import { closeEmbedded, intentNotification } from 'utils/EmbeddedUtils';
import { DECORATING_DEBOUNCE } from 'constants/UILayoutConstants';
import _ from 'lodash';
import { MessageActionConstants } from 'constants/ActionConstants';
import { DefaultNetwork, MessageConstants, RequestStatus, MAX_MESSAGE_TITLE_CHARS } from 'constants/ApplicationConstants';
import { addNotification, removeNotification } from '@spredfast/react-lib/lib/growler-notification/redux/actions';
import * as notificationFormatter from 'utils/NotificationUtils';
import { _updateMentions, updateMentions } from 'actions/MentionsActions';
import { setUnscheduledDraftDatetime } from 'actions/EnvironmentActions';
import { updateUrlList } from 'actions/LinksActions';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { createCurrentPlanFromPlanId } from 'adapters/PlanAdapter';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import { resetFullMessage } from 'actions/IndexActions';
import {
  createLinkTagsFromUrls,
  updateTitleTagVariables } from 'actions/LinkTagsActions';
import { trackAction } from '@spredfast/sf-segment-js';
import { segmentCreateMessagePropertiesSelector, segmentCreateMessageEventSelector } from 'selectors/SegmentSelectors';

export const updateMessageTitle = createAction(MessageActionConstants.UPDATE_MESSAGE_TITLE);
export const updateMessageBody = createAction(MessageActionConstants.UPDATE_MESSAGE_BODY);
export const applyToAllChannels = createAction(MessageActionConstants.APPLY_TO_ALL_CHANNELS);
export const undoApplyToAllToAlert = createAction(MessageActionConstants.UNDO_APPLY_ALL);

export const requestSaveMessage = createAction(MessageActionConstants.REQUEST_SAVE_MESSAGE);
export const setScheduledTime = createAction(MessageActionConstants.SET_SCHEDULED_TIME);
export const saveMessageRequestFailed = createAction(MessageActionConstants.SAVE_MESSAGE_REQUEST_FAILED);
export const saveMessageRequestPending = createAction(MessageActionConstants.SAVE_MESSAGE_REQUEST_PENDING);
export const saveMessageRequestSucceeded = createAction(MessageActionConstants.SAVE_MESSAGE_REQUEST_SUCCEEDED);
export const readMessageStarted = createAction(MessageActionConstants.READ_MESSAGE_STARTED);
export const readMessageCompleted = createAction(MessageActionConstants.READ_MESSAGE_COMPLETED);
export const readMessageFailed = createAction(MessageActionConstants.READ_MESSAGE_FAILED);
export const clearMessage = createAction(MessageActionConstants.CLEAR_MESSAGE);

export const messageTitleChanged = (newTitle: string) => {
  return (dispatch: Dispatch) => {
    dispatch(updateMessageTitle(newTitle.substring(0, MAX_MESSAGE_TITLE_CHARS)));
    dispatch(updateTitleTagVariables());
  };
};

export const undoApplyToAllToAlertAndRemoveNotification = (notifId: string) => {
  return (dispatch: Dispatch) => {
    dispatch(undoApplyToAllToAlert(notifId));
    return dispatch(removeNotification(notifId));
  };
};

export function undoApplyToAllAlert(networkName: string, dispatch: Dispatch): Object {
  return (
    {
      sfType: 'neutral',
      timed: true,
      timeOverride: 6000,
      id: networkName,
      children: <div>
        <div style={{color: '#00b9f2', fontSize: '16px'}}>Apply to All</div>
        <div style={{'marginTop': '8px'}}>The {notificationFormatter.toTitleCase(networkName)} message has been applied to all channels.</div>
        <div style={{'marginTop': '8px', 'cursor': 'pointer'}}><a onClick={ () => dispatch(undoApplyToAllToAlertAndRemoveNotification(networkName))}>Undo</a></div>
      </div>
    }
  );
};

export const applyToAllChannelsWithNotification = (payload: Object) => {
  return (dispatch: Dispatch) => {
    dispatch(applyToAllChannels(payload));
    return dispatch(addNotification(undoApplyToAllAlert(payload.network, dispatch, undoApplyToAllAlert)));
  };
};

type EditorStateAndUrl = {
  messageEditorStateMap: ImmutableMap<string, EditorState>,
  urls: ImmutableMap<string, ImmutableList<string>>,
  urlProperties: ImmutableMap<string, UrlPropertiesRecord>
};

export const buildMessageEditorStateMapAndUrls = (state: rootState): EditorStateAndUrl => {
  // Add in the editor states for the selected networks
  let networkList = selectedNetworksFromVoices(state);

  const editorStateMap: ImmutableMap<string, EditorState> = messageEditorStateMap(state);
  const urlMap: ImmutableMap<string, ImmutableList<string>> = urlsMap(state);

  const totalUrlList = getTotalUrlList(urlMap);

  const regexForLinks = buildRegexpFromArray(ImmutableSet(totalUrlList).toArray());

  let newEditorStateMap: ImmutableMap<string, EditorState> = new ImmutableMap();
  let newUrlMap: ImmutableMap<string, ImmutableList<string>> = new ImmutableMap();
  let newUrlProperties: ImmutableMap<string, UrlPropertiesRecord> = urlProperties(state);

  //clear all the networks from the url properties
  newUrlProperties = newUrlProperties.map( (urlProperties:UrlPropertiesRecord, key: string): UrlPropertiesRecord => {
    return urlProperties.set('associatedNetworks', ImmutableSet());
  });

  if (networkList.length === 0) {
    const firstKey: string = editorStateMap.keySeq().toArray()[0];
    const stateToCopyFrom: EditorState = editorStateMap.get(firstKey);
    const urlsToCopyFrom: ImmutableList<string> = urlMap.get(firstKey);
    const editorContent: ContentState = stateToCopyFrom.getCurrentContent();

    newEditorStateMap = newEditorStateMap.set(DefaultNetwork, EditorState.set(
      EditorState.createWithContent(editorContent),
      {
        decorator: getCompositeDecorator(regexForLinks, editorContent, DefaultNetwork)
      }
    ));

    newUrlMap = newUrlMap.set(DefaultNetwork, urlsToCopyFrom);
    urlsToCopyFrom.forEach((url:string):void => {
      const existingNetworks = newUrlProperties.get(url).associatedNetworks;
      newUrlProperties = newUrlProperties.setIn([url, 'associatedNetworks'], existingNetworks.add(DefaultNetwork));
    });
  } else {
    // Check to see if the EditorState already exists, if not, add it & copy content
    _.forEach(networkList, (network: string) => {
      if (editorStateMap.get(network)) {
        newEditorStateMap = newEditorStateMap.set(network, editorStateMap.get(network));
        if (urlMap.get(network)) {
          newUrlMap = newUrlMap.set(network, urlMap.get(network));
          urlMap.get(network).forEach((url:string):void => {
            const existingNetworks = newUrlProperties.get(url).associatedNetworks;
            newUrlProperties = newUrlProperties.setIn([url, 'associatedNetworks'], existingNetworks.add(network));
          });
        } else {
          newUrlMap = newUrlMap.set(network, ImmutableList());
        }
      } else {
        // Grab the first box to copy from
        const firstKey: string = editorStateMap.keySeq().toArray()[0];
        const stateToCopyFrom: EditorState = editorStateMap.get(firstKey);
        const urlsToCopyFrom: EditorState = urlMap.get(firstKey);
        const editorContent: ContentState = stateToCopyFrom.getCurrentContent();

        const newContent: ContentState = ContentState.createFromText(editorContent.getPlainText());

        newEditorStateMap = newEditorStateMap.set(network, EditorState.set(
          EditorState.createWithContent(newContent),
          {
            decorator: getCompositeDecorator(regexForLinks, newContent, network)
          }
        ));

        if (urlsToCopyFrom) {
          newUrlMap = newUrlMap.set(network, urlsToCopyFrom);
          urlsToCopyFrom.forEach((url:string):void => {
            const existingNetworks = newUrlProperties.get(url).associatedNetworks;
            newUrlProperties = newUrlProperties.setIn([url, 'associatedNetworks'], existingNetworks.add(network));
          });
        } else {
          newUrlMap = newUrlMap.set(network, ImmutableList());
        }
      }
    });
  }

  //now remove any urls from the properties map that aren't around any more
  let updatedTotalUrlList = getTotalUrlList(newUrlMap);
  const linkPreview = linkPreviewLinkUrl(state);
  if (linkPreview) {
    updatedTotalUrlList = updatedTotalUrlList.concat(linkPreview);
  }

  newUrlProperties = newUrlProperties.filter( (urlProperty: UrlPropertiesRecord, url: string): bool =>{
    return updatedTotalUrlList.indexOf(url) !== -1;
  });
  return {
    messageEditorStateMap: newEditorStateMap,
    urls: newUrlMap,
    urlProperties: newUrlProperties
  };

};

const shouldReprocessVideo = (state:rootState):bool => {
  return (videoUploadStatus(state) !== RequestStatus.STATUS_NEVER_REQUESTED && videoUploadStatus(state) !== RequestStatus.STATUS_CANCELED);
};

const getNewVideoNetworks = (previousVideoNetworks: Array<string>, currentVideoNetworks: Array<string>): Array<string> => {
  return _.difference(currentVideoNetworks, previousVideoNetworks);
};

export const updateMessageBodyAfterCredentialSelection = (getState: () => rootState, dispatch: Dispatch): void => {
  const videoNetworksBeforeUpdate = videoUploadNetworks(getState());

  const updates = buildMessageEditorStateMapAndUrls(getState());
  dispatch(updateMessageBody(updates.messageEditorStateMap));
  dispatch(updateFoundUrls(updates.urls));
  dispatch(updateUrlProperties(updates.urlProperties));
  dispatch(createLinkTagsFromUrls(updates.urls));

  //only reprocess if we have a video uploaded
  if (shouldReprocessVideo(getState())) {
    const newNetworks = getNewVideoNetworks(videoNetworksBeforeUpdate, videoUploadNetworks(getState()));
    if (newNetworks.length > 0) {
      dispatch(reprocessVideo(newNetworks));
    }
  }
};

export const notifyMessageCompletion = (state: rootState, type: string, succeeded: bool) => {
  let adaptedType = 'PUBLISHED';
  let message = 'Your message has been ';

  switch (type) {
  case MessageConstants.DRAFT:
    adaptedType = MessageConstants.DRAFT;
    message += 'saved as a draft.';
    break;
  case MessageConstants.PENDING:
    let schedule = scheduledDateTimeSelector(state);

    if (schedule) {
      adaptedType = 'SCHEDULE';
      message += 'scheduled.';
    } else {
      message += 'published.';
    }
    break;
  }

  return (dispatch: Dispatch) => {
    let embdedNotified = false;
    if (succeeded) {
      embdedNotified = intentNotification('saved', message, adaptedType);
    }

    if (!embdedNotified) {
      return dispatch(addNotification(notificationFormatter.buildMessageCompletedNotification(message, adaptedType, succeeded)));
    }
  };
};

export const saveMessage = (messageStatus: string) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    if (messageCurrentlySavingSelector(getState())) {
      return dispatch(saveMessageRequestPending());
    }

    const compCampIds = getCompanyCampaignFromRoute(getState());
    const messageBody = createMessageObject(getState(), messageStatus);

    dispatch(requestSaveMessage(messageStatus));

    let messageApiAction;
    if (messageHasEventId(getState())) {
      messageApiAction = MessageApi.editMessage(compCampIds.companyId, compCampIds.campaignId, compCampIds.eventId, messageBody);
    } else {
      messageApiAction = MessageApi.createMessage(compCampIds.companyId, compCampIds.campaignId, messageBody, messageHasCopyId(getState()));
    }

    return messageApiAction
      .then(res => {
        const createMessageSegmentEvent = segmentCreateMessageEventSelector(getState());
        const createMessageSegmentData = segmentCreateMessagePropertiesSelector(getState());

        dispatch(resetFullMessage());
        dispatch(saveMessageRequestSucceeded());
        dispatch(notifyMessageCompletion(getState(), messageStatus, true));

        trackAction(createMessageSegmentEvent, createMessageSegmentData, () => {
          closeEmbedded();
        });

        return res;
      })
      .catch((err) => {
        dispatch(addNotification(notificationFormatter.messageSaveFailedNotification()));
        dispatch(saveMessageRequestFailed());
        return err;
      });
  };
};

export const readMessage = (dispatch: Dispatch, getState: () => rootState) => {
  const routerParams = getCompanyCampaignFromRoute(getState());
  const state = getState();
  //only read a message if we have an event id or copy id
  const eventId = getEventId(state);
  const copyId = getCopyId(state);

  const idToLoad = eventId !== -1 ? eventId : copyId;

  if (idToLoad !== -1) {
    dispatch(readMessageStarted());
    return MessageApi.readMessage(routerParams.companyId,
      routerParams.campaignId,
      idToLoad,
      copyId !== -1)
    .then(res => {
      const userTimezoneOffset = getUserTimezone(state).offset;
      const messagePayload = createMessageStateFromPayload(res, userTimezoneOffset);

      if (_.get(res, ['schedule', 'immediatePublish'])) {
        dispatch(setUnscheduledDraftDatetime(_.get(res, ['schedule', 'scheduledPublishDate'])));
      }

      dispatch(readMessageCompleted(messagePayload.message));
      dispatch(updateFoundUrls(messagePayload.urls));
      dispatch(updateUrlProperties(messagePayload.urlProperties));

      if (messagePayload.images.length) {
        dispatch(readImagesCompleted(messagePayload.images));
      }

      if ( _.get(messagePayload, 'videos.socialVideos', []).size > 0) {
        dispatch(readVideosCompleted(messagePayload.videos));
        //if we don't have any thumbnail candidates, we'll need to kick off a copy
        const thumbnailCandidates = firstVideo(getState()).thumbnails;
        if (thumbnailCandidates.size === 0) {
          dispatch(reprocessVideo(selectedNetworks(getState()))); //getState here because we need the updated values after the message has been persisted to our store
        }
      }

      if (_.get(messagePayload, 'linkPreview.linkUrl', '')) {
        dispatch(readLinkPreviewCompleted(messagePayload.linkPreview));
        dispatch(rescrapeLinkPreviewOnMessageLoad(messagePayload.linkPreview));
      }

      if (_.get(messagePayload, 'notes.notesList')) {
        dispatch(readNotesCompleted(messagePayload.notes));
      }

      if (_.get(messagePayload, 'facebook')) {
        dispatch(readFacebookCompleted(messagePayload.facebook));
      }

      if (messagePayload.contentLabels.length) {
        dispatch(readContentLabelsCompleted(messagePayload.contentLabels));
      }

      let currentPlan = createCurrentPlanFromPlanId(messagePayload.currentPlanId,  getPlans(getState()));
      dispatch(updateCurrentPlan(currentPlan));

      if (_.get(messagePayload, ['message', 'mentions'])) {
        dispatch(_updateMentions(messagePayload.message.mentions));
      }

      const editAccess = _.get(res, ['userAuthorizedForEdit'], false);

      if (_.get(messagePayload, 'assignee')) {
        dispatch(updateCurrentAssignee(messagePayload.assignee));
      } else {
        dispatch(createCurrentAssignee(dispatch, getState));
      }

      dispatch(userCanEditPermission(editAccess));
      dispatch(voiceCredentialCheckToggle(messagePayload.checkedCredentials));

      return res;

    }).catch((err) => {
      dispatch(readMessageFailed(err.message));
    });
  }
};
export const readMessageAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return readMessage(dispatch, getState);
  };
};

// This doesn't care about existing URLs, and only goes based on what is in the editor
export const parseUrlsAndDecorateEditor = (messageUpdate:{editorState:any, network:string, getCompositeDecorator: Function}) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const network = messageUpdate.network;
    const editorState = messageUpdate.editorState;
    //const decorators = messageUpdate.decorators;

    const urlMap: ImmutableMap<string, ImmutableList<string>> = urlsMap(getState());
    const totalUrlList = getTotalUrlList(urlMap);
    const regexForLinks = buildRegexpFromArray(ImmutableSet(totalUrlList).toArray());

    const newMessageEditorStateMap = messageEditorStateMap(getState()).set(
      network,
      EditorState.set(
        editorState,
        {
          decorator: getCompositeDecorator(regexForLinks, editorState.getCurrentContent(), network, editorState.getSelection())
        }
      )
    );

    return dispatch(updateMessageBody(newMessageEditorStateMap));
  };
};

// This needs to be done on debounce in order to prevent changes to the EditorState's CurrentContent
//  during an onChange event as that causes a great deal more problems.
const debounceDecorating = _.debounce((dispatch, messageUpdate) => {
  return dispatch(parseUrlsAndDecorateEditor(messageUpdate));
}, DECORATING_DEBOUNCE);

export const sendUpdatedMessageBody = (messageUpdate:{editorState:any, network:string}) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();

    let editorStateMap = messageEditorStateMap(state);

    //We only want to dispatch an update if the network already exists in our state
    if (editorStateMap.get(messageUpdate.network)) {
      const network = messageUpdate.network;
      const editorState = messageUpdate.editorState;

      let newMessageEditorStateMap: ImmutableMap<string, EditorState> = editorStateMap.set(network, editorState);

      //Now we need to see if our cursor is 'inside' an URL. If not, we
      // need to update our url list in the store.
      const anchorTextBlock = editorState.getCurrentContent().getBlockForKey(editorState.getSelection().getAnchorKey()).getText();
      const anchorOffset = editorState.getSelection().getAnchorOffset();

      const insideSomeUrl = isPositionInsideAnUrl(anchorTextBlock, anchorOffset);

      debounceDecorating(dispatch, messageUpdate);

      dispatch(updateMessageBody(newMessageEditorStateMap));
      dispatch(updateMentions(newMessageEditorStateMap));
      if (!messageTitleSelector(state)) {
        dispatch(updateTitleTagVariables());
      }

      if (!insideSomeUrl) {
        dispatch(updateUrlList());
      }
    }
  };
};
