/* @flow */
import { createAction } from 'redux-actions';
import { getCompanyCampaignFromRoute, messageHasEventId } from 'selectors/RouterSelector';
import { PlanActionConstants } from 'constants/ActionConstants';
import { createPlansFromPayload } from 'adapters/PlanAdapter';
import { getPlanById } from 'selectors/PlanSelectors';
import type { GetPlanDataResponse } from 'apis/PlanApi';
import { getPlansData } from 'apis/PlanApi';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';


export const updateCurrentPlan  = createAction(PlanActionConstants.UPDATE_CURRENT_PLAN);
export const requestPlanData = createAction(PlanActionConstants.REQUEST_PLAN_DATA);
export const planDataReceived = createAction(PlanActionConstants.PLAN_DATA_RECEIVED);
export const planDataError = createAction(PlanActionConstants.PLAN_DATA_ERROR);

export const sendPlanDataRequest = (dispatch: Dispatch, getState: () => rootState) => {
  const compCampIds = getCompanyCampaignFromRoute(getState());
  dispatch(requestPlanData());

  return getPlansData(compCampIds.companyId, compCampIds.campaignId)
    .then((res: GetPlanDataResponse) => {
      if (res.data) {
        dispatch(planDataReceived(createPlansFromPayload(res.data, compCampIds.campaignId)));
        const planData = getPlanById(getState());
        const messageHasId = messageHasEventId(getState());
        if (planData && !messageHasId) {
          dispatch(updateCurrentPlan(planData));
        }
        return res;
      } else {
        throw new Error('The required plan data was not found on the payload for getPlanData');
      }
    })
    .catch((error: Error) => {
      dispatch(planDataError(error.message));
      return error;
    });
};
export const sendPlanDataRequestAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendPlanDataRequest(dispatch, getState);
  };
};
