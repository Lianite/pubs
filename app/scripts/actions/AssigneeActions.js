/* @flow */
import { createAction } from 'redux-actions';
import { getAllAssignees, getValidAssignees, getAssigneeVoices } from 'apis/AssigneeApi';
import { AssigneeActionConstants } from 'constants/ActionConstants';
import { createAssigneesFromPayload } from 'adapters/AssigneeAdapter';
import { getCampaignId, getCompanyId } from 'selectors/RouterSelector';
import { getInvalidAssignees, getAssigneeVoiceConflicts, getAssignees } from 'selectors/AssigneeSelectors';
import { getFilteredVoiceIds } from 'selectors/AccountsSelectors';
import { getUserId, getUserFirstName, getUserLastName, featureFlags } from 'reducers/index';
import { AssigneeRecord } from 'records/AssigneeRecords';
import _ from 'lodash';
import Promise from 'bluebird';
import { assigneeErrors } from 'constants/AssigneeConstants';
import { getAllSupportedNetworks } from 'constants/ApplicationConstants';

import type { Action } from 'actions/types';
import type { AssigneeData } from 'adapters/types';
import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';
import type { GetAssigneeResponse } from 'apis/AssigneeApi';

export const updateCurrentAssignee  = createAction(AssigneeActionConstants.UPDATE_CURRENT_ASSIGNEE);
export const requestAllAssignees  = createAction(AssigneeActionConstants.REQUEST_ALL_ASSIGNEES);
export const requestValidAssignees  = createAction(AssigneeActionConstants.REQUEST_VALID_ASSIGNEES);
export const allAssigneesError  = createAction(AssigneeActionConstants.ALL_ASSIGNEES_ERROR);
export const valideAssigneesError  = createAction(AssigneeActionConstants.VALID_ASSIGNEES_ERROR);
export const allAssigneesReceived  = createAction(AssigneeActionConstants.ALL_ASSIGNEES_RECEIVED);
export const validAssigneesReceived  = createAction(AssigneeActionConstants.VALID_ASSIGNEES_RECEIVED);
export const fetchAssigneesVoicesStarted  = createAction(AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_STARTED);
export const fetchAssigneesVoicesSuccessful = createAction(AssigneeActionConstants.FETCH_ASSIGNEE_VOICES);
export const fetchConflictingAssigneesVoicesSuccessful  = createAction(AssigneeActionConstants.FETCH_CONFLICTING_ASSIGNEE_VOICES);
export const fetchAssigneesVoicesError  = createAction(AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_FAILED);
export const clearAssignee  = createAction(AssigneeActionConstants.CLEAR_ASSIGNEE);

export const createCurrentAssignee  = (getState: () => rootState):Action<AssigneeRecord> => {
  const currentAssignee = new AssigneeRecord({
    id: getUserId(getState()),
    firstName: getUserFirstName(getState()),
    lastName: getUserLastName(getState()),
    conflictingVoices: []
  });
  return updateCurrentAssignee(currentAssignee);
};


export const sendAllAssigneesRequest = (dispatch: Dispatch, getState: () => rootState) => {
  dispatch(requestAllAssignees());

  return getAllAssignees(getCompanyId(getState()), getCampaignId(getState()))
    .then((res: GetAssigneeResponse) => {
      if (res.data) {
        dispatch(allAssigneesReceived(createAssigneesFromPayload(res.data)));
        return res;
      } else {
        throw new Error(assigneeErrors.allAssigneesErrorText);
      }
    })
    .catch((error: Error) => {
      dispatch(allAssigneesError(error.message));
      return error;
    });
};

export const fetchAssigneesVoices = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {

    dispatch(fetchAssigneesVoicesStarted());

    Promise.map(getAssignees(getState()), (assignee) => {
      return getAssigneeVoices(getCompanyId(getState()), getCampaignId(getState()), assignee.id, getAllSupportedNetworks(featureFlags(getState())))
      .then((res) => {
        return dispatch(fetchAssigneesVoicesSuccessful(
          {
            voices: res,
            id: assignee.id
          }
        ));
      })
      .catch((error: Error) => {
        return dispatch(fetchAssigneesVoicesError(error.message));
      });
    });

    return Promise.map(getInvalidAssignees(getState()), (invalidAssignee) => {
      return getAssigneeVoices(getCompanyId(getState()), getCampaignId(getState()), invalidAssignee.id, getAllSupportedNetworks(featureFlags(getState())))
      .then((res) => {
        return dispatch(fetchConflictingAssigneesVoicesSuccessful(
          {
            conflictingVoices: getAssigneeVoiceConflicts(res)(getState()),
            id: invalidAssignee.id
          }
        ));
      })
      .catch((error: Error) => {
        return dispatch(fetchAssigneesVoicesError(error.message));
      });
    });
  };
};

export const sendValidAssigneesRequest = (dispatch: Dispatch, getState: () => rootState) => {
  dispatch(requestValidAssignees());
  const voices = _.map(getFilteredVoiceIds(getState()), (voiceId) => {
    return _.toNumber(voiceId);
  });
  return getValidAssignees(getCompanyId(getState()), getCampaignId(getState()), voices)
    .then((res: Array<AssigneeData>) => {
      if (res) {
        dispatch(validAssigneesReceived(createAssigneesFromPayload(res)));
        return res;
      } else {
        throw new Error(assigneeErrors.validAssigneesError);
      }
    })
    .catch((error: Error) => {
      dispatch(valideAssigneesError(error.message));
      return error;
    });
};

export const resendValidAssigneesRequest = (): Function => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    return sendValidAssigneesRequest(dispatch, getState)
      .then(() => {
        dispatch(fetchAssigneesVoices());
      });
  };
};
