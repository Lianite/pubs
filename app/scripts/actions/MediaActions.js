/* @flow */
import { createAction } from 'redux-actions';
import { MediaActionConstants } from 'constants/ActionConstants';
import { customVideoThumbnail, customVideoThumbnailUploadingError } from 'reducers/index';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

export const sendUploadImageRequest = createAction(MediaActionConstants.UPLOAD_IMAGE, (files: Array<File>) => {
  return files;
});
export const requestUploadImage = createAction(MediaActionConstants.REQUEST_UPLOAD_IMAGE);
export const uploadImageSucceeded = createAction(MediaActionConstants.UPLOAD_IMAGE_SUCCEEDED);
export const uploadImageFailed = createAction(MediaActionConstants.UPLOAD_IMAGE_FAILED);
export const uploadImageCanceled = createAction(MediaActionConstants.UPLOAD_IMAGE_CANCELED);
export const revertToDefault = createAction(MediaActionConstants.REVERT_TO_DEFAULT);
export const readImagesCompleted = createAction(MediaActionConstants.READ_IMAGES_COMPLETED);


export const sendUploadVideoRequest = createAction(MediaActionConstants.UPLOAD_VIDEO, (files: Array<File>) => {
  return files;
});

export const removeUploadedVideo = createAction(MediaActionConstants.REMOVE_UPLOADED_VIDEO);
export const uploadVideoProgress = createAction(MediaActionConstants.UPLOAD_VIDEO_PROGESS);
export const requestUploadVideo = createAction(MediaActionConstants.REQUEST_UPLOAD_VIDEO);
export const uploadVideoSucceeded = createAction(MediaActionConstants.UPLOAD_VIDEO_SUCCEEDED);
export const uploadVideoFailed = createAction(MediaActionConstants.UPLOAD_VIDEO_FAILED);
export const uploadVideoCanceled = createAction(MediaActionConstants.UPLOAD_VIDEO_CANCELED);

export const setCustomVideoTitle = createAction(MediaActionConstants.SET_CUSTOM_VIDEO_TITLE);

export const requestCopyVideo = createAction(MediaActionConstants.REQUEST_COPY_VIDEO);
export const copyVideoSucceeded = createAction(MediaActionConstants.COPY_VIDEO_SUCCEEDED);
export const copyVideoFailed = createAction(MediaActionConstants.COPY_VIDEO_FAILED);

export const requestTranscodeVideoProgress = createAction(MediaActionConstants.REQUEST_TRANSCODE_VIDEO_PROGRESS);
export const updateTranscodeVideoProgress = createAction(MediaActionConstants.UPDATE_TRANSCODE_VIDEO_PROGRESS);
export const transcodeVideoSucceeded = createAction(MediaActionConstants.TRANSCODE_VIDEO_SUCCEEDED);
export const requestTranscodeVideoResults = createAction(MediaActionConstants.REQUEST_TRANSCODE_VIDEO_RESULTS);
export const transcodeVideoResultsSucceeded = createAction(MediaActionConstants.TRANSCODE_VIDEO_RESULTS_SUCCEEDED);

export const uploadVideoThumb = createAction(MediaActionConstants.UPLOAD_VIDEO_THUMB);
export const requestUploadVideoThumb = createAction(MediaActionConstants.REQUEST_UPLOAD_VIDEO_THUMB);
export const uploadVideoThumbSucceeded = createAction(MediaActionConstants.UPLOAD_VIDEO_THUMB_SUCCEEDED);
export const uploadVideoThumbFailed = createAction(MediaActionConstants.UPLOAD_VIDEO_THUMB_FAILED);
export const uploadVideoThumbCanceled = createAction(MediaActionConstants.UPLOAD_VIDEO_THUMB_CANCELED);
export const removeVideoThumb = createAction(MediaActionConstants.REMOVE_VIDEO_THUMB);

export const readVideosCompleted = createAction(MediaActionConstants.READ_VIDEOS_COMPLETED);
export const reprocessVideo = createAction(MediaActionConstants.REPROCESS_VIDEO);
export const videoThumbnailFrameChanged = createAction(MediaActionConstants.VIDEO_THUMBNAIL_FRAME_CHANGED);


function mediaIsImage(files = []) {
  return files.length && files.every(file => file.type && file.type.indexOf('image/') === 0);
}

function mediaIsVideo(files = []) {
  return files.length && files.every(file => file.type && file.type.indexOf('video/') === 0);
}

export const sendUploadMediaRequest = (files: Array<File>) => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    if (mediaIsImage(files)) {
      dispatch(sendUploadImageRequest(files));
    } else if (mediaIsVideo(files)) {
      dispatch(sendUploadVideoRequest(files));
    }
  };
};

export const removeOrCancelCustomThumb = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    if (customVideoThumbnail(getState()) || customVideoThumbnailUploadingError(getState())) {
      dispatch(removeVideoThumb());
    } else {
      dispatch(uploadVideoThumbCanceled());
    }
  };
};
