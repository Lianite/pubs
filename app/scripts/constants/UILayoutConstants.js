/* @flow */
import { SupportedNetworks } from 'constants/ApplicationConstants';

export const PostPreviewWidths = {
  [SupportedNetworks.FACEBOOK]: '500px',
  [SupportedNetworks.TWITTER]: '600px'
};

export const POST_PREVIEW_OVERLAY_TRANSITION_TIMING = 125;

export const TARGETING_LAYOUT_CONSTANTS = {
  AUTO_SCROLL_DURATION: 500,
  AUTO_SCROLL_PIXEL_OFFSET: -40
};

export const MENTIONS_QUERY_DEBOUNCE = 500;
export const DECORATING_DEBOUNCE = 10;

export const keyCodeConstants = {
  ESC_KEY: 27
};

/**
 * Before changing any of the values in this constants object, be sure to test the
 * resulting changes in the DOM to ensure the styles remain usable. These constants,
 * as they stand right now, are tied to widths, paddings, margins, etc in scss files.
 */
export const accountRollupUIConstants = {
  ACCOUNT_BUTTON_SIZE_PLUS_MARGIN: 310, //max-width + margins in SelectedAccount.scss:.selectedAccount
  ACCOUNT_ROWS_BEFORE_ROLLUP: 2, //number of rows we allow to remain 'unrolled' for account buttons before rolling accounts
  ACCOUNT_ROLLUP_APPROX_WIDTH: -135, //the approxamate default width for rolled accounts
  ACCOUNT_ROLLUP_TOOLTIP_SPACE_TO_ARROW: 5 //the spacing from the edge of an account rollup tooltip to the arrow that extends the bubble
};

export const PLAN_COLOR_BAR_WIDTH = '10px';
export const PLAN_PICKER_TEXT_WIDTH = '253px';

export const TOOLTIP_ALIGN = {
  offset: [0.75, 8]
};

export const TOOLTIP_OVERLAY_STYLES = {
  maxWidth: '220px',
  fontFamily: 'Open sans',
  fontSize: '12px'
};

export const textWidthForHoverText = {
  LABELS_WRAPPER: 245,
  SELECTED_ACCOUNT: 225,
  LABEL: 173,
  PLAN_PICKER: 334,
  LINK_PREVIEW_URL_PICKER: 334,
  ASSIGNEE_DROPDOWN: 334
};

export const toolTipOverlayStyle = {
  'overflowWrap': 'break-word',
  'fontFamily': 'Open Sans',
  'fontSize': '12px'
};

export const messageHeaderUIConstants = {
  DEFAULT_WIDTH: 1024              // Min Width of the App
};
