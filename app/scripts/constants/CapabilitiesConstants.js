/* @flow */
import type { NetworkCapabilities } from 'constants/types';


export const TwitterConstants : NetworkCapabilities = {
  maxChars: 140
};
