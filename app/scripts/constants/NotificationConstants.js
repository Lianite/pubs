/* @flow */

export const MessageNotificationsText = {
  genericError: 'Oops! There was a problem and we could not save your message.'
};
