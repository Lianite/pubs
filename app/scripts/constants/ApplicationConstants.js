/* @flow */

import _ from 'lodash';

import type { Map as ImmutableMap } from 'immutable';
/**
  Constants that deal with network calls and apis
*/

export const KeyCodes = {
  ESC_KEY: 27
};

export const DatetimeConstants = {
  MS_IN_SEC: 1000
};

export const DefaultNetwork = 'MESSAGE';

export const DefaultNetworkForBackend = 'MULTI_CHANNEL';

export const SupportedNetworks = {
  [DefaultNetwork]: DefaultNetwork,
  FACEBOOK: 'FACEBOOK',
  TWITTER: 'TWITTER',
  GOOGLEPLUS: 'GOOGLEPLUS'
};

export const getAllSupportedNetworks = (featureFlags: ImmutableMap<string, bool>): Array<string> => {
  let unsupportedNetworks = [DefaultNetwork];
  if (!featureFlags.get('googleplus', false)) {
    unsupportedNetworks = unsupportedNetworks.concat([SupportedNetworks.GOOGLEPLUS]);
  }
  return _.difference(_.values(SupportedNetworks), unsupportedNetworks);
};

export const networkIcons = {
  [SupportedNetworks.FACEBOOK]: 'rad-icon-network-facebook',
  [SupportedNetworks.TWITTER]: 'icon-network-twitter-bounds',
  [SupportedNetworks.GOOGLEPLUS]: 'icon-network-google-plus-bounds'
};

export const networkUserFacingName = {
  [SupportedNetworks.FACEBOOK]: SupportedNetworks.FACEBOOK,
  [SupportedNetworks.TWITTER]: SupportedNetworks.TWITTER,
  [SupportedNetworks.GOOGLEPLUS]: 'GOOGLE+'
};

export const DarkPostConstants = {
  INVISIBLE: 'INVISIBLE',
  VISIBLE: 'VISIBLE'
};

export const PermissionConstants = {
  FULL_PUBLISHING_ACCESS_KEY: 'accesslevel.publishing.full',
  PUBLISH_PUBLISHING_ACCESS_KEY: 'accesslevel.publishing.publish',
  EDIT_PUBLISHING_ACCESS_KEY: 'accesslevel.publishing.edit',
  CREATE_PUBLISHING_ACCESS_KEY: 'accesslevel.publishing.create',
  VIEW_PUBLISHING_ACCESS_KEY: 'accesslevel.publishing.view',
  NONE_PUBLISHING_ACCESS_KEY: 'accesslevel.publishing.none'
};

export const MessageConstants = {
  PENDING: 'PENDING',
  DRAFT: 'DRAFT'
};
export type MessageStatusType = 'PENDING' | 'DRAFT' | 'PUBLISHED' | 'SCHEDULE';

export const VoiceFilteringConstants = {
  FILTER_SELECTED: 'SELECTED',
  FILTER_UNSELECTED: 'UNSELECTED',
  DEFAULT_MAX_VOICES_PER_PAGE: 40,
  ELEMENT_HEIGHT: 35,
  ACCOUNT_SELECTOR_DEFAULT_HEIGHT: 355,
  ACCOUNT_SELECTOR_PADDING_BOTTOM: 120,
  ROLLED_CREDENTIAL_LIST_MAX: 10,
  ROLLED_CREDENTIAL_TRANSITION_MS: 200,
  ADVANCED_OPTIONS_HEIGHT: 46
};

export type RequestStatusType = 'STATUS_NEVER_REQUESTED' | 'STATUS_REQUESTED' | 'STATUS_LOADED' | 'STATUS_ERROR' | 'STATUS_CANCELED';

export const RequestStatus = {
  STATUS_NEVER_REQUESTED: 'STATUS_NEVER_REQUESTED',
  STATUS_REQUESTED: 'STATUS_REQUESTED',
  STATUS_LOADED: 'STATUS_LOADED',
  STATUS_ERROR: 'STATUS_ERROR',
  STATUS_CANCELED: 'STATUS_CANCELED'
};

export const HTTPSTATUS = {
  OK: 200,
  CREATED: 201,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  INTERNAL_SERVER_ERROR: 500
};

export const TimeConstants = {
  OFFSET_DIVISOR: 100,
  HOURS_TO_SECONDS_MULTIPLIER: 3600
};

export const CONV_AUTH_URL = '/user/check-session?redirect=';

export const UploadingStatus = {
  WAITING: 'WAITING',
  UPLOADING: 'UPLOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
};

export const MediaMimeTypeConstants = {
  JPEG: 'image/jpeg',
  PNG: 'image/png',
  GIF: 'image/gif',
  ANIMATED_GIF: 'image/gif+animated'
};

export const FilesizeConstants = {
  '3MB': 3145728,
  '5MB': 5242880,
  '15MB': 15728640,
  '50MB': 52428800
};

export type UploadingStatusType = 'WAITING' | 'UPLOADING' | 'SUCCESS' | 'ERROR';
export type SupportedNetworksType = typeof SupportedNetworks.FACEBOOK | typeof SupportedNetworks.TWITTER

export const LinkPreviewMaxCustomThumbs = 10;

export const MAX_MESSAGE_TITLE_CHARS = 64;

export const EnvironmentConstants = {
  LABEL_CREATION_ADMIN_PRIVILEGE: 'EDIT_COMPANY',
  CONVERSATIONS: 'Conversations',
  PROD_LOGIN_HOSTNAME: 'login.spredfast.com',
  STAGING_LOGIN_HOSTNAME: 'infralogin.spredfast.com',
  LOCAL_DEVELOPMENT_CONV_HOSTNAME: 'devbox.spredfast.com',
  TEST_CONV_HOSTNAME: 'mock.spredfast.com',
  PROD: 'production',
  STAGING: 'staging',
  DEV: 'development',
  TEST: 'test'
};

export const LinkTags = {
  PROD_HOSTNAME: 'linktagging.spredfast.com',
  STAGE_HOSTNAME: 'linktagging-stage.spredfast.com',
  DEV_HOSTNAME: 'linktagging-dev.spredfast.com',
  SF_TRACKING_ID_TOKEN: '<<sf_tracking_id>>',
  DATETIME_TOKEN: '<<sf_date_time_tag_value>>',
  SPREDFAST_TRACKING_ID_TAG_VARIABLE_TYPE: 'spredfast-tracking-id',
  CHANNEL_TAG_VARIABLE_TYPE: 'channel',
  ACCOUNT_TAG_VARIABLE_TYPE: 'account',
  SINGLE_VALUE_TAG_VARIABLE_TYPE: 'single-value',
  DATETIME_TAG_VARIABLE_TYPE: 'datetime',
  LABELS_TAG_VARIABLE_TYPE: 'labels',
  TITLE_TAG_VARIABLE_TYPE: 'title'
};

export const LabelConstants = {
  MAXIMUM_CHARACTER_LENGTH: 255
};

export const DEFAULT_USER_AVATAR_URI: string = '/images/default-avatar.jpg';

export const TARGETING_TYPES = {
  LOCALE: 'LOCALE',
  GEOGRAPHY: 'GEOGRAPHY',
  GENDER: 'GENDER'
};

export const TARGETING_SUBTYPES = {
  COUNTRY: 'COUNTRY',
  REGION: 'REGION',
  METRO: 'METRO',
  ZIP_CODE: 'ZIP_CODE'
};

export const GENDER_TARGETING_TYPES = {
  All: {description: 'All', value: '0'},
  Men: {description: 'Male', value: '1'},
  Women: {description: 'Female', value: '2'}
};
