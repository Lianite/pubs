/* @flow */
export const updateMessageDebounceTime = 500; //in ms
export const updateDebounceInputTime = 2000; //in ms
export const toolTipPopUpDelay = 1; //in sec

export const SidebarPages = {
  BASICS: 'SIDEBAR_PAGE_BASICS',
  WORKFLOW: 'SIDEBAR_PAGE_WORKFLOW',
  NOTES: 'SIDEBAR_PAGE_NOTES',
  LINKS: 'SIDEBAR_PAGE_LINKS',
  FACEBOOK: 'SIDEBAR_PAGE_FACEBOOK',
  TWITTER: 'SIDEBAR_PAGE_TWITTER',
  GOOGLEPLUS: 'SIDEBAR_PAGE_GOOGLEPLUS'
};

export const DynamicSidebarButtons = {
  LINKS: 'SIDEBAR_PAGE_LINKS',
  FACEBOOK: 'SIDEBAR_PAGE_FACEBOOK',
  TWITTER: 'SIDEBAR_PAGE_TWITTER',
  GOOGLEPLUS: 'SIDEBAR_PAGE_GOOGLEPLUS'
};

type SidebarButtonPropertyType = {
  buttonImage: string,
  pageName: string,
  isActive?: bool
}

export type SidebarButtonsType = {
  SIDEBAR_PAGE_BASICS: SidebarButtonPropertyType,
  SIDEBAR_PAGE_WORKFLOW: SidebarButtonPropertyType,
  SIDEBAR_PAGE_NOTES: SidebarButtonPropertyType,
  SIDEBAR_PAGE_LINKS?: SidebarButtonPropertyType,
  SIDEBAR_PAGE_FACEBOOK?: SidebarButtonPropertyType,
  SIDEBAR_PAGE_TWITTER?: SidebarButtonPropertyType,
  SIDEBAR_PAGE_GOOGLEPLUS?: SidebarButtonPropertyType
}

export const ValidationType = {
  VALIDATION_TYPE_NO_PROBLEMS: 'VALIDATION_TYPE_NO_PROBLEMS',
  VALIDATION_TYPE_ERROR: 'VALIDATION_TYPE_ERROR',
  VALIDATION_TYPE_EDIT_PERMISSION_ERROR: 'VALIDATION_TYPE_EDIT_PERMISSION_ERROR',
  VALIDATION_TYPE_IMPROVEMENT: 'VALIDATION_TYPE_IMPROVEMENT'
};

export const CarouselDirections = {
  FORWARD: 'FORWARD',
  BACKWARD: 'BACKWARD'
};
