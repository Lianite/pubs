/* @flow */
import {SidebarPages} from 'constants/UIStateConstants';
import _ from 'lodash';

import type { NotesMenuItemsType } from 'constants/types';

export const messageBodyText = {
  messageEditorDefaultPlaceholder: "What's happening?"
};

export const SidebarTexts = {
  [SidebarPages.BASICS]: 'Basics',
  [SidebarPages.WORKFLOW]: 'Workflow',
  [SidebarPages.NOTES]: 'Notes',
  [SidebarPages.LINKS]: 'Links',
  [SidebarPages.FACEBOOK]: 'Facebook',
  [SidebarPages.TWITTER]: 'Twitter',
  [SidebarPages.GOOGLEPLUS]: 'Google+'
};

export const SidebarComingSoonTexts = {
  [SidebarPages.BASICS]: 'Add labels to your message and designate a plan — coming soon!',
  [SidebarPages.WORKFLOW]: 'Use your preset Assignment and Approval workflows in this tab — coming soon!',
  [SidebarPages.NOTES]: 'See activity, track important information around your message, and communicate with team members — coming soon!',
  [SidebarPages.LINKS]: 'This should never show up',
  [SidebarPages.FACEBOOK]: 'Reach your audience with targeting and paid options and more — coming soon!',
  [SidebarPages.TWITTER]: 'Set automatic retweets and more — coming soon!',
  [SidebarPages.GOOGLEPLUS]: 'Google+'
};

export const mainText = {
  permissionBlockingHeader: "Sorry, you don't have access to publishing",
  permissionBlockingSubtitle: 'Have any questions? ',
  permissionBlockingSubtitleLinkText: 'Contact Customer Support'
};

export const schedulerAgendaTexts = {
  calendarComingSoon: 'Calendar is arriving soon!'
};

export const messageTitleBoxText = {
  defaultPlaceHolderText: 'Untitled Message'
};

export const MessageBoxHeaderTexts = {
  applyToAllButton: 'Apply To All Channels'
};

export const publishBarText = {
  immediatelyPublishText: 'PUBLISH',
  schedulePublishText: 'SCHEDULE',
  saveDraftButtonText: 'SAVE DRAFT'
};

export const permissionsText = {
  noEditAccessError: 'You can view this post, but you can\'t edit its contents or publish.'
};

export const validationText = {
  generalError: ' has an error, check below',
  cannotUseMultiPubs: 'Sorry, you don\'t have access to publishing',
  contactCustomerSupport: 'Contact Customer Support',
  haveAnyQuestions: 'Have any questions?'
};

export const scheduleMessageText = {
  header: 'Schedule Post',
  timeDateTextInputPlaceholder: 'Type or select a time and date…',
  timeZoneSubtitle: 'Time Zone:',
  datetimeStringNotCurrent: 'Oops, that date is in the past!',
  datetimeStringInvalid: 'Oops, that is not a valid date or time!',
  timeSelectorPlaceholderText: 'Type or select a time...',
  noTimesFound: 'No time found'
};

export const mediaBarText = {
  dropText: 'Drop media files here',
  addMediaPrefix: 'Add Media from your ',
  computer: 'Computer'
};

export const networkListText = {
  noNetworkSelected: 'No Network Selected'
};

export const videoText = {
  uploading: 'Uploading',
  transcoding: 'Prepping For Networks',
  customizeThumbnail: 'Customize Thumbnail',
  videoTitle: 'Video Title',
  customThumbWarning: 'Custom thumbnail images are only supported on Facebook',
  customThumbUploading: 'Uploading custom thumbnail',
  selectFromComp: 'Select From Computer',
  frameGrab: 'Frame Grab'
};

export const videoPlayer = {
  unsupported: 'Your browser does not support the video tag.'
};

export const planPickerText = {
  title: 'Plans',
  placeHolderText: 'Select Plan...',
  subText: 'Select a Plan to associate with your message',
  emptyList: 'No plans',
  emptyFilter: 'No plans matching that name'
};

export const labelsWrapperText = {
  title: 'Labels',
  groupViolationMessage: 'These labels belong to same label group and can’t both be used. Issue must be resolved to publish.',
  placeHolderText: 'Select Labels...',
  mainText: 'Add labels that will be associated with your publishing message.'
};

export const linkPreviewText = {
  titleHolder: 'Enter a title for link preview',
  descriptionHolder: 'Enter a description for link preview',
  imageHolder: 'No image available',
  customizeThumbnail: 'Customize Thumbnail',
  scrapingPreview: 'Generating Link Preview... '
};

export const linkPreviewUrlPickerText = {
  title: 'Link Preview',
  none: 'None'
};

export const mediaErrorText = {
  imageUploadError: 'Image Upload Error',
  videoUploadError: 'Video Upload Error',
  videoCustomThumbUploadError: 'Upload Error',
  imageSizeTooLargeTitle: 'Image Too Large',
  videoSizeTooLargeTitle: 'Video Too Large',
  videoDurationErrorTitle: 'Video Duration Invalid',
  videoDurationTooLongDescription: 'Video too long',
  videoDurationTooShortDescription: 'Video too short',
  mediaSizeTooLargeDescription: 'Exceeds size limit'
};

export const accountSelectionText = {
  loadingAccountsText: 'Loading More Accounts...',
  authenticationWarningText: 'Account requires reauthentication.',
  selectAll: 'Select All',
  clearSelection: 'Clear Selection',
  show: 'Show',
  hide: 'Hide',
  advancedOptions: 'Account & Search Options',
  advancedOptionsLabel: 'Account & Search Options',
  searchAccount: 'Search Account Name',
  searchAccountSet: 'Search Account Set Name'
};

export const mentionsQueryMenuText = {
  noResultsFound: 'Oops, we didn\'t find any accounts with that name. Try searching again!',
  errorQuerying: 'Looks like we’re having trouble returning results. Try again shortly!',
  retrievingAccounts: 'Retrieving Accounts…'
};

export const linkOptions = {
  shortenOnPublish: 'Shorten on Publish',
  linksTitle: 'LINKS',
  addLinkTags: 'Add Link Tags',
  linkTagsDisableTooltip: 'Link tagging is only available for shortened URLs'
};

//Notes

//this could also be implmeneted with $Keys<NotesMenuItems>, but that is an experimental
//feature in flow at the moment.
export const NotesMenuItems = {
  NOTES: 'NOTES',
  ACTIVITY: 'ACTIVITY'
};

export const NotesMenuItemsList: Array<NotesMenuItemsType> = _.map(NotesMenuItems, item => item);

export const NO_NOTES_TEXT = 'No notes yet';
export const NO_NOTES_VIEW_ACTIVITY = 'view activity';
export const NOTE_SAVE_ERROR_HEADER = 'Note cannot be saved';
export const NOTE_SAVE_ERROR_BODY = 'Please try again shortly';
export const NOTE_ADD_BUTTON_TEXT = 'Save Note';
export const NOTE_INPUT_PLACEHOLDER_TEXT = 'Add a note...';
export const NO_ACTIVITY_TEXT = 'No activity yet';
export const NO_ACTIVITY_VIEW_NOTES = 'view notes';
export const NO_NOTES_OR_ACTIVITY = 'No notes or activity yet.';

export const workflowContextMenuText = {
  title: 'WORKFLOW',
  approvalsPlaceholderHeader: 'Approvals are arriving soon!',
  approvalsPlaceholderBody: 'Send your message into an approval path and more - coming soon!'
};

export const networkOptionsBarText = {
  addTitlePlaceholder: 'Add a title for this video (optional)...',
  updateTitlePlaceholder: 'UPDATE FACEBOOK VIDEO TITLE'
};

//Authentication warnings
export const AUTHENTICATION_WARNING_ACCOUNT_NAME_COMPANION = '(not authenticated)';
export const AUTHENTICATION_WARNING_TOOLTIP = 'This account is not authenticated';

//Assign
export const assigneeText = {
  title: 'Assignment',
  mainText: 'Message assigned to:',
  emptyFilterText: 'No assignee of that name found.',
  plural: {
    toolTipText: 'cannot be selected because they don’t have access to the currently selected account sets.',
    accountSetConflictHeader : 'Account Sets Conflicts',
    accountSetRestrictedHeader : 'Account Sets Not Available',
    viewAccountSets: ' View account sets.',
    warningMessage: ' account sets not available for this assignee and won’t be available in account list.'
  },
  singular: {
    toolTipText: 'cannot be selected because they don’t have access to the currently selected account set.',
    accountSetConflictHeader : 'Account Set Conflict',
    accountSetRestrictedHeader : 'Account Set Not Available',
    viewAccountSets: ' View account set.',
    warningMessage: ' account set not available for this assignee and won’t be available in account list.'
  }
};

export const assigneeErrors = {
  allAssigneesErrorText: 'The required assignee data was not found on the payload for getAllAssignees',
  validAssigneesError: 'The required assignee data was not found on the payload for getValidAssignees'
};

//Facebook Menu
export const FACEBOOK_MENU_HEADER = 'facebook';
export const FACEBOOK_TARGETING_PLACEHOLDER_HEADER = 'Targeting is arriving soon!';
export const FACEBOOK_TARGETING_PLACEHOLDER_BODY = 'Target your audience by interest, location, and other demographics.';
export const FACEBOOK_TARGETING_HEADER = 'targeting';
export const FACEBOOK_TARGETING_LANGUAGE_HEADER = 'languages';
export const FACEBOOK_TARGETING_LANGUAGE_EMPTY_LIST = 'No languages match your search';
export const FACEBOOK_TARGETING_LANGUAGE_PLACEHOLDER = 'Enter a Language...';
export const FACEBOOK_TARGETING_LOCATION_EMPTY_LIST = 'You can add a country, state/region, city, or postal code.';
export const FACEBOOK_TARGETING_LOCATION_PLACEHOLDER = 'Enter a Location...';
export const FACEBOOK_TARGETING_LANGUAGE_INFO = 'Leave this blank unless the audience you are targeting uses a language that is not common to the location you have chosen above.';
export const FACEBOOK_TARGETING_DISABLED_MESSAGE = 'Targeting Not Available';
export const FACEBOOK_TARGETING_DISABLED_DETAILS = (count: number): string => (
  `You\'ve selected ${count} account${count > 1 ? 's' : ''} that ${count > 1 ? 'don\'t' : 'doesn\'t'} support Targeting.`
);
export const FACEBOOK_TARGETING_VIEW_UNSUPPORTED_ACCOUNTS = (count: number) => `View Unsupported Account${count > 1 ? 's' : ''}`;
export const FACEBOOK_TARGETING_REMOVE = 'Remove Targeting';
export const FACEBOOK_TARGETING_REMOVE_DETAILS = 'Not all of your selected Facebook accounts support Targeting. Remove problematic accounts or remove targeting.';
export const FACEBOOK_ACCOUNT_DISABLED_MESSAGE = 'Account does not support ';
export const FACEBOOK_TARGETING_ACCOUNT_DISABLED = 'Facebook Targeting';

export const FACEBOOK_DARKPOST_CHECKBOX_TEXT = 'Make This A Dark Post';
export const FACEBOOK_DARKPOST_CHECKBOX_TOOLTIP_TEXT = 'By checking this option, your post will be an unpublished page post (aka dark). It will not be shown to your organic audience.';
export const FACEBOOK_DARKPOST_ACCOUNT_DISABLED = 'Dark Posting';
export const FACEBOOK_DARKPOST_DISABLED_MESSAGE = 'Dark Posting Not Available';
export const FACEBOOK_DARKPOST_DISABLED_DETAILS = (count: number): string => (
  `You\'ve selected ${count} account${count > 1 ? 's' : ''} that ${count > 1 ? 'don\'t' : 'doesn\'t'} support Dark Posting.`
);

export const FACEBOOK_TARGETING_GENDER_HEADER = 'Gender';
export const FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS = {
  All: 'All',
  Male: 'Men',
  Female: 'Women'
};

//Frame Grab
export const FRAME_GRAB_TEXT = {
  FRAME_GRAB_HEADER_TEXT: 'FRAME GRAB',
  FRAME_GRAB_PRIMARY_BUTTON_TEXT: 'DONE',
  FRAME_GRAB_SECONDARY_BUTTON_TEXT: 'CANCEL'
};
