/* @flow */
import { NotesMenuItems } from 'constants/UITextConstants';

export type EnvDetails = {
  environmentUrl: string
};

export type PublishingAppType = {
  envDetails: Array<EnvDetails>
};

export type NetworkCapabilities = {
  maxChars: number
}

export type NotesMenuItemsType = $Keys<typeof NotesMenuItems>;
