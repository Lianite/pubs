/* @flow */
export const MAX_NOTE_CHARS = 150;

export const NOTE_SAVE_FAIL_NOTIFICATION_EXPIRE = 3000;

export const NOTE_TRANSITION_TIMEOUT = 500;

export const NOTES_LIST_SCROLL_ANIMATION_DURATION = 250;

export const NOTES_LIST_SCROLL_ANIMATION_OFFSET = -100;
