/* @flow */

export const ActivityDescriptionConstants = {
  CREATED: 'Created',
  EDITED: 'Edited'
};

export const defaultAvatarThumbnail = '/images/default-avatar.jpg';
