/* @flow */
export const assigneeErrors = {
  allAssigneesErrorText: 'The required assignee data was not found on the payload for getAllAssignees',
  validAssigneesError: 'The required assignee data was not found on the payload for getValidAssignees'
};
