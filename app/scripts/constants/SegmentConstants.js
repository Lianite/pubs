/* @flow */
export const SegmentEventConstants = {
  PUBLISH_MESSAGE: 'Publish',
  SAVE_MESSAGE_AS_DRAFT: 'Save message as draft'
};

export const SegmentProductConstants = {
  MULTICHANNEL_PUBLISHING: 'Multichannel Publishing',
  CONVERSATIONS_PUBLISHING: 'Conversations:Publishing'
};
