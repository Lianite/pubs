/* @flow */
import { handleActions } from 'redux-actions';
import {
  getPlainTextFromEditorState } from 'utils/MessageUtils';
import {
  Record,
  Map as ImmutableMap,
  List as ImmutableList } from 'immutable';
import { EditorState, ContentState } from 'draft-js';
import { createSelector } from 'reselect';
import { RequestStatus, DefaultNetwork, MessageConstants } from 'constants/ApplicationConstants';
import { MessageActionConstants } from 'constants/ActionConstants';
import { createDeepEqualSelector } from 'utils/SelectorUtils';
import { setStateFromObject } from 'utils/ReducerUtils';
import { ActivitiesRecord } from 'records/MessageRecords';
import _ from 'lodash';

import type Moment from 'moment';
import type { Action } from 'actions/types';

export class ScheduledDatetimeRecord extends Record({
  scheduledDatetime: null,
  scheduledDatetimeHasError: false
}) {
  scheduledDatetime: ?Moment;
  scheduledDatetimeHasError: bool;
}

const defaultState = {
  eventId: null,
  eventAuthorUserId: null,
  messageStatus: '',
  messageTitle: '',
  messageEditorStateMap: ImmutableMap({
    [DefaultNetwork]: EditorState.createEmpty()
  }),
  saving: false,
  saveMessageRequestState: RequestStatus.STATUS_NEVER_REQUESTED,
  loadingMessageStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  loadingMessageError: '',
  scheduledDatetime: new ScheduledDatetimeRecord(),
  activities: ImmutableList()
};

export class MessageReducerState extends Record(_.cloneDeep(defaultState)) {
  eventId: ?number;
  eventAuthorUserId: ?number;
  messageStatus: string;
  messageTitle: string;
  messageEditorStateMap: ImmutableMap<string, EditorState>;
  saving: bool;
  saveMessageRequestState: string;
  loadingMessageStatus: string;
  loadingMessageError: string;
  scheduledDatetime: ScheduledDatetimeRecord;
  activities: ImmutableList<ActivitiesRecord>
};

//SELECTORS
export type MapOfMessageText = {[key: string]: string};

export const _getEventAuthorUserId = (state: MessageReducerState) => state.eventAuthorUserId;
export const _messageEditorStateMap = (state: MessageReducerState) => state.messageEditorStateMap;
export const _messageCurrentlySavingSelector = (state: MessageReducerState) => state.saving;
export const _messageTitleSelector = (state: MessageReducerState) => state.messageTitle;
export const _eventIdSelector = (state: MessageReducerState) => state.eventId;
export const _messageIsSaved = (state: MessageReducerState) => !!state.eventId;
export const _messageEditorState = (state: MessageReducerState, network:string) => state.messageEditorStateMap.get(network);
export const _loadingMessageStatusSelector = (state: MessageReducerState) => state.loadingMessageStatus;
export const _scheduledDateTimeSelector = (state: MessageReducerState) => state.scheduledDatetime.scheduledDatetime;
export const _scheduledDateTimeSelectorIsValid = (state: MessageReducerState) => state.scheduledDatetime.scheduledDatetimeHasError;
export const _saveMessageRequestState = (state: MessageReducerState): string => state.saveMessageRequestState;
export const _getMessageStatus = (state: MessageReducerState) => state.messageStatus;
export const _getActivities = (state: MessageReducerState) => state.activities;

export const _messageEditorStateText = (state:MessageReducerState, network:string) => {
  return getPlainTextFromEditorState(_messageEditorState(state, network));
};

export const _totalNetworkCount = createSelector(
  _messageEditorStateMap,
  (messageEditorStateMap) => {
    return messageEditorStateMap.size;
  }
);

export const _getMessageEditorStateMapText = createSelector(
  _messageEditorStateMap,
  (editorStateMap: ImmutableMap<string, EditorState>): MapOfMessageText => {
    let returnObject = {};
    editorStateMap.forEach((value:EditorState, key:string) =>{
      returnObject[key] = value.getCurrentContent().getPlainText();
    });

    return returnObject;
  }
);

export const _disableApplyToAll = createDeepEqualSelector(
  _getMessageEditorStateMapText,
  (editorStateTextMap: MapOfMessageText): bool => {
    //disable apply to all if all the text is already the same
    let array = _.values(editorStateTextMap);
    return !!array.reduce((a, b) => {
      return (a === b) ? a : NaN;
    });
  }
);

export const _loadingMessageHasCompleted = createSelector(
  _loadingMessageStatusSelector,
  (loadedStatus) => loadedStatus === RequestStatus.STATUS_LOADED
);

export const _savingMessageHasCompleted = createSelector(
  _saveMessageRequestState,
  (saveMessageRequestState) => saveMessageRequestState === RequestStatus.STATUS_LOADED
);

type funcReturn = (state: any) => Array<string>;

//NOTE: because _messageEditorStateMap changes whenever *any* content
// changes for any network, or when a network is added or removed, we
// only want to output a difference if the values have actually changed.
// So make an internal selector that will output a list of networks any
// time anything changes. Then, create the real _selectedNetworks, which
// will only output when the actual *values* of the set of networks has
// changed.

const _selectedNetworksInternal = createSelector(
  _messageEditorStateMap,
  (messageEditorStateMap) => {
    return _.without(messageEditorStateMap.keySeq().toArray(), DefaultNetwork);
  }
);

export const _selectedNetworks: funcReturn = createDeepEqualSelector(
  _selectedNetworksInternal,
  (selectedNetworks) => {
    return selectedNetworks;
  }
);

//REDUCERS
let reducers = {};

reducers[MessageActionConstants.SAVE_MESSAGE_REQUEST_FAILED] = (state: MessageReducerState, action: Action<string>): MessageReducerState => {
  return state.merge({
    saveMessageRequestState: RequestStatus.STATUS_ERROR,
    saving: false
  });
};

reducers[MessageActionConstants.SAVE_MESSAGE_REQUEST_PENDING] = (state: MessageReducerState, action: Action<string>): MessageReducerState => {
  return state.set('saveMessageRequestState', RequestStatus.STATUS_REQUESTED);
};

reducers[MessageActionConstants.SAVE_MESSAGE_REQUEST_SUCCEEDED] = (state: MessageReducerState, action: Action<string>): MessageReducerState => {
  return state.set('saveMessageRequestState', RequestStatus.STATUS_LOADED);
};

reducers[MessageActionConstants.UPDATE_MESSAGE_TITLE] = (state: MessageReducerState, action: Action<string>): MessageReducerState => {
  return state.set('messageTitle', action.payload);
};

reducers[MessageActionConstants.APPLY_TO_ALL_CHANNELS] = (state: MessageReducerState, action): MessageReducerState => {
  let messageEditorStateMap = state.messageEditorStateMap;

  let newMessageEditorStateMap = state.messageEditorStateMap.withMutations(newMessageEditorStateMap => {
    messageEditorStateMap.keySeq().toArray().forEach((key) => {
      let contentToCopy: ContentState = action.payload.editorState.getCurrentContent();
      let oldEditorState: EditorState = messageEditorStateMap.get(key);
      newMessageEditorStateMap = newMessageEditorStateMap.set(key, EditorState.push(oldEditorState, contentToCopy));
    });
  });

  newMessageEditorStateMap = newMessageEditorStateMap.set(action.payload.network, EditorState.moveFocusToEnd(newMessageEditorStateMap.get(action.payload.network)));

  return state.set('messageEditorStateMap', newMessageEditorStateMap);
};

reducers[MessageActionConstants.UPDATE_MESSAGE_BODY] = (state: MessageReducerState, action): MessageReducerState => {
  return state.set('messageEditorStateMap', action.payload);
};

reducers[MessageActionConstants.UNDO_APPLY_ALL] = (state: MessageReducerState, action: Action<*>): MessageReducerState => {
  let messageEditorStateMap: ImmutableMap<string, EditorState> = state.messageEditorStateMap;
  let newMessageEditorStateMap = state.messageEditorStateMap.withMutations(newMessageEditorStateMap => {
    messageEditorStateMap.keySeq().toArray().forEach((key) => {
      if (key !== action.payload) {
        newMessageEditorStateMap = newMessageEditorStateMap.set(key, EditorState.undo(messageEditorStateMap.get(key)));
      } else {
        newMessageEditorStateMap = newMessageEditorStateMap.set(key, messageEditorStateMap.get(key));
      }
    });
  });

  return state.set('messageEditorStateMap', newMessageEditorStateMap);
};

reducers[MessageActionConstants.REQUEST_SAVE_MESSAGE] = (state: MessageReducerState, action: Action<typeof MessageConstants>): MessageReducerState => {
  return state.merge({
    saving: true,
    messageStatus: action.payload
  });
};

reducers[MessageActionConstants.READ_MESSAGE_STARTED] = (state: MessageReducerState, action: Action<bool>): MessageReducerState => {
  return state.set('loadingMessageStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[MessageActionConstants.READ_MESSAGE_COMPLETED] = (state: MessageReducerState, { payload }): MessageReducerState => {
  let newState = new MessageReducerState({
    eventId: payload.eventId,
    eventAuthorUserId: payload.eventAuthorUserId,
    messageStatus: payload.status,
    messageTitle: payload.messageTitle,
    messageEditorStateMap: payload.messageEditorStateMap,
    loadingMessageStatus: RequestStatus.STATUS_LOADED,
    voices: state.voices,
    scheduledDatetime: payload.scheduledDatetime,
    activities: payload.activities
  });

  return newState;
};

reducers[MessageActionConstants.READ_MESSAGE_FAILED] = (state: MessageReducerState, action: Action<string>): MessageReducerState => {
  return state.merge({
    loadingMessageStatus: RequestStatus.STATUS_ERROR,
    loadingMessageError: action.payload
  });
};

reducers[MessageActionConstants.SET_SCHEDULED_TIME] = (state: MessageReducerState, action: Action<Object>): MessageReducerState => {
  return state.withMutations(newState => {
    newState.setIn(['scheduledDatetime', 'scheduledDatetime'], action.payload.scheduledDatetime);
    newState.setIn(['scheduledDatetime', 'scheduledDatetimeHasError'], action.payload.scheduledDatetimeHasError);
  });
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: MessageReducerState, action: Action<Object>): MessageReducerState => {
  return setStateFromObject(state, defaultState, action.payload);
};

export default handleActions(reducers, new MessageReducerState());

/**
 * Message Selectors
 */
export const getMessageEditorStateMap = createSelector(
  state => state.messageEditorStateMap,
  (messageEditorStateMap) => {
    return messageEditorStateMap;
  }
);
