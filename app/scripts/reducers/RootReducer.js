/* @flow */

import { combineReducers } from 'redux';
import { reducers } from 'reducers/index';
import { routerStateReducer } from 'redux-router';
import { growlerReducer } from '@spredfast/react-lib/lib/growler-notification/redux/reducer';
import { ReduxModal } from '@spredfast/react-lib';

const rootReducer = combineReducers({
  modal: ReduxModal.reducer,
  router: routerStateReducer,
  growlerNotification: growlerReducer,
  ...reducers
});

export default rootReducer;

