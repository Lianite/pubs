/* @flow */
import { handleActions } from 'redux-actions';
import { Record, List as ImmutableList, Map as ImmutableMap } from 'immutable';
import _ from 'lodash';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { MentionsActionConstants } from 'constants/ActionConstants';
import { MentionsRecord } from 'types/MentionsTypes.js';

import type { SupportedNetworksType } from 'constants/ApplicationConstants';
import type { Action } from 'actions/types';


const defaultState = {
  listOfMentions: ImmutableMap(),
  mentionsQueryList: ImmutableList(),
  isMentionsQueryOpen: ImmutableMap({
    [SupportedNetworks.FACEBOOK]: false,
    [SupportedNetworks.TWITTER]: false
  }),
  loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_QUERY_NOT_REQUESTED,
  currentMentionOffset: ''
};
export class MentionsReducerState extends Record(_.cloneDeep(defaultState)) {
  listOfMentions: ImmutableMap<string, ImmutableList<MentionsRecord>>;
  mentionsQueryList: ImmutableList<MentionsRecord>;
  isMentionsQueryOpen: ImmutableMap<string, bool>;
  loadMentionsQueryStatus: string;
  currentMentionOffset: string;
};

//SELECTORS
export const _getMentionsQueryList = (state:MentionsReducerState) => state.mentionsQueryList;
export const _getMentionsQueryUiState = (state:MentionsReducerState, network: SupportedNetworksType) => state.isMentionsQueryOpen.get(network);
export const _getMentionsQueryStatus = (state:MentionsReducerState) => state.loadMentionsQueryStatus;
export const _getCurrentMentionsList = (state:MentionsReducerState) => state.listOfMentions;
export const _getCurrentMentionOffset = (state:MentionsReducerState) => state.currentMentionOffset;
export const _getNetworkMentionsList = (state:MentionsReducerState, network: SupportedNetworksType) => state.listOfMentions.get(network);
export const _getMentionsSaveRequest = (state:MentionsReducerState) => {
  let requestListOfMentions = [];
  state.listOfMentions.forEach((networks, networkKey) => {
    networks.forEach((mention) => {
      let displayName = mention.displayName;
      if (networkKey === SupportedNetworks.TWITTER) {
        displayName = `@${mention.screenName}`;
      }
      requestListOfMentions.push({
        displayName: displayName,
        link: mention.link,
        offsetBegin: mention.offsetBegin,
        offsetEnd: mention.offsetEnd,
        profileId: mention.profileId,
        service: networkKey
      });
    });
  });
  return requestListOfMentions;
};

//REDUCERS
let reducers = {};

reducers[MentionsActionConstants.OPEN_MENTIONS_QUERY] = (state: MentionsReducerState, action: Action<SupportedNetworksType>): MentionsReducerState => {
  return state.setIn(['isMentionsQueryOpen', action.payload], true);
};

reducers[MentionsActionConstants.CLOSE_MENTIONS_QUERY] = (state: MentionsReducerState, action: Action<SupportedNetworksType>): MentionsReducerState => {
  return state.setIn(['isMentionsQueryOpen', action.payload], false);
};

reducers[MentionsActionConstants.MENTIONS_RECEIVED] = (state: MentionsReducerState, action: Action<Array<MentionsRecord>>): MentionsReducerState => {
  return state.withMutations(newState => {
    newState.set('mentionsQueryList', ImmutableList(action.payload));
    newState.set('loadMentionsQueryStatus', MentionsActionConstants.MENTIONS_RECEIVED);
  });
};

reducers[MentionsActionConstants.MENTIONS_DATA_ERROR] = (state: MentionsReducerState, action: Action<string>): MentionsReducerState => {
  return state.set('loadMentionsQueryStatus', MentionsActionConstants.MENTIONS_DATA_ERROR);
};

reducers[MentionsActionConstants.REQUEST_MENTIONS_DATA] = (state: MentionsReducerState, action: Action<string>): MentionsReducerState => {
  return state.set('loadMentionsQueryStatus', MentionsActionConstants.REQUEST_MENTIONS_DATA);
};

reducers[MentionsActionConstants.UPDATE_MENTIONS] = (state: MentionsReducerState, action: Action<string>): MentionsReducerState => {
  return state.withMutations(newState => {
    newState.set('listOfMentions', action.payload);
    newState.set('mentionsQueryList', ImmutableList());
    newState.set('loadMentionsQueryStatus', MentionsActionConstants.MENTIONS_QUERY_NOT_REQUESTED);
  });
};

reducers[MentionsActionConstants.CURRENT_MENTION_OFFSET] = (state: MentionsReducerState, action: Action<string>): MentionsReducerState => {
  return state.set('currentMentionOffset', action.payload);
};


export default handleActions(reducers, new MentionsReducerState());
