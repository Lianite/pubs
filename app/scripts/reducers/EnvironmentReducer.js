/* @flow */
import { handleActions } from 'redux-actions';
import { RequestStatus } from 'constants/ApplicationConstants';
import { EnvironmentActionConstants } from 'constants/ActionConstants';
import { setStateFromObject } from 'utils/ReducerUtils';
import { fromJS, Record, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { createSelector } from 'reselect';
import type { Action } from 'actions/types';
import type { RequestStatusType } from 'constants/ApplicationConstants';
import { StoredPlan } from 'records/PlanRecords';
import { filterUserAvatar } from 'utils/ApplicationUtils';
import { getCurrentEnvironment } from 'utils/ApiUtils';
import featureFlags from 'config/FeatureFlags';

export class TimezoneRecord extends Record({
  abbreviation: 'UTC',
  name: 'UTC',
  offset: 0
}) {
  abbreviation: string;
  name: string;
  offset: number;
};

export class CompanyRecord extends Record({
  companyMetadata: {
    contentLabelCreationRestricted: false,
    salesforceEnabled: false,
    shortenLinksByDefault: false
  },
  id: -1,
  mediaConfigDTO: {
    imageAllowedMimeTypes: ImmutableList()
  },
  name: '',
  enabledFeatures: []
})  {
  companyMetadata: {
    contentLabelCreationRestricted: boolean;
    salesforceEnabled: boolean;
    shortenLinksByDefault: boolean
  };
  id: number;
  mediaConfigDTO: {
    imageAllowedMimeTypes: ImmutableList<string>
  };
  name: string;
  enabledFeatures: Array<string>
};

export class UserRoleRecord extends Record({
  name: '',
  privileges: []
}) {
  name: string;
  privileges: Array<string>;
};

export class ConfigurationRecord extends Record({
  businessHourTimezone: {
    abbreviation: 'PST',
    name: 'PST8PDT',
    offset: '-0800'
  },
  businessHours: [],
  contentLabelCreationRestricted: true,
  id: -1,
  initiativeScopingLabels: true,
  prioritizeOpenGraphDescriptionInLinkPreviews: false,
  prioritizeOpenGraphImagesInLinkPreviews: false,
  sfTrackingIdFormat: 'IN_A_QUERY_STRING_PARAM_NAME',
  twitter: {
    defaultRetweetType: 'MANUAL',
    maxSearchStreamsPerAccount: 15,
    maxUserStreamsPerAccount: 15,
    supportedRetweetTypes: [
      'MANUAL'
    ]
  }
}) {
  businessHourTimezone: {
    abbreviation: string;
    name: string;
    offset: string;
  };
  businessHours: Array<any>;
  contentLabelCreationRestricted: bool;
  id: number;
  initiativeScopingLabels: bool;
  prioritizeOpenGraphDescriptionInLinkPreviews: bool;
  prioritizeOpenGraphImagesInLinkPreviews: bool;
  sfTrackingIdFormat: string;
  twitter: {
    defaultRetweetType: string;
    maxSearchStreamsPerAccount: number;
    maxUserStreamsPerAccount: number;
    supportedRetweetTypes: Array<string>;
  }
};

const defaultState = {
  csrfTokenStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  csrfToken: '',
  csrfTokenRequestError: '',
  userDataStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  userId: -1,
  userFirstName: '',
  userLastName: '',
  userAvatar: '',
  userDataRequestError: '',
  userRole: new UserRoleRecord(),
  campaignIdStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  campaignIdRequestError: '',
  userTimezone: new TimezoneRecord(),
  capabilitiesStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  capabilities: new ImmutableMap(),
  capabilitiesRequestError: '',
  featureFlags: new ImmutableMap(featureFlags[getCurrentEnvironment()]),
  unscheduledDraftDatetime: null,
  companyStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  company: new CompanyRecord(),
  companyRequestError:'',
  plansData: ImmutableList(),
  plansDataStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  plansDataError: '',
  fullPublishingAccess: false,
  viewAccess: true,
  editAccess: true,
  configuration: new ConfigurationRecord(),
  multiChannelEnabled: true
};

export class EnvironmentState extends Record(defaultState) {
  csrfTokenStatus: string;
  csrfToken: string;
  csrfTokenRequestError: string;
  userDataStatus: string;
  userId: number;
  userFirstName: string;
  userLastName: string;
  userAvatar: string;
  userDataRequestError: string;
  userRole: UserRoleRecord;
  campaignIdStatus: string;
  campaignIdRequestError: string;
  userTimezone: TimezoneRecord;
  capabilitiesStatus: string;
  capabilities: ImmutableMap<any, any>;
  capabilitiesRequestError: string;
  featureFlags: ImmutableMap<string, bool>;
  unscheduledDraftDatetime: ?number;
  companyStatus: RequestStatusType;
  company: CompanyRecord;
  companyRequestError: string;
  plansData: ImmutableList<StoredPlan>;
  plansDataStatus: string;
  plansDataError: string;
  fullPublishingAccess: bool;
  viewAccess: bool;
  editAccess: bool;
  configuration: ConfigurationRecord;
  multiChannelEnabled: bool;
};

//SELECTORS
export const _capabilitiesMap = (state: EnvironmentState): ImmutableMap<any, any> => state.capabilities;
export const _getCsrfTokenStatus = (state: EnvironmentState): string => state.csrfTokenStatus;
export const _getCsrfToken = (state: EnvironmentState): string => state.csrfToken;
export const _getCsrfTokenError = (state: EnvironmentState): ?string => state.csrfTokenRequestError;
export const _getUserDataStatus = (state: EnvironmentState): string => state.userDataStatus;
export const _getUserId = (state: EnvironmentState): number => state.userId;
export const _getUserFirstName = (state: EnvironmentState): string => state.userFirstName;
export const _getUserLastName = (state: EnvironmentState): string => state.userLastName;
export const _getUserAvatar = (state: EnvironmentState): string => state.userAvatar;
export const _getUserDataError = (state: EnvironmentState): ?string => state.userDataRequestError;
export const _getUserTimezone = (state: EnvironmentState): TimezoneRecord => state.userTimezone;
export const _getUnscheduledDraftDatetime = (state: EnvironmentState): ?number => state.unscheduledDraftDatetime;
export const _getCompanyData = (state: EnvironmentState): CompanyRecord => state.company;
export const _hasFullPublishingAccessFlag = (state: EnvironmentState): bool => state.fullPublishingAccess;
export const _hasViewAccess = (state: EnvironmentState): bool => state.viewAccess;
export const _hasEditAccess = (state: EnvironmentState): bool => state.editAccess;
export const _userRole = (state: EnvironmentState): UserRoleRecord => state.userRole;
export const _userPermissions = (state: EnvironmentState): Array<string> => state.userRole.privileges;
export const _contentLabelCreationRestricted = (state: EnvironmentState): bool => state.configuration.contentLabelCreationRestricted;
export const _initiativeScopingLabels = (state: EnvironmentState): bool => state.configuration.initiativeScopingLabels;
export const _getCompanyRequestStatus = (state: EnvironmentState): string => state.companyStatus;
export const _isMultiChannelEnabled = (state: EnvironmentState): bool => state.multiChannelEnabled;
export const _featureFlags = (state: EnvironmentState): ImmutableMap<string, bool> => state.featureFlags;

export const _fetchingUserHasCompleted = createSelector(
  _getUserDataStatus,
  (userFetchStatus) => userFetchStatus === RequestStatus.STATUS_LOADED
);

//REDUCERS
let reducers = {};

reducers[EnvironmentActionConstants.REQUEST_CSRF_TOKEN] = (state: EnvironmentState): EnvironmentState => {
  return state.set('csrfTokenStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[EnvironmentActionConstants.CSRF_TOKEN_RECEIVED] = (state: EnvironmentState, action: Action<string>): EnvironmentState => {
  window.PublishingApp.CSRF_TOKEN = action.payload;
  return state.set('csrfTokenStatus', RequestStatus.STATUS_LOADED).set('csrfToken', action.payload);
};

reducers[EnvironmentActionConstants.CSRF_TOKEN_ERROR] = (state: EnvironmentState, action: Action<Error>): EnvironmentState => {
  return state.set('csrfTokenStatus', RequestStatus.STATUS_ERROR).set('csrfTokenRequestError', action.payload);
};

reducers[EnvironmentActionConstants.REQUEST_USER_DATA] = (state: EnvironmentState): EnvironmentState => {
  return state.set('userDataStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[EnvironmentActionConstants.USER_DATA_RECEIVED] = (state: EnvironmentState, action: Action<{id: number, timezone: Object, viewAccess: bool, role: any, firstName: string, lastName: string, avatar: string}>): EnvironmentState => {
  return state.withMutations(newState => {
    newState.set('userDataStatus', RequestStatus.STATUS_LOADED)
      .set('userId', action.payload.id)
      .set('userFirstName', action.payload.firstName)
      .set('userLastName', action.payload.lastName)
      .set('userAvatar', filterUserAvatar(action.payload.avatar))
      .set('userTimezone', new TimezoneRecord(action.payload.timezone))
      .set('viewAccess', action.payload.viewAccess)
      .set('userRole', action.payload.role);
  });
};

reducers[EnvironmentActionConstants.USER_CAN_EDIT_PERMISSION] = (state: EnvironmentState, action: Action<bool>): EnvironmentState => {
  return state.set('editAccess', action.payload);
};

reducers[EnvironmentActionConstants.USER_DATA_ERROR] = (state: EnvironmentState, action: Action<Error>): EnvironmentState => {
  return state.set('userDataStatus', RequestStatus.STATUS_ERROR).set('userDataRequestError', action.payload);
};

reducers[EnvironmentActionConstants.REQUEST_CAMPAIGN_ID] = (state: EnvironmentState): EnvironmentState => {
  return state.set('campaignIdStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[EnvironmentActionConstants.CAMPAIGN_ID_RECEIVED] = (state: EnvironmentState, action: Action<number>): EnvironmentState => {
  return state.set('campaignIdStatus', RequestStatus.STATUS_LOADED);
};

reducers[EnvironmentActionConstants.CAMPAIGN_ID_ERROR] = (state: EnvironmentState, action: Action<Error>): EnvironmentState => {
  return state.set('campaignIdStatus', RequestStatus.STATUS_ERROR).set('campaignIdRequestError', action.payload);
};

reducers[EnvironmentActionConstants.REQUEST_CAPABILITIES] = (state: EnvironmentState): EnvironmentState => {
  return state.set('capabilitiesStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[EnvironmentActionConstants.CAPABILITIES_RECEIVED] = (state: EnvironmentState, action: Action<Object>): EnvironmentState => {
  return state.set('capabilitiesStatus', RequestStatus.STATUS_LOADED).set('capabilities', fromJS(action.payload));
};

reducers[EnvironmentActionConstants.CAPABILITIES_ERROR] = (state: EnvironmentState, action: Action<Error>): EnvironmentState => {
  return state.set('capabilitiesStatus', RequestStatus.STATUS_ERROR).set('capabilitiesRequestError', action.payload);
};

reducers[EnvironmentActionConstants.REQUEST_COMPANY_DATA] = (state: EnvironmentState): EnvironmentState => {
  return state.set('companyStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[EnvironmentActionConstants.COMPANY_RECEIVED] = (state: EnvironmentState, action: Action<Object>): EnvironmentState => {
  return state.withMutations(EnvironmentState => {
    EnvironmentState.set('companyStatus', RequestStatus.STATUS_LOADED);
    EnvironmentState.set('company', new CompanyRecord(action.payload));
  });
};

reducers[EnvironmentActionConstants.FEATURES_RECEIVED] = (state: EnvironmentState, action: Action<bool>): EnvironmentState => {
  return state.set('multiChannelEnabled', action.payload);
};

reducers[EnvironmentActionConstants.COMPANY_DATA_ERROR] = (state: EnvironmentState, action: Action<Error>): EnvironmentState => {
  return state.withMutations(EnvironmentState => {
    EnvironmentState.set('companyStatus', RequestStatus.STATUS_ERROR);
    EnvironmentState.set('companyRequestError', action.payload);
  });
};

reducers[EnvironmentActionConstants.CLEAR_ENVIRONMENT] = (state: EnvironmentState): EnvironmentState => {
  return setStateFromObject(state, defaultState);
};

reducers[EnvironmentActionConstants.SET_UNSCHEDULED_DRAFT_DATETIME] = (state: EnvironmentState, action: Action<Error>): EnvironmentState => {
  return state.set('unscheduledDraftDatetime', action.payload);
};

reducers[EnvironmentActionConstants.SET_CONFIGURATION] = (state: EnvironmentState, action: Action<Object>): EnvironmentState => {
  return state.set('configuration', action.payload);
};

export default handleActions(reducers, new EnvironmentState());
