/* @flow */
import message, { MessageReducerState } from 'reducers/MessageReducer';
import linkPreviewReducer from 'reducers/LinkPreviewReducer';
import { LinkPreviewState } from 'records/LinkPreviewRecords';
import linkTags from 'reducers/LinkTagsReducer';
import { LinkTagsState } from 'records/LinkTagsRecords';
import linksReducer from 'reducers/LinksReducer';
import { LinksState, UrlPropertiesRecord } from 'records/LinksRecords';
import notesReducer from 'reducers/NotesReducer';
import { NotesState } from 'records/NotesRecords';
import { FacebookState } from 'records/FacebookRecords';
import assignee from 'reducers/AssigneeReducer';
import { AssigneeReducerState } from 'records/AssigneeRecords';
import uiState, { UIStateReducerState } from 'reducers/UIStateReducer';
import environment, { EnvironmentState } from 'reducers/EnvironmentReducer';
import plan, { PlanReducerState, _getCurrentPlan, _getPlans } from 'reducers/PlanReducer';
import videos from 'reducers/VideosReducer';
import { VideosState } from 'records/VideosRecords';
import imageReducer from 'reducers/ImageReducer';
import { ImageReducerState } from 'records/ImageRecords';
import labelsReducer, { LabelsReducerState } from 'reducers/LabelsReducer';
import mentions, { MentionsReducerState } from 'reducers/MentionsReducer';
import facebook, {
    _facebookTargeting,
    _facebookTargetingSelectedLanguages,
    _facebookTargetingLanguageOptions,
    _facebookTargetingIncludedLocations,
    _facebookTargetingLocationOptions,
    _getDarkPostStatus,
    _facebookTargetingSelectedGender
} from 'reducers/FacebookReducer';

import type { ActivitiesRecord } from 'records/MessageRecords';
import type { SupportedNetworksType } from 'constants/ApplicationConstants';
import type { Map as ImmutableMap, List as ImmutableList, OrderedSet as ImmutableOrderedSetType } from 'immutable';
import type { LinkPreviewMediaRecord } from 'records/LinkPreviewRecords';
import type { SocialVideoRecord } from 'records/VideosRecords';
import type { EditorState } from 'draft-js';
import type { RequestStatusType } from 'constants/ApplicationConstants';
import type { AccountsState } from 'records/AccountsRecord';

//sub-reducers
import { _voiceSelectorOpen,
         _sidebarOpen,
         _currentlyDraggingMedia,
         _activePage,
         _previewVisible
       } from 'reducers/UIStateReducer';

import { _getEventAuthorUserId,
        _messageEditorStateMap,
        _messageCurrentlySavingSelector,
        _messageTitleSelector,
        _eventIdSelector,
        _messageIsSaved,
        _messageEditorState,
        _loadingMessageStatusSelector,
        _scheduledDateTimeSelector,
        _scheduledDateTimeSelectorIsValid,
        _messageEditorStateText,
        _getMessageEditorStateMapText,
        _disableApplyToAll,
        _loadingMessageHasCompleted,
        _saveMessageRequestState,
        _savingMessageHasCompleted,
        _selectedNetworks,
        _totalNetworkCount,
        _getMessageStatus,
        _getActivities } from 'reducers/MessageReducer';

import accounts,
  { _voiceSelector,
    _fetchVoicesLoading,
    _checkedCredentialsSelector,
    _accountFilterText,
    _hasCurrentAccountChannelFilter,
    _getCurrentAccountSelectionFilter,
    _getCurrentAccountChannelFilter,
    _searchOnVoices } from 'reducers/AccountsReducer';

import { _urlsMap,
        _urlsList,
        _urlProperties,
        _getLinksForNetworkSelector,
        _getLinkRegexString } from 'reducers/LinksReducer';

import {
    _linkTagsUrls
} from 'reducers/LinkTagsReducer';

import { _linkPreviewSourceMedia,
        _linkPreviewCustomUploadedMedia,
        _linkPreviewAlreadyUploadingMedia,
        _linkPreviewDescription,
        _linkPreviewCaption,
        _linkPreviewTitle,
        _linkPreviewThumbnailUrl,
        _linkPreviewCurrentlyScraping,
        _linkPreviewLinkUrl,
        _linkPreviewVideoSrc,
        _linkPreviewVideoLink,
        _linkPreviewRemoved,
        _linkPreviewCustomThumbError,
        _linkPreviewUploadingCustomThumbErrorText
} from 'reducers/LinkPreviewReducer';

import { _imageUploadStatus,
        _currentImageThumbnail,
        _currentImageFull,
        _imageId,
        _imageCanBeUploaded,
        _imageFilesize,
        _imageMimeType,
        _imageUploadError,
        _imageWidth,
        _imageHeight,
        _hasImageBeenRequested
} from 'reducers/ImageReducer';

import { _getMentionsQueryList,
        _getMentionsQueryUiState,
        _getMentionsQueryStatus,
        _getCurrentMentionsList,
        _getMentionsSaveRequest,
        _getCurrentMentionOffset,
        _getNetworkMentionsList } from 'reducers/MentionsReducer';

import { _firstVideo,
        _videoUploadStatus,
        _videoUploadPercent,
        _defaultVideoThumbnail,
        _videoTranscodeStatus,
        _allVideoFileSizes,
        _allVideoTranscodedFilesizes,
        _videoIdForNetwork,
        _videoDisplayNameForNetwork,
        _videoUploadAndTranscodeError,
        _videoKey,
        _customVideoThumbnail,
        _customVideoThumbnailId,
        _customVideoThumbnailUploadingStatus,
        _customVideoThumbnailUploadingError,
        _getCustomVideoTitle,
        _hasVideoBeenRequested,
        _videoThumbnails,
        _videoThumbnailToDisplay,
        _videoTranscodePercent} from 'reducers/VideosReducer';

import {
  _notesList,
  _currentNoteText,
  _savingNote,
  _hasNotes
} from 'reducers/NotesReducer';

import { _getCsrfTokenStatus,
        _getCsrfToken,
        _getCsrfTokenError,
        _getUserDataStatus,
        _getUserId,
        _getUserFirstName,
        _getUserLastName,
        _getUserAvatar,
        _getUserDataError,
        _getUserTimezone,
        _fetchingUserHasCompleted,
        _capabilitiesMap,
        _getUnscheduledDraftDatetime,
        _getCompanyData,
        _hasFullPublishingAccessFlag,
        _hasViewAccess,
        _hasEditAccess,
        _contentLabelCreationRestricted,
        _initiativeScopingLabels,
        _userRole,
        _userPermissions,
        _featureFlags,
        _isMultiChannelEnabled } from 'reducers/EnvironmentReducer';

import {
  _getAvailableLabels,
  _getSelectedLabels,
  _hasLabels,
  _getCurrentLabelText
} from 'reducers/LabelsReducer';

import type { TimezoneRecord } from 'reducers/EnvironmentReducer';
import type { NoteState } from 'records/NotesRecords';
import { _getAllAssignees,
         _getValidAssignees,
         _getCurrentAssignee,
         _getAllAssigneesError,
         _getAllAssigneesStatus,
         _getValidAssigneesStatus,
         _getValidAssigneesError } from 'reducers/AssigneeReducer';
import type { VideoThumbnailType } from 'reducers/VideosReducer';

export type rootState = {
  assignee: AssigneeReducerState,
  router: Object,
  videos: VideosState,
  message: MessageReducerState,
  uiState: UIStateReducerState,
  environment: EnvironmentState,
  mentions: MentionsReducerState,
  images: ImageReducerState,
  plan: PlanReducerState,
  linkTags: LinkTagsState,
  linkPreview: LinkPreviewState,
  links: LinksState,
  labels: LabelsReducerState,
  notes: NotesState,
  facebook: FacebookState,
  accounts: AccountsState
}

//selectors

export const getAvailableLabels = (state: rootState) => _getAvailableLabels(state.labels);
export const getSelectedLabels = (state: rootState) => _getSelectedLabels(state.labels);
export const hasLabels = (state: rootState) => _hasLabels(state.labels);
export const getCurrentLabelText = (state: rootState) => _getCurrentLabelText(state.labels);

//top level selectors
export const getImages = (state: rootState) => state.images;
export const getMessageReducer = (state: rootState) => state.message;
export const getUIStateReducer = (state: rootState) => state.uiState;

//Plans
export const getCurrentPlan = (state: rootState) => _getCurrentPlan(state.plan);
export const getPlans = (state: rootState) => _getPlans(state.plan);

// UIState
export const voiceSelectorOpen = (state: rootState) => _voiceSelectorOpen(state.uiState);
export const sidebarOpen = (state: rootState) => _sidebarOpen(state.uiState);
export const currentlyDraggingMedia = (state: rootState) => _currentlyDraggingMedia(state.uiState);
export const activePage = (state: rootState) => _activePage(state.uiState);
export const previewVisible = (state: rootState) => _previewVisible(state.uiState);

// Message
export const getEventAuthorUserId = (state:rootState) => _getEventAuthorUserId(state.message);
export const messageEditorStateMap = (state:rootState): ImmutableMap<string, EditorState> => _messageEditorStateMap(state.message);
export const messageCurrentlySavingSelector = (state:rootState) => _messageCurrentlySavingSelector(state.message);
export const messageTitleSelector = (state:rootState) => _messageTitleSelector(state.message);
export const eventIdSelector = (state:rootState) => _eventIdSelector(state.message);
export const messageIsSaved = (state:rootState) => _messageIsSaved(state.message);
export const messageEditorState = (state:rootState, network:string) => _messageEditorState(state.message, network);
export const loadingMessageStatusSelector = (state:rootState) => _loadingMessageStatusSelector(state.message);
export const scheduledDateTimeSelector = (state:rootState) => _scheduledDateTimeSelector(state.message);
export const scheduledDateTimeSelectorIsValid = (state:rootState) => _scheduledDateTimeSelectorIsValid(state.message);
export const messageEditorStateText = (state:rootState, network:string) => _messageEditorStateText(state.message, network);
export const getMessageEditorStateMapText = (state:rootState) => _getMessageEditorStateMapText(state.message);
export const disableApplyToAll = (state:rootState) => _disableApplyToAll(state.message);
export const loadingMessageHasCompleted = (state:rootState) => _loadingMessageHasCompleted(state.message);
export const saveMessageRequestState = (state:rootState) => _saveMessageRequestState(state.message);
export const savingMessageHasCompleted = (state:rootState) => _savingMessageHasCompleted(state.message);
export const selectedNetworks = (state:rootState): Array<string> => _selectedNetworks(state.message);
export const getMessageStatus = (state:rootState):string => _getMessageStatus(state.message);
export const totalNetworkCount = (state:rootState): number => _totalNetworkCount(state.message);
export const getActivities = (state:rootState): ImmutableList<ActivitiesRecord> => _getActivities(state.message);

//Accounts
export const voiceSelector = (state:rootState) => _voiceSelector(state.accounts);
export const fetchVoicesLoading = (state:rootState) => _fetchVoicesLoading(state.accounts);
export const checkedCredentialsSelector = (state:rootState) => _checkedCredentialsSelector(state.accounts);
export const accountFilterText = (state:rootState) => _accountFilterText(state.accounts);
export const getCurrentAccountChannelFilter = (state:rootState) => _getCurrentAccountChannelFilter(state.accounts);
export const hasCurrentAccountChannelFilter = (state:rootState) => _hasCurrentAccountChannelFilter(state.accounts);
export const getCurrentAccountSelectionFilter = (state:rootState) => _getCurrentAccountSelectionFilter(state.accounts);
export const searchOnVoices = (state:rootState) => _searchOnVoices(state.accounts);

//Links
export const urlsList = (state:rootState, network:string):ImmutableList<string> => _urlsList(state.links, network);
export const urlsMap = (state:rootState):ImmutableMap<string, ImmutableList<string>> => _urlsMap(state.links);
export const urlProperties = (state:rootState):ImmutableMap<string, UrlPropertiesRecord> => _urlProperties(state.links);
export const getLinksForNetworkSelector = (state:rootState, network: string) => _getLinksForNetworkSelector(state.links, network);
export const getLinkRegexString = (state:rootState):?RegExp => _getLinkRegexString(state.links);

//Link Tags
export const linkTagsUrls = (state:rootState) => _linkTagsUrls(state.linkTags);

//Link Preview
export const linkPreviewSourceMedia = (state:rootState):ImmutableOrderedSetType<LinkPreviewMediaRecord> => _linkPreviewSourceMedia(state.linkPreview);
export const linkPreviewCustomUploadedMedia = (state:rootState):ImmutableOrderedSetType<LinkPreviewMediaRecord> => _linkPreviewCustomUploadedMedia(state.linkPreview);
export const linkPreviewAlreadyUploadingMedia = (state:rootState):bool => _linkPreviewAlreadyUploadingMedia(state.linkPreview);
export const linkPreviewDescription = (state:rootState):string => _linkPreviewDescription(state.linkPreview);
export const linkPreviewCaption = (state:rootState):string => _linkPreviewCaption(state.linkPreview);
export const linkPreviewTitle = (state:rootState):string => _linkPreviewTitle(state.linkPreview);
export const linkPreviewThumbnailUrl = (state:rootState):string => _linkPreviewThumbnailUrl(state.linkPreview);
export const linkPreviewCurrentlyScraping = (state:rootState):bool => _linkPreviewCurrentlyScraping(state.linkPreview);
export const linkPreviewLinkUrl = (state:rootState):string => _linkPreviewLinkUrl(state.linkPreview);
export const linkPreviewVideoSrc = (state:rootState):string => _linkPreviewVideoSrc(state.linkPreview);
export const linkPreviewVideoLink = (state:rootState):string => _linkPreviewVideoLink(state.linkPreview);
export const linkPreviewRemoved = (state:rootState):bool => _linkPreviewRemoved(state.linkPreview);
export const linkPreviewCustomThumbError = (state:rootState):bool => _linkPreviewCustomThumbError(state.linkPreview);
export const linkPreviewUploadingCustomThumbErrorText = (state:rootState):string => _linkPreviewUploadingCustomThumbErrorText(state.linkPreview);

//Environment
export const getCsrfTokenStatus = (state: rootState): string => _getCsrfTokenStatus(state.environment);
export const getCsrfToken = (state: rootState): string => _getCsrfToken(state.environment);
export const getCsrfTokenError = (state: rootState): ?string => _getCsrfTokenError(state.environment);
export const getUserDataStatus = (state: rootState): string => _getUserDataStatus(state.environment);
export const getUserId = (state: rootState): number => _getUserId(state.environment);
export const getUserFirstName = (state: rootState): string => _getUserFirstName(state.environment);
export const getUserLastName = (state: rootState): string => _getUserLastName(state.environment);
export const getUserAvatar = (state: rootState): string => _getUserAvatar(state.environment);
export const getUserDataError = (state: rootState): ?string => _getUserDataError(state.environment);
export const getUserTimezone = (state: rootState): TimezoneRecord => _getUserTimezone(state.environment);
export const fetchingUserHasCompleted = (state: rootState): TimezoneRecord => _fetchingUserHasCompleted(state.environment);
export const capabilitiesMap = (state: rootState) => _capabilitiesMap(state.environment);
export const getUnscheduledDraftDatetime = (state: rootState) => _getUnscheduledDraftDatetime(state.environment);
export const getCompanyData = (state: rootState) => _getCompanyData(state.environment);
export const hasFullPublishingAccessFlag = (state: rootState) => _hasFullPublishingAccessFlag(state.environment);
export const hasViewAccess = (state: rootState) => _hasViewAccess(state.environment);
export const hasEditAccess = (state: rootState) => _hasEditAccess(state.environment);
export const contentLabelCreationRestricted = (state: rootState) => _contentLabelCreationRestricted(state.environment);
export const initiativeScopingLabels = (state: rootState) => _initiativeScopingLabels(state.environment);
export const userRole = (state: rootState) => _userRole(state.environment);
export const userPermissions = (state: rootState) => _userPermissions(state.environment);
export const isMultiChannelEnabled = (state: rootState) => _isMultiChannelEnabled(state.environment);
export const featureFlags = (state: rootState) => _featureFlags(state.environment);

//Images
export const imageUploadStatus = (state:rootState) => _imageUploadStatus(state.images);
export const currentImageThumbnail = (state:rootState) => _currentImageThumbnail(state.images);
export const currentImageFull = (state:rootState) => _currentImageFull(state.images);
export const imageId = (state:rootState) => _imageId(state.images);
export const imageCanBeUploaded = (state: rootState) => _imageCanBeUploaded(state.images);
export const imageFilesize = (state: rootState) => _imageFilesize(state.images);
export const imageMimeType = (state: rootState) => _imageMimeType(state.images);
export const imageUploadError = (state: rootState) => _imageUploadError(state.images);
export const imageWidth = (state: rootState) => _imageWidth(state.images);
export const imageHeight = (state: rootState) => _imageHeight(state.images);
export const hasImageBeenRequested = (state: rootState):bool => _hasImageBeenRequested(state.images);

//Mentions
export const getMentionsQueryList = (state:rootState) => _getMentionsQueryList(state.mentions);
export const getMentionsQueryUiState = (state:rootState, network: SupportedNetworksType) => _getMentionsQueryUiState(state.mentions, network);
export const getMentionsQueryStatus = (state:rootState) => _getMentionsQueryStatus(state.mentions);
export const getCurrentMentionsList = (state:rootState) => _getCurrentMentionsList(state.mentions);
export const getMentionsSaveRequest = (state:rootState) => _getMentionsSaveRequest(state.mentions);
export const getCurrentMentionOffset = (state:rootState) => _getCurrentMentionOffset(state.mentions);
export const getNetworkMentionsList = (state:rootState,  network: SupportedNetworksType) => _getNetworkMentionsList(state.mentions, network);

//Videos
export const firstVideo = (state: rootState):SocialVideoRecord => _firstVideo(state.videos);
export const videoUploadStatus = (state: rootState) => _videoUploadStatus(state.videos);
export const videoUploadPercent = (state: rootState): number => _videoUploadPercent(state.videos);
export const defaultVideoThumbnail = (state: rootState): ?VideoThumbnailType => _defaultVideoThumbnail(state.videos);
export const videoTranscodeStatus = (state: rootState) => _videoTranscodeStatus(state.videos);
export const allVideoFileSizes = (state: rootState): {[key: string]: number} => _allVideoFileSizes(state.videos);
export const allVideoTranscodedFilesizes = (state: rootState): {[key: string]: number} => _allVideoTranscodedFilesizes(state.videos);
export const videoIdForNetwork = (state: rootState, network:string):number => _videoIdForNetwork(state.videos, network);
export const videoDisplayNameForNetwork = (state: rootState, network:string):string => _videoDisplayNameForNetwork(state.videos, network);
export const videoUploadAndTranscodeError = (state: rootState):string => _videoUploadAndTranscodeError(state.videos);
export const videoKey = (state: rootState):string => _videoKey(state.videos);
export const customVideoThumbnail = (state: rootState):string => _customVideoThumbnail(state.videos);
export const customVideoThumbnailId = (state: rootState):string => _customVideoThumbnailId(state.videos);
export const customVideoThumbnailUploadingStatus = (state: rootState):RequestStatusType => _customVideoThumbnailUploadingStatus(state.videos);
export const customVideoThumbnailUploadingError = (state: rootState):string => _customVideoThumbnailUploadingError(state.videos);
export const getCustomVideoTitle = (state: rootState):string => _getCustomVideoTitle(state.videos);
export const hasVideoBeenRequested = (state: rootState):string => _hasVideoBeenRequested(state.videos);
export const videoThumbnails = (state: rootState): Array<VideoThumbnailType> => _videoThumbnails(state.videos);
export const videoThumbnailToDisplay = (state: rootState): VideoThumbnailType => _videoThumbnailToDisplay(state.videos);
export const videoTranscodePercent = (state: rootState): number => _videoTranscodePercent(state.videos);

//Assignee
export const getCurrentAssignee = (state: rootState) => _getCurrentAssignee(state.assignee);
export const getAllAssigneesError = (state: rootState) => _getAllAssigneesError(state.assignee);
export const getValidAssigneesError = (state: rootState) => _getValidAssigneesError(state.assignee);
export const getAllAssigneesStatus = (state: rootState) => _getAllAssigneesStatus(state.assignee);
export const getValidAssigneesStatus = (state: rootState) => _getValidAssigneesStatus(state.assignee);
export const getAllAssignees = (state: rootState) => _getAllAssignees(state.assignee);
export const getValidAssignees = (state: rootState) => _getValidAssignees(state.assignee);

//Notes
export const notesList = (state: rootState): ImmutableList<NoteState> => _notesList(state.notes);
export const currentNoteText = (state: rootState): string => _currentNoteText(state.notes);
export const savingNote = (state: rootState): bool => _savingNote(state.notes);
export const hasNotes = (state: rootState): bool => _hasNotes(state.notes);

//Facebook
export const facebookTargeting = (state: rootState) => _facebookTargeting(state.facebook);
export const facebookTargetingSelectedLanguages = (state: rootState) => _facebookTargetingSelectedLanguages(state.facebook);
export const facebookTargetingLanguageOptions = (state: rootState) => _facebookTargetingLanguageOptions(state.facebook);
export const facebookTargetingIncludedLocations = (state: rootState) => _facebookTargetingIncludedLocations(state.facebook);
export const facebookTargetingLocationOptions = (state: rootState) => _facebookTargetingLocationOptions(state.facebook);
export const getDarkPostStatus = (state: rootState) => _getDarkPostStatus(state.facebook);
export const facebookTargetingSelectedGender = (state: rootState) => _facebookTargetingSelectedGender(state.facebook);


export const reducers = {
  assignee,
  videos,
  message,
  uiState,
  environment,
  mentions,
  facebook,
  images: imageReducer,
  plan,
  linkPreview: linkPreviewReducer,
  linkTags,
  links: linksReducer,
  labels: labelsReducer,
  notes: notesReducer,
  accounts
};
