/* @flow */
import { handleActions } from 'redux-actions';
import { FacebookState } from 'records/FacebookRecords';
import { TargetingState, GenderState } from 'records/TargetingRecords';
import {
  FacebookActionConstants,
  MessageActionConstants
} from 'constants/ActionConstants';
import {
  LanguageState,
  LocationState
} from 'records/TargetingRecords';
import { TARGETING_SUBTYPES } from 'constants/ApplicationConstants';
import { List as ImmutableList } from 'immutable';

import type { Action } from 'actions/types';
import type { TargetingEntity } from 'adapters/types';

//SELECTORS
export const _facebookTargeting = (state: FacebookState) => state.targeting;
export const _facebookTargetingSelectedLanguages = (state: FacebookState) => state.targeting.language.selectedLanguages;
export const _facebookTargetingLanguageOptions = (state: FacebookState) => state.targeting.language.languageOptions;
export const _facebookTargetingLocationOptions = (state: FacebookState) => state.targeting.location.locationOptions;
export const _facebookTargetingIncludedLocations = (state: FacebookState) => state.targeting.location.includedLocations;
export const _getDarkPostStatus = (state: FacebookState): boolean => state.darkPostStatus;
export const _facebookTargetingSelectedGender = (state: FacebookState): GenderState => state.targeting.gender;

//REDUCERS
let reducers = {};

reducers[FacebookActionConstants.ADD_TARGETING_LANGUAGE] = (state: FacebookState, action: Action<TargetingEntity>): FacebookState => {
  const languageAlreadySelected = state.targeting.language.selectedLanguages.find((language: LanguageState) => language.value === action.payload.value);
  return languageAlreadySelected
  ? state
  : state.setIn(
    ['targeting', 'language', 'selectedLanguages'],
    state.targeting.language.selectedLanguages.push(new LanguageState(action.payload))
  );
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: FacebookState, action: Action<Object>): FacebookState => {
  return new FacebookState();
};

reducers[FacebookActionConstants.REMOVE_TARGETING_LANGUAGE] = (state: FacebookState, action: Action<string>): FacebookState => {
  const indexToRemove = state.targeting.language.selectedLanguages.findIndex((language: LanguageState) => language.description === action.payload);
  return indexToRemove === -1
    ? state
    : state.setIn(
      ['targeting', 'language', 'selectedLanguages'],
      state.targeting.language.selectedLanguages.remove(indexToRemove)
    );
};

reducers[FacebookActionConstants.SET_DARK_POST_VISIBILITY] = (state: FacebookState, action: Action<boolean>): FacebookState => {
  return state.set('darkPostStatus', action.payload);
};

reducers[FacebookActionConstants.SET_GENDER_TARGETING] = (state: FacebookState, action: Action<{value: string, description: string}>): FacebookState => {
  return state.setIn(
    ['targeting', 'gender'],
    new GenderState(action.payload)
  );
};

reducers[FacebookActionConstants.CLEAR_GENDER_TARGETING] = (state: FacebookState): FacebookState => {
  return state.setIn(
    ['targeting', 'gender'],
    new GenderState()
  );
};

reducers[FacebookActionConstants.CLEAR_DARK_POST_VISIBILITY] = (state: FacebookState, action: Action<boolean>): FacebookState => {
  return state.set('darkPostStatus', false);
};

reducers[FacebookActionConstants.READ_FACEBOOK_COMPLETED] = (state: FacebookState, action: Action<FacebookState>): FacebookState => action.payload;

reducers[FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LANGUAGES_COMPLETED] = (state: FacebookState, action: Action<Array<TargetingEntity>>): FacebookState => {
  return state.setIn(['targeting', 'language', 'languageOptions'], new ImmutableList(action.payload.map((language: TargetingEntity) => new LanguageState(language))));
};

reducers[FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LOCATIONS_COMPLETED] = (state: FacebookState, action: Action<Array<TargetingEntity>>): FacebookState => {
  return state.setIn(['targeting', 'location', 'locationOptions'], new ImmutableList(action.payload.map((location: TargetingEntity) => {
    const locationState = new LocationState(location);
    return locationState;
  })));
};

reducers[FacebookActionConstants.ADD_TARGETING_INCLUDED_LOCATION] = (state: FacebookState, action: Action<TargetingEntity>): FacebookState => {
  const alreadySelected: bool = state.targeting.location.includedLocations.some((location: LocationState) => action.payload.description === location.description);
  return alreadySelected
    ? state
    : state.setIn(['targeting', 'location', 'includedLocations'], state.targeting.location.includedLocations.push(new LocationState(action.payload)));
};

reducers[FacebookActionConstants.REMOVE_TARGETING_INCLUDED_LOCATION] = (state: FacebookState, action: Action<string>): FacebookState => {
  const indexToRemove = state.targeting.location.includedLocations.findIndex((location: LocationState) => location.description === action.payload);
  return indexToRemove === -1
    ? state
    : state.setIn(
      ['targeting', 'location', 'includedLocations'],
      state.targeting.location.includedLocations.remove(indexToRemove)
    );
};

reducers[FacebookActionConstants.REMOVE_TARGETING_INCLUDED_COUNTRY] = (state: FacebookState, action: Action<string>): FacebookState => {
  return state.setIn(
    ['targeting', 'location', 'includedLocations'],
    state.targeting.location.includedLocations.filter((location: LocationState) => {
      if (location.subType === TARGETING_SUBTYPES.COUNTRY) {
        return location.description !== action.payload;
      } else {
        return !(
          location.parentTargetingValue &&
          location.parentTargetingValue.description === action.payload &&
          location.parentTargetingValue.subType === TARGETING_SUBTYPES.COUNTRY
        );
      }
    })
  );
};

reducers[FacebookActionConstants.CLEAR_TARGETING] = (state: FacebookState): FacebookState => state.set('targeting', new TargetingState());

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: FacebookState): FacebookState => new FacebookState();

export default handleActions(reducers, new FacebookState());
