/* @flow */
import { handleActions } from 'redux-actions';

import { AccountsActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { AccountsState } from 'records/AccountsRecord';
import { RequestStatus } from 'constants/ApplicationConstants';
import {
  Map as ImmutableMap,
  List as ImmutableList } from 'immutable';

import type { CredentialRecord } from 'records/AccountsRecord';
import type { Action } from 'actions/types';

//SELECTORS
export const _voiceSelector = (state:AccountsState) => state.voices;
export const _getCurrentAccountChannelFilter = (state:AccountsState): ImmutableList<string> => state.currentAccountChannelFilter;
export const _hasCurrentAccountChannelFilter = (state:AccountsState): bool => state.currentAccountChannelFilter.size > 0;
export const _getCurrentAccountSelectionFilter = (state:AccountsState): string => state.currentAccountSelectionFilter;
export const _fetchVoicesLoading = (state:AccountsState) => state.fetchVoicesRequestState === RequestStatus.STATUS_REQUESTED;
export const _checkedCredentialsSelector = (state:AccountsState) => state.checkedCredentials;
export const _searchOnVoices = (state:AccountsState) => state.searchOnVoices;
export const _accountFilterText = (state:AccountsState) => state.currentAccountFilterText;

//REDUCERS
let reducers = {};

reducers[AccountsActionConstants.FETCH_VOICES] = (state: AccountsState, action): AccountsState => {
  return state.withMutations(newState => {
    newState.set('voices', action.payload)
      .set('fetchVoicesRequestState', RequestStatus.STATUS_LOADED);
  });
};

reducers[AccountsActionConstants.VOICE_CRED_TOGGLE] = (state: AccountsState, action: Action<ImmutableMap<string, CredentialRecord>>): AccountsState => {
  return state.set('checkedCredentials', action.payload);
};

reducers[AccountsActionConstants.QUICK_CREDENTIAL_SELECT] = (state: AccountsState, action: Action<ImmutableMap<string, CredentialRecord>>): AccountsState => {
  return state.set('checkedCredentials', action.payload);
};

reducers[AccountsActionConstants.REMOVE_NETWORK_VOICES] = (state: AccountsState, action: Action<string>): AccountsState => {
  const voiceTree = state.checkedCredentials.filter(cred => cred.socialNetwork !== action.payload);
  return state.set('checkedCredentials', voiceTree);
};

reducers[AccountsActionConstants.UPDATE_FILTER_CHANNELS] = (state: AccountsState, action: Action<string>): AccountsState => {
  if (state.currentAccountChannelFilter.contains(action.payload)) {
    const indexOfNetworkToRemove = state.currentAccountChannelFilter.indexOf(action.payload);
    return state.set('currentAccountChannelFilter', state.currentAccountChannelFilter.delete(indexOfNetworkToRemove));
  } else if (!action.payload) {
    return state.set('currentAccountChannelFilter', ImmutableList());
  } else {
    return state.set('currentAccountChannelFilter', state.currentAccountChannelFilter.push(action.payload));
  }
};

reducers[AccountsActionConstants.UPDATE_EXCLUSIVE_FILTER_CHANNEL] = (state: AccountsState, action: Action<string>): AccountsState => {
  return state.set('currentAccountChannelFilter', ImmutableList([action.payload]));
};

reducers[AccountsActionConstants.UPDATE_FILTER_SELECTION] = (state: AccountsState, action: Action<string>): AccountsState => {
  return state.set('currentAccountSelectionFilter', action.payload);
};

reducers[AccountsActionConstants.UPDATE_FILTER_TEXT] = (state: AccountsState, action: Action<string>): AccountsState => {
  return state.set('currentAccountFilterText', action.payload);
};

reducers[AccountsActionConstants.SEARCH_ON_VOICES_TOGGLE] = (state: AccountsState, action: Action<void>): AccountsState => {
  return state.set('searchOnVoices', !state.get('searchOnVoices'));
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: AccountsState, action: Action<void>): AccountsState => {
  return state.set('checkedCredentials', ImmutableMap());
};

export default handleActions(reducers, new AccountsState());
