/* @flow */
import { handleActions } from 'redux-actions';
import {
  LinkTagsActionConstants } from 'constants/ActionConstants';
import { RequestStatus } from 'constants/ApplicationConstants';
import { LinkTagsState, LinkTagPropertiesRecord, TagVariableValue } from 'records/LinkTagsRecords';
import {
  fromJS,
  Map as ImmutableMap } from 'immutable';

//SELECTORS
export const _linkTagsUrls = (state:LinkTagsState) => state.urls;

//REDUCERS
let reducers = {};

reducers[LinkTagsActionConstants.CREATE_LINK_TAG_START] = (state: LinkTagsState, {payload}): LinkTagsState => {
  let newState = state;
  if (!state.getIn(['urls', payload.link])) {
    newState = state.setIn(['urls', payload.link], ImmutableMap());
  }
  return newState.setIn(['urls', payload.link, payload.network], new LinkTagPropertiesRecord({}));
};

reducers[LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.mergeIn(['urls', payload.link, payload.network], { ...payload.linkTag, enabled: true });
};

reducers[LinkTagsActionConstants.CREATE_LINK_TAG_FAILED] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.mergeIn(['urls', payload.link, payload.network], {
    enabled: false
  });
};

reducers[LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.updateIn(['urls', payload.link, payload.network], linkTag => {
    const tagVariableValue = linkTag.get('variableValues').find(tagVariable => tagVariable.get('variableId') !== payload.tagVariableId);
    const newTagVariableValue = new TagVariableValue({
      variableId: payload.tagVariableId,
      values: fromJS(payload.tagVariableValue)
    });

    if (!newTagVariableValue.equals(tagVariableValue)) {
      let newLinkTagVariableValues = linkTag.get('variableValues').filter(tagVariable => tagVariable.get('variableId') !== payload.tagVariableId);
      newLinkTagVariableValues = newLinkTagVariableValues.push(newTagVariableValue);
      return linkTag.merge({
        variableValues: newLinkTagVariableValues,
        saveStatus: RequestStatus.STATUS_NEVER_REQUESTED
      });
    }

    return state;
  });
};

reducers[LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE_VALUES] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.mergeIn(['urls', payload.link, payload.network], {
    variableValues: fromJS(payload.newValues),
    saveStatus: RequestStatus.STATUS_NEVER_REQUESTED
  });
};

reducers[LinkTagsActionConstants.SAVE_LINK_TAG_START] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.mergeIn(['urls', payload.link, payload.network], {
    saveStatus: RequestStatus.STATUS_REQUESTED
  });
};

reducers[LinkTagsActionConstants.SAVE_LINK_TAG_COMPLETED] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.mergeIn(['urls', payload.link, payload.network], {
    saveStatus: RequestStatus.STATUS_LOADED
  });
};

reducers[LinkTagsActionConstants.SAVE_LINK_TAG_FAILED] = (state: LinkTagsState, {payload}): LinkTagsState => {
  return state.mergeIn(['urls', payload.link, payload.network], {
    saveStatus: RequestStatus.STATUS_ERROR
  });
};

export default handleActions(reducers, new LinkTagsState());