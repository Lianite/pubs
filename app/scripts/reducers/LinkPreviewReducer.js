/* @flow */
import { handleActions } from 'redux-actions';
import { OrderedSet as ImmutableOrderedSet } from 'immutable';
import { LinkPreviewActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { LinkPreviewState, LinkPreviewMediaRecord } from 'records/LinkPreviewRecords';
import { RequestStatus } from 'constants/ApplicationConstants';

import type { Action } from 'actions/types';

//SELECTORS
export const _linkPreviewSourceMedia = (state:LinkPreviewState): ImmutableOrderedSet<LinkPreviewMediaRecord> => state.sourceMedia;
export const _linkPreviewCustomUploadedMedia = (state:LinkPreviewState): ImmutableOrderedSet<LinkPreviewMediaRecord> => state.customUploadedMedia;
export const _linkPreviewAlreadyUploadingMedia = (state:LinkPreviewState): bool => {
  return state.uploadingCustomThumbStatus === RequestStatus.STATUS_REQUESTED;
};
export const _linkPreviewDescription = (state:LinkPreviewState): string => state.description;
export const _linkPreviewCaption = (state:LinkPreviewState): string => state.caption;
export const _linkPreviewTitle = (state:LinkPreviewState): string => state.title;
export const _linkPreviewThumbnailUrl = (state:LinkPreviewState): string => state.thumbnailUrl;
export const _linkPreviewCurrentlyScraping = (state:LinkPreviewState): bool => state.currentlyScraping;
export const _linkPreviewLinkUrl = (state:LinkPreviewState): string => state.linkUrl;
export const _linkPreviewVideoSrc = (state:LinkPreviewState): string => state.videoSrc;
export const _linkPreviewVideoLink = (state:LinkPreviewState): string => state.videoLink;
export const _linkPreviewRemoved = (state:LinkPreviewState): bool => state.removed;
export const _linkPreviewCustomThumbError = (state:LinkPreviewState): bool => (state.uploadingCustomThumbStatus === RequestStatus.STATUS_ERROR);
export const _linkPreviewUploadingCustomThumbErrorText = (state:LinkPreviewState): string => state.uploadingCustomThumbError;

//REDUCERS
let reducers = {};

reducers[LinkPreviewActionConstants.UPDATE_LINK_PREVIEW] = (state: LinkPreviewState, action): LinkPreviewState => {
  return new LinkPreviewState(action.payload);
};

reducers[LinkPreviewActionConstants.UPDATE_LINK_PREVIEW_IMAGES] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.merge({
    sourceMedia: action.payload.sourceMedia,
    customUploadedMedia: action.payload.customUploadedMedia
  });
};

reducers[LinkPreviewActionConstants.REMOVE_LINK_PREVIEW] = (state: LinkPreviewState, action): LinkPreviewState => {
  return new LinkPreviewState({removed: true});
};

reducers[LinkPreviewActionConstants.EDIT_LINK_PREVIEW] = (state: LinkPreviewState, action): LinkPreviewState => {
  let tmpPayload = action.payload;

  if (typeof tmpPayload.currentlyScraping === 'undefined') {
    tmpPayload.currentlyScraping = false;
  }

  return state.merge(tmpPayload);
};

reducers[LinkPreviewActionConstants.SCRAPING_LINK_PREVIEW] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.set('currentlyScraping', true);
};

reducers[LinkPreviewActionConstants.CANCEL_LINK_SCRAPE] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.set('currentlyScraping', false);
};

reducers[LinkPreviewActionConstants.REQUEST_UPLOAD_CUSTOM_THUMB] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.merge({
    uploadingCustomThumbStatus: RequestStatus.STATUS_REQUESTED,
    uploadingCustomThumbError: ''
  });
};

reducers[LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_SUCCEEDED] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.set('uploadingCustomThumbStatus', RequestStatus.STATUS_LOADED);
};

reducers[LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_FAILED] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.merge({
    uploadingCustomThumbStatus: RequestStatus.STATUS_ERROR,
    uploadingCustomThumbError: action.payload});
};

reducers[LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_CANCELED] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.set('uploadingCustomThumbStatus', RequestStatus.STATUS_CANCELED);
};

reducers[LinkPreviewActionConstants.UPDATE_IMAGE_THUMBNAIL] = (state: LinkPreviewState, action: Action<Object>): LinkPreviewState => {
  return state.set('thumbnailUrl', action.payload);
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: LinkPreviewState, action): LinkPreviewState => {
  return new LinkPreviewState();
};

reducers[LinkPreviewActionConstants.READ_LINK_PREVIEW_COMPLETED] = (state: LinkPreviewState, action): LinkPreviewState => {
  return new LinkPreviewState({
    linkUrl: action.payload.linkUrl,
    thumbnailUrl: action.payload.thumbnailUrl,
    description: action.payload.description,
    caption: action.payload.caption,
    title: action.payload.title
  });
};
reducers[LinkPreviewActionConstants.CLEAR_ERROR] = (state: LinkPreviewState, action): LinkPreviewState => {
  return state.merge({
    uploadingCustomThumbStatus: RequestStatus.STATUS_NEVER_REQUESTED,
    uploadingCustomThumbError: ''
  });
};

export default handleActions(reducers, new LinkPreviewState());
