/* @flow */
import { handleActions } from 'redux-actions';
import { AssigneeActionConstants } from 'constants/ActionConstants';
import { List as ImmutableList } from 'immutable';
import { RequestStatus } from 'constants/ApplicationConstants';
import { setStateFromObject } from 'utils/ReducerUtils';
import { AssigneeRecord, AssigneeReducerState } from 'records/AssigneeRecords';
import type { RequestStatusType } from 'constants/ApplicationConstants';
import type { Action } from 'actions/types';
import type { AssigneeData } from 'adapters/types';

//SELECTORS
export const _getCurrentAssignee = (state: AssigneeReducerState) => state.currentAssignee.toObject();
export const _getAllAssigneesError = (state: AssigneeReducerState) => state.allAssigneesError;
export const _getValidAssigneesError = (state: AssigneeReducerState) => state.valideAssigneesError;
export const _getAllAssigneesStatus = (state: AssigneeReducerState) => state.allAssigneesStatus;
export const _getValidAssigneesStatus = (state: AssigneeReducerState) => state.validAssigneesStatus;
export const _getValidAssignees = (state: AssigneeReducerState) => state.validAssignees;
export const _getAllAssignees = (state: AssigneeReducerState) => state.allAssignees;

//REDUCERS
let reducers = {};

reducers[AssigneeActionConstants.UPDATE_CURRENT_ASSIGNEE] = (state: AssigneeReducerState, action: Action<AssigneeData>): AssigneeReducerState => {
  return state.set('currentAssignee', new AssigneeRecord(action.payload));
};

reducers[AssigneeActionConstants.ALL_ASSIGNEES_RECEIVED] = (state: AssigneeReducerState, action: Action<Array<AssigneeData>>): AssigneeReducerState => {
  const data = ImmutableList(action.payload.map(assignee => new AssigneeRecord(assignee)));
  return state.merge({
    allAssigneesStatus: RequestStatus.STATUS_LOADED,
    allAssignees: data
  });
};

reducers[AssigneeActionConstants.VALID_ASSIGNEES_RECEIVED] = (state: AssigneeReducerState, action: Action<Array<AssigneeData>>): AssigneeReducerState => {
  const data = ImmutableList(action.payload.map(assignee => new AssigneeRecord(assignee)));
  return state.merge({
    validAssigneesStatus: RequestStatus.STATUS_LOADED,
    validAssignees: data
  });
};

reducers[AssigneeActionConstants.FETCH_ASSIGNEE_VOICES] = (state: AssigneeReducerState, action): AssigneeReducerState => {
  state = state.set('fetchAssigneesVoicesStatus', RequestStatus.STATUS_LOADED);
  const updatedAllAssignees = state.get('allAssignees').map((assignee) => {
    if (assignee.id === action.payload.id) {
      return assignee.set('voices', action.payload.voices);
    } else {
      return assignee;
    }
  });
  return state.set('allAssignees', updatedAllAssignees);
};

reducers[AssigneeActionConstants.FETCH_CONFLICTING_ASSIGNEE_VOICES] = (state: AssigneeReducerState, action): AssigneeReducerState => {
  state = state.set('fetchAssigneesVoicesStatus', RequestStatus.STATUS_LOADED);
  const updatedAllAssignees = state.get('allAssignees').map((assignee) => {
    if (assignee.id === action.payload.id) {
      return assignee.set('conflictingVoices', action.payload.conflictingVoices);
    } else {
      return assignee;
    }
  });
  return state.set('allAssignees', updatedAllAssignees);
};

reducers[AssigneeActionConstants.REQUEST_ALL_ASSIGNEES] = (state: AssigneeReducerState, action: Action<RequestStatusType>): AssigneeReducerState => {
  return state.set('allAssigneesStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[AssigneeActionConstants.REQUEST_VALID_ASSIGNEES] = (state: AssigneeReducerState, action: Action<RequestStatusType>): AssigneeReducerState => {
  return state.set('validAssigneesStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_STARTED] = (state: AssigneeReducerState, action: Action<Error>): AssigneeReducerState => {
  return state.set('fetchAssigneesVoicesStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[AssigneeActionConstants.ALL_ASSIGNEES_ERROR] = (state: AssigneeReducerState, action: Action<Error>): AssigneeReducerState => {
  return state.set('allAssigneesStatus', RequestStatus.STATUS_ERROR).set('allAssigneesError', action.payload);
};

reducers[AssigneeActionConstants.VALID_ASSIGNEES_ERROR] = (state: AssigneeReducerState, action: Action<Error>): AssigneeReducerState => {
  return state.set('validAssigneesStatus', RequestStatus.STATUS_ERROR).set('valideAssigneesError', action.payload);
};

reducers[AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_FAILED] = (state: AssigneeReducerState, action: Action<Error>): AssigneeReducerState => {
  return state.set('fetchAssigneesVoicesStatus', RequestStatus.STATUS_ERROR).set('fetchAssigneesVoicesError', action.payload);
};

reducers[AssigneeActionConstants.CLEAR_ASSIGNEE] = (state: AssigneeReducerState, action): AssigneeReducerState => {
  return setStateFromObject(state, AssigneeReducerState, ['validAssignees', 'allAssignees']);
};

export default handleActions(reducers, new AssigneeReducerState());
