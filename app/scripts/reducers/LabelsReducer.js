/* @flow */
import { handleActions } from 'redux-actions';
import { LabelActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { fromJS, Record, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { setStateFromObject } from 'utils/ReducerUtils';

import _ from 'lodash';

const defaultState = {
  selectedLabels: ImmutableList(),
  availableLabels: ImmutableMap(),
  currentLabelText: ''
};

export class Label extends Record({
  id: '',
  title: ''
}) {
  id: string;
  title: string;
};

export class LabelsReducerState extends Record(_.cloneDeep(defaultState)) {
  selectedLabels: ImmutableList<Label>;
  availableLabels: ImmutableMap<string, Label>;
  groupViolation: bool;
}

//SELECTORS
export const _getAvailableLabels = (state: LabelsReducerState) => state.availableLabels;
export const _getSelectedLabels = (state: LabelsReducerState) => state.selectedLabels;
export const _hasLabels = (state: LabelsReducerState) => state && state.selectedLabels.size > 0;
export const _getCurrentLabelText = (state: LabelsReducerState) => state.currentLabelText;
export const _hasLabelGroupViolation = (state: LabelsReducerState) => state.groupViolation;

//REDUCERS
let reducers = {};

reducers[LabelActionConstants.ADD_LABEL] = (state: LabelsReducerState, action): LabelsReducerState => {
  const selectedLabels = state.get('selectedLabels').toJS();

  let labelAlreadySelected = false;

  _.forEach(selectedLabels, (label) => {
    if (label.title === action.payload.title) {
      labelAlreadySelected = true;
    }
  });

  if (labelAlreadySelected || !action.payload.title || action.payload.title.length === 0) {
    return state;
  } else {
    selectedLabels.push(action.payload);

    return state.set('selectedLabels', fromJS(selectedLabels));
  }
};

reducers[LabelActionConstants.REMOVE_LABEL] = (state: LabelsReducerState, action): LabelsReducerState => {
  let selectedLabels = state.get('selectedLabels').toJS();

  for (let i = 0; i < selectedLabels.length; i++) {
    if (selectedLabels[i].title === action.payload) {
      selectedLabels.splice(i, 1);
      break;
    }
  }

  return state.set('selectedLabels', fromJS(selectedLabels));
};

reducers[LabelActionConstants.LABELS_LOADED] = (state: LabelsReducerState, action): LabelsReducerState => {
  return state.set('availableLabels', fromJS(action.payload));
};

reducers[LabelActionConstants.LABELS_READ_COMPLETE] = (state: LabelsReducerState, action): LabelsReducerState => {
  return state.set('selectedLabels', fromJS(action.payload));
};

reducers[LabelActionConstants.APPEND_NEW_LABEL] = (state: LabelsReducerState, action): LabelsReducerState => {
  return state.set('availableLabels', state.get('availableLabels').push(fromJS(action.payload)).sort((a, b) => {
    return a.get('title') > b.get('title');
  }));
};

reducers[LabelActionConstants.UPDATE_LABEL_TEXT] = (state: LabelsReducerState, action): LabelsReducerState => {
  return state.set('currentLabelText', action.payload);
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: LabelsReducerState, action): LabelsReducerState => {
  return setStateFromObject(state, defaultState, ['availableLabels']);
};

export default handleActions(reducers, new LabelsReducerState());
