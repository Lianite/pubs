/* @flow */
import { handleActions } from 'redux-actions';
import {
  Map as ImmutableMap,
  Set as ImmutableSet } from 'immutable';
import { LinksState } from 'records/LinksRecords';
import { getTotalUrlList, buildRegexpFromArray } from 'utils/LinkUtils';
import { LinksActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import type {
  UrlsType,
  UrlPropertiesType } from 'records/LinksRecords';

//SELECTORS
export const _urlsMap = (state:LinksState):UrlsType => state.urls;
export const _urlsList = (state: LinksState, network: string) => state.urls.get(network);
export const _urlProperties = (state:LinksState):UrlPropertiesType => state.urlProperties;

export const _getLinksForNetworkSelector = (state: LinksState, network: string) : Array<string> => {
  let urlProperties = state.urlProperties;
  let links = [];
  urlProperties.forEach( (urlPropertyRecord, url, map) => {
    if (urlPropertyRecord.get('associatedNetworks').has(network)) {
      links.push(url);
    }
  });

  return links;
};

export const _getLinkRegexString = (state:LinksState): ?RegExp => {
  const totalUrlList = getTotalUrlList(state.urls);
  return buildRegexpFromArray(ImmutableSet(totalUrlList).toArray());
};

//REDUCERS
let reducers = {};

reducers[LinksActionConstants.UPDATE_FOUND_URLS] = (state: LinksState, action): LinksState => {
  return state.set('urls', ImmutableMap(action.payload));
};

reducers[LinksActionConstants.UPDATE_URL_PROPERTIES] = (state: LinksState, action): LinksState => {
  return state.set('urlProperties', ImmutableMap(action.payload));
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: LinksState, action): LinksState => {
  return new LinksState();
};


export default handleActions(reducers, new LinksState());
