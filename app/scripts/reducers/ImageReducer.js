/* @flow */
import { handleActions } from 'redux-actions';
import { MediaActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { RequestStatus, MediaMimeTypeConstants } from 'constants/ApplicationConstants';
import { setStateFromObject } from 'utils/ReducerUtils';
import { getUploadStatus } from 'utils/ApplicationUtils';
import { ImageReducerState, defaultState } from 'records/ImageRecords';

import type { Action } from 'actions/types';

//SELECTORS
export const _imageUploadStatus = (state: ImageReducerState) => getUploadStatus(state.imageUploadStatus);
export const _imageCanBeUploaded = (state: ImageReducerState) => {
  return state.imageUploadStatus === RequestStatus.STATUS_NEVER_REQUESTED || state.imageUploadStatus === RequestStatus.STATUS_CANCELED;
};

export const _currentImageThumbnail = (state: ImageReducerState):string => state.imageThumbUrl;
export const _currentImageFull = (state: ImageReducerState):string => state.imageFullUrl;
export const _imageId = (state: ImageReducerState):?number => state.imageId;
export const _imageFilesize = (state: ImageReducerState):number => state.imageFilesize;
export const _imageMimeType = (state: ImageReducerState):typeof MediaMimeTypeConstants => state.imageMimeType;
export const _imageUploadError = (state: ImageReducerState):string => state.imageUploadError;
export const _imageWidth = (state: ImageReducerState):number => state.imageWidth;
export const _imageHeight = (state: ImageReducerState):number => state.imageHeight;
export const _hasValidImage = (state: ImageReducerState):bool => !!state.imageId && state.imageUploadStatus === RequestStatus.STATUS_LOADED;
export const _hasImageBeenRequested = (state: ImageReducerState):bool => state.imageUploadStatus !== RequestStatus.STATUS_NEVER_REQUESTED && state.imageUploadStatus !== RequestStatus.STATUS_CANCELED;
//REDUCERS
let reducers = {};

reducers[MediaActionConstants.REVERT_TO_DEFAULT] = (state: ImageReducerState, action: Action<?any>): ImageReducerState => {
  return new ImageReducerState();
};

reducers[MediaActionConstants.REQUEST_UPLOAD_IMAGE] = (state: ImageReducerState, action: Action<?any>): ImageReducerState => {
  return state.set('imageUploadStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[MediaActionConstants.UPLOAD_IMAGE_SUCCEEDED] = (state: ImageReducerState, action: Action<{
  id:number, fullUrl:string, thumbUrl:string, filesize: number, fileType: string, mimeType: string, imageWidth: number, imageHeight: number}>): ImageReducerState => {
  return state.merge({
    imageUploadStatus: RequestStatus.STATUS_LOADED,
    imageId: action.payload.id,
    imageFullUrl: action.payload.fullUrl,
    imageThumbUrl: action.payload.thumbUrl,
    imageFilesize: action.payload.filesize,
    imageMimeType: action.payload.mimeType,
    imageWidth: action.payload.imageWidth,
    imageHeight: action.payload.imageHeight
  });
};

reducers[MediaActionConstants.UPLOAD_IMAGE_FAILED] = (state: ImageReducerState, action: Action<string>): ImageReducerState => {
  return state.merge({
    imageUploadStatus: RequestStatus.STATUS_ERROR,
    imageUploadError: action.payload
  });
};

reducers[MediaActionConstants.READ_IMAGES_COMPLETED] = (state: ImageReducerState, action): ImageReducerState => {
  //NOTE: when we make this reducer's state a List, we'll want to modify this to accept the entire array
  const imagesById = action.payload[0];
  const newRecord = new ImageReducerState({
    imageId: imagesById.id,
    imageFullUrl: imagesById.originalUrl,
    imageThumbUrl: imagesById.thumbnailUrl,
    imageMimeType: imagesById.mimeType,
    imageFilesize: imagesById.imageSizeAfterResizing,
    imageUploadStatus: RequestStatus.STATUS_LOADED,
    imageWidth: imagesById.width,
    imageHeight: imagesById.height
  });


  return state.merge(newRecord);
};

reducers[MediaActionConstants.UPLOAD_IMAGE_CANCELED] = (state: ImageReducerState, action: Action<string>): ImageReducerState => {
  return state.set('imageUploadStatus', RequestStatus.STATUS_CANCELED);
};
reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: ImageReducerState): ImageReducerState => {
  return setStateFromObject(state, defaultState);
};

export default handleActions(reducers, new ImageReducerState());
