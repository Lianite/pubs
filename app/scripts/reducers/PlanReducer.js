/* @flow */
import { handleActions } from 'redux-actions';
import { PlanActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { StoredPlan } from 'records/PlanRecords';
import { fromJS, Record, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { RequestStatus } from 'constants/ApplicationConstants';
import type { PlanList, Plan } from 'adapters/types';
import type { Action } from 'actions/types';
import { setStateFromObject } from 'utils/ReducerUtils';

import _ from 'lodash';

const defaultState = {
  plansData: ImmutableList(),
  plansDataStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  plansDataError: '',
  currentPlan: ImmutableMap({
    name: '',
    id: '',
    color: ''
  })
};

export class PlanReducerState extends Record(_.cloneDeep(defaultState)) {
  currentPlan: StoredPlan;
}

//SELECTORS
export const _getCurrentPlan = (state: PlanReducerState) => state.currentPlan.toObject();
export const _getPlans = (state: PlanReducerState): Array<Plan> => state.plansData.toArray();
export const _getPlansDataStatus = (state: PlanReducerState): ?string => state.plansDataStatus;
export const _getPlansDataError = (state: PlanReducerState): ?string => state.plansDataError;

//REDUCERS
let reducers = {};

reducers[PlanActionConstants.UPDATE_CURRENT_PLAN] = (state: PlanReducerState, action): PlanReducerState => {
  return state.set('currentPlan', fromJS(action.payload));
};

reducers[PlanActionConstants.PLAN_DATA_RECEIVED] = (state: PlanReducerState, action: Action<PlanList>): PlanReducerState => {
  let data = ImmutableList(action.payload);
  return state.set('plansDataStatus', RequestStatus.STATUS_LOADED).set('plansData', data);
};

reducers[PlanActionConstants.PLAN_DATA_ERROR] = (state: PlanReducerState, action: Action<Error>): PlanReducerState => {
  return state.set('plansDataStatus', RequestStatus.STATUS_ERROR).set('plansDataError', action.payload);
};

reducers[PlanActionConstants.REQUEST_PLAN_DATA] = (state: PlanReducerState): PlanReducerState => {
  return state.set('plansDataStatus', RequestStatus.STATUS_REQUESTED);
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: PlanReducerState): PlanReducerState => {
  return setStateFromObject(state, defaultState, ['plansData']);
};

export default handleActions(reducers, new PlanReducerState());
