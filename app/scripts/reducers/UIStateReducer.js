/* @flow */
import { handleActions } from 'redux-actions';
import { Record } from 'immutable';
import { UIStateActionConstants } from 'constants/ActionConstants';
import { SidebarPages } from 'constants/UIStateConstants';
import { setStateFromObject } from 'utils/ReducerUtils';

import type { Action } from 'actions/types';

import _ from 'lodash';

const defaultState = {
  sidebarOpen: true,
  activePage: SidebarPages.BASICS,
  voiceSelectorOpen: false,
  currentlyDraggingMedia: false,
  previewVisible: false
};

export class UIStateReducerState extends Record(_.cloneDeep(defaultState)) {
  sidebarOpen: bool
  activePage: string
  voiceSelectorOpen: bool
  currentlyDraggingMedia: bool
  previewVisible: bool
}

//SELECTORS
export const _voiceSelectorOpen = (state: UIStateReducerState): bool => state.voiceSelectorOpen;
export const _sidebarOpen = (state: UIStateReducerState): bool => state.sidebarOpen;
export const _currentlyDraggingMedia = (state: UIStateReducerState): bool => state.currentlyDraggingMedia;
export const _activePage = (state: UIStateReducerState): string => state.activePage;
export const _previewVisible = (state: UIStateReducerState): bool => state.previewVisible;

//REDUCERS
let reducers = {};

reducers[UIStateActionConstants.OPEN_SIDEBAR] = (state: UIStateReducerState, action: Action<string>): UIStateReducerState => {
  return state.set('sidebarOpen', true).set('activePage', action.payload);
};

reducers[UIStateActionConstants.CLOSE_SIDEBAR] = (state: UIStateReducerState): UIStateReducerState => {
  return state.set('sidebarOpen', false).set('activePage', '');
};

reducers[UIStateActionConstants.TOGGLE_VOICE_SELECTOR] = (state: UIStateReducerState, action): UIStateReducerState => {
  if (action.payload === 'focus') {
    return state.set('voiceSelectorOpen', true);
  } else if (action.payload === 'blur') {
    return state.set('voiceSelectorOpen', false);
  } else {
    return state.set('voiceSelectorOpen', !state.get('voiceSelectorOpen'));
  }
};

reducers[UIStateActionConstants.DRAG_STATE_CHANGED] = (state: UIStateReducerState, action: Action<bool>): UIStateReducerState => {
  return state.set('currentlyDraggingMedia', action.payload);
};

reducers[UIStateActionConstants.CLEAR_UI_STATE] = (state: UIStateReducerState): UIStateReducerState => {
  return setStateFromObject(state, defaultState);
};

reducers[UIStateActionConstants.PREVIEW_VISIBILITY_CHANGED] = (state: UIStateReducerState, action: Action<bool>): UIStateReducerState => {
  return state.set('previewVisible', action.payload);
};

export default handleActions(reducers, new UIStateReducerState());
