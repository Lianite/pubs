/* @flow */
import { handleActions } from 'redux-actions';
import { NotesActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { NoteState, NotesState } from 'records/NotesRecords';
import { List as ImmutableList } from 'immutable';

import type { Note } from 'adapters/types';

//SELECTORS
export const _notesList = (state: NotesState): ImmutableList<NoteState>  => state.notesList;
export const _currentNoteText = (state: NotesState): string => state.currentNoteText;
export const _savingNote = (state: NoteState): bool => state.savingNote;
export const _hasNotes = (state: NotesState): bool => state && state.notesList.size > 0;

//REDUCERS
let reducers = {};

reducers[NotesActionConstants.READ_NOTES_COMPLETED] = (state: NotesState, action): NotesState => {
  if (!action.payload.notesList) {
    return state;
  }
  const notesArray: Array<NoteState> = action.payload.notesList.map((note: Note): NoteState => new NoteState({
    authorFirstName: note.authorFirstName,
    authorLastName: note.authorLastName,
    authorAvatarThumbnail: note.authorAvatarThumbnail,
    noteCreationTimestamp: note.noteCreationTimestamp,
    noteText: note.noteText
  }));
  const notesList: ImmutableList<NoteState> = ImmutableList(notesArray);
  return state.set('notesList', notesList);
};

reducers[NotesActionConstants.ADD_NOTE] = (state: NotesState, action): NotesState => {
  return state.merge({
    notesList: state.get('notesList').push(new NoteState(action.payload)),
    currentNoteText: ''
  });
};

reducers[NotesActionConstants.CURRENT_NOTE_TEXT_CHANGED] = (state: NotesState, action): NotesState => {
  return state.merge({
    currentNoteText: action.payload.text
  });
};

reducers[NotesActionConstants.SAVE_NOTE_REQUEST_STARTED] = (state: NotesState, action): NotesState => {
  return state.set('savingNote', true);
};

reducers[NotesActionConstants.SAVE_NOTE_REQUEST_COMPLETED] = (state: NotesState, action): NotesState => {
  return state.set('savingNote', false);
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: NoteState, action): NotesState => new NotesState();

export default handleActions(reducers, new NotesState());
