/* @flow */
import _ from 'lodash';
import { handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { createDeepEqualSelector } from 'utils/SelectorUtils';
import { MediaActionConstants, MessageActionConstants } from 'constants/ActionConstants';
import { DefaultNetwork, RequestStatus } from 'constants/ApplicationConstants';
import { List as ImmutableList, Set as ImmutableSet } from 'immutable';
import { SocialVideoRecord, VideosState } from 'records/VideosRecords';

import type { Action } from 'actions/types';
import type {
  PostSocialVideoResponse,
  TranscodingProgressResponse,
  TranscodingResultsResponse } from 'apis/MediaApi';
import type { RequestStatusType } from 'constants/ApplicationConstants';

export type VideoFileSizeMapType = {[key: string]: number};

//SELECTORS
const _allVideoFileSizesInternal = (state:VideosState): VideoFileSizeMapType => {
  let videoSizesPerNetwork = {};
  videoSizesPerNetwork = state.socialVideos.reduce((accumulator: VideoFileSizeMapType, video: SocialVideoRecord): VideoFileSizeMapType => {
    if (video && video.targetService) {
      accumulator[video.targetService] = video.videoSize;
    }
    return accumulator;
  }, videoSizesPerNetwork);

  return videoSizesPerNetwork;
};

export const _allVideoFileSizes = createDeepEqualSelector(_allVideoFileSizesInternal, (sizes: VideoFileSizeMapType): VideoFileSizeMapType => sizes);

const _allVideoTranscodedFilesizesInternal = (state:VideosState): VideoFileSizeMapType => {
  let videoSizesPerNetwork = {};

  state.socialVideos.reduce( (accumulator: VideoFileSizeMapType, video: SocialVideoRecord): VideoFileSizeMapType => {
    if (video && video.targetService) {
      accumulator[video.targetService] = video.transcodedVideoSize;
    }
    return accumulator;
  }, videoSizesPerNetwork);

  return videoSizesPerNetwork;
};

export const _allVideoTranscodedFilesizes = createDeepEqualSelector(_allVideoTranscodedFilesizesInternal, (sizes: VideoFileSizeMapType): VideoFileSizeMapType => sizes);

export const _videoForNetwork = (state:VideosState, network: string): SocialVideoRecord => {
  let videoToReturn = new SocialVideoRecord();

  if (state.socialVideos && state.socialVideos.size > 0) {
    const possibleVideos = state.socialVideos.filter( (video:SocialVideoRecord):bool => {
      return video.targetService === network;
    });

    //NOTE: We have to update this if we support multiple different videos for a network, since
    //      this is a list
    if (possibleVideos.size > 0) {
      videoToReturn = possibleVideos.first();
    } else if (possibleVideos.size === 0 && network === DefaultNetwork) {
      //In this case, we only have videos for a specific network, but we want one for the default network,
      // most likely because we originally uploaded the video with networks, and then removed them before saving
      // In this case, just use the first video.
      videoToReturn = state.socialVideos.first();
    }
  }

  return videoToReturn;
};


export const _firstVideo = (state:VideosState):SocialVideoRecord => {
  if (_.get(state, 'socialVideos', []).size > 0) {
    return state.socialVideos.first();
  } else {
    return new SocialVideoRecord();
  }
};

export type VideoThumbnailType = {|
  uri: string,
  id: string
|};

export const _videoUploadStatus = createSelector(_firstVideo, (video:SocialVideoRecord):RequestStatusType => video.uploadStatus);
export const _videoUploadPercent = createSelector(_firstVideo, (video:SocialVideoRecord):number => video.uploadPercent);
export const _defaultVideoThumbnail = createSelector(_firstVideo, (video:SocialVideoRecord): ?VideoThumbnailType => video.thumbnails ? video.thumbnails.first() : undefined);
export const _videoTranscodeStatus = createSelector(_firstVideo, (video:SocialVideoRecord):RequestStatusType => video.transcodeProgressStatus);
export const _hasVideoBeenRequested = createSelector(_firstVideo, (video:SocialVideoRecord):bool => video.uploadStatus !== RequestStatus.STATUS_NEVER_REQUESTED && video.uploadStatus !== RequestStatus.STATUS_CANCELED);
export const _videoThumbnails = createSelector(_firstVideo, (video:SocialVideoRecord):Array<VideoThumbnailType> => video.thumbnails.toArray());
export const _customVideoThumbnailId = (state: VideosState): string => state.customThumbnailId;
export const _getCustomVideoTitle = (state: VideosState): string => state.customVideoTitle;
export const _customVideoThumbnail = (state: VideosState): string => state.customThumbnailUri;
export const _customVideoThumbnailUploadingStatus = (state: VideosState): RequestStatusType => state.customThumbnailUploadStatus;
export const _customVideoThumbnailUploadingError = (state: VideosState): string => state.customThumbnailUploadError;
export const _videoIdForNetwork = createSelector(_videoForNetwork, (video:SocialVideoRecord):number => video.id);
export const _videoDisplayNameForNetwork = createSelector(_videoForNetwork, (video:SocialVideoRecord):string => video.displayName);
export const _videoUploadAndTranscodeError = (state: VideosState): string => state.videoUploadAndTranscodeError;
export const _videoKey = (state: VideosState): string => state.videoKey;

export const _videoTranscodePercent = (state: VideosState): number => {
  let percentComplete: number = 0;

  if (state.socialVideos && state.socialVideos.size > 0) {
    percentComplete = state.socialVideos.reduce((percent:number, video: SocialVideoRecord): number => {
      return percent > video.transcodeProgress ? video.transcodeProgress : percent;
    }, 100);
  }

  return percentComplete;
};

export const _videoThumbnailToDisplay = createSelector(
  [_firstVideo, _customVideoThumbnailId, _customVideoThumbnail],
  (video:SocialVideoRecord, customId: string, customUri:string):VideoThumbnailType => {
    if (customId && customUri) {
      return {
        id: customId,
        uri: customUri
      };
    } else {
      const index = video.thumbnailIndexSelected;
      const possiblyChangedThumb = index !== -1 && index >= 0 && index < video.thumbnails.size ? video.thumbnails.get(index) : undefined;
      const defaultThumb = (video.thumbnails && video.thumbnails.size > 0) ? video.thumbnails.first() : {id: '', uri: ''};

      return possiblyChangedThumb ? possiblyChangedThumb : defaultThumb;
    }
  }
);

//REDUCERS
let reducers = {};

reducers[MediaActionConstants.REQUEST_UPLOAD_VIDEO] = (state: VideosState): VideosState => {
  const socialVideos = ImmutableSet([new SocialVideoRecord({
    uploadStatus: RequestStatus.STATUS_REQUESTED,
    uploadPercent: 0
  })]);
  return state.set('socialVideos', socialVideos);
};

reducers[MediaActionConstants.UPLOAD_VIDEO_PROGESS] = (state: VideosState, action: Action<number>): VideosState => {
  return state.set('socialVideos', state.socialVideos.map( (video: SocialVideoRecord): SocialVideoRecord => {
    return video.merge({
      uploadPercent: action.payload
    });
  }));
};

reducers[MediaActionConstants.REQUEST_COPY_VIDEO] = (state: VideosState): VideosState => {

  //clear out thumbnails and transcoding status
  return state.set('socialVideos', state.socialVideos.map( (video: SocialVideoRecord): SocialVideoRecord => {
    return video.merge({
      thumbnails:ImmutableList(),
      transcodeProgressStatus: RequestStatus.STATUS_NEVER_REQUESTED,
      transcodeProgress: 0,
      transcodeResultsStatus: RequestStatus.STATUS_NEVER_REQUESTED
    });
  }));
};

reducers[MediaActionConstants.COPY_VIDEO_SUCCEEDED] = (state: VideosState, action: Action<PostSocialVideoResponse>): VideosState => {
  const newSocialVideos = ImmutableSet(action.payload.socialVideos.map(video => {
    const props = _.omit(video, ['size', 'videoThumbnail', 'videoStatus']);
    return new SocialVideoRecord({
      ...props,
      videoSize: video.size,
      uploadStatus: RequestStatus.STATUS_LOADED
    });
  }));
  let updatedSocialVideos = state.socialVideos;
  newSocialVideos.forEach( (video: SocialVideoRecord) => {
    if (state.socialVideos.every( (existingVideo: SocialVideoRecord): bool => {
      return ((existingVideo.id !== video.id) || (existingVideo.id === video.id && existingVideo.targetService !== video.targetService));
    })) {
      updatedSocialVideos = updatedSocialVideos.add(video);
    }
  });
  return state.set('socialVideos', updatedSocialVideos);
};

reducers[MediaActionConstants.UPLOAD_VIDEO_SUCCEEDED] = (state: VideosState, action: Action<PostSocialVideoResponse>): VideosState => {
  state = state.set('videoKey', action.payload.videoKey);
  const socialVideos = action.payload.socialVideos.map(video => {
    const props = _.omit(video, ['size', 'videoThumbnail', 'videoStatus']);
    return new SocialVideoRecord({
      ...props,
      videoSize: video.size,
      uploadStatus: RequestStatus.STATUS_LOADED
    });
  });
  return state.set('socialVideos', ImmutableSet(socialVideos));
};

reducers[MediaActionConstants.REQUEST_TRANSCODE_VIDEO_PROGRESS] = (state: VideosState): VideosState => {
  let socialVideos = state.socialVideos;
  socialVideos = socialVideos.map(video => video.set('transcodeProgressStatus', RequestStatus.STATUS_REQUESTED));
  return state.set('socialVideos', socialVideos);
};

reducers[MediaActionConstants.UPDATE_TRANSCODE_VIDEO_PROGRESS] = (state: VideosState, action: Action<TranscodingProgressResponse>): VideosState => {
  let socialVideos = state.get('socialVideos');
  socialVideos = socialVideos.map(video => video.set('transcodeProgress', action.payload[video.id]));
  return state.set('socialVideos', socialVideos);
};

reducers[MediaActionConstants.TRANSCODE_VIDEO_SUCCEEDED] = (state: VideosState): VideosState => {
  let socialVideos = state.socialVideos;
  socialVideos = socialVideos.map(video => video.set('transcodeProgressStatus', RequestStatus.STATUS_LOADED));
  return state.set('socialVideos', socialVideos);
};

reducers[MediaActionConstants.REQUEST_TRANSCODE_VIDEO_RESULTS] = (state: VideosState): VideosState => {
  let socialVideos = state.socialVideos;
  socialVideos = socialVideos.map(video => video.set('transcodeResultsStatus', RequestStatus.STATUS_REQUESTED));
  return state.set('socialVideos', socialVideos);
};

reducers[MediaActionConstants.TRANSCODE_VIDEO_RESULTS_SUCCEEDED] = (state: VideosState, action: Action<TranscodingResultsResponse>): VideosState => {
  let socialVideos = state.socialVideos;
  socialVideos = socialVideos.map(video => {
    const videoResults = _.find(action.payload, {videoId: video.get('id')});
    if (videoResults) {
      const props = _.omit(videoResults, ['videoStatus', 'videoId', 'size']);
      return video.merge({
        ...props,
        transcodedVideoSize: videoResults.size,
        thumbnails: ImmutableList(videoResults.thumbnails),
        transcodeResultsStatus: RequestStatus.STATUS_LOADED,
        thumbnailIndexSelected: -1
      });
    }
  });
  return state.set('socialVideos', socialVideos);
};

reducers[MediaActionConstants.UPLOAD_VIDEO_FAILED] = (state: VideosState, action: Action<string>): VideosState => {
  const socialVideos = state.socialVideos.map( (video:SocialVideoRecord) => {
    return video.set('uploadStatus', RequestStatus.STATUS_ERROR);
  });
  state = state.set('socialVideos', socialVideos);
  return state.set('videoUploadAndTranscodeError', action.payload);
};

reducers[MediaActionConstants.UPLOAD_VIDEO_CANCELED] = (state: VideosState): VideosState => {
  const socialVideos = state.socialVideos.map( (video:SocialVideoRecord) => {
    return video.merge({
      uploadStatus: RequestStatus.STATUS_CANCELED,
      uploadPercent: 0,
      transcodeProgressStatus: RequestStatus.STATUS_CANCELED,
      transcodeResultsStatus: RequestStatus.STATUS_CANCELED
    });
  });
  const newState = state.set('socialVideos', socialVideos);

  //Clear out the video key so we don't think we can re-transcode
  return newState.set('videoKey', '');
};

reducers[MediaActionConstants.SET_CUSTOM_VIDEO_TITLE] = (state: VideosState, action: Action<string>): VideosState => {
  return state.set('customVideoTitle', action.payload);
};

reducers[MediaActionConstants.REQUEST_UPLOAD_VIDEO_THUMB] = (state: VideosState): VideosState => {
  return state.merge({
    customThumbnailUploadStatus: RequestStatus.STATUS_REQUESTED,
    customThumbnailId: '',
    customThumbnailUri: ''
  });
};

type CustomVideoThumbInfo = {|
  id: string;
  uri: string;
|};

reducers[MediaActionConstants.UPLOAD_VIDEO_THUMB_SUCCEEDED] = (state: VideosState, action: Action<CustomVideoThumbInfo>): VideosState => {
  return state.merge({
    customThumbnailId: action.payload.id,
    customThumbnailUri: action.payload.uri,
    customThumbnailUploadStatus: RequestStatus.STATUS_LOADED
  });
};

reducers[MediaActionConstants.UPLOAD_VIDEO_THUMB_FAILED] = (state: VideosState, action: Action<string>): VideosState => {
  return state.merge({
    customThumbnailUploadStatus: RequestStatus.STATUS_ERROR,
    customThumbnailUploadError: action.payload
  });
};

reducers[MediaActionConstants.UPLOAD_VIDEO_THUMB_CANCELED] = (state: VideosState): VideosState => {
  return state.set('customThumbnailUploadStatus', RequestStatus.STATUS_CANCELED);
};

reducers[MediaActionConstants.REMOVE_VIDEO_THUMB] = (state: VideosState): VideosState => {
  return state.merge({
    customThumbnailUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
    customThumbnailId: '',
    customThumbnailUri: '',
    customThumbnailUploadError: ''
  });
};

reducers[MediaActionConstants.VIDEO_THUMBNAIL_FRAME_CHANGED] = (state: VideosState, action: Action<VideoThumbnailType>): VideosState => {
  //update the index of the selected frame if it is in the thumbnails list
  const socialVideos = state.socialVideos.map( (video:SocialVideoRecord) => {
    const newIndex = video.thumbnails.findIndex((thumbnail: VideoThumbnailType): bool => {
      return thumbnail.id === action.payload.id;
    });
    if (newIndex !== -1 && newIndex !== video.thumbnailIndexSelected) {
      return video.merge({
        thumbnailIndexSelected: newIndex
      });
    } else {
      return video;
    }
  });
  return state.merge({
    socialVideos,
    customThumbnailId: '',
    customThumbnailUri: '',
    customThumbnailUploadError: '',
    customThumbnailUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
  });
};

reducers[MediaActionConstants.REMOVE_UPLOADED_VIDEO] = (state: VideosState): VideosState => {
  return new VideosState();
};

reducers[MessageActionConstants.CLEAR_MESSAGE] = (state: VideosState, action): VideosState => {
  return new VideosState();
};

reducers[MediaActionConstants.READ_VIDEOS_COMPLETED] = (state: VideosState, action: Action<Object>): VideosState => {
  return new VideosState(action.payload);
};

export default handleActions(reducers, new VideosState());
