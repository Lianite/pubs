/* @flow */
import React, {Component} from 'react';
import Dropzone from 'react-dropzone';
import styles from 'styles/modules/components/LinkPreview.scss';
import { linkPreviewText } from 'constants/UITextConstants';
import classNames from 'classnames/bind';
import { LoadingAnimation } from '@spredfast/react-lib';
import ImageError from 'components/media/ImageError';
import _ from 'lodash';
import { buildUrlHref } from 'utils/LinkUtils';

import type { PublishingError } from 'records/ErrorRecords';

const cx = classNames.bind(styles);

let dropzoneRef;

type LinkPreviewState = {
  hovered?: bool
};

type LinkPreviewProps = {
  currentlyDraggingMedia:bool,
  description: string,
  caption: string,
  captionUrl: string,
  title: string,
  networks: Array<string>,
  thumbnailUrl: string,
  onDropFile?: ((file:File) => void),
  isAlreadyUploading: bool,
  onRemove: Function,
  cycleThumbnailForward: Function,
  cycleThumbnailBackward: Function,
  enableCarousel: bool,
  onEdit: Function,
  onCancelUpload: Function,
  onAcknowledgeError: Function,
  linkPreviewLoading: bool,
  onScrapeCancel: Function,
  linkUrl: string,
  onRefreshClicked: Function,
  error: ?PublishingError
};

type LinkPreviewExtendedProps = {
  currentlyDraggingMedia:bool,
  description: string,
  caption: string,
  captionUrl: string,
  title: string,
  networks: Array<string>,
  thumbnailUrl: string,
  onDropFile?: ((file:File) => void),
  isAlreadyUploading: bool,
  onRemove: Function,
  cycleThumbnailForward: Function,
  cycleThumbnailBackward: Function,
  enableCarousel: bool,
  onEdit: Function,
  onCancelUpload: Function,
  onAcknowledgeError: Function,
  linkPreviewLoading: bool,
  onScrapeCancel: Function,
  linkUrl: string,
  onRefreshClicked: Function,
  error: PublishingError,
  onMouseOver: Function,
  onMouseOut: Function,
  hovered: bool
};

type ThumbnailProps = {
  image: string,
  cycleThumbnailForward: Function,
  cycleThumbnailBackward: Function,
  enableCarousel: bool
};

type LinkPreviewDetailsProps = {
  description: string,
  title: string,
  onChange: Function
};

type CloseButtonProps = {
  onClick: Function
};

type NetworkListProps = {
  networks: Array<string>
};

type CustomizeThumbnailProps = {
  disabled: bool
};

const Thumbnail = ({image, cycleThumbnailBackward, cycleThumbnailForward, enableCarousel}: ThumbnailProps) => {
  const thumbnailStyles = cx({
    thumbnail: true,
    multiImage: enableCarousel
  });
  return (
    <div className={thumbnailStyles}>
      <img src={image} alt='link-preview-thumbnail' />
      <div className={styles.cycleImagesBackward} onClick={cycleThumbnailBackward}>
        <div>
          <i className='rad-icon-chevron_down' />
        </div>
      </div>
      <div className={styles.cycleImagesForward} onClick={cycleThumbnailForward}>
        <div>
          <i className='rad-icon-chevron_up' />
        </div>
      </div>
    </div>
  );
};

const ThumbnailHolder = (props:{children?:any, onCloseClicked: Function}) => {
  if (props.children && _.some(props.children, (child):bool => !!child )) {
    return (<div className={styles.thumbnailHolder}>
      <span className='icon-remove' onClick={props.onCloseClicked}></span>
      {props.children}
    </div>);
  } else {
    return (
      <div className={styles.thumbnailHolder}>
        <p>
          <i className='icon-no-image' />
          <span>{linkPreviewText.imageHolder}</span>
        </p>
      </div>
    );
  }
};

const customizeThumbnailClicked = () => {
  if (dropzoneRef) {
    dropzoneRef.open();
  }
};

const CustomizeThumbnail = (props: CustomizeThumbnailProps) => {
  //Eventually, we might make this a popup where you can select images from your
  // computer or from content center. For right now just make it a link-style
  // button
  return (
    <div className={styles.customizeThumbnailHolder}>
      <button disabled={props.disabled} onClick={customizeThumbnailClicked} className={styles.customizeThumbnail}>
        {linkPreviewText.customizeThumbnail}
      </button>
    </div>
  );
};

const LinkPreviewDetails = ({title, description, onChange}: LinkPreviewDetailsProps) => {
  const onChangeDescription = (event) => {
    onChange({
      description: event.target.textContent
    });
  };

  const onChangeTitle = (event) => {
    onChange({
      title: event.target.textContent
    });
  };

  return (
    <div className={styles.linkPreviewDetails}>
      <div className={styles.row}>
        <h3
          contentEditable
          suppressContentEditableWarning
          data-place-holder={linkPreviewText.titleHolder}
          onBlur={onChangeTitle}>{title}</h3>
        <i className='rad-icon-edit'/>
      </div>
      <div className={styles.row}>
        <p
          contentEditable
          suppressContentEditableWarning
          data-place-holder={linkPreviewText.descriptionHolder}
          onBlur={onChangeDescription}>{description}</p>
        <i className='rad-icon-edit'/>
      </div>
    </div>
  );
};

const CloseButton = ({onClick}: CloseButtonProps) => {
  return (
    <button type='button' className={styles.closeButton} onClick={onClick}>
      <span className='icon-modal_close'></span>
    </button>
  );
};

const NetworkList = ({networks}: NetworkListProps) => {
  networks = networks.map(network => network.toLowerCase());
  return (
    <p className={styles.networks}>
      {networks.map(network => <i key={network} className={`rad-icon-network-${network} ${styles.icon} ${styles[network]}`} />)}
    </p>
  );
};

const onDrop = (onDropCallback, files: Array<File>) => {
  if (onDropCallback && files && files.length > 0) {
    onDropCallback(files[0]); //If we are going to handle multiple link preview custom thumb uploads, need to change this
  }
};

const buildPreviewLoading = (onScrapeCancel) => {
  return (
    <div className={styles.linkPreviewLoading}>
      <LoadingAnimation />
      <span className={cx({
        italicize: true,
        gray: true
      })}>{linkPreviewText.scrapingPreview}</span>
      <a className={styles.link} onClick={onScrapeCancel}>Cancel</a>
    </div>
  );
};

const buildLinkPreview = (props: LinkPreviewExtendedProps) => {
  const isAlreadyUploading = props.isAlreadyUploading;
  const error = props.error;
  const draggingOverlay = props.currentlyDraggingMedia ?
    <div className={styles.draggingMediaOverlay}></div> : null;

  const acceptedImageTypes = isAlreadyUploading ? 'none' : 'image/*';

  const imageErrorComponent = error ? <ImageError title={error.title} description={error.description} /> : null;

  return (
    <Dropzone ref = {node => {
      dropzoneRef = node;
    }}
      onDrop={onDrop.bind(null, props.onDropFile)}
      className={styles.customThumbDropzone}
      activeClassName={styles.customThumbDropAccept}
      rejectClassName={styles.customThumbDropReject}
      multiple={false}
      disableClick
      accept={acceptedImageTypes}>
      <div className={styles.leftColumn} onMouseOver={props.onMouseOver} onMouseLeave={props.onMouseOut}>
        {draggingOverlay}
        {props.thumbnailUrl && !isAlreadyUploading && !props.error ?
          <Thumbnail image={props.thumbnailUrl} cycleThumbnailBackward={props.cycleThumbnailBackward} cycleThumbnailForward={props.cycleThumbnailForward} enableCarousel={props.enableCarousel} isAlreadyUploading={isAlreadyUploading} /> :
          <ThumbnailHolder onCloseClicked={error ? props.onAcknowledgeError : props.onCancelUpload}>
            {isAlreadyUploading ? <LoadingAnimation /> : null}
            {imageErrorComponent}
          </ThumbnailHolder>
        }
        <CustomizeThumbnail disabled={isAlreadyUploading}/>
      </div>
    </Dropzone>
  );
};

const buildPreviewDetails = (props: LinkPreviewExtendedProps) => {
  return (
    <div className={styles.rightColumn} onMouseOver={props.onMouseOver} onMouseLeave={props.onMouseOut}>
      <div className={styles.topContainer}>
        <LinkPreviewDetails title={props.title} description={props.description} onChange={props.onEdit}/>
        <CloseButton onClick={props.onRemove}/>
      </div>
      <div className={styles.bottomContainer}>
        <p className={styles.caption}><a href={buildUrlHref(props.captionUrl)} target='_blank'>{props.caption}</a></p>
        <p className={styles.caption}>{props.caption}</p>
        <div className={cx({
          networkRefreshContainer: true,
          hover: props.hovered
        })}>
          <NetworkList networks={props.networks}/>
          <div className={'rad-icon-refresh ' + styles.refresh} onClick={() => {
            props.onRefreshClicked(props.linkUrl);
          }}></div>
        </div>
      </div>
    </div>
  );
};

export default class LinkPreview extends Component<void, LinkPreviewProps, any> {
  state: LinkPreviewState;
  static displayName = 'LinkPreview';

  constructor(props: LinkPreviewProps) {
    super(props);

    this.state = {
      hovered: false
    };
    (this:any).onMouseOver = (this:any).onMouseOver.bind(this);
    (this:any).onMouseOut = (this:any).onMouseOut.bind(this);
  }

  onMouseOver = () => {
    this.setState({hovered: true});
  }

  onMouseOut = () => {
    this.setState({hovered: false});
  }

  render = () => {
    const loading = this.props.linkPreviewLoading;

    const passDownProps = Object.assign({}, this.props, {
      onMouseOver: this.onMouseOver,
      onMouseOut: this.onMouseOut,
      hovered: this.state.hovered
    });
    return (
      <div className={styles.linkPreview}>
        {loading ? buildPreviewLoading(this.props.onScrapeCancel) : buildLinkPreview(passDownProps)}
        {loading ? null : buildPreviewDetails(passDownProps)}
      </div>
    );
  }
};
