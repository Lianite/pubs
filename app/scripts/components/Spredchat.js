/* eslint-disable spredfast/no-default-export */
import React, { PropTypes } from 'react';
import SpredchatNoteMeta from '@spredfast/react-lib/lib/SpredchatNoteMeta';
import { getConversationsEnviromentDetails } from 'utils/ApiUtils';

const { string } = PropTypes;

// https://github.com/spredfast/spredchatui#react
export default function Spredchat({ id, name, companyId, campaignId }) {
  if (!id || !name) {
    return null;
  }

  let environmentUrl = getConversationsEnviromentDetails().environmentUrl;

  return (
    <div id='spredchat-ui'>
      {/* NOTE: this is a workaround to an assumption that spredchat makes. Currently it looks
        for global nav before rendering out sprechat data */}
      <div id='global-nav' style={{display: 'none'}}></div>
      <SpredchatNoteMeta
        id={id}
        name={name}
        type='publishing'
        url={`${environmentUrl}/planner/index/company_id/${companyId}/campaign_id/${campaignId}/calendar?messageSidePanel=MESSAGE%3A${id}&mode=week_view`}
      />
    </div>
  );
}

Spredchat.propTypes = {
  id: string.isRequired,
  name: string.isRequired,
  companyId: string.isRequired,
  campaignId: string.isRequired
};

Spredchat.displayName = 'Spredchat';
