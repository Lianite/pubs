/* @flow */
import React, {Component} from 'react';
import Dropzone from 'react-dropzone';
import MessageTitleBox from 'components/MessageTitleBox';
import OptionsSidebarContainer from 'containers/OptionsSidebarContainer';
import MessageEditorContainer from 'containers/MessageEditorContainer';
import MessagePublishBar from 'components/MessagePublishBar';
import GlobalValidationBar from 'components/GlobalValidationBar';
import ScheduleContainer from 'containers/ScheduleContainer';
import Spredchat from 'components/Spredchat';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { POST_PREVIEW_OVERLAY_TRANSITION_TIMING } from 'constants/UILayoutConstants';
import { validationText } from 'constants/UITextConstants';
import styles from 'styles/modules/components/Main.scss';

import type { Plan } from 'adapters/types';
import type Moment from 'moment';
import type { MessageValidationData } from 'selectors/ValidationSelector';

import GrowlerReduxHost from '@spredfast/react-lib/lib/growler-notification/redux/GrowlerReduxHost';
import '@spredfast/react-lib/styles/GrowlerHost.scss';

type Props = {
  eventId: ?number,
  messageTitle: string,
  scheduledDatetime: ?Moment,
  canPublish: bool,
  canSaveDraft: bool,
  validation: MessageValidationData,
  messageHasId: bool,
  companyId: string,
  campaignId: string,
  cancelVerbage: string,
  previewVisible: bool,
  hasEditAccess: bool,
  currentPlan: Plan,
  canUseMultichannelPubs: bool,
  onTitleChange: () => void,
  onScheduleClicked: () => void,
  dragStateChanged?: () => bool,
  onCancelClicked: () => void,
  initializeAppData: () => void
};

type State = {
  canUseMultichannelPubs: bool
}

export default class Main extends Component<void, Props, State> {
  static displayName = 'Main';
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      canUseMultichannelPubs: true
    };

    //bind
    (this:any).onDragEnter = (this:any).onDragEnter.bind(this);
    (this:any).onDragLeave = (this:any).onDragLeave.bind(this);
    (this:any).onDrop = (this:any).onDrop.bind(this);
  }

  componentWillMount() {
    this.props.initializeAppData();
  }

  onDragEnter() {
    if (this.props.dragStateChanged) {
      this.props.dragStateChanged(true);
    }
  }

  onDragLeave() {
    if (this.props.dragStateChanged) {
      this.props.dragStateChanged(false);
    }
  }

  onDrop() {
    if (this.props.dragStateChanged) {
      this.props.dragStateChanged(false);
    }
  }

  renderSpredchat() {
    if (this.props.eventId && this.props.messageTitle) {
      return (
        <Spredchat
          id={this.props.eventId.toString()}
          name={this.props.messageTitle}
          companyId={this.props.companyId}
          campaignId={this.props.campaignId}
        />
      );
    }
  }

  renderPreviewOverlay() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear
        transitionAppearTimeout={POST_PREVIEW_OVERLAY_TRANSITION_TIMING}
        transitionEnterTimeout={POST_PREVIEW_OVERLAY_TRANSITION_TIMING}
        transitionLeaveTimeout={POST_PREVIEW_OVERLAY_TRANSITION_TIMING}
        transitionName={{
          appear: styles.postPreviewToggleAppear,
          appearActive: styles.postPreviewToggleAppearActive,
          enter: styles.postPreviewToggleEnter,
          enterActive: styles.postPreviewToggleEnterActive,
          leave: styles.postPreviewToggleLeave,
          leaveActive: styles.postPreviewToggleLeaveActive
        }}>
        {
          this.props.previewVisible ?
            <div className={styles.postPreviewOverlay}></div> :
            null
        }
      </ReactCSSTransitionGroup>
    );
  };

  renderPermissionBlockingOverlay() {
    if (!this.props.canUseMultichannelPubs) {
      return (
        <div className={styles.permissionBlockingOverlay}>
          <div className={styles.permissionBlockingContent}>
            <i className={`${styles.permissionBlockingIcon} icon-access-empty`}/>
            <p className={styles.permissionBlockingHeader}>{validationText.cannotUseMultiPubs}</p>
            <p className={styles.permissionBlockingSubtitle}>
              {validationText.haveAnyQuestions} <a href='mailto:support@spredfast.com'>{validationText.contactCustomerSupport}</a>
            </p>
          </div>
        </div>
      );
    }
  };

  render() {
    const {
      messageTitle,
      onTitleChange,
      validation,
      onScheduleClicked,
      currentPlan
    } = this.props;

    return (
      <div id='publishing-ui-app-container'>

        {this.renderPermissionBlockingOverlay()}

        <Dropzone
          className={styles.globalDropZone}
          onDragEnter={this.onDragEnter}
          onDragLeave={this.onDragLeave}
          onDrop={this.onDrop}
          disableClick
        >
          {this.renderSpredchat()}

          <GrowlerReduxHost />
          <div className={styles.appContainer}>
            <GlobalValidationBar
              validation={validation}
            />
            <MessageTitleBox
              messageTitle={messageTitle}
              onChange={onTitleChange}
              currentPlanColor={currentPlan.color}
              closeButtonClicked={this.props.onCancelClicked}
              canUseMultichannelPubs={this.props.canUseMultichannelPubs}
            />

            <div className={styles.sidebarMessageSchedulerContainer}>
              <OptionsSidebarContainer />
              <MessageEditorContainer />

              <ScheduleContainer />

            </div>
            <MessagePublishBar
              publishImmediately={!this.props.scheduledDatetime}
              onScheduleClicked={onScheduleClicked}
              canUseMultichannelPubs={this.props.canUseMultichannelPubs}
              canPublish={this.props.canPublish}
              canSaveDraft={this.props.canSaveDraft}
              onCancelClicked={this.props.onCancelClicked}
              cancelVerbage={this.props.cancelVerbage}
              hasEditAccess={this.props.hasEditAccess} />
          </div>
        </Dropzone>
        {this.renderPreviewOverlay()}
      </div>
    );
  }
}
