/* @flow */
import React, { Component } from 'react';
import styles from 'styles/modules/components/message/NetworkOptionsBar.scss';
import { networkOptionsBarText } from 'constants/UITextConstants';
import { TextInput } from '@spredfast/react-lib';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

type Props = {
  customVideoTitle: string,
  setCustomVideoTitle: ((customTitle: string) => void)
}

type State = {
  expanded: bool
}

class NetworkOptionsBar extends Component<void, Props, State> {
  state: State;

  constructor(props: Props): void {
    super(props);

    this.state = {
      expanded: false
    };

    (this:any).expandBar = this.expandBar.bind(this);
  };

  expandBar(): void {
    this.setState({
      expanded: !this.state.expanded
    });
  }

  render() {

    const tabBarStyle = cx({
      tabBar: true,
      expanded: this.state.expanded
    });

    const contentBarStyle = cx({
      contentBar: true,
      expanded: this.state.expanded
    });

    const videoTitleStyle = cx({
      videoTitle: true,
      expanded: this.state.expanded
    });

    const tabBarMessage = this.state.expanded ? (<span className={styles.tabBarMessage}>{networkOptionsBarText.updateTitlePlaceholder}</span>) : null;
    const content = (<div className={videoTitleStyle}>
      <TextInput
        value={this.props.customVideoTitle}
        placeholder={networkOptionsBarText.addTitlePlaceholder}
        onDebouncedChange={e => {
          this.props.setCustomVideoTitle(e.target.value);
        }}
        debounceInterval={200}
      />
    </div>);

    return (
      <div className={styles.barWrapper}>
        <div className={tabBarStyle}>
          {tabBarMessage}
          <button className={styles.customVideoTitleButton} onClick={this.expandBar}>
            <div className='icon-video-title'></div>
            <span>Video Title</span>
          </button>
        </div>
        <div className={contentBarStyle}>
          {content}
        </div>
      </div>
    );
  }
};

export default NetworkOptionsBar;
