/* @flow */
import React, { Component } from 'react';
import styles from 'styles/modules/components/MentionsQuery.scss';

import {EditorState} from 'draft-js';
import { LoadingAnimation } from '@spredfast/react-lib';
import MentionQueryRow from 'components/MentionQueryRow.js';

import { MentionsActionConstants } from 'constants/ActionConstants';
import { mentionsQueryMenuText } from 'constants/UITextConstants';
import { DECORATOR_TYPES, getCompositeDecorator } from 'decorators/EditorDecorator';

import type { List as ImmutableList } from 'immutable';
import type { MentionsRecord } from 'types/MentionsTypes';
import type { CompositeDecorator } from 'draft-js';

type Props = {
  mentionsQueryList: ImmutableList<MentionsRecord>,
  sendUpdatedEditorState: Function,
  getMentionsQueryUiState: boolean,
  editorState: EditorState,
  network: string,
  decorators: CompositeDecorator,
  onBlur: Function,
  loadMentionsQueryStatus: string,
  closeMentionsQueryDialog: Function,
  currentMentionOffset: string
};

type DefaultProps = {
  mentionsQueryList: ImmutableList<MentionsRecord>,
  getMentionsQueryUiState: boolean
};

class MentionsQuery extends Component<DefaultProps, Props, any> {
  static displayName = 'MentionsQuery';
  static rootNode;
  static defaultProps: DefaultProps = {
    mentionsQueryList: [],
    getMentionsQueryUiState: false
  };

  constructor(props: Props) {
    super(props);
  };

  componentWillReceiveProps(nextProps: Props) {
    // NOTE: This checks to see if the cursor went outside of an incomplete mention.
    //       If so, it will close the mentionQuery window
    const selectionStartKey = nextProps.editorState.getSelection().getStartKey();
    const focusOffset = nextProps.editorState.getSelection().getFocusOffset();

    nextProps.editorState.getBlockTree(selectionStartKey).forEach((blockRecord) => {
      if (blockRecord.start < focusOffset && blockRecord.end >= focusOffset) {
        if (blockRecord.decoratorKey === null || !blockRecord.decoratorKey.includes(getCompositeDecorator.compositeDecoratorMapping[DECORATOR_TYPES.MentionQueryDecorator])) {
          this.props.closeMentionsQueryDialog(this.props.network);
          return false;
        }
      }
    });

  }


  getPositioningStyleForQueryBox() {
    let atMentionLocation = document.getElementById(`${this.props.currentMentionOffset}`);
    let messageBox = document.getElementById(`editorWrapperId-${this.props.network}`);

    if (atMentionLocation && messageBox) {
      let messageBoxBoundingBox = messageBox.getBoundingClientRect();

      return {
        'top': `${atMentionLocation.getBoundingClientRect().top + 20}px`,
        'left': `${messageBoxBoundingBox.left + 10}px`,
        'width': `${(messageBoxBoundingBox.right - messageBoxBoundingBox.left) - 20}px`
      };
    }
    return {'visibility': 'hidden'};
  }

  buildSpinnerForMentions(): React.Element<any> {
    return (
      <div className={styles.loadingQuerySuggestions}>
        <LoadingAnimation className={styles.loadingMentionsSpinner} style={{'position': 'static'}}/>
        <div className={styles.retrievingAccounts}>{mentionsQueryMenuText.retrievingAccounts}</div>
      </div>
    );
  }

  buildErrorForMentions(): React.Element<any> {
    return (<div className={styles.errorQuerying}>{mentionsQueryMenuText.errorQuerying}</div>);
  }

  buildMentionsQueryMenuSuggestions(listOfMentions: ImmutableList<MentionsRecord>): Array<Object> {
    return listOfMentions.map((mention) => {
      return (
        <MentionQueryRow
          key={mention.profileId}
          mention={mention}
          network={this.props.network}
          editorState={this.props.editorState}
          decorators={this.props.decorators}
          sendUpdatedEditorState={this.props.sendUpdatedEditorState}
          onBlur={this.props.onBlur}
        />
      );
    }).toArray();
  };


  buildMentionsSuggestions(loadMentionsQueryStatus: string, listOfMentions: ImmutableList<MentionsRecord>): any {
    if (loadMentionsQueryStatus === MentionsActionConstants.REQUEST_MENTIONS_DATA) {
      return this.buildSpinnerForMentions();
    } else if (loadMentionsQueryStatus === MentionsActionConstants.MENTIONS_DATA_ERROR) {
      return this.buildErrorForMentions();
    } else if (listOfMentions.size === 0 && loadMentionsQueryStatus === MentionsActionConstants.MENTIONS_RECEIVED) {
      return (<div className={styles.noResultsFound} key={'noResultsFound'}>{mentionsQueryMenuText.noResultsFound}</div>);
    } else {
      return this.buildMentionsQueryMenuSuggestions(listOfMentions);
    }
  };

  render() {
    const {mentionsQueryList, loadMentionsQueryStatus} = this.props;

    if (this.props.getMentionsQueryUiState || loadMentionsQueryStatus === MentionsActionConstants.REQUEST_MENTIONS_DATA) {
      return (
        <div tabIndex='989' className={styles.mentionsQuery} style={this.getPositioningStyleForQueryBox()}>
          { this.buildMentionsSuggestions(loadMentionsQueryStatus, mentionsQueryList) }
        </div>
      );
    }
    return null;
  }
};

export default MentionsQuery;
