/* @flow */
import React from 'react';
import styles from 'styles/modules/components/MessageTitleBox.scss';
import {  messageTitleBoxText } from 'constants/UITextConstants';
import { MAX_MESSAGE_TITLE_CHARS } from 'constants/ApplicationConstants';
import { TextInput } from '@spredfast/react-lib';
import { Embed } from '@spredfast/sf-intents';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

type Props = {
  messageTitle: string,
  onChange: Function,
  currentPlanColor: string,
  closeButtonClicked: () => void,
  canUseMultichannelPubs: bool
};

const MessageTitleBox = ({messageTitle, onChange, currentPlanColor, closeButtonClicked, canUseMultichannelPubs}: Props) => {
  const renderCloseButton = () => {
    if (Embed.isEmbedded()) {
      const closeButtonStyles = cx(styles.closeButton, {
        embeddedCloseButton: !canUseMultichannelPubs
      });
      return (
        <button className={closeButtonStyles} onClick={closeButtonClicked}>
          <i className='icon-modal-close-thick' />
        </button>
      );
    }
  };

  const titleColorBarStyle = {
    backgroundColor: currentPlanColor
  };
  return (
    <div className={styles.headerBar}>
      <span className={styles.tempTitle}>
        <div className={styles.titleColorBar} style={titleColorBarStyle}></div>
        <TextInput
          maxLength={MAX_MESSAGE_TITLE_CHARS}
          value={messageTitle}
          placeholder={messageTitleBoxText.defaultPlaceHolderText}
          onDebouncedChange={e => {
            onChange(e.target.value);
          }} />
      </span>
      {renderCloseButton()}
    </div>
  );
};

export default MessageTitleBox;
