/* @flow */
import React, { Component } from 'react';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import { toolTipPopUpDelay } from 'constants/UIStateConstants';
import { toolTipOverlayStyle } from 'constants/UILayoutConstants';

type State = {
  actualTextWidth: number
}

type Props = {
  displayAtWidth: number,
  evalText: string,
  evalStyles: any,
  overlayStyle: any,
  children: any,
  placement: string,
  overlay: any
};

type DefaultProps = {
  displayAtWidth: number,
  evalText: string,
  evalStyles: Object,
  overlayStyle: Object,
  children: string,
  placement: string,
  overlay: any
};

export const getActualTextWidth = (text: any, styles: any) => {
  if (text && text.length) {
    let tmpElement = document.createElement('div');
    tmpElement.innerText = text || '';

    if (styles) {
      Object.keys(styles).forEach((key) => {
        // $FlowFixMe: Not an error error
        tmpElement.style[key] = styles[key];
      });
    }
    tmpElement.style.display = 'inline';

    document.body.appendChild(tmpElement);

    const tmpWidth = tmpElement.offsetWidth;

    document.body.removeChild(tmpElement);

    return tmpWidth;
  } else {
    return -1;
  }
};

class HoverText extends Component<DefaultProps, Props, State> {
  static displayName = 'HoverText';
  state: State;

  static defaultProps: DefaultProps = {
    displayAtWidth: 0,
    evalText: '',
    evalStyles: {},
    overlayStyle: {},
    children: '',
    placement: 'topLeft',
    overlay: undefined
  };

  constructor(props: Props): void {
    super(props);

    this.state = {
      actualTextWidth: -1
    };
  }

  componentWillMount() {
    const actualWidth = getActualTextWidth(this.props.evalText, this.props.evalStyles);

    if (actualWidth !== -1) {
      this.setState({
        actualTextWidth: actualWidth
      });
    }
  }

  render() {
    const shouldDisplay = this.state.actualTextWidth === -1 || (this.state.actualTextWidth || 0) >= (this.props.displayAtWidth || 0);

    let overlayStyle = {
      ...toolTipOverlayStyle,
      ...this.props.overlayStyle
    };

    if (!shouldDisplay) {
      overlayStyle.display = 'none';
    }

    const overlay = this.props.overlay || this.props.evalText;

    return (
      <Tooltip overlay={overlay}
       destroyTooltipOnHide
       mouseEnterDelay={toolTipPopUpDelay}
       placement={this.props.placement}
       overlayStyle={overlayStyle}>
        {this.props.children}
      </Tooltip>
    );
  }
};

export default HoverText;