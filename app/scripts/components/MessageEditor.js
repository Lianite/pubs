/* @flow */
import React, { Component } from 'react';
import AccountSelector from 'components/AccountSelector';
import LinkPreview from 'components/LinkPreview';
import MessageContainer from 'containers/MessageContainer';
import MediaBarContainer from 'containers/MediaBarContainer';
import styles from 'styles/modules/components/MessageEditor.scss';
import _ from 'lodash';
import { Map as ImmutableMap } from 'immutable';
import { VoiceFilteringConstants } from 'constants/ApplicationConstants';

import type { List as ImmutableList } from 'immutable';
import type { EditorState } from 'draft-js';
import type { ImageType, VideoType, MediaStatusType } from 'selectors/AttachedMediaSelector';
import type { CredentialRecord } from 'records/AccountsRecord';
import type { LinkPreviewProp } from 'records/LinkPreviewRecords';

type Props = {
  messageEditorStateMap: ImmutableMap<string, EditorState>,
  voiceDropdownToggle: Function,
  selectedAccountList: ImmutableMap<string, CredentialRecord>,
  linkPreview: LinkPreviewProp,
  videoUploadNetworks: Array<string>,
  removeLinkPreview: Function,
  editLinkPreview: Function,
  removeSelectedAccount: Function,
  removeSelectedAccountsPerNetwork: ((network: string) => void),
  resendValidAssigneesRequest: () => Function,
  removeAttachment: Function,
  removeUploadedVideo: Function,
  updateFilterText: Function,
  updateExclusiveFilterChannel: ((channel: string) => void),
  updateFilterSelection: ((filterOption: string) => void),
  currentAccountChannelFilter: ImmutableList<string>,
  filterText: string,
  currentlyDraggingMedia: bool,
  mediaStatus: MediaStatusType,
  image: ImageType,
  video: VideoType,
  onDrop?: ((files:Array<File>) => void),
  onCustomLinkPreviewDropped?: ((file:File) => void),
  canShowLinkPreview: bool,
  canShowMediaBar: bool,
  cancelImageUpload: () => void,
  cancelVideoUpload: () => void,
  cycleThumbnailForward: () => void,
  cycleThumbnailBackward: () => void,
  onCancelUpload: () => void,
  onAcknowledgeError: () => void,
  onScrapeCancel: Function,
  onRefreshClicked: Function,
  onDropCustomVideoThumb?: ((file:File) => void),
  removeOrCancelCustomThumb?: () => void,
  voiceSelectorOpen: bool
};

type State = {
  accountSelectorHeight: number,
  accountSelectorWidth: number
};

export default class MessageEditor extends Component<void, Props, State> {
  static displayName = 'MessageEditor';
  topLevelNode: any;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      accountSelectorHeight: VoiceFilteringConstants.ACCOUNT_SELECTOR_DEFAULT_HEIGHT,
      accountSelectorWidth: 0
    };

    (this:any).adjustAccountSelectorSize = this.adjustAccountSelectorSize.bind(this);
  };

  componentDidMount() {
    window.addEventListener('resize', this.adjustAccountSelectorSize);
    this.adjustAccountSelectorSize();
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.adjustAccountSelectorSize);
  };

  adjustAccountSelectorSize() {
    if (this.refs.accountsWrapper) {
      this.setState({
        accountSelectorWidth: this.refs.accountsWrapper.offsetWidth
      });
    }
  };

  messages() {
    return this.props.messageEditorStateMap.keySeq().toArray().map( (name)=> {
      return (
        <MessageContainer
          network={name}
          key={name} />
      );
    });
  };

  render() {
    const {
      linkPreview,
      removeLinkPreview,
      editLinkPreview,
      currentlyDraggingMedia,
      onCustomLinkPreviewDropped,
      cycleThumbnailForward,
      cycleThumbnailBackward,
      onCancelUpload,
      onAcknowledgeError,
      canShowLinkPreview,
      onScrapeCancel,
      onRefreshClicked
    } = this.props;

    const linkPreviewComponent = canShowLinkPreview || linkPreview.linkPreviewLoading ?
      <LinkPreview
        currentlyDraggingMedia={currentlyDraggingMedia}
        captionUrl={_.get(linkPreview, 'linkPreviewInfo.linkUrl', '')}
        description={ _.get(linkPreview, 'linkPreviewInfo.description', '') }
        caption={_.get(linkPreview, 'linkPreviewInfo.caption', '')}
        title={_.get(linkPreview, 'linkPreviewInfo.title', '')}
        thumbnailUrl={_.get(linkPreview, 'linkPreviewInfo.thumbnailUrl', '')}
        linkUrl={_.get(linkPreview, 'linkPreviewInfo.linkUrl', '')}
        networks={linkPreview.linkPreviewNetworks}
        onDropFile={onCustomLinkPreviewDropped}
        isAlreadyUploading={linkPreview.linkPreviewAlreadyUploadingMedia}
        onRemove={removeLinkPreview}
        cycleThumbnailForward={cycleThumbnailForward}
        cycleThumbnailBackward={cycleThumbnailBackward}
        enableCarousel={linkPreview.enableCarousel}
        onEdit={editLinkPreview}
        onCancelUpload={onCancelUpload}
        onAcknowledgeError={onAcknowledgeError}
        linkPreviewLoading={linkPreview.linkPreviewLoading}
        onScrapeCancel={onScrapeCancel}
        onRefreshClicked={onRefreshClicked}
        error={linkPreview.linkPreviewCustomThumbError} /> : null;

    const mediaBar = this.props.canShowMediaBar ?
      <MediaBarContainer
        networks={this.props.messageEditorStateMap.keySeq().toArray()}
        mediaStatus={this.props.mediaStatus}
        image={this.props.image}
        video={this.props.video}
        videoUploadNetworks={this.props.videoUploadNetworks}
        removeAttachment={this.props.removeAttachment}
        removeUploadedVideo={this.props.removeUploadedVideo}
        currentlyDraggingMedia={this.props.currentlyDraggingMedia}
        onDrop={this.props.onDrop}
        cancelImageUpload={this.props.cancelImageUpload}
        cancelVideoUpload={this.props.cancelVideoUpload}
        onDropCustomVideoThumb={this.props.onDropCustomVideoThumb}
        removeOrCancelCustomThumb={this.props.removeOrCancelCustomThumb}/> : null;

    return (
      <div className={styles.messageEditor}>
        <AccountSelector
          onClick={this.props.voiceDropdownToggle}
          voiceSelectorOpen={this.props.voiceSelectorOpen}
          filterText={this.props.filterText}
          currentAccountChannelFilter={this.props.currentAccountChannelFilter}
          updateFilterText={this.props.updateFilterText}
          updateExclusiveFilterChannel={this.props.updateExclusiveFilterChannel}
          updateFilterSelection={this.props.updateFilterSelection}
          accounts={this.props.selectedAccountList}
          removeSelectedAccount={this.props.removeSelectedAccount}
          removeSelectedAccountsPerNetwork={this.props.removeSelectedAccountsPerNetwork}
          accountSelectorHeight={this.state.accountSelectorHeight}
          accountSelectorWidth={this.state.accountSelectorWidth}
          resendValidAssigneesRequest={this.props.resendValidAssigneesRequest}
           />

        <div className={styles.messageWrapper} ref='accountsWrapper'>
          {this.messages()}
        </div>

        {linkPreviewComponent}
        {mediaBar}
      </div>
    );
  }
}
