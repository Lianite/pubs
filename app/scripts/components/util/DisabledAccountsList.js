/* @flow */
import React from 'react';
import {
  FACEBOOK_TARGETING_VIEW_UNSUPPORTED_ACCOUNTS
} from 'constants/UITextConstants';
import {
  TOOLTIP_ALIGN,
  TOOLTIP_OVERLAY_STYLES
} from 'constants/UILayoutConstants';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import { CredentialRecord } from 'records/AccountsRecord';

import styles from 'styles/modules/components/optionsbar/networks/FacebookTargeting.scss';

export const DisabledAccountsList = ({ invalidAccounts }: { invalidAccounts: Array<CredentialRecord> }) => (
  <Tooltip
    trigger={['click']}
    overlay={
      <div>
        {
          invalidAccounts.map((account: CredentialRecord, index: number) => (
            <div key={index}>
              { account.name }
            </div>
          ))
        }
      </div>
  }
  overlayStyle={TOOLTIP_OVERLAY_STYLES}
  align={ TOOLTIP_ALIGN }>
    <div className={styles.link}>
      {FACEBOOK_TARGETING_VIEW_UNSUPPORTED_ACCOUNTS(invalidAccounts.length)}
    </div>
  </Tooltip>
);
