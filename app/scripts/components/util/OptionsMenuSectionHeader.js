/* @flow */
import React from 'react';
import classNames from 'classnames/bind';

import styles from 'styles/modules/components/util/OptionsMenuSectionHeader.scss';

type OptionsMenuSectionHeaderProps = {
  title: string,
  subheading?: bool,
  children?: React.Element<any>,
  iconRight?: bool,
  style?: Object
};

type TitleWrapperProps = {
  title: string,
};

const cx = classNames.bind(styles);

const TitleWrapper = (props: TitleWrapperProps) => <span className={styles.title}>{props.title}</span>;

const OptionsMenuSectionHeader = (props: OptionsMenuSectionHeaderProps) => (
  <div
    className={cx('wrapper', {
      subheading: props.subheading
    })}
    style={props.style}>
    {
      props.iconRight
      ? <div><TitleWrapper title={props.title}/> {props.children}</div>
      : <div>{props.children} <TitleWrapper title={props.title}/></div>
    }
  </div>
);

export default OptionsMenuSectionHeader;
