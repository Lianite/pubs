/* @flow */
import React from 'react';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import DropdownButton from 'components/util/DropdownButton';
import AlertBox from 'components/util/AlertBox';
import Label from 'components/util/Label';
import { Combobox } from 'react-widgets';
import _ from 'lodash';
import classNames from 'classnames/bind';

import styles from 'styles/modules/components/util/DropdownSection.scss';

type ComboboxConfig = {
  data: Array<any>,
  valueField: string,
  textField: string | () => string,
  placeholder?: string,
  itemComponent?: (props: Object) => React.Element<any>,
  value?: any,
  textField: string | () => string,
  placeholder?: string,
  readOnly?: bool,
  messages?: Object,
  filter?: string | bool,
  onBlur?: Function,
  onChange?: Function,
  disabled?: bool,
  disabledField?: string //Used to determine the item/choice field to check if the object should be disabled
};

type SelectedDatum = {
  text: string,
  error?: bool,
  color?: string
};

export type DropdownSectionProps = {
  title: string,
  icon?: React.Element<any>,
  iconRight?: bool,
  subheading?: bool,
  description?: string,
  comboBoxConfig: ComboboxConfig,
  selectedData: Array<SelectedDatum>,
  onRemoveDatum: (removedData: string) => void,
  errorText? : string,
  shouldToggleOpen?: bool,
  fetchData: Function,
  addDatum: Function,
  onInputValueChange: (value: string) => void,
  clearInputOnSelect: bool,
  canAddData?: bool,
  maxDatumLength: number,
  singleSelect: bool,
  onClearButtonClick: () => void,
  shouldToggleOpen: bool,
  colorSelected?: bool,
  onToggle: (open: bool) => void,
};

type DropdownSectionDefaultProps = {
  selectedData: Array<Object>,
  onRemoveDatum: (datum: any) => void,
  shouldToggleOpen: bool,
  fetchData: () => void,
  addDatum: (datum: any) => void,
  onInputValueChange: (value: string) => void,
  clearInputOnSelect: bool,
  canAddData: bool,
  maxDatumLength: number,
  singleSelect: bool,
  onClearButtonClick: () => void,
  onToggle: (open: bool) => void,
};

type SelectedDataProps = {
  data: Array<Object>,
  onRemoveDatum: (removedData: string) => void,
};

export const SelectedData = (props: SelectedDataProps) => (
  <div>
    {
      props.data && props.data.length
      ? (
        _.map(props.data, (datum: SelectedDatum, index: number) => (
          <Label
            key={index}
            onRemove={props.onRemoveDatum.bind(null, datum.text)}
            error={!!datum.error}>
            {datum.text}
          </Label>
        ))
      )
      : null
    }
  </div>
);

const cx = classNames.bind(styles);

class DropdownSection extends React.Component<DropdownSectionDefaultProps, DropdownSectionProps, any> {

  static defaultProps: DropdownSectionDefaultProps = {
    selectedData: [],
    onRemoveDatum: () => {},
    fetchData: () => {},
    addDatum: () => {},
    onInputValueChange: () => {},
    clearInputOnSelect: false,
    canAddData: true,
    maxDatumLength: 255,
    singleSelect: false,
    onClearButtonClick: () => {},
    shouldToggleOpen: true,
    onToggle: () => {}
  };

  props: DropdownSectionProps;

  handleDatumCreation: (value: any) => void;
  topLevelNode: ?Object;
  fetchData: Function;
  addDatum: Function;
  hasSelected: bool;

  constructor(props: DropdownSectionProps) {
    super(props);
    this.state = {
      comboBoxOpen: false
    };
    (this: any).onBlurHandler = this.onBlurHandler.bind(this);
    (this: any).topLevelRef = this.topLevelRef.bind(this);
    (this: any).onChange = this.onChange.bind(this);
    (this: any).onFocus = this.onFocus.bind(this);
    this.hasSelected = false;
  }

  componentWillReceiveProps(nextProps: DropdownSectionProps) {
    if (nextProps.shouldToggleOpen === false && nextProps.shouldToggleOpen !== this.props.shouldToggleOpen) {
      this.setState({comboBoxOpen: false});
    }
  }

  topLevelRef(ref: any) {
    this.topLevelNode = ref;
  };

  toggleOpen() {
    const open: bool = !this.state.comboBoxOpen;
    this.setState({
      comboBoxOpen: open
    });
    this.props.onToggle(open);
  };

  onChange(value: any) {
    let newValue: any = value;
    if (this.hasSelected && this.props.clearInputOnSelect) {
      newValue = '';
    }
    this.setState({
      comboBoxValue: newValue
    });
    this.props.comboBoxConfig.onChange && this.props.comboBoxConfig.onChange(newValue);
    if (!value[this.props.comboBoxConfig.disabledField]) {
      this.props.onInputValueChange(newValue);
      this.hasSelected = false;
    }
  }

  onFocus(e: Object) {
    if (this.props.shouldToggleOpen === false) {
      this.setState({
        comboBoxOpen: false
      });
    } else if (!(e.target && e.target.tagName ===  'INPUT' && e.relatedTarget && e.relatedTarget.tagName === 'LI')) {
      this.setState({
        comboBoxOpen: true
      });
      this.props.onToggle(true);
    }
  }

  onBlurHandler(e: any) {
    if ((this.topLevelNode && !this.topLevelNode.contains(e.relatedTarget)) || _.isNull(e.relatedTarget)) {
      this.setState({
        comboBoxOpen: false
      });
      this.props.onToggle(false);
    }
    this.props.comboBoxConfig.onBlur && this.props.comboBoxConfig.onBlur();
  };

  shouldRenderClearButton() {
    return this.props.singleSelect && this.props.selectedData && this.props.selectedData.length;
  }

  getExistingDatum(text: string) {
    return this.props.comboBoxConfig.data.find((datum) => (
      typeof datum !== 'string' && _.get(datum, this.props.comboBoxConfig.valueField) === text
    ));
  }

  handleDatumCreation(value: any) {
    let newValue = value;

    if (!this.props.singleSelect) {
      newValue = _.get(value, this.props.comboBoxConfig.valueField, '') || newValue;

      if (typeof newValue === 'string') {
        newValue = newValue.trim().replace(/\s\s+/, ' ').replace(/\,/g, '');
      }

      this.props.addDatum(newValue);
    }

    if (!this.props.singleSelect && this.props.canAddData) {
      newValue = '';
    } else {
      this.props.addDatum(value);
    }

    this.props.fetchData(newValue);
  }

  renderColorBar() {
    const selectedColor = this.props.colorSelected && this.props.selectedData && this.props.selectedData[0] && this.props.selectedData[0].color;
    return selectedColor ? <div
      className={styles.colorBar}
      style={{
        backgroundColor: selectedColor
      }}/>
    : null;
  }

  render() {
    const adjustedComboBoxConfig = Object.assign({}, this.props.comboBoxConfig);
    delete adjustedComboBoxConfig.disabledField;
    return (
      <div className={styles.dropdownSection} ref={this.topLevelRef}>
        <OptionsMenuSectionHeader
          title={this.props.title}
          iconRight={this.props.iconRight}
          subheading={this.props.subheading}>
          {this.props.icon}
        </OptionsMenuSectionHeader>
        <div className={styles.description}>{this.props.description}</div>
        {
          !this.props.singleSelect
          ? <SelectedData
            onRemoveDatum={this.props.onRemoveDatum}
            data={this.props.selectedData} />
          : null
        }
        {
          this.props.errorText
          ? <AlertBox text={this.props.errorText} />
          : null
        }
        <div
          className={cx('comboBoxContainer', {
            clearButtonVisible: this.shouldRenderClearButton()
          })}>
          {this.renderColorBar()}
          <Combobox
            {...adjustedComboBoxConfig}
            value={this.props.comboBoxConfig.value || this.state.comboBoxValue}
            open={this.state.comboBoxOpen}
            onFocus={this.onFocus}
            onToggle={() => {}}
            onBlur={this.onBlurHandler}
            onKeyPress={() => {
              if (!this.state.comboBoxOpen) {
                this.toggleOpen();
              }
            }}
            onChange={this.onChange}
            onSelect={(value) => {
              if (!value[this.props.comboBoxConfig.disabledField]) {
                this.hasSelected = true;
                if (this.props.singleSelect) {
                  this.toggleOpen();
                }
                if (!this.state.comboBoxOpen && !this.state.singleSelect) {
                  this.toggleOpen();
                }
                this.handleDatumCreation(value);
              }
            }}/>
          <DropdownButton
            onClick={() =>{
              this.props.shouldToggleOpen ? this.toggleOpen() : () => {};
            }}
            onBlur={this.onBlurHandler}
            open={this.state.comboBoxOpen}/>
          {
            this.shouldRenderClearButton()
            ? <button type='button' className={styles.clearSelectedDatum} onClick={this.props.onClearButtonClick}>
              <span className='icon-modal_close'></span>
            </button>
            : null
          }
        </div>
      </div>
    );
  }
};

export default DropdownSection;
