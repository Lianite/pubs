/* @flow */
import React from 'react';
import classNames from 'classnames/bind';
import styles from 'styles/modules/components/optionsbar/Label.scss';
import HoverText from 'components/HoverText';
import { textWidthForHoverText } from 'constants/UILayoutConstants';

type Props = {
  children?: any,
  id?: number,
  onRemove: Function,
  error: bool
};
const cx = classNames.bind(styles);

const Label = (props: Props) => {
  return (
    <HoverText
        displayAtWidth={textWidthForHoverText.LABEL}
        evalText={props.children}>
      <div className={props.error ? cx(styles.label, styles.error) : styles.label}>
        <div className={styles.labelName}>{props.children}</div>
        <span className={styles.closeButton + ' icon-modal_close'} onClick={() => {
          props.onRemove();
        }}></span>
      </div>
    </HoverText>
  );
};

export default Label;
