/* @flow */
import React from 'react';

import styles from 'styles/modules/components/util/FeaturePlaceholder.scss';

type Props = {
  image: string;
  header: string;
  body: string;
};

const FeaturePlaceholder = ({image, header, body}: Props) => (
  <div className={styles.featurePlaceholder}>
    <div className={styles.image} style={{backgroundImage: `url(${image})`}} />
    <div className={styles.placeholderText}>
      <h1>{header}</h1>
      {
        body.split('\n').map((textBlock: string, index: number) => <p key={index}>{textBlock}</p>)
      }
    </div>
  </div>
);

export default FeaturePlaceholder;
