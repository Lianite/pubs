/* @flow */
import React from 'react';

import styles from 'styles/modules/components/util/DropdownButton.scss';

type Props = {
  onClick: () => void,
  onBlur?: () => void,
  open: bool
};

const DropdownButton = ({onClick, onBlur, open}: Props) => (
  <button
    className={styles.button}
    onClick={onClick}
    onBlur={onBlur}>
    <i className={open ? 'icon-caret-up' : 'icon-caret-down'}/>
  </button>
);

export default DropdownButton;
