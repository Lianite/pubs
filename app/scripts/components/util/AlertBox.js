import React from 'react';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import classNames from 'classnames/bind';

import styles from 'styles/modules/components/util/AlertBox.scss';

export type Props = {
  text: string,
  additionalText?: string,
  link?: React.Element,
  soft: bool,
};

const cx = classNames.bind(styles);

const AlertBox = (props: Props) => {

  return (
    <div className={cx('alertWrapper', {
      soft: props.soft
    })}>
      <RADIcon className={styles.alertIcon} glyph='warning-dot'/>
      <div className={styles.alertText}>
        <div className={styles.header}>
          {props.text}
        </div>
        <div className={styles.additionalText}>
          {props.additionalText}
        </div>
        <div className={styles.link}>
          {props.link}
        </div>
      </div>
    </div>
    );
};

export default AlertBox;