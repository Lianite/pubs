/* @flow */
import React from 'react';
import FeaturePlaceholder from 'components/util/FeaturePlaceholder';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/util/FeaturePlaceholder.scss';

let shallowWrapper;

const mainProps = {
  image: 'image:url',
  header: 'header',
  body: 'this is the body'
};

const render = (props = mainProps) => {
  props = {
    ...mainProps,
    ...props
  };
  shallowWrapper = shallow(<FeaturePlaceholder {...props} />);
};

describe('Feature Placeholder Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const placeholder = shallowWrapper.find(`.${styles.featurePlaceholder}`);
    expect(placeholder.length).toEqual(1);

    const image = placeholder.find(`.${styles.image}`);
    expect(image.length).toEqual(1);
    expect(image.prop('style')).toDeepEqual({
      backgroundImage: 'url(image:url)'
    });

    const text = placeholder.find(`.${styles.placeholderText}`);
    expect(text.length).toEqual(1);

    const header = text.find('h1');
    expect(header.length).toEqual(1);
    expect(header.text()).toEqual('header');

    const body = text.find('p');
    expect(body.length).toEqual(1);
    expect(body.text()).toEqual('this is the body');
  });

  it('should render correctly when the text body has newlines', () => {
    render({
      body: 'this is \n the body'
    });

    const body = shallowWrapper.find('p');
    expect(body.length).toEqual(2);
    expect(body.at(0).text()).toEqual('this is ');
    expect(body.at(1).text()).toEqual(' the body');
  });
});
