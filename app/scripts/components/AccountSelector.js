/* @flow */
import React from 'react';
import styles from 'styles/modules/components/AccountSelector.scss';
import VoiceListContainer from 'containers/VoiceListContainer';
import SelectedAccount from 'components/SelectedAccount';
import { TextInput } from '@spredfast/react-lib';
import { Map as ImmutableMap } from 'immutable';
import { accountRollupUIConstants, keyCodeConstants } from 'constants/UILayoutConstants';
import _ from 'lodash';

import type { List as ImmutableList } from 'immutable';
import type { CredentialRecord } from 'records/AccountsRecord';

type Props = {
  onClick: Function,
  removeSelectedAccount: Function,
  removeSelectedAccountsPerNetwork: ((network: string) => void),
  resendValidAssigneesRequest: Function,
  accounts: ImmutableMap<string, CredentialRecord>,
  accountSelectorHeight: number,
  accountSelectorWidth: number,
  updateFilterText: Function,
  filterText: string,
  updateExclusiveFilterChannel: ((channel: string) => void),
  updateFilterSelection: ((filterOption: string) => void),
  currentAccountChannelFilter: ImmutableList<string>,
  voiceSelectorOpen: bool
};

const AccountSelector = (props: Props) => {

  const onFocusHandler = () => {
    props.onClick('focus');
  };

  const buildSelectedAccountList = () => {
    let accountElements = [];

    props.accounts.forEach((value, key) => {
      const account = props.accounts.get(key);
      accountElements.push(<SelectedAccount
        key={key}
        accountName={account.name}
        authenticated={account.authenticated}
        service={account.socialNetwork}
        accountId={account.uniqueId}
        remove={props.removeSelectedAccount}
        resendValidAssigneesRequest={props.resendValidAssigneesRequest}/>);
    });

    return accountElements;
  };

  const buildSelectedAccountsRolledUp = () => {
    let rolledAccountsPerNetwork = {};

    props.accounts.forEach((value, key) => {
      const account = props.accounts.get(key);
      if (!rolledAccountsPerNetwork[account.socialNetwork]) {
        rolledAccountsPerNetwork[account.socialNetwork] = [];
      }
      rolledAccountsPerNetwork[account.socialNetwork].push(account);
    });

    return _.map(rolledAccountsPerNetwork, (networkAccs: Array<CredentialRecord>, key: string) => {
      return (<SelectedAccount
        key={key}
        accountName={`${networkAccs.length} ${networkAccs.length === 1 ? 'account' : 'accounts'}`}
        removeSelectedAccountsPerNetwork={props.removeSelectedAccountsPerNetwork}
        service={key}
        credentials={networkAccs}
        authenticated={_.every(networkAccs, account => account.authenticated)}
        updateExclusiveFilterChannel={props.updateExclusiveFilterChannel}
        updateFilterSelection={props.updateFilterSelection}
        currentAccountChannelFilter={props.currentAccountChannelFilter}
        voiceSelectorOpen={props.voiceSelectorOpen}
        openAccountSelector={props.onClick}
        resendValidAssigneesRequest={props.resendValidAssigneesRequest}/>);
    });
  };

  let topLevelNode;

  const handleClick = (e) => {
    if (e.target.classList.contains(styles.validClickTarget)) {
      props.onClick();
    }
  };

  const onBlur = (e): void => {
    //e.relatedTarget should be the new item that is / will be focused.
    // Because onBlur actually gets called on this item when you focus
    // on a child element, we only want to close the dropdown if we
    // clicked somewhere else on the document.
    if (!topLevelNode.contains(e.relatedTarget)) {
      props.onClick('blur');
    }
  };

  const onFocus = (e: SyntheticEvent): void => {
    //If someone clicked on the toplevel node, the onClick handler will
    // handle opening / closing the dropdown. Otherwise (if we clicked
    // on a child that is getting focus) then the onClick won't be called,
    // so we need to handle the opening of the dropdown here
    if (e.target !== topLevelNode) {
      props.onClick('focus');
    }
  };

  const topLevelRef = (ref): void => {
    topLevelNode = ref;
  };

  const handleEsc = (e: SyntheticKeyboardEvent): void => {
    if (e.keyCode === keyCodeConstants.ESC_KEY) { //ESC key is 27
      props.onClick('blur');
    }
  };

  let accountsToDisplay;
  const totalAccountWidth = props.accounts.size * accountRollupUIConstants.ACCOUNT_BUTTON_SIZE_PLUS_MARGIN;

  if (totalAccountWidth > (props.accountSelectorWidth * accountRollupUIConstants.ACCOUNT_ROWS_BEFORE_ROLLUP)) {
    accountsToDisplay = buildSelectedAccountsRolledUp();
  } else {
    accountsToDisplay = buildSelectedAccountList();
  }

  return (
    <div tabIndex='1' ref={topLevelRef} className={styles.accountSelectorBar + ' ' + styles.validClickTarget} onFocus={onFocus} onClick={handleClick} onBlur={onBlur} onKeyUp={handleEsc}>
      <div className={styles.validClickTarget + ' ' + styles.flexProtect}>
        <span className={styles.validClickTarget + ' ' + styles.accountsTag}>Account(s): </span>

        {accountsToDisplay}
        <TextInput
          className={styles.accountFilterInput}
          placeholder='Add accounts... '
          value={props.filterText}
          onDebouncedChange={e => {
            if (!props.voiceSelectorOpen) {
              props.onClick();
            }
            props.updateFilterText(e.target.value);
          }}
          onFocus={onFocusHandler}
          debounceInterval={200}
        />
      </div>
      <VoiceListContainer
        accountSelectorRef={topLevelNode}
        className={styles.voiceListContainer}
        accountSelectorHeight={props.accountSelectorHeight} />
    </div>
  );
};

export default AccountSelector;
