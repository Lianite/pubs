/* @flow */
import React, { Component } from 'react';
import { scheduleMessageText } from 'constants/UITextConstants';
import { adjustUtcMomentTime,
  adjustUtcMomentDate,
  timeOptions,
  getDefaultScheduleTime,
  formatTimeForTextInput,
  createUtcMomentFromDate,
  getCurrentMonthDateUtc,
  dateIsCurrent } from 'utils/DateUtils';
import classNames from 'classnames/bind';
import CalendarDay from 'components/CalendarDay';
import { Combobox } from 'react-widgets';
import { Calendar } from 'react-widgets';
import moment from 'moment';
import momentLocalizer from 'react-widgets/lib/localizers/moment';

import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import type Moment from 'moment';

import styles from 'styles/modules/components/DatetimePicker.scss';

momentLocalizer(moment);
const cx = classNames.bind(styles);

type State = {
  timeSelectorOpen: bool
}

type Props = {
  scheduledDatetime: ?Moment,
  calendarCurrentDate: Date,
  calendarDate: ?Moment,
  userTimezone: TimezoneRecord,
  timeString: string,
  timeError: bool,
  setTime: (timeString: string, moment: ?Moment) => void,
  setDate: (moment: Moment) => void,
};

class DatetimePicker extends Component<void, Props, State> {
  static displayName = 'DatetimePicker';
  state: State;
  firstDayOfMonth: Date;

  constructor(props: Props): void {
    super(props);

    this.firstDayOfMonth = getCurrentMonthDateUtc(props.userTimezone.offset, moment(props.calendarCurrentDate)).local().toDate();
    this.state = {
      timeSelectorOpen: false
    };

    (this:any).toggleOpen = this.toggleOpen.bind(this);
    (this:any).timeStringChanged = this.timeStringChanged.bind(this);
    (this:any).onChooseItem = this.onChooseItem.bind(this);
    (this:any).onBlurHandler = this.onBlurHandler.bind(this);
    (this:any).calendarDateChanged = this.calendarDateChanged.bind(this);
    (this:any).renderCalendarDay = this.renderCalendarDay.bind(this);
  };

  toggleOpen() {
    this.setState({
      timeSelectorOpen: !this.state.timeSelectorOpen
    });
  };

  onBlurHandler() {
    let timeString = this.props.timeString;
    if (!timeString) {
      const defaultDatetimeUTCMoment = getDefaultScheduleTime(this.props.userTimezone.offset, moment(this.props.calendarCurrentDate));
      timeString = formatTimeForTextInput(defaultDatetimeUTCMoment, this.props.userTimezone.offset);
    }
    const adjustedDateTime = adjustUtcMomentTime(this.props.scheduledDatetime, timeString, this.props.userTimezone.offset, moment(this.props.calendarCurrentDate));

    this.props.setTime(timeString, adjustedDateTime, true);
  }

  timeStringChanged(timeString: string) {
    const adjustedDateTime = adjustUtcMomentTime(this.props.scheduledDatetime, timeString, this.props.userTimezone.offset, moment(this.props.calendarCurrentDate));

    this.props.setTime(timeString, adjustedDateTime);
  };

  calendarDateChanged(date: Date) {
    const newDateMoment = createUtcMomentFromDate(date);
    if (dateIsCurrent(newDateMoment, this.props.userTimezone.offset, moment(this.props.calendarCurrentDate))) {
      const adjustedDate = adjustUtcMomentDate(this.props.calendarDate, newDateMoment, this.props.userTimezone.offset, moment(this.props.calendarCurrentDate));
      this.props.setDate(adjustedDate);
    }
  };

  onChooseItem(option: string) {
    this.setState({
      timeSelectorOpen: false
    });

    const adjustedDateTime = adjustUtcMomentTime(this.props.scheduledDatetime, option, this.props.userTimezone.offset, moment(this.props.calendarCurrentDate));
    this.props.setTime(option, adjustedDateTime);
  };

  renderCalendarDay(dateObject: {date: Date, label: string}) {
    return (
      <CalendarDay
        date={dateObject.date}
        label={dateObject.label}
        calendarCurrentDate={this.props.calendarCurrentDate} />
    );
  };

  render() {
    const timeComboBoxContainerStyles = cx({
      timeComboBoxContainer: true
    });

    const utcMomentAsDate = this.props.calendarDate ? this.props.calendarDate.toDate() : null;

    return (
      <div className={timeComboBoxContainerStyles}>
        <div className={styles.datePicker}>
          <Calendar
            onChange={this.calendarDateChanged}
            dayFormat={day => ['S', 'M', 'T', 'W', 'T', 'F', 'S'][day.getDay()]}
            finalView={'month'}
            min={this.firstDayOfMonth}
            value={utcMomentAsDate}
            dayComponent={this.renderCalendarDay} />
        </div>

        <hr className={styles.dateTimePickerDivider} />

        <div className={styles.timePickerContainer}>
          <Combobox
            className={this.props.timeError ? 'error' : null}
            open={this.state.timeSelectorOpen}
            onToggle={this.toggleOpen}
            onClick={this.toggleOpen}
            placeholder={scheduleMessageText.timeSelectorPlaceholderText}
            data={timeOptions}
            suggest
            value={this.props.timeString}
            onSelect={this.onChooseItem}
            onChange={this.timeStringChanged}
            onBlur={this.onBlurHandler} />

          <i className='icon-clock-circle-outline' />
        </div>
      </div>
    );
  }
};

export default DatetimePicker;
