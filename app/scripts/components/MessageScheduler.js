/* @flow */
import React, { Component } from 'react';
import DatetimeInput from 'components/DatetimeInput';
import DatetimePicker from 'components/DatetimePicker';
import SchedulerAgenda from 'components/SchedulerAgenda';
import { scheduleMessageText } from 'constants/UITextConstants';
import { formatDatetimeForTextInput,
  formatTimeForTextInput,
  datetimeIsCurrent } from 'utils/DateUtils';
import _ from 'lodash';
import moment from 'moment';

import styles from 'styles/modules/components/MessageScheduler.scss';

import type Moment from 'moment';
import type { TimezoneRecord } from 'reducers/EnvironmentReducer';

type State = {
  lastValidDatetime: ?Moment,
  datetimeString: string,
  timeString: string,
  calendarDate: ?Moment,
  invalidDatetime: bool,
  invalidTime: bool,
  datetimeIsPast: bool,
  errorMessage: string
}

type Props = {
  scheduledDatetime: ?Moment,
  calendarCurrentDatetime?: ?Date, //Right now, this should only be used for testing to create consistent tests
  userTimezone: TimezoneRecord,
  loadingMessageHasCompleted: bool,
  savingMessageHasCompleted: bool,
  debounceWaitTime: number,
  setScheduledTime: (payload: {scheduledDatetime: ?Moment, scheduledDatetimeHasError: bool}) => void
}

type DefaultProps = {
  debounceWaitTime: number
}

class MessageScheduler extends Component<DefaultProps, Props, State> {
  state: State;
  static defaultProps: DefaultProps = {
    debounceWaitTime: 300
  };

  constructor(props: Props): void {
    super(props);

    this.state = {
      lastValidDatetime: props.scheduledDatetime,
      datetimeString: '',
      timeString: '',
      calendarDate: props.scheduledDatetime,
      invalidDatetime: false,
      invalidTime: false,
      datetimeIsPast: false,
      errorMessage: ''
    };

    (this: any).setDatetime = (this: any).setDatetime.bind(this);
    (this: any).setTime = (this: any).setTime.bind(this);
    (this: any).setDate = (this: any).setDate.bind(this);
  };

  updateErrors(invalidDatetime: bool, invalidTime: bool, datetimeIsPast: bool) {
    let errorMessage = '';

    if (invalidDatetime || invalidTime) {
      errorMessage = scheduleMessageText.datetimeStringInvalid;
    } else if (datetimeIsPast) {
      errorMessage = scheduleMessageText.datetimeStringNotCurrent;
    }

    this.setState({
      invalidDatetime,
      invalidTime,
      datetimeIsPast,
      errorMessage
    });
  };

  updateState(newState: State, utcDateTime: ?Moment) {
    this.setState({
      lastValidDatetime: newState.lastValidDatetime,
      datetimeString: newState.datetimeString,
      timeString: newState.timeString,
      calendarDate: newState.calendarDate
    });

    this.props.setScheduledTime({
      scheduledDatetime: utcDateTime || null,
      scheduledDatetimeHasError: newState.invalidDatetime || newState.invalidTime || newState.datetimeIsPast
    });
  }

  setDatetime(datetimeString: string, utcDateTime: ?Moment, eventIsOnBlur?: bool): void {
    let newState = _.assign({}, this.state);
    newState.datetimeString = datetimeString;
    newState.calendarDate = utcDateTime;
    newState.invalidDatetime = false;
    newState.datetimeIsPast = false;

    if (!utcDateTime) {
      newState.timeString = '';
      newState.invalidTime = false;
    } else if (utcDateTime.isValid()) {
      newState.lastValidDatetime = utcDateTime;
      newState.timeString = formatTimeForTextInput(utcDateTime, this.props.userTimezone.offset);
      newState.invalidTime = false;
      const calendarCurrentDatetimeMoment = this.props.calendarCurrentDatetime ? moment(this.props.calendarCurrentDatetime) : moment();

      if (!datetimeIsCurrent(utcDateTime, this.props.userTimezone.offset, calendarCurrentDatetimeMoment)) {
        newState.datetimeIsPast = true;
      }
    } else {
      newState.invalidDatetime = true;
    }

    this.updateState(newState, utcDateTime);

    const errorFree = !newState.invalidDatetime && !newState.invalidTime && !newState.datetimeIsPast;
    if (eventIsOnBlur || errorFree) {
      this.updateErrors(newState.invalidDatetime, newState.invalidTime, newState.datetimeIsPast);
    }
  };

  setTime(timeString: string, utcDateTime: ?Moment, eventIsOnBlur?: bool): void {
    let newState = _.assign({}, this.state);
    newState.timeString = timeString;
    newState.calendarDate = utcDateTime;
    newState.invalidTime = false;
    newState.datetimeIsPast = false;

    if (utcDateTime && utcDateTime.isValid()) {
      newState.lastValidDatetime = utcDateTime;
      newState.datetimeString = formatDatetimeForTextInput(utcDateTime, this.props.userTimezone.offset);
      newState.invalidDatetime = false;
      const calendarCurrentDatetimeMoment = this.props.calendarCurrentDatetime ? moment(this.props.calendarCurrentDatetime) : moment();

      if (!datetimeIsCurrent(utcDateTime, this.props.userTimezone.offset, calendarCurrentDatetimeMoment)) {
        newState.datetimeIsPast = true;
      }
    } else if ((utcDateTime && !utcDateTime.isValid()) || !utcDateTime) {
      newState.invalidTime = true;
    }

    this.updateState(newState, utcDateTime);

    const errorFree = !newState.invalidDatetime && !newState.invalidTime && !newState.datetimeIsPast;
    if (eventIsOnBlur || errorFree) {
      this.updateErrors(newState.invalidDatetime, newState.invalidTime, newState.datetimeIsPast);
    }
  };

  setDate(utcDate: Moment) {
    let newState = _.assign({}, this.state);
    newState.invalidDatetime = false;
    newState.datetimeIsPast = false;

    newState.datetimeString = formatDatetimeForTextInput(utcDate, this.props.userTimezone.offset);
    newState.timeString = formatTimeForTextInput(utcDate, this.props.userTimezone.offset);
    newState.lastValidDatetime = utcDate;
    newState.calendarDate = utcDate;

    this.updateState(newState, utcDate);
    this.updateErrors(newState.invalidDatetime, newState.invalidTime, newState.datetimeIsPast);
  };

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.loadingMessageHasCompleted !== nextProps.loadingMessageHasCompleted && nextProps.loadingMessageHasCompleted) {
      this.setState({
        datetimeString: formatDatetimeForTextInput(nextProps.scheduledDatetime, nextProps.userTimezone.offset),
        timeString: formatTimeForTextInput(nextProps.scheduledDatetime, nextProps.userTimezone.offset),
        calendarDate: nextProps.scheduledDatetime
      });
    }
    if (this.props.savingMessageHasCompleted !== nextProps.savingMessageHasCompleted && nextProps.savingMessageHasCompleted) {
      this.setState({
        datetimeString: '',
        timeString: '',
        calendarDate: null
      });
    }
  }

  render() {
    return (
      <div className={styles.scheduleContainer}>
        <div className={styles.scheduleTitle}>
          SCHEDULE
        </div>

        <DatetimeInput
          headerTitle={scheduleMessageText.header}
          datetimeString={this.state.datetimeString}
          calendarCurrentDatetime={this.props.calendarCurrentDatetime || moment().toDate()}
          setDatetime={this.setDatetime}
          datetimeError={this.state.invalidDatetime || this.state.datetimeIsPast}
          errorMessage={this.state.errorMessage}
          userTimezone={this.props.userTimezone}
          debounceDelayMs={this.props.debounceWaitTime} />

        <DatetimePicker
          scheduledDatetime={this.state.lastValidDatetime}
          calendarCurrentDate={this.props.calendarCurrentDatetime || moment().toDate()}
          calendarDate={this.state.calendarDate}
          timeString={this.state.timeString}
          userTimezone={this.props.userTimezone}
          timeError={this.state.invalidTime || this.state.datetimeIsPast}
          setTime={this.setTime}
          setDate={this.setDate} />

        <SchedulerAgenda/>

      </div>
    );
  }
};

export default MessageScheduler;
