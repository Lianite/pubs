/* @flow */
import React from 'react';
import styles from 'styles/modules/components/MediaBar.scss';
import Dropzone from 'react-dropzone';
import classNames from 'classnames/bind';
import { mediaBarText } from 'constants/UITextConstants';
import Image from 'components/media/Image';
import Video from 'components/media/Video';
import PlaceHolder from 'components/media/PlaceHolder';
import type { ImageType, VideoType, MediaStatusType } from 'selectors/AttachedMediaSelector';
import type { VideoThumbnailType } from 'adapters/types';

const cx = classNames.bind(styles);

let dropzoneRef;

type Props = {
  currentlyDraggingMedia: bool,
  networks: Array<string>,
  videoUploadNetworks: Array<string>,
  onDrop?: ((files:Array<File>) => void),
  mediaStatus: MediaStatusType,
  removeAttachment: Function,
  removeUploadedVideo: Function,
  cancelImageUpload: () => void,
  cancelVideoUpload: () => void,
  image: ImageType,
  video: VideoType,
  onDropCustomVideoThumb?: ((file:File) => void),
  removeOrCancelCustomThumb?: () => void,
  onVideoThumbnailFrameChanged?: (thumbnail: VideoThumbnailType) => void
};

const onDrop = (onDropCallback, files: Array<File>) => {
  if (onDropCallback) {
    onDropCallback(files);
  }
};

const onLoadFilesClick = () => {
  dropzoneRef.open();
};


const MediaBar = (props: Props) => {
  const dragging = props.currentlyDraggingMedia;
  const shouldDropzoneReact = dragging && props.mediaStatus.waiting;

  const mediaBarStyle = cx('mediaBar', {
    dragging: shouldDropzoneReact,
    hasPlaceholder: !props.mediaStatus.waiting
  });

  const placeholderText = dragging ? mediaBarText.dropText : mediaBarText.addMediaPrefix;
  const handleRemoveImage = props.mediaStatus.uploading ? props.cancelImageUpload : props.removeAttachment;
  const handleRemoveVideo = props.video.completed || props.mediaStatus.error ? props.removeUploadedVideo : props.cancelVideoUpload;

  const acceptMedia = props.mediaStatus.waiting ? 'image/*,video/mp4,video/x-m4v,video/*' : 'none';
  const updateDropzoneRef = node => {
    dropzoneRef = node;
  };

  const displayImage = !props.image.waiting;
  const displayVideo = !props.video.waiting;

  const mediaPlaceholderStyle = cx('mediaPlaceholder', {
    image: displayImage,
    video: displayVideo,
    waiting: props.mediaStatus.waiting,
    working: props.mediaStatus.uploading,
    completed: props.mediaStatus.completed,
    error: props.mediaStatus.error
  });

  return (
    <div className={mediaBarStyle}>
      <Dropzone
        ref={updateDropzoneRef}
        onDrop={onDrop.bind(null, props.onDrop)}
        className={styles.mediaBarDropzone}
        activeClassName={styles.mediaBarDropAccept}
        rejectClassName={styles.mediaBarDropReject}
        multiple={false}
        disableClick
        accept={acceptMedia}>
        {props.mediaStatus.waiting ?
          <PlaceHolder
            text={placeholderText}
            dragging={dragging}
            onLoadFiles={onLoadFilesClick} /> : null}
        <div className={mediaPlaceholderStyle}>
          {displayImage ?
            <Image
              networks={props.networks}
              uploading={!!props.image.uploading}
              image={props.image.currentImageThumbnail || ''}
              onRemove={handleRemoveImage}
              mediaFilesizeError={props.image.imageFilesizeError} /> : null}
          {displayVideo ?
            <Video
              currentlyDraggingMedia={dragging}
              video={props.video}
              networks={props.videoUploadNetworks}
              onRemove={handleRemoveVideo}
              onDropCustomThumb={props.onDropCustomVideoThumb}
              removeOrCancelCustomThumb={props.removeOrCancelCustomThumb}
              onVideoThumbnailFrameChanged={props.onVideoThumbnailFrameChanged}/> : null}
        </div>
      </Dropzone>
    </div>
  );
};

export default MediaBar;
