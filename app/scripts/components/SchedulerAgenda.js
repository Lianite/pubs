/* @flow */
import React from 'react';
import styles from 'styles/modules/components/SchedulerAgenda.scss';

import {schedulerAgendaTexts} from 'constants/UITextConstants';

const SchedulerAgenda = () => (
  <div className={styles.scheduleAgendaContainer}>
    <div className={`icon-cal-coming-soon ${styles.comingSoonSvg}`} />
    <div className={styles.comingSoonText}>{schedulerAgendaTexts.calendarComingSoon}</div>
  </div>
);

export default SchedulerAgenda;
