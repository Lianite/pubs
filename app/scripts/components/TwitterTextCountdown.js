/* @flow */
import React from 'react';
import twitterText from 'twitter-text';
import { TwitterConstants } from 'constants/CapabilitiesConstants';
import styles from 'styles/modules/components/TwitterTextCountdown.scss';

type Props = {
  text: string
};

const TwitterTextCountdown = (props: Props) => {
  let remainingChars = TwitterConstants.maxChars - twitterText.getTweetLength(props.text);
  let countdownStyle = styles.twitterTextCountdownContainer;
  if (remainingChars < 0) {
    countdownStyle += ' ' + styles.warning;
  }
  return (
    <div className={countdownStyle}>
      {remainingChars}
    </div>
  );
};

export default TwitterTextCountdown;
