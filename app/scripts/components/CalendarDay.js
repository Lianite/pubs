/* @flow */
import React, { Component } from 'react';
import styles from 'styles/modules/components/CalendarDay.scss';
import classNames from 'classnames/bind';
import _ from 'lodash';

const cx = classNames.bind(styles);

type Props = {
  date: Date,
  label: string,
  calendarCurrentDate: Date
}

// NOTE: this is stateful because the API for the calendar widget
//       needs it to be stateful per their api ¯\_(ツ)_/¯
class CalendarDay extends Component<void, Props, void> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const dateLabel = this.props.label.replace(/^0+/, '');
    let compareDate = _.cloneDeep(this.props.date);
    compareDate.setHours(0, 0, 0, 0);

    let compareCalendarCurrentDate = _.cloneDeep(this.props.calendarCurrentDate);
    compareCalendarCurrentDate.setHours(0, 0, 0, 0);

    const datetime = compareDate.getTime();
    const calendarDatetime = compareCalendarCurrentDate.getTime();

    const dateStyles = cx({
      calendarDay: true,
      outOfDate: datetime < calendarDatetime,
      currentDate: datetime === calendarDatetime
    });

    return (
      <div>
        <div className={dateStyles}>
          {dateLabel}
        </div>
      </div>
    );
  }
};

export default CalendarDay;
