/* @flow */
import React from 'react';
import styles from 'styles/modules/components/Voice.scss';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import '@spredfast/react-lib/styles/Checkbox.scss';

type Props = {
  id: number;
  name: string;
  credentials: Array<any>;
  unfilteredCredentialCount: number;
  handleChecked: (id: number, credentialId?: number) => void;
  checkedEntry: Object;
};

const Voice = ({id, name, credentials, unfilteredCredentialCount, handleChecked, checkedEntry}: Props) => {
  let checkState = false;

  if (Object.keys(checkedEntry).length > 0) {
    if (Object.keys(checkedEntry).length === unfilteredCredentialCount) {
      checkState = true;
    } else {
      checkState = 'indeterminate';
    }
  }

  return (
    <div className={styles.voice}>
      <div className={checkState ? styles.voiceHolder : styles.uncheckedVoiceHolder} onClick={() => {
        handleChecked(id);
      }}>
        <Checkbox checked={checkState} />
        <span className={styles.voiceName} >{name}</span>
      </div>
    </div>
  );
};

Voice.defaultProps = {
  checked: false
};

export default Voice;
