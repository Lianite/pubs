/* @flow */
import React, { Component } from 'react';
import classNames from 'classnames';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';

import styles from 'styles/modules/components/SidebarButton.scss';

type Props = {
  buttonPageName: string,
  buttonImage: string,
  active?: bool,
  onClick: () => void,
  error?: bool
}

export default class SidebarButton extends Component<void, Props, void> {
  rootNode: Object;
  componentWillEnter: () => void;
  componentWillLeave: () => void;

  constructor(props: Props) {
    super(props);
  }

  cacheNode = node => {
    this.rootNode = node;
  }

  componentWillLeave(callback: () => void) {
    this.rootNode.className = classNames(styles.sidebarButtonContainer, styles.sfSidebarButtonExit);
    setTimeout(callback, 500);
  };

  componentWillEnter(callback:  () => void) {
    const timeoutFunction = (callback) => {
      if (callback) {
        callback();
      }
      this.rootNode.className = styles.sidebarButtonContainer;
    };

    this.rootNode.className = classNames(styles.sidebarButtonContainer, styles.sfSidebarButtonEnter);
    setTimeout(function() {
      timeoutFunction(callback);
    }, 600);
  }

  render() {
    let buttonStyle = styles.sidebarButton;
    if (this.props.active) {
      buttonStyle += ' ' + styles.activeButton;
    }

    return (
      <div className={styles.sidebarButtonContainer} ref={this.cacheNode}>
        <button type='button' className={buttonStyle} onClick={this.props.onClick}>
          {
            this.props.error
            ? <RADIcon className={styles.error} glyph='warning-dot' />
            : null
          }
          <div className={this.props.buttonImage}></div>
          <span className={styles.tooltiptext}>{this.props.buttonPageName}</span>
        </button>
      </div>
    );
  };
};
