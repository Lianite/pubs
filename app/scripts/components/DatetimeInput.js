/* @flow */
import React from 'react';
import { scheduleMessageText } from 'constants/UITextConstants';
import { getDefaultScheduleTime,
  formatDatetimeForTextInput,
  createUtcMomentFromString } from 'utils/DateUtils';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import classNames from 'classnames/bind';
import moment from 'moment';

import styles from 'styles/modules/components/DatetimeInput.scss';

const cx = classNames.bind(styles);

type Props = {
  headerTitle?: string,
  datetimeString: string,
  calendarCurrentDatetime: Date,
  setDatetime: (datetimeString: string) => void,
  datetimeError: bool,
  errorMessage: ?string,
  userTimezone: TimezoneRecord,
  debounceDelayMs?: number
};

const DatetimeInput = (props: Props) => {
  const onChangeHandler = (datetimeString: string): void => {
    props.setDatetime(datetimeString, createUtcMomentFromString(datetimeString));
  };

  const onBlurHandler = (datetimeString: string): void => {
    let utcMoment = createUtcMomentFromString(datetimeString);
    if (utcMoment) {
      let validDatetimeString = datetimeString;

      if (utcMoment.isValid()) {
        validDatetimeString = formatDatetimeForTextInput(utcMoment, props.userTimezone.offset);
      }

      props.setDatetime(validDatetimeString, utcMoment, true);
    }
  };

  const populateDefaultDate = (): void => {
    if (!props.datetimeString) {
      const defaultDatetimeUTCMoment = getDefaultScheduleTime(props.userTimezone.offset, moment(props.calendarCurrentDatetime));
      props.setDatetime(formatDatetimeForTextInput(defaultDatetimeUTCMoment, props.userTimezone.offset), defaultDatetimeUTCMoment);
    }
  };

  const clearDatetime = () => {
    props.setDatetime('', null);
  };

  const renderErrors = (): ?React$Element<any> => {
    const isCurrentIconStyle = cx({
      'rad-icon-warning-dot': true,
      validationIcon: true,
      error: true
    });

    if (props.errorMessage) {
      return (
        <span>
          <span className={isCurrentIconStyle}></span>
          <p className={styles.errorText}>{props.errorMessage}</p>
        </span>
      );
    }
  };

  const datetimeInputWrapperStyles = cx({
    datetimeInputWrap: true,
    datetimePrenent: props.datetimeString
  });

  const datetimeInputStyles = cx({
    scheduleCalendarDateTimeInput: true,
    error: props.datetimeError
  });

  const inputHeaderTitle = props.headerTitle ?
    (<p className={styles.scheduleCalendarHeader}>{props.headerTitle}</p>) : null;

  const clearInputButton = props.datetimeString ?
    (<button type='button' className={styles.clearButton} onClick={clearDatetime}>
      <span className='icon-modal_close'></span>
    </button>) : null;

  return (
    <div className={styles.scheduleCalendar}>
      {inputHeaderTitle}

      {renderErrors()}

      <div className={datetimeInputWrapperStyles}>
        <input
          className={datetimeInputStyles}
          placeholder={scheduleMessageText.timeDateTextInputPlaceholder}
          value={props.datetimeString}
          onChange={(e) => {
            onChangeHandler(e.target.value);
          }}
          onClick={populateDefaultDate}
          onBlur={(e) => {
            onBlurHandler(e.target.value);
          }} />

        <i className='icon-calendar' />
      </div>

      {clearInputButton}

      <p className={styles.scheduleCalendarTimeZone}>{scheduleMessageText.timeZoneSubtitle} {props.userTimezone.abbreviation}</p>
    </div>
  );
};

export default DatetimeInput;
