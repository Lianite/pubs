/* @flow */
import React from 'react';
import styles from 'styles/modules/components/MentionsQuery.scss';

import { SupportedNetworks } from 'constants/ApplicationConstants';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import { insertMentionIntoEditorState } from 'utils/MentionsUtils';
import { EditorState } from 'draft-js';

import type { MentionsRecord } from 'types/MentionsTypes';
import type { CompositeDecorator } from 'draft-js';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

type Props = {
  mention: MentionsRecord,
  network: string,
  editorState: EditorState,
  decorators: CompositeDecorator,
  sendUpdatedEditorState: Function,
  onBlur: Function
};

const selectMention = (mention: MentionsRecord, editorState: EditorState, network: string, decorators: CompositeDecorator, sendUpdatedEditorState: Function, onBlur: Function) => {
  let newEditorState = insertMentionIntoEditorState(mention, editorState, network);
  sendUpdatedEditorState(
    {
      editorState: newEditorState,
      network: network,
      decorators
    });
  onBlur();
};


const MentionQueryRow = (props: Props): any => {
  let {mention, network, decorators, editorState, onBlur, sendUpdatedEditorState} = props;

  const twitterMentionNameStyles = cx({
    normalWeightHeader: true,
    nonVerified: !mention.verified
  });

  return (
    <div onClick={() => selectMention(mention, editorState, network, decorators, sendUpdatedEditorState, onBlur)} className={styles.individualMentionContainer}>
      <div className={styles.mentionIcon}>
        <img src={mention.icon} className={styles.mentionIcon}/>
      </div>
      <div className={styles.mentionsTextBox}>
        <h5>
          <span>{mention.displayName}</span>
          { mention.verified ? <RADIcon className={styles.verifiedIcon} glyph='feature_enabled' /> : null}
          { (network === SupportedNetworks.TWITTER && mention.screenName) ? (
            <span className={twitterMentionNameStyles}>{`@${mention.screenName}`}</span>
          ) : null}
        </h5>
        <div className={ styles.extraInfoList }>
          <span className={ styles.extraInfo }>
            { (network === SupportedNetworks.FACEBOOK && mention.likes > -1) ?
              <RADIcon className={styles.extraInfoIcon} glyph='twitter-favorite' />
              : null
            }
            { network === SupportedNetworks.FACEBOOK ?
              ` ${mention.likes} likes`
              : null
            }
          </span>
          <span className={ styles.extraInfo }>
            { mention.category ?
              <RADIcon className={styles.extraInfoIcon} glyph='folder' />
              : null
            }
            {` ${mention.category}`}
          </span>
          <span className={ styles.extraInfo }>
            { (network === SupportedNetworks.FACEBOOK && mention.location) ?
              <RADIcon className={styles.extraInfoIcon} glyph='geo' />
              : null
            }
            { network === SupportedNetworks.FACEBOOK ?
              ` ${mention.location}`
              : null
            }
          </span>
        </div>
      </div>
    </div>
  );
};

export default MentionQueryRow;
