/* @flow */
import React from 'react';
import classNamesBind from 'classnames/bind';
import { ValidationType } from 'constants/UIStateConstants';
import styles from 'styles/modules/components/GlobalValidationBar.scss';

import type { MessageValidationData } from 'selectors/ValidationSelector';

type Props = {
  validation: MessageValidationData
}

const cx = classNamesBind.bind(styles);

const GlobalValidationBar = (props: Props) => {
  let barStyle = cx({
    globalValidationBar: true,
    active: props.validation.type !== ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
    error: props.validation.type === ValidationType.VALIDATION_TYPE_ERROR,
    improvement: props.validation.type === ValidationType.VALIDATION_TYPE_IMPROVEMENT,
    permissionError: props.validation.type === ValidationType.VALIDATION_TYPE_EDIT_PERMISSION_ERROR
  });

  let iconStyle = cx({
    'rad-icon-warning-dot': props.validation.type === ValidationType.VALIDATION_TYPE_ERROR,
    validationIcon: true,
    visible: props.validation.type !== ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
    error: props.validation.type === ValidationType.VALIDATION_TYPE_ERROR,
    improvement: props.validation.type === ValidationType.VALIDATION_TYPE_IMPROVEMENT,
    'icon-no-edit': props.validation.type === ValidationType.VALIDATION_TYPE_EDIT_PERMISSION_ERROR
  });

  return (
    <div className={barStyle}>
      <div className={styles.contentWrapper}>
        <span className={iconStyle}></span>
        <span>{props.validation.validationText}</span>
      </div>
    </div>
  );
};

export default GlobalValidationBar;
