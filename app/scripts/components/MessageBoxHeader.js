/* @flow */
import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/bind';
import { ValidationType } from 'constants/UIStateConstants';
import { SupportedNetworks, networkIcons, networkUserFacingName } from 'constants/ApplicationConstants';
import { MessageBoxHeaderTexts } from 'constants/UITextConstants';
import { PostPreviewWidths } from 'constants/UILayoutConstants';
import PostPreview from '@spredfast/social-media-preview';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import styles from 'styles/modules/components/MessageBoxHeader.scss';

import type { PerNetworkValidationData } from 'selectors/ValidationSelector';
import type { PostPreview as PostPreviewData } from 'selectors/PostPreviewSelector';

type Props = {
  disableApplyToAll: bool,
  numberOfNetworks: number,
  network: string,
  applyToAllChannels: () => void,
  validation?: PerNetworkValidationData,
  postPreview: PostPreviewData,
  onPreviewVisibilityChange: (visible: bool) => void,
  previewVisible: bool
};

type State = {
  // These state properties are needed to compensate for deficiencies in the tooltip component.
  // When the tooltip shifts from "bottom" placement to "top" placement and then back, the tooltip
  // arrow can end up pointing in the wrong direction. Additionally, this compensates for the fact that
  // when the tooltip is large enough to visibly block its target, the arrow will still be visible and not have
  // any meaningful purpose
  fixPreviewArrow: bool,
  hidePreviewArrow: bool
};

type Rect = {
  top: number,
  right: number,
  bottom: number,
  left: number
};

const cx = classNames.bind(styles);
const tooltipPlacementBottomClassName = 'rc-tooltip-placement-bottom';
const tooltipHideArrowClassName = 'post-preview-arrow-hidden';

type ApplyToAllButtonProps = {
  disableApplyToAll: bool,
  numberOfNetworks: number,
  onApplyToAllChannels: Function
};

export const ApplyToAllButton = ({ disableApplyToAll, numberOfNetworks, onApplyToAllChannels }: ApplyToAllButtonProps) => {
  const applyToAllStyle = cx({
    applyToAllButton: true,
    disabledApplyAll: disableApplyToAll,
    shrunkHeader: true
  });

  return (numberOfNetworks > 1 ?
    <button className={applyToAllStyle}
      onClick={onApplyToAllChannels} disabled={disableApplyToAll}>{MessageBoxHeaderTexts.applyToAllButton}</button> :
    null);
};

type ErrorMessageProps = {
  validation?: PerNetworkValidationData
};

export const ErrorMessage = ({ validation }: ErrorMessageProps) => {
  if (validation && validation.type === ValidationType.VALIDATION_TYPE_NO_PROBLEMS) {
    return null;
  }

  const visibleError = !!(validation && validation.type === ValidationType.VALIDATION_TYPE_ERROR);
  const visibleImprovement = !!(validation && validation.type === ValidationType.VALIDATION_TYPE_IMPROVEMENT);
  const validationStyle = cx({
    validationIcon: true,
    error: visibleError,
    improvement: visibleImprovement,
    visible: visibleError || visibleImprovement
  });
  const inlineTextStyle = cx({
    validationText: true,
    error: visibleError,
    improvement: visibleImprovement,
    visible: visibleError || visibleImprovement
  });
  const inlineText = validation ? validation.inlineText : '';

  return (
    <div className={styles.messageHeaderErrorLine}>
      <span className={'rad-icon-warning-dot ' + validationStyle}/>
      <span className={inlineTextStyle}>{inlineText}</span>
    </div>
  );
};


class MessageBoxHeader extends React.Component {

  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);
    this.state = {
      fixPreviewArrow: false,
      hidePreviewArrow: false
    };
  }

  onApplyToAllChannels() {
    this.props.applyToAllChannels();
  }

  onPreviewVisibilityChange(visible: bool) {
    this.props.onPreviewVisibilityChange(visible);
    if (!visible) {
      this.setState({
        fixPreviewArrow: false,
        hidePreviewArrow: false
      });
    }
  }

  previewTargetRect(): Rect {
    return this.refs.previewTarget && ReactDOM.findDOMNode(this.refs.previewTarget).getBoundingClientRect() || {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    };
  }

  previewTargetIsBlocked(blockerRect: Rect): bool {
    const previewTargetRect: Rect = this.previewTargetRect();
    return (
        previewTargetRect.top >= blockerRect.top &&
        previewTargetRect.bottom <= blockerRect.bottom &&
        previewTargetRect.left >= blockerRect.left &&
        previewTargetRect.right <= blockerRect.right
    );
  }

  onPreviewPopupAlign(previewPopupDomNode: Object) {
    const previewTargetRect: Rect = this.previewTargetRect();
    const previewPopupRect: Rect = previewPopupDomNode.getBoundingClientRect();

    const hidePreviewArrow = this.previewTargetIsBlocked(previewPopupRect);
    const fixPreviewArrow = previewTargetRect.top < previewPopupRect.top;

    if (hidePreviewArrow !== this.state.hidePreviewArrow || fixPreviewArrow !== this.state.fixPreviewArrow) {
      this.setState({
        hidePreviewArrow,
        fixPreviewArrow
      });
    }
  }

  createPostPreview = (): ?React.Component => {
    let width: ?string = PostPreviewWidths[this.props.network];
    let Component;
    switch (this.props.network) {
    case SupportedNetworks.FACEBOOK:
      Component = PostPreview.Facebook;
      break;
    case SupportedNetworks.TWITTER:
      Component = PostPreview.Twitter;
      break;
    default:
      return null;
    }
    return (
      <Tooltip
        overlay={
          <Component
            {...this.props.postPreview}
            width={width}
            showSeeMore
            platforms={['DESKTOP', 'MOBILE']}
            showNetworkHeader/>
        }
        overlayClassName={classNames('post-preview-tooltip', {
          [tooltipPlacementBottomClassName]: this.state.fixPreviewArrow,
          [tooltipHideArrowClassName]: this.state.hidePreviewArrow
        })}
        overlayStyle={{
          maxWidth: width,
          width
        }}
        align={{
          offset: [0.75, -10]
        }}
        trigger={['click']}
        transitionName='post-preview-toggle'
        placement='bottom'
        onVisibleChange={this.onPreviewVisibilityChange.bind(this)}
        onPopupAlign={this.onPreviewPopupAlign.bind(this)}
        destroyTooltipOnHide
        ref='previewTarget'>
        <span className={`icon-eye-pelorous ${styles.previewIcon} ${this.props.previewVisible ? styles.previewVisible : ''}`}></span>
      </Tooltip>
      );
  }

  render() {
    return (
      <div className={styles.messageHeader} ref='messageHeader'>
        <div className={styles.messageHeaderTopLine}>
          <span className={styles.messageTitle} >
            <i className={`${networkIcons[this.props.network]} ${styles.networkIcon} ${styles[this.props.network.toLowerCase()]}`} />
            <span>{networkUserFacingName[this.props.network]}</span>
            {this.createPostPreview()}
          </span>
          <ApplyToAllButton
            disableApplyToAll={this.props.disableApplyToAll}
            numberOfNetworks={this.props.numberOfNetworks}
            onApplyToAllChannels={this.props.applyToAllChannels.bind(this)}/>
        </div>
        <ErrorMessage validation={this.props.validation}/>
      </div>
    );
  }
};

export default MessageBoxHeader;
