/* @flow */
import React, { Component } from 'react';
import styles from 'styles/modules/components/Message.scss';
import { Editor, EditorState } from 'draft-js';
import { messageBodyText } from 'constants/UITextConstants';
import TwitterTextCountdown from 'components/TwitterTextCountdown';
import MessageBoxHeaderContainer from 'containers/MessageBoxHeaderContainer';
import NetworkOptionsBar from 'components/message/NetworkOptionsBar';
import { getPlainTextFromEditorState } from 'utils/MessageUtils';
import { getCompositeDecorator } from 'decorators/EditorDecorator';
import { List as ImmutableList } from 'immutable';
import MentionsQuery from 'components/MentionsQuery';
import { KeyCodes } from 'constants/ApplicationConstants';

import type { MentionsRecord } from 'types/MentionsTypes';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import type { PerNetworkValidationData } from 'selectors/ValidationSelector';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

type Props = {
  messageEditorState: any,
  onChange: (editorState: any) => void,
  onBlur: (editorState: any) => void,
  network: string,
  applyToAllChannels: () => void,
  updateMessageDebounceTime?: number,
  messageEditorDefaultPlaceholder?: string,
  disableApplyToAll: bool,
  numberOfNetworks: number,
  validation: PerNetworkValidationData,
  mentionsQueryList: ImmutableList<MentionsRecord>,
  getMentionsQueryUiState: bool,
  loadMentionsQueryStatus: string,
  linkRegexString: string,
  closeMentionsQueryDialog: () => void,
  hasVideoBeenRequested: bool,
  customVideoTitle: string,
  setCustomVideoTitle: ((customTitle: string) => void),
  currentMentionOffset: string,
  networkMentionsList: ImmutableList<MentionsRecord>
};
type DefaultProps = {
  messageEditorDefaultPlaceholder: string,
  getMentionsQueryUiState: boolean
};

const createTwitterCountdownIfNecessary = (network: string, messageText: string) => {
  let domList = [];

  if (network === 'TWITTER') {
    domList.push(<TwitterTextCountdown text={messageText} key={network + '-TwitterCountdown'}/>);
  }

  return domList;
};

class Message extends Component<DefaultProps, Props, any> {
  static displayName = 'Message';
  static defaultProps: DefaultProps = {
    messageEditorDefaultPlaceholder: messageBodyText.messageEditorDefaultPlaceholder,
    getMentionsQueryUiState: false
  };
  mentionQuery: HTMLElement;

  constructor(props: Props) {
    super(props);

    (this: any).focus = (this: any).focus.bind(this);
    (this: any).onChange = (this: any).onChange.bind(this);
    (this: any).onBlur = (this: any).onBlur.bind(this);
    (this: any).handleEsc = (this: any).handleEsc.bind(this);
  };

  onChange(editorState: EditorState): void {
    // NOTE: Passing in the decorators here keeps a circular dependency on 'Actions'
    //       from happening with the 'Decorator/Containers'
    this.props.onChange(
      {
        editorState,
        network: this.props.network,
        getCompositeDecorator: (linkRegex, contentState, network, editorState) => getCompositeDecorator(linkRegex, contentState, network, editorState)
      });
  };

  onBlur(e: Object): void {
    if (!e.relatedTarget || !this.mentionQuery.contains(e.relatedTarget)) {
      this.props.closeMentionsQueryDialog(this.props.network);
      this.props.onBlur();
    }
  };

  handleEsc(e: SyntheticKeyboardEvent): void {
    if (e.keyCode === KeyCodes.ESC_KEY) {
      this.props.closeMentionsQueryDialog(this.props.network);
    }
  };

  focus(): void {
    this.refs.editor.focus();
  };

  applyToAllChannels(): void {
    this.props.applyToAllChannels({
      editorState: this.props.messageEditorState,
      network: this.props.network
    });
  };

  renderNetworkOptionsBar() {
    if (this.props.network === SupportedNetworks.FACEBOOK && this.props.hasVideoBeenRequested) {
      return (
        <NetworkOptionsBar
          customVideoTitle={this.props.customVideoTitle}
          setCustomVideoTitle={this.props.setCustomVideoTitle} />
      );
    }
  };

  render() {
    let {network, numberOfNetworks, validation, disableApplyToAll} = this.props;
    // Disable apply to all if there is no text or if we were told to disable
    let finalDisableApplyToAll = !(this.props.messageEditorState.getCurrentContent().getPlainText()) || disableApplyToAll;

    let editorStyles = cx({
      editor: true,
      editorWithNetworksOptionBar: this.props.network === SupportedNetworks.FACEBOOK && this.props.hasVideoBeenRequested,
      editorWithTwitterCountdown: this.props.network === SupportedNetworks.TWITTER
    });

    let editorWrapperStyles = cx({
      twitterWrapper: network === 'TWITTER',
      editorWrapper: true
    });

    return (
      <div className={styles.message}>
        <MessageBoxHeaderContainer
          disableApplyToAll={finalDisableApplyToAll}
          numberOfNetworks={numberOfNetworks}
          network={network}
          applyToAllChannels={() => {
            this.applyToAllChannels();
          }}
          validation={validation}
        />
        <div className={editorWrapperStyles}>
          <div className={editorStyles} onClick={this.focus} onKeyUp={this.handleEsc} id={`editorWrapperId-${this.props.network}`}>
            <Editor editorState={this.props.messageEditorState}
              onChange={this.onChange}
              onBlur={this.onBlur}
              placeholder={this.props.messageEditorDefaultPlaceholder}
              ref='editor'
              spellCheck
              stripPastedStyles
              onKeyUp={this.handleEsc}
            />
            {createTwitterCountdownIfNecessary(network, getPlainTextFromEditorState(this.props.messageEditorState))}
          </div>
        </div>
        <div ref={(ref) => {
          this.mentionQuery = ref;
        }}>
          <MentionsQuery
            onBlur={this.props.onBlur}
            loadMentionsQueryStatus={this.props.loadMentionsQueryStatus}
            onKeyUp={this.handleEsc}
            editorState={this.props.messageEditorState}
            network={this.props.network}
            mentionsQueryList={this.props.mentionsQueryList}
            sendUpdatedEditorState={this.props.onChange}
            decorators={getCompositeDecorator(this.props.linkRegexString, this.props.messageEditorState.getCurrentContent(), this.props.network, this.props.messageEditorState.getSelection())}
            getMentionsQueryUiState={this.props.getMentionsQueryUiState}
            currentMentionOffset={this.props.currentMentionOffset}
            closeMentionsQueryDialog={this.props.closeMentionsQueryDialog} />
        </div>
        { this.renderNetworkOptionsBar() }
      </div>
    );
  }
};

export default Message;
