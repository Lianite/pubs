import React, { Component } from 'react';
import _ from 'lodash';
import { toTitleCase } from 'utils/NotificationUtils';
import { VoiceFilteringConstants, networkIcons } from 'constants/ApplicationConstants';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import classNames from 'classnames/bind';
import { TOOLTIP_ALIGN } from 'constants/UILayoutConstants';
import {
  AUTHENTICATION_WARNING_ACCOUNT_NAME_COMPANION,
  AUTHENTICATION_WARNING_TOOLTIP
} from 'constants/UITextConstants';

import styles from 'styles/modules/components/SelectedAccount.scss';

import type { List as ImmutableList } from 'immutable';
import type { CredentialRecord } from 'reducers/MessageReducer';

const cx = classNames.bind(styles);

type Props = {
  removeSelectedAccountsPerNetwork?: ((network: string) => void),
  remove?: Function,
  resendValidAssigneesRequest: (() => void),
  accountName: string,
  service: string,
  accountId?: string,
  authenticated?: bool,
  credentials?: Array<CredentialRecord>,
  updateExclusiveFilterChannel: ((channel: string) => void),
  updateFilterSelection: ((filterOption: string) => void),
  currentAccountChannelFilter: ImmutableList<string>,
  voiceSelectorOpen?: bool,
  openAccountSelector?: (() => void)
};

type State = {
  showPopover: bool
};

export default class SelectedAccount extends Component<void, Props, State> {
  static displayName = 'SelectedAccount';
  state: State;
  selectedAccountRef: HTMLElement;

  constructor(props: Props) {
    super(props);

    this.state = {
      showPopover: false
    };

    (this:any).toggleCredentialPopover = this.toggleCredentialPopover.bind(this);
    (this:any).selectRolledFilter = this.selectRolledFilter.bind(this);
  };

  toggleCredentialPopover(showToggle: bool): void {
    this.setState({
      showPopover: showToggle
    });
  };

  selectRolledFilter() {
    if (this.props.credentials) {
      if (!_.get(this.props, ['currentAccountChannelFilter'], {}).includes(this.props.service)) {
        this.props.updateExclusiveFilterChannel(this.props.service);
        this.props.updateFilterSelection(VoiceFilteringConstants.FILTER_SELECTED);
      }
      if (!this.props.voiceSelectorOpen) {
        this.props.openAccountSelector();
      }
    }
  }

  render() {
    const selectedAccountStyles = cx({
      selectedAccount: true,
      selectedAccountHoverable: !!this.props.credentials
    });

    const networkIconClassname = `${networkIcons[this.props.service]} ${styles.networkIcon} ${styles[this.props.service.toLowerCase()]}`;

    const tooltipOverlay = this.props.credentials
    ? <div>
      <p className={styles.credentialTooltipHeader}>{toTitleCase(this.props.service)} {this.props.credentials.length === 1 ? 'Account' : 'Accounts'}</p>

      <ul className={styles.credentialList}>
        {
          _.map(this.props.credentials, (cred, credIndex) => {
            if (credIndex < VoiceFilteringConstants.ROLLED_CREDENTIAL_LIST_MAX) {
              return (<li key={`cred-${cred.id}`}>{`${cred.name}${!cred.authenticated ? ` ${AUTHENTICATION_WARNING_ACCOUNT_NAME_COMPANION}` : ''}`}</li>);
            } else if (credIndex === VoiceFilteringConstants.ROLLED_CREDENTIAL_LIST_MAX) {
              return (<li className={styles.moreCredentialsFooter} key={`cred-${cred.service}-more`}>and {this.props.credentials.length - credIndex} more...</li>);
            }
          })
        }
      </ul>
    </div>
  : !this.props.authenticated
    ? AUTHENTICATION_WARNING_TOOLTIP
    : '';

    const tooltipProps = {
      overlay: tooltipOverlay,
      overlayStyle: {
        fontFamily: 'Open sans'
      },
      align: TOOLTIP_ALIGN,
      transitionName: 'credential-popover-toggle'
    };
    if (!this.props.credentials && this.props.authenticated) {
      tooltipProps.visible = false;
    }

    return (
      <Tooltip {...tooltipProps}>
        <div className={styles.selectedAccountContainer}>
          <div className={selectedAccountStyles} onMouseEnter={() => {
            this.toggleCredentialPopover(true);
          }} onMouseLeave={() => {
            this.toggleCredentialPopover(false);
          }} onClick={() => {
            this.selectRolledFilter();
          }} ref={(ref) => {
            this.selectedAccountRef = ref;
          }}>
            <div className={styles.selectedAccountContent}>
              <div className={networkIconClassname}></div>
              <div className={styles.accountName}>{this.props.accountName}</div>
              {
                !this.props.authenticated
                ? <RADIcon className={styles.authenticationWarning} glyph='warning-dot'/>
                : null
              }
              <div className={`${styles.closeButton} icon-modal_close`} onClick={(e) => {
                e.stopPropagation();
                if (this.props.removeSelectedAccountsPerNetwork) {
                  this.props.removeSelectedAccountsPerNetwork(this.props.service);
                } else if (this.props.remove) {
                  this.props.remove(undefined, this.props.accountId);
                }
                this.props.resendValidAssigneesRequest();
              }}></div>
            </div>
          </div>
        </div>
      </Tooltip>
    );
  };
};
