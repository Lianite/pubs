/* @flow */
import React from 'react';
import styles from 'styles/modules/components/Credential.scss';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import '@spredfast/react-lib/styles/Checkbox.scss';
import { networkIcons } from 'constants/ApplicationConstants';
import { accountSelectionText } from 'constants/UITextConstants';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import classNames from 'classnames/bind';

type Props = {
  uniqueId: string;
  name: string;
  service: string;
  handleChecked: (id: string) => void;
  checked?: bool;
  authenticated: bool;
  disabled?: bool;
  disabledMessage?: string;
}

const cx = classNames.bind(styles);

const Credential = ({uniqueId, name, service, handleChecked, checked, authenticated, disabled, disabledMessage}: Props) => {
  return (
    <div
      className={cx('credential', {
        authenticationWarning: !authenticated,
        disabled,
        disabledMessage
      })}
      onClick={() => {
        !disabled && handleChecked(uniqueId);
      }}>
      <Checkbox checked={checked} disabled={disabled}
        />
      <div className={`${networkIcons[service]} ${styles.networkIcon} ${styles[service.toLowerCase()]}`}></div>
      <span className={styles.credentialName}>
        {
          !authenticated
          ? <RADIcon className={styles.credentialWarningIcon} glyph='warning-dot' />
          : null
        }
        {name}
      </span>
      <div className={styles.warning}>
        {
          !authenticated
          ? <span>{accountSelectionText.authenticationWarningText}</span>
          : null
        }
        {
          disabled
          ? <span>{disabledMessage}</span>
          : null
        }
      </div>
    </div>
  );
};

Credential.defaultProps = {
  checked: false
};

export default Credential;
