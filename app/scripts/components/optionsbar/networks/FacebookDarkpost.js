/* @flow */
import React from 'react';
import { DisabledAccountsList } from 'components/util/DisabledAccountsList';
import {
  FACEBOOK_DARKPOST_CHECKBOX_TEXT,
  FACEBOOK_DARKPOST_CHECKBOX_TOOLTIP_TEXT,
  FACEBOOK_DARKPOST_DISABLED_DETAILS,
  FACEBOOK_DARKPOST_DISABLED_MESSAGE
} from 'constants/UITextConstants';
import AlertBox from 'components/util/AlertBox';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import {
  TOOLTIP_OVERLAY_STYLES
} from 'constants/UILayoutConstants';

import type { CredentialRecord } from 'records/AccountsRecord';

import styles from 'styles/modules/components/optionsbar/networks/FacebookDarkpost.scss';

type Props = {
  canDarkPost: bool,
  darkPostStatus: bool,
  invalidDarkPostAccounts: Array<CredentialRecord>,
  setDarkPostVisibility: (visible: bool) => void
};

export default class FacebookDarkpost extends React.Component<void, Props, any> {
  props: Props;

  render() {
    return (
      this.props.canDarkPost ?
        <div className={styles.facebookDarkPostContainer}>
          <Checkbox
            disabled={!this.props.canDarkPost}
            checked={ this.props.darkPostStatus }
            onChange={ () => {
              this.props.setDarkPostVisibility(!this.props.darkPostStatus);
            }}
            style={{'verticalAlign': 'middle', 'fontSize': '21px', 'marginRight': '6px'}}
          />
          {FACEBOOK_DARKPOST_CHECKBOX_TEXT}
          <Tooltip
            overlay={ FACEBOOK_DARKPOST_CHECKBOX_TOOLTIP_TEXT}
            destroyTooltipOnHide
            placement='topLeft'
            overlayStyle={TOOLTIP_OVERLAY_STYLES}
            align={ {offset:[-24, 8]} }>
            <RADIcon style={{'verticalAlign': 'middle', 'fontSize': '18px', 'marginLeft': '6px'}} glyph='info'/>
          </Tooltip>
        </div> :
        <div className={styles.facebookDarkPostDisabledContainer}>
          <AlertBox
            text={FACEBOOK_DARKPOST_DISABLED_MESSAGE}
            additionalText={FACEBOOK_DARKPOST_DISABLED_DETAILS(this.props.invalidDarkPostAccounts.length)}
            link={
              <div>
                <DisabledAccountsList invalidAccounts={this.props.invalidDarkPostAccounts}/>
              </div>
            }
            soft>
          </AlertBox>
        </div>
    );
  }
}
