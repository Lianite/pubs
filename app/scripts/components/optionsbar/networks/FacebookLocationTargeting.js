/* @flow */
import React from 'react';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import Location from 'components/optionsbar/networks/targeting/Location';
import LocationResult from 'components/optionsbar/networks/targeting/LocationResult';
import DropdownButton from 'components/util/DropdownButton';
import { Combobox } from 'react-widgets';
import {
  FACEBOOK_TARGETING_LOCATION_EMPTY_LIST,
  FACEBOOK_TARGETING_LOCATION_PLACEHOLDER
} from 'constants/UITextConstants';
import { TARGETING_LAYOUT_CONSTANTS } from 'constants/UILayoutConstants';
import velocity from 'velocity-animate';
import classNames from 'classnames/bind';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import _ from 'lodash';

import type { TargetingEntity, SelectedLocationsTree } from 'adapters/types';

import styles from 'styles/modules/components/optionsbar/networks/FacebookLocationTargeting.scss';

const cx = classNames.bind(styles);

type Props = {
  onInputChange: () => void,
  locationOptions: Array<TargetingEntity>,
  selectedLocations: SelectedLocationsTree,
  addIncludedLocation: (location: TargetingEntity) => void,
  removeIncludedLocation: (description: string) => void,
  removeIncludedCountry: (description: string) => void
};

type State = {
  inputValue: string | TargetingEntity;
  inputOpen: bool;
};

class FacebookLocationTargeting extends React.Component<void, Props, any> {

  props: Props;
  state: State;
  hasSelected: bool;
  onInputChange: (value: string | TargetingEntity) => void;
  onInputSelect: (location: TargetingEntity) => void;
  debouncedOnInputChange: (query: string) => void;
  onInputButtonClick: () => void;
  onInputFocus: () => void;
  onInputBlur: () => void;
  lastAddedLocation: TargetingEntity;
  lastAddedLocationRef: ?HTMLElement;
  locationsListRef: ?HTMLElement;
  lastAddedLocationRefCallback: (element: HTMLElement) => void;
  isLastAddedLocation: (location: Object) => bool;
  locationsListRefCallback: (element: HTMLElement) => void;
  currentlyBlurring: bool;

  constructor(props: Props) {
    super(props);
    this.debouncedOnInputChange = _.debounce(this.props.onInputChange, 250, { leading: true }).bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onInputSelect = this.onInputSelect.bind(this);
    this.onInputButtonClick = this.onInputButtonClick.bind(this);
    this.onInputFocus = this.onInputFocus.bind(this);
    this.onInputBlur = this.onInputBlur.bind(this);
    this.hasSelected = false;
    this.lastAddedLocationRefCallback = this.lastAddedLocationRefCallback.bind(this);
    this.isLastAddedLocation = this.isLastAddedLocation.bind(this);
    this.locationsListRefCallback = this.locationsListRefCallback.bind(this);
    this.currentlyBlurring = false;

    this.state = {
      inputValue: '',
      inputOpen: false
    };
  }

  componentDidUpdate(prevProps: Props) {
    if (this.lastAddedLocationRef) {
      velocity(this.lastAddedLocationRef, 'scroll', {
        container: this.locationsListRef,
        duration: TARGETING_LAYOUT_CONSTANTS.AUTO_SCROLL_DURATION,
        offset: TARGETING_LAYOUT_CONSTANTS.AUTO_SCROLL_PIXEL_OFFSET
      });
      this.lastAddedLocationRef = null;
    }
  }

  onInputChange(value: string | TargetingEntity) {
    if (typeof value === 'string') {
      this.debouncedOnInputChange(value);
      this.setState({
        inputValue: value
      });
    } else if (!this.hasSelected) {
      this.setState({
        inputValue: value.description
      });
    }
    this.hasSelected = false;
  }

  onInputSelect(location: TargetingEntity) {
    this.hasSelected = true;
    this.lastAddedLocation = location;
    this.props.addIncludedLocation(location);
    this.setState({
      inputValue: ''
    });
  }

  onInputButtonClick() {
    if (!this.currentlyBlurring) {
      this.setState({
        inputOpen: !this.state.inputOpen
      });
    }
    this.currentlyBlurring = false;
  }

  onInputFocus() {
    this.setState({
      inputOpen: true
    });
  }

  onInputBlur() {
    this.currentlyBlurring = true;
    this.setState({
      inputOpen: false
    });
  }

  isLastAddedLocation(location: Object) {
    return this.lastAddedLocation && this.lastAddedLocation.description === location.description;
  }

  lastAddedLocationRefCallback(element: HTMLElement) {
    this.lastAddedLocationRef = element;
  }

  locationsListRefCallback(element: HTMLElement) {
    this.locationsListRef = element;
  }

  renderSelectedLocations() {
    return (
      _.map(this.props.selectedLocations, (country: Object, countryIndex: number) => (
        <div
          className={styles.country}
          key={countryIndex}>
          <div className={styles.countryHeader}>
            <span className={styles.countryDescription}>
              {country.description}
            </span>
            <i className={cx('remove', 'icon-remove-bare')}  onClick={this.props.removeIncludedCountry.bind(null, country.description)} />
          </div>
          <ReactCSSTransitionGroup
            transitionEnterTimeout={1000}
            transitionLeave={false}
            transitionName={{
              enter: styles.locationHighlightEnter,
              enterActive: styles.locationHighlightEnterActive
            }}>
            {
              country.children.length
              ? _.map(country.children, (location: Object, locationIndex: number) => {
                const refCallback = this.isLastAddedLocation(location) ? this.lastAddedLocationRefCallback : null;
                return (
                  <div key={locationIndex} className={styles.locationWrapper}>
                    <Location refCallback={refCallback} description={location.description} onRemove={this.props.removeIncludedLocation}/>
                  </div>
                );
              }
              )
              : (
                <div key={`country-${country.description}`} className={styles.locationWrapper}>
                  <Location refCallback={this.isLastAddedLocation(country) ? this.lastAddedLocationRefCallback : null} description={country.description} onRemove={this.props.removeIncludedLocation}/>
                </div>
              )
          }
          </ReactCSSTransitionGroup>
        </div>
      ))
    );
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <OptionsMenuSectionHeader
          title='Locations'
          subheading/>
        <div className={styles.box}>
          <div className={cx('locations', {
            notEmpty: !!Object.keys(this.props.selectedLocations).length
          })} ref={this.locationsListRefCallback}>
            <ReactCSSTransitionGroup
                    transitionEnterTimeout={1000}
                    transitionLeave={false}
                    transitionName={{
                      enter: styles.locationHighlightEnter,
                      enterActive: styles.locationHighlightEnterActive
                    }}>
              { this.renderSelectedLocations() }
            </ReactCSSTransitionGroup>
          </div>
          <div className={styles.inputWrapper}>
            <Combobox
              placeholder={FACEBOOK_TARGETING_LOCATION_PLACEHOLDER}
              open={this.state.inputOpen}
              onToggle={() => {}}
              className={styles.input}
              itemComponent={LocationResult}
              value={this.state.inputValue}
              onFocus={this.onInputFocus}
              onBlur={this.onInputBlur}
              onChange={this.onInputChange}
              onSelect={this.onInputSelect}
              data={this.props.locationOptions}
              valueField='value'
              textField='description'
              messages={{
                emptyList: FACEBOOK_TARGETING_LOCATION_EMPTY_LIST,
                emptyFilter: FACEBOOK_TARGETING_LOCATION_EMPTY_LIST
              }}/>
            <DropdownButton
              open={this.state.inputOpen}
              onClick={this.onInputButtonClick}/>
          </div>
        </div>
      </div>
    );
  }
}

export default FacebookLocationTargeting;
