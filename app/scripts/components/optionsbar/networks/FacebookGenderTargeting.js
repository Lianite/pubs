/* @flow */
import React from 'react';
import _ from 'lodash';
import {
  FACEBOOK_TARGETING_GENDER_HEADER,
  FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS
} from 'constants/UITextConstants';
import {
  GENDER_TARGETING_TYPES
} from 'constants/ApplicationConstants';
import { GenderState } from 'records/TargetingRecords';

import styles from 'styles/modules/components/optionsbar/networks/FacebookGenderTargeting.scss';

import LabeledRadioButton from '@spredfast/react-lib/lib/LabeledRadioButton';
import '@spredfast/react-lib/styles/LabeledRadioButton.scss';
import '@spredfast/react-lib/styles/RadioButton.scss';


type Props = {
  selectGenderTargeting: (selectedGender: string) => void,
  targetingSelectedGender: GenderState
};

const FacebookGenderTargeting = ({selectGenderTargeting, targetingSelectedGender} : Props) => {

  const genderOptions = _.map(GENDER_TARGETING_TYPES, (gender) =>  {
    const checked = targetingSelectedGender.value === gender.value;
    return (
      <LabeledRadioButton
        checked={checked}
        onChange={selectGenderTargeting.bind(this, FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS[gender.description])}
        text={FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS[gender.description]} key={`gender_targeting_radio_${gender.description}`}
        style={{flexGrow: 1, flexShrink: 1}}
      />
    );
  });

  return (
    <div className={styles.facebookGenderTargetingContainer}>
      <div className={styles.title}>
        {FACEBOOK_TARGETING_GENDER_HEADER}
      </div>
      <div className={styles.genderTargetingRadioButtonContainer} role='radiogroup'>
        {genderOptions}
      </div>
    </div>
  );
};

export default FacebookGenderTargeting;
