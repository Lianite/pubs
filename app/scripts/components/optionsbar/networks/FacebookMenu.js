/* @flow */
import React from 'react';
import {
  FACEBOOK_MENU_HEADER,
  FACEBOOK_TARGETING_PLACEHOLDER_HEADER,
  FACEBOOK_TARGETING_PLACEHOLDER_BODY
} from 'constants/UITextConstants';
import FacebookTargeting from 'components/optionsbar/networks/FacebookTargeting';
import FacebookDarkpost from 'components/optionsbar/networks/FacebookDarkpost';
import FeaturePlaceholder from 'components/util/FeaturePlaceholder';
import { CredentialRecord } from 'records/AccountsRecord';
import { GenderState } from 'records/TargetingRecords';
import targetingPlaceholderImage from 'images/targeting-placeholder.svg';

import type { TargetingEntity, SelectedLocationsTree } from 'adapters/types';

import styles from 'styles/modules/components/optionsbar/networks/FacebookMenu.scss';

type Props = {
  onLanguageTargetingInputChange: (query: string) => void,
  addTargetingLanguage: (language: TargetingEntity) => void,
  removeTargetingLanguage: (language: TargetingEntity) => void,
  clearTargeting: () => void,
  targetingSelectedLanguages: Array<TargetingEntity>,
  targetingLanguageOptions: Array<TargetingEntity>,
  onLocationTargetingInputChange: () => void,
  targetingSelectedLocations: SelectedLocationsTree,
  targetingLocationOptions: Array<TargetingEntity>,
  addTargetingIncludedLocation: (location: TargetingEntity) => void,
  removeTargetingIncludedLocation: (description: string) => void,
  removeTargetingIncludedCountry: (description: string) => void,
  invalidTargetingAccounts: Array<CredentialRecord>,
  invalidDarkPostAccounts: Array<CredentialRecord>,
  setDarkPostVisibility: (visible: bool) => void,
  canTarget: bool,
  invalidTargetingAccounts: Array<CredentialRecord>,
  targetingSet: bool,
  darkPostStatus: bool,
  canDarkPost: bool,
  targetingIsEnabled: bool,
  selectGenderTargeting: () => void,
  targetingSelectedGender: GenderState
};

class FacebookMenu extends React.Component<void, Props, any> {

  props: Props;

  render() {
    return (
      <div className={styles.wrapper}>
        <div className={styles.header}>
          {FACEBOOK_MENU_HEADER}
        </div>
        <FacebookDarkpost
          canDarkPost={this.props.canDarkPost}
          darkPostStatus={this.props.darkPostStatus}
          invalidDarkPostAccounts={this.props.invalidDarkPostAccounts}
          setDarkPostVisibility={this.props.setDarkPostVisibility}/>
        {
          !this.props.targetingIsEnabled
          ? <FeaturePlaceholder
            image={targetingPlaceholderImage}
            header={FACEBOOK_TARGETING_PLACEHOLDER_HEADER}
            body={FACEBOOK_TARGETING_PLACEHOLDER_BODY} />
          : <FacebookTargeting
            canTarget={this.props.canTarget}
            targetingSet={this.props.targetingSet}
            onLanguageTargetingInputChange={this.props.onLanguageTargetingInputChange}
            addTargetingLanguage={this.props.addTargetingLanguage}
            removeTargetingLanguage={this.props.removeTargetingLanguage}
            targetingSelectedLanguages={this.props.targetingSelectedLanguages}
            targetingLanguageOptions={this.props.targetingLanguageOptions}
            locationOptions={this.props.targetingLocationOptions}
            selectedLocations={this.props.targetingSelectedLocations}
            invalidTargetingAccounts={this.props.invalidTargetingAccounts}
            onLocationTargetingInputChange={this.props.onLocationTargetingInputChange}
            addIncludedLocation={this.props.addTargetingIncludedLocation}
            removeIncludedLocation={this.props.removeTargetingIncludedLocation}
            removeIncludedCountry={this.props.removeTargetingIncludedCountry}
            invalidTargetingAccounts={this.props.invalidTargetingAccounts}
            clearTargeting={this.props.clearTargeting}
            selectGenderTargeting={this.props.selectGenderTargeting}
            targetingSelectedGender={this.props.targetingSelectedGender}/>
        }
      </div>
    );
  }
}

export default FacebookMenu;
