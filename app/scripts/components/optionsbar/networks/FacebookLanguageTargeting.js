/* @flow */
import React from 'react';
import DropdownSection from 'components/util/DropdownSection';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import { RADIcon } from '@spredfast/react-lib';
import {
  FACEBOOK_TARGETING_LANGUAGE_HEADER,
  FACEBOOK_TARGETING_LANGUAGE_EMPTY_LIST,
  FACEBOOK_TARGETING_LANGUAGE_PLACEHOLDER,
  FACEBOOK_TARGETING_LANGUAGE_INFO
} from 'constants/UITextConstants';
import {
  TOOLTIP_ALIGN,
  TOOLTIP_OVERLAY_STYLES
} from 'constants/UILayoutConstants';
import _ from 'lodash';

import type { TargetingEntity } from 'adapters/types';

type Props = {
  onInputChange: (query: string) => void,
  addTargetingLanguage: (language: TargetingEntity) => void,
  removeTargetingLanguage: (language: TargetingEntity) => void,
  targetingSelectedLanguages: Array<TargetingEntity>,
  targetingLanguageOptions: Array<TargetingEntity>
};

class FacebookLanguageTargeting extends React.Component<void, Props, any> {

  props: Props;
  addTargetingLanguage: (languageValue: string) => void;
  findTargetingLanguageByValue: (value: string) => TargetingEntity;
  onInputChange: (value: string) => void;

  constructor(props: Props) {
    super(props);
    this.addTargetingLanguage = this.addTargetingLanguage.bind(this);
    this.findTargetingLanguageByValue = this.findTargetingLanguageByValue.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  addTargetingLanguage(languageValue: string) {
    const targetingLanguageToAdd = this.findTargetingLanguageByValue(languageValue);
    this.props.addTargetingLanguage(targetingLanguageToAdd);
  }

  findTargetingLanguageByValue(value: string) {
    return _.find(this.props.targetingLanguageOptions, (language: TargetingEntity): bool => language.value === value);
  }

  onInputChange(value: string | TargetingEntity) {
    if (typeof value === 'string') {
      this.props.onInputChange(value);
    }
  }

  render() {
    return (
      <DropdownSection
        title={FACEBOOK_TARGETING_LANGUAGE_HEADER}
        icon={
          <span
            style={{
              display: 'none' // this tooltip is not meaningful until facebook location targeting is implemented
            }}>
            <Tooltip
            overlay={FACEBOOK_TARGETING_LANGUAGE_INFO}
            placement='top'
            align={TOOLTIP_ALIGN}
            overlayStyle={TOOLTIP_OVERLAY_STYLES}>
              <RADIcon glyph='info' />
            </Tooltip>
          </span>
        }
        onInputValueChange={this.onInputChange}
        iconRight
        subheading
        addDatum={this.addTargetingLanguage}
        onRemoveDatum={this.props.removeTargetingLanguage}
        clearInputOnSelect
        selectedData={
          _.map(this.props.targetingSelectedLanguages, (language) => ({
            text: language.description
          }))
        }
        comboBoxConfig={{
          data: this.props.targetingLanguageOptions,
          valueField: 'value',
          textField: 'description',
          messages: {
            emptyList: FACEBOOK_TARGETING_LANGUAGE_EMPTY_LIST
          },
          placeholder: FACEBOOK_TARGETING_LANGUAGE_PLACEHOLDER
        }}>
      </DropdownSection>
    );
  }
}

export default FacebookLanguageTargeting;
