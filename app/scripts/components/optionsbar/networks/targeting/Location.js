/* @flow */
import React from 'react';
import classNames from 'classnames/bind';

import styles from 'styles/modules/components/optionsbar/networks/targeting/Location.scss';

const cx = classNames.bind(styles);

type Props = {
  description: string;
  onRemove: (value: string) => void;
  refCallback?: ?(element: HTMLElement) => void;
};

const Location = ({description, onRemove, refCallback}: Props) => (
  <div className={styles.location} ref={refCallback}>
    <i className='icon-location' />
    <span className={styles.description}>
      {description}
    </span>
    <span className={cx('remove', 'icon-remove-bare')} onClick={onRemove.bind(null, description)}></span>
  </div>
);

export default Location;
