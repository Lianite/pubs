/* @flow */
import React from 'react';
import { TARGETING_SUBTYPES } from 'constants/ApplicationConstants';

import styles from 'styles/modules/components/optionsbar/networks/targeting/LocationResult.scss';

import type { TargetingEntity, TargetingSubtype } from 'adapters/types';

type Props = {
  item: TargetingEntity
};

const SubTypeToStringMap : {
  [TargetingSubtype] : string
} = {
  [TARGETING_SUBTYPES.COUNTRY]: 'Country',
  [TARGETING_SUBTYPES.REGION]: 'Region',
  [TARGETING_SUBTYPES.METRO]: 'City',
  [TARGETING_SUBTYPES.ZIP_CODE]: 'Zip Code'
};

const LocationResult = (props: Props) => (
  <div>
    <div className={styles.locationResultDescription}>{props.item.description}</div>
    <div className={styles.locationResultSubType}>
      { SubTypeToStringMap[props.item.subType] || '' }
    </div>
  </div>
);

export default LocationResult;
