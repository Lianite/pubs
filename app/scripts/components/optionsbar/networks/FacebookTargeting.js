/* @flow */
import React from 'react';
import FacebookLanguageTargeting from 'components/optionsbar/networks/FacebookLanguageTargeting';
import FacebookLocationTargeting from 'components/optionsbar/networks/FacebookLocationTargeting';
import FacebookGenderTargeting from 'components/optionsbar/networks/FacebookGenderTargeting';
import {
  FACEBOOK_TARGETING_HEADER,
  FACEBOOK_TARGETING_DISABLED_MESSAGE,
  FACEBOOK_TARGETING_DISABLED_DETAILS,
  FACEBOOK_TARGETING_REMOVE,
  FACEBOOK_TARGETING_REMOVE_DETAILS
} from 'constants/UITextConstants';
import AlertBox from 'components/util/AlertBox';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import { CredentialRecord } from 'records/AccountsRecord';
import { GenderState } from 'records/TargetingRecords';
import { DisabledAccountsList } from 'components/util/DisabledAccountsList';

import styles from 'styles/modules/components/optionsbar/networks/FacebookTargeting.scss';

import type { TargetingEntity, SelectedLocationsTree } from 'adapters/types';

type Props = {
  canTarget: bool,
  targetingSet: bool,
  onLanguageTargetingInputChange: (query: string) => void,
  addTargetingLanguage: (language: TargetingEntity) => void,
  removeTargetingLanguage: (language: TargetingEntity) => void,
  targetingSelectedLanguages: Array<TargetingEntity>,
  targetingLanguageOptions: Array<TargetingEntity>,
  locationOptions: Array<TargetingEntity>,
  selectedLocations: SelectedLocationsTree,
  invalidTargetingAccounts: Array<CredentialRecord>,
  onLocationTargetingInputChange: () => void,
  addIncludedLocation: (location: TargetingEntity) => void,
  removeIncludedLocation: (description: string) => void,
  removeIncludedCountry: (description: string) => void,
  clearTargeting: () => void,
  selectGenderTargeting: () => void,
  targetingSelectedGender: GenderState
};

//TODO: wrap this component in its own FacebookTargeting Container
class FacebookTargeting extends React.Component<void, Props, any> {

  props: Props;
  addTargetingLanguage: (languageValue: string) => void;
  findTargetingLanguageByValue: (value: string) => TargetingEntity;

  render() {
    return (
      <div>
        <OptionsMenuSectionHeader
          title={FACEBOOK_TARGETING_HEADER}>
          <span
            className='icon-targeting'>
          </span>
        </OptionsMenuSectionHeader>
        {
          this.props.canTarget
          ? (
            <div>
              <FacebookGenderTargeting
                selectGenderTargeting={this.props.selectGenderTargeting}
                targetingSelectedGender={this.props.targetingSelectedGender}/>
              <FacebookLanguageTargeting
                onInputChange={this.props.onLanguageTargetingInputChange}
                addTargetingLanguage={this.props.addTargetingLanguage}
                removeTargetingLanguage={this.props.removeTargetingLanguage}
                targetingSelectedLanguages={this.props.targetingSelectedLanguages}
                targetingLanguageOptions={this.props.targetingLanguageOptions}/>
              <FacebookLocationTargeting
                onInputChange={this.props.onLocationTargetingInputChange}
                locationOptions={this.props.locationOptions}
                selectedLocations={this.props.selectedLocations}
                addIncludedLocation={this.props.addIncludedLocation}
                removeIncludedLocation={this.props.removeIncludedLocation}
                removeIncludedCountry={this.props.removeIncludedCountry}/>
            </div>
            )
            : this.props.targetingSet
                  ? <div>
                    <AlertBox
                      text={FACEBOOK_TARGETING_DISABLED_MESSAGE}
                      additionalText={FACEBOOK_TARGETING_REMOVE_DETAILS}
                      link={
                        <div>
                          <DisabledAccountsList invalidAccounts={this.props.invalidTargetingAccounts}/>
                          <div className={styles.divider} />
                          <div className={styles.link} onClick={this.props.clearTargeting}>
                            {FACEBOOK_TARGETING_REMOVE}
                          </div>
                        </div>
                      }>
                    </AlertBox>
                  </div>
                  : <div>
                    <AlertBox
                      text={FACEBOOK_TARGETING_DISABLED_MESSAGE}
                      additionalText={FACEBOOK_TARGETING_DISABLED_DETAILS(this.props.invalidTargetingAccounts.length)}
                      link={<DisabledAccountsList invalidAccounts={this.props.invalidTargetingAccounts}/>}
                      soft>
                    </AlertBox>
                  </div>
                }
      </div>
      );
  }
}

export default FacebookTargeting;
