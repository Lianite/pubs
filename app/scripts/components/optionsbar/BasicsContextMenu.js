/* @flow */
import React from 'react';
import styles from 'styles/modules/components/optionsbar/BasicsContextMenu.scss';
import PlanPicker from 'components/optionsbar/PlanPicker';
import LabelsWrapper from 'components/optionsbar/LabelsWrapper';
import '@spredfast/react-lib/styles/Icon.scss';
import { LabelsRecord } from 'records/LabelsRecords';
import { List as ImmutableList } from 'immutable';

import type { Label } from 'records/LabelsRecords';

type Props = {
  currentPlan: Object,
  onSelectPlan: Function,
  plans: Array<Object>,
  selectedLabels: Array<Label>,
  availableLabels: Array<Label>,
  canCreateLabels: bool,
  onRemoveLabel: Function,
  addLabel: Function,
  updateLabelText: Function,
  currentLabelText: string,
  fetchLabelsAction: Function,
  labelErrorGroup: ImmutableList<LabelsRecord>
};

const BasicsContextMenu = (props: Props) => {
  return (
    <div className={styles.basicsContextMenu}>
      <div className={styles.title}>BASICS</div>
      <div className={styles.scrollableContainer}>
        <LabelsWrapper
          selectedLabels={props.selectedLabels}
          availableLabels={props.availableLabels}
          onRemoveLabel={props.onRemoveLabel}
          addLabel={props.addLabel}
          updateLabelText={props.updateLabelText}
          fetchLabelsAction={props.fetchLabelsAction}
          labelErrorGroup={props.labelErrorGroup}
          canCreateLabels={props.canCreateLabels}
          currentLabelText={props.currentLabelText}
          />
        <div className={styles.divider}></div>
        <PlanPicker currentPlan={props.currentPlan} onSelectPlan={props.onSelectPlan} plans={props.plans}/>
      </div>
    </div>
  );
};

export default BasicsContextMenu;
