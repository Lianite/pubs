/* @flow */
import React from 'react';
import styles from 'styles/modules/components/optionsbar/LinkPreviewUrlPicker.scss';
import { linkPreviewUrlPickerText } from 'constants/UITextConstants';

import type {Set as ImmutableSet } from 'immutable';

import HoverText from 'components/HoverText';
import { textWidthForHoverText } from 'constants/UILayoutConstants';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import '@spredfast/react-lib/styles/Icon.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

type LinkPreviewUrlPickerProps = {
  urls: ImmutableSet<string>,
  selectedUrl: string,
  onSelect: Function
};

type OptionPickerType = {
  options: ImmutableSet<string>,
  onSelect: Function,
  selectedOption: string,
  noneOption?: bool,
  inline?: bool
};

type OptionType = {
  value: string,
  onSelect: Function,
  checked: bool
};

export const Option = ({value, onSelect, checked}: OptionType) => {
  const className = cx('radioBtn', {checked});
  const onClick = () => {
    onSelect(value);
  };
  return (
    <li key={value}>
      <div onClick={onClick}>
        <button className={className} role='radio' aria-checked={checked}></button>
        <HoverText
          displayAtWidth={textWidthForHoverText.LINK_PREVIEW_URL_PICKER}
          evalText={value}>
          <span>{value}</span>
        </HoverText>
      </div>
    </li>
  );
};

export const OptionPicker = ({options, selectedOption, onSelect, noneOption, inline}: OptionPickerType) => {
  const optionPickerContainerClassname = cx('linkPreviewContainer', {
    inline: inline
  });
  return (
    <ul className={optionPickerContainerClassname}>
      {noneOption ?
        <Option
          value={linkPreviewUrlPickerText.none}
          checked={!selectedOption}
          onSelect={() => onSelect()}
        />
        :
        null
      }

      {options.map(value => (
        <Option
          key={value}
          value={value}
          checked={value === selectedOption}
          onSelect={onSelect}
        />
      ))}
    </ul>
  );
};

const LinkPreviewUrlPicker = ({urls, selectedUrl, onSelect}: LinkPreviewUrlPickerProps) => {
  return (
    <div className={styles.linkPreviewUrlPicker}>
      <h3>{linkPreviewUrlPickerText.title}<RADIcon className={styles.radIconInfo} glyph='info'/></h3>
      <OptionPicker options={urls} selectedOption={selectedUrl} onSelect={onSelect} noneOption />
    </div>
  );
};

export default LinkPreviewUrlPicker;
