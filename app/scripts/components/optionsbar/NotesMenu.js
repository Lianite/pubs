/* @flow */
import React from 'react';
import { default as ProductNav, ProductNavItem } from '@spredfast/react-lib/lib/ProductNav';
import { NotesMenuItems,
  NotesMenuItemsList,
  NO_NOTES_TEXT,
  NO_NOTES_VIEW_ACTIVITY,
  NO_ACTIVITY_TEXT,
  NO_ACTIVITY_VIEW_NOTES,
  NO_NOTES_OR_ACTIVITY } from 'constants/UITextConstants';
import {
  NOTE_TRANSITION_TIMEOUT,
  NOTES_LIST_SCROLL_ANIMATION_DURATION,
  NOTES_LIST_SCROLL_ANIMATION_OFFSET
} from 'constants/NotesMenuConstants';
import Note from 'components/optionsbar/Note';
import NoteInput from 'components/optionsbar/NoteInput';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import classNames from 'classnames/bind';
import velocity from 'velocity-animate';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import styles from 'styles/modules/components/optionsbar/NotesMenu.scss';
import noNotesImage from 'images/nonotes.svg';

import type { ActivitiesObject } from 'records/MessageRecords';
import type { NotesMenuItemsType } from 'constants/types';
import type { Note as NoteType } from 'adapters/types';

export type NotesMenuProps = {
  notes: Array<NoteType>,
  activities: Array<ActivitiesObject>,
  userTimezone: TimezoneRecord,
  userFirstName: string,
  userLastName: string,
  userAvatar: string,
  onCurrentNoteTextChange: Function,
  createPendingNote: Function,
  createNoteForSavedMessage: Function,
  messageIsSaved: bool,
  initialNoteText: string,
  disableInput: bool
};

type NotesMenuState = {
  selectedView: NotesMenuItemsType,
  focusedOnNoteInput: bool,
  currentNoteText: string,
  scrolledToTop: bool
};

const cx = classNames.bind(styles);
class NotesMenu extends React.Component<void, NotesMenuProps, any> {
  state: NotesMenuState;
  notesListTop: HTMLElement;
  notesList: HTMLElement;

  constructor(props: NotesMenuProps) {
    super(props);
    this.state = {
      selectedView: NotesMenuItemsList[0],
      focusedOnNoteInput: false,
      currentNoteText: this.props.initialNoteText || '',
      scrolledToTop: true
    };

    (this:any).onNotesListScroll = this.onNotesListScroll.bind(this);
    (this:any).onNoteInputAreaBlur = this.onNoteInputAreaBlur.bind(this);
    (this:any).onNoteInputAreaFocus = this.onNoteInputAreaFocus.bind(this);
    (this:any).onAddNote = this.onAddNote.bind(this);
  }

  componentWillReceiveProps(nextProps: NotesMenuProps) {
    if (this.props.initialNoteText !== nextProps.initialNoteText) {
      this.updateCurrentNoteText(nextProps.initialNoteText);
    }
  }

  onAddNote() {
    this.scrollToTopOfNotesList();
    this.updateCurrentNoteText('');
  }

  updateCurrentNoteText(text: string = '') {
    this.setState({
      currentNoteText: text
    });
  }

  onInputChange(text: string) {
    this.props.onCurrentNoteTextChange({ text });
    this.updateCurrentNoteText(text);
  }

  scrollToTopOfNotesList() {
    velocity(this.notesListTop, 'scroll', {
      container: this.notesList,
      duration: NOTES_LIST_SCROLL_ANIMATION_DURATION,
      offset: NOTES_LIST_SCROLL_ANIMATION_OFFSET
    });
  }

  selectMenuItem(item: string) {
    this.setState({
      selectedView: item,
      scrolledToTop: true
    });
  }

  onNotesListScroll(e: Object) {
    if (e.target.scrollTop === 0) {
      this.setState({
        scrolledToTop: true
      });
    } else if (this.state.scrolledToTop) {
      this.setState({
        scrolledToTop: false
      });
    }
  }

  onNoteInputAreaBlur() {
    this.setState({
      focusedOnNoteInput: false
    });
  }

  onNoteInputAreaFocus() {
    this.setState({
      focusedOnNoteInput: true
    });
  }

  showNoteInputFooter(): bool {
    return this.state.focusedOnNoteInput || !!this.state.currentNoteText.length;
  }

  buildNotesContent() {
    return (
      <div className={styles.notesWrapper}>
        <div
          className={cx({
            notesList: true,
            scrolledToTop: this.state.scrolledToTop
          })}
          onScroll={(e) => {
            this.onNotesListScroll(e);
          }}
          ref={(ref) => {
            this.notesList = ref;
          }}
          key='notes-container' >
          <div ref={(ref) => {
            this.notesListTop = ref;
          }}></div>
          {
            !this.props.notes.length
              ? <div className={styles.noNotesPlaceholder}>
                <img src={noNotesImage} />
                { this.props.activities.length ?
                  <div>
                    <h3 className={styles.noNotesBanner}>{NO_NOTES_TEXT}</h3>
                    <button className={styles.viewOtherTabButton} onClick={this.selectMenuItem.bind(this, 'ACTIVITY')}>
                      <p>{NO_NOTES_VIEW_ACTIVITY}</p>
                    </button>
                  </div>
                :
                  <h3 className={styles.noNotesBanner}>{NO_NOTES_OR_ACTIVITY}</h3>
                }
              </div>
              : null
          }
          <ReactCSSTransitionGroup
            transitionName={{
              enter: styles.noteEnter,
              enterActive: styles.noteEnterActive
            }}
            transitionEnterTimeout={NOTE_TRANSITION_TIMEOUT}
            transitionLeave={false}>
            {
              this.props.notes.map((note, index) => (
                <Note
                  authorFirstName={note.authorFirstName}
                  authorLastName={note.authorLastName}
                  authorAvatarThumbnail={note.authorAvatarThumbnail}
                  timestamp={note.noteCreationTimestamp}
                  authoredContent={note.noteText}
                  userTimezone={this.props.userTimezone}
                  key={`note-${this.props.notes.length - 1 - index}`} />
                ))
            }
          </ReactCSSTransitionGroup>
        </div>
        <div className={cx({
          noteInputArea: true,
          noteInputFooterVisible: this.showNoteInputFooter()
        })}>
          <NoteInput
            onCurrentNoteTextChange={this.onInputChange.bind(this)}
            createPendingNote={this.props.createPendingNote}
            createNoteForSavedMessage={this.props.createNoteForSavedMessage}
            messageIsSaved={this.props.messageIsSaved}
            userFirstName={this.props.userFirstName}
            userLastName={this.props.userLastName}
            userAvatar={this.props.userAvatar}
            currentNoteText={this.state.currentNoteText}
            onTextAreaBlur={this.onNoteInputAreaBlur}
            onTextAreaFocus={this.onNoteInputAreaFocus}
            showFooter={this.showNoteInputFooter()}
            onAddNote={this.onAddNote}
            disabled={this.props.disableInput}/>
        </div>
      </div>
    );
  };

  buildActivityContent() {
    return (
      <div className={styles.notesWrapper}>
        <div
          className={cx({
            notesList: true,
            scrolledToTop: this.state.scrolledToTop
          })}
          onScroll={(e) => {
            this.onNotesListScroll(e);
          }}
          key='activity-container'>
          {
            !this.props.activities.length
              ? <div className={styles.noNotesPlaceholder}>
                <img src={noNotesImage} />
                { this.props.notes.length ?
                  <div>
                    <h3 className={styles.noNotesBanner}>{NO_ACTIVITY_TEXT}</h3>
                    <button className={styles.viewOtherTabButton} onClick={this.selectMenuItem.bind(this, 'NOTES')}>
                      <p>{NO_ACTIVITY_VIEW_NOTES}</p>
                    </button>
                  </div>
                :
                  <h3 className={styles.noNotesBanner}>{NO_NOTES_OR_ACTIVITY}</h3>
                }
              </div>
              : null
          }
          {
            this.props.activities.map((activity, index) => (
              <Note
                  authorFirstName={activity.authorFirstName}
                  authorLastName={activity.authorLastName}
                  authorAvatarThumbnail={activity.authorAvatarThumbnail}
                  timestamp={activity.createdAtTimestamp}
                  authoredContent={activity.description}
                  userTimezone={this.props.userTimezone}
                  key={`activity-${this.props.activities.length - 1 - index}`}/>
              ))
          }
        </div>
      </div>
    );
  }

  buildMenuContent() {
    switch (this.state.selectedView) {
    case NotesMenuItems.NOTES:
      return this.buildNotesContent();
    case NotesMenuItems.ACTIVITY:
      return this.buildActivityContent();
    }
  }

  render() {
    return (
      <div className={styles.notesMenu}>
        <div className={styles.notesMenuItemsPicker}>
          <ProductNav>
            {
              NotesMenuItemsList.map((item, index) => (
                <ProductNavItem
                  key={index}
                  isSelected={this.state.selectedView === item}
                  onClick={this.selectMenuItem.bind(this, item)}>
                  {item}
                </ProductNavItem>
              ))
            }
          </ProductNav>
        </div>
        {this.buildMenuContent()}
      </div>
    );
  }
}

export default NotesMenu;
