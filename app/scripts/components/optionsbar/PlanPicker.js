/* @flow */
import React, { Component } from 'react';
import styles from 'styles/modules/components/optionsbar/PlanPicker.scss';
import {emptyPlan} from 'constants/PlanConstants';
import { planPickerText } from 'constants/UITextConstants';
import '@spredfast/react-lib/styles/Icon.scss';
import _ from 'lodash';
import HoverText from 'components/HoverText';
import { textWidthForHoverText } from 'constants/UILayoutConstants';

import DropdownSection from 'components/util/DropdownSection';

type State = {
  currentPlan: any,
  selectorOpen: bool
};

type Props = {
  onSelectPlan: () => void,
  currentPlan: Object,
  plans: Array<{
    color: string,
    id: string,
    name: string
  }>
};

type PlanProps = {
  text: string,
  item: {
    color: string
  }
};

class PlanPicker extends Component<any, Props, any> {

  props: Props;
  state: State;

  constructor(props: Props): void {
    super(props);

    (this: any).onClearButtonClick = this.onClearButtonClick.bind(this);
    (this: any).updateCurrentPlan = this.updateCurrentPlan.bind(this);
    (this: any).onBlur = this.onBlur.bind(this);
    (this: any).renderPlan = this.renderPlan.bind(this);
    (this: any).onToggleOpen = this.onToggleOpen.bind(this);

    this.state = {
      currentPlan: this.props.currentPlan,
      selectorOpen: false
    };
  };

  renderPlan(props: PlanProps) {
    const colorBarStyle = {
      backgroundColor: this.state.selectorOpen ? props.item.color : 'inherit'
    };
    return (
      <div style={colorBarStyle}>
        <div className={styles.choice}>
          <HoverText
            displayAtWidth={textWidthForHoverText.PLAN_PICKER}
            evalText={props.text}>
            <span className={styles.choiceText}>{props.text}</span>
          </HoverText>
        </div>
      </div>
    );
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.currentPlan !== this.state.currentPlan) {
      this.updateCurrentPlan(nextProps.currentPlan);
    }
  }

  updateCurrentPlan(value: any) {
    this.setState({
      currentPlan: value
    });
  }

  addDatum(value: string) {
    this.props.onSelectPlan(value);
  }

  onClearButtonClick() {
    this.props.onSelectPlan(emptyPlan);
  }

  onBlur(e: any) {
    if (!this.state.currentPlan) {
      this.props.onSelectPlan(emptyPlan);
    } else {
      const validPlan: Object = _.find(this.props.plans, (plan): bool => {
        return _.isEqual(plan, this.state.currentPlan);
      });
      if (validPlan) {
        this.props.onSelectPlan(validPlan);
      } else {
        this.setState({currentPlan: this.props.currentPlan});
      }
    }
  }

  onToggleOpen(open: bool) {
    this.setState({
      selectorOpen: open
    });
  }

  render() {
    return (
      <div className={styles.PlanPicker}>
        <DropdownSection
          colorSelected
          singleSelect
          onClearButtonClick={this.onClearButtonClick}
          title={planPickerText.title}
          icon={<span className='icon-planner'></span>}
          description={planPickerText.subText}
          selectedData={this.props.currentPlan && this.props.currentPlan.name ? [this.props.currentPlan] : []}
          addDatum={this.props.onSelectPlan}
          onToggle={this.onToggleOpen}
          canAddData={false}
          comboBoxConfig={{
            data: this.props.plans,
            valueField: 'id',
            textField: 'name',
            value: this.state.currentPlan,
            itemComponent: this.renderPlan,
            placeholder: planPickerText.placeHolderText,
            messages: {
              emptyList: planPickerText.emptyList,
              emptyFilter: planPickerText.emptyFilter
            },
            filter: 'contains',
            onChange: this.updateCurrentPlan,
            onBlur: this.onBlur
          }}/>
      </div>
    );
  }
};

export default PlanPicker;
