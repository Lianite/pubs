/* @flow */
import React from 'react';
import classNames from 'classnames/bind';
import {
  NOTE_ADD_BUTTON_TEXT,
  NOTE_INPUT_PLACEHOLDER_TEXT
} from 'constants/UITextConstants';
import { DatetimeConstants } from 'constants/ApplicationConstants';

import styles from 'styles/modules/components/optionsbar/NoteInput.scss';

export type NoteInputProps = {
  onCurrentNoteTextChange: Function,
  createPendingNote: Function,
  createNoteForSavedMessage: Function,
  onAddNote: Function,
  onTextAreaFocus: Function,
  onTextAreaBlur: Function,
  messageIsSaved: bool,
  userFirstName: string,
  userLastName: string,
  userAvatar: string,
  currentNoteText: string,
  showFooter: bool,
  disabled: bool
};

const cx = classNames.bind(styles);

class NoteInput extends React.Component<void, NoteInputProps, any> {

  constructor(props: NoteInputProps) {
    super(props);
  }

  onTextAreaChange(event: Object) {
    this.props.onCurrentNoteTextChange(event.target.value);
  }

  onButtonClick() {
    if (!this.props.messageIsSaved) {
      this.props.createPendingNote({
        noteText: this.props.currentNoteText,
        authorFirstName: this.props.userFirstName,
        authorLastName: this.props.userLastName,
        noteCreationTimestamp: Math.floor((new Date).getTime() / DatetimeConstants.MS_IN_SEC),
        authorAvatarThumbnail: this.props.userAvatar
      });
    } else {
      this.props.createNoteForSavedMessage(this.props.currentNoteText);
    }
    this.props.onAddNote();
  }

  render() {
    return (
      <div
        className={styles.noteInputWrapper}>
        <textarea
          className={cx({
            noteInput: true
          })}
          disabled={this.props.disabled}
          placeholder={NOTE_INPUT_PLACEHOLDER_TEXT}
          onFocus={this.props.onTextAreaFocus.bind(this)}
          onBlur={this.props.onTextAreaBlur.bind(this)}
          onChange={this.onTextAreaChange.bind(this)}
          value={this.props.currentNoteText}
          ref='input'/>
        {
          this.props.showFooter
          ? <div className={styles.footer}>
            <button
              className={styles.addNote}
              disabled={!this.props.currentNoteText || this.props.disabled}
              onClick={this.onButtonClick.bind(this)}>
              {NOTE_ADD_BUTTON_TEXT}
            </button>
          </div>
          : null
        }
      </div>
    );
  }
}

export default NoteInput;
