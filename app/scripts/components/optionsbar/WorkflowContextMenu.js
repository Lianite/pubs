/* @flow */
import React from 'react';
import AssigneeDropDown from 'components/optionsbar/AssigneeDropDown';
import FeaturePlaceholder from 'components/util/FeaturePlaceholder';
import { workflowContextMenuText } from 'constants/UITextConstants';

import approvalsPlaceholderImage from 'images/approvals-placeholder.svg';

import type { AssigneeData } from 'adapters/types';
import styles from 'styles/modules/components/optionsbar/WorkflowContextMenu.scss';

type Props = {
    currentAssignee: AssigneeData,
    onSelectAssignee: Function,
    resendValidAssigneesRequest: Function,
    assignees: Array<AssigneeData>,
    unavailableVoices: Array<Object>
};

const WorkflowContextMenu = (props: Props) => {
  return (
    <div className={styles.WorkflowContextMenu}>
      <div className={styles.title}>{workflowContextMenuText.title}</div>
      <AssigneeDropDown {...props}/>
      <div className={styles.divider}></div>
      <FeaturePlaceholder
        image={approvalsPlaceholderImage}
        header={workflowContextMenuText.approvalsPlaceholderHeader}
        body={workflowContextMenuText.approvalsPlaceholderBody}/>
    </div>
  );
};

export default WorkflowContextMenu;
