/* @flow */
import React from 'react';
import type { LinkTagDetails } from 'records/LinkTagsRecords';

import TextTagVariable from 'components/optionsbar/linkTags/LinkTagsTextVariable';

type Props = {
  link: string,
  linkTag: LinkTagDetails,
  updateLinkTagVariable: Function
};

const ComponentForVariableType = {
  text: TextTagVariable
};

const getTagVariablesToRender = (tagVariables) => {
  return tagVariables
    .map(tagVariable => ({tagVariable, Component: ComponentForVariableType[tagVariable.get('type')]}))
    .filter(componentData => componentData.Component);
};

const getTagVariableValue = (variableValues, id: number) => {
  return variableValues
    .find(value => value.get('variableId') === id);
};

const LinkTagsVariables = (props: Props) => {
  let variablesToRender = getTagVariablesToRender(props.linkTag.tagVariables);
  return (
    <div>
      {variablesToRender.map(({tagVariable, Component}) => {
        return (
          <Component
            key={tagVariable.get('id')}
            link={props.link}
            tagVariable={tagVariable}
            variableValue={getTagVariableValue(props.linkTag.variableValues, tagVariable.get('id'))}
            updateLinkTagVariable={props.updateLinkTagVariable}
          />
        );
      })}
    </div>
  );
};

export default LinkTagsVariables;