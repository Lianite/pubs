/* @flow */
import React from 'react';
import styles from 'styles/modules/components/optionsbar/linkTags/LinkTagsVariables.scss';
import { TextInput } from '@spredfast/react-lib';
import {TagVariable, TagVariableValue} from 'records/LinkTagsRecords';

type Props = {
  link: string,
  tagVariable: TagVariable,
  variableValue: TagVariableValue,
  updateLinkTagVariable: Function
};

const LinkTextVariable = ({tagVariable, variableValue, updateLinkTagVariable, link}: Props) => {
  const variableDescription = tagVariable.get('variableDescription');
  const value = variableValue && variableValue.get('values') && variableValue.get('values').first() && variableValue.get('values').first() && variableValue.get('values').first().get('value');
  const onChange = (event) => updateLinkTagVariable(link, tagVariable.get('id'), [{value: event.target.value}]);
  return (
    <div className={styles.linkTagVariable}>
      <label>
        {`${variableDescription.get('fieldName')}${variableDescription.get('required') ? ' *' : ''}:`}
      </label>

      <TextInput
        className={styles.variableInput}
        value={value}
        name={tagVariable.get('name')}
        onDebouncedChange={onChange}
        debounceInterval={1500}
      />
    </div>
  );
};

export default LinkTextVariable;
