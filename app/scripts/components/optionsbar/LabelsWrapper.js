/* @flow */
import React, { Component } from 'react';
import styles from 'styles/modules/components/optionsbar/LabelsWrapper.scss';
import '@spredfast/react-lib/styles/Icon.scss';
import { List as ImmutableList} from 'immutable';
import { labelsWrapperText } from 'constants/UITextConstants';
import { textWidthForHoverText } from 'constants/UILayoutConstants';

import HoverText from 'components/HoverText';
import DropdownSection from 'components/util/DropdownSection';

import type { Label } from 'records/LabelsRecords';

type Props = {
  selectedLabels: Array<Label>,
  addLabel: Function,
  availableLabels: Array<Label>,
  fetchLabelsAction: Function,
  onRemoveLabel: Function,
  labelErrorGroup: ImmutableList<Object>,
  canCreateLabels: bool,
  updateLabelText: Function,
  currentLabelText: string
};

type DefaultProps = {
  selectedLabels: Array<Label>,
  addLabel: Function,
  availableLabels: Array<Label>,
  fetchLabelsAction: Function,
  onRemoveLabel: Function,
  labelErrorGroup: ImmutableList<Object>,
  canCreateLabels: bool,
  updateLabelText: Function,
  currentLabelText: string
};

const DropdownLabel = (props: any) => (
  <HoverText
      displayAtWidth={textWidthForHoverText.LABELS_WRAPPER}
      evalText={props.text}>
    <span className={styles.label}>{props.text}</span>
  </HoverText>
);

class LabelsWrapper extends Component<DefaultProps, Props, any> {
  static defaultProps: DefaultProps = {
    selectedLabels: [],
    addLabel: () => {},
    availableLabels: [],
    fetchLabelsAction: () => {},
    onRemoveLabel: () => {},
    updateLabelText: () => {},
    currentLabelText: '',
    labelErrorGroup: ImmutableList()
  };
  fetchLabelDebounceRef: Function;
  addLabelDebounceRef: Function;

  constructor(props: Props): void {
    super(props);
  };

  render() {
    return (
      <DropdownSection
          title={labelsWrapperText.title}
          icon={<span className='icon-label-thick'></span>}
          description={labelsWrapperText.mainText}
          selectedData={this.props.selectedLabels.map((label) => ({
            text: label.title,
            error: !!this.props.labelErrorGroup.find((errorLabel) => errorLabel.get('id') === label.id)
          }))}
          errorText={!this.props.labelErrorGroup.isEmpty() ? labelsWrapperText.groupViolationMessage : ''}
          onRemoveDatum={this.props.onRemoveLabel}
          shouldToggleOpen={this.props.labelErrorGroup.isEmpty()}
          addDatum={this.props.addLabel}
          fetchData={this.props.fetchLabelsAction}
          onInputValueChange={this.props.updateLabelText}
          clearInputOnSelect
          comboBoxConfig={{
            data: this.props.availableLabels,
            valueField: 'title',
            textField: 'title',
            value: this.props.currentLabelText,
            itemComponent: DropdownLabel,
            readOnly: !this.props.labelErrorGroup.isEmpty(),
            placeholder: labelsWrapperText.placeHolderText
          }}/>
    );
  }
};

export default LabelsWrapper;
