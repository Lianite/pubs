/* @flow */
import React, { Component } from 'react';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import '@spredfast/react-lib/styles/Icon.scss';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import { toolTipPopUpDelay } from 'constants/UIStateConstants';
import DropdownSection from 'components/util/DropdownSection';
import _ from 'lodash';
import { assigneeText } from 'constants/UITextConstants';
import { toolTipOverlayStyle, textWidthForHoverText } from 'constants/UILayoutConstants';
import HoverText from 'components/HoverText';

import type { AssigneeData } from 'adapters/types';
import type { VoiceResponse } from 'apis/AccountsApi';
import styles from 'styles/modules/components/optionsbar/AssigneeDropDown.scss';

type Props = {
  currentAssignee: AssigneeData,
  onSelectAssignee: Function,
  resendValidAssigneesRequest: Function,
  assignees: Array<AssigneeData>,
  unavailableVoices: Array<VoiceResponse>
};

type DefaultProps = {
  currentAssignee: AssigneeData,
  onSelectAssignee: Function,
  assignees: Array<AssigneeData>,
  unavailableVoices: Array<VoiceResponse>
};

type State = {
  comboBoxOpen: bool,
  comboBoxValue: Object
}

const buildOverLay = (voices: Array<VoiceResponse>, header: string, toolTipText:? string) => {
  return (
    <div>
      <span>{toolTipText}</span>
      <h1 >{header}</h1>
      <ul>
        {_.map(voices, (voice) => {
          return (<li key={voice.data.id}>{voice.data.name}</li>);
        })}
      </ul>
    </div>
  );
};

class AssigneeDropDown extends Component<DefaultProps, Props, State> {
  static defaultProps: DefaultProps = {
    currentAssignee: {},
    onSelectAssignee: () => {},
    assignees: [],
    invalidAssignees: [],
    unavailableVoices: []
  };
  state: State;

  constructor(props: Props): void {
    super(props);
    this.state = {
      comboBoxOpen: false,
      comboBoxValue: props.currentAssignee
    };
    (this:any).onBlurHandler = this.onBlurHandler.bind(this);
    (this:any).onChange = this.onChange.bind(this);
    (this:any).choice = this.choice.bind(this);
    (this:any).buildTextField = this.buildTextField.bind(this);
    (this:any).buildWarning = this.buildWarning.bind(this);
    (this:any).onSelect = this.onSelect.bind(this);
  };

  componentWillMount() {
    this.props.resendValidAssigneesRequest();
  };

  buildWarning() {
    const text = this.props.unavailableVoices.length > 1 ? assigneeText.plural : assigneeText.singular;
    return _.isEmpty(this.props.unavailableVoices) ? null :
    (
      <div className={styles.warningWrapper}>
        <RADIcon className={styles.warningIcon} glyph='warning-dot'/>
        <div className={styles.warningText}>
          {this.props.unavailableVoices.length} {text.warningMessage}
          <Tooltip
          overlay={buildOverLay(this.props.unavailableVoices, text.accountSetRestrictedHeader, null)}
          trigger={['click']}
          destroyTooltipOnHide
          placement='right'
          overlayStyle={toolTipOverlayStyle}>
            <span className={styles.viewAccountSets}>{text.viewAccountSets}</span>
          </Tooltip>
        </div>
      </div>
    );
  };

  onBlurHandler(e: Object = {}) {
    if (!this.state.comboBoxValue.id) {
      this.setState({comboBoxValue: this.props.currentAssignee});
    } else {
      this.props.onSelectAssignee(this.state.comboBoxValue);
    }
  };

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.currentAssignee !== nextProps.currentAssignee) {
      this.setState({comboBoxValue: nextProps.currentAssignee});
    }
  }

  choice(choiceProps: Object) {
    const message = choiceProps.item.conflictingVoices.length > 1 ? assigneeText.plural : assigneeText.singular;
    const text = `${choiceProps.item.firstName} ${choiceProps.item.lastName} ${message.toolTipText}`;
    return !choiceProps.item.invalid ?
    (<HoverText
      displayAtWidth={textWidthForHoverText.ASSIGNEE_DROPDOWN}
      evalText={choiceProps.text}>
      <span>{choiceProps.text}</span>
    </HoverText>)
    :
    (<div className={styles.conflictWrapper}>
      <Tooltip
      overlay={buildOverLay(choiceProps.item.conflictingVoices, message.accountSetConflictHeader, text)}
      trigger={['hover']}
      mouseEnterDelay={toolTipPopUpDelay}
      destroyTooltipOnHide
      placement='right'
      overlayStyle={toolTipOverlayStyle}>
        <div className={styles.choiceContainer}>
          <span className={styles.choiceTextDisabled}>{choiceProps.text}</span>
          {choiceProps.item.invalid ? <RADIcon className={styles.warningIcon} glyph='warning-dot'/> : null}
        </div>
      </Tooltip>
    </div>);
  };

  buildTextField(item: any) {
    return typeof item === 'string' ? item : `${item.firstName} ${item.lastName}`;
  };

  onSelect(value: Object) {
    if (!value.invalid) {
      this.props.onSelectAssignee(value);
    }
  };

  onChange(value: any) {
    if (!value.invalid) {
      this.setState({
        comboBoxValue: value,
        comboBoxOpen: false
      });
    }
  };

  render() {
    const {assignees} = this.props;
    return (
      <div>
        <div className={styles.AssignDropDown}>
          <DropdownSection
            singleSelect
            title={assigneeText.title}
            icon={<span style={{'fontSize':'18px'}} className='icon-assignment'></span>}
            description={assigneeText.mainText}
            selectedData={this.props.currentAssignee && this.props.currentAssignee.name ? [this.props.currentAssignee] : []}
            addDatum={this.props.onSelectAssignee}
            canAddData={false}
            comboBoxConfig={{
              data: assignees,
              valueField: 'id',
              textField: this.buildTextField,
              value: this.state.comboBoxValue,
              itemComponent: this.choice,
              messages: {
                emptyFilter: assigneeText.emptyFilterText
              },
              onChange: this.onChange,
              onBlur: this.onBlurHandler,
              disabledField:'invalid',
              suggest: true
            }}/>
        </div>
        {this.buildWarning()}
      </div>
    );
  };
};

export default AssigneeDropDown;
