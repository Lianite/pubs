/* @flow */
import React from 'react';
import {
  createUtcMomentFromUnixTimestamp,
  formatDatetimeForNoteDisplay
} from 'utils/DateUtils';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import { MAX_NOTE_CHARS } from 'constants/NotesMenuConstants';

import styles from 'styles/modules/components/optionsbar/Note.scss';

export type NoteProps = {
  authorFirstName: string,
  authorLastName: string,
  authorAvatarThumbnail: string,
  timestamp: number,
  authoredContent: string,       //TODO: future improvement, polymorphic type?
  authoredContentMaxChars: number,
  userTimezone: TimezoneRecord
};

type DefaultNoteProps = {
  authoredContentMaxChars: number
}

type NoteState = {
  expanded: bool
};

export type AuthorAvatarProps = {
  authorFirstName: string,
  authorLastName: string,
  authorAvatarThumbnail: string
};
export const AuthorAvatar = ({authorFirstName, authorLastName, authorAvatarThumbnail}: AuthorAvatarProps) => (
  <div className={styles.avatarWrapper}>
    {
      authorAvatarThumbnail
      ? <div
          className={styles.avatar}
          style={{backgroundImage: `url(${authorAvatarThumbnail})`}}>
      </div>
      : <div className={styles.avatarInitials}>
        {`${authorFirstName[0]} ${authorLastName[0]}`}
      </div>
    }
  </div>
);

export type ReadMoreToggleProps = {
  onClick: Function,
  expanded: bool
};
export const ReadMoreToggle = ({onClick, expanded}: ReadMoreToggleProps) => (
  <a
    className={styles.readMore}
    href='javascript:;'
    onClick={onClick}>
    {
      expanded
      ? 'Read Less'
      : 'Read More'
    }
  </a>
);

export type NoteBodyProps = {
  authoredContent: string,     //TODO: future improvement, polymorphic type?
  authoredContentMaxChars?: number,
  onReadMoreClick: Function,
  expanded: bool
};
export const NoteBody = ({authoredContent, onReadMoreClick, expanded, authoredContentMaxChars}: NoteBodyProps) => {
  const maxChars = authoredContentMaxChars || MAX_NOTE_CHARS;
  return (
    <div className={styles.body}>
      {
        authoredContent.length <= maxChars
        ? authoredContent
        : <div>
          {
            !expanded
            ? `${authoredContent.slice(0, maxChars)} ...`
            : authoredContent
          }
          <ReadMoreToggle
            onClick={onReadMoreClick}
            expanded={expanded} />
        </div>
      }
    </div>
  );
};

class Note extends React.Component<DefaultNoteProps, NoteProps, any> {
  state: NoteState
  static defaultProps = {
    authoredContentMaxChars: MAX_NOTE_CHARS
  };

  constructor(props: NoteProps) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  toggleNoteExpand() {
    this.setState({
      expanded: !this.state.expanded
    });
  }

  formatTimestamp() {
    const moment = createUtcMomentFromUnixTimestamp(this.props.timestamp, this.props.userTimezone.get('offset'));
    return formatDatetimeForNoteDisplay(moment, this.props.userTimezone.get('offset'));
  }

  render() {
    return (
      <div className={styles.note}>
        <div className={styles.header}>
          <AuthorAvatar
            authorFirstName={this.props.authorFirstName}
            authorLastName={this.props.authorLastName}
            authorAvatarThumbnail={this.props.authorAvatarThumbnail}/>
          <div className={styles.info}>
            <div className={styles.authorName}>
              {`${this.props.authorFirstName} ${this.props.authorLastName}`}
            </div>

            <div className={styles.timestamp}>
              {this.formatTimestamp()}
            </div>
          </div>
        </div>
        <NoteBody
          authoredContent={this.props.authoredContent}
          authoredContentMaxChars={this.props.authoredContentMaxChars}
          onReadMoreClick={this.toggleNoteExpand.bind(this)}
          expanded={this.state.expanded} />
      </div>
    );
  }
}

export default Note;
