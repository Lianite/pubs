/* @flow */
import React from 'react';
import styles from 'styles/modules/components/optionsbar/LinksContextMenu.scss';
import LinkPreviewUrlPicker from './LinkPreviewUrlPicker';
import { linkOptions } from 'constants/UITextConstants';
import { TOOLTIP_OVERLAY_STYLES } from 'constants/UILayoutConstants';

import type { Map as ImmutableMap, Set as ImmutableSet } from 'immutable';

import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import '@spredfast/react-lib/styles/Checkbox.scss';

import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import '@spredfast/react-lib/styles/Checkbox.scss';

import RADIcon from '@spredfast/react-lib/lib/RADIcon';
import '@spredfast/react-lib/styles/Icon.scss';
import LinkTagsVariables from 'components/optionsbar/linkTags/LinkTagsVariables';
import type { LinkTagDetails } from 'records/LinkTagsRecords';
import { UrlPropertiesRecord } from 'records/LinksRecords';

type Props = {
  urls: ImmutableSet<string>,
  urlProperties: ImmutableMap<string, UrlPropertiesRecord>,
  onChangeLinkProperty: () => void,
  addLinkPreview: Function,
  selectedLinkPreviewUrl: string,
  canAddAttachmentByType: {linkPreview: boolean, images: boolean, video: boolean},
  linkTagsByLink: ImmutableMap<string, LinkTagDetails>,
  linkTagsEnable: bool,
  updateLinkTagVariable: Function
};

const tooltipOverlayStyle = {
  ...TOOLTIP_OVERLAY_STYLES,
  maxWidth: 300
};

const generateIndividualLinkOptions = (urls, urlProperties, onChangeLinkProperty, linkTagsByLink, updateLinkTagVariable, linkTagsEnable) => {
  return urls.map((link) => {
    const shortenOnPublish = urlProperties.get(link).get('shortenOnPublish');
    const addLinkTags = urlProperties.get(link).get('addLinkTags');
    const linkTag = linkTagsByLink.get(link);
    const showAddLinkTagsCheckbox = linkTagsEnable && linkTag;
    const showLinkTagsVariables = linkTagsEnable && addLinkTags && shortenOnPublish && linkTag;
    const onChangeAddLinkTags = () => onChangeLinkProperty(link, 'addLinkTags', !addLinkTags);
    const onChangeShortenOnPublish = () => {
      onChangeLinkProperty(link, 'shortenOnPublish', !shortenOnPublish);
      if (shortenOnPublish) {
        onChangeLinkProperty(link, 'addLinkTags', false);
      }
    };

    return (
      <div className={styles.linkShorten} key={link + '_link_options'}>
        <div className={styles.linkToShorten}>{link}</div>
        <div className={styles.linkCheckboxContainer}>
          <Checkbox checked={shortenOnPublish} onChange={onChangeShortenOnPublish} />
          <span>{linkOptions.shortenOnPublish}</span>
        </div>

        { showAddLinkTagsCheckbox ?
          <div className={styles.linkCheckboxContainer}>
            <Checkbox checked={addLinkTags} onChange={onChangeAddLinkTags} disabled={!shortenOnPublish}/>
            <span>
              {linkOptions.addLinkTags}
              {!shortenOnPublish ?
                <Tooltip overlayStyle={tooltipOverlayStyle} overlay={linkOptions.linkTagsDisableTooltip}><RADIcon className={styles.infoIcon} glyph='info'/></Tooltip> : null
              }
            </span>
          </div> : null }

        { showLinkTagsVariables ?
          <div className={styles.linkVariablesContainer}>
            <LinkTagsVariables link={link} linkTag={linkTag} updateLinkTagVariable={updateLinkTagVariable}/>
          </div> : null }
      </div>
    );
  });
};

const LinksContextMenu = (props: Props) => {
  const showLinkPreviewUrlPicker = props.urls.size > 0 && props.canAddAttachmentByType.linkPreview;

  return (
    <div className={styles.linksContextMenu}>
      <div className={styles.title}>{linkOptions.linksTitle} <RADIcon glyph='info'/></div>
      {showLinkPreviewUrlPicker ?
        <LinkPreviewUrlPicker urls={props.urls} selectedUrl={props.selectedLinkPreviewUrl} onSelect={props.addLinkPreview}/> : null }
      {generateIndividualLinkOptions(props.urls, props.urlProperties, props.onChangeLinkProperty, props.linkTagsByLink, props.updateLinkTagVariable, props.linkTagsEnable)}
    </div>
  );
};

export default LinksContextMenu;
