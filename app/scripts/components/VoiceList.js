/* @flow */
import React, {Component} from 'react';
import styles from 'styles/modules/components/VoiceList.scss';
import Credential from 'components/Credential';
import Voice from 'components/Voice';
import InfiniteScroll from 'react-infinite';
import { VoiceFilteringConstants, SupportedNetworks } from 'constants/ApplicationConstants';
import { LoadingAnimation } from '@spredfast/react-lib';
import classNames from 'classnames/bind';
import {
  accountSelectionText,
  FACEBOOK_TARGETING_ACCOUNT_DISABLED,
  FACEBOOK_DARKPOST_ACCOUNT_DISABLED,
  FACEBOOK_ACCOUNT_DISABLED_MESSAGE
} from 'constants/UITextConstants';
import { OptionPicker } from 'components/optionsbar/LinkPreviewUrlPicker';
import _ from 'lodash';
import { Set as ImmutableSet } from 'immutable';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import type { Map as ImmutableMap, List as ImmutableList } from 'immutable';
import type { SocialNetworkAccount } from 'adapters/types';

const cx = classNames.bind(styles);

type VoiceListHeaderProps = {
  filteredAccountCount: number;
  totalAccountCount: number;
  checkedCount: number;
  currentAccountChannelFilter: ImmutableList<string>;
  currentAccountSelectionFilter: string;
  advancedOptionsOpen: bool;
  searchOnVoices: bool;
  featureFlags: ImmutableMap<string, bool>;
  toggleAdvancedOptions: () => void;
  quickSelectAllVoices: Function;
  updateFilterChannels: (channel?: string) => void;
  updateFilterSelection: (filter?: string) => void;
  quickSelectAndFocusReset: (selectedAndRefocus: bool) => void;
  searchOnVoicesToggle: () => void;
};

export const VoiceListHeader = (props: VoiceListHeaderProps) => {
  const allButtonStyles = cx({
    filterButtonActive: props.currentAccountChannelFilter.size === 0
  });

  const twitterButtonStyles = cx({
    filterIconButton: true,
    twitterFilterActive: props.currentAccountChannelFilter.contains(SupportedNetworks.TWITTER)
  });

  const facebookButtonStyles = cx({
    filterIconButton: true,
    channelIconButtonFacebook: true,
    facebookFilterActive: props.currentAccountChannelFilter.contains(SupportedNetworks.FACEBOOK)
  });

  const googleplusButtonStyles = cx({
    filterIconButton: true,
    googleplusFilterActive: props.currentAccountChannelFilter.contains(SupportedNetworks.GOOGLEPLUS),
    googleplusDisabled: !props.featureFlags || !props.featureFlags.get('googleplus', false)
  });

  const allSelectionButtonStyles = cx({
    filterIconButton: true,
    allSelectionButton: true,
    allSelectionActive: !props.currentAccountSelectionFilter
  });

  const checkedSelectionButtonStyles = cx({
    filterIconButton: true,
    checkedSelectionActive: props.currentAccountSelectionFilter === VoiceFilteringConstants.FILTER_SELECTED
  });

  const uncheckedSelectionButtonStyles = cx({
    filterIconButton: true,
    uncheckedSelectionActive: props.currentAccountSelectionFilter === VoiceFilteringConstants.FILTER_UNSELECTED
  });

  return (
    <div>
      <div className={styles.filteredAccountCount}>
        <p className={styles.filterCount}>Showing {props.filteredAccountCount} / {props.totalAccountCount}</p>

        <div className={styles.filterControls}>
          <p>Filter</p>

          <div className={styles.channelFilters}>
            <button className={allButtonStyles} onClick={(e) => {
              props.updateFilterChannels();
            }}><span className={styles.allButtonText}>All</span><i className='icon-acct-filters-all-boxes' /></button>
            <button className={twitterButtonStyles} onClick={(e) => {
              props.updateFilterChannels(SupportedNetworks.TWITTER);
            }}><i className='rad-icon-network-twitter' /></button>
            <button className={facebookButtonStyles} onClick={(e) => {
              props.updateFilterChannels(SupportedNetworks.FACEBOOK);
            }}><i className='rad-icon-network-facebook' /></button>
            <button className={googleplusButtonStyles} onClick={(e) => {
              props.updateFilterChannels(SupportedNetworks.GOOGLEPLUS);
            }}><i className='rad-icon-network-google-plus' /></button>
          </div>

          <div className={styles.selectionFilters}>
            <button className={allSelectionButtonStyles} onClick={(e) => {
              props.updateFilterSelection();
            }}><i className='icon-acct-filters-selected-unselected' /></button>
            <button className={checkedSelectionButtonStyles} onClick={(e) => {
              props.updateFilterSelection(VoiceFilteringConstants.FILTER_SELECTED);
            }}><i className='icon-acct-filters-selected' /></button>
            <button className={uncheckedSelectionButtonStyles} onClick={(e) => {
              props.updateFilterSelection(VoiceFilteringConstants.FILTER_UNSELECTED);
            }}><i className='icon-acct-filters-unselected' /></button>
          </div>
        </div>
      </div>

      <div className={styles.quickSelectAndAdvancedOptions}>
        <div className={styles.quickSelectWrapper}>
          <button disabled={(props.checkedCount !== props.filteredAccountCount) ? false : true} className={styles.interactable} onClick={() => {
            props.quickSelectAndFocusReset(true);
          }}>{accountSelectionText.selectAll}</button>
          <span>|</span>
          <button disabled={(props.checkedCount > 0) ? false : true} className={styles.interactable} onClick={() => {
            props.quickSelectAndFocusReset(false);
          }}>{accountSelectionText.clearSelection}</button>
        </div>

        <div className={styles.quickSelectWrapper}>
          <button className={cx(styles.interactable, 'advancedOptionsButton')} onClick={() => {
            props.toggleAdvancedOptions(props.advancedOptionsOpen);
          }}>{props.advancedOptionsOpen ? accountSelectionText.hide : accountSelectionText.show} {accountSelectionText.advancedOptions}</button>
        </div>
      </div>

      <ReactCSSTransitionGroup
        transitionEnterTimeout={VoiceFilteringConstants.ROLLED_CREDENTIAL_TRANSITION_MS}
        transitionLeaveTimeout={VoiceFilteringConstants.ROLLED_CREDENTIAL_TRANSITION_MS}
        transitionName={{
          enter: styles.advancedOptionsEnter,
          enterActive: styles.advancedOptionsEnterActive,
          leave: styles.advancedOptionsLeave,
          leaveActive: styles.advancedOptionsLeaveActive
        }}>
        { props.advancedOptionsOpen ?
          <div className={cx(styles.quickSelectAndAdvancedOptions, styles.advancedOptionsRow)}>
            <span>{accountSelectionText.advancedOptionsLabel}</span>
            <div className={styles.optionPickerWrapper}>
              <OptionPicker
                options={ImmutableSet([accountSelectionText.searchAccount, accountSelectionText.searchAccountSet])}
                selectedOption={props.searchOnVoices ? accountSelectionText.searchAccountSet : accountSelectionText.searchAccount}
                onSelect={props.searchOnVoicesToggle}
                inline />
            </div>
          </div>
          :
          null
        }
      </ReactCSSTransitionGroup>
    </div>
  );
};

type VoiceListProps = {
  voiceChecked: (voiceId: number, credentialId?: number) => void;
  voices: Array<any>;
  checkedVoices: Object;
  resendValidAssigneesRequest: () => void;
  filteredAccountCount: number;
  totalAccountCount: number;
  checkedCount: number;
  isOpen?: bool;
  accountSelectorHeight: number;
  voicesLoading: bool;
  currentAccountChannelFilter: ImmutableList<string>;
  currentAccountSelectionFilter: string;
  searchOnVoices: bool;
  featureFlags: ImmutableMap<string, bool>;
  quickSelectAllVoices: Function;
  updateFilterChannels: (channel?: string) => void;
  updateFilterSelection: (filter?: string) => void;
  facebookTargetingSet: bool;
  searchOnVoicesToggle: () => void;
  darkPostStatus: bool;
};

type DefaultProps = {
  voices?: Array<any>;
  filteredAccountCount: number;
  checkedCount: number;
  isOpen?: bool;
};

type State = {
  advancedOptionsOpen: bool
};

export default class VoiceList extends Component<DefaultProps, VoiceListProps, State> {
  static displayName = 'VoiceList';
  static defaultProps = {
    voices: [],
    filteredAccountCount: 0,
    totalAccountCount: 0,
    checkedCount: 0,
    isOpen: false,
    accountSelectorHeight: VoiceFilteringConstants.ACCOUNT_SELECTOR_DEFAULT_HEIGHT
  };
  state: State;

  constructor(props: VoiceListProps) {
    super(props);

    this.state = {
      advancedOptionsOpen: false
    };

    (this:any).quickSelectAndFocusReset = this.quickSelectAndFocusReset.bind(this);
    (this:any).toggleAdvancedOptions = this.toggleAdvancedOptions.bind(this);
  };

  toggleAdvancedOptions(currentState: bool) {
    this.setState({advancedOptionsOpen: !currentState});
  }

  quickSelectAndFocusReset(selectAll: bool) {
    this.refs.filteredAccountCount.focus();
    this.props.quickSelectAllVoices(selectAll);
  }

  buildVoiceListHeader() {
    const headerProps = _.pick(this.props, ['currentAccountSelectionFilter',
            'currentAccountChannelFilter',
            'filteredAccountCount',
            'totalAccountCount',
            'updateFilterChannels',
            'updateFilterSelection',
            'checkedCount',
            'filteredAccountCount',
            'searchOnVoicesToggle',
            'searchOnVoices',
            'featureFlags']);

    // NOTE: the ref on a small container around this stateless component is so that we can
    //       use the ref as we did before this component was stateless, as you cannot add a ref
    //       directly to a stateless component. the tabIndex is so that we can .focus() what is normally
    //       in HTML an unfocuasable element (div).
    return (
      <div ref='filteredAccountCount' tabIndex={0}>
        <VoiceListHeader {...headerProps}
          quickSelectAndFocusReset={this.quickSelectAndFocusReset}
          toggleAdvancedOptions={this.toggleAdvancedOptions}
          advancedOptionsOpen={this.state.advancedOptionsOpen} />
      </div>
    );
  }

  handleVoiceChecked(voiceId: number, credentialId?: number): void {
    this.props.voiceChecked(voiceId, credentialId);
    this.props.resendValidAssigneesRequest();
  };

  disabledInfo(cred: SocialNetworkAccount): {disabled?: bool, disabledMessage?: string} {
    let disabledInfo = {};
    let disabledMessageSegments = [];

    switch (cred.service) {
    case SupportedNetworks.FACEBOOK:
      const disableBecauseOfTargeting = this.props.facebookTargetingSet && (!cred.authenticated || !cred.targetingSupported);
      const disableBecauseOfDarkPosting = this.props.darkPostStatus && !cred.organicVisibilitySupported;

      if (disableBecauseOfTargeting) {
        disabledMessageSegments.push(FACEBOOK_TARGETING_ACCOUNT_DISABLED);
      }
      if (disableBecauseOfDarkPosting) {
        disabledMessageSegments.push(FACEBOOK_DARKPOST_ACCOUNT_DISABLED);
      }
    }

    // Build the message for why it is disabled.
    let disabledMessage = '';
    if (disabledMessageSegments.length > 0) {
      disabledMessage += FACEBOOK_ACCOUNT_DISABLED_MESSAGE;
      for (let i = 0; i < disabledMessageSegments.length - 1; i++) {
        disabledMessage += `${disabledMessageSegments[i]} `;
      }
      disabledMessage += `${disabledMessageSegments.length > 1 ? 'and ' : ''}`;
      disabledMessage += `${disabledMessageSegments[disabledMessageSegments.length - 1]}.`;
    }

    disabledInfo = {
      disabled: disabledMessage.length > 0 ? true : false,
      disabledMessage
    };
    return disabledInfo;
  }

  buildVoiceList(): Array<Voice | Credential> {
    let accountRows = [];

    this.props.voices.map((voice, voiceIndex) => {
      const checkedVoiceEntry = this.props.checkedVoices[voice.id] || {};

      if (voice.credentials && voice.credentials.length > 0) {
        accountRows.push(
          <Voice
            {...voice}
            key={`voice-${voiceIndex}`}
            checkedEntry={checkedVoiceEntry}
            handleChecked={() => {
              this.handleVoiceChecked(voice.id);
            }} />
        );
      }

      voice.credentials.map((cred, credIndex) => {
        const checkedCreds = (checkedVoiceEntry || {});
        const isChecked = !!(checkedCreds[cred.uniqueId]);
        const disabledInfo = this.disabledInfo(cred);
        const disableAlreadChecked = !!checkedCreds[cred.uniqueId] && checkedCreds[cred.uniqueId] !== cred.id;

        accountRows.push(
          <Credential
            {...cred}
            disabled={disabledInfo.disabled || disableAlreadChecked}
            disabledMessage={disabledInfo.disabledMessage}
            key={`voice-${voiceIndex}-cred-${credIndex}`}
            checked={isChecked}
            handleChecked={() => {
              this.handleVoiceChecked(voice.id, cred.uniqueId);
            }} />
        );
      });
    });

    return accountRows;
  };

  loadingVoices() {
    return (
      <div className={styles.voicesLoadingContainer}>
        <div className={styles.voicesLoadingContentBox}>
          <LoadingAnimation />
          <p>{accountSelectionText.loadingAccountsText}</p>
        </div>
      </div>
    );
  };

  render() {
    let elementToRender = null;

    const loadingAnimationStyles = cx({
      loadingVoicesFinished: !this.props.voicesLoading
    });

    if (this.props.isOpen) {
      elementToRender = (
        <div className={styles.voiceListContainer}>
          <div className={styles.voiceListHeader}>
            {this.buildVoiceListHeader()}
          </div>
          <div className={styles.voiceScrollContainer} style={{height: `${this.props.accountSelectorHeight + (this.state.advancedOptionsOpen ? VoiceFilteringConstants.ADVANCED_OPTIONS_HEIGHT : 0)}px`}}>
            <InfiniteScroll
              className={loadingAnimationStyles}
              containerHeight={this.props.accountSelectorHeight + (this.state.advancedOptionsOpen ? VoiceFilteringConstants.ADVANCED_OPTIONS_HEIGHT : 0)}
              elementHeight={VoiceFilteringConstants.ELEMENT_HEIGHT}
              isInfiniteLoading={this.props.voicesLoading}
              loadingSpinnerDelegate={this.loadingVoices()}
              infiniteLoadBeginEdgeOffset={this.props.accountSelectorHeight} >
              {this.buildVoiceList()}
            </InfiniteScroll>
          </div>
        </div>
      );
    }
    return (
      <div className={styles.voiceListParentDiv}>
        {elementToRender}
      </div>
    );
  }
};
