/* @flow */
import React from 'react';
import { publishBarText } from 'constants/UITextConstants';
import { MessageConstants } from 'constants/ApplicationConstants';

import Button from '@spredfast/react-lib/lib/Button';
import '@spredfast/react-lib/styles/Button.scss';

import styles from 'styles/modules/components/MessagePublishBar.scss';

type Props = {
  publishImmediately: bool,
  onScheduleClicked: () => void,
  onCancelClicked: () => void,
  canPublish: bool,
  canSaveDraft: bool,
  cancelVerbage: string,
  hasEditAccess: bool,
  canUseMultichannelPubs: bool
};

const schedule_button_classes = styles.publishBarButton + ' ' + styles.publishBarButtonSchedule;
const cancel_button_classes = styles.publishBarButton + ' ' + styles.publishBarButtonCancel;
const save_draft_button_classes = styles.publishBarButton + ' ' + styles.publishBarButtonSaveDraft;

const MessagePublishBar = (props: Props) => {
  let publishProps = {};
  let draftProps = {};

  if (!props.canPublish) {
    publishProps.disabled = true;
  }

  if (!props.canSaveDraft) {
    draftProps.disabled = true;
  }

  const publishButtonText = props.publishImmediately ? publishBarText.immediatelyPublishText : publishBarText.schedulePublishText;

  return (
    <div className={styles.publishBar}>
      {(props.hasEditAccess && props.canUseMultichannelPubs) ?
        <div className={styles.saveButtonsContainer}>
          <Button {...publishProps} className={schedule_button_classes} onClick={() => {
            props.onScheduleClicked(MessageConstants.PENDING);
          }}>{publishButtonText}</Button>

          <Button {...draftProps} className={save_draft_button_classes} onClick={() => {
            props.onScheduleClicked(MessageConstants.DRAFT);
          }}>{publishBarText.saveDraftButtonText}</Button>
        </div>
      :
      null
    }

      <Button className={cancel_button_classes} onClick={() => {
        props.onCancelClicked();
      }}>{props.cancelVerbage}</Button>
    </div>
  );
};

export default MessagePublishBar;
