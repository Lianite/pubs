import React from 'react';
import styles from 'styles/modules/components/media/PlaceHolder.scss';
import uploadMediaImage from 'images/UploadMedia.svg';
import { mediaBarText } from 'constants/UITextConstants';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

type PlaceHolderProps = {
  text: string,
  dragging: bool,
  onLoadFiles: Function
};

const PlaceHolder = ({text, dragging, onLoadFiles} : PlaceHolderProps) => {
  const iconStyle = cx('mediaBarIcon', {dragging});
  return (
    <div className={styles.placeHolder}>
      <img className={iconStyle} src={uploadMediaImage}></img>
      <span>{text}</span>
      {!dragging ?
        <button onClick={onLoadFiles} className={styles.mediaBarButton}>
          {mediaBarText.computer}
        </button> : null}
    </div>
  );
};

export default PlaceHolder;