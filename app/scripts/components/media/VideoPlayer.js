import React from 'react';
import styles from 'styles/modules/components/media/VideoPlayer.scss';
import { videoPlayer } from 'constants/UITextConstants';

type VideoPlayerProps = {
  onClose: Function,
  video: string
};

const VideoPlayer = ({video, onClose}:VideoPlayerProps) => {
  return (
    <div className={styles.videoPlayer}>
      <button type='button' className={styles.closeButton} onClick={onClose}>
        <span className='icon-modal_close'></span>
      </button>
      <video controls>
        <source src={video}/>
        {videoPlayer.unsupported}
      </video>
    </div>
  );
};

export default VideoPlayer;