/* @flow */
import React, { Component }  from 'react';
import styles from 'styles/modules/components/media/FrameGrab.scss';
import classNames from 'classnames/bind';
import _ from 'lodash';

import type { VideoThumbnailType } from 'adapters/types';

const cx = classNames.bind(styles);

type FrameGrabProps = {
  initialThumbnail: VideoThumbnailType,
  possibleThumbnails: Array<VideoThumbnailType>,
  selectedThumbnailChanged: (selectedThumbnail: VideoThumbnailType) => void
};

type FrameGrabState = {
  currentlySelectedThumb: VideoThumbnailType
}

type PotentialThumbnailProps = {
  selected?: bool,
  id: string,
  uri: string,
  onClick: (id: string, uri: string) => void
};

export const PotentialThumbnail = (props: PotentialThumbnailProps) => {
  const potentialThumbClass = cx({
    potentialThumbnail: true,
    selected: !!props.selected
  });

  const potentialThumbStyle = {
    backgroundImage: props.uri && `url(${props.uri})` || 'none'
  };

  //NOTE: we need a nbsp in the content or the size gets messed up
  return (
    <div className={potentialThumbClass} style={potentialThumbStyle} onClick={() => {
      props.onClick(props.id, props.uri);
    }}>
      &nbsp;
    </div>
  );
};

const renderThumbnails = (
  initialThumbnail: VideoThumbnailType,
  possibleThumbnails: Array<VideoThumbnailType>,
  onClick: (id: string, uri: string) => void
  ) => {
  const thumbnailComponents = possibleThumbnails.map((thumbnail: VideoThumbnailType): any => {
    const isSelected = _.isEqual(initialThumbnail, thumbnail);
    return (
      <PotentialThumbnail
        key={thumbnail.id}
        id={thumbnail.id}
        uri={thumbnail.uri}
        selected={isSelected}
        onClick={onClick}
      />
    );
  });

  return thumbnailComponents;
};

class FrameGrab extends Component<void, FrameGrabProps, FrameGrabState> {
  static displayName = 'FrameGrab';
  state: FrameGrabState;
  onNewThumbnailSelected: (id:string, uri:string) => void;

  constructor(props: FrameGrabProps) {
    super(props);
    this.state = {
      currentlySelectedThumb: props.initialThumbnail
    };

    this.onNewThumbnailSelected = this.onNewThumbnailSelected.bind(this);
  }

  onNewThumbnailSelected(id: string, uri: string) {
    const newThumbnail = {id, uri};
    if (this.props.selectedThumbnailChanged && !_.isEqual(newThumbnail, this.state.currentlySelectedThumb)) {
      this.props.selectedThumbnailChanged(newThumbnail);
    }

    this.setState({
      currentlySelectedThumb: newThumbnail
    });
  }

  render() {
    return (
      <div className={styles.frameGrab}>
        {renderThumbnails(this.state.currentlySelectedThumb, this.props.possibleThumbnails, this.onNewThumbnailSelected)}
      </div>
    );
  }
};

export default FrameGrab;
