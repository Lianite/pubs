/* @flow */
import React, { Component } from 'react';
import VideoPlayer from 'components/media/VideoPlayer';
import Dropzone from 'react-dropzone';
import { videoText } from 'constants/UITextConstants';
import classNames from 'classnames/bind';
import { ReduxModal } from '@spredfast/react-lib';
import FrameGrabModal from 'components/media/FrameGrabModal';

import styles from 'styles/modules/components/media/VideoThumbnail.scss';

import type { VideoThumbnailType } from 'adapters/types';

const cx = classNames.bind(styles);

type State = {
  videoPlayerOpen: bool,
  customizeMenuOpen: bool
};

type Props = {
  image: string,
  video: string,
  currentlyDraggingMedia:bool,
  canCustomize?: bool,
  onDropFile?: ((file:File) => void),
  isAlreadyUploading?: bool,
  openModal?: (modal: any, id: string, props: Object) => void,
  onVideoThumbnailFrameChanged?: (thumbnail: VideoThumbnailType) => void
};

let dropzoneRef;

const customizeThumbnailClicked = () => {
  if (dropzoneRef) {
    dropzoneRef.open();
  }
};

type CustomizeThumbnailProps = {
  onMenuClicked: () => void,
  onFolderClicked: () => void,
  onGridClicked:() => void,
  expanded: bool
}

export const CustomizeThumbnail = (props: CustomizeThumbnailProps) => {
  const icon = props.expanded ? 'rad-icon-chevron_up' : 'rad-icon-chevron_down';
  const customizeThumbnailStyle = cx({
    'customizeThumbnail': true,
    'expanded': props.expanded
  });
  return (
    <div className={customizeThumbnailStyle}>
      <div className={styles.topMenu}>
        <button onClick={props.onMenuClicked}>
          <span>{videoText.customizeThumbnail}</span>
          <i className={icon} />
        </button>
      </div>
      <div className={styles.expandedMenu}>
        <div>
          <button onClick={props.onFolderClicked}>
            <i className='icon-folder' />
            <span>{videoText.selectFromComp}</span>
          </button>
        </div>
        <div>
          <button onClick={props.onGridClicked}>
            <i className='icon-grid-9' />
            <span>{videoText.frameGrab}</span>
          </button>
        </div>
      </div>
    </div>
  );
};

const onDrop = (onDropCallback, files: Array<File>) => {
  if (onDropCallback && files && files.length > 0) {
    onDropCallback(files[0]); //If we are going to handle multiple link preview custom thumb uploads, need to change this
  }
};

export class VideoThumbnail extends Component<void, Props, State> {
  static displayName = 'VideoThumbnail';
  state: State;
  toggleVideoPlayerOpen: Function;
  toggleCustomizeMenuOpen: Function;
  getCustomThumbFromFile: Function;
  getCustomThumbFromFrameGrab: Function;

  constructor(props: Props) {
    super(props);
    this.state = {
      videoPlayerOpen: false,
      customizeMenuOpen: false
    };
    this.toggleVideoPlayerOpen = this.toggleVideoPlayerOpen.bind(this);
    this.toggleCustomizeMenuOpen = this.toggleCustomizeMenuOpen.bind(this);
    this.getCustomThumbFromFile = this.getCustomThumbFromFile.bind(this);
    this.getCustomThumbFromFrameGrab = this.getCustomThumbFromFrameGrab.bind(this);
  }

  toggleCustomizeMenuOpen() {
    this.setState({
      customizeMenuOpen: !this.state.customizeMenuOpen
    });
  }

  toggleVideoPlayerOpen() {
    this.setState({
      videoPlayerOpen: !this.state.videoPlayerOpen
    });
  }

  getCustomThumbFromFile() {
    customizeThumbnailClicked();
    this.toggleCustomizeMenuOpen();
  }

  getCustomThumbFromFrameGrab() {
    if (this.props.openModal) {
      this.props.openModal(FrameGrabModal, 'frameGrab', { /*props*/ });
    }
  }

  modalHandlers = {
    // This is how we handle a response passed by the modal we opened.
    frameGrab: (response, close) => {
      if (this.props.onVideoThumbnailFrameChanged && response) {
        this.props.onVideoThumbnailFrameChanged(response);
      }
      close();
      //close the menu if open
      if (this.state.customizeMenuOpen) {
        this.toggleCustomizeMenuOpen();
      }
    }
  }

  render() {
    const draggingOverlay = (this.props.currentlyDraggingMedia && this.props.canCustomize) ?
      <div className={styles.draggingMediaOverlay}></div> : null;
    const acceptedImageTypes = this.props.isAlreadyUploading ? 'none' : 'image/*';
    const customizeThumbnail = this.props.canCustomize ?
      <CustomizeThumbnail
        expanded={this.state.customizeMenuOpen}
        onMenuClicked={this.toggleCustomizeMenuOpen}
        onFolderClicked={this.getCustomThumbFromFile}
        onGridClicked={this.getCustomThumbFromFrameGrab}
      />
    :
      null;
    const thumbnailContent = (
      <div className={styles.thumbnailContent}>
        <div className={styles.thumbnail} onClick={this.toggleVideoPlayerOpen}>
          {draggingOverlay}
          <i className={cx('icon-play-button', 'playIcon')} />
          {this.state.videoPlayerOpen ? <VideoPlayer video={this.props.video} onClose={this.toggleVideoPlayerOpen.bind(this)}/> : null}
          <img src={this.props.image} alt='video-preview-thumbnail'/>
        </div>
        {customizeThumbnail}
      </div>
    );
    if (this.props.canCustomize) {
      return (
        <Dropzone ref = {node => {
          dropzoneRef = node;
        }}
          onDrop={onDrop.bind(null, this.props.onDropFile)}
          className={styles.customThumbDropzone}
          activeClassName={styles.customThumbDropAccept}
          rejectClassName={styles.customThumbDropReject}
          multiple={false}
          disableClick
          accept={acceptedImageTypes}>
          {thumbnailContent}
        </Dropzone>
      );
    } else {
      return thumbnailContent;
    }
  }
};

// You need to register your modal with the ReduxModal Host
ReduxModal.Host.registerModalType(FrameGrabModal);

export default ReduxModal.ModalResponder(VideoThumbnail);
