import React  from 'react';
import { DefaultNetwork, networkIcons } from 'constants/ApplicationConstants';
import { networkListText } from 'constants/UITextConstants';
import styles from 'styles/modules/components/media/NetworkList.scss';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

function thereIsNoNetworkSelected(networks) {
  return networks.length === 0 || (networks.length === 1 && networks[0] === DefaultNetwork);
}

type NetworkListProps = {
  networks: Array<string>
};

const NetworkList = ({networks}: NetworkListProps) => {
  if (thereIsNoNetworkSelected(networks)) {
    return (<span className={styles.noNetworkSelected}>{networkListText.noNetworkSelected}</span>);
  }
  networks = networks.map(network => network.toLowerCase());
  return (
    <span className={styles.networks}>
      {networks.map(network => <i key={network} className={cx(networkIcons[network.toUpperCase()], styles.icon, styles[network])} />)}
    </span>
  );
};

export default NetworkList;
