/* @flow */
import React  from 'react';
import styles from 'styles/modules/components/media/Video.scss';
import { LoadingAnimation } from '@spredfast/react-lib';
import { videoText } from 'constants/UITextConstants';
import VideoThumbnail from 'components/media/VideoThumbnail';
import NetworkList from './NetworkList';
import ImageError from 'components/media/ImageError';

import type { VideoType } from 'selectors/AttachedMediaSelector';
import type { VideoThumbnailType } from 'adapters/types';

type ThumbnailHolderProps = {
  text: string
};
const ThumbnailHolder = ({text}: ThumbnailHolderProps) => {
  return (
    <div className={styles.thumbnailHolder}>
      <LoadingAnimation />
      <span>{text}</span>
    </div>
  );
};

type VideoDetailsProps = {
  title: string
};

const VideoDetails = ({title} : VideoDetailsProps) => {
  return (
    <div className={styles.videoDetails}>
      <h3>{title || videoText.videoTitle}</h3>
    </div>
  );
};


type CloseButtonProps = {
  onClick: Function
};

const CloseButton = ({onClick} : CloseButtonProps) => {
  return (
    <button type='button' className={styles.closeButton} onClick={onClick}>
      <span className='icon-modal_close'></span>
    </button>
  );
};

type VideoProps = {
  currentlyDraggingMedia: bool,
  video: VideoType,
  onRemove: () => void,
  networks: Array<string>,
  onDropCustomThumb?: ((file:File) => void),
  removeOrCancelCustomThumb?: () => void,
  onVideoThumbnailFrameChanged?: (thumbnail: VideoThumbnailType) => void
};

const Video = ({video,
  onRemove,
  networks,
  currentlyDraggingMedia,
  onDropCustomThumb,
  removeOrCancelCustomThumb,
  onVideoThumbnailFrameChanged} : VideoProps) => {

  const error = video.videoError;
  const canCustomizeThumb = video.canCustomizeThumb;
  const customThumb = video.customThumb;
  const customThumbWarning = video.customThumbWarning;
  const onCloseButtonClick = removeOrCancelCustomThumb || (() => {});

  const transcodingPercent: number = video.videoTranscodePercent || 0;
  const transcodingMessage = videoText.transcoding + ' (' + Math.round(transcodingPercent) + '%)';

  const uploadingPercent: number = video.uploadPercent || 0;
  const uploadingMessage = videoText.uploading + ' (' + Math.round(uploadingPercent) + '%)';

  const videoStatus = video.uploading ? uploadingMessage : (video.customThumbUploading ? videoText.customThumbUploading : transcodingMessage);
  const thumbnailComponent = !video.thumbnail || video.customThumbUploading ?
    <ThumbnailHolder text={videoStatus}/> :
    <VideoThumbnail
      currentlyDraggingMedia={currentlyDraggingMedia}
      image={canCustomizeThumb && customThumb ? customThumb : video.thumbnail}
      video={video.previewUrl}
      canCustomize={!!canCustomizeThumb}
      onDropFile={onDropCustomThumb}
      onVideoThumbnailFrameChanged={onVideoThumbnailFrameChanged}/>;

  const closeCancelThumbButton = (video.customThumbUploading || (customThumb && canCustomizeThumb) || (error && error.dismissable)) ? (<span className='icon-remove' onClick={onCloseButtonClick}></span>) : null;
  const infoMessage = customThumbWarning ? (<p>{videoText.customThumbWarning}</p>) : null;

  return (
    <div className={styles.video}>
      <div className={styles.leftColumn}>
        {error ? <ImageError title={error.title} description={error.description} /> :
        thumbnailComponent}
        {closeCancelThumbButton}
      </div>
      <div className={styles.rightColumn}>
        <div className={styles.topContainer}>
          <VideoDetails title={video.title || ''}/>
          <CloseButton onClick={onRemove}/>
        </div>
        <div className={styles.middleContainer}>
          {infoMessage}
        </div>
        <div className={styles.bottomContainer}>
          <NetworkList networks={networks}/>
        </div>
      </div>
    </div>
  );
};

export default Video;
