/* @flow */
import React from 'react';
import styles from 'styles/modules/components/media/ImageError.scss';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

type ImageErrorProps = {
  title: string,
  description: string
};
const ImageError = ({title, description}: ImageErrorProps) => {
  return (
    <div className={styles.imageErrorContainer}>
      <div className={styles.mediaErrorText}>
        <div>
          <i className={cx('rad-icon-warning-dot', 'validationIcon', 'error')} />
          <p className={styles.mediaErrorTitle}>{title}</p>
          <p className={styles.mediaErrorDescription}>{description}</p>
        </div>
      </div>
      <div className={styles.mediaErrorOverlay}></div>
    </div>
  );
};

export default ImageError;
