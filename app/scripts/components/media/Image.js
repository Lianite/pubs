/* @flow */
import React from 'react';
import ImageError from 'components/media/ImageError';
import styles from 'styles/modules/components/media/Image.scss';
import { LoadingAnimation } from '@spredfast/react-lib';
import classNames from 'classnames/bind';
import NetworkList from './NetworkList';

import type { PublishingError } from 'records/ErrorRecords';

const cx = classNames.bind(styles);

type ImageProps = {
  networks: Array<string>,
  uploading: boolean,
  image: string,
  onRemove: Function,
  mediaFilesizeError: ?PublishingError
};

const Image = (props: ImageProps) => {
  const imagePlaceHolderStyle = {
    backgroundImage: props.image && `url(${props.image})` || 'none'
  };

  return (
    <div className={styles.image}>
      <div className={styles.mediaPlaceholderImage} style={imagePlaceHolderStyle}>
        {props.uploading ?
          <LoadingAnimation /> : null}
        {props.mediaFilesizeError ?
          <ImageError title={props.mediaFilesizeError.title} description={props.mediaFilesizeError.description}/> : null}
        <i className={cx('mediaRemoveButton', 'icon-remove')} onClick={props.onRemove}/>
      </div>
      {!props.uploading ?
        <div className={styles.networkBar}><NetworkList networks={props.networks} /></div> : null}
    </div>
  );
};

export default Image;
