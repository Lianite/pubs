/* @flow */
import React, { Component } from 'react';
import { Modal, Button } from '@spredfast/react-lib';
import FrameGrabContainer from 'containers/media/FrameGrabContainer';

import { FRAME_GRAB_TEXT } from 'constants/UITextConstants';

import '@spredfast/react-lib/styles/Button.scss';
import styles from 'styles/modules/components/media/FrameGrabModal.scss';

import type { VideoThumbnailType } from 'adapters/types';

type Props = {
  respond: Function,
  close: Function
};

class FrameGrabModal extends Component<void, Props, void> {
  static displayName = 'FrameGrabModal';

  props: Props;

  currentlySelectedThumb: VideoThumbnailType;
  selectedThumbnailChanged: Function;

  constructor(props: Props) {
    super(props);

    this.selectedThumbnailChanged = this.selectedThumbnailChanged.bind(this);
  }

  selectedThumbnailChanged(thumbnail:VideoThumbnailType) {
    this.currentlySelectedThumb = thumbnail;
  }

  render() {
    const { respond } = this.props;
    return (
      <div className={styles.frameGrabModalContainer}>
        <Modal.Modal isLarge onEsc={respond.bind(false)}>
          <Modal.Header>
            <span>{FRAME_GRAB_TEXT.FRAME_GRAB_HEADER_TEXT}</span>
            <button className={styles.closeButton} onClick={respond.bind(false)}>
              <i className='icon-modal-close-thick' />
            </button>
          </Modal.Header>
          <Modal.Body>
            <FrameGrabContainer
              selectedThumbnailChanged={this.selectedThumbnailChanged} />
          </Modal.Body>
          <Modal.Footer>
            <Modal.FooterButtons>
              <Button sfType='secondary' size='sm' onClick={() => {
                respond(false);
              }}>{FRAME_GRAB_TEXT.FRAME_GRAB_SECONDARY_BUTTON_TEXT}</Button>
              <Button sfType='primary' size='sm' onClick={() => {
                respond(this.currentlySelectedThumb || false);
              }}>{FRAME_GRAB_TEXT.FRAME_GRAB_PRIMARY_BUTTON_TEXT}</Button>
            </Modal.FooterButtons>
          </Modal.Footer>
        </Modal.Modal>
      </div>
    );
  }
}

export default FrameGrabModal;
