/* @flow */
import React, {Component} from 'react';
import SidebarButton from 'components/SidebarButton';
import { SidebarPages } from 'constants/UIStateConstants';
import {SidebarTexts, SidebarComingSoonTexts} from 'constants/UITextConstants';
import { networkIcons, SupportedNetworks } from 'constants/ApplicationConstants';
import styles from 'styles/modules/components/OptionsSideBar.scss';
import type { Map as ImmutableMap, Set as ImmutableSet } from 'immutable';
import LinksContextMenu from 'components/optionsbar/LinksContextMenu';
import BasicsContextMenuContainer from 'containers/BasicsContextMenuContainer';
import NotesMenuContainer from 'containers/optionsbar/NotesMenuContainer';
import FacebookMenuContainer from 'containers/optionsbar/networks/FacebookMenuContainer';
import WorkflowContainer from 'containers/optionsbar/WorkflowContainer';
import ReactTransitionGroup from 'react-addons-transition-group';
import _ from 'lodash';

import comingSoonPlaceholder from 'images/comingsoon.png';
import type { SidebarButtonsType } from 'constants/UIStateConstants';
import type { LinkTagDetails } from 'records/LinkTagsRecords';
import type { UrlPropertiesRecord } from 'records/LinksRecords';

type Props = {
  sidebarOpen: bool,
  activePage: string,
  onSidebarTabClicked: () => void,
  onSidebarClosed: () => void,
  onChangeLinkProperty: () => void,
  urls: ImmutableSet<string>,
  urlProperties: ImmutableMap<string, UrlPropertiesRecord>,
  activeNetworks: Array<string>,
  addLinkPreview: Function,
  selectedLinkPreviewUrl: string,
  linkTagsByLink: ImmutableMap<string, LinkTagDetails>,
  linkTagsEnable: bool,
  updateLinkTagVariable: Function,
  canAddAttachmentByType: {linkPreview: boolean, images: boolean, video: boolean},
  facebookHasError: bool
};

type ButtonInfo = {
  buttonImage: string,
  pageName: string,
  isActive?: bool,
  error?: bool
};

const createSidebarButton = (buttonInfo: ButtonInfo, onSidebarButtonClicked: any) => {
  return (<SidebarButton
    buttonImage={buttonInfo.buttonImage}
    buttonPageName={buttonInfo.pageName}
    onClick={onSidebarButtonClicked}
    active={buttonInfo.isActive}
    key={buttonInfo.pageName}
    error={buttonInfo.error}/>);
};

const buildOptionsContextMenu = (props:Props) => {
  switch (props.activePage) {
  case SidebarPages.BASICS:
    return (<span><BasicsContextMenuContainer/></span>);
  case SidebarPages.LINKS:
    return (
      <LinksContextMenu
        urls={props.urls}
        urlProperties={props.urlProperties}
        onChangeLinkProperty={props.onChangeLinkProperty}
        addLinkPreview={props.addLinkPreview}
        linkTagsEnable={props.linkTagsEnable}
        selectedLinkPreviewUrl={props.selectedLinkPreviewUrl}
        linkTagsByLink={props.linkTagsByLink}
        updateLinkTagVariable={props.updateLinkTagVariable}
        canAddAttachmentByType={props.canAddAttachmentByType}/>
    );
  case SidebarPages.NOTES:
    return (
      <NotesMenuContainer />
    );
  case SidebarPages.FACEBOOK:
    return (
      <FacebookMenuContainer />
   );
  case SidebarPages.WORKFLOW:
    return (
      <WorkflowContainer />
    );

  default:
    return props.sidebarOpen ?
    (
      <div className={styles.comingSoonPlaceholder}>
        <img src={comingSoonPlaceholder} />
        <h3>{SidebarTexts[props.activePage]} is arriving soon!</h3>
        <h2>{SidebarComingSoonTexts[props.activePage]}</h2>
      </div>
    )
    :
    null;
  }
};

export default class OptionsSideBar extends Component<void, Props, void> {
  static displayName = 'OptionsSideBar';

  constructor(props: Props) {
    super(props);
  }

  componentWillReceiveProps(nextProps: Props) {
    const {activePage, activeNetworks, urls, onSidebarTabClicked, sidebarOpen, selectedLinkPreviewUrl} = nextProps;

    if ( activeNetworks === this.props.activePage ) {
      return;
    }

    // This needs to happen first because of racing!
    switch (activePage) {
    case SidebarPages.LINKS:
      if ( urls.size < 1 && !selectedLinkPreviewUrl) {
        onSidebarTabClicked(sidebarOpen, activePage, SidebarPages.BASICS);
      }
      break;

    case SidebarPages.FACEBOOK:
      if ( _.indexOf(activeNetworks, 'FACEBOOK') === -1) {
        onSidebarTabClicked(sidebarOpen, activePage, SidebarPages.BASICS);
      }
      break;

    case SidebarPages.TWITTER:
      if ( _.indexOf(activeNetworks, 'TWITTER') === -1) {
        onSidebarTabClicked(sidebarOpen, activePage, SidebarPages.BASICS);
      }
      break;

    default:
      break;
    }
  }

  buildButtonList(onSidebarButtonClicked: any, anyLinks: bool, activeNetworks: Array<string>, activePage: string) {
    let buttons: SidebarButtonsType = {
      [SidebarPages.BASICS] : {
        buttonImage: 'icon-basics',
        pageName: SidebarTexts[SidebarPages.BASICS]
      },
      [SidebarPages.WORKFLOW] : {
        buttonImage: 'icon-workflow',
        pageName: SidebarTexts[SidebarPages.WORKFLOW]
      },
      [SidebarPages.NOTES] : {
        buttonImage: 'icon-note',
        pageName: SidebarTexts[SidebarPages.NOTES]
      }
    };


    // Temporary control for whether or not the links tab should be displayed
    if (anyLinks) {
      buttons[SidebarPages.LINKS] = {
        buttonImage: 'icon-link',
        pageName: SidebarTexts[SidebarPages.LINKS]
      };
    }

    if ( _.indexOf(activeNetworks, 'FACEBOOK') > -1) {
      buttons[SidebarPages.FACEBOOK] = {
        buttonImage: networkIcons[SupportedNetworks.FACEBOOK],
        pageName: SidebarTexts[SidebarPages.FACEBOOK],
        error: this.props.facebookHasError
      };
    }

    if ( _.indexOf(activeNetworks, 'TWITTER') > -1) {
      buttons[SidebarPages.TWITTER] = {
        buttonImage: networkIcons[SupportedNetworks.TWITTER],
        pageName: SidebarTexts[SidebarPages.TWITTER]
      };
    }

    //Set the active button
    if (activePage && buttons[activePage]) {
      buttons[activePage].isActive = true;
    } else if (activePage) {
      buttons[SidebarPages.BASICS].isActive = true;
    }

    let domButtons = [];
    Object.keys(buttons).forEach((key: string) => {
      //the final parameter expected is the requested page, which will be the key for this
      // entry in the array
      domButtons.push(createSidebarButton(buttons[key], onSidebarButtonClicked.bind(this, key)));
    });

    domButtons.push(<div key={'buttonsFiller'} className={styles.buttonsFiller}/>);
    return domButtons;
  };

  render() {
    const {sidebarOpen, activePage, onSidebarTabClicked, onSidebarClosed, urls, activeNetworks, selectedLinkPreviewUrl} = this.props;
    let optionsSideBarStyle = styles.optionsSideBar;
    let closeButtonStyle = styles.closeButton;
    let optionButtonsContainer = styles.optionButtonsContainer;
    let optionsContextMenu = styles.optionsContextMenu;

    if (sidebarOpen) {
      optionsSideBarStyle += (' ' + styles.optionsSideBarOpen);
      closeButtonStyle += (' ' + styles.closeButtonSidebarOpen);
    }

    let onCloseButtonClicked = onSidebarClosed.bind(this, sidebarOpen);
    let onSidebarButtonClicked = onSidebarTabClicked.bind(this, sidebarOpen, activePage);

    return (
      <div className={optionsSideBarStyle} >
        <ReactTransitionGroup component='div' className={optionButtonsContainer}>
          {this.buildButtonList(onSidebarButtonClicked, (urls.size > 0 || !!selectedLinkPreviewUrl), activeNetworks, activePage)}
        </ReactTransitionGroup>
        <div className={optionsContextMenu}>
          {buildOptionsContextMenu(this.props)}
          <button type='button' className={closeButtonStyle} onClick={onCloseButtonClicked}>
            <span className='icon-modal_close'></span>
          </button>
        </div>
      </div>
    );
  }
}
