import React, {Component} from 'react';
import { Route, IndexRoute } from 'react-router';
import MainContainer from 'containers/MainContainer';
import { getConversationsEnviromentDetails } from 'utils/ApiUtils';

class RedirectToDefaultCompanyValue extends Component<DefaultProps, Props, void> {
  constructor(refs) {
    super();

    const companyLocation = (refs.params[0] || getConversationsEnviromentDetails().externalCompanyId);
    const queryParams = refs.location.search;

    refs.history.push(`/company/${companyLocation}${queryParams}`);
  }

  render() {
    return null;
  }
};

export default (
  <Route path='/'>
    <Route path='company'>
      <Route path=':companyId' component={MainContainer}>
        <Route path='campaign'>
          <Route path=':campaignId'>
            <Route path='edit'>
              <Route path=':eventId'/>
            </Route>
            <Route path='copy'>
              <Route path=':copyId'/>
            </Route>
          </Route>
        </Route>
      </Route>
      <IndexRoute component={RedirectToDefaultCompanyValue} />
    </Route>
    <IndexRoute component={RedirectToDefaultCompanyValue} />
  </Route>
);
