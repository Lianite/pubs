/* @flow */
import _ from 'lodash';
import type { AssigneeData } from 'adapters/types';
import { List as ImmutableList } from 'immutable';
import { AssigneeRecord } from 'records/AssigneeRecords';

export const createAssigneesFromPayload = (payload: Array<AssigneeData>): ImmutableList<AssigneeRecord> => {
  return ImmutableList(_.map(payload, (assignee) => {
    return new AssigneeRecord({
      id: assignee.id,
      firstName: assignee.firstName,
      lastName: assignee.lastName,
      invalid: false
    });
  }));
};
