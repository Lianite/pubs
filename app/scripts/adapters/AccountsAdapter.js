/* @flow */
import { CredentialRecord } from 'records/AccountsRecord';
import { Map as ImmutableMap } from 'immutable';
import { getFilteredCredentials } from 'selectors/AccountsSelectors';
import {
  canTarget as canFacebookTarget,
  targetingSet as facebookTargetingSet,
  canDarkPost
} from 'selectors/FacebookSelector';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { checkedCredentialsSelector, getDarkPostStatus } from 'reducers/index';
import _ from 'lodash';

import type { rootState } from 'reducers/index';
// old utils functions

const validForTargetingState = (cred: CredentialRecord, state: rootState): bool => {
  switch (cred.service) {
  case SupportedNetworks.FACEBOOK:
    return !canFacebookTarget(state) || !facebookTargetingSet(state) || (cred.authenticated && cred.targetingSupported);
  }
  return true;
};

const validForDarkPostState = (cred: CredentialRecord, state: rootState): bool => {
  switch (cred.service) {
  case SupportedNetworks.FACEBOOK:
    return !getDarkPostStatus(state) || !canDarkPost(state) || (cred.organicVisibilitySupported);
  }
  return true;
};

const getVoiceCheckedState = (credentials, checkedCredentials, state: rootState) => {
  let allChecked = true;

  credentials.forEach(cred => {
    if (validForTargetingState(cred, state) && validForDarkPostState(cred, state) && checkedCredentials.get(cred.uniqueId) === undefined) {
      allChecked = false;
    }
  });

  return allChecked;
};

const getTargetVoice = (state, voiceId) => {
  const voices = getFilteredCredentials(state);

  // we need to define a flow type in this case for voices, however, with a
  // backend change that will be refactored in the FE soon, I don't think it's worth
  // annotating until that change is made. $FlowFixMe
  return _.find(voices, voice => voice.id === voiceId).credentials;
};

const buildCredentialData = ({id, service, socialNetworkId, uniqueId, name, accountType, targetingSupported, authenticated, organicVisibilitySupported}) => {
  return new CredentialRecord({
    id,
    name,
    socialCredentialType: accountType,
    socialNetwork: service,
    socialNetworkId: socialNetworkId,
    uniqueId,
    targetingSupported,
    authenticated,
    organicVisibilitySupported
  });
};

export const buildVoicesCheckedState = (voiceId: number, credentialId?: string, state: rootState): ImmutableMap<string, CredentialRecord> => {
  let checkedCredentials = checkedCredentialsSelector(state);
  let voiceCredentials = null;

  if (credentialId) {
    if (checkedCredentials.get(credentialId)) {
      return checkedCredentials.delete(credentialId);
    } else {
      voiceCredentials = getTargetVoice(state, voiceId);

      const credential = _.find(voiceCredentials, cred => {
        return cred.uniqueId === credentialId;
      });
      if (validForTargetingState(credential, state) && validForDarkPostState(credential, state)) {
        return checkedCredentials.set(credentialId, buildCredentialData(credential));
      }
    }
  } else {
    voiceCredentials = getTargetVoice(state, voiceId);
    const unCheckCredentials = getVoiceCheckedState(voiceCredentials, checkedCredentials, state);
    const newlyCheckedCredentials = {};
    const alreadyCheckedCredentials = {};

    voiceCredentials.forEach(cred => {
      checkedCredentials.forEach((value) => {
        if (value.id !== cred.id && value.uniqueId === cred.uniqueId) {
          alreadyCheckedCredentials[cred.uniqueId] = true;
        }
      });
      if (validForTargetingState(cred, state) && validForDarkPostState(cred, state) && !alreadyCheckedCredentials[cred.uniqueId]) {
        newlyCheckedCredentials[cred.uniqueId] = buildCredentialData(cred);
      }
    });

    if (unCheckCredentials) {
      return checkedCredentials.filter((value) => !(_.indexOf(Object.keys(newlyCheckedCredentials), value.uniqueId) > -1));
    } else {
      return checkedCredentials.merge(ImmutableMap(newlyCheckedCredentials));
    }
  }

  return checkedCredentials;
};

export const checkAllVoices = (state: rootState): ImmutableMap<string, CredentialRecord> => {
  let checkedCredentials = checkedCredentialsSelector(state);
  const voices = getFilteredCredentials(state);

  checkedCredentials = checkedCredentials.withMutations(checkedCredential => {
    voices.forEach(voice => {
      voice.credentials.forEach(cred => {
        if (validForTargetingState(cred, state) && validForDarkPostState(cred, state) && !checkedCredential.has(cred.uniqueId)) {
          checkedCredential.set(cred.uniqueId, buildCredentialData(cred));
        }
      });
    });
  });

  return checkedCredentials;
};

export const uncheckAllVoices = (state: rootState): ImmutableMap<string, CredentialRecord> => {
  let checkedCredentials = checkedCredentialsSelector(state);
  const voices = getFilteredCredentials(state);

  checkedCredentials = checkedCredentials.withMutations(checkedCredential => {
    voices.forEach(voice => {
      voice.credentials.forEach(cred => {
        if (checkedCredential.keySeq().contains(cred.uniqueId)) {
          checkedCredential.remove(cred.uniqueId);
        }
      });
    });
  });

  return checkedCredentials;
};
