/* @flow */
import { TARGETING_TYPES, TARGETING_SUBTYPES } from 'constants/ApplicationConstants';
import type { Map as ImmutableMap } from 'immutable';
import type { SupportedNetworksType } from 'constants/ApplicationConstants';
import type { CredentialRecord } from 'records/AccountsRecord';

export type SocialNetworkAccount = {
  socialCredentialType: string,
  id: number,
  name: string,
  socialNetworkId: string,
  uniqueId: string,
  targetingSupported?: bool,
  authenticated?: bool,
  service?: string,
  organicVisibilitySupported?: bool
};

export type Plan = {
  id: string,
  name: string,
  color: string
};

export type PlanList = Array<Plan>;

export type LinkPreviewType = {
  linkUrl: string,
  thumbnailUrl?: string,
  description: string,
  caption: string,
  title: string,
  videoSrc?: string,
  videoLink?: string
};

export type VideoThumbnailType = {|
  id: string,
  uri: string
|};

export type VideoType = {
  id: number,
  displayName: string,
  duration?: ?number,
  size?: ?number,
  transcodedSize?: ?number,
  summary?: ?string,
  videoThumbnail?: ?VideoThumbnailType,
  videoURI?: ?string,
  videoStatus?: ?string,
  type?: ?string,
  callToAction?: ?{
      present: bool
  },
  videoTitle?: ?string,
  tags?: ?Array<string>,
  category?: ?string,
  targetService?: ?string,
  thumbnailCandidates?: any,
  videoPreview?: ?string,
  videoS3Key?: ?string
};

export type LinksByUrlPayloadObject = {
  fullUrl: string,
  shortenOnPublish: bool,
  addLinkTags: bool
};

export type LinkDTOsType = Array<LinksByUrlPayloadObject>;

export type SingleChannelMessage = {
  socialNetwork: string,
  messageContent: {|
    text: string,
    imageIDs: Array<number>,
    links: any,
    linkPreview: ?LinkPreviewType,
    linkDTOs: LinkDTOsType,
    targetingProfile?: Object,
    video: ?VideoType
  |},
  socialNetworkAccounts: Array<SocialNetworkAccount>,
  organicVisibility: string
};

export type ImageById = {
  id: number,
  originalUrl: string,
  thumbnailUrl: string
};

export type MentionsMessageResponse = {
  offsetBegin: number,
  offsetEnd: number,
  displayName: string,
  id?: number,
  link: string,
  profileId: string,
  service: SupportedNetworksType
};

export type Note = {
  authorFirstName: string,
  authorLastName: string,
  authorAvatarThumbnail: string,
  noteCreationTimestamp: number,
  noteText: string
};

export type NotePayload = {
    noteText: string,
    noteCreationTimestamp: number
};

export type AssigneeData = {
  id: number,
  firstName: string,
  lastName: string
};

export type ActivityPayloadData = {
  id: number,
  author: {
    id: number,
    firstName: string,
    lastName: string,
    thumb: string,
    defaultCompanyId: number
  },
  type: 'CREATE' | 'EDIT',
  createdDate: {
    date: number,
    text: string,
    timezone: {
      name: string,
      abbreviation: string,
      offset: string
    }
  }
}

export type LinksByUrlType = {
  [key:string]: LinksByUrlPayloadObject
};

export type MessageObject = {|
  eventId?: ?number,
  planId?: string,
  status?: string,
  spredfastMessageTitle: string,
  assignee: ?AssigneeData,
  activityLogs?: ?Array<ActivityPayloadData>,
  eventAuthorUserId?: number,
  userAuthorizedForEdit?: bool,
  singleChannelMessages: Array<SingleChannelMessage>,
  schedule?: {
    immediatePublish: bool,
    scheduledPublishDate?: number
  },
  imagesById?: {
    [id: string]: ImageById
  },
  mentions: Array<MentionsMessageResponse>,
  linksByUrl: LinksByUrlType,
  contentLabels: Array<Object>,
  notes?: Array<NotePayload>
|};

export type MessageStateObject = {|
  message: Object,
  images: Array<any>,
  linkPreview: Object,
  currentPlanId: string,
  assignee: ?AssigneeData,
  contentLabels: Array<any>,
  notes: {|
    notesList: Array<any>
  |},
  videos: Object,
  urls: Object,
  urlProperties: Object,
  facebook: Object,
  checkedCredentials: ImmutableMap<string, CredentialRecord>
|};

export type TargetingSubtype = $Keys<typeof TARGETING_SUBTYPES> | typeof undefined;

export type TargetingEntity = {|
  value: string,
  description: string,
  type: string,
  subType?: TargetingSubtype,
  parentTargetingValue?: TargetingEntity
|};

export type SelectedLocationsTree = {
  [key: string]: {
    description: string,
    children: Array<TargetingEntity>
  }
};

export type TargetingProfile = {|
  type: string,
  service: SupportedNetworksType,
  targetingValues: {|
    [typeof TARGETING_TYPES.LOCALE]: Array<TargetingEntity>
  |}
|};

export type TargetingType = typeof TARGETING_TYPES.LOCALE;

