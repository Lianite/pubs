/* @flow */
import { checkedCredentialsSelector,
  eventIdSelector,
  messageTitleSelector,
  scheduledDateTimeSelector,
  getUserTimezone,
  imageId,
  getUnscheduledDraftDatetime,
  getCurrentPlan,
  getSelectedLabels,
  hasLabels,
  getCustomVideoTitle,
  getCurrentAssignee,
  getEventAuthorUserId,
  getMentionsSaveRequest,
  getDarkPostStatus,
  getLinksForNetworkSelector,
  videoIdForNetwork,
  videoDisplayNameForNetwork,
  videoThumbnailToDisplay,
  facebookTargetingSelectedGender } from 'reducers/index';
import { notesToSave } from 'selectors/NotesSelector';
import {
  selectedLanguages as facebookSelectedLanguages,
  includedLocations as facebookIncludedLocations
} from 'selectors/FacebookSelector';
import { createNotesPayload } from 'adapters/NotesAdapter';
import { getLinkPreviewMap } from 'selectors/LinkPreviewSelector';
import { ScheduledDatetimeRecord } from 'reducers/MessageReducer';
import { CredentialRecord } from 'records/AccountsRecord';
import { identifyUrls, buildRegexpFromArray } from 'utils/LinkUtils';
import { EditorState, convertFromRaw  } from 'draft-js';
import { getCompositeDecorator } from 'decorators/EditorDecorator';
import {
  Map as ImmutableMap,
  List as ImmutableList,
  Set as ImmutableSet,
  Record as ImmutableRecord
} from 'immutable';
import { createUnixTimestamp, createUtcMomentFromUnixTimestamp } from 'utils/DateUtils';
import { capitalizeFirstLetterActivity } from 'utils/StringUtils';
import { MentionsRecord } from 'types/MentionsTypes.js';
import {
  DefaultNetwork,
  DefaultNetworkForBackend,
  RequestStatus,
  TARGETING_TYPES,
  GENDER_TARGETING_TYPES,
  DarkPostConstants,
  SupportedNetworks,
  DatetimeConstants } from 'constants/ApplicationConstants';
import { defaultAvatarThumbnail } from 'constants/MessageConstants';
import { SocialVideoRecord } from 'records/VideosRecords';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { FacebookState } from 'records/FacebookRecords';
import {
  TargetingState,
  LanguagesState,
  LanguageState,
  LocationsState,
  LocationState,
  GenderState
} from 'records/TargetingRecords';
import {
  getFormatedUrlProperties,
  getLinkDTOsForNetworkSelector
} from 'selectors/LinksSelector';
import { AssigneeRecord } from 'records/AssigneeRecords';
import { ActivitiesRecord } from 'records/MessageRecords';
import _ from 'lodash';

import type { SupportedNetworksType } from 'constants/ApplicationConstants';
import type {
  MessageStateObject,
  MessageObject,
  SingleChannelMessage,
  MentionsMessageResponse,
  TargetingEntity,
  TargetingProfile,
  TargetingType,
  ActivityPayloadData,
  LinkDTOsType,
  LinksByUrlType } from 'adapters/types';
import type { rootState } from 'reducers/index';
import type { VideoThumbnailType } from 'reducers/VideosReducer';

type AdaptedPayload = {
  messageEditorStateMap: Object,
  checkedCredentials: Object,
  linkPreview: Object,
  urls: Object,
  urlProperties: Object,
  facebook?: Object
};

/**
 * Redux State -> Payload Adapter Functions
 */

const getPlainTextForNetwork = (state : rootState, network : string) : string => {
  var messageText = '';
  if (state.message.messageEditorStateMap.get(network)) {
    messageText = state.message.messageEditorStateMap.get(network).getCurrentContent().getPlainText();
  }

  return messageText;
};

const buildContentLabelsArray = (state: rootState): Array<Object> => {
  let retArray = [];

  if (hasLabels(state)) {
    const selectedLabels = getSelectedLabels(state).toJS();

    _.forEach(selectedLabels, (label) => {
      retArray.push({
        id: label.id,
        title: label.name,
        ...label
      });
    });
  }

  return retArray;
};

const buildTargetingPayloadFromState = (state: rootState, network: SupportedNetworksType) : ?TargetingProfile => {
  let selectedLanguages: Array<TargetingEntity> = [];
  let includedLocations: Array<TargetingEntity> = [];
  let selectedGender: GenderState;

  switch (network) {
  case SupportedNetworks.FACEBOOK:
    selectedLanguages = facebookSelectedLanguages(state);
    includedLocations = facebookIncludedLocations(state);
    selectedGender = facebookTargetingSelectedGender(state);
    break;
  default:
    return;
  }

  const targetingValues = {};
  if (selectedLanguages.length) {
    targetingValues[TARGETING_TYPES.LOCALE] = selectedLanguages;
  }
  if (includedLocations.length) {
    targetingValues[TARGETING_TYPES.GEOGRAPHY] = includedLocations;
  }

  if (selectedGender.value !== GENDER_TARGETING_TYPES.All.value) {
    targetingValues[TARGETING_TYPES.GENDER] = [selectedGender.toJS()];
  }

  if (!Object.keys(targetingValues).length) {
    return;
  }

  return {
    service: network,
    type: 'GENERAL',
    targetingValues
  };
};

const createSingleChannelMessage = (state : rootState, credList: Array<Object>, network : string) : SingleChannelMessage => {
  let singleChannelMessage : SingleChannelMessage = {
    messageContent: {
      text: getPlainTextForNetwork(state, network),
      links: getLinksForNetworkSelector(state, network),
      linkDTOs: getLinkDTOsForNetworkSelector(state).get(network, []),
      imageIDs: [],
      linkPreview: getLinkPreviewMap(state)[network] || null,
      video: null
    },
    socialNetwork: (network === DefaultNetwork ? DefaultNetworkForBackend : network),
    socialNetworkAccounts: credList,
    organicVisibility: DarkPostConstants.VISIBLE
  };

  const targetingProfile = buildTargetingPayloadFromState(state, network);
  if (targetingProfile) {
    singleChannelMessage.messageContent.targetingProfile = targetingProfile;
  }

  const darkPostStatus = getDarkPostStatus(state);
  if (darkPostStatus && singleChannelMessage.socialNetwork === SupportedNetworks.FACEBOOK) {
    singleChannelMessage.organicVisibility = DarkPostConstants.INVISIBLE;
  }

  //set the video info, if necessary
  const videoIdFromState = videoIdForNetwork(state, network);
  const videoDisplayNameFromState = videoDisplayNameForNetwork(state, network);
  if (videoIdFromState !== -1) {
    singleChannelMessage.messageContent.video = {
      id: videoIdFromState,
      displayName: videoDisplayNameFromState
    };

    const customVideoTitle = getCustomVideoTitle(state);
    if (customVideoTitle && singleChannelMessage.messageContent.video) {
      singleChannelMessage.messageContent.video.videoTitle = customVideoTitle;
    }

    const thumb = videoThumbnailToDisplay(state);
    if (singleChannelMessage.messageContent.video) {
      singleChannelMessage.messageContent.video.videoThumbnail = thumb;
    }
  }
  // NOTE: eventually we'll want to loop through this per channel when we have
  //       multi-photo support for Twitter
  const imageIdToUse = imageId(state);
  if (imageIdToUse) {
    singleChannelMessage.messageContent.imageIDs.push(imageIdToUse);
  }

  return singleChannelMessage;
};

const formatSelectedCredentials = (state: rootState): Map<string, Array<Object>> => {
  let checkedCredentialsByNetwork: Map<string, Array<Object>> = new Map();
  let checkedCredentials = checkedCredentialsSelector(state);
  checkedCredentials.forEach((cred, credId) => {
    let network = cred.socialNetwork;

    let networkCredList = checkedCredentialsByNetwork.get(network);
    const credObj = _.pick(cred, ['id', 'name', 'socialCredentialType', 'socialNetworkId', 'uniqueId']);
    if (networkCredList) {
      networkCredList.push(credObj);
    } else {
      checkedCredentialsByNetwork.set(network, [credObj]);
    }
  });

  return checkedCredentialsByNetwork;
};

export const createMessageObject = (state: rootState, messageStatus: string) => {
  const scheduledDatetime = scheduledDateTimeSelector(state);
  const unscheduledDraftDatetime = getUnscheduledDraftDatetime(state);
  const eventId = eventIdSelector(state);
  const eventAuthorUserId = getEventAuthorUserId(state);
  const notes = notesToSave(state);
  let messageObject : MessageObject  = {
    status: messageStatus,
    spredfastMessageTitle: messageTitleSelector(state),
    assignee: getCurrentAssignee(state),
    singleChannelMessages: [],
    schedule: {
      immediatePublish: !scheduledDatetime
    },
    linksByUrl: getFormatedUrlProperties(state),
    contentLabels: buildContentLabelsArray(state),
    notes: createNotesPayload(notes),
    mentions: getMentionsSaveRequest(state)
  };

  if (eventId) {
    messageObject.eventId = eventId;
  }
  if (eventAuthorUserId) {
    messageObject.eventAuthorUserId = eventAuthorUserId;
  }

  let credentialsByNetwork = formatSelectedCredentials(state);
  if (credentialsByNetwork.size === 0) {
    messageObject.singleChannelMessages.push(createSingleChannelMessage(state, [], DefaultNetwork));
  } else {
    credentialsByNetwork.forEach((credList, network) => {
      let singleChannelMessage : SingleChannelMessage = createSingleChannelMessage(state, credList, network);
      messageObject.singleChannelMessages.push(singleChannelMessage);
    });
  }

  if (scheduledDatetime) {
    const timestamp:number = createUnixTimestamp(scheduledDatetime, getUserTimezone(state).offset);
    if (messageObject.schedule) {
      messageObject.schedule.scheduledPublishDate = timestamp;
    }
  } else if (unscheduledDraftDatetime) {
    if (messageObject.schedule) {
      messageObject.schedule.scheduledPublishDate = unscheduledDraftDatetime;
    }
  }

  if (getCurrentPlan(state).id) {
    messageObject.planId = getCurrentPlan(state).id;
  }

  return messageObject;
};

/**
 * Payload -> Redux State Adapter Functions
 */
const pickLinkPreview = (payload: MessageObject) => {
  const channelWithLinkPreview = _.find(payload.singleChannelMessages, (singleChannelMessage) : SingleChannelMessage => singleChannelMessage.messageContent && singleChannelMessage.messageContent.linkPreview);
  return channelWithLinkPreview && channelWithLinkPreview.messageContent && channelWithLinkPreview.messageContent.linkPreview;
};

const regexAstralSymbols = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;

// Replace every surrogate pair with a BMP symbol and get the length.
function countSymbols(string) {
  return string.replace(regexAstralSymbols, '_').length;
}

export const makeRawContent = (text: string = '', mentions: ImmutableList<MentionsRecord>) => {
  return text.split('\n')
    .reduce((acc, text) => {
      let entityRanges = mentions
        .filter((mention) => mention.offsetBegin >= acc.offsetForBlock && mention.offsetEnd <= acc.offsetForBlock + text.length)
        .map((mention) => {
          const key = '' + acc.entityIndex++;
          acc.content.entityMap[key] = { type: 'MENTION', mutability: 'SEGMENTED', data: { mention } };
          const textInBlockBeforeMention = text.slice(0, mention.offsetBegin - acc.offsetForBlock);
          const symbols = countSymbols(textInBlockBeforeMention);
          const surrogatePairSymbols = textInBlockBeforeMention.length - symbols;
          return {
            offset: mention.offsetBegin - surrogatePairSymbols - acc.offsetForBlock,
            length: mention.displayName.length,
            key
          };
        });

      acc.content.blocks.push({ text, type: 'unstyled', entityRanges });
      acc.offsetForBlock += text.length + 1; // include the now missing \n!

      return acc;
    }, {
      content: { blocks: [], entityMap: {} },
      offsetForBlock: 0,
      entityIndex: 0
    }).content;
};

export const makeContentState = (text: string, mentions: ImmutableList<MentionsRecord>) => {
  const rawContent = makeRawContent(text, mentions);
  return convertFromRaw(rawContent);
};

const createNetworkState = (singleChannelMessage: SingleChannelMessage): ?ImmutableRecord<{targeting: TargetingState}> => {
  const network = singleChannelMessage.socialNetwork;
  const targetingValues = singleChannelMessage &&
                                          singleChannelMessage.messageContent &&
                                          singleChannelMessage.messageContent.targetingProfile &&
                                          singleChannelMessage.messageContent.targetingProfile.targetingValues ||
                                          {};
  let targetingState = new TargetingState();
  for (let targetingType: TargetingType in targetingValues) {
    switch (targetingType) {
    case TARGETING_TYPES.LOCALE:
      targetingState = targetingState
        .set('language', new LanguagesState({
          selectedLanguages: ImmutableList(targetingValues[targetingType].map((targetingLanguage) => new LanguageState(targetingLanguage)))
        }));
      break;
    case TARGETING_TYPES.GEOGRAPHY:
      targetingState = targetingState
        .set('location', new LocationsState({
          includedLocations: ImmutableList(targetingValues[targetingType].map((targetingLocation) => new LocationState(targetingLocation)))
        }));
      break;
    case TARGETING_TYPES.GENDER:
      let gender = targetingValues[targetingType][0];
      targetingState = targetingState.set('gender', new GenderState({description: gender.description, value: gender.value}));
      break;
    }
  }
  switch (network) {
  case SupportedNetworks.FACEBOOK:
    return new FacebookState({
      targeting: targetingState,
      darkPostStatus: singleChannelMessage.organicVisibility === DarkPostConstants.INVISIBLE
    });
  }
};

const getUrlPropertiesRecord = (url:string, linkDTOs:LinkDTOsType, linksByUrl:LinksByUrlType, network:string) => {
  let shortenOnPublish;
  let addLinkTags;
  if (linkDTOs) {
    const linkDTO = linkDTOs.find(link => link.fullUrl === url);
    shortenOnPublish = linkDTO && linkDTO.shortenOnPublish;
    addLinkTags = linkDTO && linkDTO.addLinkTags;
  } else {
    shortenOnPublish = linksByUrl[url] && linksByUrl[url].shortenOnPublish;
    addLinkTags = linksByUrl[url] && linksByUrl[url].addLinkTags;
  }

  return new UrlPropertiesRecord({
    associatedNetworks: ImmutableSet([network]),
    shortenOnPublish,
    addLinkTags
  });
};

const createAdaptedStateFromPayload = (payload: MessageObject, immutableMentions: ImmutableMap<string, ImmutableList<MentionsRecord>>): AdaptedPayload => {
  const adaptedPayload = {
    messageEditorStateMap: {},
    checkedCredentials: {},
    linkPreview: pickLinkPreview(payload) || {},
    videos: [],
    urls: {},
    urlProperties: {}
  };

  _.forEach(payload.singleChannelMessages, (singleChannelMessage) => {
    const singleMessageContentState = makeContentState(singleChannelMessage.messageContent.text, immutableMentions.get(singleChannelMessage.socialNetwork, ImmutableMap()));
    const linksRegex = buildRegexpFromArray(_.keys(payload.linksByUrl));
    const networkState = createNetworkState(singleChannelMessage);
    const network = singleChannelMessage.socialNetwork === DefaultNetworkForBackend ? DefaultNetwork : singleChannelMessage.socialNetwork;
    adaptedPayload.messageEditorStateMap[network] = EditorState.createWithContent(singleMessageContentState, getCompositeDecorator(linksRegex, singleMessageContentState, network));

    if (networkState) {
      adaptedPayload[network.toLowerCase()] = networkState;
    }

    //TODO: Add explicit tests for this in MessageAdapterTests
    //      create the urls
    const listOfUrls = identifyUrls(singleChannelMessage.messageContent.text);
    adaptedPayload.urls[network] = ImmutableList(listOfUrls);

    const linkDTOs = singleChannelMessage.messageContent.linkDTOs;

    //update the url properties
    _.forEach(listOfUrls, (url:string) => {
      if (!adaptedPayload.urlProperties[url]) {
        adaptedPayload.urlProperties[url] = getUrlPropertiesRecord(url, linkDTOs, payload.linksByUrl, network);
      } else {
        //Add the current network to the list
        adaptedPayload.urlProperties[url] = adaptedPayload.urlProperties[url].set('associatedNetworks', adaptedPayload.urlProperties[url].associatedNetworks.add(network));
      }
    });

    //if there was a link preview that wasn't in the list of URLs, we still need
    // it to be in the url properties
    if (adaptedPayload.linkPreview && adaptedPayload.linkPreview.linkUrl) {
      if (!adaptedPayload.urlProperties[adaptedPayload.linkPreview.linkUrl]) {
        const shouldShorten = (payload.linksByUrl[adaptedPayload.linkPreview.linkUrl] || {}).shortenOnPublish;
        adaptedPayload.urlProperties[adaptedPayload.linkPreview.linkUrl] = new UrlPropertiesRecord({
          associatedNetworks: ImmutableSet([DefaultNetwork]),
          shortenOnPublish: shouldShorten
        });
      }
    }

    //add it to the videos
    if (_.get(singleChannelMessage, 'messageContent.video')) {
      adaptedPayload.videos.push(_.get(singleChannelMessage, 'messageContent.video'));
    }

    _.forEach(singleChannelMessage.socialNetworkAccounts, (networkAccount) => {
      adaptedPayload.checkedCredentials[networkAccount.uniqueId] = networkAccount;
      adaptedPayload.checkedCredentials[networkAccount.uniqueId].socialNetwork = singleChannelMessage.socialNetwork;
    });
  });

  return adaptedPayload;
};

export const createCheckedCredentialsFromPayload = (credentials: Object): ImmutableMap<string, CredentialRecord> => {
  return ImmutableMap(_.mapValues(credentials, (value) => {
    return new CredentialRecord(value);
  }));
};

const createMentionsStateFromPayload = (payloadMentions: Array<MentionsMessageResponse>): ImmutableMap<SupportedNetworksType, MentionsRecord> => {
  let mentionsMap = ImmutableMap();
  mentionsMap = mentionsMap.withMutations(newMentionsMap => {
    _.forEach(payloadMentions, (mention) => {
      if (!newMentionsMap.get(mention.service)) {
        newMentionsMap.set(mention.service, ImmutableList());
      }
      const mentionsList = newMentionsMap.get(mention.service).push(new MentionsRecord(mention));
      newMentionsMap.set(mention.service, mentionsList);
    });
  });
  return mentionsMap;
};

export const createVideosFromPayload = (adaptedPayload: Object) => {
  var socialVideoSet = ImmutableSet();

  let customThumbId, customThumbUri, s3Key;
  let thumbnailCandidates = [];

  _.get(adaptedPayload, 'videos', []).forEach((video) => {
    socialVideoSet = socialVideoSet.add(new SocialVideoRecord({
      displayName: video.displayName,
      duration: video.duration,
      id: video.id,
      videoSize: video.size,
      transcodedVideoSize: video.transcodedSize,
      summary: video.summary,
      type: video.type,
      videoPreviewUrl: video.videoPreview,
      thumbnails: ImmutableList(video.thumbnailCandidates),
      thumbnailIndexSelected: _.findIndex(video.thumbnailCandidates, (thumbnail: VideoThumbnailType): bool => {
        return thumbnail.id === video.videoThumbnail.id;
      }),
      uploadStatus: RequestStatus.STATUS_LOADED,
      targetService: video.targetService,
      transcodeProgressStatus: RequestStatus.STATUS_LOADED,
      transcodeProgress: 100,
      transcodeResultsStatus: RequestStatus.STATUS_LOADED
    }));

    if (!s3Key && video.videoS3Key) {
      s3Key = video.videoS3Key;
    }

    if (video.thumbnailCandidates) {
      thumbnailCandidates = thumbnailCandidates.concat(video.thumbnailCandidates);
    }

    if (video.videoThumbnail && video.videoThumbnail.id) {
      customThumbId = video.videoThumbnail.id;
    }

    if (video.videoThumbnail && video.videoThumbnail.uri) {
      customThumbUri = video.videoThumbnail.uri;
    }
  });

  //We only have a true custom thumb if it's not one of the candidates.
  let hasCustomVideoThumb = !!customThumbId && !!customThumbUri;
  const existingIndex: number = thumbnailCandidates.findIndex((thumbnail: VideoThumbnailType, index: number): bool => {
    return customThumbId === thumbnail.id;
  });
  hasCustomVideoThumb = hasCustomVideoThumb && (existingIndex === -1);

  return {
    customVideoTitle: _.get(adaptedPayload, ['videos', '0', 'videoTitle'], ''),
    socialVideos: socialVideoSet,
    videoKey: s3Key || '',
    customThumbnailId: hasCustomVideoThumb ? customThumbId : '',
    customThumbnailUri: hasCustomVideoThumb ? customThumbUri : '',
    customThumbnailUploadStatus: hasCustomVideoThumb ? RequestStatus.STATUS_LOADED : RequestStatus.STATUS_NEVER_REQUESTED
  };
};

export const createActivityLogFromPayload = (activities: Array<ActivityPayloadData>): ImmutableList<ActivitiesRecord> => {
  return ImmutableList(_.map(activities, (activity: ActivityPayloadData): ActivitiesRecord => {
    const thumb = activity.author.thumb === defaultAvatarThumbnail ? '' : activity.author.thumb;
    return new ActivitiesRecord({
      authorFirstName: activity.author.firstName,
      authorLastName: activity.author.lastName,
      authorAvatarThumbnail: thumb,
      createdAtTimestamp: activity.createdDate.date / DatetimeConstants.MS_IN_SEC,
      description: capitalizeFirstLetterActivity(activity.type)
    });
  }));
};

export const createMessageStateFromPayload = (payload: MessageObject, userTimezoneOffset: number): MessageStateObject => {
  const mentions = createMentionsStateFromPayload(payload.mentions);
  const adaptedPayload = createAdaptedStateFromPayload(payload, mentions);
  let scheduleRecord;
  let activities;

  if (payload.schedule && !payload.schedule.immediatePublish) {
    scheduleRecord = new ScheduledDatetimeRecord({
      scheduledDatetime: createUtcMomentFromUnixTimestamp(payload.schedule.scheduledPublishDate, userTimezoneOffset)
    });
  } else {
    scheduleRecord = new ScheduledDatetimeRecord();
  }

  if (payload.activityLogs) {
    activities = createActivityLogFromPayload(payload.activityLogs);
  }

  const message = {
    eventId: payload.eventId,
    eventAuthorUserId: payload.eventAuthorUserId,
    status: payload.status,
    messageTitle: payload.spredfastMessageTitle,
    messageEditorStateMap: ImmutableMap(adaptedPayload.messageEditorStateMap),
    scheduledDatetime: scheduleRecord,
    mentions,
    activities
  };

  const notes = payload.notes;
  const facebook = adaptedPayload.facebook || new FacebookState();
  const images = _.map(payload.imagesById, (image) => image);
  const videos = createVideosFromPayload(adaptedPayload);
  const linkPreview = adaptedPayload.linkPreview;
  const assignee = new AssigneeRecord(payload.assignee);
  const currentPlanId = payload.planId || '';
  const contentLabels = payload.contentLabels || [];
  const urls = adaptedPayload.urls;
  const urlProperties = adaptedPayload.urlProperties;
  const checkedCredentials = createCheckedCredentialsFromPayload(adaptedPayload.checkedCredentials);

  return {
    message,
    images,
    linkPreview,
    assignee,
    currentPlanId,
    contentLabels,
    notes: {
      notesList: notes || []
    },
    videos,
    urls,
    urlProperties,
    facebook,
    checkedCredentials
  };
};
