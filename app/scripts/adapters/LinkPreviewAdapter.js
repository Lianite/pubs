/* @flow */
import { LinkPreviewMediaRecord } from 'records/LinkPreviewRecords';

export const convertToMediaRecord = (listOfUrls: Array<string>): Array<LinkPreviewMediaRecord> => {
  return listOfUrls.map( (urlString: string) => {
    let tempRec = new LinkPreviewMediaRecord({
      id: -1,
      src: urlString
    });

    return tempRec;
  });
};
