/* @flow */
import { filterUserAvatar } from 'utils/ApplicationUtils';

import type { Note, NotePayload } from 'adapters/types';

export const createNotesPayload = (notes: Array<Note>) : Array<NotePayload> => (
    notes.map(({ noteText, noteCreationTimestamp }) => ({ noteText, noteCreationTimestamp }))
);

export const createNoteFromPayload = (notePayload: Note): Note => {
  const {
    noteText,
    authorFirstName,
    authorLastName,
    authorAvatarThumbnail,
    noteCreationTimestamp
  } = notePayload;
  return {
    noteText: noteText || '',
    authorFirstName: authorFirstName || '',
    authorLastName: authorLastName || '',
    authorAvatarThumbnail: filterUserAvatar(authorAvatarThumbnail),
    noteCreationTimestamp: noteCreationTimestamp || 0
  };
};