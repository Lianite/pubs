/* @flow */
import _ from 'lodash';
import type { PlanList, Plan } from 'adapters/types';
import type { PlanData } from 'apis/PlanApi';
import {emptyPlan} from 'constants/PlanConstants';
import { fromJS } from 'immutable';

export const createPlansFromPayload = (payload: Array<PlanData>, campaignId: string): PlanList => {
  const filteredPayload = _.filter(payload, (plan): bool => {
    return plan.campaignId === _.toSafeInteger(campaignId);
  });
  return _.map(filteredPayload, (value) => {
    return {id: value.id, name: value.name, color: value.color};
  });
};

export const createCurrentPlanFromPlanId = (planId: string, plans: Array<Plan>): Plan => {
  return _.find(plans, (plan): bool => {
    return planId === plan.id;
  }) || fromJS(emptyPlan);
};
