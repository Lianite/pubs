let optionalMiddleware = [];

if (process.env.NODE_ENV !== 'production') {
  optionalMiddleware = [
    // Optional Middleware can go here
  ];
}

export default [
  ...optionalMiddleware
];
