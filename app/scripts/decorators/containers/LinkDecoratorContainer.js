/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { Dispatch } from 'redux';
import { createAction } from 'redux-actions';
import { UIStateActionConstants } from 'constants/ActionConstants';
import type { rootState } from 'reducers/index';
import LinkDecorator from 'decorators/components/LinkDecorator';

const decoratorOpenSidebar = createAction(UIStateActionConstants.OPEN_SIDEBAR);
const openSidebarIfClosed = (sidebarOpen: bool, currentActivePage: string, requestedActivePage: string): any => {
  return (dispatch: Dispatch): void  => {
    if (!sidebarOpen || (currentActivePage !== requestedActivePage)) {
      dispatch(decoratorOpenSidebar(requestedActivePage));
    }
  };
};


function mapStateToProps(state: rootState) {
  const { uiState } = state;
  return {
    sidebarOpen: uiState.sidebarOpen,
    activePage: uiState.activePage
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    onSidebarOpened: bindActionCreators(openSidebarIfClosed, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LinkDecorator);
