/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MentionQueryDecorator from 'decorators/components/MentionQueryDecorator';
import { queryForMentions, recordCurrentMentionOffset, closeMentionsQuery } from 'actions/MentionsActions';
import { getCurrentMentionOffset, messageEditorState } from 'reducers/index';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

function mapStateToProps(state: rootState, props) {
  return {
    currentMentionOffset: getCurrentMentionOffset(state),
    networkEditorState: messageEditorState(state, props.network)
  };
}

function mapDispatchToProps(dispatch: Dispatch): Object {
  return {
    queryForMentions: bindActionCreators(queryForMentions, dispatch),
    recordCurrentMentionOffset: bindActionCreators(recordCurrentMentionOffset, dispatch),
    closeMentionsQuery: bindActionCreators(closeMentionsQuery, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MentionQueryDecorator);
