/* @flow */
import { findWithRegex } from 'utils/DecoratorUtils';
import { Entity } from 'draft-js';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { TwitterConstants } from 'constants/CapabilitiesConstants';
import _ from 'lodash';

import type { SelectionState, ContentBlock } from 'draft-js';

export const getExtraChars = (network: string, contentBlockOffsetMap: any, selection: SelectionState) => {
  return (contentBlock: ContentBlock, callback: Function) => {
    if (network === SupportedNetworks.TWITTER) {
      const currBlockOffset = _.get(contentBlockOffsetMap, contentBlock.getKey(), 0);
      const currBlockEnd = currBlockOffset + contentBlock.getText().length + 1;

      let callbackStart = 0;
      let callbackEnd = contentBlock.getText().length;

      if (TwitterConstants.maxChars >= currBlockOffset && TwitterConstants.maxChars <= currBlockEnd) {
        callbackStart = TwitterConstants.maxChars - currBlockOffset;
      }

      if (currBlockEnd >= TwitterConstants.maxChars) {
        callback(callbackStart, callbackEnd);
      }
    }
  };
};

export const getFindUrlsByRegex = (regex: RegExp) => {
  return (contentBlock: ContentBlock, callback: Function) => {
    return findWithRegex(regex, contentBlock, callback);
  };
};

export const MENTION_REGEX = /\B@(\w+\s?\w+){1,2}/g;
export const getFindMentionsByRegex = (network: string, selection: SelectionState) => {
  return (contentBlock: ContentBlock, callback: Function) => {
    let regex;
    if (network === SupportedNetworks.FACEBOOK || network === SupportedNetworks.TWITTER) {
      regex = MENTION_REGEX;
    }

    if (regex && selection && selection.getFocusKey() === contentBlock.getKey()) {
      //We only want this to show up right now if you don't have text selected, which
      // means focus and anchor are the same.
      if (selection.getAnchorKey() === selection.getFocusKey() && selection.getFocusOffset() === selection.getAnchorOffset()) {
        return findWithRegex(regex, contentBlock, callback, selection.getFocusOffset());
      }
    }
  };
};

export const findMentions = (contentBlock: ContentBlock, callback: Function) => {
  const characterInMention = (character) => {
    const entityKey = character.getEntity();
    return entityKey !== null && Entity.get(entityKey).get('type') === 'MENTION';
  };

  return (contentBlock: ContentBlock, callback: Function) => {
    return contentBlock.findEntityRanges(characterInMention, callback);
  };
};
