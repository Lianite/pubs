import React from 'react';
import { CompositeDecorator } from 'draft-js';
import LinkDecoratorContainer from 'decorators/containers/LinkDecoratorContainer';
import ExtraCharsDecorator from 'decorators/components/ExtraCharsDecorator';
import MentionLinkDecorator from 'decorators/components/MentionLinkDecorator';
import MentionQueryContainer from 'decorators/containers/MentionQueryContainer';

import { allBlockOffsets } from 'utils/DraftJSUtils';

import * as Strategies from 'decorators/Strategies';

import type { SelectionState, ContentState } from 'draft-js';

/**
 * Here we're caching the rendering of the mentions suggestions box per
 * Message component (and thus per network). This is so that we don't completely
 * re-render DOM on every update to the draftjs editor.
 */
let mentionsByNetwork = {};
const getMentionQueryContainerWithNetwork = (Component: Component, network: string): ((props: {children: any}) => Component) => {
  if (!mentionsByNetwork[network]) {
    mentionsByNetwork[network] = (props: {children: any}) => <Component network={network} {...props} />; // eslint-disable-line
  }
  return mentionsByNetwork[network];
};

export const DECORATOR_TYPES = {
  ExtraCharDecorator: 'EXTRA_CHARS_DECORATOR',
  LinkDecorator: 'LINK_DECORATOR',
  MentionDecorator: 'MENTION_DECORATOR',
  MentionQueryDecorator: 'MENTION_QUERY_DECORATOR'
};

export function getCompositeDecorator(regex: RegExp, content: ContentState, network: string, selection?: SelectionState ): CompositeDecorator {
  //make a map of stuff
  const mapOfOffsets = allBlockOffsets(content);
  getCompositeDecorator.compositeDecoratorMapping = {};

  const decorators = [
    {
      strategy: Strategies.getExtraChars(network, mapOfOffsets),
      component: ExtraCharsDecorator,
      orderType: DECORATOR_TYPES.ExtraCharDecorator
    },
    {
      strategy: Strategies.getFindUrlsByRegex(regex),
      component: LinkDecoratorContainer,
      orderType: DECORATOR_TYPES.LinkDecorator
    },
    {
      strategy: Strategies.findMentions(),
      component: MentionLinkDecorator,
      orderType: DECORATOR_TYPES.MentionDecorator
    },
    {
      strategy: Strategies.getFindMentionsByRegex(network, selection),
      component: getMentionQueryContainerWithNetwork(MentionQueryContainer, network),
      orderType: DECORATOR_TYPES.MentionQueryDecorator
    }
  ];

  decorators.forEach((deco, index) => {
    getCompositeDecorator.compositeDecoratorMapping[deco.orderType] = `${index}.`;
  });

  return new CompositeDecorator(decorators);
};
