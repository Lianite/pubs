/*flow*/
import React, { Component } from 'react';
import styles from 'styles/modules/components/MentionsQuery.scss';

type Props = {
  decoratedText: string,
  queryForMentions: () => void,
  recordCurrentMentionOffset: (offsetKey: string) => void,
  children: Object,
  network: string,
  offsetKey: string,
  networkEditorState: any,
  currentMentionOffset: string
}

type DefaultProps = {
  decoratedText: '',
  queryForMentions: () => void,
  children: {},
}

const SEARCH_TEXT_MIN_LENGTH = 2;

class MentionQueryDecorator extends Component <DefaultProps, Props, State> {
  displayName = 'MentionQueryContainer';
  constructor(props: Props): void {
    super(props);
  };

  componentDidMount() {
    //We do these checks here so that when you are clicking around, if a MentionQueryDecorator
    // created without typing, the 'componentWillReceiveProps' won't be called. So we should
    // check here if this is something we should start a mentions search for.
    if (this.props.decoratedText.length >= SEARCH_TEXT_MIN_LENGTH) {
      this.props.queryForMentions(this.props.decoratedText, this.props.offsetKey, this.props.network);
      if (this.props.offsetKey !== this.props.currentMentionOffset) {
        this.props.recordCurrentMentionOffset(this.props.offsetKey);
      }
    }
  }

  componentWillReceiveProps(newProps: DecoratorProps) {
    if (newProps.decoratedText.length >= SEARCH_TEXT_MIN_LENGTH
      && newProps.decoratedText !== this.props.decoratedText) {
      newProps.queryForMentions(newProps.decoratedText, newProps.offsetKey, newProps.network);
    }

    if (this.props.decoratedText !== newProps.decoratedText && this.props.offsetKey !== this.props.currentMentionOffset) {
      this.props.recordCurrentMentionOffset(this.props.offsetKey);
    }
  };

  render(props) {
    return (
      <a id={`${this.props.offsetKey}`} className={styles.mentionsQueryDecorator}>{this.props.children}</a>
    );
  }
};

export default MentionQueryDecorator;
