/* @flow */
import React from 'react';
import styles from 'styles/modules/components/ExtraChars.scss';

type Props = {
  children: Object
}

export const ExtraCharsDecorator = (props: Props) => (
  <a className={styles.extraCharsDecorator}>{props.children}</a>
);


export default ExtraCharsDecorator;
