/*flow*/
import React from 'react';
import styles from 'styles/modules/components/MentionsQuery.scss';

type Props = {
  children: Object
}

export const MentionLinkDecorator = (props: Props) => (
  <a className={styles.mentionsLinkDecorator}>{props.children}</a>
);

export default MentionLinkDecorator;
