/*flow*/
import React from 'react';
import {SidebarPages} from 'constants/UIStateConstants';

type Props = {
  sidebarOpen: bool,
  activePage: string,
  onSidebarOpened: () => void,
  children: Object
}


export const LinkDecorator = (props: Props) => (
  <a onClick={() => props.onSidebarOpened(props.sidebarOpen, props.activePage, SidebarPages.LINKS)}>{props.children}</a>
);


export default LinkDecorator;
