import { Record } from 'immutable';

export class MentionsRecord extends Record({
  category: '',
  displayName: '',
  icon: '',
  likes: -1,
  link: '',
  location: '',
  profileId: '',
  screenName: '',
  verified: false,
  offsetBegin: -1,
  offsetEnd: -1,
  offsetKey: ''
}) {
  category: string;
  displayName: string;
  icon: string;
  likes: number;
  link: string;
  location: string;
  profileId: string;
  screenName: string;
  verified: boolean;
  offsetBegin: number;
  offsetEnd: number;
  offsetKey: string;
};

export type MentionQueryResponse = {
  data: Array<MentionsRecord>,
  nextActions: {},
  permissions: Array<any>,
  uri: string,
  verbs: Array<any>
};
