/* @flow */

export default {
  /**
    @param url: sets window.location to the specified url, useful for testing
  */
  redirect: (url :string) :void => {
    /* istanbul ignore next */
    window.location = url;
  },
  /**
    A wrapper around window.location, useful for testing
  */
  getWindowLocation: () :string =>{
    /* istanbul ignore next */
    return '' + window.location;
  }
};
