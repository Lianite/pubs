/* @flow */
const Raven = require('raven-js');

type SentryParams = {
  userId: number;
  groupId: number;
  email: string;
  firstName: string;
  lastName: string;
}
export default function setupSentry({ userId, groupId, email, firstName, lastName }: SentryParams) {
  if (window.PublishingApp.commitHash !== 'dev') {
    const SENTRY_URL = 'https://1b68877edb854830b89ef2a90ef904cd@app.getsentry.com/81889';

    Raven.config(SENTRY_URL, {
      release: window.PublishingApp.commitHash,
      tags: {
        build: window.PublishingApp.commitHash
      },
      whitelistUrls: [
        /publishing-qa.spredfast.com/,
        /publishing.spredfast.com/
      ]
    })
    .install();

    Raven.setUserContext({
      userId,
      groupId,
      email,
      firstName,
      lastName
    });

    window.addEventListener('unhandledrejection', (event) => {
      Raven.captureException(event.reason);
    });

    const originalConsoleError = window.console.error;
    window.console.error = function(message) {
      originalConsoleError.apply(console, arguments);
      Raven.captureException(message);
    };
  }
}
