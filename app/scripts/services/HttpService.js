/* @flow */
import _ from 'lodash';
import Promise from 'bluebird';
import superagent from 'superagent';
import GlobalsWrapper from 'services/GlobalsWrapper';
import { HTTPSTATUS, CONV_AUTH_URL } from 'constants/ApplicationConstants';
import { getConversationsEnviromentDetails } from 'utils/ApiUtils';

function getApiEnv(): string {
  try {
    return getConversationsEnviromentDetails().environmentUrl;
  } catch (e) {
    /* eslint-disable no-console */
    console.error('No environment details found.', e);
    /* eslint-enable no-console */
    return '';
  }
}

export const CONV_API: string = getApiEnv();

const _adjustApiEnv = (endpoint: string, prefix:?string, port:?string):string => {
  let newApiString = endpoint;
  if (prefix) {
    let prefixInsertPosition = 0;
    const httpOffset = 7;
    const httpsOffset = 8;
    const httpStart = endpoint.indexOf('http://');
    const httpsStart = endpoint.indexOf('https://');
    if (httpStart !== -1) {
      prefixInsertPosition = httpOffset;
    } else if (httpsStart !== -1) {
      prefixInsertPosition = httpsOffset;
    }
    newApiString = [newApiString.slice(0, prefixInsertPosition), prefix, newApiString.slice(prefixInsertPosition)].join('');
  }

  if (port) {
    newApiString = newApiString + ':' + port;
  }

  return newApiString;
};

const _sendReq = (request: Request): Promise => {
  return new Promise((resolve, reject, onCancel) => {
    request.end(function(err, res) {
      if (err) {
        // We are not authed, take the user to an authenticated page
        // that will bounce them to login, then redirect to us
        if (err.status === HTTPSTATUS.UNAUTHORIZED) { //add in unauthed in here
          GlobalsWrapper.redirect(`${CONV_API}${CONV_AUTH_URL}${GlobalsWrapper.getWindowLocation()}`);
        }
        // TODO: setup sentry to log errors in sending
        // _logToSentry(request, res);

        return reject(err);
      }

      return resolve(res.body);
    });

    onCancel(() => {
      request.abort();
    });
  });
};

const _setRequestHeaders = function(request: Request, type: string, customHeaders: Object = {}): Request {
  const csrfToken = window.PublishingApp && window.PublishingApp.CSRF_TOKEN;
  const headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Spredfast-CSRF-Token': csrfToken,
    ...customHeaders
  };
  if (type !== 'POST') {
    headers['X-HTTP-Method-Override'] = type;
  }

  return request
    .accept('application/json')
    .withCredentials()
    .set(headers);
};

type HandlerFunctionObject = {[key:string]:?Function};

type RequestType = {
  type: string,
  url: string,
  body?: Object,
  form?: Object,
  file?: File,
  headers?: Object,
  handlers?: HandlerFunctionObject,
  endpoint?: string,
  endpointPrefix?: string,
  port?: string
};

const _requestType = function({type, url, body, form, file, headers, handlers, endpoint = CONV_API, endpointPrefix, port}: RequestType ): Request {
  let request = null;
  let updatedEndpoint = _adjustApiEnv(endpoint, endpointPrefix, port);

  if (type === 'GET') {
    request = superagent.get(`${updatedEndpoint}${url}`);
  } else {
    //for non-GET requests we have to do some special stuff.
    //1) always use POST and set the real method in the X-HTTP-Method-Override header
    //2) set the csrf token in a header
    //always use POST with method override header
    request = superagent.post(`${updatedEndpoint}${url}`);

    if (body) {
      request.send(body);
    }

    if (form) {
      request.type('form');
      request.send(form);
    }

    if (file) {
      //Right now, the backend always expects multipart files to come in with
      // a 'file' key.
      request.attach('file', file);
    }

    //attach any provided handlers
    if (handlers) {
      _.forOwn(handlers, (handler: Function, eventName: string):void => {
        if (request && handler) {
          request.on(eventName, handler);
        }
      });
    }
  }

  return _setRequestHeaders(request, type, headers);
};

/**
@param {string} url - the url to request
*/
type getParams = {
  url: string,
  endpointPrefix?: string,
  port?: string
};
export function get(config : getParams): Promise {
  return _sendReq(_requestType({...config, type: 'GET'}));
}

/**
@param {string} url - the url to request
@param {object} body - the post body, will be sent as 'application/json'
*/
type postParams = {
  url: string,
  body: Object,
  endpoint?: string,
  endpointPrefix?: string,
  port?: string
};
export function post(config : postParams): Promise {
  return _sendReq(_requestType({...config, type: 'POST'}));
}

/**
@param {string} url - the url to request
@param {object} form - the post form, will be sent as 'application/x-www-form-urlencoded'
*/
type postFormParams = {
  url: string,
  form: Object
};
export function postForm(config : postFormParams): Promise {
  return _sendReq(_requestType({...config, type: 'POST'}));
}

/**
@param {string} url - the url to request
@param {object} form - the post form, will be sent as 'application/x-www-form-urlencoded'
@param {object} headers
*/
type postMultipartFileParams = {
  url: string,
  file: File,
  handlers?: HandlerFunctionObject,
  headers?: Object,
  endpointPrefix?: string,
  port?: string
};
export function postMultipartFile(config : postMultipartFileParams): Promise {
  config = _.omit(config, 'body');
  return _sendReq(_requestType({...config, type: 'POST'}));
}

/**
@param {string} url - the url to request
@param {object} body - the post body, will be sent as 'application/json'
*/
type putParams = {
  url: string,
  body: Object
};
export function put(config : putParams): Promise {
  return _sendReq(_requestType({...config, type: 'PUT'}));
}

/**
@param {string} url - the url to request
@param {object} body - the post body, will be sent as 'application/json'
*/
type delParams = {
  url: string,
  body: Object
};
export function del(config : delParams): Promise {
  return _sendReq(_requestType({...config, type: 'DELETE'}));
}
