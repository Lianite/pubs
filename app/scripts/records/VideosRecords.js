/* @flow */
import { RequestStatus, DefaultNetwork } from 'constants/ApplicationConstants';
import { Record, Set as ImmutableSet, List as ImmutableList } from 'immutable';

import type { VideoThumbnailType } from 'adapters/types';
import type { RequestStatusType } from 'constants/ApplicationConstants';

export class SocialVideoRecord extends Record({
  displayName: '',
  duration: 0,
  id: -1,
  videoSize: 0,
  transcodedVideoSize: 0,
  summary: '',
  type: '',
  videoPreviewUrl: '',
  thumbnails: ImmutableList(),
  thumbnailIndexSelected: -1,
  uploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  uploadPercent: 0,
  targetService: DefaultNetwork,
  transcodeProgressStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  transcodeProgress: 0,
  transcodeResultsStatus: RequestStatus.STATUS_NEVER_REQUESTED
}) {
  displayName: string;
  duration: number;
  id: number;
  videoSize: number;
  transcodedVideoSize: number;
  summary: string;
  type: string;
  videoPreviewUrl: string;
  thumbnails: ImmutableList<VideoThumbnailType>;
  thumbnailIndexSelected: number;
  uploadStatus: RequestStatusType;
  uploadPercent: number;
  targetService: string;
  transcodeProgressStatus: RequestStatusType;
  transcodeProgress: number;
  transcodeResultsStatus: RequestStatusType;
};

const defaultState = {
  customVideoTitle: '',
  socialVideos: ImmutableSet([new SocialVideoRecord()]),
  videoKey: '',
  videoUploadAndTranscodeError: '',
  customThumbnailId: '',
  customThumbnailUri: '',
  customThumbnailUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  customThumbnailUploadError: ''
};

export class VideosState extends Record(defaultState) {
  customVideoTitle: string;
  socialVideos: ImmutableSet<SocialVideoRecord>;
  videoKey: string;
  videoUploadAndTranscodeError: string;
  customThumbnailId: string;
  customThumbnailUri: string;
  customThumbnailUploadStatus: RequestStatusType;
  customThumbnailUploadError: string;
};
