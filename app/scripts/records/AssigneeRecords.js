/* @flow */
import { Record, List as ImmutableList } from 'immutable';
import { RequestStatus } from 'constants/ApplicationConstants';

import type { VoiceData } from 'apis/AccountsApi';
import type { RequestStatusType } from 'constants/ApplicationConstants';

export class AssigneeRecord extends Record({
  firstName: '',
  lastName: '',
  id: 0,
  voices: [],
  conflictingVoices: [],
  invalid: false
}) {
  firstName: string;
  lastName: string;
  id: number;
  voices: Array<Object>;
  conflictingVoices: Array<VoiceData>;
  invalid: bool;
};

export class AssigneeReducerState extends Record({
  currentAssignee: new AssigneeRecord(),
  allAssigneesError: '',
  valideAssigneesError: '',
  fetchAssigneesVoicesError: '',
  allAssigneesStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  validAssigneesStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  fetchAssigneesVoicesStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  validAssignees: ImmutableList(),
  allAssignees: ImmutableList()
}) {
  currentAssignee: AssigneeRecord;
  allAssigneesError: string;
  valideAssigneesError: string;
  allAssigneesStatus: RequestStatusType;
  validAssigneesStatus: RequestStatusType;
  allAssignees: ImmutableList<AssigneeRecord>;
  validAssignees: ImmutableList<AssigneeRecord>;
};
