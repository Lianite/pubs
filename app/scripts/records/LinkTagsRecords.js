/* @flow */
import { Record, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import _ from 'lodash';
import { RequestStatus } from 'constants/ApplicationConstants';
import type { RequestStatusType } from 'constants/ApplicationConstants';

export class TagVariableValue extends Record({
  variableId: 0,
  values: ImmutableList()
}) {
  variableId: number;
  values: ImmutableList<Object>;
};

export class TagVariableDescriptionOption extends Record({
  name: '',
  selected: false,
  value: ''
}) {
  name: string;
  selected: bool;
  value: string;
};

export class TagVariableDescription extends Record({
  fieldName: '',
  required: false,
  type: '',
  options: ImmutableList()
}) {
  fieldName: string;
  required: bool;
  type: string;
  options: ImmutableList<TagVariableDescriptionOption>
};

export class TagVariable extends Record({
  id: 0,
  name: '',
  type: '',
  variableDescription: ImmutableMap()
}) {
  id: number;
  name: string;
  type: string;
  variableDescription: ImmutableMap<string, TagVariableDescription>
};

export type tagVariablesType = ImmutableList<TagVariable>;

export type variableValuesType = ImmutableList<TagVariableValue>;

export class LinkTagDetails extends Record({
  tagVariables: ImmutableList(),
  variableValues: ImmutableList()
}) {
  tagVariables: tagVariablesType;
  variableValues: variableValuesType;
};

export class UrlLinkTagVariableValues extends Record({
  url: '',
  variableValues: ImmutableList()
}) {
  url: string;
  variableValues: variableValuesType;
};

export class LinkTagPropertiesRecord extends Record({
  domain: '',
  enabled: false,
  errorLoadingVariables: false,
  id: 0,
  loadingVariables: false,
  needToBeReloaded: false,
  reloaded: false,
  saveStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  tagVariables: ImmutableList(),
  url: '',
  variableValues: ImmutableList()
}) {
  domain: string;
  enabled: bool;
  errorLoadingVariables: bool;
  id: number;
  loadingVariables: bool;
  needToBeReloaded: bool;
  reloaded: bool;
  saveStatus: RequestStatusType
  tagVariables: tagVariablesType;
  url: string;
  variableValues: variableValuesType
};

export const defaultState = {
  urls: ImmutableMap({})
};

export type LinkTagsUrlsType = ImmutableMap<string, ImmutableMap<string, LinkTagPropertiesRecord>>;

export class LinkTagsState extends Record(_.cloneDeep(defaultState)) {
  urls: LinkTagsUrlsType;
};
