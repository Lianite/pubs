/* @flow */
import {
  Record,
  List as ImmutableList,
  Map as ImmutableMap
} from 'immutable';
import { RequestStatus } from 'constants/ApplicationConstants';
import type { UploadingStatusType } from 'constants/ApplicationConstants';

export class AccountsState extends Record({
  voices: [],
  checkedCredentials: ImmutableMap(),
  currentAccountChannelFilter: ImmutableList(),
  currentAccountSelectionFilter: '',
  currentAccountFilterText: '',
  searchOnVoices: false,
  fetchVoicesRequestState: RequestStatus.STATUS_NEVER_REQUESTED
}) {
  voices: Array<Object>; // TODO: annotate this
  checkedCredentials: ImmutableMap<string, CredentialRecord>;
  currentAccountChannelFilter: ImmutableList<string>;
  currentAccountSelectionFilter: string;
  currentAccountFilterText: string;
  searchOnVoices: bool;
  fetchVoicesRequestState: UploadingStatusType
};

export class CredentialRecord extends Record({
  id: '',
  name: '',
  socialCredentialType: '',
  socialNetwork: '',
  socialNetworkId: '',
  uniqueId: '',
  targetingSupported: false,
  authenticated: false,
  organicVisibilitySupported: false
}) {
  id: number;
  name: string;
  socialCredentialType: string;
  socialNetwork: string;
  socialNetworkId: string;
  uniqueId: string;
  targetingSupported: bool;
  authenticated: bool;
  organicVisibilitySupported: bool;
};
