/* @flow */
import { Record } from 'immutable';

export class StoredPlan extends Record({
  color: '',
  id: '',
  name: ''
}) {
  color: string;
  id: string;
  name: string;
};
