/* @flow */
import {
    List as ImmutableList,
    Record
} from 'immutable';
import { TARGETING_TYPES } from 'constants/ApplicationConstants';

import type { TargetingEntity, TargetingType, TargetingSubtype } from 'adapters/types';

const defaultTargetingEntityState = {
  value: '',
  description: '',
  type: ''
};

export class TargetingEntityState extends Record({...defaultTargetingEntityState}) {
  value: string;
  description: string;
  type: TargetingType;
};

export class GenderState extends TargetingEntityState {
  constructor({ value, description } : { value: string, description: string } = {value: '0', description: 'All'}) {
    super({
      value,
      description,
      type: 'GENDER'
    });
  }
}

export class LanguageState extends TargetingEntityState {
  constructor({ value, description } : { value: string, description: string }) {
    super({
      value,
      description,
      type: TARGETING_TYPES.LOCALE
    });
  }
}

export class LocationState extends Record({
  ...defaultTargetingEntityState,
  subType: '',
  parentTargetingValue: ''
}) {
  constructor({
    value,
    description,
    subType,
    parentTargetingValue
  } : {
    value: string,
    description: string,
    subType?: TargetingSubtype,
    parentTargetingValue?: TargetingEntity
  }) {
    super({
      value,
      description,
      subType,
      parentTargetingValue: parentTargetingValue ? new LocationState(parentTargetingValue) : undefined,
      type: TARGETING_TYPES.GEOGRAPHY
    });
  }
}

export class LanguagesState extends Record({
  languageOptions: ImmutableList(),
  selectedLanguages: ImmutableList()
}) {
  languageOptions: ImmutableList<LanguageState>
  selectedLanguages: ImmutableList<LanguageState>
};

export class LocationsState extends Record({
  locationOptions: ImmutableList(),
  includedLocations: ImmutableList()
}) {
  locationOptions: ImmutableList<LocationState>
  includedLocations: ImmutableList<LocationState>
};

export class TargetingState extends Record({
  language: new LanguagesState(),
  location: new LocationsState(),
  gender: new GenderState()
}) {
  language: LanguagesState;
  location: LocationState;
  gender: GenderState;
};
