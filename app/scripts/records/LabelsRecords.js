import { Record } from 'immutable';

export type Label = {
    id: number,
    title: string
};

export class LabelsRecord extends Record({
  id: -1,
  title: ''
}) {
  id: number
  title: string
};