/* @flow */
import { Record } from 'immutable';
import { TargetingState } from 'records/TargetingRecords';

export class FacebookState extends Record({
  targeting: new TargetingState(),
  darkPostStatus: false
}) {
  targeting: TargetingState
  darkPostStatus: boolean
};
