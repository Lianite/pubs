/* @flow */
import { Record, OrderedSet as ImmutableOrderedSet } from 'immutable';
import { RequestStatus } from 'constants/ApplicationConstants';
import _ from 'lodash';

import type { RequestStatusType } from 'constants/ApplicationConstants';
import type { PublishingError } from 'records/ErrorRecords';

export type LinkPreviewInfo = {|
  description: string,
  caption: string,
  title: string,
  thumbnailUrl: string,
  currentlyScraping: bool,
  linkUrl: string,
  videoSrc: string,
  videoLink: string,
  removed: bool
|};

export type LinkPreviewProp = {|
  linkPreviewInfo: LinkPreviewInfo,
  linkPreviewNetworks: Array<string>,
  linkPreviewLoading: bool,
  enableCarousel: bool,
  linkPreviewAlreadyUploadingMedia: bool,
  linkPreviewCustomThumbError: ?PublishingError
|};

export class LinkPreviewMediaRecord extends Record({
  id: -1,
  src: ''
}) {
  id: number
  src: string
};

const defaultState = {
  linkUrl: '',
  thumbnailUrl: '',
  description: '',
  caption: '',
  title: '',
  sourceMedia: ImmutableOrderedSet(),
  customUploadedMedia: ImmutableOrderedSet(),
  uploadingCustomThumbStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  uploadingCustomThumbError: '',
  currentlyScraping: false,
  previewLinkValidationNeeded: false,
  networks: [],
  removed: false,
  videoSrc: '',
  videoLink: ''
};

export class LinkPreviewState extends Record(_.cloneDeep(defaultState)) {
  linkUrl: string;
  thumbnailUrl: string;
  description: string;
  caption: string;
  title: string;
  sourceMedia: ImmutableOrderedSet<LinkPreviewMediaRecord>;
  customUploadedMedia: ImmutableOrderedSet<LinkPreviewMediaRecord>;
  uploadingCustomThumbStatus: RequestStatusType;
  uploadingCustomThumbError: string;
  currentlyScraping: bool;
  previewLinkValidationNeeded: bool;
  networks: Array<string>;
  removed: bool;
  videoSrc: string;
  videoLink: string;
};
