/* @flow */
import {
  Record,
  Map as ImmutableMap,
  List as ImmutableList,
  Set as ImmutableSet } from 'immutable';
import { DefaultNetwork } from 'constants/ApplicationConstants';
import _ from 'lodash';

export class UrlPropertiesRecord extends Record({
  associatedNetworks: ImmutableSet(),
  shortenOnPublish: true,
  addLinkTags: false
}) {
  associatedNetworks: ImmutableSet<string>;
  shortenOnPublish: bool;
  addLinkTags: bool;
};

export type UrlsType = ImmutableMap<string, ImmutableList<string>>;
export type UrlPropertiesType = ImmutableMap<string, UrlPropertiesRecord>;

export const defaultState = {
  urls: ImmutableMap({
    [DefaultNetwork]: ImmutableList()
  }),
  urlProperties: ImmutableMap()
};

export class LinksState extends Record(_.cloneDeep(defaultState)) {
  urls: UrlsType;
  urlProperties: UrlPropertiesType;
};
