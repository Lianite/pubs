/* @flow */
import {
  Record,
  List as ImmutableList
} from 'immutable';
import _ from 'lodash';
import type { Note } from 'adapters/types';

export const defaultNoteStateObj = {
  authorFirstName: '',
  authorLastName: '',
  authorAvatarThumbnail: '',
  noteCreationTimestamp: 0,
  noteText: ''
};

export class NoteState extends Record(defaultNoteStateObj, 'NoteState') {
  constructor(noteData: ?Note) {
    super(noteData);
  }
};

export const defaultNotesState = {
  notesList: new ImmutableList(),
  currentNoteText: '',
  savingNote: false,
  focusedOnNoteInput: false
};

export class NotesState extends Record(_.cloneDeep(defaultNotesState)) {
  notesList: ImmutableList<NoteState>;
  currentNoteText: string;
  savingNote: bool;
  focusedOnNoteInput: bool;
};
