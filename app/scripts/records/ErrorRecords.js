/* @flow */
import { Record, List as ImmutableList } from 'immutable';

export class PublishingError extends Record({
  title: '',
  description: '',
  networks: ImmutableList(),
  dismissable: true
}) {
  title: string
  description: string
  networks: ImmutableList<string>
  dismissable: bool
};
