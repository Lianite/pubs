/* @flow */
import { Record } from 'immutable';

export type ActivitiesObject = {|
  authorFirstName: string;
  authorLastName: string;
  authorAvatarThumbnail: string;
  createdAtTimestamp: number;
  description: string;
|};

const activitiesDefaults = {
  authorFirstName: '',
  authorLastName: '',
  authorAvatarThumbnail: '',
  createdAtTimestamp: 0,
  description: ''
};

export class ActivitiesRecord extends Record(activitiesDefaults, 'ActivitiesRecord') {
  constructor(activityData: ?ActivitiesObject) {
    super(activityData);
  }
};
