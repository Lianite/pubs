/* @flow */
import { Record } from 'immutable';
import { RequestStatus, MediaMimeTypeConstants } from 'constants/ApplicationConstants';

export const defaultState = {
  imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  imageUploadError: '',
  imageFilesize: 0,
  imageType: '',
  imageMimeType: '',
  imageId: null,
  imageFullUrl: '',
  imageThumbUrl: '',
  imageWidth: 0,
  imageHeight: 0
};

export class ImageReducerState extends Record(defaultState) {
  imageUploadStatus: string;
  imageUploadError: string;
  imageId: ?number;
  imageMimeType: typeof MediaMimeTypeConstants;
  imageFilesize: number;
  imageFullUrl: string;
  imageThumbUrl: string;
  imageWidth: number;
  imageHeight: number;
};
