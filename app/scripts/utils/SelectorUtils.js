import { createSelectorCreator, defaultMemoize } from 'reselect';
import _ from 'lodash';


//create a deepEqual selector that will verify that any returns
// from dependent selectors are deeply equal instead of shallow
// This is important when dependent selectors create new Objects
// every invocation, even though the resulting object may have the
// same content as the previous call

export const createDeepEqualSelector = createSelectorCreator(
  defaultMemoize,
  _.isEqual
);
