/* @flow */
import { Modifier, Entity, EditorState } from 'draft-js';
import {
  getLeafFromOffsetKey,
  decodeOffsetKey,
  blockOffset
} from 'utils/DraftJSUtils';
import type { ContentState } from 'draft-js';
import type { MentionsRecord } from 'types/MentionsTypes';

export const insertMentionIntoEditorState = (mention: MentionsRecord, editorState: EditorState, network: string) => {
  const leaf = getLeafFromOffsetKey(editorState, mention.offsetKey);

  if (leaf === null || leaf === undefined) {
    throw new Error('leaf is undefined, but mention was selected. This should not occur.');
  }

  const { blockKey } = decodeOffsetKey(mention.offsetKey);
  const offsetBegin = blockOffset(editorState, blockKey) + leaf.start;
  const displayName: string = (network === 'TWITTER'
      ? `@${mention.screenName}`
      : mention.displayName);
  const newMention: MentionsRecord = Object.assign({}, mention, {
    displayName,
    offsetBegin,
    offsetEnd: offsetBegin + displayName.length
  });

  const entityKey = Entity.create('MENTION', 'SEGMENTED', { mention });
  let newContent: ContentState = Modifier.replaceText(
    editorState.getCurrentContent(),
    editorState.getSelection().merge({
      anchorOffset: leaf.start,
      focusOffset: leaf.end
    }),
    newMention.displayName,
    null,
    entityKey
  );
  // this should probably be conditionally added,
  // only if we are at the end of the block,
  // but the current behavior is to always add it
  // so here we are. someday maybe you can enhance this
  newContent = Modifier.insertText(
    newContent,
    newContent.getSelectionAfter(),
    ' '
  );

  let newEditorState = EditorState.push(
    editorState,
    newContent,
    'insert-mention'
  );
  newEditorState = EditorState.forceSelection(
    newEditorState,
    newContent.getSelectionAfter()
  );

  return newEditorState;
};
