/* @flow */
import React from 'react';
import type { Dispatch, ActionCreator } from 'redux';
import { NOTE_SAVE_FAIL_NOTIFICATION_EXPIRE } from 'constants/NotesMenuConstants';
import {
  NOTE_SAVE_ERROR_HEADER,
  NOTE_SAVE_ERROR_BODY
} from 'constants/UITextConstants';
import { MessageNotificationsText } from 'constants/NotificationConstants';
import _ from 'lodash';

export const toTitleCase = (str: string) => {
  return str.replace(/\w\S*/g, ( word ) => {
    return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
  });
};

const warningHeaderStyle = {
  color: '#fda532',
  fontSize: '16px'
};

const warningBodyStyle = {
  marginTop: '8px'
};

export function undoApplyToAllAlert(networkName: string, dispatch: Dispatch, actionCreator: ActionCreator): Object {
  return (
    {
      sfType: 'warning',
      timed: true,
      timeOverride: 6000,
      id: networkName,
      children: <div>
        <div style={warningHeaderStyle}>Apply to All</div>
        <div style={warningBodyStyle}>The {toTitleCase(networkName)} message has been applied to all channels.</div>
        <div style={_.extend(warningBodyStyle, {'cursor': 'pointer'})}><a onClick={ () => dispatch(actionCreator(networkName))}>Undo</a></div>
      </div>
    }
  );
};

export function buildMessageCompletedNotification(message: string, type: string, succeeded: bool): Object {
  return (
    {
      sfType: succeeded ? 'positive' : 'severe',
      timed: true,
      timeOverride: 3000,
      id: type,
      children: <div>
        <div>{message}</div>
      </div>
    }
  );
}

export function messageSaveFailedNotification(): Object {
  return (
    {
      sfType: 'severe',
      timed: true,
      timeOverride: NOTE_SAVE_FAIL_NOTIFICATION_EXPIRE, //3000 ms
      children: (
        <div>
          {MessageNotificationsText.genericError}
        </div>
      )
    }
  );
};

export function noteSaveFailedNotification(): Object {
  return (
    {
      sfType: 'warning',
      timed: true,
      timeOverride: NOTE_SAVE_FAIL_NOTIFICATION_EXPIRE,
      children: (
        <div>
          <div style={warningHeaderStyle}>
            { NOTE_SAVE_ERROR_HEADER }
          </div>
          <div style={warningBodyStyle}>
            { NOTE_SAVE_ERROR_BODY }
          </div>
        </div>
      )
    }
  );
}
