/* @flow */
import type { ContentState, EditorState, ContentBlock } from 'draft-js';

import _ from 'lodash';

const takeUntil = (f, xs): Array<any> => {
  var ys = [];
  var i = 0;
  while (f(xs[i])) {
    ys.push(xs[i]);
    i += 1;
  }
  return ys;
};

const getBlockTextLengthWithLineBreak = (block: ContentBlock): number => {
  return block.getText().length + 1; //with line break
};

export const blockOffset = (editorState: EditorState, blockKey: string): number =>{
  return takeUntil(
      (block: ContentBlock) => block.get('key') !== blockKey,
      editorState.getCurrentContent().getBlocksAsArray()
  ).reduce(
    (sum: number, block: ContentBlock) => sum + getBlockTextLengthWithLineBreak(block) /* line break */,
  0);
};

export type BlockOffsetMapType = {[key: string]: number};

export const allBlockOffsets = (content: ContentState): BlockOffsetMapType => {
  let blockMap: BlockOffsetMapType = {};

  let offset = 0;
  if ( content ) {
    _.forEach(content.getBlocksAsArray(), (block: ContentBlock) => {
      blockMap[block.getKey()] = offset;
      offset += getBlockTextLengthWithLineBreak(block); /* line break*/
    });
  }

  return blockMap;
};

export const decodeOffsetKey = (offsetKey: string): {blockKey: string, decoratorKey: number, leafKey: number} => {
  const [blockKey, decoratorKey, leafKey] = offsetKey.split('-');
  return {
    blockKey,
    decoratorKey: parseInt(decoratorKey, 10),
    leafKey: parseInt(leafKey, 10)
  };
};


export const getLeafFromOffsetKey = (editorState: EditorState, offsetKey: string): ?{start: number, end: number} => {
  const { blockKey, decoratorKey, leafKey } = decodeOffsetKey(offsetKey);

  const blockTree = editorState.getBlockTree(blockKey);

  return blockTree
    ? blockTree.getIn([decoratorKey, 'leaves', leafKey])
    : null;
};
