/* @flow */
import TwitterText from 'twitter-text';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';
import _ from 'lodash';

export const identifyUrls = (text: string) => {
  return TwitterText.extractUrls(text);
};

export type TwitterTextUrlIndexPairType = {
  url: string,
  indices: Array<number>
};

export const identifyUrlsWithIndeces = (text:string):TwitterTextUrlIndexPairType => {
  return TwitterText.extractUrlsWithIndices(text);
};

export const buildRegexpFromArray = (arr: Array<string>): ?RegExp => {

  //first, sort the array so the longest urls are first. Otherwise, if you have
  // an url that is a superset of a previous url, the regex won't select correctly.
  // (i.e. google.com, google.com/docs)

  const sortedArr = _.orderBy(arr, [(url: string):number => {
    return url.length;
  }], ['desc']);

  const regexSearchList = sortedArr.map((url) => {
    return _.escapeRegExp(url);
  });

  // This is designed to select nothing. Basically, its a way to clear out any selections we have
  //  so when the user removes a character that invalidates a link, it is removed correctly. Additionally,
  //  it is performant while still keeping the logic simple.
  let searchString = regexSearchList.length ? '(' + regexSearchList.join('|') + ')' : '';

  return searchString.length > 0 ? new RegExp(searchString, 'g') : null;
};

export const extractUrlFromSourceMedia = (sourceMedia: any) => {
  return (sourceMedia || []).map((elem) => {
    return elem.src;
  });
};

export const getTotalUrlList = (urlMap: ImmutableMap<string, ImmutableList<string>>): Array<string> => {
  let totalUrlList = [];
  urlMap.forEach( (urlList) => {
    totalUrlList = totalUrlList.concat(urlList.toArray());
  });

  return totalUrlList;
};

export const buildUrlHref = (url:string) => {
  return 'http://www.' + url.replace(/^(http|https):\/\//, '')
                            .replace(/^www\./, '');
};

export const isPositionInsideAnUrl = (text: string, position: number): bool => {
  const foundUrls = identifyUrlsWithIndeces(text);

  return _.some(foundUrls, (urlParsingEntry:TwitterTextUrlIndexPairType):bool => {
    return position >= urlParsingEntry.indices[0] && position <= urlParsingEntry.indices[1];
  });
};
