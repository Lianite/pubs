/* @flow */
import type { ContentBlock } from 'draft-js';

//If you pass 'limit' in, it will only call 'callback' on a range that satisfies the regex *and*
// contains the 'limit' index. It will also only call 'callback' from the start of the regex to
// the limit index instead of the end of the regex.
export const findWithRegex = (regex: RegExp, contentBlock: ContentBlock, callback: Function, limit?: ?number) => {
  if (regex === null) {
    return;
  }

  const text = contentBlock.getText();
  let matchArr, start;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    if (limit && limit >= start && limit <= (start + matchArr[0].length) ) {
      callback(start, limit);
    } else if (!limit) {
      callback(start, start + matchArr[0].length);
    }
  }
};
