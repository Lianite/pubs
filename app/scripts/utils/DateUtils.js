import moment from 'moment';

import type Moment from 'moment';

/**
 * Acceptable datetime formats for manual user entry. Moment will try to
 * create dates with these formats in order.
 * @type {Array}
 */
const acceptablDateTimeStringFormats = [
  'M/D/YYYY h:mma',
  'M/D/YYYY h:mm',
  'M/D/YYYY ha',
  'M/D/YYYY h',
  'M/D/YYYY',
  'M/D h:mma',
  'M/D h:mm',
  'M/D ha',
  'M/D h',
  'M/D',

  'M/D/YYYY, h:mma',
  'M/D/YYYY, h:mm',
  'M/D/YYYY, ha',
  'M/D/YYYY, h',
  'M/D, h:mma',
  'M/D, h:mm',
  'M/D, ha',
  'M/D, h',

  'YYYY/M/D h:mma',
  'YYYY/M/D h:mm',
  'YYYY/M/D ha',
  'YYYY/M/D h',
  'YYYY/M/D',
  'YYYY/M h:mma',
  'YYYY/M h:mm',
  'YYYY/M ha',
  'YYYY/M h',
  'YYYY/M',

  'YYYY/M/D, h:mma',
  'YYYY/M/D, h:mm',
  'YYYY/M/D, ha',
  'YYYY/M/D, h',
  'YYYY/M, h:mma',
  'YYYY/M, h:mm',
  'YYYY/M, ha',
  'YYYY/M, h',

  'YYYY-M-D h:mma',
  'YYYY-M-D h:mm',
  'YYYY-M-D ha',
  'YYYY-M-D h',
  'YYYY-M-D',
  'YYYY-M h:mma',
  'YYYY-M h:mm',
  'YYYY-M ha',
  'YYYY-M h',
  'YYYY-M',

  'YYYY-M-D, h:mma',
  'YYYY-M-D, h:mm',
  'YYYY-M-D, ha',
  'YYYY-M-D, h',
  'YYYY-M, h:mma',
  'YYYY-M, h:mm',
  'YYYY-M, ha',
  'YYYY-M, h',

  'M-D-YYYY h:mma',
  'M-D-YYYY h:mm',
  'M-D-YYYY ha',
  'M-D-YYYY h',
  'M-D-YYYY',
  'M-D h:mma',
  'M-D h:mm',
  'M-D ha',
  'M-D h',
  'M-D',

  'M-D-YYYY, h:mma',
  'M-D-YYYY, h:mm',
  'M-D-YYYY, ha',
  'M-D-YYYY, h',
  'M-D, h:mma',
  'M-D, h:mm',
  'M-D, ha',
  'M-D, h',

  'M D YYYY h:mma',
  'M D YYYY h:mm',
  'M D YYYY ha',
  'M D YYYY h',
  'M D YYYY',
  'M D h:mma',
  'M D h:mm',
  'M D ha',
  'M D h',
  'M D',

  'M D YYYY, h:mma',
  'M D YYYY, h:mm',
  'M D YYYY, ha',
  'M D YYYY, h',
  'M D, h:mma',
  'M D, h:mm',
  'M D, ha',
  'M D, h',

  'YYYY M D h:mma',
  'YYYY M D h:mm',
  'YYYY M D ha',
  'YYYY M D h',
  'YYYY M D',
  'YYYY M h:mma',
  'YYYY M h:mm',
  'YYYY M ha',
  'YYYY M h',
  'YYYY M',

  'YYYY M D, h:mma',
  'YYYY M D, h:mm',
  'YYYY M D, ha',
  'YYYY M D, h',
  'YYYY M, h:mma',
  'YYYY M, h:mm',
  'YYYY M, ha',
  'YYYY M, h',

  'MMM D YYYY h:mma',
  'MMM D YYYY h:mm',
  'MMM D YYYY ha',
  'MMM D YYYY h',
  'MMM D YYYY',
  'MMM D h:mma',
  'MMM D h:mm',
  'MMM D ha',
  'MMM D h',
  'MMM D',

  'MMM D YYYY, h:mma',
  'MMM D YYYY, h:mm',
  'MMM D YYYY, ha',
  'MMM D YYYY, h',
  'MMM D, h:mma',
  'MMM D, h:mm',
  'MMM D, ha',
  'MMM D, h',

  'MMM D, YYYY h:mma',
  'MMM D, YYYY h:mm',
  'MMM D, YYYY ha',
  'MMM D, YYYY h',
  'MMM D, YYYY',

  'MMM D, YYYY, h:mma',
  'MMM D, YYYY, h:mm',
  'MMM D, YYYY, ha',
  'MMM D, YYYY, h',

  'MMM Do YYYY h:mma',
  'MMM Do YYYY h:mm',
  'MMM Do YYYY ha',
  'MMM Do YYYY h',
  'MMM Do YYYY',
  'MMM Do h:mma',
  'MMM Do h:mm',
  'MMM Do ha',
  'MMM Do h',
  'MMM Do',

  'MMM Do YYYY, h:mma',
  'MMM Do YYYY, h:mm',
  'MMM Do YYYY, ha',
  'MMM Do YYYY, h',
  'MMM Do YYYY',
  'MMM Do, h:mma',
  'MMM Do, h:mm',
  'MMM Do, ha',
  'MMM Do, h',

  'MMM Do, YYYY h:mma',
  'MMM Do, YYYY h:mm',
  'MMM Do, YYYY ha',
  'MMM Do, YYYY h',
  'MMM Do, YYYY',

  'MMM Do, YYYY, h:mma',
  'MMM Do, YYYY, h:mm',
  'MMM Do, YYYY, ha',
  'MMM Do, YYYY, h',

  'MMMM D YYYY h:mma',
  'MMMM D YYYY h:mm',
  'MMMM D YYYY ha',
  'MMMM D YYYY h',
  'MMMM D YYYY',
  'MMMM D h:mma',
  'MMMM D h:mm',
  'MMMM D ha',
  'MMMM D h',
  'MMMM D',

  'MMMM D YYYY, h:mma',
  'MMMM D YYYY, h:mm',
  'MMMM D YYYY, ha',
  'MMMM D YYYY, h',
  'MMMM D, h:mma',
  'MMMM D, h:mm',
  'MMMM D, ha',
  'MMMM D, h',

  'MMMM D, YYYY h:mma',
  'MMMM D, YYYY h:mm',
  'MMMM D, YYYY ha',
  'MMMM D, YYYY h',
  'MMMM D, YYYY',

  'MMMM D, YYYY, h:mma',
  'MMMM D, YYYY, h:mm',
  'MMMM D, YYYY, ha',
  'MMMM D, YYYY, h',

  'MMMM Do YYYY h:mma',
  'MMMM Do YYYY h:mm',
  'MMMM Do YYYY ha',
  'MMMM Do YYYY h',
  'MMMM Do YYYY',
  'MMMM Do h:mma',
  'MMMM Do h:mm',
  'MMMM Do ha',
  'MMMM Do h',
  'MMMM Do',

  'MMMM Do YYYY, h:mma',
  'MMMM Do YYYY, h:mm',
  'MMMM Do YYYY, ha',
  'MMMM Do YYYY, h',
  'MMMM Do, h:mma',
  'MMMM Do, h:mm',
  'MMMM Do, ha',
  'MMMM Do, h',

  'MMMM Do, YYYY h:mma',
  'MMMM Do, YYYY h:mm',
  'MMMM Do, YYYY ha',
  'MMMM Do, YYYY h',
  'MMMM Do, YYYY',

  'MMMM Do, YYYY, h:mma',
  'MMMM Do, YYYY, h:mm',
  'MMMM Do, YYYY, ha',
  'MMMM Do, YYYY, h'
];

const acceptablTimeStringFormats = [
  'h:mma',
  'h:mm',
  'h'
];

export const timeOptions = [
  '12:00am',
  '12:30am',
  '1:00am',
  '1:30am',
  '2:00am',
  '2:30am',
  '3:00am',
  '3:30am',
  '4:00am',
  '4:30am',
  '5:00am',
  '5:30am',
  '6:00am',
  '6:30am',
  '7:00am',
  '7:30am',
  '8:00am',
  '8:30am',
  '9:30am',
  '10:00am',
  '10:30am',
  '11:00am',
  '11:30am',
  '12:00pm',
  '12:30pm',
  '1:00pm',
  '1:30pm',
  '2:00pm',
  '2:30pm',
  '3:00pm',
  '3:30pm',
  '4:00pm',
  '4:30pm',
  '5:00pm',
  '5:30pm',
  '6:00pm',
  '6:30pm',
  '7:00pm',
  '7:30pm',
  '8:00pm',
  '8:30pm',
  '9:30pm',
  '10:00pm',
  '10:30pm',
  '11:00pm',
  '11:30pm'
];

/**
 * The following functions create moment objects
 */
const getBrowserToAppOffset = (userOffset: number): number => {
  const calculatedUserOffset = userOffset / 100 * 60;
  const utcOffset = moment().utcOffset();
  return utcOffset - calculatedUserOffset;
};

export const getCurrentDateTimeUTC = (userOffset: number, currentMomentDatetime?: Moment): Moment => {
  const browserToAppOffset = getBrowserToAppOffset(userOffset);
  let defaultMoment = currentMomentDatetime || moment();
  defaultMoment = defaultMoment.seconds(0).milliseconds(0);
  return moment(defaultMoment).subtract(browserToAppOffset, 'minutes').utc();
};

export const getCurrentDateUTC = (userOffset: number, currentMomentDatetime?: Moment): Moment => {
  const browserToAppOffset = getBrowserToAppOffset(userOffset);
  let defaultMoment = currentMomentDatetime || moment();
  defaultMoment = defaultMoment.hours(0).minutes(0).seconds(0).milliseconds(0);
  return moment(defaultMoment).subtract(browserToAppOffset, 'minutes').utc();
};

export const getCurrentMonthDateUtc = (userOffset: number, currentMomentDatetime?: Moment): Moment => {
  const browserToAppOffset = getBrowserToAppOffset(userOffset);
  let defaultMoment = currentMomentDatetime || moment();
  defaultMoment = defaultMoment.date(1).hours(0).minutes(0).seconds(0).milliseconds(0);
  return moment(defaultMoment).subtract(browserToAppOffset, 'minutes').utc();
};

const dateIsAfterCurrent = (utcMoment: Moment, userOffset: number, currentMomentDatetime?: Moment): bool => {
  return utcMoment.isAfter(getCurrentDateTimeUTC(userOffset, currentMomentDatetime), 'day');
};

export const dateIsCurrent = (utcMoment: Moment, userOffset: number, currentMomentDatetime?: Moment): bool => {
  return utcMoment.isSameOrAfter(getCurrentDateUTC(userOffset, currentMomentDatetime), 'day');
};

export const datetimeIsCurrent = (utcMoment: Moment, userOffset: number, currentMomentDatetime?: Moment): bool => {
  return utcMoment.isSameOrAfter(getCurrentDateTimeUTC(userOffset, currentMomentDatetime), 'minute');
};

export const getDefaultScheduleTime = (userOffset: number, currentMomentDatetime?: Moment): Moment => {
  return getCurrentDateTimeUTC(userOffset, currentMomentDatetime).add(5, 'minutes');
};

export const createUtcMomentFromString = (dateString: string): ?Moment => {
  if (dateString) {
    return moment(dateString.trim(), acceptablDateTimeStringFormats, true).utc();
  } else {
    return null;
  }
};

export const createUtcMomentFromDate = (date: Date): ?Moment => {
  if (date) {
    return moment(date).utc();
  } else {
    return null;
  }
};

export const adjustUtcMomentTime = (utcMoment: ?Moment, timeString: string, offset: number, currentMomentDatetime?: Moment): Moment => {
  if (!utcMoment || !utcMoment.isValid()) {
    utcMoment = getDefaultScheduleTime(offset, currentMomentDatetime);
  }

  const timeMoment = moment(timeString.trim(), acceptablTimeStringFormats, true);
  if (timeMoment.isValid()) {
    let minutesToSet = timeMoment.minutes();
    let hourToSet = timeMoment.hours();

    let adjustedUtcMomentTime = moment(utcMoment).local();

    adjustedUtcMomentTime.hour(hourToSet);
    adjustedUtcMomentTime.minutes(minutesToSet);

    return adjustedUtcMomentTime.utc();
  } else {
    return moment.invalid();
  }
};

export const adjustUtcMomentDate = (utcMoment: ?Moment, newDate: Moment, offset: number, currentMomentDate?: Moment): Moment => {
  if (!utcMoment || !utcMoment.isValid()) {
    if (dateIsAfterCurrent(newDate, offset, currentMomentDate)) {
      return newDate.clone();
    } else {
      return getDefaultScheduleTime(offset, currentMomentDate);
    }
  }

  if (newDate.isValid()) {
    let adjustedUtcMomentDate = moment(utcMoment).local();
    adjustedUtcMomentDate.date(newDate.date());
    adjustedUtcMomentDate.month(newDate.month());
    adjustedUtcMomentDate.year(newDate.year());

    if (!datetimeIsCurrent(adjustedUtcMomentDate, offset, currentMomentDate)) {
      return getDefaultScheduleTime(offset, currentMomentDate);
    }

    return adjustedUtcMomentDate.utc();
  }
};

export const formatDatetimeForTextInput = (utcMoment: ?Moment, offset?: string): string => {
  if (utcMoment) {
    const localMoment = utcMoment.clone().local();
    return localMoment.format('MM/DD/YYYY, h:mma');
  } else {
    return '';
  }
};

export const formatDatetimeForNoteDisplay = (utcMoment: ?Moment, offset?: string) : string => {
  if (utcMoment) {
    const localMoment = utcMoment.clone().local();
    return localMoment.format('MMM D, h:mma');
  } else {
    return '';
  }
};

export const formatTimeForTextInput = (utcMoment: Moment, offset?: string): string  => {
  if (utcMoment) {
    const localMoment = utcMoment.clone().local();
    return localMoment.format('h:mma');
  } else {
    return '';
  }
};

export const createUtcMomentFromUnixTimestamp = (unixTimestamp: number, userOffset: number): Moment => {
  const browserToAppOffset = getBrowserToAppOffset(userOffset);
  return moment.unix(unixTimestamp).subtract(browserToAppOffset, 'minutes').utc();
};

export const createUnixTimestamp = (utcMoment: Moment, userOffset: number): number => {
  const browserToAppOffset = getBrowserToAppOffset(userOffset);
  let newUtcMoment = utcMoment.clone();
  return newUtcMoment.add(browserToAppOffset, 'minutes').unix();
};
