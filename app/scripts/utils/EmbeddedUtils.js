/* @flow */
import { Embed } from '@spredfast/sf-intents';

import type { MessageStatusType } from 'constants/ApplicationConstants';
/**
 * Checks to see if app is embedded in an intent, if so, closes it.
 * @return {bool - true if closes the embedded, false if not}
 */
export const closeEmbedded = (): bool => {
  if (Embed.isEmbedded()) {
    Embed.trigger('close');
    Embed.close();
    return true;
  }
  return false;
};

/**
 * Checks to see if app is embedded in an intent, if so it will trigger an notificaiton
 * event on the surrounding app.
 * @type {
 *       trigger: string - the type of event to trigger,
 *       message: string - the message body displayed in notificaiton
 *       adaptedType: MessageStatusType - the type of action in MCP that triggered the notification
 * }
 * @return {bool - true if the embedded event was trigger, otherwise false}
 */
export const intentNotification = (trigger: string, message: string, adaptedType: MessageStatusType): bool => {
  if (Embed.isEmbedded()) {
    Embed.trigger(trigger, {
      message: message,
      actionType: adaptedType
    });
    return true;
  }
  return false;
};
