/* @flow */
import { EnvironmentConstants } from 'constants/ApplicationConstants';
import _ from 'lodash';

export const getCopyApiString = (shouldCopy: bool): string => {
  return shouldCopy ? '/copy' : '';
};

type ConversationsEnviromentDetailsType = {
  environmentUrl: string,
  productEnvironment: string,
  productName: string,
  externalCompanyId?: string,
  productEnvironmentId?: number
};

export const getConversationsEnviromentDetails = () : ConversationsEnviromentDetailsType => {
  const envDetails = window.PublishingApp && window.PublishingApp.envDetails;
  return envDetails.find(env => env.productName === EnvironmentConstants.CONVERSATIONS);
};

const isProdEnvironment = () => {
  return window.PublishingApp.lh === EnvironmentConstants.PROD_LOGIN_HOSTNAME;
};

const isDevEnvironment = () => {
  const conv = getConversationsEnviromentDetails();
  const dev = `//${EnvironmentConstants.LOCAL_DEVELOPMENT_CONV_HOSTNAME}`;
  return conv.environmentUrl.indexOf(dev) >= 0;
};

const isTestEnvironment = () => {
  const conv = getConversationsEnviromentDetails();
  const dev = `//${EnvironmentConstants.TEST_CONV_HOSTNAME}`;
  return conv.environmentUrl.indexOf(dev) >= 0;
};

const isStagingEnvironment = () => {
  return !isProdEnvironment() && !isDevEnvironment() && !isTestEnvironment();
};

export const getCurrentEnvironment = () => {
  const environments = [{
    name: EnvironmentConstants.PROD,
    status: isProdEnvironment()
  }, {
    name: EnvironmentConstants.DEV,
    status: isDevEnvironment()
  }, {
    name: EnvironmentConstants.TEST,
    status: isTestEnvironment()
  }, {
    name: EnvironmentConstants.STAGING,
    status: isStagingEnvironment()
  }];
  const currentEnvironment = _.find(environments, {status: true});
  return currentEnvironment.name;
};
