export const setStateFromObject = (state, obj, exemptions = []) => {
  return state.withMutations((ctx) => {
    Object.keys(obj).forEach((key) => {
      if (exemptions.indexOf(key) === -1) {
        ctx.set(key, obj[key]);
      }
    });
  });
};