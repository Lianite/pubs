/* @flow */
import { DEFAULT_USER_AVATAR_URI } from 'constants/ApplicationConstants';
import { UploadingStatus, RequestStatus } from 'constants/ApplicationConstants';

import type { UploadingStatusType } from 'constants/ApplicationConstants';

export const getUploadStatus = (requestStatus:string) => {
  let convertedMediaStatus: UploadingStatusType;
  switch (requestStatus) {
  case RequestStatus.STATUS_NEVER_REQUESTED:
  case RequestStatus.STATUS_CANCELED:
    convertedMediaStatus = UploadingStatus.WAITING;
    break;
  case RequestStatus.STATUS_REQUESTED:
    convertedMediaStatus = UploadingStatus.UPLOADING;
    break;
  case RequestStatus.STATUS_LOADED:
    convertedMediaStatus = UploadingStatus.SUCCESS;
    break;
  case RequestStatus.STATUS_ERROR:
    convertedMediaStatus = UploadingStatus.ERROR;
    break;
  }

  return convertedMediaStatus;
};

export const filterUserAvatar = (rawAvatarUri: string = '') =>  rawAvatarUri !== DEFAULT_USER_AVATAR_URI ? rawAvatarUri : '';
