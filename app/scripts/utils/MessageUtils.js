/* @flow */
import { EditorState } from 'draft-js';

export const getPlainTextFromEditorState = (editorState: EditorState): string => {
  return editorState ? editorState.getCurrentContent().getPlainText() : '';
};
