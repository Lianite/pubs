/* @flow */

export const capitalizeFirstLetter = (string: string): string => {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};

export const capitalizeFirstLetterActivity = (string: string): string => {
  const capitalCase = capitalizeFirstLetter(string);
  switch (capitalCase) {
  case 'Create':
  case 'Approve':
    return capitalCase + 'd';
  case 'Submit':
  case 'Resubmit':
    return capitalCase + 'ted';
  default:
    return capitalCase + 'ed';
  }
};
