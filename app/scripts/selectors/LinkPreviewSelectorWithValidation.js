/* @flow */
import { createStructuredSelector } from 'reselect';
import { linkPreviewCustomThumbError } from 'selectors/ValidationSelector';
import { linkPreviewInfo,
        linkPreviewNetworks,
        linkPreviewLoading,
        linkPreviewHasMultipleThumbnails } from 'selectors/LinkPreviewSelector';
import { linkPreviewAlreadyUploadingMedia } from 'reducers/index';

export const linkPreview = createStructuredSelector({
  linkPreviewInfo,
  linkPreviewNetworks,
  linkPreviewLoading,
  enableCarousel: linkPreviewHasMultipleThumbnails,
  linkPreviewAlreadyUploadingMedia,
  linkPreviewCustomThumbError
});
