/* @flow */
import { createDeepEqualSelector } from 'utils/SelectorUtils';
import {
    notesList,
    eventIdSelector
} from 'reducers/index';

import type { List as ImmutableList } from 'immutable';
import type { Note } from 'adapters/types';
import type { NoteState } from 'records/NotesRecords';

const sortNotes = (noteA: Note, noteB: Note): number => {
  return noteB.noteCreationTimestamp - noteA.noteCreationTimestamp;
};

export const notesToSave: () => Array<Note> = createDeepEqualSelector(
  [eventIdSelector, notesList],
  (eventId?: number, notesList: ImmutableList<NoteState>): Array<Note> => eventId !== null ? [] : notesList.toJS()
);

export const notesForDisplay: () => Array<Note> = createDeepEqualSelector(
  notesList,
  (notes: ImmutableList<NoteState>): Array<Note> => notes.toJS().sort(sortNotes)
);
