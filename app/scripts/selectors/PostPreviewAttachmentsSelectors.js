/* @flow */
import { createSelector } from 'reselect';
import { getLinkPreviewMap } from 'selectors/LinkPreviewSelector';
import {
  attachedImage,
  attachedVideo
} from 'selectors/AttachedMediaSelector';

import type {
  VideoType,
  ImageType
} from 'selectors/AttachedMediaSelector';

import type { LinkPreviewMapType } from 'selectors/LinkPreviewSelector';

export type PostPreviewAttachment = {|
  type: 'link' | 'image' | 'video' | 'text',
  attachmentTitle?: string,
  attachmentLink?: string,
  attachmentCaption?: string,
  attachmentLinkImage?: string,
  attachmentDescription?: string,
  attachmentVideoThumbnail?: string,
  attachmentVideoUri?: string,
  attachmentImages?: Array<{uri: string, width: number, height: number}>
|};

export const attachments = createSelector(
    getLinkPreviewMap,
    attachedVideo,
    attachedImage,
    (state, props) => props.network,
    (linkPreviewMap: LinkPreviewMapType, attachedVideo: VideoType, attachedImage: ImageType, network: string): PostPreviewAttachment => {
      const linkPreview = linkPreviewMap[network];
      if (linkPreview) {
        return {
          type: 'link',
          attachmentLink: linkPreview.linkUrl,
          attachmentTitle: linkPreview.title,
          attachmentCaption: linkPreview.caption,
          attachmentLinkImage: linkPreview.thumbnailUrl,
          attachmentDescription: linkPreview.description
        };
      }
      if (attachedVideo.previewUrl) {
        return {
          type: 'video',
          attachmentTitle: attachedVideo.title,
          attachmentVideoThumbnail: attachedVideo.customThumb || attachedVideo.thumbnail,
          attachmentVideoUri: attachedVideo.previewUrl
        };
      }
      if (attachedImage.currentImageFull) {
        return {
          type: 'image',
          attachmentImages: [{
            uri: attachedImage.currentImageFull,
            width: attachedImage.imageWidth || 0,
            height: attachedImage.imageHeight || 0
          }]
        };
      }
      return {
        type: 'text'
      };
    }
);

export const type = createSelector(
  attachments,
  (attachment): string => attachment.type
);

export const attachmentTitle = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentTitle
);

export const attachmentCaption = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentCaption
);

export const attachmentLinkImage = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentLinkImage
);

export const attachmentDescription = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentDescription
);

export const attachmentLink = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentLink
);

export const attachmentVideoThumbnail = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentVideoThumbnail
);

export const attachmentVideoUri = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentVideoUri
);

export const attachmentImages = createSelector(
  attachments,
  (attachment): ?string => attachment.attachmentImages
);
