import { createSelector } from 'reselect';
import { Embed } from '@spredfast/sf-intents';

export const messageHasEventId = state => !!state.router.params.eventId;
export const messageHasCopyId = state => !!state.router.params.copyId;
export const messageHasId = state => (messageHasEventId(state) || messageHasCopyId(state));
export const currentPathname = state => state.router.location.pathname;
export const currentQueryParams = state => state.router.location.search;
export const getPlanIdFromRoute = state => state.router.location.query.plan;
export const getCompanyId = state => state.router.params.companyId;
export const getCampaignId = state => state.router.params.campaignId;
export const getEventId = state => (state.router.params.eventId || -1);
export const getCopyId = state => (state.router.params.copyId || -1);

export const getCancelButtonVerbage = createSelector(
  [messageHasEventId],
  (hasEventId) => {
    let verbage = 'Clear';

    if (Embed.isEmbedded() || hasEventId) {
      verbage = 'Cancel';
    }

    return verbage;
  }
);

export type RouteInfo = {|
  companyId: ?number,
  campaignId: ?number,
  eventId: ?number
|}

export const getCompanyCampaignFromRoute = (state: rootState) : RouteInfo => {
  const eventId = getEventId(state);
  return {
    companyId: (((state || {}).router || {}).params || {}).companyId,
    campaignId: (((state || {}).router || {}).params || {}).campaignId,
    eventId: eventId !== -1 ? eventId : undefined
  };
};
