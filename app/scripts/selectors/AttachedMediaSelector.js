/* @flow */
import _ from 'lodash';
import { createSelector } from 'reselect';
import {
  imageUploadStatus,
  firstVideo,
  videoUploadPercent,
  hasImageBeenRequested,
  hasVideoBeenRequested,
  currentImageThumbnail,
  currentImageFull,
  capabilitiesMap,
  selectedNetworks,
  imageWidth,
  imageHeight,
  customVideoThumbnail,
  customVideoThumbnailUploadingStatus,
  getCustomVideoTitle,
  videoThumbnailToDisplay,
  videoTranscodePercent} from 'reducers/index';

import { linkPreviewInfo } from 'selectors/LinkPreviewSelector';
import { UploadingStatus } from 'constants/ApplicationConstants';
import { imageFilesizeError, videoError } from 'selectors/ValidationSelector';

import { getUploadStatus } from 'utils/ApplicationUtils';
import { RequestStatus } from 'constants/ApplicationConstants';

import type { LinkPreviewInfo } from 'records/LinkPreviewRecords';
import type { PublishingError } from 'records/ErrorRecords';

const videoUploadStatus = createSelector(firstVideo, video => getUploadStatus(video.get('uploadStatus')));

function isVideoCustomThumbSupported(network: string, capabilitiesMap): bool {
  return !!capabilitiesMap.getIn([network, 'NEW_MESSAGE', 'VIDEO', 'videoCapabilityDTO', 'customThumbnailSupported']);
}

function isVideoUploadSupported(network, capabilitiesMap) {
  return !!capabilitiesMap.getIn([network, 'NEW_MESSAGE', 'VIDEO', 'videoCapabilityDTO']);
}

const supportedNetworks = (capabilitiesMap, selectedNetworks) => {
  return selectedNetworks.filter(network => isVideoUploadSupported(network, capabilitiesMap));
};

const isUploading = uploadStatus => {
  return uploadStatus === UploadingStatus.UPLOADING;
};

const uploadIsWaiting = uploadStatus => {
  return uploadStatus === UploadingStatus.WAITING;
};

const uploadIsComplete = uploadStatus => {
  return _.includes([UploadingStatus.SUCCESS, UploadingStatus.ERROR], uploadStatus);
};

const uploadHasFail = uploadStatus => {
  return uploadStatus === UploadingStatus.ERROR;
};

export const videoUploadNetworks = createSelector(
  capabilitiesMap,
  selectedNetworks,
  (capabilitiesMap, selectedNetworks) => {
    return supportedNetworks(capabilitiesMap, selectedNetworks);
  }
);

export type ImageType = {
  uploading?: boolean,
  waiting?: boolean,
  currentImageThumbnail?: string,
  currentImageFull?: string,
  imageWidth?: number,
  imageHeight?: number,
  imageFilesizeError?: ?PublishingError
};

export const attachedImage = createSelector(
  imageUploadStatus,
  currentImageThumbnail,
  currentImageFull,
  imageFilesizeError,
  imageWidth,
  imageHeight,
  (imageUploadStatus, currentImageThumbnail, currentImageFull, imageFilesizeError, imageWidth, imageHeight) => {
    return {
      uploading: isUploading(imageUploadStatus),
      waiting: uploadIsWaiting(imageUploadStatus),
      currentImageThumbnail: currentImageThumbnail,
      currentImageFull: currentImageFull,
      imageFilesizeError: imageFilesizeError,
      imageWidth: imageWidth,
      imageHeight: imageHeight
    };
  }
);

export type MediaStatusType = {
  uploading?: boolean,
  waiting?: boolean,
  completed?: boolean,
  error?: boolean
};
export const mediaStatus = createSelector(
  imageUploadStatus,
  videoUploadStatus,
  (imageUploadStatus, videoUploadStatus) => {
    return {
      waiting: uploadIsWaiting(imageUploadStatus) && uploadIsWaiting(videoUploadStatus),
      uploading: isUploading(imageUploadStatus) || isUploading(videoUploadStatus),
      completed: uploadIsComplete(imageUploadStatus) || uploadIsComplete(videoUploadStatus),
      error: uploadHasFail(imageUploadStatus) || uploadHasFail(videoUploadStatus)
    };
  }
);
export type VideoType = {
  uploading?: boolean,
  uploadPercent?: number,
  waiting?: boolean,
  completed?: boolean,
  thumbnail?: string,
  title?: string,
  previewUrl?: string,
  videoError?: ?PublishingError,
  canCustomizeThumb?: bool,
  customThumb?: string,
  customThumbWarning?: bool,
  customThumbUploading?: bool,
  videoTranscodePercent?: number
};

export const canCustomizeVideoThumb = createSelector(
  selectedNetworks,
  capabilitiesMap,
  (selectedNetworks:Array<string>, capabilitiesMap): bool => {
    return _.some(selectedNetworks, (network: string): bool => {
      return isVideoCustomThumbSupported(network, capabilitiesMap);
    });
  }
);

export const customVideoThumbnailWarning = createSelector(
  selectedNetworks,
  capabilitiesMap,
  canCustomizeVideoThumb,
  customVideoThumbnail,
  (selectedNetworks:Array<string>, capabilitiesMap, canCustomizeVideoThumb, customVideoThumbnail): bool => {
    //Warn if not every network supports custom thumbnails, but at least one
    // *does* support it, and we have a custom thumbnail
    return (!_.every(selectedNetworks, (network: string): bool => {
      return isVideoCustomThumbSupported(network, capabilitiesMap);
    })) && canCustomizeVideoThumb && customVideoThumbnail;
  }
);

export const attachedVideo = createSelector(
  firstVideo,
  videoUploadStatus,
  videoUploadPercent,
  videoError,
  videoThumbnailToDisplay,
  canCustomizeVideoThumb,
  customVideoThumbnail,
  customVideoThumbnailWarning,
  customVideoThumbnailUploadingStatus,
  getCustomVideoTitle,
  videoTranscodePercent,
  (video,
    videoUploadStatus,
    videoUploadPercent,
    videoError,
    videoThumbnailToDisplay,
    canCustomizeVideoThumb,
    customVideoThumbnail,
    customVideoThumbnailWarning,
    customVideoThumbnailUploadingStatus,
    customVideoTitle,
    videoTranscodePercent): VideoType => {

    return {
      uploading: isUploading(videoUploadStatus),
      uploadPercent: videoUploadPercent,
      waiting: uploadIsWaiting(videoUploadStatus),
      completed: uploadIsComplete(getUploadStatus(video.get('transcodeResultsStatus'))),
      thumbnail: videoThumbnailToDisplay.uri,
      title: customVideoTitle || video.get('displayName'),
      previewUrl: video.get('videoPreviewUrl'),
      videoError,
      canCustomizeThumb: canCustomizeVideoThumb,
      customThumb: customVideoThumbnail,
      customThumbWarning: customVideoThumbnailWarning,
      customThumbUploading: customVideoThumbnailUploadingStatus === RequestStatus.STATUS_REQUESTED,
      videoTranscodePercent: videoTranscodePercent
    };
  }
);

export const hasAttachmentByType = createSelector(
  linkPreviewInfo,
  hasImageBeenRequested,
  hasVideoBeenRequested,
  (linkPreview: LinkPreviewInfo, hasImageBeenRequested: bool, hasVideoBeenRequested: bool) => {
    return {
      linkPreview: (_.get(linkPreview, 'linkUrl', '') !== '' || _.get(linkPreview, 'currentlyScraping', false)),
      images: hasImageBeenRequested,
      video: hasVideoBeenRequested
    };
  }
);

export const canAddAttachmentByType = createSelector(
  hasAttachmentByType,
  (hasType) => {
    return {
      linkPreview: !hasType.images && !hasType.video,
      images: !hasType.linkPreview && !hasType.video,
      video: !hasType.linkPreview && !hasType.images
    };
  }
);
