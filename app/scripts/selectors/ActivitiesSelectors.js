/* @flow */
import { createDeepEqualSelector } from 'utils/SelectorUtils';
import { getActivities } from 'reducers/index';

import type { List as ImmutableList } from 'immutable';
import type { ActivitiesObject, ActivitiesRecord } from 'records/MessageRecords';

const sortActivities = (activityA: ActivitiesObject, activityB: ActivitiesObject): number => {
  return activityB.createdAtTimestamp - activityA.createdAtTimestamp;
};

export const activitiesForDisplay: () => Array<ActivitiesObject> = createDeepEqualSelector(
  getActivities,
  (activities: ImmutableList<ActivitiesRecord>): Array<ActivitiesObject> => activities.toJS().sort(sortActivities)
);
