/* @flow */
import { createSelector } from 'reselect';
import { fromJS, List as ImmutableList } from 'immutable';
import _ from 'lodash';

import {
  getAvailableLabels,
  getSelectedLabels,
  getCurrentLabelText
} from 'reducers/index';

import { canCreateLabel } from 'selectors/EnvironmentSelector';

const findExistingVal = (haystack: any, needle: any) => {
  return haystack.find((haystackItem) => {
    if (haystackItem.get('title') === needle || needle.title) {
      return true;
    } else {
      return false;
    }
  });
};

export const getAvailableLabelsWithoutSelectedLabels = createSelector(
  [getAvailableLabels, getSelectedLabels, getCurrentLabelText, canCreateLabel],
  (availableLabels, selectedLabels, currentLabelText, canCreateLabel) => {
    let aLabels = availableLabels.toJS();
    const sLabels = selectedLabels.toJS();

    let newALabels = [];

    const currentInputTextElement = findExistingVal(availableLabels, currentLabelText);

    if (availableLabels.size > 0) {
      _.forEach(aLabels, (aValue, aKey) => {
        _.forEach(sLabels, (sValue, sKey) => {
          if (aValue.title === sValue.title) {
            delete (aLabels[aKey]);
          }
        });
      });

      _.forEach(aLabels, (aValue, aKey) => {
        if (aValue) {
          newALabels.push(aValue);
        }
      });
    }

    if (canCreateLabel && !currentInputTextElement && currentLabelText.replace(/\s/g, '').length > 0) {
      newALabels = [currentLabelText].concat(newALabels);
    }

    return newALabels;
  }
);

export const getLabelErrorGroup = createSelector(
  getSelectedLabels,
  (selectedLabels) => {
    let sLabels = selectedLabels.toJS();
    let labelGroups: Object = {};

    _.forEach(sLabels, (label) => {
      let labelGroup = labelGroups[label.contentLabelGroupId] || [];
      if (label.contentLabelGroupId) {
        labelGroup.push(label);
        labelGroup = _.uniqWith(labelGroup, _.isEqual);
        labelGroups[label.contentLabelGroupId] = labelGroup;
      }
    });

    let errorGroup = _.find(labelGroups, (group): bool => {
      return group.length > 1;
    });

    errorGroup = errorGroup ? fromJS(errorGroup) : ImmutableList();

    return errorGroup;
  }
);

export const labelsWithErrorsFirst = createSelector(
  [getSelectedLabels, getLabelErrorGroup],
  (selectedLabels, errorGroup) => {

    let sLabels = selectedLabels.filterNot((label) => {
      return errorGroup.some((errorLabel) => {
        return errorLabel.get('id') === label.get('id');
      });
    });

    errorGroup.forEach((label) => {
      sLabels = sLabels.push(label);
    });

    return sLabels.toJS();

  }
);

export const labelExists = (labelName: any) => {
  return createSelector(
    [ getAvailableLabels ],
    (availableLabels) => {
      return findExistingVal(availableLabels, labelName);
    }
  );
};
