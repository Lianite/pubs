/* @flow */
import { createSelector } from 'reselect';
import { List as ImmutableList } from 'immutable';
import {
  getAllAssignees,
  getValidAssignees,
  voiceSelector
} from 'reducers/index';
import { getFilteredVoiceIds } from 'selectors/AccountsSelectors';
import { AssigneeRecord } from 'records/AssigneeRecords';
import type { GetVoicesResponse } from 'apis/AccountsApi';
import _ from 'lodash';

export const getInvalidAssignees = createSelector(
  [getAllAssignees, getValidAssignees],
  (allAssignees: ImmutableList<AssigneeRecord>, validAssignees: ImmutableList<AssigneeRecord>) => {
    let invalidAssignees = allAssignees.filterNot((assignee) => {
      return validAssignees.some((validAssignee) => {
        return assignee.id === validAssignee.id;
      });
    });
    invalidAssignees = invalidAssignees.map((invalidAssignee) => {
      return invalidAssignee.set('invalid', true);
    });
    return invalidAssignees;
  }
);

export const getAssignees = createSelector(
  [getInvalidAssignees, getValidAssignees],
  (invalidAssignees: ImmutableList<AssigneeRecord>, validAssignees: ImmutableList<AssigneeRecord>) => {
    return validAssignees.concat(invalidAssignees).toJS();
  }
);

const findConflictingVoices = (filteredVoiceIds: Array<string>, voices:Array<Object>, res: GetVoicesResponse) => {
  const conflictingVoiceIds = _.reject(filteredVoiceIds, (voiceId): bool => {
    return _.some(res, (voice): bool => {
      return voice.data.id === _.toNumber(voiceId);
    });
  });
  return _.filter(voices, (voice): bool => {
    return _.some(conflictingVoiceIds, (conflictingVoiceId): bool => {
      return _.toNumber(conflictingVoiceId) === voice.data.id;
    });
  });
};

export const getAssigneeVoiceConflicts = (res: GetVoicesResponse) =>  {
  return createSelector(
    [getFilteredVoiceIds, voiceSelector],
    (filteredVoiceIds: Array<string>, voices:Array<Object>) => {
      return findConflictingVoices(filteredVoiceIds, voices, res);
    }
  );
};
