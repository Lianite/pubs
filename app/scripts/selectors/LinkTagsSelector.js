/* @flow */
import { createSelector } from 'reselect';
import {
  Map as ImmutableMap,
  List as ImmutableList } from 'immutable';
import {
  LinkTagDetails,
  LinkTagPropertiesRecord } from 'records/LinkTagsRecords';
import type { LinkTagsUrlsType } from 'records/LinkTagsRecords';
import {
  messageTitleSelector,
  linkTagsUrls,
  getMessageEditorStateMapText
} from 'reducers/index';
import { RequestStatus } from 'constants/ApplicationConstants';
import _ from 'lodash';
import {
  featureFlags,
  getCompanyData
} from 'reducers/index';
import { CompanyRecord } from 'reducers/EnvironmentReducer';

const linkTagIsEnable = linkTag => linkTag.get('enabled');
const linkTagIsBeenSaved = linkTag => linkTag.get('saveStatus') === RequestStatus.STATUS_REQUESTED;
const linkTagWasSaved = linkTag => linkTag.get('saveStatus') === RequestStatus.STATUS_LOADED;

const requiredVariablesSatisfied = linkTag => {
  const requiredVariables = linkTag.get('tagVariables').filter(tagVariable => tagVariable.getIn(['variableDescription', 'required']));
  if (requiredVariables.isEmpty()) {
    return true;
  }
  return requiredVariables.every(tagVariable => {
    const variableValue = linkTag.get('variableValues').find(variableValue => variableValue.get('variableId') === tagVariable.get('id'));
    return variableValue && variableValue.get('values') && variableValue.get('values').first();
  });
};

const linkTagNeedToBeSaved = linkTag => {
  const saveStatus = linkTag.get('saveStatus');
  return saveStatus !== RequestStatus.STATUS_LOADED &&
    saveStatus !== RequestStatus.STATUS_REQUESTED &&
    requiredVariablesSatisfied(linkTag);
};

export const linkTagsByLink = createSelector(
  linkTagsUrls,
  (linkTagUrlList:LinkTagsUrlsType) => {
    return linkTagUrlList.reduce((linkTagsByLink, networks, url) => {
      networks
        .filter(linkTag => linkTag.get('enabled'))
        .forEach(linkTag => {
          if (!linkTagsByLink.get(url)) {
            linkTagsByLink = linkTagsByLink.set(url, new LinkTagDetails({
              tagVariables: linkTag.get('tagVariables'),
              variableValues: linkTag.get('variableValues')
            }));
          }
        });
      return linkTagsByLink;
    }, ImmutableMap());
  }
);

export const linkTagsReadyToBeSaved = createSelector(
  linkTagsUrls,
  (linkTagUrlList:LinkTagsUrlsType) => {
    return linkTagUrlList.reduce((linkTagsReadyToBeSaved, linkTagsByNetwork, url) => {
      linkTagsByNetwork
        .filter(linkTagIsEnable)
        .filter(linkTagNeedToBeSaved)
        .forEach((linkTag, network) => {
          linkTagsReadyToBeSaved = linkTagsReadyToBeSaved.push(ImmutableMap({link: url, network, linkTag}));
        });

      return linkTagsReadyToBeSaved;
    }, ImmutableList());
  }
);

export const linkTagsAreBeenSaved = createSelector(
  linkTagsUrls,
  (linkTagUrlList:LinkTagsUrlsType) => {
    return linkTagUrlList.reduce((linkTagsAreBeenSaved, linkTagsByNetwork, url) => {
      linkTagsByNetwork
        .filter(linkTagIsEnable)
        .filter(linkTagIsBeenSaved)
        .forEach((linkTag, network) => {
          linkTagsAreBeenSaved = linkTagsAreBeenSaved.push(ImmutableMap({link: url, network, linkTag}));
        });

      return linkTagsAreBeenSaved;
    }, ImmutableList());
  }
);

type LinkTagDetailsType = {
  link: string,
  network: string,
  linkTag: LinkTagPropertiesRecord
};
export type SavedLinkTagsType = ImmutableList<ImmutableMap<string, LinkTagDetailsType>>;

export const savedLinkTags = createSelector(
  linkTagsUrls,
  (linkTagUrlList:LinkTagsUrlsType) => {
    return linkTagUrlList.reduce((savedLinkTags, linkTagsByNetwork, url) => {
      linkTagsByNetwork
        .filter(linkTagIsEnable)
        .filter(linkTagWasSaved)
        .forEach((linkTag, network) => {
          savedLinkTags = savedLinkTags.push(ImmutableMap({link: url, network, linkTag}));
        });

      return savedLinkTags;
    }, ImmutableList());
  }
);

export const linkTagsEnable = createSelector(
  featureFlags,
  getCompanyData,
  (features:ImmutableMap<string, bool>, companyData:CompanyRecord) => {
    return features.get('linkTags') && companyData.get('enabledFeatures').includes('link_tagging');
  }
);

const getMessageTitle = (title:string, message:string) => {
  let messageTitle = title;
  if (!messageTitle) {
    messageTitle = message;
    if (messageTitle && messageTitle.length > 63) {
      messageTitle = messageTitle.split(' ').slice(0, 4).join(' ');
      if (messageTitle.length > 63) {
        messageTitle = messageTitle.substring(0, 63);
      }
    }
  }
  return messageTitle;
};

export const getMessageTitleByNetwork = createSelector(
  messageTitleSelector,
  getMessageEditorStateMapText,
  (messageTitle:string, message:Object) => {
    return _.reduce(message, (titleMap, messageText, network) => {
      return titleMap.set(network, getMessageTitle(messageTitle, messageText) || network);
    }, ImmutableMap());
  }
);
