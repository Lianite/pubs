/* @flow */
import { createSelector } from 'reselect';
import { getPlans } from 'reducers/index';
import { getPlanIdFromRoute } from 'selectors/RouterSelector';
import _ from 'lodash';

export const getPlanById = createSelector(
  getPlans,
  getPlanIdFromRoute,
  (plans, planIdFromRoute) => {
    return _.find(plans, (plan) => plan.id === planIdFromRoute);
  }
);
