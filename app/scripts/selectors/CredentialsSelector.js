/* @flow */
import { createSelector } from 'reselect';
import { checkedCredentialsSelector } from 'reducers/index';
import _ from 'lodash';

export const credentialUniqueIds = createSelector(
    checkedCredentialsSelector,
    (credentials) => _.map(credentials.toJS(), account => account.uniqueId)
);