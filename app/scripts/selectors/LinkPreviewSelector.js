/* @flow */
import { createSelector, createStructuredSelector } from 'reselect';
import {
  selectedNetworks,
  urlsMap,
  urlProperties,
  capabilitiesMap,
  linkPreviewSourceMedia,
  linkPreviewCustomUploadedMedia,
  linkPreviewDescription,
  linkPreviewCaption,
  linkPreviewTitle,
  linkPreviewThumbnailUrl,
  linkPreviewCurrentlyScraping,
  linkPreviewLinkUrl,
  linkPreviewVideoSrc,
  linkPreviewVideoLink,
  linkPreviewRemoved } from 'reducers/index';
import { DefaultNetwork } from 'constants/ApplicationConstants';
import _ from 'lodash';
import {
  Map as ImmutableMap,
  List as ImmutableList } from 'immutable';

import type { LinkPreviewMediaRecord, LinkPreviewInfo } from 'records/LinkPreviewRecords';

export type LinkPreviewMapType = {
  [network: string]: LinkPreviewInfo
};

export const linkPreviewInfo = createStructuredSelector({
  description: linkPreviewDescription,
  caption: linkPreviewCaption,
  title: linkPreviewTitle,
  thumbnailUrl: linkPreviewThumbnailUrl,
  currentlyScraping: linkPreviewCurrentlyScraping,
  linkUrl: linkPreviewLinkUrl,
  videoSrc: linkPreviewVideoSrc,
  videoLink: linkPreviewVideoLink,
  removed: linkPreviewRemoved
});

export const fullLinkPreviewThumbnailList = createSelector(
  [linkPreviewSourceMedia, linkPreviewCustomUploadedMedia],
  (scrapedImages, uploadedImages):Array<LinkPreviewMediaRecord> => {
    if (scrapedImages && uploadedImages) {
      return uploadedImages.concat(scrapedImages).toArray();
    } else {
      return [];
    }
  }
);

export const getPossibleLinkPreviewThumbnails = createSelector(
  [linkPreviewSourceMedia, linkPreviewCustomUploadedMedia],
  (scrapedImages, uploadedImages) => {
    if (scrapedImages && uploadedImages) {
      return uploadedImages.concat(scrapedImages).toArray().map( (mediaRecord: LinkPreviewMediaRecord):string => {
        return mediaRecord.src;
      });
    } else {
      return [];
    }
  }
);

export const linkPreviewHasMultipleThumbnails = createSelector(
  getPossibleLinkPreviewThumbnails,
  (thumbnailList) => {
    return thumbnailList.length > 1;
  }
);

function isLinkPreviewSupported(network, capabilitiesMap) {
  return !!capabilitiesMap.getIn([network, 'NEW_MESSAGE', 'NOTE', 'linkPreview', 'supported']);
}

const supportedNetworks = (capabilitiesMap, selectedNetworks) => {
  return selectedNetworks.filter(network => isLinkPreviewSupported(network, capabilitiesMap));
};

export const allSelectedNetworksSupportLinkPreview = createSelector(
  capabilitiesMap,
  selectedNetworks,
  (capabilitiesMap, selectedNetworks) => {
    return _.every(selectedNetworks, (network:string):bool => {
      return isLinkPreviewSupported(network, capabilitiesMap);
    });
  }
);

export const linkPreviewNetworks = createSelector(
  capabilitiesMap,
  selectedNetworks,
  (capabilitiesMap, selectedNetworks) => {
    let networks = [DefaultNetwork];
    if (selectedNetworks && selectedNetworks.length > 0) {
      networks = supportedNetworks(capabilitiesMap, selectedNetworks);
    }

    return networks;
  }
);

export const linkPreviewLoading = createSelector(
  linkPreviewInfo,
  (linkPreview: LinkPreviewInfo):bool => {
    return linkPreview.currentlyScraping;
  }
);

export const canShowLinkPreview = createSelector(
  linkPreviewInfo,
  capabilitiesMap,
  selectedNetworks,
  linkPreviewNetworks,
  (linkPreview: LinkPreviewInfo, capabilitiesMap, selectedNetworks, linkPreviewNetworks): bool => {
    return (linkPreview.linkUrl && (!selectedNetworks.length || linkPreviewNetworks.length > 0)) || linkPreview.currentlyScraping;
  }
);

export const getLinkPreviewAssetId = createSelector(
  [linkPreviewInfo, fullLinkPreviewThumbnailList],
  (linkPreview: LinkPreviewInfo, thumbList:Array<LinkPreviewMediaRecord>) : number => {
    let linkPreviewAssetId = -1;
    let currentLinkPreviewThumb:?LinkPreviewMediaRecord = _.find(thumbList, (entry:LinkPreviewMediaRecord):bool => {
      return entry.src === linkPreview.thumbnailUrl;
    });

    if (currentLinkPreviewThumb) {
      linkPreviewAssetId = currentLinkPreviewThumb.id;
    }

    return linkPreviewAssetId;
  }
);

export const linkPreviewWasRemoved = createSelector(
  linkPreviewInfo,
  (linkPreview: LinkPreviewInfo): bool => {
    return linkPreview && linkPreview.removed;
  }
);

export const selectedLinkPreviewUrl = createSelector(
  linkPreviewInfo,
  linkPreviewWasRemoved,
  (linkPreview: LinkPreviewInfo, linkPreviewWasRemoved: bool): string => {
    if (linkPreviewWasRemoved) {
      return '';
    }
    return linkPreview.linkUrl;
  }
);

export const getLinkPreviewMap = createSelector(
  selectedLinkPreviewUrl,
  linkPreviewInfo,
  linkPreviewNetworks,
  getLinkPreviewAssetId,
  (selectedLinkPreviewUrl, linkPreview: LinkPreviewInfo, linkPreviewNetworks, assetId) : LinkPreviewMapType => {
    const linkPreviewMap = {};
    if (selectedLinkPreviewUrl) {
      linkPreviewNetworks.forEach(network => {
        linkPreviewMap[network] = {
          linkUrl: linkPreview.linkUrl,
          description: linkPreview.description,
          caption: linkPreview.caption,
          title: linkPreview.title,
          thumbnailUrl: linkPreview.thumbnailUrl,
          videoSrc: linkPreview.videoSrc,
          videoLink: linkPreview.videoLink
        };
        //add asset id if there is one
        if (assetId !== -1) {
          linkPreviewMap[network].imageAssetId = assetId;
        }
      });
    }

    return linkPreviewMap;
  }
);

export const linkPreviewUrlsListCurrentlyVisible = createSelector(
  urlsMap,
  urlProperties,
  linkPreviewNetworks,
  (urlsMap: ImmutableMap<string, ImmutableList<string>>, urlProperties, linkPreviewNetworks) : ImmutableList<string> => {
    //NOTE: Right now this works mainly becuase there is only a single message
    //      box that supports link preview. Either just Facebook, or no network
    //      selected. When we have multiple networks that support link preview,
    //      this might need to change
    let returnList = ImmutableList();
    const numNetworks = urlsMap.size;
    urlsMap.forEach((urlList: ImmutableList<string>, network: string):any => {
      if ( (network === DefaultNetwork && numNetworks === 1)  || _.includes(linkPreviewNetworks, network)) {
        returnList = returnList.concat(urlList);
      }
    });

    return returnList;
  }
);
