/* @flow */
import { createSelector } from 'reselect';
import { featureFlags } from 'reducers/index';
import { GenderState } from 'records/TargetingRecords';
import {
    CredentialRecord
} from 'records/AccountsRecord';
import {
    List as ImmutableList,
    Map as ImmutableMap
} from 'immutable';
import {
  SupportedNetworks,
  TARGETING_SUBTYPES,
  GENDER_TARGETING_TYPES
} from 'constants/ApplicationConstants';
import {
    checkedCredentialsSelector,
    facebookTargetingSelectedLanguages,
    facebookTargetingLanguageOptions,
    facebookTargetingIncludedLocations,
    facebookTargetingLocationOptions,
    facebookTargetingSelectedGender
} from 'reducers/index';
import {
    LanguageState,
    LanguagesState,
    LocationsState,
    LocationState
} from 'records/TargetingRecords';

import type { TargetingEntity, SelectedLocationsTree } from 'adapters/types';
import type { rootState } from 'reducers/index';

export const selectedLanguages: (state: rootState) => Array<TargetingEntity> = createSelector(
    [facebookTargetingSelectedLanguages],
    (selectedLanguages: LanguageState) => selectedLanguages.toJS()
);

export const targetingSet: (state: rootState) => bool = createSelector(
  [facebookTargetingSelectedLanguages, facebookTargetingSelectedGender, facebookTargetingIncludedLocations],
  (selectedLanguages: ImmutableList<LanguageState>, selectedGender: GenderState, includedLocations: ImmutableList<LocationState>) => (
    !!selectedLanguages.size ||
    (selectedGender.value !== GENDER_TARGETING_TYPES.All.value) ||
    !!includedLocations.size
  )
);

export const languageOptions: (state: rootState) => Array<TargetingEntity> = createSelector(
    [facebookTargetingSelectedLanguages, facebookTargetingLanguageOptions],
    (selectedLanguages: LanguagesState, languageOptions: LanguagesState) => {
      return languageOptions
          .filter((language) => !selectedLanguages
          .find((languageOption) => languageOption.value === language.value))
          .toJS()
          .sort((langA: TargetingEntity, langB: TargetingEntity) => langA.description.localeCompare(langB.description));
    }
);

export const includedLocationsByCountry: (state: rootState) => SelectedLocationsTree = createSelector(
  [facebookTargetingIncludedLocations],
  (includedLocations: LocationsState) => {
    return includedLocations.reduce((reduction: Object, location: LocationState) => {
      switch (location.subType) {
      case TARGETING_SUBTYPES.COUNTRY:
        reduction[location.description] = reduction[location.description] || {
          description: location.description,
          value: location.value,
          children: []
        };
        break;
      case TARGETING_SUBTYPES.REGION:
      case TARGETING_SUBTYPES.METRO:
      case TARGETING_SUBTYPES.ZIP_CODE:
        const countryName = location.parentTargetingValue.description;
        reduction[countryName] = reduction[countryName] || {
          description: countryName,
          children: []
        };
        reduction[countryName].children.push(location.toJS());
        break;
      }
      return reduction;
    }, {});
  }
);

export const locationOptions: (state: rootState) => Array<TargetingEntity> = createSelector(
    [facebookTargetingIncludedLocations, facebookTargetingLocationOptions, includedLocationsByCountry],
    (includedLocations: LocationsState, locationOptions: LocationsState, includedLocationsByCountry: SelectedLocationsTree) => {
      return locationOptions
          .filter((location) => !includedLocations
            .find((locationOption) => locationOption.value === location.value) &&
            !(location.subType === TARGETING_SUBTYPES.COUNTRY && includedLocationsByCountry[location.description])
          )
          .toJS()
          .sort((locA: TargetingEntity, locB: TargetingEntity) => {
            if (locA.subType !== locB.subType) {
              return (
                locB.subType === TARGETING_SUBTYPES.COUNTRY ||
                (locB.subType === TARGETING_SUBTYPES.REGION && (locA.subType === TARGETING_SUBTYPES.METRO || locA.subType === TARGETING_SUBTYPES.ZIP_CODE)) ||
                (locB.subType === TARGETING_SUBTYPES.METRO && locA.subType === TARGETING_SUBTYPES.ZIP_CODE)
              ) ? 1 : -1;
            }
            return locA.description.localeCompare(locB.description);
          });
    }
);

export const includedLocations: (state: rootState) => Array<TargetingEntity> = createSelector(
  [facebookTargetingIncludedLocations],
  (includedLocations: LocationsState) => includedLocations.toJS()
);

export const checkedFacebookCredentials: (state: rootState) => ImmutableList<CredentialRecord> = createSelector(
  [checkedCredentialsSelector],
  (credentials: ImmutableMap<string, CredentialRecord>): ImmutableList<CredentialRecord> => credentials.filter((credential: CredentialRecord) => credential.socialNetwork === SupportedNetworks.FACEBOOK).toList()
);

export const invalidTargetingAccounts: () => Array<CredentialRecord> = createSelector(
  [checkedFacebookCredentials],
  (facebookCredentials: ImmutableList<CredentialRecord>): Array<CredentialRecord> => facebookCredentials.filter((credential: CredentialRecord) => !credential.authenticated || !credential.targetingSupported).toArray()
);

export const canTarget: () => bool = createSelector(
  [checkedFacebookCredentials, invalidTargetingAccounts],
  (checkedFacebookCredentials: ImmutableList<CredentialRecord>, invalidTargetingAccounts: Array<CredentialRecord>): bool => !!checkedFacebookCredentials.size && !invalidTargetingAccounts.length
);

export const targetingIsEnabled: () => bool = createSelector(
  [featureFlags],
  (featureFlags) => featureFlags.get('facebookTargeting')
);

export const hasError: () => bool = createSelector(
  [targetingSet, canTarget],
  (targetingSet: bool, canTarget: bool) => (
    targetingSet && !canTarget
  )
);

export const invalidDarkPostAccounts: () => Array<CredentialRecord> = createSelector(
  [checkedFacebookCredentials],
  (facebookCredentials: ImmutableList<CredentialRecord>): Array<CredentialRecord> => facebookCredentials.filter((credential: CredentialRecord) => !credential.organicVisibilitySupported).toArray()
);

export const canDarkPost: () => bool = createSelector(
  [checkedFacebookCredentials, invalidDarkPostAccounts],
  (checkedFacebookCredentials: ImmutableList<CredentialRecord>, invalidDarkPostAccounts: Array<CredentialRecord>): bool => !!checkedFacebookCredentials.size && !invalidDarkPostAccounts.length
);
