/* @flow */
import { createSelector, createStructuredSelector } from 'reselect';
import { getMessageStatus,
  hasLabels,
  hasNotes,
  scheduledDateTimeSelector,
  selectedNetworks } from 'reducers/index';
import { MessageConstants, SupportedNetworks } from 'constants/ApplicationConstants';
import { SegmentEventConstants, SegmentProductConstants } from 'constants/SegmentConstants';
import { getConversationsEnviromentDetails } from 'utils/ApiUtils';
import _ from 'lodash';

/**
 * IMPORTANT:
 * This selecor is used to determine whether something that's logged in segment
 * will communicate up to Gainsight, which is a audience tracking tool SEs and other
 * execs use to track user interaction with the app. This flag should be set to true
 * iif the environment we're in is prod, otherwise it should remain off. Note: the flag
 * must be set to true AND the environment much actually be prod (it's at that point
 * we point our api key to the gainsight segment proj)
 */
const integrationsSelector = () => {
  let envUrl = getConversationsEnviromentDetails().environmentUrl;
  let communicateToGainsight = false;
  if (envUrl) {
    if ((envUrl.indexOf('app.spredfast') > -1 || envUrl.indexOf('app3.spredfast') > -1 || envUrl.indexOf('vpc.spredfast') > -1)) {
      communicateToGainsight = true;
    }
  }

  return {
    Gainsight: communicateToGainsight
  };
};

const hasSpecifiedNetwork = (network) => {
  return createSelector(
    selectedNetworks,
    (selectedNetworks) => _.includes(selectedNetworks, network),
  );
};

const hasScheduledTime = createSelector(
  scheduledDateTimeSelector,
  (scheduledDatetime) => !!scheduledDatetime
);

export const segmentCreateMessageEventSelector = createSelector(
  getMessageStatus,
  (messageStatus) => {
    if (messageStatus === MessageConstants.PENDING) {
      return SegmentEventConstants.PUBLISH_MESSAGE;
    } else if (messageStatus === MessageConstants.DRAFT) {
      return SegmentEventConstants.SAVE_MESSAGE_AS_DRAFT;
    }
  }
);

export const segmentCreateMessagePropertiesSelector = createStructuredSelector({
  hasFacebook: hasSpecifiedNetwork(SupportedNetworks.FACEBOOK),
  hasTwitter: hasSpecifiedNetwork(SupportedNetworks.TWITTER),
  hasLabel: hasLabels,
  hasNotes: hasNotes,
  hasSchedule: hasScheduledTime,
  hasSponsor: () => false, // NOTE: this will change to a selector once we implement branded messaging for facebook
  integrations: integrationsSelector,
  product: () => SegmentProductConstants.CONVERSATIONS_PUBLISHING
});
