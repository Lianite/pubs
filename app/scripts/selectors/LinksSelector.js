/* @flow */
import {
  selectedLinkPreviewUrl,
  linkPreviewNetworks } from 'selectors/LinkPreviewSelector';
import {
  savedLinkTags } from 'selectors/LinkTagsSelector';
import { createDeepEqualSelector } from 'utils/SelectorUtils';
import { createSelector } from 'reselect';
import { urlsMap, urlProperties } from 'reducers/index';
import {
  OrderedSet as ImmutableOrderedSet,
  List as ImmutableList,
  Set as ImmutableSet,
  Map as ImmutableMap } from 'immutable';
import { DefaultNetwork } from 'constants/ApplicationConstants';
import type {
  UrlsType,
  UrlPropertiesType } from 'records/LinksRecords';
import type {
  SavedLinkTagsType } from 'selectors/LinkTagsSelector';

export const urls = createDeepEqualSelector(
  urlsMap,
  (urlsMap:UrlsType) => {
    let allLinks = [];
    urlsMap.forEach((urlList:ImmutableList<string>) => {
      allLinks = allLinks.concat(urlList.toArray());
    });
    return ImmutableOrderedSet(allLinks);
  }
);

export const getFormatedUrlProperties = createSelector(
  urlProperties,
  (urls:UrlPropertiesType) => {
    return urls.map((propertySet, url) => {
      return {
        fullUrl: url,
        shortenOnPublish: propertySet.shortenOnPublish,
        addLinkTags: propertySet.addLinkTags
      };
    }).toObject();
  }
);

export const getLinkDTOsForNetworkSelector = createSelector(
  urlProperties,
  getFormatedUrlProperties,
  selectedLinkPreviewUrl,
  linkPreviewNetworks,
  savedLinkTags,
  (urlProperties:UrlPropertiesType, formatedUrlProperties:Object, selectedLinkPreviewUrl:string, linkPreviewNetworks:Array<string>, savedLinkTags:SavedLinkTagsType) => {
    return urlProperties.reduce((updatedMap, properties, link) => {
      if (link === selectedLinkPreviewUrl) {
        const networks = properties.get('associatedNetworks').concat(linkPreviewNetworks);
        properties = properties.set('associatedNetworks', networks);
      }
      properties.get('associatedNetworks').forEach(network => {
        const urlProperties = {...formatedUrlProperties[link]};
        if (urlProperties.addLinkTags) {
          const linkTag = savedLinkTags.find(linkTag => linkTag.get('network') === network && linkTag.get('link') === link);
          if (linkTag) {
            urlProperties.tagId = linkTag.getIn(['linkTag', 'id']);
          }
        }
        const links = ImmutableList(updatedMap.get(network))
          .push(urlProperties)
          .toArray();
        updatedMap = updatedMap.set(network, links);
      });
      return updatedMap;
    }, ImmutableMap());
  }
);

export const linkPreviewUrls = createSelector(
  urls,
  urlProperties,
  linkPreviewNetworks,
  selectedLinkPreviewUrl,
  (urls:ImmutableSet<string>, urlProperties:UrlPropertiesType, linkPreviewNetworks:Array<string>, selectedLinkPreviewUrl:string) => {
    const allUrls = selectedLinkPreviewUrl && !urls.includes(selectedLinkPreviewUrl) ? ImmutableOrderedSet([selectedLinkPreviewUrl].concat(urls.toArray())) : urls;
    return allUrls.filter(url => {
      const associatedNetworks = urlProperties.getIn([url, 'associatedNetworks']);
      return url === selectedLinkPreviewUrl ||
        (associatedNetworks && !associatedNetworks.intersect([...linkPreviewNetworks, DefaultNetwork]).isEmpty());
    });
  }
);
