/* @flow */
import { createSelector } from 'reselect';
import { createDeepEqualSelector } from 'utils/SelectorUtils';
import _ from 'lodash';
import { validationText, mediaErrorText, permissionsText } from 'constants/UITextConstants';
import { ValidationType } from 'constants/UIStateConstants';
import { messageEditorStateText,
  messageEditorStateMap,
  messageCurrentlySavingSelector,
  scheduledDateTimeSelectorIsValid,
  imageFilesize,
  selectedNetworks,
  imageMimeType,
  imageUploadError,
  capabilitiesMap,
  hasEditAccess,
  hasViewAccess,
  linkPreviewUploadingCustomThumbErrorText,
  videoUploadAndTranscodeError,
  customVideoThumbnailUploadingError,
  firstVideo,
  allVideoFileSizes,
  allVideoTranscodedFilesizes,
  imageUploadStatus,
  videoUploadStatus,
  videoTranscodeStatus,
  customVideoThumbnailUploadingStatus,
  linkPreviewCurrentlyScraping,
  linkPreviewAlreadyUploadingMedia,
  isMultiChannelEnabled,
  savingNote,
  hasVideoBeenRequested,
  hasImageBeenRequested,
  eventIdSelector,
  loadingMessageStatusSelector,
  getUserDataStatus
} from 'reducers/index';
import { messageHasDestination } from 'selectors/AccountsSelectors';
import { canShowLinkPreview, allSelectedNetworksSupportLinkPreview } from 'selectors/LinkPreviewSelector';
import { TwitterConstants } from 'constants/CapabilitiesConstants';
import { PublishingError } from 'records/ErrorRecords';
import { RequestStatus, UploadingStatus, MediaMimeTypeConstants, DefaultNetwork} from 'constants/ApplicationConstants';
import { toTitleCase } from 'utils/NotificationUtils';
import { List as ImmutableList, Map as ImmutableMap } from 'immutable';
import { getLabelErrorGroup } from 'selectors/LabelsSelector';
import { hasError as hasFacebookError } from 'selectors/FacebookSelector';
import {
  linkTagsAreBeenSaved,
  linkTagsReadyToBeSaved } from 'selectors/LinkTagsSelector';

import type { EditorState } from 'draft-js';
import type { UploadingStatusType, RequestStatusType } from 'constants/ApplicationConstants';
import type { SocialVideoRecord } from 'records/VideosRecords';
import type { NetworkCapabilities } from 'constants/types';

export type PerNetworkValidationData = {
  type: string,
  inlineText: string,
  hoverText: string
}

export type MessageValidationData = {
  type: string,
  validationText: string
}

type ValidationFunction = ((network: string, networkUIName: string, capabilities: NetworkCapabilities, networkPlainText: string) => PerNetworkValidationData);

const emptyValidation: PerNetworkValidationData = {
  type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
  inlineText: '',
  hoverText: ''
};

//Helper method to validate a single network
const validateNetwork = (network: string, networkUIName: string, capabilities: NetworkCapabilities, networkPlainText: string): PerNetworkValidationData => {
  let validation: PerNetworkValidationData = _.cloneDeep(emptyValidation);

  if (networkPlainText.length > capabilities.maxChars) {
    validation = {
      type: ValidationType.VALIDATION_TYPE_ERROR,
      inlineText: networkUIName + ' messages must be ' + capabilities.maxChars + ' characters or less.',
      hoverText: networkUIName + ' messages must be ' + capabilities.maxChars + ' characters or less.'
    };
  }

  return validation;
};

//create a validator method that can be called with just a text string and a network

const getValidator = (network): ValidationFunction => {
  //This will eventually return a dynamically created validation
  //  function dependent on the network.
  let validator = validateNetwork.bind(null, network, '', {maxChars: Number.MAX_SAFE_INTEGER});

  if (network === 'TWITTER') {
    validator = validateNetwork.bind(null, network, 'Twitter', TwitterConstants);
  }

  return validator;
};

//We want to cache our selectors we create to get validation for a particular
// network. This is because these selectors will be created one for each message
// box on the screen, and also will be used as dependencies for the topLevelValidation
// selector. We don't want multiple instances floating around, or else the validation
// per network might be called more than once.
var selectorCache = {};

export const makeGetValidationForNetwork = (network: string) => {

  let validate: Function = getValidator(network);

  if (!selectorCache[network]) {
    selectorCache[network] = createSelector(
      [messageEditorStateText],
      (text: string): PerNetworkValidationData => {
        let validationReturn: MessageValidationData = {
          type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
          validationText: ''
        };
        return validate(text, validationReturn);
      }
    );
  }

  return selectorCache[network];
};

export const canUseMultichannelPubs = createSelector(
  hasViewAccess,
  isMultiChannelEnabled,
  eventIdSelector,
  loadingMessageStatusSelector,
  getUserDataStatus,
  (hasViewAccess: bool, isMultiChannelEnabled: bool, eventId: bool,
    loadingMessageStatus: RequestStatusType, userDataStatus: RequestStatusType): bool => {
    if (loadingMessageStatus !== RequestStatus.STATUS_REQUESTED &&
      userDataStatus === RequestStatus.STATUS_LOADED) {
      return (!!eventId || (hasViewAccess && isMultiChannelEnabled));
    } else {
      return true;
    }
  }
);

// Bind arguments starting after the first one passed in.
// This is needed because when 'topLevelValidation' is
// called, it will just be passed the state, but the
// dependent selectors (one for each of the networks currently
// active), need to be passed the network so they can
// grab the correct part of the state tree

const bind_trailing_args = (fn, ...bound_args) => {
  return function(...args) {
    return fn(args[0], ...bound_args);
  };
};

//This top level validation selector needs to be dependent on all of the
// selector creators for the individual networks. For right now, we're
// going to list them explicitly. Ideally, we'd want these to be created
// from a list of possible networks store somewhere else.

//Important notes:
// 1. `bind_trailing_args` is used to make sure that the selector created
//     will have the network parameter passed in whenever the top level
//     validation is recomputed.
// 2. `makeGetValidationForNetwork` will create a separate selector for
//    each network. However, we cache these (above) so that in the entire
//    app, we only have one selector created for each network. Since
//    `makeGetValidationForNetwork` is also used to create the selectors
//    for the individual Message components, we don't want extra instances
//    of these selectors hanging around. You might get the validation selector
//    for any network called multiple times (one for each instance) otherwise

export const topLevelValidation = createDeepEqualSelector(
  [bind_trailing_args(makeGetValidationForNetwork(DefaultNetwork), DefaultNetwork),
   bind_trailing_args(makeGetValidationForNetwork('FACEBOOK'), 'FACEBOOK'),
   bind_trailing_args(makeGetValidationForNetwork('TWITTER'), 'TWITTER'),
   hasEditAccess,
   canUseMultichannelPubs],
    (messageValidation: PerNetworkValidationData, facebookValidation: PerNetworkValidationData, twitterValidation: PerNetworkValidationData, hasEditAccess: bool, canUseMultichannelPubs: bool) => {
      let validationReturn: MessageValidationData;
      if (!hasEditAccess && canUseMultichannelPubs) {
        validationReturn = {
          type: ValidationType.VALIDATION_TYPE_EDIT_PERMISSION_ERROR,
          validationText: permissionsText.noEditAccessError
        };
      } else {
        validationReturn = {
          type: twitterValidation.type,
          validationText: twitterValidation.type === ValidationType.VALIDATION_TYPE_ERROR ? 'Twitter' + validationText.generalError : ''
        };
      }
      //For now, we're only checking for twitter errors. In the future, we will
      // have to combine errors / improvements from different networks

      return validationReturn;
    }
);

export const linkPreviewCustomThumbError = createSelector(
linkPreviewUploadingCustomThumbErrorText, selectedNetworks,
(errorText: string, networks: Array<string>): ?PublishingError => {
  if (errorText) {
    return new PublishingError({
      title: mediaErrorText.imageUploadError,
      description: errorText,
      networks: ImmutableList(networks)
    });
  }
});

export const imageFilesizeError = createSelector(
  [capabilitiesMap, imageFilesize, imageMimeType, selectedNetworks, imageUploadError],
  (capabilities, filesize, mimeType, networks, imageUploadError) => {
    if (filesize && networks && !imageUploadError) {
      let imageErrorsNetworks = [];
      const filteredCapabilities = capabilities.filter((capability, key) => {
        return networks.indexOf(key) > -1;
      });
      filteredCapabilities.map((val, key) => {
        const imageCapability = val.getIn(['NEW_MESSAGE', 'NOTE', 'multimediaSpecs']);
        let maxImageSize;
        if (mimeType === MediaMimeTypeConstants.ANIMATED_GIF && key === 'TWITTER') {
          maxImageSize = imageCapability.getIn([1, 'maxSize']);
        } else {
          maxImageSize = imageCapability.getIn([0, 'maxSize']);
        }

        if (filesize > maxImageSize) {
          imageErrorsNetworks.push(toTitleCase(key));
        }
      });

      if (imageErrorsNetworks.length) {
        return new PublishingError({
          title: mediaErrorText.imageSizeTooLargeTitle,
          description: `${mediaErrorText.mediaSizeTooLargeDescription} for ${imageErrorsNetworks.join(', ')}`,
          networks: ImmutableList(imageErrorsNetworks)
        });
      }
    } else if (imageUploadError) { // captures server errors and prints them to the user
      return new PublishingError({
        title: mediaErrorText.imageUploadError,
        description: imageUploadError,
        networks: ImmutableList(networks)
      });
    }
  }
);

export const videoDuration = createSelector(
  firstVideo,
  (video: SocialVideoRecord):number => {
    return video.duration;
  }
);

export const videoError = createSelector(
  [capabilitiesMap,
    allVideoFileSizes,
    allVideoTranscodedFilesizes,
    videoDuration,
    selectedNetworks,
    videoUploadAndTranscodeError,
    customVideoThumbnailUploadingError],
  (capabilities,
    allVideoFileSizes,
    allVideoTranscodedFilesizes,
    duration,
    networks,
    videoError,
    customThumbnailError) => {
    if ( (allVideoFileSizes !== {} || duration) && networks && !videoError && !customThumbnailError) {
      let mediaDurationLongErrorsNetworks = [];
      let mediaDurationShortErrorsNetworks = [];
      let mediaSizeErrorsNetworks = [];

      const filteredCapabilities = capabilities.filter((capability, key) => {
        return networks.indexOf(key) > -1;
      });
      filteredCapabilities.map((val, key) => {
        const videoCapability = val.getIn(['NEW_MESSAGE', 'VIDEO', 'videoCapabilityDTO'], ImmutableMap());
        const maxVideoSize: number = videoCapability.get('maxFileSize', Number.MAX_SAFE_INTEGER);
        const minVideoDuration: number = videoCapability.get('minDurationMilliseconds', 0);
        const maxVideoDuration: number = videoCapability.get('maxDurationMilliseconds', Number.MAX_SAFE_INTEGER);
        const useTranscodedVideo: bool = videoCapability.get('transcodedVideoUsedForPublishing', false);

        if (duration > maxVideoDuration) {
          mediaDurationLongErrorsNetworks.push(toTitleCase(key));
        }
        if (duration > 0 && duration < minVideoDuration) {
          mediaDurationShortErrorsNetworks.push(toTitleCase(key));
        }
        const filesizeToCompare = useTranscodedVideo ? allVideoTranscodedFilesizes[key] : allVideoFileSizes[key];
        if (filesizeToCompare && filesizeToCompare > maxVideoSize) {
          mediaSizeErrorsNetworks.push(toTitleCase(key));
        }
      });

      if (mediaDurationLongErrorsNetworks.length) {
        return new PublishingError({
          title: mediaErrorText.videoDurationErrorTitle,
          description: `${mediaErrorText.videoDurationTooLongDescription} for ${mediaDurationLongErrorsNetworks.join(', ')}`,
          networks: ImmutableList(mediaDurationLongErrorsNetworks),
          dismissable: false
        });
      } else if (mediaDurationShortErrorsNetworks.length) {
        return new PublishingError({
          title: mediaErrorText.videoDurationErrorTitle,
          description: `${mediaErrorText.videoDurationTooShortDescription} for ${mediaDurationShortErrorsNetworks.join(', ')}`,
          networks: ImmutableList(mediaDurationShortErrorsNetworks),
          dismissable: false
        });
      } else if (mediaSizeErrorsNetworks.length) {
        return new PublishingError({
          title: mediaErrorText.videoSizeTooLargeTitle,
          description: `${mediaErrorText.mediaSizeTooLargeDescription} for ${mediaSizeErrorsNetworks.join(', ')}`,
          networks: ImmutableList(mediaSizeErrorsNetworks),
          dismissable: false
        });
      }
    } else if (videoError) { // captures server errors and prints them to the user
      return new PublishingError({
        title: mediaErrorText.videoUploadError,
        description: videoError,
        networks: ImmutableList(networks),
        dismissable: false
      });
    } else if (customThumbnailError) {
      return new PublishingError({
        title: mediaErrorText.videoCustomThumbUploadError,
        description: customThumbnailError,
        networks: ImmutableList(networks)
      });
    }
  }
);

export const asyncActionOccurring = createSelector(
  [imageUploadStatus,
    videoUploadStatus,
    videoTranscodeStatus,
    customVideoThumbnailUploadingStatus,
    linkPreviewCurrentlyScraping,
    linkPreviewAlreadyUploadingMedia,
    savingNote],
  (imageUploadStatus: UploadingStatusType,
    videoTranscodeStatus: string,
    videoUploadStatus: string,
    customVideoThumbnailUploadingStatus: string,
    linkPreviewCurrentlyScraping:bool,
    linkPreviewAlreadyUploadingMedia:bool,
    savingNote: bool):bool => {

    return imageUploadStatus === UploadingStatus.UPLOADING ||
      videoUploadStatus === RequestStatus.STATUS_REQUESTED ||
      videoTranscodeStatus === RequestStatus.STATUS_REQUESTED ||
      customVideoThumbnailUploadingStatus === RequestStatus.STATUS_REQUESTED ||
      linkPreviewCurrentlyScraping ||
      linkPreviewAlreadyUploadingMedia ||
      savingNote;
  }
);

export const messageHasPublishableContent = createSelector(
  [messageEditorStateMap,
    canShowLinkPreview,
    allSelectedNetworksSupportLinkPreview,
    hasImageBeenRequested,
    hasVideoBeenRequested],
    (messages,
    canShowLinkPreview,
    allSelectedNetworksSupportLinkPreview,
    hasImageBeenRequested,
    hasVideoBeenRequested) => {
      messages = messages.toJSON();

      const messageHasText = _.every(Object.values(messages), (messageEditorState:EditorState) => {
        return messageEditorState.getCurrentContent().hasText();
      });

      return messageHasText || (canShowLinkPreview && allSelectedNetworksSupportLinkPreview) || hasImageBeenRequested || hasVideoBeenRequested;
    }
);

export const messageCanPublishSelector = createSelector(
  [
    messageCurrentlySavingSelector,
    messageHasDestination,
    topLevelValidation,
    messageHasPublishableContent,
    scheduledDateTimeSelectorIsValid,
    imageFilesizeError,
    videoError,
    linkPreviewCustomThumbError,
    hasEditAccess,
    getLabelErrorGroup,
    asyncActionOccurring,
    hasFacebookError,
    linkTagsAreBeenSaved,
    linkTagsReadyToBeSaved
  ],
  (
    saving,
    hasDestination,
    validation,
    publishableContent,
    dateError,
    imageFilesizeError,
    videoError,
    linkPreviewCustomThumbError,
    hasEditAccess,
    labelErrorGroup,
    asyncActionOccurring,
    hasFacebookError,
    linkTagsAreBeenSaved,
    linkTagsReadyToBeSaved
    ) => (
      !saving &&
      validation.type !== ValidationType.VALIDATION_TYPE_ERROR &&
      publishableContent &&
      hasDestination &&
      !dateError &&
      !imageFilesizeError &&
      !videoError &&
      !linkPreviewCustomThumbError &&
      hasEditAccess &&
      labelErrorGroup.isEmpty() &&
      !asyncActionOccurring &&
      !hasFacebookError &&
      linkTagsAreBeenSaved.isEmpty() &&
      linkTagsReadyToBeSaved.isEmpty()
  )
);

export const messageCanSaveDraftSelector = createSelector(
  [
    messageCurrentlySavingSelector,
    scheduledDateTimeSelectorIsValid,
    hasEditAccess,
    asyncActionOccurring,
    getLabelErrorGroup,
    hasFacebookError,
    linkTagsAreBeenSaved
  ],
  (saving, dateError, hasEditAccess, asyncActionOccurring, labelErrorGroup, hasFacebookError, linkTagsAreBeenSaved) => {
    return (
      !saving &&
      !dateError &&
      hasEditAccess &&
      !asyncActionOccurring &&
      labelErrorGroup.isEmpty() &&
      !hasFacebookError &&
      linkTagsAreBeenSaved.isEmpty()
    );
  }
);
