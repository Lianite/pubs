/* @flow */
import { createSelector } from 'reselect';
import {
  contentLabelCreationRestricted,
  initiativeScopingLabels,
  userPermissions } from 'reducers/index';

export const canCreateLabel = createSelector(
  userPermissions,
  contentLabelCreationRestricted,
  initiativeScopingLabels,
  (privileges, onlyAdminCreationAllowed, noLabelCreationAllowed) => {
    let allowed = false;

    if (!noLabelCreationAllowed && !onlyAdminCreationAllowed) {
      allowed = true;
    }

    return allowed;
  }
);