/* @flow */
import { createSelector, createStructuredSelector } from 'reselect';
import {
    urlProperties,
    checkedCredentialsSelector,
    messageEditorStateText,
    getCurrentMentionsList
} from 'reducers/index';
import {
  attachmentTitle,
  attachmentCaption,
  attachmentLinkImage,
  attachmentDescription,
  attachmentLink,
  type,
  attachmentVideoUri,
  attachmentVideoThumbnail,
  attachmentImages
} from 'selectors/PostPreviewAttachmentsSelectors';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { Map as ImmutableMap } from 'immutable';
import { UrlPropertiesRecord } from 'records/LinksRecords';

import type { MentionsRecord } from 'types/MentionsTypes';

export type PostPreview = {|
  shortenedUrls: Array<string>,
  name?: string,
  message?: string,
  type: 'link' | 'image' | 'video' | 'none',
  attachment_title?: string,
  attachment_link?: string,
  attachment_caption?: string,
  attachment_link_image?: string,
  attachment_description?: string,
  attachment_video_thumbnail?: string,
  attachment_video_uri?: string,
  attachment_images?: Array<string>
|};

export const shortenedUrls = createSelector(
  urlProperties,
  (urlProperties: ImmutableMap<string, UrlPropertiesRecord>) => urlProperties.filter(urlProps => urlProps.shortenOnPublish).map((urlProps, url) => url).toArray()
);

export const mentions = createSelector(
  getCurrentMentionsList,
  (state, props = {}) => props.network,
  (mentions, network) => {
    const mentionsForNetwork = mentions.get(network, ImmutableMap());
    if (mentionsForNetwork.size) {
      return mentions.get(network).map((mention: MentionsRecord) => {
        return {
          displayName: network === SupportedNetworks.TWITTER ? mention.screenName : mention.displayName,
          offsetBegin: mention.offsetBegin,
          offsetEnd: mention.offsetEnd,
          link: mention.link
        };
      }).toArray();
    }

    return [];
  }
);

const facebookAccountPrefixes = [
  'Page: ',
  'Group: '
];

const accountName = (account: ImmutableMap<string, string>, network: string): string => {
  const isFacebook = network.toLowerCase() === SupportedNetworks.FACEBOOK.toLowerCase();
  let preparedName = account.get('name');
  if (isFacebook) {
    facebookAccountPrefixes.some((prefix) => {
      if (preparedName.startsWith(prefix)) {
        preparedName = preparedName.slice(prefix.length);
        return true;
      }
      return false;
    });
  }
  return preparedName;
};

export const name = createSelector(
  checkedCredentialsSelector,
  (state, props = {}) => props.network,
  (accounts, network) => {
    const firstAccount = accounts.filter(account => account.get('socialNetwork') === network).first();
    return firstAccount ? accountName(firstAccount, network) : '';
  }
);

export const message = createSelector(
  (state, props = {}) => messageEditorStateText(state, props.network),
  (message) => message
);

export default createStructuredSelector({
  urls: shortenedUrls,
  name,
  message,
  attachment_title: attachmentTitle,
  attachment_caption: attachmentCaption,
  attachment_link_image: attachmentLinkImage,
  attachment_description: attachmentDescription,
  attachment_link: attachmentLink,
  type,
  attachment_video_uri: attachmentVideoUri,
  attachment_video_thumbnail: attachmentVideoThumbnail,
  attachment_images: attachmentImages,
  mentions
});
