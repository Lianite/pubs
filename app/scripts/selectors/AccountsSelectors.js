/* @flow */
import { createSelector } from 'reselect';
import { CredentialRecord } from 'records/AccountsRecord';
import { AssigneeRecord } from 'records/AssigneeRecords';
import { checkedCredentialsSelector,
  voiceSelector,
  accountFilterText,
  getCurrentAccountChannelFilter,
  getCurrentAccountSelectionFilter,
  searchOnVoices,
  getAllAssignees,
  getCurrentAssignee
} from 'reducers/index';
import { VoiceFilteringConstants } from 'constants/ApplicationConstants';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import {canTarget as canFacebookTarget, canDarkPost } from 'selectors/FacebookSelector';
import {
	List as ImmutableList,
	Map as ImmutableMap,
  Set as ImmutableSet
} from 'immutable';
import _ from 'lodash';

import type { rootState } from 'reducers/index';

export const messageHasDestination = createSelector(
  checkedCredentialsSelector,
  (creds) => {
    return creds.size > 0;
  }
);

const validForTargetingState = (cred: CredentialRecord, { canFacebookTarget }): bool => {
  switch (cred.data.service) {
  case SupportedNetworks.FACEBOOK:
    return !canFacebookTarget || cred.data.authenticated && cred.data.targetingSupported;
  }
  return true;
};

export const getUnavailableVoices = createSelector(
  [voiceSelector, getCurrentAssignee, getAllAssignees],
  (voices: Array<Object>, currentAssignee: AssigneeRecord, allAssignees: ImmutableList<AssigneeRecord>) => {
    const assignee = allAssignees.find((assignee) => {
      return assignee.get('id') === currentAssignee.id;
    }) || currentAssignee;
    return _.reject(voices, (voice): bool => {
      return _.some(assignee.voices, (assigneeVoice): bool => {
        return voice.data.id === assigneeVoice.data.id;
      });
    });
  }
);

export const getAvailableVoices = createSelector(
  voiceSelector,
  getUnavailableVoices,
  (voices, unavailableVoices) => {
    return _.reject(voices, (voice) => {
      return _.some(unavailableVoices, (unavailableVoice) => {
        return unavailableVoice.data.id === voice.data.id;
      });
    });
  }
);

const validForDarkPostState = (cred: CredentialRecord, { canDarkPost }): bool => {
  switch (cred.data.service) {
  case SupportedNetworks.FACEBOOK:
    return !canDarkPost || cred.data.authenticated && cred.data.organicVisibilitySupported;
  }
  return true;
};

const getNumberOfUnfilteredCredentials = (credentials: Array<CredentialRecord>, canDarkPost: bool, canFacebookTarget: bool): ?number => {
  return credentials.filter((cred: CredentialRecord) =>
    validForTargetingState(cred, { canFacebookTarget }) && validForDarkPostState(cred, { canDarkPost })
  ).length;
};


export const getFirstNetworkCheckedCredentialUniqueId = (state: rootState, network: string) => {
  let creds = checkedCredentialsSelector(state);
  let chosenCredential = '';

  creds.forEach((credential) => {
    if (credential.socialNetwork === network) {
      chosenCredential = credential.uniqueId;
    }
  });

  return chosenCredential;
};

export const getFilteredCredentials = createSelector(
  getAvailableVoices,
  checkedCredentialsSelector,
  accountFilterText,
  getCurrentAccountChannelFilter,
  getCurrentAccountSelectionFilter,
  canFacebookTarget,
  canDarkPost,
  searchOnVoices,
  (voices, checkedCredentials, accountTextFilter, accountChannelFilter, accountSelectionFilter, canFacebookTarget, canDarkPost, searchOnVoices) => {
    let returnVoiceArray = [];
    voices.forEach(voice => {

      let voiceCredentials = [];

      voice.data.credentials.forEach(cred => {
        let filterCriteria = true;

        if (!searchOnVoices) {
          filterCriteria = filterCriteria && (cred.data.name.toLowerCase().indexOf(accountTextFilter.toLowerCase()) > -1) &&
              (!accountChannelFilter.size || accountChannelFilter.includes(cred.data.service));
        } else {
          filterCriteria = filterCriteria && (voice.data.name.toLowerCase().indexOf(accountTextFilter.toLowerCase()) > -1) &&
              (!accountChannelFilter.size || accountChannelFilter.includes(cred.data.service));
        }

        if (accountSelectionFilter === VoiceFilteringConstants.FILTER_UNSELECTED) {
          filterCriteria = filterCriteria && (checkedCredentials.keySeq().toArray().indexOf(cred.data.uniqueId) === -1);
        } else if (accountSelectionFilter === VoiceFilteringConstants.FILTER_SELECTED) {
          filterCriteria = filterCriteria && (checkedCredentials.keySeq().toArray().indexOf(cred.data.uniqueId) > -1);
        }

        if (filterCriteria) {
          voiceCredentials.push({
            id: cred.data.id,
            name: cred.data.name,
            service: cred.data.service,
            uniqueId: cred.data.uniqueId,
            accountType: cred.data.accountType,
            socialNetworkId: cred.data.socialNetworkId,
            targetingSupported: cred.data.targetingSupported,
            authenticated: cred.data.authenticated,
            organicVisibilitySupported: cred.data.organicVisibilitySupported
          });
        }
      });

      if (voiceCredentials.length) {
        returnVoiceArray.push({
          id: voice.data.id,
          name: voice.data.name,
          credentials: voiceCredentials,
          unfilteredCredentialCount: getNumberOfUnfilteredCredentials(voice.data.credentials, canDarkPost, canFacebookTarget)
        });
      }
    });

    return returnVoiceArray;
  }
);

export const getTotalFilteredCredentialCount = createSelector(
  getFilteredCredentials,
  (filteredCredentials) => {
    let totalFilteredCredentials = 0;

    filteredCredentials.forEach(voice => {
      voice.credentials.forEach(cred => {
        totalFilteredCredentials++;
      });
    });

    return totalFilteredCredentials;
  }
);

export const getTotalCredentialCount = createSelector(
  getAvailableVoices,
  (voices) => {
    let totalCredentials = 0;

    voices.forEach(voice => {
      voice.data.credentials.forEach(cred => {
        totalCredentials++;
      });
    });

    return totalCredentials;
  }
);

//This is going to return a similar structure as _selectedNetworks. However,
// _selectedNetworks is much faster, as it only has to iterate over each network.
// selectedNetworksFromVoices is meant to be used when the credential list changes
// in order to update the messageEditorStateMap. In this case, we will have to
// iterate over all the selected credentials.

export const selectedNetworksFromVoices = createSelector(
  checkedCredentialsSelector,
  (checkedCredentials: ImmutableMap<string, CredentialRecord>) => {
    let setOfNetworks: ImmutableSet<string> = ImmutableSet();
    checkedCredentials.forEach((value: CredentialRecord) => {
      setOfNetworks = setOfNetworks.add(value.socialNetwork);
    });

    return setOfNetworks.toArray();
  });

export const checkedVoiceTree = createSelector(
  [ checkedCredentialsSelector, voiceSelector ],
  (checkedTree, voices) => {
    let tree = {};

    voices.forEach(voice => {
      voice.data.credentials.forEach(cred => {

        if (checkedTree.get(cred.data.uniqueId)) {
          if (!tree[voice.data.id]) {
            tree[voice.data.id] = {};
          }
          tree[voice.data.id][cred.data.uniqueId] = checkedTree.get(cred.data.uniqueId).id;
        }
      });
    });

    return tree;
  }
);

export const getFilteredVoiceIds = createSelector(
  checkedVoiceTree,
  (voiceTree): Array<string> => {
    return _.keys(voiceTree);
  }
);

export const totalCheckedCredentialsInCurrentView = createSelector(
  checkedCredentialsSelector,
  (checkedCredentials) => {
    return checkedCredentials.size;
  }
);
