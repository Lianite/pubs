var childProcess = require('child_process');
var fs = require('fs');
var path = require('path');

module.exports.fetch = function (cb) {
  try {
    childProcess.execSync('./generate.sh', {
      cwd: __dirname
    });
  } catch (e) {
    return cb(e);
  }

  cb(null, fs.readFileSync(path.join(__dirname, './server.key')), fs.readFileSync(path.join(__dirname, './server.pem')));
};
