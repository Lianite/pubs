#!/bin/bash

if [ ! -f ./ca.pem ]; then
  openssl rand 200 | openssl dgst -sha1 > ca.pass
  openssl genrsa -aes256 -passout file:ca.pass -out ca.key 4096
  openssl req -config ./openssl.conf -extensions v3_ca -passin file:ca.pass -new -x509 -days 7300 -sha256 -nodes -subj "/C=US/ST=Texas/L=Austin/O=Spredfast/OU=Engineering/CN=SF CA Cert/" -key ca.key -out ca.pem
fi

if [ ! -f ./server.pem ]; then
  openssl genrsa -out server.key 2048
  openssl req -config ./openssl.conf -reqexts server_cert -sha256 -newkey rsa:2048 -nodes -subj "/C=US/ST=Texas/L=Austin/O=Spredfast/OU=Engineering/CN=SF Localhost Cert/" -key server.key -keyout server.pem -out server.csr
  openssl x509 -extfile ./openssl.conf -extensions server_cert -passin file:ca.pass -sha256 -req -days 3650 -in server.csr -CA ca.pem -CAkey ca.key -set_serial 01 -out server.pem
  rm server.csr
fi

security verify-cert -c server.pem &> /dev/null

if [ $? -ne 0 ]; then
  echo "Time to add the cert!"
  LOGIN_KEYCHAIN="$(echo -e "$(security login-keychain)" | tr -d '[[:space:]"]')"
  sudo security add-trusted-cert -d -r trustRoot -k $LOGIN_KEYCHAIN ca.pem
fi
