# Spredfast Publishing UI

This repo contains the source-code for Spredfast's multichannel publishing application. This application is a self-hosted app that will can be pulled into applications via `sf-intents` (WIP) and can be accessed at the following domains (WIP):
* publishing.spredfast.com
* publishing-qa.spredfast.com

[![Build Status: CircleCI](https://circleci.com/gh/spredfast/publishing-ui/tree/master.svg?style=shield&circle-token=20de77cd5ef52e4b8f1487115696d3f95c2ca17f)](https://circleci.com/gh/spredfast/publishing-ui/tree/master) [![Coverage Status](https://coveralls.io/repos/github/spredfast/publishing-ui/badge.svg?branch=master&t=pM213t)](https://coveralls.io/github/spredfast/publishing-ui?branch=master)

### Getting started

Currently we are using the `master` branch as a production snapshot, while `development` is our sprint integration branch which houses QA approved tasks. If you're making a new branch for a feature that is not a hotfix, you should most likely **branch off of and merge into development**. At the end of a sprint or at a determined release interval, we will make a release-candidate for QA to regression test, and then merge that candidate into master for a production deployment.

This application uses a react/redux ecosystem on the client side, and uses Shamon for its server side.

We also use `yarn` instead of `npm`, since `yarn` is much better about deterministically recreating dependency trees. However, if you've never used `yarn` before, you'll need to install it. For Mac OS X, it's fairly easy if you have Homebrew:

`brew update`

`brew install yarn`

See https://yarnpkg.com/en/docs/install for more yarn installation details.

* run `yarn install` to install all dependencies
* open a local dev server:
  * Add the following entry to your `/etc/hosts`:
  ```sh
  127.0.0.1       publishing-local.spredfast.com
  ```
  * run `yarn run dev` (most likely you'll run this) or `yarn run dev:<env>` where `<env>` will be either `qa`, or `prod`. This determines where the login service will auth you from when opening the sever.
    * **NOTE:** If you get an error about `ETIMEDOUT` when starting a dev server, just rerun the `yarn run dev` command.

* run tests in dev and prod environments:
  * to run **all** tests that CircleCI will use to determine build success, run `yarn run quality`
  * to lint the application, run `yarn run lint`, any failures throw by eslint will break the build in CircleCI
  * to check flow annotations, run `yarn run lint:flow`, any failures throw by flow will break the build in CircleCI *NOTE:* at the moment we only care about files that have been annotated in our linting, but we will eventually enforce flowchecking on all app files
  * to continually run tests while development and debugging, run `yarn run test:dev`
  * to run tests just once against the prod build, run `yarn test`, any failures thrown by failed tests will break the build in CircleCI

**Note:** After push your changes CircleCI is running all the tests and evaluating the linting of the code base files.
The build will fail if some test or lint rule fails. You can enable execute `yarn run quality` on pre-commit or on pre-push.

* `yarn run precommit:install` executes all the tests and linters before every commit
* `yarn run prepush:install` executes all the tests and linters before every push

If you want to remove the hooks you have to execute `yarn run precommit:remove` or `yarn run prepush:remove` depending of what you installed before.

### Publishing the app

* CircleCI will automatically pickup deployments for static content to S3 on multiple patterns:
  * creating a PR: `pr-<branch name>-<version number>`
  * a green push to master:  `master-<version number>`
  * a tag with the name `release-.*`: `production-<version number>`

**NOTE:** if you wish to tag the release for a prod deployment of the client, execute the following via terminal:
```
gulp deploy-client:prod
```
This will create a tag and push it to git, which will then be picked up by CircleCI.

* CircleCI will also automatically pickup server deployments on multiple patterns:
  * a tag with the name `server-prod.*`: deploy server and `index.hbs` to publishing.spredfast.com
  * a tag with the name `server-infra.*`: deploy server and `index.hbs` to publishing-qa.spredfast.com

* You can also ad-hoc publish static content to S3 using `gulp deploy --target<env>`

**NOTE:** If you wish to tag the release of the server for either prod or infra, run the following via the terminal:
```
gulp server --env=<infra / prod>
```
This will create a tag and push it to git, which will then be picked up by CircleCI.

### Coverage/Coveralls

We use coverage metrics in this library, coverage will fail if submitting a PR with < 90% coverage, or if coverage % has dropped 2% from what's it's being compared to in the PR.
