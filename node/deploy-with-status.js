/* eslint-disable no-process-env */
const shell = require('shelljs');
const github = require('octonode');

const client = github.client('c9d712c60b4a9dcbcc1143a53d2a34991d9cbf69');
const ciConfig = require('../config/ciConfig');

module.exports = function(env, cb) {
  const sha = process.env.CIRCLE_SHA1;

  if (ciConfig.githubRepo && ciConfig.publicAppUrl) {
    const repo = client.repo(ciConfig.githubRepo);

    repo.status(sha, {
      state: 'pending',
      target_url: `https://circleci.com/gh/${ciConfig.githubRepo}/${process.env.CIRCLE_BUILD_NUM}`,
      description: 'pending',
      context: 'al'
    }, function() {
      shell.exec(`gulp deploy --target=${env}`, function(code) {
        if (code !== 0) {
          repo.status(sha, {
            state: 'failure',
            target_url: `https://circleci.com/gh/${ciConfig.githubRepo}/${process.env.CIRCLE_BUILD_NUM}`,
            description: 'failure',
            context: 'al'
          }, function() {});
          return;
        }

        const target_url = env === 'production' || env === 'master' ?
          `${ciConfig.publicAppUrl}` :
          `${ciConfig.publicQaAppUrl}?env=${env}`;

        repo.status(sha, {
          state: 'success',
          target_url,
          description: 'success',
          context: 'al'
        }, function() {
          if (cb) {
            cb();
          }
        });
      });
    });
  }
};
