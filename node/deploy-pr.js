/* eslint-disable no-process-env */
/* eslint-disable no-console */
if (!process.env.CI_PULL_REQUEST || process.env.CIRCLE_BRANCH === 'master') {
  console.log('No process.env.CI_PULL_REQUEST or CIRCLE_BRANCH is master');
  console.log('process.env.CI_PULL_REQUEST: ', process.env.CI_PULL_REQUEST);
  console.log('process.env.CIRCLE_BRANCH: ', process.env.CIRCLE_BRANCH);

  process.exit(0);
}

const github = require('octonode');
const kebabCase = require('lodash/kebabCase');
const Promise = require('bluebird');
const deploy = require('./deploy-with-status');
const ciConfig = require('../config/ciConfig');

const client = github.client('c9d712c60b4a9dcbcc1143a53d2a34991d9cbf69');
const repo = ciConfig.githubRepo;

// using this until there's a callback added to issue.deleteComment
function deleteComment(id) {
  return new Promise(function(resolve, reject) {
    client.del(`/repos/${repo}/issues/comments/${id}`, {}, function(err, s, b, h) {
      if (err) {
        return reject(err);
      }
      if (s !== 204) {
        return resolve(new Error('Issue deleteComment error'));
      } else {
        return resolve(null, b, h);
      }
    });
  });
}

if (repo) {
  const pullRequestNum = process.env.CI_PULL_REQUEST.match(/\/pull\/(\d+)/)[1];
  const branch = process.env.CIRCLE_BRANCH;
  const env = `pr-${kebabCase(branch)}`;

  deploy(env, function() {
    const issue = client.issue(repo, pullRequestNum);
    issue.comments(function(err, comments) {
      const deletePromises = [];

      if (comments) {
        comments.forEach(function(comment) {
          if (comment.user.login === 'ExintEng') {
            deletePromises.push(deleteComment(comment.id));
          }
        });
      }

      Promise.all(deletePromises).then(function() {
        issue.createComment({
          body: `Environment created at: ${ciConfig.publicQaAppUrl}?env=${env}`
        }, function() {

        });
      });
    });
  });
}
