import Promise from 'bluebird';
/* eslint-disable no-unused-vars */
import jasmineAjax from 'jasmine-ajax';
/* eslint-enable no-unused-vars */
import * as immutableMatchers from 'tests/test-utils/jasmine-matchers/ImmutableMatchers';
import * as editorStateMatchers from 'tests/test-utils/jasmine-matchers/EditorStateMatchers';
import * as messageMatchers from 'tests/test-utils/jasmine-matchers/MessageMatchers';

import { initSegmentService, identify } from '@spredfast/sf-segment-js';
import { EnvironmentConstants } from 'constants/ApplicationConstants';
import { setTestConvDetails } from 'tests/test-utils/EnvironmentTestUtils';

Promise.config({ cancellation: true });
window.Promise = Promise;

setTestConvDetails();

/**
 * Here we do a fake initSegmentService so that we can hit segment
 * calls without reporting or coming into js error complaining about
 * tracking without setting up segment first.
 */
initSegmentService({
  environment: `https://${EnvironmentConstants.TEST_CONV_HOSTNAME}`,
  product: 'Multichannel Testing',
  segmentKey: 'notARealSegmentAPIKey12345'
});
identify({
  userDirectoryId: 123456789,
  companyDirectoryId: 123456789,
  companyId: 2,
  companyName: 'Spredfast Test Company',
  email: 'notarealemail@spredfast.com',
  firstName: 'Test',
  lastName: 'User'
});

beforeEach(() => {
  jasmine.addMatchers(immutableMatchers);
  jasmine.addMatchers(editorStateMatchers);
  jasmine.addMatchers(messageMatchers);
  jasmine.Ajax.install();
});

afterEach(() => {
  jasmine.Ajax.uninstall();
});

const testUtilsContext = require.context('../tests/test-utils/', true, /^\.\/.*\.js$/);
testUtilsContext.keys().forEach(testUtilsContext);
const apiResponsesTestsContext = require.context('../tests/api-responses/', true, /^\.\/.*\.js$/);
apiResponsesTestsContext.keys().forEach(apiResponsesTestsContext);

const testsContext = require.context('.', true, /Tests$/);
testsContext.keys().forEach(testsContext);

const sourceContext = require.context('../app/scripts/', true, /^\.\/.*\/.*\.js$/);
sourceContext.keys().forEach(sourceContext);
