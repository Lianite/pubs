/* @flow */
import {
  createNotesPayload,
  createNoteFromPayload
} from 'adapters/NotesAdapter';
import mockState from 'tests/test-utils/MockPublishingState';
import { List as ImmutableList } from 'immutable';

import type { Note } from 'adapters/types';

describe('Notes Adapter', () => {
  let state = mockState({
    notes: {
      notesList: ImmutableList([
        {
          authorFirstName: 'test',
          authorLastName: 'person',
          noteCreationTimestamp: 1234,
          noteText: 'hello',
          authorAvatarThumbnail: 'image.org'
        },
        {
          authorFirstName: 'example',
          authorLastName: 'user',
          noteCreationTimestamp: 5432,
          noteText: 'goodbye',
          authorAvatarThumbnail: 'test.com'
        }
      ])
    }
  })();

  let notePayload: Note = {
    noteText: 'test text',
    authorFirstName: 'fake',
    authorLastName: 'user',
    authorAvatarThumbnail: 'http://avatar.com',
    noteCreationTimestamp: 1234
  };

  it('should create a note object for redux from server payload', () => {
    expect(createNoteFromPayload(notePayload)).toDeepEqual({
      noteText: 'test text',
      authorFirstName: 'fake',
      authorLastName: 'user',
      authorAvatarThumbnail: 'http://avatar.com',
      noteCreationTimestamp: 1234
    });
  });

  it('should strip out the default author avatar from server payload', () => {
    expect(createNoteFromPayload({
      ...notePayload,
      authorAvatarThumbnail: '/images/default-avatar.jpg'
    })).toDeepEqual({
      noteText: 'test text',
      authorFirstName: 'fake',
      authorLastName: 'user',
      authorAvatarThumbnail: '',
      noteCreationTimestamp: 1234
    });
  });

  it('should create a server payload from a list of notes', () => {
    expect(createNotesPayload(state.notes.notesList.toJS())).toDeepEqual([
      {
        noteText: 'hello',
        noteCreationTimestamp: 1234
      },
      {
        noteText: 'goodbye',
        noteCreationTimestamp: 5432
      }
    ]);
  });
});
