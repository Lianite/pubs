/* @flow */
import * as messageAdapter from 'adapters/MessageAdapter';
import { EditorState, ContentState } from 'draft-js';
import { MessageConstants } from 'constants/ApplicationConstants';
import { ScheduledDatetimeRecord } from 'reducers/MessageReducer';
import { CredentialRecord } from 'records/AccountsRecord';
import { SocialVideoRecord } from 'records/VideosRecords';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import { createUnixTimestamp } from 'utils/DateUtils';
import moment from 'moment';
import { mockMessageResponse } from 'tests/api-responses/MessageApiResponses';
import mockState from 'tests/test-utils/MockPublishingState';
import { DefaultNetwork, SupportedNetworks } from 'constants/ApplicationConstants';
import { MentionsRecord } from 'types/MentionsTypes.js';
import { FacebookState } from 'records/FacebookRecords';
import {
  TargetingState,
  LanguagesState,
  LanguageState,
  LocationsState,
  LocationState,
  GenderState
} from 'records/TargetingRecords';
import {
  is,
  fromJS,
  List as ImmutableList,
  Map as ImmutableMap,
  Set as ImmutableSet
} from 'immutable';
import type { MessageObject } from 'adapters/types';
import {
  savedLinkTag,
  unsavedLinkTag } from 'tests/selectors/LinkTagsSelectorTests';

describe('Message Adapter', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      router: {},
      environment: {
        userId: 3,
        userTimezone: new TimezoneRecord(),
        capabilities: fromJS({
          FACEBOOK: {
            NEW_MESSAGE: {
              NOTE: {
                linkPreview: {
                  supported: true
                }
              }
            }
          },
          TWITTER: {
            NEW_MESSAGE: {
              NOTE: {
                linkPreview: {
                  supported: false
                }
              }
            }
          }
        })
      },
      uiState: {sidebarOpen: true},
      plan:{
        currentPlan: fromJS({
          name: '',
          id: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
          color: ''
        })
      },
      accounts: {
        checkedCredentials: ImmutableMap({
          '4': new CredentialRecord({
            id: 4,
            socialCredentialType: 'PAGE',
            socialNetwork: 'FACEBOOK',
            name: 'Page: Planet Express Care',
            uniqueId: 'b4e23029d2329ed985f27f9b7be94959',
            socialNetworkId: '235463903175221'
          }),
          '7': new CredentialRecord({
            id: 7,
            socialCredentialType: 'PAGE',
            socialNetwork: 'TWITTER',
            name: 'Page: Concert Tour of planet express',
            uniqueId: '9aaffbc91c08be39db7f7b82e6938e97',
            socialNetworkId: '369769786370875'
          }),
          '42': new CredentialRecord({
            id: 42,
            socialCredentialType: 'USER',
            socialNetwork: 'FACEBOOK',
            name: 'Phillip Fry',
            uniqueId: '6d7a5b7880bc0e72036185aa075506e9',
            socialNetworkId: '100002998125245'
          })
        })
      },
      images: {
        imageId: 1,
        imageFullUrl: 'sf-mock-imageurl.com',
        imageThumbUrl: 'sf-mock-imageurl-thumb.com'
      },
      message: {
        eventId: null,
        messageStatus: MessageConstants.PENDING,
        messageTitle: 'My message Title',
        messageEditorStateMap: ImmutableMap({
          'FACEBOOK': EditorState.createWithContent(ContentState.createFromText('Facebook message text')),
          'TWITTER': EditorState.createWithContent(ContentState.createFromText('Twitter message text'))
        }),
        scheduledDatetime: new ScheduledDatetimeRecord({
          scheduledDatetime: moment.unix(670896)
        })
      },
      media: {
        mediaId: 1,
        mediaFullUrl: 'sf-mock-imageurl.com',
        mediaThumbUrl: 'sf-mock-imageurl-thumb.com'
      },
      videos: {
        video: new SocialVideoRecord()
      },
      linkPreview: {
        linkUrl: 'www.nfl.com',
        thumbnailUrl: 'http://i.nflcdn.com/static/content/public/static/img/share/shield.jpg',
        description: 'The official source for NFL news, video highlights, fantasy football, game-day coverage, schedules, stats, scores and more.',
        caption: 'www.nfl.com',
        title: 'NFL.com - Official Site of the National Football League',
        videoSrc: '',
        videoLink: ''
      },
      links: {
        urlProperties: ImmutableMap({
          'reddit.com': new UrlPropertiesRecord({
            shortenOnPublish: true,
            addLinkTags: false,
            associatedNetworks: ImmutableSet(['TWITTER'])
          }),
          'google.com': new UrlPropertiesRecord({
            shortenOnPublish: false,
            addLinkTags: true,
            associatedNetworks: ImmutableSet(['TWITTER', 'FACEBOOK'])
          }),
          'www.nfl.com': new UrlPropertiesRecord({
            shortenOnPublish: false,
            addLinkTags: false,
            associatedNetworks: ImmutableSet([])
          })
        }),
        urls: ImmutableMap({
          'FACEBOOK': ImmutableList(['google.com']),
          'TWITTER': ImmutableList(['reddit.com'])
        })
      },
      linkTags: fromJS({
        urls: {
          'google.com': {
            [SupportedNetworks.FACEBOOK]: savedLinkTag,
            [SupportedNetworks.TWITTER]: unsavedLinkTag
          }
        }
      }),
      assignee: {
        firstName: 'Bob',
        lastName: 'Kelso',
        id: 1,
        conflictingVoices: [],
        voices: [],
        invalid: false
      },
      mentions: {
        listOfMentions: ImmutableMap({
          [SupportedNetworks.FACEBOOK]: ImmutableList([
            new MentionsRecord({
              'profileId': '7177913734',
              'screenName': 'testScreenName',
              'displayName': 'Reddit',
              'link': 'https://www.facebook.com/reddit/',
              'icon': 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
              'likes': 1026470,
              'category': 'News/Media Website',
              'location': 'Redditch, United Kingdom',
              'verified': true,
              'offsetKey': 'aaaaa-0-0'
            })
          ]),
          [SupportedNetworks.TWITTER]: ImmutableList([
            new MentionsRecord({
              'profileId': '7177913734',
              'screenName': 'testScreenName',
              'displayName': 'Reddit',
              'link': 'https://www.facebook.com/reddit/',
              'icon': 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
              'likes': 1026470,
              'category': 'News/Media Website',
              'location': 'Redditch, United Kingdom',
              'verified': true,
              'offsetKey': 'aaaaa-0-0'
            })
          ])
        })
      },
      notes: {
        notesList: ImmutableList([{
          authorFirstName: 'test',
          authorLastName: 'person',
          noteCreationTimestamp: 1234,
          noteText: 'hello'
        }])
      },
      facebook: new FacebookState({
        targeting: new TargetingState({
          language: new LanguagesState({
            selectedLanguages: ImmutableList([
              new LanguageState({
                value: '1',
                description: 'English'
              }),
              new LanguageState({
                value: '2',
                description: 'Chinese'
              })
            ])
          }),
          location: new LocationsState({
            includedLocations: ImmutableList([
              new LocationState({
                value: 'US',
                description: 'United States',
                subType: 'COUNTRY'
              })
            ])
          }),
          gender: new GenderState({
            value: '1',
            description: 'Male'
          })
        })
      })
    })();
  });

  it('Create a message payload for server from redux state', () => {
    let expectedOutputObject: MessageObject = {
      planId: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
      status: MessageConstants.PENDING,
      spredfastMessageTitle: 'My message Title',
      assignee: {
        firstName: '',
        lastName: '',
        id: 0,
        conflictingVoices: [],
        voices: [],
        invalid: false
      },
      singleChannelMessages:[
        {
          messageContent: {
            text: 'Facebook message text',
            links: [
              'google.com'
            ],
            imageIDs: [1],
            linkPreview: {
              linkUrl: 'www.nfl.com',
              description: 'The official source for NFL news, video highlights, fantasy football, game-day coverage, schedules, stats, scores and more.',
              caption: 'www.nfl.com',
              title: 'NFL.com - Official Site of the National Football League',
              thumbnailUrl: 'http://i.nflcdn.com/static/content/public/static/img/share/shield.jpg',
              videoSrc: '',
              videoLink: ''
            },
            linkDTOs: [{
              fullUrl: 'google.com',
              shortenOnPublish: false,
              addLinkTags: true,
              tagId: 2
            }, {
              fullUrl: 'www.nfl.com',
              shortenOnPublish: false,
              addLinkTags: false
            }],
            video: null,
            targetingProfile: {
              service: 'FACEBOOK',
              type: 'GENERAL',
              targetingValues: {
                LOCALE: [{
                  value: '1',
                  description: 'English',
                  type: 'LOCALE'
                }, {
                  value: '2',
                  description: 'Chinese',
                  type: 'LOCALE'
                }],
                'GEOGRAPHY': [{
                  value: 'US',
                  description: 'United States',
                  type: 'GEOGRAPHY',
                  subType: 'COUNTRY',
                  parentTargetingValue: undefined
                }],
                GENDER: [{
                  value: '1',
                  description: 'Male',
                  type: 'GENDER'
                }]
              }
            }
          },
          socialNetwork: 'FACEBOOK',
          socialNetworkAccounts: [
            {
              id: 4,
              name: 'Page: Planet Express Care',
              socialCredentialType: 'PAGE',
              socialNetworkId: '235463903175221',
              uniqueId: 'b4e23029d2329ed985f27f9b7be94959'
            },
            {
              id: 42,
              name: 'Phillip Fry',
              socialCredentialType: 'USER',
              socialNetworkId: '100002998125245',
              uniqueId: '6d7a5b7880bc0e72036185aa075506e9'
            }
          ],
          organicVisibility: 'VISIBLE'
        },
        {
          messageContent: {
            text: 'Twitter message text',
            links: [
              'reddit.com',
              'google.com'
            ],
            linkDTOs: [{
              fullUrl: 'reddit.com',
              shortenOnPublish: true,
              addLinkTags: false
            }, {
              fullUrl: 'google.com',
              shortenOnPublish: false,
              addLinkTags: true
            }],
            imageIDs: [1],
            linkPreview: null,
            video: null
          },
          socialNetwork: 'TWITTER',
          socialNetworkAccounts: [
            {
              id: 7,
              name: 'Page: Concert Tour of planet express',
              socialCredentialType: 'PAGE',
              socialNetworkId: '369769786370875',
              uniqueId: '9aaffbc91c08be39db7f7b82e6938e97'
            }
          ],
          organicVisibility: 'VISIBLE'
        }
      ],
      schedule: {
        immediatePublish: false,
        scheduledPublishDate: createUnixTimestamp(moment.unix(670896), 0)
      },
      linksByUrl: {
        'reddit.com': {
          fullUrl: 'reddit.com',
          shortenOnPublish: true,
          addLinkTags: false
        },
        'google.com': {
          fullUrl: 'google.com',
          shortenOnPublish: false,
          addLinkTags: true
        },
        'www.nfl.com': {
          fullUrl: 'www.nfl.com',
          shortenOnPublish: false,
          addLinkTags: false
        }
      },
      contentLabels: [],
      mentions: [
        { displayName: 'Reddit', link: 'https://www.facebook.com/reddit/', offsetBegin: -1, offsetEnd: -1, profileId: '7177913734', service: 'FACEBOOK' },
        { displayName: '@testScreenName', link: 'https://www.facebook.com/reddit/', offsetBegin: -1, offsetEnd: -1, profileId: '7177913734', service: 'TWITTER' }
      ],
      notes: [{
        noteCreationTimestamp: 1234,
        noteText: 'hello'
      }]
    };

    let testoutput = messageAdapter.createMessageObject(state, MessageConstants.PENDING);
    expect(testoutput).toDeepEqual(expectedOutputObject);

    // Testing the exepcted adapted output of an edited message (message with an eventId)
    const editMessageState = state.message.set('eventId', 1);
    state.message = editMessageState;
    expectedOutputObject.eventId = 1;
    expectedOutputObject.notes = [];

    testoutput = messageAdapter.createMessageObject(state, MessageConstants.PENDING);
    expect(testoutput).toDeepEqual(expectedOutputObject);
  });

  it('should not include any notes in payload when message has already been persisted', () => {
    state.message = state.message.set('eventId', 123);

    let testoutput = messageAdapter.createMessageObject(state, MessageConstants.PENDING);
    expect(testoutput.notes).toDeepEqual([]);
  });

  it('reads videos as expected from a server message', () => {
    const companyId = '2';
    const initiativeId = '3';
    const messageStatus = MessageConstants.PENDING;

    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1,
      invalid: false
    };
    let messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, assignee, messageStatus});

    //add videos onto the payload
    messageObj.singleChannelMessages[0].messageContent.video = {
      id: 649,
      duration: 4000,
      size: 2000,
      transcodedSize: 1000,
      displayName: 'displayName',
      summary: null,
      videoThumbnail: {
        id: 'thumbCandidateID0',
        uri: 'thumbCandidateURI0'
      },
      videoURI: null,
      videoStatus: 'TRANSCODING_FINISHED',
      type: null,
      callToAction: {
        present: false
      },
      videoTitle: 'title',
      tags: null,
      category: null,
      targetService: 'FACEBOOK',
      thumbnailCandidates: [{
        id: 'thumbCandidateID0',
        uri: 'thumbCandidateURI0'
      },
      {
        id: 'thumbCandidateID1',
        uri: 'thumbCandidateURI1'
      }],
      videoPreview: 'http://path.to.video/preview/video.mp4',
      videoS3Key: 's3keyHere'
    };

    let sampleState = messageAdapter.createMessageStateFromPayload(messageObj, 0);

    expect(sampleState.videos.socialVideos.size).toBe(1);
    expect(sampleState.videos.videoKey).toBe('s3keyHere');
    expect(sampleState.videos.customThumbnailId).toBe('');
    expect(sampleState.videos.customThumbnailUri).toBe('');
    const videoFromPayload = sampleState.videos.socialVideos.first();
    expect(videoFromPayload.id).toBe(649);
    expect(videoFromPayload.displayName).toBe('displayName');
    expect(videoFromPayload.duration).toBe(4000);
    expect(videoFromPayload.videoSize).toBe(2000);
    expect(videoFromPayload.transcodedVideoSize).toBe(1000);
  });

  it('reads video with custom thumb as expected from a server message', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const messageStatus = MessageConstants.PENDING;
    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1,
      invalid: false
    };
    let messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, assignee, userId, messageStatus});

    //add videos onto the payload
    messageObj.singleChannelMessages[0].messageContent.video = {
      id: 649,
      duration: 4000,
      size: 2000,
      transcodedSize: 1000,
      displayName: 'displayName',
      summary: null,
      videoThumbnail: {
        id: 'customThumbId',
        uri: 'customThumbUri'
      },
      videoURI: null,
      videoStatus: 'TRANSCODING_FINISHED',
      type: null,
      callToAction: {
        present: false
      },
      videoTitle: 'title',
      tags: null,
      category: null,
      targetService: 'FACEBOOK',
      thumbnailCandidates: [{
        id: 'thumbCandidateID0',
        uri: 'thumbCandidateURI0'
      },
      {
        id: 'thumbCandidateID1',
        uri: 'thumbCandidateURI1'
      }],
      videoPreview: 'http://path.to.video/preview/video.mp4',
      videoS3Key: 's3keyHere'
    };

    let sampleState = messageAdapter.createMessageStateFromPayload(messageObj, 0);

    expect(sampleState.videos.socialVideos.size).toBe(1);
    expect(sampleState.videos.videoKey).toBe('s3keyHere');
    expect(sampleState.videos.customThumbnailId).toBe('customThumbId');
    expect(sampleState.videos.customThumbnailUri).toBe('customThumbUri');
    const videoFromPayload = sampleState.videos.socialVideos.first();
    expect(videoFromPayload.id).toBe(649);
    expect(videoFromPayload.displayName).toBe('displayName');
    expect(videoFromPayload.duration).toBe(4000);
    expect(videoFromPayload.videoSize).toBe(2000);
    expect(videoFromPayload.transcodedVideoSize).toBe(1000);
  });

  it('reads schedule dateTime from a server message', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const messageStatus = MessageConstants.PENDING;

    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1
    };
    let messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, assignee, userId, messageStatus});
    messageObj.schedule = {
      immediatePublish: false,
      scheduledPublishDate: 12345
    };

    const sampleState = messageAdapter.createMessageStateFromPayload(messageObj, 0);
    const scheduledMoment = sampleState.message.scheduledDatetime.scheduledDatetime;
    expect(createUnixTimestamp(scheduledMoment, 0)).toBe(12345);
  });

  it('ignores any time when immediatePublish is true from a server message', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const messageStatus = MessageConstants.PENDING;

    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1
    };
    let messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, assignee, userId, messageStatus});
    messageObj.schedule = {
      immediatePublish: true,
      scheduledPublishDate: 12345
    };

    const sampleState = messageAdapter.createMessageStateFromPayload(messageObj, 0);
    const scheduledMoment = sampleState.message.scheduledDatetime.scheduledDatetime;
    expect(scheduledMoment).toBe(null);
  });

  it('handles getting no schedule object from a server message', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const messageStatus = MessageConstants.PENDING;

    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1,
      voices: [],
      conflictingVoices: []
    };
    let messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, assignee, userId, messageStatus});
    delete messageObj.schedule;

    const sampleState = messageAdapter.createMessageStateFromPayload(messageObj, 0);
    const scheduledMoment = sampleState.message.scheduledDatetime.scheduledDatetime;
    expect(scheduledMoment).toBe(null);
  });

  it('handles getting back a link preview url not in a message text', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const messageStatus = MessageConstants.PENDING;

    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1
    };
    const messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, assignee, userId, messageStatus});
    const sampleState = messageAdapter.createMessageStateFromPayload(messageObj, 0);

    //urlProperties should have a default entry for the url
    expect(sampleState.urlProperties['www.nfl.com']).toBeTruthy(); //www.nfl.com is link preview for facebook
    expect(sampleState.urlProperties['www.nfl.com'].associatedNetworks.toArray()[0]).toBe(DefaultNetwork); //www.nfl.com is link preview for facebook
  });

  it('should not include a targeting profile for facebook when no targeting values have been selected', () => {
    state.facebook = new FacebookState();
    const payload = messageAdapter.createMessageObject(state, MessageConstants.PENDING);
    expect(payload.singleChannelMessages[0].messageContent.targetingProfile).toBe(undefined);
  });

  it('should not include a targeting profile for networks that do not support targeting', () => {
    const payload = messageAdapter.createMessageObject(state, MessageConstants.PENDING);
    expect(payload.singleChannelMessages[1].messageContent.targetingProfile).toBe(undefined);
  });

  it('should correctly set authenticated and targeting support state for checked credentials', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const assignee = { id: 1, firstName: 'a', lastName: 'b' };
    const messageStatus = MessageConstants.PENDING;
    const messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, userId, messageStatus, assignee});

    const messageState = messageAdapter.createMessageStateFromPayload(messageObj, 0);
    const checkedCredentials = messageState.checkedCredentials;

    expect(checkedCredentials.getIn(['99401e8b0f76e57d306d572b00a28a47', 'authenticated'])).toBe(true);
    expect(checkedCredentials.getIn(['99401e8b0f76e57d306d572b00a28a47', 'targetingSupported'])).toBe(true);
    expect(checkedCredentials.getIn(['8579c03719c01e46db822cce2c7605f9', 'authenticated'])).toBe(false);
    expect(checkedCredentials.getIn(['8579c03719c01e46db822cce2c7605f9', 'targetingSupported'])).toBe(false);
    expect(checkedCredentials.getIn(['3756d04ef7010042d1ab6d71917f6013', 'authenticated'])).toBe(true);
    expect(checkedCredentials.getIn(['3756d04ef7010042d1ab6d71917f6013', 'targetingSupported'])).toBe(false);
  });

  it('should correctly read in activity log information from payload', () => {
    const companyId = '2';
    const initiativeId = '3';
    const userId = 4;
    const assignee = { id: 1, firstName: 'a', lastName: 'b' };
    const messageStatus = MessageConstants.PENDING;
    const activityLogs = [{
      id: 1,
      author: {
        id: 1,
        firstName: 'Test',
        lastName: 'User',
        thumb: 'images/testimg.jpg',
        defaultCompanyId: 1
      },
      type: 'CREATE',
      createdDate: {
        date: 2525900051,
        text: '',
        timezone: {
          name: '2050/01/15 12:00am',
          abbreviation: 'American/Chicago',
          offset: '-600'
        }
      }
    }];
    const messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, userId, messageStatus, assignee, activityLogs});

    const messageState = messageAdapter.createMessageStateFromPayload(messageObj, 0);
    const activityData = messageState.message.activities;
    expect(activityData.equals(messageAdapter.createActivityLogFromPayload(activityLogs))).toBeTruthy();
  });

  describe('Facebook State', () => {

    let messageState;

    beforeEach(() => {
      const companyId = '2';
      const initiativeId = '3';
      const userId = 4;
      const assignee = { id: 1, firstName: 'a', lastName: 'b' };
      const messageStatus = MessageConstants.PENDING;
      const messageObj: MessageObject = mockMessageResponse({companyId, initiativeId, userId, messageStatus, assignee});

      messageState = messageAdapter.createMessageStateFromPayload(messageObj, 0);
    });

    it('should correctly set facebook targeting in state from a server payload', () => {
      expect(is(
        messageState.facebook.targeting,
        new TargetingState({
          language: new LanguagesState({
            selectedLanguages: ImmutableList([
              new LanguageState({
                value: 'ES',
                description: 'Spanish',
                type: 'LOCALE'
              }),
              new LanguageState({
                value: 'EN',
                description: 'English',
                type: 'LOCALE'
              })
            ])
          }),
          location: new LocationsState({
            includedLocations: ImmutableList([
              new LocationState({
                value: 'US',
                description: 'United States',
                subType: 'COUNTRY',
                type: 'GEOGRAPHY'
              }),
              new LocationState({
                value: 'LDN',
                description: 'London',
                subType: 'METRO',
                type: 'GEOGRAPHY'
              })
            ])
          }),
          gender: new GenderState({
            value: '1',
            description: 'Male'
          })
        })
      )).toBe(true);
    });

    it('should correctly set facebook darkpost in state from a server payload', () => {
      expect(messageState.facebook.darkPostStatus).toBe(true);
    });
  });
});
