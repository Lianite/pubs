/* @flow */
import { createPlansFromPayload, createCurrentPlanFromPlanId } from 'adapters/PlanAdapter';

const payload =
  [{
    id:'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
    name:'Test 1',
    currentStatus:'DRAFT"',
    status:'DRAFT',
    companyId:2,
    campaignId:3,
    campaignName:'Support',
    color:'#119146',
    planCountersDTO:{
      draftMessageCount:0,
      scheduledMessageCount:0,
      waitingApprovalMessageCount:0,
      publishedMessageCount:0,
      totalEngagementMessageCount:0
    }
  },
    {
      id:'debfba3a-7dfc-49f6-a3fd-c05eb2d92e01',
      name:'Test 2',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    },
    {
      id:'debfba3a-7dfc-49f6-aafd-c05eb2d90e01',
      name:'Test 3',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    },
    {
      id:'debfba3a-7dfc-49f6-a3fd-c05eb34390e01',
      name:'Test 4',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    }
  ];
describe('Plan Adapter', () => {
  it('Create an array of plan objects for redux from the payload', () => {
    let plans = createPlansFromPayload(payload, '3');
    expect(plans.length).toEqual(4);
  });
  it('Should find a plan out of a list of plans', () => {
    let plan = createCurrentPlanFromPlanId('debfba3a-7dfc-49f6-a3fd-c05eb34390e01',
                createPlansFromPayload(payload, '3'));
    expect(plan).toBeDefined();
  });
});
