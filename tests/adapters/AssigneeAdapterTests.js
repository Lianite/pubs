/* @flow */
import {
  createAssigneesFromPayload
} from 'adapters/AssigneeAdapter';
import { is, List as ImmutableList } from 'immutable';
import { AssigneeRecord } from 'records/AssigneeRecords';
import type { AssigneeData } from 'adapters/types';

describe('Assignee Adapter', () => {

  const assigneePayload: Array<AssigneeData> = [
    new AssigneeRecord({
      firstName: 'Isaac',
      lastName: 'test',
      id: 1
    }),
    new AssigneeRecord({
      firstName: 'Isaac',
      lastName: 'test',
      id: 2
    }),
    new AssigneeRecord({
      firstName: 'Isaac',
      lastName: 'test',
      id: 3
    })
  ];

  it('should create an assignee list for redux from server payload', () => {
    expect(is(createAssigneesFromPayload(assigneePayload), ImmutableList(assigneePayload))).toBe(true);
  });

});
