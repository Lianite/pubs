/* @flow */
import * as AccountsSelectors from 'selectors/AccountsSelectors';
import {
  AccountsState,
  CredentialRecord
} from 'records/AccountsRecord';
import  { AssigneeRecord, AssigneeReducerState } from 'records/AssigneeRecords';
import mockState from 'tests/test-utils/MockPublishingState';
import { Map as ImmutableMap } from 'immutable';

describe('Accounts Selector', () => {

  const defaultAccounts: AccountsState = new AccountsState({
    voices: [
      {
        data: {
          id: 1,
          name: 'Test 1',
          credentials: [
            {
              data: {
                id: 1,
                name: 'Page: Facebook Test',
                service: 'FACEBOOK',
                accountType: 'PAGE',
                socialNetworkId: '1234',
                uniqueId: '12345',
                authenticated: true,
                targetingSupported: true,
                organicVisibilitySupported: true
              }
            },
            {
              data: {
                id: 2,
                name: 'Do not target',
                service: 'FACEBOOK',
                accountType: 'NOT A PAGE?',
                socialNetworkId: '1234',
                uniqueId: '12345',
                authenticated: false,
                targetingSupported: false,
                organicVisibilitySupported: true
              }
            },
            {
              data: {
                id: 3,
                name: 'Page: Target',
                service: 'FACEBOOK',
                accountType: 'PAGE',
                socialNetworkId: '1234',
                uniqueId: '12345',
                authenticated: true,
                targetingSupported: true,
                organicVisibilitySupported: true
              }
            }
          ]
        }
      },
      {
        data: {
          id: 2,
          name: 'Test 2',
          credentials: [
            {
              data: {
                id: 1,
                name: 'Page: Facebook Test',
                service: 'FACEBOOK',
                accountType: 'PAGE',
                socialNetworkId: '1234',
                uniqueId: '12345',
                authenticated: true,
                targetingSupported: true,
                organicVisibilitySupported: true
              }
            }
          ]
        }
      }
    ]
  });

  let state;

  beforeEach(() => {
    state = mockState({
      accounts: defaultAccounts,
      assignee: new AssigneeReducerState({
        currentAssignee: new AssigneeRecord({
          voices: [{ data: { id: 1 } }]
        })
      })
    })();
  });

  describe('Get Filtered Credentials selector', () => {
    it('should include all facebook accounts in unfiltered credential count if facebook targeting is not possible', () => {
      expect(AccountsSelectors.getFilteredCredentials(state)[0].unfilteredCredentialCount).toEqual(3);
    });

    it('should include only valid facebook targeting accounts in unfiltered credential count if facebook targeting is possible', () => {
      state = mockState({
        accounts: defaultAccounts.set('checkedCredentials', ImmutableMap({
          '1': new CredentialRecord({
            id: 1,
            name: 'Page: Facebook Test',
            socialNetwork: 'FACEBOOK',
            accountType: 'PAGE',
            authenticated: true,
            targetingSupported: true,
            organicVisibilitySupported: true
          })
        })),
        assignee: new AssigneeReducerState({
          currentAssignee: new AssigneeRecord({
            voices: [{ data: { id: 1 } }]
          })
        })
      })();
      expect(AccountsSelectors.getFilteredCredentials(state)[0].unfilteredCredentialCount).toEqual(2);
    });

    it('should filter results based on the advanced option search criteria', () => {
      state = mockState({
        accounts: defaultAccounts.merge({
          currentAccountFilterText: 'Page:',
          searchOnVoices: false
        }),
        assignee: new AssigneeReducerState({
          currentAssignee: new AssigneeRecord({
            voices: [{ data: { id: 1 } }, { data: { id: 2 } }]
          })
        })
      })();

      let filteredAccounts = AccountsSelectors.getFilteredCredentials(state);
      expect(filteredAccounts[0].credentials.length).toEqual(2); // two accounts in the first voice with 'Page:'
      expect(filteredAccounts[1].credentials.length).toEqual(1); // one accounts in the second voice with 'Page:'

      state = mockState({
        accounts: defaultAccounts.merge({
          currentAccountFilterText: 'Page:',
          searchOnVoices: true
        }),
        assignee: new AssigneeReducerState({
          currentAssignee: new AssigneeRecord({
            voices: [{ data: { id: 1 } }, { data: { id: 2 } }]
          })
        })
      })();

      filteredAccounts = AccountsSelectors.getFilteredCredentials(state);
      expect(filteredAccounts.length).toEqual(0); // no account sets have the text "Page:"
    });
  });
});
