/* @flow */
import * as PlanSelectors from 'selectors/PlanSelectors';
import mockState from 'tests/test-utils/MockPublishingState';
import { List as ImmutableList } from 'immutable';
import { StoredPlan } from 'records/PlanRecords';

describe('Plans Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      router: {
        location: {
          query: {
            plan: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01'
          }
        }
      },
      plan: {
        plansData: ImmutableList([
          new StoredPlan({
            id: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
            name: 'Test 1',
            color: '#119146'
          })
        ])
      }
    })();
  });

  describe('getPlanById selector', () => {
    it('select the plan off of plansData per the id from the router', () => {
      expect(PlanSelectors.getPlanById(state)).toDeepEqual(state.plan.plansData.get('0'));
    });
  });
});
