/* @flow */
import * as LinkTagsSelector from 'selectors/LinkTagsSelector';
import {
  fromJS,
  List as ImmutableList,
  Map as ImmutableMap,
  is as ImmutableIs
} from 'immutable';
import mockState from 'tests/test-utils/MockPublishingState';
import {
  SupportedNetworks,
  RequestStatus,
  DefaultNetwork } from 'constants/ApplicationConstants';
import {
  LinkTagPropertiesRecord,
  TagVariable,
  TagVariableValue,
  LinkTagDetails } from 'records/LinkTagsRecords';
import { CompanyRecord } from 'reducers/EnvironmentReducer';
import { EditorState, ContentState } from 'draft-js';

export const unsavedLinkTag = new LinkTagPropertiesRecord({
  id: 1,
  enabled: true,
  saveStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  tagVariables: ImmutableList([new TagVariable({
    id: 4,
    variableDescription: ImmutableMap({
      required: true
    })
  })]),
  variableValues: ImmutableList([new TagVariableValue({
    variableId: 4,
    values: ImmutableList([ImmutableMap({value: 'SD'})])
  })])
});

export const savedLinkTag = new LinkTagPropertiesRecord({
  id: 2,
  enabled: true,
  saveStatus: RequestStatus.STATUS_LOADED,
  tagVariables: ImmutableList([new TagVariable({
    variableDescription: ImmutableMap({
      required: false
    })
  })]),
  variableValues: ImmutableList([new TagVariableValue({
    variableId: 4,
    values: ImmutableList([ImmutableMap({value: 'SD'})])
  })])
});

const disabledLinkTag = new LinkTagPropertiesRecord({
  id: 3,
  enabled: false,
  saveStatus: RequestStatus.STATUS_NEVER_REQUESTED,
  tagVariables: ImmutableList([new TagVariable({
    variableDescription: ImmutableMap({
      required: false
    })
  })]),
  variableValues: ImmutableList([new TagVariableValue({
    variableId: 4,
    values: ImmutableList([ImmutableMap({value: 'SD'})])
  })])
});

export const linkTagIsBeenSaved = new LinkTagPropertiesRecord({
  id: 4,
  enabled: true,
  saveStatus: RequestStatus.STATUS_REQUESTED,
  tagVariables: ImmutableList([new TagVariable({
    id: 4,
    variableDescription: ImmutableMap({
      required: true
    })
  })]),
  variableValues: ImmutableList([new TagVariableValue({
    variableId: 4,
    values: ImmutableList([ImmutableMap({value: 'SD'})])
  })])
});

const defaultLinkTags = {
  urls: fromJS({
    'foo.com': {
      [DefaultNetwork]: unsavedLinkTag,
      [SupportedNetworks.FACEBOOK]: savedLinkTag
    },
    'bar.com': {
      [SupportedNetworks.TWITTER]: disabledLinkTag
    }
  })
};

describe('LinkTags Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      linkTags: defaultLinkTags,
      environment: ImmutableMap({
        featureFlags: ImmutableMap({
          linkTags: true
        }),
        company: new CompanyRecord({
          enabledFeatures: ['link_tagging']
        })
      })
    })();
  });

  describe('linkTagsByLink', () => {
    it('should return tagVariables and variablesValues for each enabled link', () => {
      const linkTagsByLink = LinkTagsSelector.linkTagsByLink(state);
      const expectedLinkTagsByLink = ImmutableMap({
        'foo.com': new LinkTagDetails({
          tagVariables: ImmutableList([new TagVariable({
            id: 4,
            variableDescription: fromJS({
              required: true
            })
          })]),
          variableValues: ImmutableList([new TagVariableValue({
            variableId: 4,
            values: fromJS([{value: 'SD'}])
          })])
        })
      });
      expect(ImmutableIs(linkTagsByLink, expectedLinkTagsByLink)).toEqual(true);
    });
  });

  describe('linkTagsReadyToBeSaved', () => {
    it('should return a list with the link tags to save', () => {
      const linkTagsReadyToBeSaved = LinkTagsSelector.linkTagsReadyToBeSaved(state);
      const expectedlinkTagsReadyToBeSaved = ImmutableList([
        ImmutableMap({
          link: 'foo.com',
          network: DefaultNetwork,
          linkTag: unsavedLinkTag
        })
      ]);
      expect(ImmutableIs(linkTagsReadyToBeSaved, expectedlinkTagsReadyToBeSaved)).toEqual(true);
    });
  });

  describe('linkTagsAreBeenSaved', () => {
    it('should return a list with the link tags that are been saved', () => {
      state = mockState({
        linkTags: {
          urls: fromJS({
            'foo.com': {
              [DefaultNetwork]: savedLinkTag,
              [SupportedNetworks.FACEBOOK]: linkTagIsBeenSaved
            },
            'bar.com': {
              [SupportedNetworks.TWITTER]: disabledLinkTag
            }
          })
        }
      })();
      const linkTagsAreBeenSaved = LinkTagsSelector.linkTagsAreBeenSaved(state);
      const expectedlinkTagsAreBeenSaved = ImmutableList([
        ImmutableMap({
          link: 'foo.com',
          network: SupportedNetworks.FACEBOOK,
          linkTag: linkTagIsBeenSaved
        })
      ]);
      expect(ImmutableIs(linkTagsAreBeenSaved, expectedlinkTagsAreBeenSaved)).toEqual(true);
    });
  });

  describe('savedLinkTags', () => {
    it('should return a list with the link tags that were saved', () => {
      state = mockState({
        linkTags: {
          urls: fromJS({
            'foo.com': {
              [DefaultNetwork]: savedLinkTag,
              [SupportedNetworks.FACEBOOK]: linkTagIsBeenSaved
            },
            'bar.com': {
              [SupportedNetworks.TWITTER]: disabledLinkTag
            }
          })
        }
      })();
      const linkTagsSaved = LinkTagsSelector.savedLinkTags(state);
      const expectedlinkTagsSaved = ImmutableList([
        ImmutableMap({
          link: 'foo.com',
          network: DefaultNetwork,
          linkTag: savedLinkTag
        })
      ]);
      expect(ImmutableIs(linkTagsSaved, expectedlinkTagsSaved)).toEqual(true);
    });
  });

  describe('linkTagsEnable', () => {
    it('should return false when feature flag is turned off', () => {
      expect(LinkTagsSelector.linkTagsEnable(state)).toEqual(true);

      state = mockState({
        linkTags: defaultLinkTags,
        environment: ImmutableMap({
          featureFlags: ImmutableMap({
            linkTags: false
          })
        })
      })();

      expect(LinkTagsSelector.linkTagsEnable(state)).toEqual(false);
    });
  });

  describe('getMessageTitleByNetwork', () => {
    it('should return message title', () => {
      state = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText(''))
          }),
          messageTitle: 'foo'
        }
      })();

      expect(LinkTagsSelector.getMessageTitleByNetwork(state).equals(fromJS({
        [SupportedNetworks.TWITTER]: 'foo'
      }))).toEqual(true);
    });

    it('should return message text if the message length is < 64 and there is no title', () => {
      state = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText('some text'))
          })
        }
      })();

      expect(LinkTagsSelector.getMessageTitleByNetwork(state).equals(fromJS({
        [SupportedNetworks.TWITTER]: 'some text'
      }))).toEqual(true);
    });

    it('should return the first 4 words of message text if the message length is < 63 and there is no title', () => {
      state = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s'))
          })
        }
      })();

      expect(LinkTagsSelector.getMessageTitleByNetwork(state).equals(fromJS({
        [SupportedNetworks.TWITTER]: 'Lorem Ipsum is simply'
      }))).toEqual(true);
    });

    it('should return the first 64 characters of the message text if the four first words are longer than 63 characters and there is no title', () => {
      state = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText('LoremIpsumIsSimplyDummyText ofThePrinting andTypesettingIndustry. LoremIpsumHasBeenThe industry standard dummy text ever since the 1500s'))
          })
        }
      })();

      expect(LinkTagsSelector.getMessageTitleByNetwork(state).equals(fromJS({
        [SupportedNetworks.TWITTER]: 'LoremIpsumIsSimplyDummyText ofThePrinting andTypesettingIndustr'
      }))).toEqual(true);
    });

    it('should return the network name if there is not a title or a message', () => {
      state = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText(''))
          })
        }
      })();
      const expectedResult = fromJS({
        [SupportedNetworks.TWITTER]: SupportedNetworks.TWITTER
      });
      expect(LinkTagsSelector.getMessageTitleByNetwork(state).equals(expectedResult)).toEqual(true);
    });
  });

});
