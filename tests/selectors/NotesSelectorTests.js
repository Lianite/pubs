/* @flow */
import * as NotesSelector from 'selectors/NotesSelector';
import { NotesState } from 'records/NotesRecords';
import mockState from 'tests/test-utils/MockPublishingState';
import { List as ImmutableList } from 'immutable';

const defaultNotes: NotesState = new NotesState({
  notesList: ImmutableList([{
    authorFirstName: 'test',
    authorLastName: 'user',
    authorAvatarThumbnail: 'http://avatar.com',
    noteText: 'hello',
    noteCreationTimestamp: 1234
  }, {
    authorFirstName: 'example',
    authorLastName: 'person',
    authorAvatarThumbnail: 'http://avatar.org',
    noteText: 'goodbye',
    noteCreationTimestamp: 9876
  }])
});

describe('Notes Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      message: {
        eventId: null
      },
      notes: defaultNotes
    })();
  });

  describe('Notes to Save selector', () => {
    it('should select all notes when the message has not been persisted ', () => {
      expect(NotesSelector.notesToSave(state)).toDeepEqual([{
        authorFirstName: 'test',
        authorLastName: 'user',
        authorAvatarThumbnail: 'http://avatar.com',
        noteText: 'hello',
        noteCreationTimestamp: 1234
      }, {
        authorFirstName: 'example',
        authorLastName: 'person',
        authorAvatarThumbnail: 'http://avatar.org',
        noteText: 'goodbye',
        noteCreationTimestamp: 9876
      }]);
    });

    it('should select no notes when the message has been persisted ', () => {
      state.message = state.message.set('eventId', 123);
      expect(NotesSelector.notesToSave(state)).toDeepEqual([]);
    });
  });

  describe('notesForDisplay selector', () => {
    it('should sort the notes in order for decsending date', () => {
      expect(NotesSelector.notesForDisplay(state)).toDeepEqual([
        {
          authorFirstName: 'example',
          authorLastName: 'person',
          authorAvatarThumbnail: 'http://avatar.org',
          noteText: 'goodbye',
          noteCreationTimestamp: 9876
        },
        {
          authorFirstName: 'test',
          authorLastName: 'user',
          authorAvatarThumbnail: 'http://avatar.com',
          noteText: 'hello',
          noteCreationTimestamp: 1234
        }
      ]);
    });
  });
});
