/* @flow */
import * as ValidationSelector from 'selectors/ValidationSelector';
import { fromJS } from 'immutable';
import mockState from 'tests/test-utils/MockPublishingState';
import { mediaErrorText } from 'constants/UITextConstants';
import { RequestStatus } from 'constants/ApplicationConstants';
import { PublishingError } from 'records/ErrorRecords';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { EditorState, ContentState } from 'draft-js';
import { SocialVideoRecord } from 'records/VideosRecords';
import { CredentialRecord } from 'records/AccountsRecord';
import { LanguageState } from 'records/TargetingRecords';
import { LinkTagsState } from 'records/LinkTagsRecords';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import {
  linkTagIsBeenSaved,
  unsavedLinkTag,
  savedLinkTag } from 'tests/selectors/LinkTagsSelectorTests';

const defaultMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createWithContent(ContentState.createFromText('here is the message')),
  TWITTER: EditorState.createWithContent(ContentState.createFromText('here is the message'))
});

const bothEmptyMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createEmpty(),
  TWITTER: EditorState.createEmpty()
});

const singleEmptyMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createWithContent(ContentState.createFromText('here is the message')),
  TWITTER: EditorState.createEmpty()
});

const onlyFBEmptyMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createEmpty()
});

const defaultCapabilities = fromJS({
  FACEBOOK: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: true
        }
      },
      VIDEO: {
        videoCapabilityDTO: {
          maxFileSize: 1024,
          minDurationMilliseconds: 1000,
          maxDurationMilliseconds: 4000,
          transcodedVideoUsedForPublishing: false
        }
      }
    }
  },
  TWITTER: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: true
        }
      },
      VIDEO: {
        videoCapabilityDTO: {
          maxFileSize: 512,
          minDurationMilliseconds: 500,
          maxDurationMilliseconds: 3000,
          transcodedVideoUsedForPublishing: true
        }
      }
    }
  }
});

const twoBigVideos = ImmutableList([new SocialVideoRecord({
  videoSize: 2048,
  transcodedVideoSize: 2048,
  targetService: 'FACEBOOK'
}), new SocialVideoRecord({
  videoSize: 2048,
  transcodedVideoSize: 2048,
  targetService: 'TWITTER'
})]);

describe('Validation Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      }
    })();
  });

  describe('messageHasPublishableContent', () => {
    it('should be false if no text', () => {
      state.message = state.message.set('messageEditorStateMap', bothEmptyMessageEditorStateMap);
      expect(ValidationSelector.messageHasPublishableContent(state)).toBe(false);
    });

    it('should be false if only one network has text', () => {
      state.message = state.message.set('messageEditorStateMap', singleEmptyMessageEditorStateMap);
      expect(ValidationSelector.messageHasPublishableContent(state)).toBe(false);
    });

    it('should be true if both networks have text', () => {
      expect(ValidationSelector.messageHasPublishableContent(state)).toBe(true);
    });

    it('should be true if no text but image', () => {
      state.images = state.images.set('imageUploadStatus', RequestStatus.STATUS_LOADED);
      state.images = state.images.set('imageId', 123);
      state.message = state.message.set('messageEditorStateMap', bothEmptyMessageEditorStateMap);
      expect(ValidationSelector.messageHasPublishableContent(state)).toBe(true);
    });

    it('should be true if no text but video', () => {
      state.videos = state.videos.set('socialVideos', ImmutableList([new SocialVideoRecord({
        uploadStatus: RequestStatus.STATUS_LOADED
      })]));
      state.message = state.message.set('messageEditorStateMap', bothEmptyMessageEditorStateMap);
      expect(ValidationSelector.messageHasPublishableContent(state)).toBe(true);
    });

    it('should be true if no text but link preview and just facebook', () => {
      state.linkPreview = state.linkPreview.set('linkUrl', 'cnn.com');
      state.message = state.message.set('messageEditorStateMap', onlyFBEmptyMessageEditorStateMap);
      expect(ValidationSelector.messageHasPublishableContent(state)).toBe(true);
    });
  });

  describe('asyncActionOccuring', () => {
    it('should not be true by default', () => {
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(false);
    });

    it('should be true by when image uploading', () => {
      state.images = state.images.set('imageUploadStatus', RequestStatus.STATUS_REQUESTED);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });

    it('should be true by when video uploading', () => {
      const uploadingVideo = ImmutableList([new SocialVideoRecord({
        uploadStatus: RequestStatus.STATUS_REQUESTED
      })]);
      state.videos = state.videos.set('socialVideos', uploadingVideo);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });

    it('should be true by when video transcoding', () => {
      const uploadingVideo = ImmutableList([new SocialVideoRecord({
        transcodeProgressStatus: RequestStatus.STATUS_REQUESTED
      })]);
      state.videos = state.videos.set('socialVideos', uploadingVideo);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });

    it('should be true by when custom video thumb uploading', () => {
      state.videos = state.videos.set('customThumbnailUploadStatus', RequestStatus.STATUS_REQUESTED);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });

    it('should be true by when link preview scraping', () => {
      state.linkPreview = state.linkPreview.set('currentlyScraping', true);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });

    it('should be true by when link preview custom thumb uploading', () => {
      state.linkPreview = state.linkPreview.set('uploadingCustomThumbStatus', RequestStatus.STATUS_REQUESTED);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });

    it('should be true by when saving a note', () => {
      state.notes = state.notes.set('savingNote', true);
      expect(ValidationSelector.asyncActionOccurring(state)).toBe(true);
    });
  });

  describe('videoError', () => {
    it('should show a generic error if we have one for custom thumb', () => {
      state.videos = state.videos.set('customThumbnailUploadError', 'thumb error here');
      let expectedError = new PublishingError({
        title: mediaErrorText.videoCustomThumbUploadError,
        description: 'thumb error here',
        networks: ImmutableList(['FACEBOOK', 'TWITTER'])
      });

      expect(ValidationSelector.videoError(state)).toDeepEqual(expectedError);
    });

    it('should show size error for both networks', () => {
      state.videos = state.videos.set('socialVideos', twoBigVideos);
      let expectedError = new PublishingError({
        title: mediaErrorText.videoSizeTooLargeTitle,
        description: 'Exceeds size limit for Facebook, Twitter',
        networks: ImmutableList(['Facebook', 'Twitter']),
        dismissable: false
      });

      expect(ValidationSelector.videoError(state)).toDeepEqual(expectedError);
    });

    it('should show duration long error for both networks', () => {
      const twoLongVideos = ImmutableList([new SocialVideoRecord({
        videoSize: 128,
        transcodedVideoSize: 128,
        duration: 5000,
        targetService: 'FACEBOOK'
      }), new SocialVideoRecord({
        videoSize: 128,
        duration: 5000,
        transcodedVideoSize: 128,
        targetService: 'TWITTER'
      })]);
      state.videos = state.videos.set('socialVideos', twoLongVideos);
      let expectedError = new PublishingError({
        title: mediaErrorText.videoDurationErrorTitle,
        description: 'Video too long for Facebook, Twitter',
        networks: ImmutableList(['Facebook', 'Twitter']),
        dismissable: false
      });

      expect(ValidationSelector.videoError(state)).toDeepEqual(expectedError);
    });

    it('should show duration short error for both networks', () => {
      const twoLongVideos = ImmutableList([new SocialVideoRecord({
        videoSize: 128,
        transcodedVideoSize: 128,
        duration: 1,
        targetService: 'FACEBOOK'
      }), new SocialVideoRecord({
        videoSize: 128,
        duration: 1,
        transcodedVideoSize: 128,
        targetService: 'TWITTER'
      })]);
      state.videos = state.videos.set('socialVideos', twoLongVideos);
      let expectedError = new PublishingError({
        title: mediaErrorText.videoDurationErrorTitle,
        description: 'Video too short for Facebook, Twitter',
        networks: ImmutableList(['Facebook', 'Twitter']),
        dismissable: false
      });

      expect(ValidationSelector.videoError(state)).toDeepEqual(expectedError);
    });

    it('should not show an error for default state', () => {
      expect(ValidationSelector.videoError(state)).toBe(undefined);
    });

    it('should show a generic error if we have one', () => {
      state.videos = state.videos.set('videoUploadAndTranscodeError', 'error here');
      let expectedError = new PublishingError({
        title: mediaErrorText.videoUploadError,
        description: 'error here',
        networks: ImmutableList(['FACEBOOK', 'TWITTER']),
        dismissable: false
      });

      expect(ValidationSelector.videoError(state)).toDeepEqual(expectedError);
    });
  });

  describe('messageCanSaveDraft', () => {
    it('should be false when there is a facebook error', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: '1',
          description: 'English'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1'], new CredentialRecord({
        id: '1',
        name: 'test',
        socialNetwork: 'FACEBOOK',
        targetingSupported: false,
        authenticated: false
      }));

      expect(ValidationSelector.messageCanSaveDraftSelector(state)).toBe(false);
    });

    it('should be true when there is no facebook error', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: '1',
          description: 'English'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1'], new CredentialRecord({
        id: '1',
        name: 'test',
        socialNetwork: 'FACEBOOK',
        targetingSupported: true,
        authenticated: true
      }));

      expect(ValidationSelector.messageCanSaveDraftSelector(state)).toBe(true);
    });

    it('should be true when there is no link tags', () => {
      state.linkTags = new LinkTagsState({
        urls: ImmutableMap()
      });
      expect(ValidationSelector.messageCanSaveDraftSelector(state)).toBe(true);
    });

    it('should be false when there are link tags that are been saved', () => {
      state.linkTags = new LinkTagsState({
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.FACEBOOK]: linkTagIsBeenSaved
          }
        })
      });
      expect(ValidationSelector.messageCanSaveDraftSelector(state)).toBe(false);
    });

    it('should be true when the link tags are saved', () => {
      state.linkTags = new LinkTagsState({
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.FACEBOOK]: savedLinkTag
          }
        })
      });
      expect(ValidationSelector.messageCanSaveDraftSelector(state)).toBe(true);
    });
  });

  describe('messageCanPublish', () => {
    beforeEach(() => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        }
      })();
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: '1',
          description: 'English'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1'], new CredentialRecord({
        id: '1',
        name: 'test',
        socialNetwork: 'FACEBOOK',
        targetingSupported: true,
        authenticated: true
      }));
    });

    it('should be true when there is no facebook error', () => {
      expect(ValidationSelector.messageCanPublishSelector(state)).toBe(true);
    });

    it('should be false when there is a facebook error', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: '1',
          description: 'English'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1'], new CredentialRecord({
        id: '1',
        name: 'test',
        socialNetwork: 'FACEBOOK',
        targetingSupported: false,
        authenticated: false
      }));

      expect(ValidationSelector.messageCanPublishSelector(state)).toBe(false);
    });

    it('should be true when there is no link tags', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: '1',
          description: 'English'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1'], new CredentialRecord({
        id: '1',
        name: 'test',
        socialNetwork: 'FACEBOOK',
        targetingSupported: true,
        authenticated: true
      }));
      state.linkTags = new LinkTagsState({
        urls: ImmutableMap()
      });
      expect(ValidationSelector.messageCanPublishSelector(state)).toBe(true);
    });

    it('should be false when there are link tags that are been saved', () => {
      state.linkTags = new LinkTagsState({
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.FACEBOOK]: linkTagIsBeenSaved
          }
        })
      });
      expect(ValidationSelector.messageCanPublishSelector(state)).toBe(false);
    });

    it('should be false when there are link tags that are ready to be saved', () => {
      state.linkTags = new LinkTagsState({
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.FACEBOOK]: unsavedLinkTag
          }
        })
      });
      expect(ValidationSelector.messageCanPublishSelector(state)).toBe(false);
    });

    it('should be true when the link tags are saved', () => {
      state.linkTags = new LinkTagsState({
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.FACEBOOK]: savedLinkTag
          }
        })
      });
      expect(ValidationSelector.messageCanPublishSelector(state)).toBe(true);
    });
  });

  describe('canUseMultichannelPubs', () => {
    let environment;
    let message;

    beforeEach(() => {
      environment = {
        capabilities: defaultCapabilities,
        viewAccess: true,
        editAccess: true,
        multiChannelEnabled: true,
        userDataStatus: RequestStatus.STATUS_LOADED
      };

      message = {
        eventId: 0,
        messageEditorStateMap: defaultMessageEditorStateMap,
        loadingMessageStatus: RequestStatus.STATUS_LOADED
      };
    });
    it('should allow the user to access multichannel publishing when the feature flag is set and the user has permissions', () => {
      state = mockState({environment, message})();
      expect(ValidationSelector.canUseMultichannelPubs(state)).toBeTruthy();
    });

    it('should deny the user access to multichannel publishing if the feature flag is disabled or the user does not have permissions', () => {
      environment.multiChannelEnabled = false;
      state = mockState({environment, message})();
      expect(ValidationSelector.canUseMultichannelPubs(state)).toBeFalsy();

      environment.multiChannelEnabled = true;
      environment.viewAccess = false;
      state = mockState({environment, message})();
      expect(ValidationSelector.canUseMultichannelPubs(state)).toBeFalsy();
    });

    it('should allow user access to the app if they do not have feature flag, but the message is being edited and they have permissions', () => {
      environment.multiChannelEnabled = false;
      state = mockState({environment, message})();
      expect(ValidationSelector.canUseMultichannelPubs(state)).toBeFalsy();

      message.eventId = 1234;
      state = mockState({environment, message})();
      expect(ValidationSelector.canUseMultichannelPubs(state)).toBeTruthy();
    });
  });
});
