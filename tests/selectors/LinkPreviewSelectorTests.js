/* @flow */
import * as LinkPreviewSelector from 'selectors/LinkPreviewSelector';
import { EditorState} from 'draft-js';
import {
  fromJS,
  List as ImmutableList,
  Map as ImmutableMap,
  Set as ImmutableSet } from 'immutable';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { DefaultNetwork } from 'constants/ApplicationConstants';
import mockState from 'tests/test-utils/MockPublishingState';
import { LinkPreviewState } from 'records/LinkPreviewRecords';

const defaultCapabilities = fromJS({
  FACEBOOK: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: true
        }
      }
    }
  },
  TWITTER: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: false
        }
      }
    }
  }
});

const defaultLinkPreview = {
  linkUrl: 'http://www.spredfast.com',
  caption: 'www.spredfast.com',
  description: 'Spredfast is an enterprise social media marketing platform.',
  sourceMedia: [],
  title: 'Spredfast',
  thumbnailUrl: 'someImageUrl'
};

const defaultMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createEmpty(),
  TWITTER: EditorState.createEmpty()
});

const fbOnlyMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createEmpty()
});

describe('LinkPreview Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      },
      linkPreview: defaultLinkPreview
    })();
  });

  describe('allSelectedNetworksSupportLinkPreview', () => {
    it('should be false with FB and twitter', () => {
      expect(LinkPreviewSelector.allSelectedNetworksSupportLinkPreview(state)).toBe(false);
    });

    it('should be true with just FB', () => {
      expect(LinkPreviewSelector.allSelectedNetworksSupportLinkPreview({
        ...state,
        message: {
          messageEditorStateMap: fbOnlyMessageEditorStateMap
        }
      })).toBe(true);
    });
  });

  describe('canShowLinkPreview selector', () => {
    it('should be true if there is link preview data and there is no selected networks', () => {
      const canShowLinkPreview = LinkPreviewSelector.canShowLinkPreview({
        ...state,
        message: {
          messageEditorStateMap: ImmutableMap()
        }
      });
      expect(canShowLinkPreview).toBeTruthy();
    });

    it('should be true if there is link preview data and some selected network supports link preview', () => {
      const canShowLinkPreview = LinkPreviewSelector.canShowLinkPreview({
        ...state,
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        }
      });
      expect(canShowLinkPreview).toBeTruthy();
    });

    it('should be false if there is not link preview data', () => {
      const canShowLinkPreview = LinkPreviewSelector.canShowLinkPreview({
        ...state,
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        },
        linkPreview: new LinkPreviewState()
      });
      expect(canShowLinkPreview).toBeFalsy();
    });

    it('should be false if there is link preview data but every selected network doesnt support link preview', () => {
      const canShowLinkPreview = LinkPreviewSelector.canShowLinkPreview({
        ...state,
        message: {
          messageEditorStateMap: ImmutableMap({
            TWITTER: EditorState.createEmpty()
          })
        }
      });
      expect(canShowLinkPreview).toBeFalsy();
    });
  });

  describe('linkPreviewNetworks selector', () => {
    it('should return selected networks that support link preview', () => {
      const networks = LinkPreviewSelector.linkPreviewNetworks(state);
      expect(networks).toEqual(['FACEBOOK']);
    });
  });

  describe('getLinkPreviewMap selector', () => {
    it('should return linkPreview map', () => {
      const linkPreviewMap = LinkPreviewSelector.getLinkPreviewMap(state);
      expect(linkPreviewMap).toEqual({
        FACEBOOK: {
          linkUrl: 'http://www.spredfast.com',
          description: 'Spredfast is an enterprise social media marketing platform.',
          caption: 'www.spredfast.com',
          title: 'Spredfast',
          thumbnailUrl: 'someImageUrl',
          videoSrc: '',
          videoLink: ''
        }
      });
    });

    it('should return an empty object if there are no link preview', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        }
      })();
      const linkPreviewMap = LinkPreviewSelector.getLinkPreviewMap(state);
      expect(linkPreviewMap).toEqual({});
    });

    it('should return an empty object if there link preview was marked as removed', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        },
        linkPreview: {
          removed: true,
          linkUrl: 'http://www.spredfast.com',
          caption: 'www.spredfast.com',
          description: 'Spredfast is an enterprise social media marketing platform.',
          sourceMedia: [],
          title: 'Spredfast',
          thumbnailUrl: 'someImageUrl'
        }
      })();
      const linkPreviewMap = LinkPreviewSelector.getLinkPreviewMap(state);
      expect(linkPreviewMap).toEqual({});
    });
  });

  describe('linkPreviewWasRemoved selector', () => {
    it('should return true if the link preview has the prop "removed" turned on', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        },
        linkPreview: {
          removed: true,
          linkUrl: 'http://www.spredfast.com',
          caption: 'www.spredfast.com',
          description: 'Spredfast is an enterprise social media marketing platform.',
          sourceMedia: [],
          title: 'Spredfast',
          thumbnailUrl: 'someImageUrl'
        }
      })();
      const linkPreviewWasRemoved = LinkPreviewSelector.linkPreviewWasRemoved(state);
      expect(linkPreviewWasRemoved).toBeTruthy();
    });
  });

  describe('selectedLinkPreviewUrl selector', () => {
    it('should return the current link preview url', () => {
      const selectedLinkPreviewUrl = LinkPreviewSelector.selectedLinkPreviewUrl(state);
      expect(selectedLinkPreviewUrl).toEqual('http://www.spredfast.com');
    });

    it('should return undefined if the current link preview was removed', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        },
        linkPreview: {
          removed: true,
          linkUrl: 'http://www.spredfast.com',
          caption: 'www.spredfast.com',
          description: 'Spredfast is an enterprise social media marketing platform.',
          sourceMedia: [],
          title: 'Spredfast',
          thumbnailUrl: 'someImageUrl'
        }
      })();
      const selectedLinkPreviewUrl = LinkPreviewSelector.selectedLinkPreviewUrl(state);
      expect(selectedLinkPreviewUrl).toEqual('');
    });
  });

  describe('linkPreviewUrlsListCurrentlyVisible selector', () => {
    it('should return only links from valid networks', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: ImmutableMap({
            FACEBOOK: EditorState.createEmpty(),
            TWITTER: EditorState.createEmpty()
          })
        },
        linkPreview: {
          linkUrl: 'onlylinkpreview.com'
        },
        links: {
          urlProperties: ImmutableMap({
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet(['FACEBOOK'])
            }),
            'google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet(['FACEBOOK'])
            }),
            'twitter.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet(['TWITTER'])
            })
          }),
          urls: ImmutableMap({
            'FACEBOOK': ImmutableList(['tesla.com', 'google.com']),
            'TWITTER': ImmutableList(['twitter.com'])
          })
        }
      })();

      const linkPreviewUrlsList = LinkPreviewSelector.linkPreviewUrlsListCurrentlyVisible(state);
      expect(linkPreviewUrlsList).toEqual(ImmutableList(['tesla.com', 'google.com']));
    });

    it('should return links from default network (and include duplicates)', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: ImmutableMap({
            MESSAGE: EditorState.createEmpty()
          })
        },
        linkPreview: {
          linkUrl: 'onlylinkpreview.com'
        },
        links: {
          urlProperties: ImmutableMap({
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            }),
            'google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            })
          }),
          urls: ImmutableMap({
            [DefaultNetwork]: ImmutableList(['tesla.com', 'google.com', 'tesla.com'])
          })
        }
      })();

      const linkPreviewUrlsList = LinkPreviewSelector.linkPreviewUrlsListCurrentlyVisible(state);
      expect(linkPreviewUrlsList).toEqual(ImmutableList(['tesla.com', 'google.com', 'tesla.com']));
    });
  });

});
