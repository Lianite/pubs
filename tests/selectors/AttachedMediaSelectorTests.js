/* @flow */
import * as AttachedMediaSelector from 'selectors/AttachedMediaSelector';
import { videoThumbnailToDisplay } from 'reducers/index';
import { VideosState, SocialVideoRecord } from 'records/VideosRecords';
import { LinkPreviewState } from 'records/LinkPreviewRecords';
import { EditorState} from 'draft-js';
import { fromJS, Map as ImmutableMap, Set as ImmutableSet, List as ImmutableList } from 'immutable';
import { RequestStatus } from 'constants/ApplicationConstants';
import { PublishingError } from 'records/ErrorRecords';
import mockState from 'tests/test-utils/MockPublishingState';

import type { VideoType } from 'selectors/AttachedMediaSelector';

const defaultCapabilities = fromJS({
  FACEBOOK: {
    NEW_MESSAGE: {
      VIDEO: {
        videoCapabilityDTO: {}
      }
    }
  },
  TWITTER: {
    NEW_MESSAGE: {
      VIDEO: {}
    }
  }
});

const defaultMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createEmpty(),
  TWITTER: EditorState.createEmpty()
});

describe('MessageAttachedMedia Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      }
    })();
  });

  describe('attachedImage selector', () => {
    it('should pick currentImage from state', () => {
      const attachedImage = AttachedMediaSelector.attachedImage({
        ...state,
        images: {
          imageFullUrl: 'http://image-url.com'
        }
      });
      expect(attachedImage.currentImageFull).toEqual('http://image-url.com');
    });

    it('should pick mediaFilesizeError from state', () => {
      const attachedImage = AttachedMediaSelector.attachedImage({
        ...state,
        images: {
          imageUploadError: 'some error'
        }
      });
      const expectedError = new PublishingError({
        title: 'Image Upload Error',
        description: 'some error',
        networks: ImmutableList(['FACEBOOK', 'TWITTER'])
      });
      expect(attachedImage.imageFilesizeError.equals(expectedError)).toBeTruthy();
    });

    it('should bring the waiting/uploading prop turned on/off depending of the upload status', () => {
      let attachedImage = AttachedMediaSelector.attachedImage({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
        }
      });
      expect(attachedImage.waiting).toBeTruthy();
      expect(attachedImage.uploading).toBeFalsy();

      attachedImage = AttachedMediaSelector.attachedImage({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_REQUESTED
        }
      });
      expect(attachedImage.waiting).toBeFalsy();
      expect(attachedImage.uploading).toBeTruthy();
    });
  });

  describe('mediaStatus selector', () => {
    it('should bring the waiting/uploading prop turned on/off depending of the video and image upload', () => {
      let mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
        }
      });
      expect(mediaStatus.waiting).toBeTruthy();
      expect(mediaStatus.uploading).toBeFalsy();

      mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            uploadStatus: RequestStatus.STATUS_REQUESTED
          })])
        },
        images: {
          imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
        }
      });
      expect(mediaStatus.waiting).toBeFalsy();
      expect(mediaStatus.uploading).toBeTruthy();

      mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_REQUESTED
        }
      });
      expect(mediaStatus.waiting).toBeFalsy();
      expect(mediaStatus.uploading).toBeTruthy();
    });

    it('should bring the completed prop turned on when there is an image or a video successfully uploaded', () => {
      let mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_REQUESTED
        }
      });
      expect(mediaStatus.completed).toBeFalsy();

      mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_LOADED
        }
      });
      expect(mediaStatus.completed).toBeTruthy();

      mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            uploadStatus: RequestStatus.STATUS_LOADED
          })])
        },
        images: {
          imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
        }
      });
      expect(mediaStatus.completed).toBeTruthy();
    });

    it('should bring the error prop turned on when there is an image with errors', () => {
      let mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
        }
      });
      expect(mediaStatus.error).toBeFalsy();

      mediaStatus = AttachedMediaSelector.mediaStatus({
        ...state,
        images: {
          imageUploadStatus: RequestStatus.STATUS_ERROR
        }
      });
      expect(mediaStatus.error).toBeTruthy();
    });
  });

  describe('attachedVideo selector', () => {
    it('should pick title from state', () => {
      let attachedVideo = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            displayName: 'some name'
          })])
        }
      });
      expect(attachedVideo.title).toEqual('some name');
    });

    it('should pick thumbnail from state', () => {
      let attachedVideo = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            thumbnails: ImmutableSet([{
              uri: 'some url',
              id: 1
            }, {
              uri: 'another url',
              id: 2
            }])
          })])
        }
      });
      expect(attachedVideo.thumbnail).toEqual('some url');
    });

    it('should pick previewUrl from state', () => {
      let attachedVideo = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            videoPreviewUrl: 'some url'
          })])
        }
      });
      expect(attachedVideo.previewUrl).toEqual('some url');
    });

    it('should bring the waiting/uploading prop turned on/off depending of the video uploading status', () => {
      let attachedVideo = AttachedMediaSelector.attachedVideo(state);
      expect(attachedVideo.waiting).toBeTruthy();
      expect(attachedVideo.uploading).toBeFalsy();

      attachedVideo = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            uploadStatus: RequestStatus.STATUS_REQUESTED
          })])
        }
      });
      expect(attachedVideo.waiting).toBeFalsy();
      expect(attachedVideo.uploading).toBeTruthy();
    });

    it('should bring the completed prop turned on when the video uploading is complete', () => {
      let attachedVideo = AttachedMediaSelector.attachedVideo(state);
      expect(attachedVideo.complete).toBeFalsy();

      attachedVideo = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            transcodeResultsStatus: RequestStatus.STATUS_LOADED
          })])
        }
      });
      expect(attachedVideo.completed).toBeTruthy();
    });

    it('should pick lowest transcoding percent from state', () => {
      let attachedVideo: VideoType = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            transcodeProgress: 30
          }),
          new SocialVideoRecord({
            transcodeProgress: 50
          })])
        }
      });
      expect(attachedVideo.videoTranscodePercent).toEqual(30);
    });

    it('should pick uploading percent from state', () => {
      let attachedVideo: VideoType = AttachedMediaSelector.attachedVideo({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            uploadPercent: 45
          })])
        }
      });
      expect(attachedVideo.uploadPercent).toEqual(45);
    });
  });

  describe('attachment types selectors', () => {
    it('should return no attachments by default', () => {
      const hasAttachment = AttachedMediaSelector.hasAttachmentByType(state);
      expect(hasAttachment.linkPreview).toBe(false);
      expect(hasAttachment.images).toBe(false);
      expect(hasAttachment.video).toBe(false);
    });

    it('should return attachments when it has them', () => {
      const hasAttachment = AttachedMediaSelector.hasAttachmentByType({
        ...state,
        videos: {
          socialVideos: ImmutableSet([new SocialVideoRecord({
            uploadStatus: RequestStatus.STATUS_LOADED
          })])
        },
        linkPreview: new LinkPreviewState({
          currentlyScraping: true
        }),
        images: {
          imageUploadStatus: RequestStatus.STATUS_LOADED,
          imageId: 123
        }
      });
      expect(hasAttachment.linkPreview).toBe(true);
      expect(hasAttachment.images).toBe(true);
      expect(hasAttachment.video).toBe(true);
    });
  });

  describe('videoUploadNetworks selector', () => {
    it('should return selected networks that support link preview', () => {
      const networks = AttachedMediaSelector.videoUploadNetworks(state);
      expect(networks).toEqual(['FACEBOOK']);
    });
  });

  describe('videoThumbnailToDisplay', () => {
    it('should return the first thumbnail if nothing selected', () => {
      const displayedThumb = videoThumbnailToDisplay({
        ...state,
        videos: new VideosState({
          socialVideos: ImmutableSet([new SocialVideoRecord({
            thumbnails: ImmutableList([
              {
                id: 'id0',
                uri: 'uri0'
              },
              {
                id: 'id1',
                uri: 'uri1'
              }
            ])
          })])
        })
      });

      expect(displayedThumb).toDeepEqual({id: 'id0', uri: 'uri0'});
    });

    it('should return the selected thumbnail', () => {
      const displayedThumb = videoThumbnailToDisplay({
        ...state,
        videos: new VideosState({
          socialVideos: ImmutableSet([new SocialVideoRecord({
            thumbnails: ImmutableList([
              {
                id: 'id0',
                uri: 'uri0'
              },
              {
                id: 'id1',
                uri: 'uri1'
              }
            ]),
            thumbnailIndexSelected: 1
          })])
        })
      });

      expect(displayedThumb).toDeepEqual({id: 'id1', uri: 'uri1'});
    });

    it('should return the custom thumbnail if there is one, no matter what else is selected', () => {
      const displayedThumb = videoThumbnailToDisplay({
        ...state,
        videos: new VideosState({
          socialVideos: ImmutableSet([new SocialVideoRecord({
            thumbnails: ImmutableList([
              {
                id: 'id0',
                uri: 'uri0'
              },
              {
                id: 'id1',
                uri: 'uri1'
              }
            ]),
            thumbnailIndexSelected: 1
          })]),
          customThumbnailId: 'customThumbId',
          customThumbnailUri: 'customThumbUri'
        })
      });

      expect(displayedThumb).toDeepEqual({id: 'customThumbId', uri: 'customThumbUri'});
    });
  });

});
