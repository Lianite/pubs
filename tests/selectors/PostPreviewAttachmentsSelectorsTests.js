/* @flow */
import {
  attachments,
  type,
  attachmentTitle,
  attachmentCaption,
  attachmentLinkImage,
  attachmentLink,
  attachmentDescription,
  attachmentVideoUri,
  attachmentVideoThumbnail,
  attachmentImages
} from 'selectors/PostPreviewAttachmentsSelectors';
import { fromJS, Map as ImmutableMap, Set as ImmutableSet } from 'immutable';
import { EditorState, ContentState } from 'draft-js';
import { VideosState, SocialVideoRecord } from 'records/VideosRecords';
import { RequestStatus } from 'constants/ApplicationConstants';
import mockState from 'tests/test-utils/MockPublishingState';
import { LinkPreviewState } from 'records/LinkPreviewRecords';
import { ImageReducerState } from 'records/ImageRecords';

const defaultMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createWithContent(ContentState.createFromText('this is a fantastic message')),
  TWITTER: EditorState.createEmpty()
});

const defaultCapabilities = fromJS({
  FACEBOOK: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: true
        }
      }
    }
  }
});

const defaultLinkPreview = {
  linkUrl: 'http://www.spredfast.com',
  caption: 'www.spredfast.com',
  description: 'Spredfast is an enterprise social media marketing platform.',
  sourceMedia: [],
  title: 'Spredfast',
  thumbnailUrl: 'someImageUrl'
};

describe('PostPreviewAttachments Selector', () => {
  let state;

  const props = {
    network: 'FACEBOOK'
  };

  beforeEach(() => {
    state = mockState({
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      },
      images: {}
    })();
  });

  describe('no attachment', () => {
    it('should select the appropriate props', () => {
      expect(attachments(state, props)).toDeepEqual({
        type: 'text'
      });
    });
  });

  describe('link preview', () => {
    beforeEach(() => {
      state.linkPreview = new LinkPreviewState(defaultLinkPreview);
    });

    it('should select the appropriate link preview props', () => {
      expect(attachments(state, props)).toDeepEqual({
        type: 'link',
        attachmentTitle: 'Spredfast',
        attachmentCaption: 'www.spredfast.com',
        attachmentLinkImage: 'someImageUrl',
        attachmentDescription: 'Spredfast is an enterprise social media marketing platform.',
        attachmentLink: 'http://www.spredfast.com'
      });
    });

    it('should select the type', () => {
      expect(type(state, props)).toEqual('link');
    });

    it('should select the attachmentCaption', () => {
      expect(attachmentCaption(state, props)).toEqual('www.spredfast.com');
    });

    it('should select the attachmentDescription', () => {
      expect(attachmentDescription(state, props)).toEqual('Spredfast is an enterprise social media marketing platform.');
    });

    it('should select the attachmentLink', () => {
      expect(attachmentLink(state, props)).toEqual('http://www.spredfast.com');
    });

    it('should select the attachmentLinkImage', () => {
      expect(attachmentLinkImage(state, props)).toEqual('someImageUrl');
    });

    it('should select the attachmentTitle', () => {
      expect(attachmentTitle(state, props)).toEqual('Spredfast');
    });
  });

  describe('video', () => {
    beforeEach(() => {
      state.videos = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          uploadStatus: RequestStatus.STATUS_REQUESTED,
          displayName: 'video title',
          thumbnails: ImmutableSet([{
            uri: 'http://thumbnail-uri.com',
            id: 1
          }, {
            uri: 'http://thumbnail-uri-2.com',
            id: 2
          }]),
          videoPreviewUrl: 'http://video-uri.com'
        })])
      });
    });

    it('should select the appropriate video props', () => {
      expect(attachments(state, props)).toDeepEqual({
        type: 'video',
        attachmentTitle: 'video title',
        attachmentVideoThumbnail: 'http://thumbnail-uri.com',
        attachmentVideoUri: 'http://video-uri.com'
      });
    });

    it('should select the type', () => {
      expect(type(state, props)).toEqual('video');
    });

    it('should select the attachmentVideoUri', () => {
      expect(attachmentVideoUri(state, props)).toEqual('http://video-uri.com');
    });

    it('should select the attachmentVideoThumbnail', () => {
      expect(attachmentVideoThumbnail(state, props)).toEqual('http://thumbnail-uri.com');
    });

    it('should select the attachmentTitle', () => {
      expect(attachmentTitle(state, props)).toEqual('video title');
    });
  });

  describe('images', () => {
    beforeEach(() => {
      state.images = new ImageReducerState({
        imageFullUrl: 'http://image-url.com',
        imageHeight: 100,
        imageWidth: 200
      });
    });

    it('should select the appropriate image props', () => {
      expect(attachments(state, props)).toDeepEqual({
        type: 'image',
        attachmentImages: [{
          uri: 'http://image-url.com',
          width: 200,
          height: 100
        }]
      });
    });

    it('should select the type', () => {
      expect(type(state, props)).toEqual('image');
    });

    it('should select the attachment_image', () => {
      expect(attachmentImages(state, props)).toDeepEqual([{
        uri: 'http://image-url.com',
        width: 200,
        height: 100
      }]);
    });

    it('should default dimensions to 0 for falsy values in state', () => {
      state.images = new ImageReducerState({
        imageFullUrl: 'test',
        imageHeight: undefined,
        imageWidth: 100
      });
      expect(attachmentImages(state, props)).toDeepEqual([{
        uri: 'test',
        width: 100,
        height: 0
      }]);

      state.images = new ImageReducerState({
        imageFullUrl: 'test',
        imageWidth: undefined,
        imageHeight: 100
      });
      expect(attachmentImages(state, props)).toDeepEqual([{
        uri: 'test',
        height: 100,
        width: 0
      }]);

      state.images = new ImageReducerState({
        imageFullUrl: 'test',
        imageHeight: null,
        imageWidth: null
      });
      expect(attachmentImages(state, props)).toDeepEqual([{
        uri: 'test',
        width: 0,
        height: 0
      }]);

      state.images = new ImageReducerState({
        imageFullUrl: 'test',
        imageHeight: '',
        imageWidth: 10
      });
      expect(attachmentImages(state, props)).toDeepEqual([{
        uri: 'test',
        width: 10,
        height: 0
      }]);

      state.images = new ImageReducerState({
        imageFullUrl: 'test',
        imageHeight: 0,
        imageWidth: 0
      });
      expect(attachmentImages(state, props)).toDeepEqual([{
        uri: 'test',
        width: 0,
        height: 0
      }]);
    });
  });
});
