/* @flow */
import PostPreviewSelector, {
  shortenedUrls,
  name,
  message
} from 'selectors/PostPreviewSelector';
import { fromJS, Set as ImmutableSet, Map as ImmutableMap } from 'immutable';
import { EditorState, ContentState } from 'draft-js';

import { CredentialRecord } from 'records/AccountsRecord';
import { VideosState, SocialVideoRecord } from 'records/VideosRecords';
import { ImageReducerState } from 'records/ImageRecords';
import { RequestStatus } from 'constants/ApplicationConstants';
import mockState from 'tests/test-utils/MockPublishingState';
import { LinkPreviewState } from 'records/LinkPreviewRecords';

const defaultMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createWithContent(ContentState.createFromText('here is the @body of the message')),
  TWITTER: EditorState.createEmpty()
});

const defaultLinkPreview = ImmutableMap({
  linkUrl: 'http://www.spredfast.com',
  caption: 'www.spredfast.com',
  description: 'Spredfast is an enterprise social media marketing platform.',
  sourceMedia: [],
  title: 'Spredfast',
  thumbnailUrl: 'someImageUrl'
});

const defaultCapabilities = fromJS({
  FACEBOOK: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: true
        }
      },
      VIDEO: {
        videoCapabilityDTO: {
          maxFileSize: 1024,
          minDurationMilliseconds: 1000,
          maxDurationMilliseconds: 4000,
          transcodedVideoUsedForPublishing: true
        }
      }
    }
  }
});

describe('PostPreview Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      },
      videos: {
        video: new VideosState()
      },
      media: {},
      accounts: {
        checkedCredentials: ImmutableMap({
          '1': new CredentialRecord({
            id: 2,
            socialCredentialType: 'PAGE',
            socialNetwork: 'FACEBOOK',
            name: 'Facebook test',
            uniqueId: 'a4e23029d2329ed985f27f9b7be94959',
            socialNetworkId: 'z35463903175221'
          }),
          '2': new CredentialRecord({
            id: 2,
            socialCredentialType: 'PAGE',
            socialNetwork: 'TWITTER',
            name: 'Twitter test',
            uniqueId: 'b4e23029d2329ed985f27f9b7be94959',
            socialNetworkId: '235463903175221'
          }),
          '3': new CredentialRecord({
            id: 3,
            socialCredentialType: 'PAGE',
            socialNetwork: 'TWITTER',
            name: 'Twitter test 2',
            uniqueId: 'awoeifj07i4ij',
            socialNetworkId: '2352359959'
          })
        })
      },
      links: {
        urlProperties: new ImmutableMap({})
      },
      images: {}
    })();
  });

  it('should select urls that are to be shortened on publish', () => {

    state.links = state.links.set('urlProperties', ImmutableMap({
      'www.google.com': {
        shortenOnPublish: false
      },
      'http://www.cnn.com': {
        shortenOnPublish: true
      },
      'http://espn.com': {
        shortenOnPublish: true
      }
    }));
    expect(shortenedUrls(state)).toDeepEqual([
      'http://www.cnn.com',
      'http://espn.com'
    ]);
  });

  it('should select the first account name for the network', () => {
    expect(name(state, {network: 'TWITTER'})).toEqual('Twitter test');
  });

  it('should remove "Page:" from the beginning of Facebook accounts', () => {
    state.accounts = state.accounts.set('checkedCredentials', ImmutableMap({
      '1': new CredentialRecord({
        id: 2,
        socialCredentialType: 'PAGE',
        socialNetwork: 'FACEBOOK',
        name: 'Page: name',
        uniqueId: 'a4e23029d2329ed985f27f9b7be94959',
        socialNetworkId: 'z35463903175221'
      })
    }));

    expect(name(state, {network: 'FACEBOOK'})).toEqual('name');
  });

  it('should remove "Group:" from the beginning of Facebook accounts', () => {
    state.accounts = state.accounts.set('checkedCredentials', ImmutableMap({
      '1': new CredentialRecord({
        id: 2,
        socialCredentialType: 'GROUP',
        socialNetwork: 'FACEBOOK',
        name: 'Group: name',
        uniqueId: 'a4e23029d2329ed985f27f9b7be94959',
        socialNetworkId: 'z35463903175221'
      })
    }));
    expect(name(state, {network: 'FACEBOOK'})).toEqual('name');
  });

  it('should not remove "Page:" from the beginning of non-facebook accounts', () => {
    state.accounts = state.accounts.set('checkedCredentials', ImmutableMap({
      '1': new CredentialRecord({
        id: 2,
        socialCredentialType: 'PAGE',
        socialNetwork: 'TWITTER',
        name: 'Page: name',
        uniqueId: 'a4e23029d2329ed985f27f9b7be94959',
        socialNetworkId: 'z35463903175221'
      })
    }));
    expect(name(state, {network: 'TWITTER'})).toEqual('Page: name');
  });

  it('should not remove "Group:" from the beginning of non-facebook accounts', () => {
    state.accounts = state.accounts.set('checkedCredentials', ImmutableMap({
      '1': new CredentialRecord({
        id: 2,
        socialCredentialType: 'PAGE',
        socialNetwork: 'TWITTER',
        name: 'Group: name',
        uniqueId: 'a4e23029d2329ed985f27f9b7be94959',
        socialNetworkId: 'z35463903175221'
      })
    }));
    expect(name(state, {network: 'TWITTER'})).toEqual('Group: name');
  });

  it('should not remove both "Page:" and "Group:" from the beginning of facebook accounts', () => {
    state.accounts = state.accounts.set('checkedCredentials', ImmutableMap({
      '1': new CredentialRecord({
        id: 2,
        socialCredentialType: 'PAGE',
        socialNetwork: 'FACEBOOK',
        name: 'Page: Group: name',
        uniqueId: 'a4e23029d2329ed985f27f9b7be94959',
        socialNetworkId: 'z35463903175221'
      })
    }));
    expect(name(state, {network: 'FACEBOOK'})).toEqual('Group: name');
  });

  it('should select the first account name for the network', () => {
    expect(name(state, {network: 'TWITTER'})).toEqual('Twitter test');
  });

  it('should select the message text', () => {
    expect(message(state, {network: 'FACEBOOK'})).toEqual('here is the @body of the message');
  });

  it('should select the appropriate link preview props', () => {
    state.links = state.links.set('urlProperties', ImmutableMap({
      'www.google.com': {
        shortenOnPublish: false
      },
      'http://www.cnn.com': {
        shortenOnPublish: true
      },
      'http://espn.com': {
        shortenOnPublish: true
      }
    }));
    state.linkPreview = new LinkPreviewState(defaultLinkPreview);
    const postPreview = PostPreviewSelector(state, {network: 'FACEBOOK'});
    expect(postPreview).toEqual(jasmine.objectContaining({
      name: 'Facebook test',
      message: 'here is the @body of the message',
      type: 'link',
      attachment_title: 'Spredfast',
      attachment_caption: 'www.spredfast.com',
      attachment_link_image: 'someImageUrl',
      attachment_description: 'Spredfast is an enterprise social media marketing platform.',
      attachment_link: 'http://www.spredfast.com'
    }));
    expect(postPreview.urls).toDeepEqual([
      'http://www.cnn.com',
      'http://espn.com'
    ]);
  });

  it('should select the appropriate video props', () => {
    state.videos = new VideosState({
      socialVideos: ImmutableSet([new SocialVideoRecord({
        uploadStatus: RequestStatus.STATUS_REQUESTED,
        displayName: 'video title',
        thumbnails: ImmutableSet([{
          uri: 'http://thumbnail-uri.com',
          id: 1
        }, {
          uri: 'http://thumbnail-uri-2.com',
          id: 2
        }]),
        videoPreviewUrl: 'http://video-uri.com'
      })])
    });
    const postPreview = PostPreviewSelector(state, {network: 'FACEBOOK'});
    expect(postPreview).toEqual(jasmine.objectContaining({
      name: 'Facebook test',
      message: 'here is the @body of the message',
      attachment_title: 'video title',
      attachment_video_uri: 'http://video-uri.com',
      attachment_video_thumbnail: 'http://thumbnail-uri.com',
      type: 'video'
    }));
    expect(postPreview.urls).toDeepEqual([]);
  });

  it('should select the appropriate image props', () => {
    state.images = new ImageReducerState({
      imageFullUrl: 'http://image-url.com',
      imageWidth: 100,
      imageHeight: 200
    });
    const postPreview = PostPreviewSelector(state, {network: 'FACEBOOK'});
    expect(postPreview).toEqual(jasmine.objectContaining({
      name: 'Facebook test',
      message: 'here is the @body of the message',
      type: 'image'
    }));
    expect(postPreview.urls).toDeepEqual([]);
    expect(postPreview.attachment_images).toDeepEqual([{
      uri: 'http://image-url.com',
      width: 100,
      height: 200
    }]);
  });
});
