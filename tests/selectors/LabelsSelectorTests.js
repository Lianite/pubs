/* @flow */
import * as LabelSelector from 'selectors/LabelsSelector';
import {fromJS, is } from 'immutable';
import mockState from 'tests/test-utils/MockPublishingState';
import createMockPublishingStore from 'tests/test-utils/MockPublishingStore';

describe('Label Selectors', () => {
  let state;
  let store;
  const label1 = {id: '1', title: 'Test1'};
  const label2 = {id: '2', title: 'Test2'};
  const label3 = {id: '3', title: 'Test3'};
  const label4 = {id: '4', title: 'Test4'};

  beforeEach(() => {
    state = mockState({
      labels: {
        availableLabels: fromJS([label1, label2, label3, label4]).toMap(),
        selectedLabels: fromJS([label1, label2])
      }
    })();
    store = createMockPublishingStore(state);
  });

  describe('getAvailableLabelsWithoutSelectedLabels', () => {
    it('should return the availableLabels without any of the labels that have been selected', () => {
      expect(LabelSelector.getAvailableLabelsWithoutSelectedLabels(state)).toEqual([label3, label4]);
    });
  });

  describe('labelExists', () => {
    it('should find if a label is in availableLabels', () => {
      expect(is(LabelSelector.labelExists('Test4')(store.getState()), fromJS(label4))).toBe(true);
    });
  });
});
