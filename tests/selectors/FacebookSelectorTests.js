/* @flow */
import * as FacebookSelector from 'selectors/FacebookSelector';
import { CredentialRecord } from 'records/AccountsRecord';
import { FacebookState } from 'records/FacebookRecords';
import {
  TargetingState,
  LanguagesState,
  LanguageState,
  LocationState
} from 'records/TargetingRecords';
import mockState from 'tests/test-utils/MockPublishingState';
import {
  is,
  fromJS,
  List as ImmutableList,
  Map as ImmutableMap
} from 'immutable';
import {
  TARGETING_TYPES,
  TARGETING_SUBTYPES,
  GENDER_TARGETING_TYPES
} from 'constants/ApplicationConstants';
import _ from 'lodash';

import type { TargetingEntity, SelectedLocationsTree } from 'adapters/types';

const defaultCredentials: ImmutableMap<string, CredentialRecord> = ImmutableMap({
  '1': new CredentialRecord({
    targetingSupported: true,
    authenticated: true,
    socialNetwork: 'FACEBOOK'
  }),
  '2': new CredentialRecord({
    targetingSupported: false,
    authenticated: false,
    socialNetwork: 'TWITTER'
  })
});

describe('Facebook Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      accounts: {
        checkedCredentials: defaultCredentials
      }
    })();
  });

  describe('Has Error selector', () => {
    it('should return false when targeting is on and all checked credentials support targeting', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: 'foo',
          description: 'bar'
        })
      ]));
      expect(FacebookSelector.hasError(state)).toEqual(false);
    });

    it('should return false when targeting is off and there are no facebook credentials', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1', 'socialNetwork'], 'TWITTER');
      expect(FacebookSelector.hasError(state)).toEqual(false);
    });

    it('should return true when targeting is on and not all checked credentials support targeting', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: 'foo',
          description: 'bar'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1', 'targetingSupported'], false);
      expect(FacebookSelector.hasError(state)).toEqual(true);
    });

    it('should return true when targeting is on and there are no facebook credentials', () => {
      state.facebook = state.facebook.setIn(['targeting', 'language', 'selectedLanguages'], ImmutableList([
        new LanguageState({
          value: 'foo',
          description: 'bar'
        })
      ]));
      state.accounts = state.accounts.setIn(['checkedCredentials', '1', 'socialNetwork'], 'TWITTER');
      expect(FacebookSelector.hasError(state)).toEqual(true);
    });
  });

  describe('Targeting Set selector', () => {
    it('should return true when targeting is empty', () => {
      state.facebook = new FacebookState();
      expect(FacebookSelector.targetingSet(state)).toEqual(false);
    });

    it('should return false when there are language options but none have been selected', () => {
      state.facebook = new FacebookState({
        targeting: new TargetingState({
          language: new LanguagesState({
            languageOptions: ImmutableList([
              new LanguageState({
                value: 'foo',
                description: 'bar'
              })
            ])
          })
        })
      });
      expect(FacebookSelector.targetingSet(state)).toEqual(false);
    });

    it('should return true when at least one language option has been selected', () => {
      state.facebook = new FacebookState({
        targeting: new TargetingState({
          language: new LanguagesState({
            selectedLanguages: ImmutableList([
              new LanguageState({
                value: 'foo',
                description: 'bar'
              })
            ])
          })
        })
      });
      expect(FacebookSelector.targetingSet(state)).toEqual(true);
    });

    it('should return false when no specific gender has been selected', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'gender'
      ], GENDER_TARGETING_TYPES.All);
      expect(FacebookSelector.targetingSet(state)).toEqual(false);
    });

    it('should return true when a specific gender has been selected', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'gender'
      ], GENDER_TARGETING_TYPES.Men);
      expect(FacebookSelector.targetingSet(state)).toEqual(true);
    });

    it('should return false when there are location options but none have been selected', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'location',
        'locationOptions'
      ], ImmutableList([
        new LocationState({
          value: '1',
          description: 'USA',
          subType: 'COUNTRY'
        })
      ]));
      expect(FacebookSelector.targetingSet(state)).toEqual(false);
    });

    it('should return true when at least one location has been included', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'location',
        'includedLocations'
      ], ImmutableList([
        new LocationState({
          value: '1',
          description: 'USA',
          subType: 'COUNTRY'
        })
      ]));
      expect(FacebookSelector.targetingSet(state)).toEqual(true);
    });
  });

  describe('Checked Facebook Credentials selector', () => {
    it('should select all facebook accounts', () => {
      expect(is(FacebookSelector.checkedFacebookCredentials(state), ImmutableList([
        new CredentialRecord({
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK'
        })
      ]))).toEqual(true);

      state.accounts = state.accounts.setIn(['checkedCredentials', '2', 'socialNetwork'], 'FACEBOOK');
      expect(is(FacebookSelector.checkedFacebookCredentials(state), ImmutableList([
        new CredentialRecord({
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK'
        }),
        new CredentialRecord({
          targetingSupported: false,
          authenticated: false,
          socialNetwork: 'FACEBOOK'
        })
      ]))).toEqual(true);
    });
  });

  describe('Invalid Targeting Accounts selector', () => {
    it('should be empty when all facebook accounts are authenticated and support targeting', () => {
      expect(FacebookSelector.invalidTargetingAccounts(state).length).toEqual(0);
    });

    it('should be return unauthenticated facebook accounts', () => {
      state.accounts = state.accounts.setIn(['checkedCredentials', '1', 'authenticated'], false);
      expect(is(fromJS(FacebookSelector.invalidTargetingAccounts(state)), fromJS([
        new CredentialRecord({
          authenticated: false,
          targetingSupported: true,
          socialNetwork: 'FACEBOOK'
        })
      ]))).toEqual(true);
    });

    it('should be return facebook accounts for which targeting is not supported', () => {
      state.accounts = state.accounts.setIn(['checkedCredentials', '1', 'targetingSupported'], false);
      expect(is(fromJS(FacebookSelector.invalidTargetingAccounts(state)), fromJS([
        new CredentialRecord({
          authenticated: true,
          targetingSupported: false,
          socialNetwork: 'FACEBOOK'
        })
      ]))).toEqual(true);
    });

    it('should be return facebook accounts for which targeting is not supported and unauthenticated accounts', () => {
      state.accounts = state.accounts.setIn(['checkedCredentials', '1', 'authenticated'], false);
      state.accounts = state.accounts.setIn(['checkedCredentials', '2', 'targetingSupported'], false);
      state.accounts = state.accounts.setIn(['checkedCredentials', '2', 'socialNetwork'], 'FACEBOOK');
      expect(is(fromJS(FacebookSelector.invalidTargetingAccounts(state)), fromJS([
        new CredentialRecord({
          authenticated: false,
          targetingSupported: true,
          socialNetwork: 'FACEBOOK'
        }),
        new CredentialRecord({
          authenticated: false,
          targetingSupported: false,
          socialNetwork: 'FACEBOOK'
        })
      ]))).toEqual(true);
    });
  });

  describe('Can Target selector', () => {
    it('should return true when one valid targeting facebook account is present and invalid accounts for other networks are present', () => {
      expect(FacebookSelector.canTarget(state)).toBe(true);
    });

    it('should return true when one valid targeting facebook account is present and there are no other accounts', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.deleteIn('2'));
      expect(FacebookSelector.canTarget(state)).toBe(true);
    });

    it('should return true when multiple valid targeting facebook accounts are present and invalid accounts for other networks are present', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.setIn('3', new CredentialRecord({
        targetingSupported: true,
        authenticated: true,
        socialNetwork: 'FACEBOOK'
      })));
      expect(FacebookSelector.canTarget(state)).toBe(true);
    });

    it('should return true when multiple valid targeting facebook accounts are present and there are no other accounts', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.deleteIn('2').setIn('3', new CredentialRecord({
        targetingSupported: true,
        authenticated: true,
        socialNetwork: 'FACEBOOK'
      })));
      expect(FacebookSelector.canTarget(state)).toBe(true);
    });

    it('should return false when multiple valid targeting facebook accounts but one facebook account is invalid', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.deleteIn('2').setIn('3', new CredentialRecord({
        targetingSupported: false,
        authenticated: true,
        socialNetwork: 'FACEBOOK'
      })));
      expect(FacebookSelector.canTarget(state)).toBe(false);

      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.deleteIn('2').setIn('3', new CredentialRecord({
        targetingSupported: true,
        authenticated: false,
        socialNetwork: 'FACEBOOK'
      })));
      expect(FacebookSelector.canTarget(state)).toBe(false);
    });

    it('should return false when there are no facebook accounts', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.deleteIn('1'));
      expect(FacebookSelector.canTarget(state)).toBe(false);
    });

    it('should return false when there are no facebook accounts that are authenticated', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.map((credential: CredentialRecord) => credential.set('authenticated', false)));
      expect(FacebookSelector.canTarget(state)).toBe(false);
    });

    it('should return false when there are no facebook accounts that support targeting', () => {
      state.accounts = state.accounts.set('checkedCredentials', state.accounts.checkedCredentials.map((credential: CredentialRecord) => credential.set('targetingSupported', false)));
      expect(FacebookSelector.canTarget(state)).toBe(false);
    });
  });

  describe('Location Options selector', () => {
    it('should return all location options, sorted alphabetically and by targeting subType', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'location',
        'locationOptions'
      ], ImmutableList([
        new LocationState({
          value: '2',
          description: 'United States',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.COUNTRY
        }),
        new LocationState({
          value: '100',
          description: 'Beijing',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.METRO
        }),
        new LocationState({
          value: '99',
          description: '15151',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.ZIP_CODE
        }),
        new LocationState({
          value: '105',
          description: 'Texas',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.REGION
        }),
        new LocationState({
          value: '101',
          description: 'Alabama',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.REGION
        }),
        new LocationState({
          value: '1',
          description: 'Germany',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.COUNTRY
        })
      ]));
      expect(FacebookSelector.locationOptions(state)).toDeepEqual([{
        value: '1',
        description: 'Germany',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.COUNTRY,
        parentTargetingValue: undefined
      }, {
        value: '2',
        description: 'United States',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.COUNTRY,
        parentTargetingValue: undefined
      }, {
        value: '101',
        description: 'Alabama',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.REGION,
        parentTargetingValue: undefined
      }, {
        value: '105',
        description: 'Texas',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.REGION,
        parentTargetingValue: undefined
      }, {
        value: '100',
        description: 'Beijing',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.METRO,
        parentTargetingValue: undefined
      }, {
        value: '99',
        description: '15151',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.ZIP_CODE,
        parentTargetingValue: undefined
      }]);
    });

    it('should filter out countries that have been indirectly selected by a region/metro area', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'location',
        'locationOptions'
      ], ImmutableList([
        new LocationState({
          value: '2',
          description: 'United States',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.COUNTRY
        }),
        new LocationState({
          value: '102',
          description: 'China',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.COUNTRY
        })
      ]));
      state.facebook = state.facebook.setIn([
        'targeting',
        'location',
        'includedLocations'
      ], ImmutableList([
        new LocationState({
          value: '100',
          description: 'Alabama',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.REGION,
          parentTargetingValue: {
            value: '2',
            description: 'United States',
            type: TARGETING_TYPES.GEOGRAPHY
          }
        })
      ]));
      expect(FacebookSelector.locationOptions(state)).toDeepEqual([{
        value: '102',
        description: 'China',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: TARGETING_SUBTYPES.COUNTRY,
        parentTargetingValue: undefined
      }]);
    });
  });

  describe('Included Locations selector', () => {
    it('should return all included targeting locations', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'location',
        'includedLocations'
      ], ImmutableList([
        new LocationState({
          value: '1',
          description: 'United States',
          type: TARGETING_TYPES.GEOGRAPHY
        }),
        new LocationState({
          value: '2',
          description: 'Germany',
          type: TARGETING_TYPES.GEOGRAPHY
        })
      ]));
      expect(FacebookSelector.includedLocations(state)).toDeepEqual([{
        value: '1',
        description: 'United States',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: undefined,
        parentTargetingValue: undefined
      }, {
        value: '2',
        description: 'Germany',
        type: TARGETING_TYPES.GEOGRAPHY,
        subType: undefined,
        parentTargetingValue: undefined
      }]);
    });

    it('should return no included targeting locations if there are none', () => {
      state.facebook = new FacebookState();
      expect(FacebookSelector.includedLocations(state)).toDeepEqual([]);
    });
  });

  describe('Included Locations By Country selector', () => {
    it('should return the correct map of countries and sub-locations', () => {
      state.facebook = new FacebookState().setIn([
        'targeting',
        'location',
        'includedLocations'
      ], ImmutableList([
        new LocationState({
          value: '1',
          description: 'United States',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.COUNTRY
        }),
        new LocationState({
          value: '2',
          description: 'Germany',
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.COUNTRY
        }),
        new LocationState({
          value: '3',
          description: 'Berlin',
          parentTargetingValue: {
            value: '100',
            description: 'Germany',
            type: TARGETING_TYPES.GEOGRAPHY
          },
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.METRO
        }),
        new LocationState({
          value: '6',
          description: 'Xinjiang',
          parentTargetingValue: {
            value: '101',
            description: 'China',
            type: TARGETING_TYPES.GEOGRAPHY
          },
          type: TARGETING_TYPES.GEOGRAPHY,
          subType: TARGETING_SUBTYPES.REGION
        })
      ]));
      const locationsMap: SelectedLocationsTree = FacebookSelector.includedLocationsByCountry(state);
      const expectedLocationsMap = {
        'United States': {
          description: 'United States',
          children: []
        },
        'Germany': {
          description: 'Germany',
          children: [{
            value: '3',
            description: 'Berlin',
            parentTargetingValue: {
              value: '100',
              description: 'Germany',
              type: TARGETING_TYPES.GEOGRAPHY,
              subType: undefined,
              parentTargetingValue: undefined
            },
            type: 'GEOGRAPHY',
            subType: 'METRO'
          }]
        },
        'China': {
          description: 'China',
          children: [{
            value: '6',
            description: 'Xinjiang',
            parentTargetingValue: {
              value: '101',
              description: 'China',
              type: TARGETING_TYPES.GEOGRAPHY,
              parentTargetingValue: undefined,
              subType: undefined
            },
            type: 'GEOGRAPHY',
            subType: 'REGION'
          }]
        }
      };
      expect(Object.keys(locationsMap)).toDeepEqual(Object.keys(expectedLocationsMap));
      _.forEach(locationsMap, (country: { description: string, children: Array<TargetingEntity>}, countryKey: string) => {
        const expectedCountry = expectedLocationsMap[countryKey];
        expect(country.description).toEqual(expectedCountry.description);
        country.children.forEach((child, childIndex) => {
          expect(child).toDeepEqual(expectedCountry.children[childIndex]);
        });
      });
    });
  });
});
