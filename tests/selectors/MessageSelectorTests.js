import * as ValidationSelector from 'selectors/ValidationSelector';
import * as MessageSelector from 'reducers/index';
import { totalCheckedCredentialsInCurrentView,
  checkedVoiceTree,
  getTotalFilteredCredentialCount,
  getFilteredCredentials } from 'selectors/AccountsSelectors';
import { AssigneeRecord } from 'records/AssigneeRecords';
import { SupportedNetworks, VoiceFilteringConstants } from 'constants/ApplicationConstants';
import { EditorState, ContentState } from 'draft-js';
import { ValidationType } from 'constants/UIStateConstants';
import { CredentialRecord } from 'records/AccountsRecord';
import { NoteState } from 'records/NotesRecords';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { NotesState } from 'records/NotesRecords';
import _ from 'lodash';

describe('Message Selector', () => {
  let state;

  beforeEach(() => {
    state = {
      environment: {
        editAccess: true,
        viewAccess: true
      },
      notes: new NotesState({
        notesList: ImmutableList([
          new NoteState({
            authorFirstName: 'test',
            authorLastName: 'user',
            authorAvatarThumbnail: 'http://avatar.com',
            noteText: 'hello'
          })
        ])
      }),
      assignee: {
        currentAssignee: new AssigneeRecord({
          lastName: 'McTest',
          id: '1',
          firstName: 'Test',
          voices: [{data:{id:1}}, {data:{id:2}}],
          conflictingVoices: []
        }),
        allAssignees: ImmutableList(),
        validAssignees: ImmutableList()
      },
      accounts: {
        currentAccountFilterText: 'test',
        currentAccountChannelFilter: ImmutableList(),
        currentAccountSelectionFilter: '',
        checkedCredentials: ImmutableMap({
          'unique2': new CredentialRecord({
            id: 2,
            socialCredentialType: 'PAGE',
            socialNetwork: 'TWITTER',
            name: 'Twitter test',
            uniqueId: 'unique2',
            socialNetworkId: '235463903175221'
          })
        }),
        voices: [
          {
            data: {
              id: 1,
              name: 'Test 1',
              credentials: [
                {
                  data: {
                    id: 1,
                    name: 'Page: Facebook Test',
                    service: 'FACEBOOK',
                    uniqueId: 'unique1'
                  }
                },
                {
                  data: {
                    id: 2,
                    name: 'Twitter Test',
                    service: 'TWITTER',
                    uniqueId: 'unique2'
                  }
                }
              ]
            }
          },
          {
            data: {
              id: 2,
              name: 'Test 2',
              credentials: [
                {
                  data: {
                    id: 3,
                    name: 'Page: Facebook Test',
                    service: 'FACEBOOK',
                    uniqueId: 'unique3'
                  }
                },
                {
                  data: {
                    id: 4,
                    name: 'Twitter Test',
                    service: 'TWITTER',
                    uniqueId: 'unique4'
                  }
                }
              ]
            }
          }
        ]
      },
      message: {
        messageEditorStateMap: ImmutableMap({
          'FACEBOOK': EditorState.createEmpty(),
          'TWITTER': EditorState.createEmpty()
        })
      }
    };
  });

  //This will need to be changed when the message state is refactored
  describe('Add new getFilteredCredentials selector', () => {
    it('should return correct parsed data', () => {
      const voiceSelector = getFilteredCredentials(state);

      expect(voiceSelector.length).toBe(2);
      expect(voiceSelector[0].id).toBe(1);
      expect(voiceSelector[0].name).toBe('Test 1');
      expect(voiceSelector[0].credentials.length).toBe(2);

      expect(voiceSelector[0].credentials[0].id).toBe(1);
      expect(voiceSelector[0].credentials[0].name).toBe('Page: Facebook Test');
      expect(voiceSelector[0].credentials[0].service).toBe('FACEBOOK');

      expect(voiceSelector[0].credentials[1].id).toBe(2);
      expect(voiceSelector[0].credentials[1].name).toBe('Twitter Test');
      expect(voiceSelector[0].credentials[1].service).toBe('TWITTER');
    });

    it('should return only twitter accounts on that network filtering criteria', () => {
      state.accounts.currentAccountChannelFilter = ImmutableList([SupportedNetworks.TWITTER]);
      const filteredVoices = getFilteredCredentials(state);
      expect(filteredVoices[0].credentials.length).toBe(1);
      expect(filteredVoices[1].credentials.length).toBe(1);
      expect(filteredVoices[0].credentials[0].service).toBe(SupportedNetworks.TWITTER);
      expect(filteredVoices[1].credentials[0].service).toBe(SupportedNetworks.TWITTER);
    });

    it('should return only facebook accounts on that network filtering criteria', () => {
      state.accounts.currentAccountChannelFilter = ImmutableList([SupportedNetworks.FACEBOOK]);
      const filteredVoices = getFilteredCredentials(state);
      expect(filteredVoices[0].credentials.length).toBe(1);
      expect(filteredVoices[1].credentials.length).toBe(1);
      expect(filteredVoices[0].credentials[0].service).toBe(SupportedNetworks.FACEBOOK);
      expect(filteredVoices[1].credentials[0].service).toBe(SupportedNetworks.FACEBOOK);
    });

    it('should return only checked accounts on that filtering criteria', () => {
      state.accounts.currentAccountSelectionFilter = VoiceFilteringConstants.FILTER_SELECTED;
      const filteredVoices = getFilteredCredentials(state);
      // only 1 credential, the one that is in our state's checkedCredentials
      expect(filteredVoices.length).toBe(1);
      expect(filteredVoices[0].credentials.length).toBe(1);
    });

    it('should return only checked accounts on that filtering criteria', () => {
      state.accounts.currentAccountSelectionFilter = VoiceFilteringConstants.FILTER_UNSELECTED;
      const filteredVoices = getFilteredCredentials(state);
      // 3 credential, the one that isn't in our state's checkedCredentials
      expect(filteredVoices.length).toBe(2);
      expect(filteredVoices[0].credentials.length).toBe(1);
      expect(filteredVoices[1].credentials.length).toBe(2);
    });

    it('should return only accounts that can be used by the assignee', () => {
      state.assignee.currentAssignee = new AssigneeRecord({voices:[{data:{id:1}}]});
      const filteredVoices = getFilteredCredentials(state);
      expect(filteredVoices.length).toBe(1);
      expect(filteredVoices[0].credentials.length).toBe(2);
    });
  });

  describe('Add new getTotalFilteredCredentialCount selector', () => {
    it('should return integer for total number of credentials in all voices', () => {
      const voiceTotal = getTotalFilteredCredentialCount(state);

      expect(voiceTotal).toBe(4);
    });
  });

  describe('Add new checkedVoiceTree selector', () => {
    it('should return object built out to determine checked state of all voices and credentials', () => {
      const voiceTreeChecked = checkedVoiceTree(state);

      expect(voiceTreeChecked[1]['unique2']).toBe(2);
    });
  });

  describe('Add new totalNetworkCount selector', () => {
    it('should return integer representing total networks that are selected', () => {
      const numNetworks = MessageSelector.totalNetworkCount(state);
      expect(numNetworks).toBe(2);
    });
  });

  describe('Add new totalCheckedCredentialsInCurrentView selector', () => {
    it('should return integer representing total selected credentials', () => {
      const count = totalCheckedCredentialsInCurrentView(state);

      expect(count).toBe(1);
    });
  });

  describe('Add new getSelectedCredentials selector', () => {
    it('should return array of adapted credentials that have been selected', () => {
      const selectedCredentials = MessageSelector.checkedCredentialsSelector(state);

      expect(selectedCredentials.size).toBe(1);
      expect(selectedCredentials.get('unique2').id).toBe(2);
      expect(selectedCredentials.get('unique2').name).toBe('Twitter test');
      expect(selectedCredentials.get('unique2').socialNetwork).toBe('TWITTER');
    });
  });

  describe('Add new getAccountFilterText selector', () => {
    it('should return filter text for voices/credentials', () => {
      const filterText = MessageSelector.accountFilterText(state);

      expect(filterText).toBe('test');
    });
  });

  describe('Get message map plain text selector', () => {
    it('should get plain text', () =>{
      let modifiedState = _.cloneDeep(state);
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('TWITTER', EditorState.createWithContent(ContentState.createFromText('TWITTER MESSAGE')));
      const plainTextObj = MessageSelector.getMessageEditorStateMapText(modifiedState);
      expect(plainTextObj['FACEBOOK']).toEqual('');
      expect(plainTextObj['TWITTER']).toEqual('TWITTER MESSAGE');
      expect(plainTextObj['NOT_HERE']).toBe(undefined);
    });
  });

  describe('Get disableApplyToAll', () => {
    it('should return false if text is different between networks', () =>{
      let modifiedState = _.cloneDeep(state);
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('TWITTER', EditorState.createWithContent(ContentState.createFromText('TWITTER MESSAGE')));
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('FACEBOOK', EditorState.createWithContent(ContentState.createFromText('FACEBOOK MESSAGE')));

      expect(MessageSelector.disableApplyToAll(modifiedState)).toBe(false);
    });

    it('should return true if text is the same between networks', () =>{
      let modifiedState = _.cloneDeep(state);
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('TWITTER', EditorState.createWithContent(ContentState.createFromText('MESSAGE')));
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('FACEBOOK', EditorState.createWithContent(ContentState.createFromText('MESSAGE')));

      expect(MessageSelector.disableApplyToAll(modifiedState)).toBe(true);
    });
  });

  describe('Get top level validation selector', () => {
    it('should not show any errors by default', () => {
      const validation = ValidationSelector.topLevelValidation(state);

      expect(validation.type).toBe(ValidationType.VALIDATION_TYPE_NO_PROBLEMS);
      expect(validation.validationText).toBe('');
    });

    it('should show error for twitter >140 chars', () =>{
      let modifiedState = _.cloneDeep(state);
      let largeCountEditorState = EditorState.createWithContent(ContentState.createFromText('Tristique lobortis, penatibus luctus est fusce interdum egestas hac nibh fermentum inceptos accumsan justo faucibus euismod sapien dis hac is')); //141 chars
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('TWITTER', largeCountEditorState);

      const validation = ValidationSelector.topLevelValidation(modifiedState);

      expect(validation.type).toBe(ValidationType.VALIDATION_TYPE_ERROR);
      expect(validation.validationText).not.toBe('');
    });
  });

  describe('Get individual network validation selector', () => {
    it('Twitter validation should not show any errors by default', () => {
      let twitterValidation = ValidationSelector.makeGetValidationForNetwork('TWITTER');
      const validation = twitterValidation(state, 'TWITTER');

      expect(validation.type).toBe(ValidationType.VALIDATION_TYPE_NO_PROBLEMS);
      expect(validation.inlineText).toBe('');
      expect(validation.hoverText).toBe('');
    });

    it('should show error for twitter >140 chars', () =>{
      let modifiedState = _.cloneDeep(state);
      let largeCountEditorState = EditorState.createWithContent(ContentState.createFromText('Tristique lobortis, penatibus luctus est fusce interdum egestas hac nibh fermentum inceptos accumsan justo faucibus euismod sapien dis hac is')); //141 chars
      modifiedState.message.messageEditorStateMap = modifiedState.message.messageEditorStateMap.set('TWITTER', largeCountEditorState);

      let twitterValidation = ValidationSelector.makeGetValidationForNetwork('TWITTER');
      const validation = twitterValidation(modifiedState, 'TWITTER');

      expect(validation.type).toBe(ValidationType.VALIDATION_TYPE_ERROR);
      expect(validation.inlineText).not.toBe('');
      expect(validation.inlineText).not.toBe('');
    });
  });

  describe('Message notes selector', () => {
    it('should select the appropriate notes from a message', () => {
      expect(MessageSelector.notesList(state).equals(
        ImmutableList([
          new NoteState({
            authorFirstName: 'test',
            authorLastName: 'user',
            authorAvatarThumbnail: 'http://avatar.com',
            noteText: 'hello'
          })
        ])
      )).toBeTruthy();
    });
  });
});
