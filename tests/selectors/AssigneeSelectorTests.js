/* @flow */
import * as AssignSelectors from 'selectors/AssigneeSelectors';
import { is, List as ImmutableList, Map as ImmutableMap } from 'immutable';
import mockState from 'tests/test-utils/MockPublishingState';
import createMockPublishingStore from 'tests/test-utils/MockPublishingStore';
import { AssigneeRecord } from 'records/AssigneeRecords';
import { CredentialRecord } from 'records/AccountsRecord';

describe('Assignee Selectors', () => {
  let state;
  let store;
  const assignee1 = new AssigneeRecord({id: 1, firstName: 'Test1', lastName: 'McTest', invalid: false});
  const assignee2 = new AssigneeRecord({id: 2, firstName: 'Test2', lastName: 'McTest', invalid: false});
  const assignee3 = new AssigneeRecord({id: 3, firstName: 'Test3', lastName: 'McTest', invalid: true});
  const assignee4 = new AssigneeRecord({id: 4, firstName: 'Test4', lastName: 'McTest', invalid: true});

  beforeEach(() => {
    state = mockState({
      assignee: {
        allAssignees: ImmutableList([assignee1, assignee2, assignee3, assignee4]),
        validAssignees: ImmutableList([assignee1, assignee2])
      },
      message: {
        checkedCredentials: ImmutableMap({'string': new CredentialRecord()}),
        voices: [
          {
            data: {
              credentials:[
                {
                  data:{
                    accountType: 'string',
                    companyId: 1,
                    id: 1,
                    name: 'string',
                    service: 'string',
                    socialNetworkId: 'string',
                    targetingSupported: false,
                    uniqueId: 'string',
                    voiceId: 1
                  },
                  nextActions: {},
                  uri: 'string',
                  permissions: [],
                  verbs: []
                }
              ]
            },
            id: 1,
            name: 'Test',
            nextActions: {},
            uri: 'string',
            permissions: [],
            verbs: []
          }
        ]
      }
    })();
    store = createMockPublishingStore(state);
  });

  describe('getInvalidAssignees', () => {
    it('should return the all invalidAssignees', () => {
      expect(is(AssignSelectors.getInvalidAssignees(store.getState()), ImmutableList([assignee3, assignee4]))).toBe(true);
    });
  });

  describe('getAssignees', () => {
    it('should create the assignee list with invalidAssignees at the end', () => {
      expect(AssignSelectors.getAssignees(store.getState()))
          .toEqual([assignee1.toObject(), assignee2.toObject(), assignee3.toObject(), assignee4.toObject()]);
    });
  });

  describe('getAssigneeVoiceConflicts', () => {
    it('should get a list of voices that are in conflict for the selected voices', () => {
      const res = [
        {
          data: {
            credentials: [{
              data: {
                accountType: 'string',
                companyId: 1,
                id: 1,
                name: 'string',
                service: 'string',
                socialNetworkId: 'string',
                targetingSupported: false,
                organicVisibilitySupported: false,
                uniqueId: 'string',
                voiceId: 1
              },
              nextActions: {},
              permissions: [],
              uri: 'string',
              verbs: []
            }],
            id: 1,
            name: 'string'
          },
          nextActions: {},
          permissions: [],
          uri: 'string',
          verbs: []
        }
      ];
      expect(AssignSelectors.getAssigneeVoiceConflicts(res)(store.getState()))
          .toEqual([]);
    });
  });
});
