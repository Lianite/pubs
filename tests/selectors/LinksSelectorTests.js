/* @flow */
import * as LinksSelector from 'selectors/LinksSelector';
import { EditorState} from 'draft-js';
import {
  fromJS,
  List as ImmutableList,
  Map as ImmutableMap,
  Set as ImmutableSet,
  OrderedSet as ImmutableOrderedSet} from 'immutable';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import {
  DefaultNetwork,
  SupportedNetworks } from 'constants/ApplicationConstants';
import mockState from 'tests/test-utils/MockPublishingState';
import {
  LinkTagPropertiesRecord,
  TagVariable,
  TagVariableValue } from 'records/LinkTagsRecords';
import { RequestStatus } from 'constants/ApplicationConstants';

const defaultCapabilities = fromJS({
  [SupportedNetworks.FACEBOOK]: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: true
        }
      }
    }
  },
  [SupportedNetworks.TWITTER]: {
    NEW_MESSAGE: {
      NOTE: {
        linkPreview: {
          supported: false
        }
      }
    }
  }
});

const defaultLinkPreview = {
  linkUrl: 'http://www.spredfast.com',
  caption: 'www.spredfast.com',
  description: 'Spredfast is an enterprise social media marketing platform.',
  sourceMedia: [],
  title: 'Spredfast',
  thumbnailUrl: 'someImageUrl'
};

const savedLinkTag = new LinkTagPropertiesRecord({
  id: 1,
  enabled: true,
  saveStatus: RequestStatus.STATUS_LOADED,
  tagVariables: ImmutableList([new TagVariable({
    id: 4,
    variableDescription: ImmutableMap({
      required: true
    })
  })]),
  variableValues: ImmutableList([new TagVariableValue({
    variableId: 4,
    values: ImmutableList([ImmutableMap({value: 'SD'})])
  })])
});

const defaultMessageEditorStateMap = ImmutableMap({
  [SupportedNetworks.FACEBOOK]: EditorState.createEmpty(),
  [SupportedNetworks.TWITTER]: EditorState.createEmpty()
});

describe('Links Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      },
      linkPreview: defaultLinkPreview
    })();
  });

  describe('linkPreviewUrls selector', () => {
    it('should return the urls written into the supported networks', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: defaultMessageEditorStateMap
        },
        linkPreview: {
          linkUrl: 'tesla.com',
          caption: 'tesla.com',
          description: 'Spredfast is an enterprise social media marketing platform.',
          sourceMedia: [],
          title: 'Spredfast',
          thumbnailUrl: 'someImageUrl'
        },
        links: {
          urlProperties: ImmutableMap({
            'www.google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.FACEBOOK])
            }),
            'www.reddit.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.TWITTER])
            }),
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.FACEBOOK])
            })
          }),
          urls: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: ImmutableList(['www.google.com', 'www.reddit.com', 'tesla.com'])
          })
        }
      })();
      const linkPreviewUrls = LinksSelector.linkPreviewUrls(state);
      expect(linkPreviewUrls).toEqual(ImmutableOrderedSet(['www.google.com', 'tesla.com']));
    });

    it('should return the urls written into the default network MESSAGE if there are no networks selected', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: ImmutableMap({
            [DefaultNetwork]: EditorState.createEmpty()
          })
        },
        linkPreview: {
          linkUrl: 'tesla.com',
          caption: 'tesla.com',
          description: 'Spredfast is an enterprise social media marketing platform.',
          sourceMedia: [],
          title: 'Spredfast',
          thumbnailUrl: 'someImageUrl'
        },
        links: {
          urlProperties: ImmutableMap({
            'www.google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            }),
            'www.reddit.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            }),
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            })
          }),
          urls: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: ImmutableList(['www.google.com', 'www.reddit.com', 'tesla.com'])
          })
        }
      })();
      const linkPreviewUrls = LinksSelector.linkPreviewUrls(state);
      expect(linkPreviewUrls).toEqual(ImmutableOrderedSet(['www.google.com', 'www.reddit.com', 'tesla.com']));
    });

    it('should include the selected link preview url if there is one', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: ImmutableMap({
            FACEBOOK: EditorState.createEmpty()
          })
        },
        linkPreview: {
          linkUrl: 'onlylinkpreview.com'
        },
        links: {
          urlProperties: ImmutableMap({
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.FACEBOOK])
            })
          }),
          urls: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: ImmutableList(['tesla.com'])
          })
        }
      })();
      const linkPreviewUrls = LinksSelector.linkPreviewUrls(state);
      expect(linkPreviewUrls).toEqual(ImmutableOrderedSet(['onlylinkpreview.com', 'tesla.com']));
    });
  });

  describe('getLinkDTOsForNetworkSelector', () => {
    it('should return linkDTOs for all the networks', () => {
      state = mockState({
        environment: {
          capabilities: defaultCapabilities
        },
        message: {
          messageEditorStateMap: ImmutableMap({
            FACEBOOK: EditorState.createEmpty(),
            TWITTER: EditorState.createEmpty()
          })
        },
        linkPreview: {
          linkUrl: 'onlylinkpreview.com'
        },
        links: {
          urlProperties: ImmutableMap({
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: false,
              associatedNetworks: ImmutableSet([SupportedNetworks.TWITTER])
            }),
            'onlylinkpreview.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.FACEBOOK])
            })
          }),
          urls: ImmutableMap({
            [SupportedNetworks.TWITTER]: ImmutableList(['tesla.com'])
          })
        },
        linkTags: {
          urls: fromJS({
            'onlylinkpreview.com': {
              [SupportedNetworks.FACEBOOK]: savedLinkTag
            },
            'tesla.com': {
              [SupportedNetworks.FACEBOOK]: savedLinkTag
            }
          })
        }
      })();
      const linkDTOs = LinksSelector.getLinkDTOsForNetworkSelector(state);
      const expectedLinkDTOs = ImmutableMap({
        [SupportedNetworks.TWITTER]: [{
          fullUrl: 'tesla.com',
          shortenOnPublish: true,
          addLinkTags: false
        }],
        [SupportedNetworks.FACEBOOK]: [{
          fullUrl: 'onlylinkpreview.com',
          shortenOnPublish: true,
          addLinkTags: true,
          tagId: 1
        }]
      });
      expect(linkDTOs.get(SupportedNetworks.FACEBOOK)).toDeepEqual(expectedLinkDTOs.get(SupportedNetworks.FACEBOOK));
      expect(linkDTOs.get(SupportedNetworks.TWITTER)).toDeepEqual(expectedLinkDTOs.get(SupportedNetworks.TWITTER));
    });
  });

  describe('getFormatedUrlProperties', () => {
    it('should return the formated url properties map', () => {
      state = mockState({
        links: {
          urlProperties: ImmutableMap({
            'tesla.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.FACEBOOK])
            }),
            'google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.TWITTER])
            }),
            'twitter.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([SupportedNetworks.TWITTER])
            })
          })
        }
      })();

      const urlPropertiesMap = LinksSelector.getFormatedUrlProperties(state);
      const expectedUrlPropertiesMap = {
        'tesla.com': {
          fullUrl: 'tesla.com',
          shortenOnPublish: true,
          addLinkTags: true
        },
        'google.com': {
          fullUrl: 'google.com',
          shortenOnPublish: true,
          addLinkTags: true
        },
        'twitter.com': {
          fullUrl: 'twitter.com',
          shortenOnPublish: true,
          addLinkTags: true
        }
      };
      expect(urlPropertiesMap).toDeepEqual(expectedUrlPropertiesMap);
    });
  });

  describe('urls selector', () => {
    it('should return the url list by network', () => {
      state = mockState({
        links: {
          urls: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: ImmutableList(['google.com']),
            [SupportedNetworks.TWITTER]: ImmutableList(['twitter.com', 'tesla.com'])
          })
        }
      })();

      const urls = LinksSelector.urls(state);
      const expectedUrls = ImmutableOrderedSet(['google.com', 'twitter.com', 'tesla.com']);
      expect(urls.equals(expectedUrls)).toBeTruthy();
    });
  });
});
