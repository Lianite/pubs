/* @flow */
import * as ActivitiesSelectors from 'selectors/ActivitiesSelectors';
import mockState from 'tests/test-utils/MockPublishingState';
import { List as ImmutableList } from 'immutable';
import { ActivitiesRecord } from 'records/MessageRecords';

describe('Activities Selector', () => {
  let state;

  beforeEach(() => {
    state = mockState({
      message: {
        activities: ImmutableList([
          new ActivitiesRecord({
            authorFirstName: 'Ultra',
            authorLastName: 'Co0lguy',
            authorAvatarThumbnail: '',
            createdAtTimestamp: 2525860700,
            description: 'Created'
          }),
          new ActivitiesRecord({
            authorFirstName: 'Ultra',
            authorLastName: 'Co0lguy',
            authorAvatarThumbnail: '',
            createdAtTimestamp: 2525860800,
            description: 'Editied'
          })
        ])
      }
    })();
  });

  describe('activitiesForDisplay selector', () => {
    it('should should sort all activity records by descending date, and then return them as an array of objects.', () => {
      const sortedJSifiedActivities = ActivitiesSelectors.activitiesForDisplay(state);
      expect(sortedJSifiedActivities).toEqual([
        {
          authorFirstName: 'Ultra',
          authorLastName: 'Co0lguy',
          authorAvatarThumbnail: '',
          createdAtTimestamp: 2525860800,
          description: 'Editied'
        },
        {
          authorFirstName: 'Ultra',
          authorLastName: 'Co0lguy',
          authorAvatarThumbnail: '',
          createdAtTimestamp: 2525860700,
          description: 'Created'
        }
      ]);
    });
  });
});
