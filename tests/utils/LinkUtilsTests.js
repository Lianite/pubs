/* @flow */
import * as LinkUtils from 'utils/LinkUtils';

describe('Link Util Tests', () => {
  describe('Filter Payload Test', () => {
    const payload = [
      {
        src: 'http://www.google.com',
        id: 1
      },
      {
        src: 'http://www.worldofwarcraft.com',
        id: 2
      },
      {
        src: 'http://www.reddit.com',
        id: 3
      },
      {
        src: 'http://www.facebook.com',
        id: 4
      },
      {
        src: 'http://www.wurmpedia.com',
        id: 5
      }
    ];

    it('should skip filtering payload with blank search string', () => {
      const mediaUrls = LinkUtils.extractUrlFromSourceMedia(payload);

      expect(mediaUrls.length).toBe(5);
      expect(mediaUrls[0]).toBe('http://www.google.com');
    });

    it('should correctly add http to the beginning of a url without it', () => {
      expect(LinkUtils.buildUrlHref('www.facebook.com')).toEqual('http://www.facebook.com');
    });

    it('should correctly add www inbetween the url and http url without it', () => {
      expect(LinkUtils.buildUrlHref('http://facebook.com')).toEqual('http://www.facebook.com');
    });

    it('should correctly add http://www to the url without it', () => {
      expect(LinkUtils.buildUrlHref('facebook.com')).toEqual('http://www.facebook.com');
    });

    it('should not modify the url at all if correct already', () => {
      expect(LinkUtils.buildUrlHref('http://www.facebook.com')).toEqual('http://www.facebook.com');
    });
  });

  describe('Create regex from array', () => {
    it('should create expected regex', () => {
      const regex = LinkUtils.buildRegexpFromArray(['google.com', 'cnn.com']) || {};
      expect(regex.toString()).toEqual('/(google\\.com|cnn\\.com)/g');
    });

    it('should return null if no regex', () => {
      const regex = LinkUtils.buildRegexpFromArray([]);
      expect(regex).toBe(null);
    });

    it('should return regex in sorted descending order', () => {
      const regex = LinkUtils.buildRegexpFromArray(['cnn.com', 'cnn.com/here']) || {};
      expect(regex.toString()).toEqual('/(cnn\\.com\\/here|cnn\\.com)/g');
    });
  });
});
