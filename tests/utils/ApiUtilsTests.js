/* @flow */
import {
  getConversationsEnviromentDetails,
  getCurrentEnvironment } from 'utils/ApiUtils';
import {
  setProdConvDetails,
  setTestConvDetails,
  setStagingConvDetails,
  setDevConvDetails
} from 'tests/test-utils/EnvironmentTestUtils';
import { EnvironmentConstants } from 'constants/ApplicationConstants';

describe('Api Util Tests', () => {

  afterEach(setTestConvDetails);

  it('should return the conv env details', () => {
    expect(getConversationsEnviromentDetails()).toDeepEqual({
      environmentUrl: `https://${EnvironmentConstants.TEST_CONV_HOSTNAME}`,
      productEnvironment: 'MOCK05',
      productName: 'Conversations'
    });
  });

  it('Test Environment', () => {
    expect(getCurrentEnvironment()).toEqual(EnvironmentConstants.TEST);
  });

  it('Prod Environment', () => {
    setProdConvDetails();
    expect(getCurrentEnvironment()).toEqual(EnvironmentConstants.PROD);
  });

  it('Staging Environment', () => {
    setStagingConvDetails();
    expect(getCurrentEnvironment()).toEqual(EnvironmentConstants.STAGING);
  });

  it('Dev Environment', () => {
    setDevConvDetails();
    expect(getCurrentEnvironment()).toEqual(EnvironmentConstants.DEV);
  });
});
