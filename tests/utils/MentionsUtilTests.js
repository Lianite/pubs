import * as MentionsUtils from 'utils/MentionsUtils';
import { MentionsRecord } from 'types/MentionsTypes';
import { ContentState, EditorState} from 'draft-js';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { getCompositeDecorator } from 'decorators/EditorDecorator';
import { MENTION_REGEX } from 'decorators/Strategies';

describe('Mentions Util Tests', () => {
  it('should insert a Facebook mention into a new editor state', () => {
    try {
      let contentState = ContentState.createFromText('@reddit');
      let contentBlock = contentState.getFirstBlock();
      let key = contentBlock.getKey();

      let mention = new MentionsRecord({
        'profileId': '7177913734',
        'screenName': 'testScreenName',
        'displayName': 'Reddit',
        'link': 'https://www.facebook.com/reddit/',
        'icon': 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
        'likes': 1026470,
        'category': 'News/Media Website',
        'location': 'Redditch, United Kingdom',
        'verified': true,
        'offsetKey': `${key}-0-0`
      });

      let editorState = EditorState.createWithContent(contentState, getCompositeDecorator(/a^/g, contentState, SupportedNetworks.FACEBOOK));

      let newEditor = MentionsUtils.insertMentionIntoEditorState(mention, editorState, SupportedNetworks.FACEBOOK);

      expect(newEditor).toBeTruthy();
      expect(newEditor.getCurrentContent().getPlainText()).toEqual('Reddit ');
    } catch (e) {
      expect(`${e.message} file: ${e.fileName} lineNumber: ${e.lineNumber}`).toBe('asd');
    }
  });

  it('should insert a Twitter mention into a new editor state', () => {
    let contentState = ContentState.createFromText('@reddit');
    let contentBlock = contentState.getFirstBlock();
    let key = contentBlock.getKey();

    let mention = new MentionsRecord({
      'profileId': '7177913734',
      'screenName': 'Deddit',
      'displayName': 'Deddit',
      'link': 'https://www.facebook.com/reddit/',
      'icon': 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
      'likes': 1026470,
      'category': 'News/Media Website',
      'location': 'Redditch, United Kingdom',
      'verified': true,
      'offsetKey': `${key}-0-0`
    });

    let editorState = EditorState.createWithContent(contentState, getCompositeDecorator(/a^/g, contentState, SupportedNetworks.TWITTER));

    let newEditor = MentionsUtils.insertMentionIntoEditorState(mention, editorState, SupportedNetworks.TWITTER);

    expect(newEditor).toBeTruthy();
    expect(newEditor.getCurrentContent().getPlainText()).toEqual('@Deddit ');
  });

  it('should throw an error when inserting a bad Facebook mention into a new editor state', () => {
    let contentState = ContentState.createFromText('@reddit');
    let contentBlock = contentState.getFirstBlock();
    let key = contentBlock.getKey();

    let mention = new MentionsRecord({
      'profileId': '7177913734',
      'screenName': 'testScreenName',
      'displayName': 'Reddit',
      'link': 'https://www.facebook.com/reddit/',
      'icon': 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
      'likes': 1026470,
      'category': 'News/Media Website',
      'location': 'Redditch, United Kingdom',
      'verified': true,
      'offsetKey': `${key}-0-0`
    });

    let editorState = EditorState.createEmpty(getCompositeDecorator(/a^/g, undefined, SupportedNetworks.FACEBOOK));

    expect(MentionsUtils.insertMentionIntoEditorState.bind(null, mention, editorState, SupportedNetworks.FACEBOOK)).toThrow(new Error('leaf is undefined, but mention was selected. This should not occur.'));
  });

  it('should return the correct string when using the Mentions Regex', () => {
    let twoWordMentionString = '@men tion';
    let twoWordMentionStringVerification = ['@men tion'];
    expect(twoWordMentionString.match(MENTION_REGEX)).toEqual(twoWordMentionStringVerification);

    const beginningOfString = '@men tion other text';
    const beginningOfStringVerification = ['@men tion'];
    expect(beginningOfString.match(MENTION_REGEX)).toEqual(beginningOfStringVerification);

    const endOfString = 'other text @mention';
    const endOfStringVerification = ['@mention'];
    expect(endOfString.match(MENTION_REGEX)).toEqual(endOfStringVerification);

    const middleOfString = 'other @men tion text';
    const middleOfStringVerification = ['@men tion'];
    expect(middleOfString.match(MENTION_REGEX)).toEqual(middleOfStringVerification);

    const emailString = 'notAMention@notAMention.com';
    const emailStringVerification = null;
    expect(emailString.match(MENTION_REGEX)).toEqual(emailStringVerification);

    const lineBreakString = '@mention \nother text';
    const lineBreakStringVerification = ['@mention'];
    expect(lineBreakString.match(MENTION_REGEX)).toEqual(lineBreakStringVerification);

    const secondLineString = 'other text\n@mention';
    const secondLineStringVerification = ['@mention'];
    expect(secondLineString.match(MENTION_REGEX)).toEqual(secondLineStringVerification);

    const twoTwoWordMentions = '@one mention @two mentions';
    const twoTwoWordMentionsVerification = ['@one mention', '@two mentions'];
    expect(twoTwoWordMentions.match(MENTION_REGEX)).toEqual(twoTwoWordMentionsVerification);

    const twoOneWordMentions = '@oneMention @twoMentions';
    const twoOneWordMentionsVerification = ['@oneMention', '@twoMentions'];
    expect(twoOneWordMentions.match(MENTION_REGEX)).toEqual(twoOneWordMentionsVerification);

    const numberMentions = '@oneMention1 @twoMentions2';
    const numberMentionsVerification = ['@oneMention1', '@twoMentions2'];
    expect(numberMentions.match(MENTION_REGEX)).toEqual(numberMentionsVerification);
  });

});
