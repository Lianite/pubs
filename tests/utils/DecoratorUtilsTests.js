/* @flow */
import * as DecoratorUtils from 'utils/DecoratorUtils';
import { ContentState } from 'draft-js';

describe('Decorator Utils Tests', () => {
  describe('findWithRegex', () => {
    const regex = /@test/g;
    let callbackResultsQueue = [];

    let mockCallback = (start: number, end: number) => {
      callbackResultsQueue.push({
        start,
        end
      });
    };

    beforeEach(() => {
      callbackResultsQueue = [];
    });

    it('should find basic regex in string and call callback over range', () => {
      const contentBlock = ContentState.createFromText('here is an @test for regex').getFirstBlock();
      DecoratorUtils.findWithRegex(regex, contentBlock, mockCallback);

      expect(callbackResultsQueue.length).toBe(1);
      const result = callbackResultsQueue.shift();
      expect(result.start).toBe(11);
      expect(result.end).toBe(16);
    });

    it('should not call callback when regex doesn\'t match', () => {
      const contentBlock = ContentState.createFromText('here is a test for regex').getFirstBlock();
      DecoratorUtils.findWithRegex(regex, contentBlock, mockCallback);

      expect(callbackResultsQueue.length).toBe(0);
    });

    it('should call callback multiple times when multiple matches', () => {
      const contentBlock = ContentState.createFromText('here is an @test for regex @test').getFirstBlock();
      DecoratorUtils.findWithRegex(regex, contentBlock, mockCallback);

      expect(callbackResultsQueue.length).toBe(2);
      let result = callbackResultsQueue.shift();
      expect(result.start).toBe(11);
      expect(result.end).toBe(16);
      result = callbackResultsQueue.shift();
      expect(result.start).toBe(27);
      expect(result.end).toBe(32);
    });

    it('should call callback only when limit is inside if limit is provided', () => {
      const contentBlock = ContentState.createFromText('here is an @test for regex @test').getFirstBlock();
      DecoratorUtils.findWithRegex(regex, contentBlock, mockCallback, 30);

      expect(callbackResultsQueue.length).toBe(1);
      let result = callbackResultsQueue.shift();
      expect(result.start).toBe(27);
      expect(result.end).toBe(30);
    });
  });
});
