import * as DateUtils from 'utils/DateUtils';
import moment from 'moment';

describe('Date Util Tests', () => {
  it('should create a valid Moment Object from a string', () => {
    let validDatetimeString = '04/06/1991, 12:00am';
    let validDatetimeUTCMoment = DateUtils.createUtcMomentFromString(validDatetimeString).local();

    expect(validDatetimeUTCMoment.isValid()).toBe(true);
    expect(validDatetimeUTCMoment.month() + 1).toBe(4);
    expect(validDatetimeUTCMoment.date()).toBe(6);
    expect(validDatetimeUTCMoment.year()).toBe(1991);
    expect(validDatetimeUTCMoment.hour()).toBe(0);

    validDatetimeString = 'August 1st';
    validDatetimeUTCMoment = DateUtils.createUtcMomentFromString(validDatetimeString).local();

    expect(validDatetimeUTCMoment.isValid()).toBe(true);
    expect(validDatetimeUTCMoment.month() + 1).toBe(8);
    expect(validDatetimeUTCMoment.date()).toBe(1);
    expect(validDatetimeUTCMoment.year()).toBe(new Date().getFullYear());
    expect(validDatetimeUTCMoment.hour()).toBe(0);
  });

  it('should create an invalid Moment Object from an invalid string', () => {
    let invalidDatetimeString = 'not a date';
    let invalidDatetimeUTCMoment = DateUtils.createUtcMomentFromString(invalidDatetimeString);

    expect(invalidDatetimeUTCMoment.isValid()).toBe(false);

    invalidDatetimeUTCMoment = DateUtils.createUtcMomentFromString();

    expect(invalidDatetimeUTCMoment).toBe(null);
  });

  it('format a Moment as a string', () => {
    let validDatetimeString = '04/06/1991, 12:00am';
    let validDatetimeUTCMoment = DateUtils.createUtcMomentFromString(validDatetimeString);

    expect(validDatetimeUTCMoment.isValid()).toBe(true);

    expect(DateUtils.formatDatetimeForTextInput(validDatetimeUTCMoment)).toBe(validDatetimeString);
  });

  it('adjusts a moment per a set number of hours and minutes', () => {
    let initialMoment = DateUtils.createUtcMomentFromString('04/06/1991, 12:00am');
    let modifiedMoment = DateUtils.adjustUtcMomentTime(initialMoment, '1:30am', 0);

    expect(modifiedMoment.local().hours()).toBe(1);
    expect(modifiedMoment.local().minutes()).toBe(30);

    modifiedMoment = DateUtils.adjustUtcMomentTime(null, '1:30am', 0);

    expect(modifiedMoment.local().hours()).toBe(1);
    expect(modifiedMoment.local().minutes()).toBe(30);

    modifiedMoment = DateUtils.adjustUtcMomentTime(null, 'not actually a time', 0);
    expect(modifiedMoment.isValid()).toBe(false);
  });

  it('consistently formats a moment datetime for text input', () => {
    let initialMoment = DateUtils.createUtcMomentFromString('April 6th, 1991');
    let datetimeString = DateUtils.formatDatetimeForTextInput(initialMoment);
    expect(datetimeString).toBe('04/06/1991, 12:00am');

    datetimeString = DateUtils.formatDatetimeForTextInput(null);
    expect(datetimeString).toBe('');
  });

  it('consistently formats a moment time', () => {
    let initialMoment = DateUtils.createUtcMomentFromString('April 6th, 1991');
    let datetimeString = DateUtils.formatTimeForTextInput(initialMoment);
    expect(datetimeString).toBe('12:00am');

    datetimeString = DateUtils.formatTimeForTextInput(null);
    expect(datetimeString).toBe('');
  });

  it('consistently formats a moment datetime for notes', () => {
    let initialMoment = DateUtils.createUtcMomentFromString('April 6th, 1991');
    let datetimeString = DateUtils.formatDatetimeForNoteDisplay(initialMoment);
    expect(datetimeString).toBe('Apr 6, 12:00am');

    datetimeString = DateUtils.formatDatetimeForNoteDisplay(null);
    expect(datetimeString).toBe('');
  });

  it('accurately compares a date to see if it\'s in the past or is current/in the future', () => {
    let initialMoment = DateUtils.createUtcMomentFromString('April 6th, 1991');
    expect(DateUtils.datetimeIsCurrent(initialMoment, 0)).toBe(false);
    initialMoment = DateUtils.getDefaultScheduleTime(0);
    expect(DateUtils.datetimeIsCurrent(initialMoment, 0)).toBe(true);
  });

  it('accurately compares a date, but not time, to see if it\'s in the past or is current/in the future', () => {
    let initialMoment = DateUtils.createUtcMomentFromString('April 6th, 1991');
    expect(DateUtils.dateIsCurrent(initialMoment, 0)).toBe(false);
    initialMoment = DateUtils.getDefaultScheduleTime(0);
    expect(DateUtils.dateIsCurrent(initialMoment, 0)).toBe(true);
  });

  it('creates a utc moment from a js date', () => {
    let date = new Date('1991-04-06');
    expect(date instanceof Date).toBe(true);
    date = DateUtils.createUtcMomentFromDate(date);
    expect(date instanceof Date).toBe(false);
    expect(date instanceof moment).toBe(true);
    date = DateUtils.createUtcMomentFromDate();
    expect(date).toBe(null);
  });

  it('adjusts the date of a moment date based on a new date', () => {
    const initialMoment = DateUtils.createUtcMomentFromString('April 6th, 1991');
    const currentMoment = DateUtils.getDefaultScheduleTime(0);
    const newMoment = DateUtils.createUtcMomentFromString('September 1st, 2015');
    const newFutureMoment = DateUtils.createUtcMomentFromString('September 1st, 2050');
    expect(initialMoment.isSame(newMoment, 'day')).toBe(false);

    let adjustedMoment = DateUtils.adjustUtcMomentDate(initialMoment, newMoment, 0);
    expect(adjustedMoment.isSame(currentMoment, 'day')).toBe(true);

    adjustedMoment = DateUtils.adjustUtcMomentDate(null, newMoment, 0);
    expect(adjustedMoment.isSame(currentMoment, 'day')).toBe(true);

    adjustedMoment = DateUtils.adjustUtcMomentDate(null, newFutureMoment, 0);
    expect(adjustedMoment.isSame(newFutureMoment, 'day')).toBe(true);

    adjustedMoment = DateUtils.adjustUtcMomentDate(initialMoment, newFutureMoment, 0);
    expect(adjustedMoment.isSame(newFutureMoment, 'day')).toBe(true);
  });
});
