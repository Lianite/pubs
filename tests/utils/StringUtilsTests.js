import * as StringUtils from 'utils/StringUtils';

describe('String Util Tests', () => {
  it('should capitalize the first letter of the string passed', () => {
    const testString = 'test';
    expect(StringUtils.capitalizeFirstLetter(testString)).toEqual('Test');
  });

  it('should capitalize the first letter of the string passed and add a past tense', () => {
    const testString = 'test';
    expect(StringUtils.capitalizeFirstLetterActivity(testString)).toEqual('Tested');

    const testString2 = 'create';
    expect(StringUtils.capitalizeFirstLetterActivity(testString2)).toEqual('Created');
  });
});
