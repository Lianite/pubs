/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

import type { GetTargetingAutocompleteResponse } from 'apis/FacebookApi';

function facebookApiRequest(url: string, { status, response }): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

export const mockFetchLanguagesApiResponse = (): GetTargetingAutocompleteResponse => ({
  data: {
    targetingResults: [
      {
        value: 'Test',
        description: 'Test',
        type: 'LOCALE'
      },
      {
        value: 'Test2',
        description: 'Test2',
        type: 'LOCALE'
      }
    ]
  }
});

export function stubFetchLanguagesApiResponse({
  credentialUniqueIds,
  companyId,
  campaignId,
  query,
  response
}: {
  credentialUniqueIds: Array<string>,
  companyId: string,
  campaignId: string,
  query: string,
  response: any
}) {
  const url = `/ws/company/${companyId}/campaign/${campaignId}/targeting/autocomplete/?targetingType=LOCALE&service=FACEBOOK&credentialUniqueIds%5B%5D=${credentialUniqueIds.join(',')}&startsWith=${query}`;
  facebookApiRequest(url, { status, response });
}

export const mockFetchLocationsApiResponse = (): GetTargetingAutocompleteResponse => ({
  data: {
    targetingResults: [
      {
        value: '1',
        description: 'Germany',
        type: 'GEOGRAPHY',
        subType: 'COUNTRY'
      },
      {
        value: '2',
        description: 'United States',
        type: 'GEOGRAPHY',
        subType: 'COUNTRY'
      }
    ]
  }
});

export function stubFetchLocationsApiResponse({
  credentialUniqueIds,
  companyId,
  campaignId,
  subType,
  query,
  response
}: {
  credentialUniqueIds: Array<string>,
  companyId: string,
  campaignId: string,
  subType: string,
  query: string,
  response: any
}) {
  const url = `/ws/company/${companyId}/campaign/${campaignId}/targeting/autocomplete/?targetingType=GEOLOCATION&service=FACEBOOK&credentialUniqueIds%5B%5D=${credentialUniqueIds.join(',')}&startsWith=${query}`;
  facebookApiRequest(url, { status, response });
}
