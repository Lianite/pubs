/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

import type { GetVoicesResponse } from 'apis/AccountsApi';

type StubMessageApiRequestParam = {
  status?: number;
  response?: any;
};
function messageApiRequest(url: string, { status, response }: StubMessageApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

type MockGetVoicesResponseParam = {
  companyId: string;
  initiativeId: string;
};
export function mockGetVoicesResponse({ companyId, initiativeId }: MockGetVoicesResponseParam): GetVoicesResponse {
  return [
    {
      data: {
        id: 1,
        name: 'Mock Voice 1',
        credentials: [
          {
            data: {
              accountType: 'USER',
              companyId: parseInt(companyId, 10),
              id: 2,
              name: 'Mock Credential 1',
              service: 'FACEBOOK',
              socialNetworkId: '123123',
              targetingSupported: false,
              uniqueId: '1234-123-mock-uuid',
              voiceId: 1,
              organicVisibilitySupported: false
            },
            nextActions: {},
            uri: `/ws/company/${companyId}/expandedVoice/credential/1`,
            permissions: [],
            verbs: []
          },
          {
            data: {
              accountType: 'USER',
              companyId: parseInt(companyId, 10),
              id: 3,
              name: 'Mock Credential 2',
              service: 'TWITTER',
              socialNetworkId: '65840585',
              targetingSupported: false,
              uniqueId: '1234-123-mock-uuid-2',
              voiceId: 1,
              organicVisibilitySupported: false
            },
            nextActions: {},
            uri: `/ws/company/${companyId}/expandedVoice/credential/1`,
            permissions: [],
            verbs: []
          }
        ]
      },
      nextActions: {},
      uri: `/ws/company/${companyId}/expandedVoice/${initiativeId}`,
      permissions: [],
      verbs: []
    }
  ];
}

type StubGetVoiceRequestParam = {
  companyId: string;
  initiativeId: string;
  status?: number;
  response?: GetVoicesResponse
}
export function stubGetVoicesRequest({ companyId, initiativeId, status, response }: StubGetVoiceRequestParam): void {
  const url = `/ws/company/${companyId}/expandedVoice/?campaignId=${initiativeId}&services=FACEBOOK&services=TWITTER&services=GOOGLEPLUS&socialObjectType=NOTE`;
  messageApiRequest(url, { status, response });
}
