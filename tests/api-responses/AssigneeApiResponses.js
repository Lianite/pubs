/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

type StubAssignApiRequestParam = {
  campaignId?: string;
  companyId?: string;
  userId?: number;
  status?: number;
  response?: Object;
};
function AssignApiRequest(url: string, { companyId, status, response }: StubAssignApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}


type mockGetAllAssignParam = {
  companyId: string,
  campaignId: string
};
export function mockGetAllAssignDataRequest({ companyId, campaignId }: mockGetAllAssignParam) {
  return {
    data:[
        {id: '3', firstName: 'x1', lastName: 'ACTIVE'},
        {id: '6', firstName: 'x2', lastName: 'ACTIVE'},
        {id: '5', firstName: 'x3', lastName: 'ACTIVE'},
        {id: '4', firstName: 'x4', lastName: 'ACTIVE'},
        {id: '2', firstName: 'x5', lastName: 'ACTIVE'},
        {id: '1', firstName: 'x6', lastName: 'ACTIVE'}
    ]
  };
}

type mockGetValidAssignParam = {
  companyId: string,
  campaignId: string
};
export function mockGetValidAssignDataRequest({ companyId, campaignId }: mockGetValidAssignParam) {
  return {
    data: [
        {id: '3', firstName: 'x1', lastName: 'ACTIVE'},
        {id: '6', firstName: 'x2', lastName: 'ACTIVE'},
        {id: '5', firstName: 'x3', lastName: 'ACTIVE'},
        {id: '4', firstName: 'x4', lastName: 'ACTIVE'},
        {id: '2', firstName: 'x5', lastName: 'ACTIVE'},
        {id: '1', firstName: 'x6', lastName: 'ACTIVE'}
    ]
  };
}

type mockGetAssigneeVoicesParam = {
  companyId: string,
  campaignId: string,
  userId: number
};
export function mockGetAssigneeVoicesRequest({ companyId, campaignId, userId }: mockGetAssigneeVoicesParam) {
  return {
    data: [
        {id: '3', name: 'x1'},
        {id: '6', name: 'x2'},
        {id: '5', name: 'x3'},
        {id: '4', name: 'x4'},
        {id: '2', name: 'x5'},
        {id: '1', name: 'x6'}
    ]
  };
}

export function stubGetAllAssigneeDataRequest({ companyId, campaignId, status, response }: StubAssignApiRequestParam): void {
  companyId = companyId || '';
  campaignId = campaignId || '';
  const url = `/ws/company/${companyId}/userForFilter?campaignIds=${campaignId}&pageSize=-1`;
  AssignApiRequest(url, { companyId, campaignId, status, response });
}

export function stubGetValidAssigneeDataRequest({ companyId, campaignId, status, response }: StubAssignApiRequestParam): void {
  companyId = companyId || '';
  campaignId = campaignId || '';
  const url = `/ws/multichannel/company/${companyId}/campaign/${campaignId}/voices/assignableUsers?voiceIDs=`;
  AssignApiRequest(url, { companyId, campaignId, status, response });
}

export function stubGetAssigneeVoices({ companyId, campaignId, userId, status, response }: StubAssignApiRequestParam): void {
  companyId = companyId || '';
  campaignId = campaignId || '';
  userId = userId || 0;
  const url = `/ws/company/${companyId}/expandedVoice/?campaignId=${campaignId}&services=FACEBOOK&services=TWITTER&services=GOOGLEPLUS&userId=${userId}`;
  AssignApiRequest(url, { companyId, campaignId, userId, status, response });
}
