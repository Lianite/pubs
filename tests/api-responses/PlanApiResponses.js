/* @flow */
import type { GetPlanDataResponse } from 'apis/PlanApi';

type MockGetPlanDataResponseParam = {
  companyId: string,
  campaignId: string
};
export function mockGetPlanDataRequest({ companyId, campaignId }: MockGetPlanDataResponseParam): GetPlanDataResponse {
  return {
    data:[{
      id:'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
      name:'Test 1',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    },
    {
      id:'debfba3a-7dfc-49f6-a3fd-c05eb2d92e01',
      name:'Test 2',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    },
    {
      id:'debfba3a-7dfc-49f6-aafd-c05eb2d90e01',
      name:'Test 3',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    },
    {
      id:'debfba3a-7dfc-49f6-a3fd-c05eb34390e01',
      name:'Test 4',
      currentStatus:'DRAFT"',
      status:'DRAFT',
      companyId:2,
      campaignId:3,
      campaignName:'Support',
      color:'#119146',
      planCountersDTO:{
        draftMessageCount:0,
        scheduledMessageCount:0,
        waitingApprovalMessageCount:0,
        publishedMessageCount:0,
        totalEngagementMessageCount:0
      }
    }],
    paging:{
      pageNumber:1,
      totalPages:1,
      pageSize:100,
      totalRecords:1
    }
  };
}
