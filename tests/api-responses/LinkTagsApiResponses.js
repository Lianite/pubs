/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubApiRequest } from 'tests/test-utils/stubRequest';
import { getLinkTagsEndpoint } from 'apis/LinkTagsApi';

const endpoint = getLinkTagsEndpoint();

export const getMockLinkTag = (id: number = 1) => ({
  id,
  url: 'foo.com',
  domain: 'foo.com',
  tagVariables:  [{
    id: 89,
    name: 'brand',
    type: 'select',
    variableDescription: {
      required: false,
      options: [{
        name: 'The Doghouse',
        value: 'DH',
        selected: false
      }, {
        name: 'Waggin Tails',
        value: 'WT',
        selected: false
      }],
      fieldName: 'brand'
    }
  }],
  variableValues: []
});

type StubLinkTagsApiRequestParam = {
  status?: number;
  response?: Object;
};
function linkTagsApiRequest(endpoint: string, path: string, { status, response }: StubLinkTagsApiRequestParam): void {
  stubApiRequest(endpoint, path)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

export function mockCreateLinkTag() {
  return getMockLinkTag();
}

export function mockCreateLinkTagError() {
  return {
    errors: [{
      code:'TAG_NO_DOMAIN_CONFIG_FOUND_FOR_URL',
      message:'No domain configuration could be found for the specified URL.'
    }]
  };
}

export function stubCreateLinkTag({status, response}: StubLinkTagsApiRequestParam): void {
  linkTagsApiRequest(endpoint, '/tags', {status, response});
}

export function mockSaveLinkTag(id: number) {
  return {
    ...getMockLinkTag(id),
    variableValues: [{
      variableId: 89,
      values: [{
        value: 'DH'
      }]
    }]
  };
}

export function stubSaveLinkTag(id: number, {status, response}: StubLinkTagsApiRequestParam): void {
  linkTagsApiRequest(endpoint, `/tags/${id}`, {status, response});
}

export function mockSaveLinkTagError() {
  return {
    errors: [{
      code:'TAG_INVALID_VARIABLE_VALUE',
      message:'Some error message'
    }]
  };
}

export function stubSaveLinkTagError(id: number, {status, response}: StubLinkTagsApiRequestParam): void {
  linkTagsApiRequest(endpoint, `/tags/${id}`, {status, response});
}
