/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

import type {
  GetCsrfTokenResponse,
  GetUserDataResponse,
  GetLastCampaignResponse,
  GetCapabilitiesResponse,
  GetCompanyDataResponse } from 'apis/EnvironmentApi';

type StubEnvironmentApiRequestParam = {
  campaignId?: string;
  companyId?: string;
  status?: number;
  response?: Object;
};
function environmentApiRequest(url: string, { companyId, status, response }: StubEnvironmentApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

export function mockCsrfTokenResponse(): GetCsrfTokenResponse {
  return {
    csrfToken: 'csrf-token-here'
  };
}

export function stubCsrfTokenRequest({ companyId, status, response }: StubEnvironmentApiRequestParam): void {
  let companyIdToUse = companyId || '';
  const url = `/ws/company/${companyIdToUse}/session/csrfToken/`;
  environmentApiRequest(url, { companyId, status, response });
}

type MockGetUserDataResponseParam = {
  companyId?: string;
  userId: number;
  userFirstName: string;
  userLastName: string;
  userAvatar: string;
  permissionLevel: string,
  directoryUserId: number,
  email: string
};
export function mockGetUserDataRequest({ companyId, userId, userFirstName, userLastName, userAvatar, permissionLevel, directoryUserId, email }: MockGetUserDataResponseParam): GetUserDataResponse {
  return {
    data: {
      id: userId,
      timezone: {
        abbreviation: 'CDT',
        name: 'American/Chicago',
        offset: '-500'
      },
      role: {
        publishingAccessLevel: {
          key: permissionLevel
        },
        name: '',
        privileges: []
      },
      firstName: userFirstName,
      lastName: userLastName,
      thumb: userAvatar,
      directoryUserId: directoryUserId,
      email: email
    },
    nextActions: {},
    uri: `/ws/company/${companyId || 2}/user/${userId || 2}`,
    permissions: [],
    verbs: []
  };
}

export function stubGetPlanDataRequest({ companyId, campaignId, status, response }: StubEnvironmentApiRequestParam): void {
  companyId = companyId || '';
  campaignId = campaignId || '';
  const url = `/ws/company/${companyId}/planner/plan?campaignIds%5B%5D=${campaignId}&excludePlanStatuses=COMPLETED&loadAll=true&pageSize=100&pageNumber=1`;
  environmentApiRequest(url, { companyId, status, response });
}

export function stubGetUserDataRequest({ companyId, status, response }: StubEnvironmentApiRequestParam): void {
  let companyIdToUse = companyId || '';
  const url = `/ws/company/${companyIdToUse}/session/currentUser/`;
  environmentApiRequest(url, { companyId, status, response });
}

type MockGetCampaignIdResponseParam = {
  campaignId: number;
};

export function mockGetCampaignIdRequest({ campaignId }: MockGetCampaignIdResponseParam): GetLastCampaignResponse {
  return {
    campaignId
  };
}

export function stubGetCampaignIdRequest({ status, response }: StubEnvironmentApiRequestParam): void {
  const url = '/user/get-last-campaign-id/';
  environmentApiRequest(url, { status, response });
}

export function mockCapabilitiesResponse(): GetCapabilitiesResponse {
  return {
    data: {
      foo: 'bar'
    }
  };
}

export function stubGetCapabilitiesRequest({ companyId, status, response }: StubEnvironmentApiRequestParam): void {
  let companyIdToUse = companyId || '';
  const url = `/ws/company/${companyIdToUse}/publishingCapability/`;
  environmentApiRequest(url, { companyId, status, response });
}


type mockGetCompanyDataRequestParam = {
  directoryId: number,
  companyId: number,
  companyName: string,
  features: Array<string>
};
export function mockGetCompanyDataRequest({ directoryId, companyId, features, companyName }: mockGetCompanyDataRequestParam): GetCompanyDataResponse {
  return {
    data: {
      companyMetadata: {
        contentLabelCreationRestricted: false,
        salesforceEnabled: false,
        shortenLinksByDefault: true
      },
      directoryId: directoryId,
      enabledFeatures: features,
      id: companyId,
      mediaConfigDTO: {
        imageAllowedMimeTypes: ['image/jpg,image/jpeg,image/png,image/gif,image/gif+animated']
      },
      name: companyName,
      publishingConfiguration: {
        smartPublishingPoliciesEnabled: false,
        smartPublishingSuggestionsEnabled: false
      },
      socialInboxConfig: {
        salesforceEnabled: false,
        socialCareWorkflowEnabled: false,
        streamRefreshSeconds: 150,
        twitterFeedbackCardsEnabled: false
      }
    },
    nextActions: {},
    permissions: [],
    uri: `/ws/company/${companyId || 2}`,
    verbs: []
  };
}
export function stubGetCompanyRequest({ companyId, status, response }: StubEnvironmentApiRequestParam): void {
  let companyIdToUse = companyId || 2;
  const url = `/ws/company/${companyIdToUse}/`;
  environmentApiRequest(url, { companyId, status, response });
}
