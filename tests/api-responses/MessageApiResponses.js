/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';
import { getCopyApiString } from 'utils/ApiUtils';
import moment from 'moment';

import type { LinkPreviewResponse } from 'apis/MessageApi';
import type { MessageObject, AssigneeData, ActivityPayloadData } from 'adapters/types';

type StubMessageApiRequestParam = {
  status?: number;
  response?: any;
};
function messageApiRequest(url: string, { status, response }: StubMessageApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

type StubSaveMessageRequestParam = {
  companyId: string;
  initiativeId: string;
  status?: number;
  response?: MessageObject;
  isCopy?: bool;
}
export function stubCreateMessagesRequest({ companyId, initiativeId, status, response, isCopy }: StubSaveMessageRequestParam): void {
  const copy = getCopyApiString(isCopy || false);
  const url = `/ws/multichannel/company/${companyId}/campaign/${initiativeId}/message${copy}`;
  messageApiRequest(url, { status, response });
}

type MockMessageResponseParam = {
  companyId: string,
  initiativeId: string,
  assignee?: AssigneeData,
  activityLogs?: Array<ActivityPayloadData>,
  eventId?: number,
  planId?: string,
  messageStatus: string,
  scheduledDatetime?: number,
  userId?: ?number
};
export function mockMessageResponse({ companyId, initiativeId, assignee, activityLogs, eventId, planId, userId, messageStatus, scheduledDatetime }: MockMessageResponseParam): MessageObject {
  let messageObj: MessageObject = {
    'planId': planId,
    'eventId': eventId || null,
    'status': messageStatus,
    'spredfastMessageTitle': 'my crappy title',
    'eventAuthorUserId': userId || 1,
    'userAuthorizedForEdit': true,
    'activityLogs': activityLogs || [{
      'id': 1,
      'author': {
        'id': 1,
        'firstName': 'Bob',
        'lastName': 'Kelso',
        'thumb': 'images/testimg.jpg',
        'defaultCompanyId': 1
      },
      'type': 'CREATE',
      'createdDate': {
        'date': (activityLogs ? activityLogs.createdDate.data : scheduledDatetime || 2525817600),
        'text': activityLogs ? activityLogs.createdDate.text : moment(scheduledDatetime).format('YYYY/MM/DD hh:mm:a') || '2050/01/15 12:00am',
        'timezone': {
          'name': 'American/Chicago',
          'abbreviation': 'CDT',
          'offset': '-600'
        }
      }
    }],
    'assignee': {
      'firstName': 'Bob',
      'lastName': 'Kelso',
      'id': 1,
      'conflictingVoices': [],
      'voices': [],
      'invalid': false
    },
    'schedule': {
      immediatePublish: !!scheduledDatetime
    },
    'singleChannelMessages': [
      {
        'socialNetwork': 'FACEBOOK',
        'messageContent': {
          'text': 'FB content',
          'imageIDs': [1],
          'linkPreview': {
            'linkUrl': 'www.nfl.com',
            'thumbnailUrl': 'http://i.nflcdn.com/static/content/public/static/img/share/shield.jpg',
            'description': 'The official source for NFL news, video highlights, fantasy football, game-day coverage, schedules, stats, scores and more.',
            'caption': 'www.nfl.com',
            'title': 'NFL.com - Official Site of the National Football League',
            'videoSrc': '',
            'videoLink': ''
          },
          linkDTOs: [{
            fullUrl: 'www.nfl.com',
            shortenOnPublish: true,
            addLinkTags: true
          }],
          'links': null,
          video: null,
          targetingProfile: {
            service: 'FACEBOOK',
            type: 'GENERAL',
            targetingValues: {
              GEOGRAPHY: [{
                value: 'US',
                description: 'United States',
                subType: 'COUNTRY',
                type: 'GEOGRAPHY'
              }, {
                value: 'LDN',
                description: 'London',
                subType: 'METRO',
                type: 'GEOGRAPHY'
              }],
              LOCALE: [{
                value: 'ES',
                description: 'Spanish',
                type: 'LOCALE'
              }, {
                value: 'EN',
                description: 'English',
                type: 'LOCALE'
              }],
              'GENDER': [{
                'type': 'GENDER',
                'value': '1',
                'description': 'Male'
              }]
            }
          }
        },
        'socialNetworkAccounts': [{
          'socialCredentialType': 'PAGE',
          'id': 255,
          'name': 'Page: SF_Test_Marketing',
          'socialNetworkId': '1039895926045432',
          'uniqueId': '99401e8b0f76e57d306d572b00a28a47',
          authenticated: true,
          targetingSupported: true
        }, {
          'socialCredentialType': 'PAGE',
          'id': 256,
          'name': 'Page: SF_Test_Support',
          'socialNetworkId': '174544616273324',
          'uniqueId': '8579c03719c01e46db822cce2c7605f9',
          authenticated: false,
          targetingSupported: false
        }],
        organicVisibility: 'INVISIBLE'
      }, {
        'socialNetwork': 'TWITTER',
        'messageContent': {
          'text': 'Twitter content a second time',
          'imageIDs': [1],
          'linkPreview': null,
          'linkDTOs': [],
          'links': null,
          'video': null
        },
        'socialNetworkAccounts': [{
          'socialCredentialType': 'USER',
          'id': 257,
          'name': 'wbruce_sf_test',
          'socialNetworkId': '738049322432876545',
          'uniqueId': '3756d04ef7010042d1ab6d71917f6013',
          authenticated: true,
          targetingSupported: false
        }],
        organicVisibility: 'VISIBLE'
      }],
    imagesById: {
      '1': {
        id: 1,
        originalUrl: 'sf-mock-imageurl.com',
        thumbnailUrl: 'sf-mock-imageurl-thumb.com'
      }
    },
    mentions: [
      {
        offsetBegin: 0,
        offsetEnd: 0,
        displayName: 'Facbeook Spredfast Test Page',
        id: 1,
        link: 'https://spredfast.test.com',
        profileId: '123123',
        service: 'FACEBOOK'
      }
    ],
    linksByUrl: {
      'https://spredfast.test.com': {
        fullUrl: 'https://spredfast.test.com',
        shortenOnPublish: false,
        addLinkTags: false
      }
    },
    contentLabels: [],
    notes: [{
      authorFirstName: 'test',
      authorLastName: 'user',
      authorAvatarThumbnail: 'http://thumbnail-uri.com',
      noteCreationTimestamp: 1479159612,
      noteText: 'this is a note'
    }]
  };

  if (scheduledDatetime && messageObj.schedule) {
    messageObj.schedule.scheduledPublishDate = scheduledDatetime;
  }

  return messageObj;
}

export function mockGetLinkPreviewResponse(): LinkPreviewResponse {
  return {
    linkUrl: 'http://www.facebook.com',
    description: 'Create an account or log into Facebook. Connect with friends, family and other people you know. Share photos and videos, send messages and get updates.',
    caption: 'www.facebook.com',
    title: 'Facebook - Log In or Sign Up',
    sourceMedia: [
      {
        type: 'image',
        src: 'https://www.facebook.com/images/fb_icon_325x325.png',
        videoSrc: null,
        videoLink: null,
        previewImg: null,
        width: 325,
        height: 325
      },
      {
        type: 'image',
        src: 'https://scontent.xx.fbcdn.net/t39.2365-6/851565_602269956474188_918638970_n.png',
        videoSrc: null,
        videoLink: null,
        previewImg: null,
        width: 48,
        height: 43
      }
    ],
    previewLinkValidationNeeded: false
  };
}

type StubReadMessageRequestParam = {
  companyId: string,
  initiativeId: string,
  eventId: string,
  planId?: string,
  status?: number,
  response?: MessageObject,
  isCopy?: bool
};

export function stubReadMessagesRequest({ companyId, initiativeId, eventId, status, response, isCopy }: StubReadMessageRequestParam): void {
  const copy = isCopy ? '/copy' : '';
  const url = `/ws/multichannel/company/${companyId}/campaign/${initiativeId}/message${copy}/${eventId}`;
  messageApiRequest(url, { status, response });
}

export function stubEditMessagesRequest({ companyId, initiativeId, eventId, status, response }: StubReadMessageRequestParam): void {
  const url = `/ws/multichannel/company/${companyId}/campaign/${initiativeId}/message/${eventId}`;
  messageApiRequest(url, { status, response });
}

type StubGetLinkPreviewRequestParam = {
  companyId: string,
  campaignId: string,
  url: string,
  status?: number,
  response?: LinkPreviewResponse
};

export function stubGetLinkPreviewRequest({companyId, campaignId, url, status, response}: StubGetLinkPreviewRequestParam): void {
  const encodedUrl = encodeURIComponent(url);
  const requestUrl = `/ws/company/${companyId}/campaign/${campaignId}/linkPreview/?url=${encodedUrl}&forceFetch=false`;

  messageApiRequest(requestUrl, { status, response });
}
