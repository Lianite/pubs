/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';
import _ from 'lodash';

import type { MentionQueryResponse } from 'types/MentionsTypes';

type StubMessageApiRequestParam = {
  status?: number;
  response?: any;
};
function mentionsApiRequest(url: string, { status, response }: StubMessageApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

type MockGetVoicesResponseParam = {
  companyId: string;
  initiativeId: string;
};
export function mockMentionsQueryResponse({ companyId, initiativeId }: MockGetVoicesResponseParam): MentionQueryResponse {
  return (
    {
      'data': [
        {
          'profileId':'7177913734',
          'screenName':'',
          'displayName':'Reddit',
          'link':'https://www.facebook.com/reddit/',
          'icon':'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
          'likes':1026470,
          'category':'News/Media Website',
          'location':'',
          'verified':true
        },
        {
          'profileId':'112236622122442',
          'screenName':'',
          'displayName':'Redditch',
          'link':'https://www.facebook.com/pages/Redditch/112236622122442',
          'icon':'https://external.xx.fbcdn.net/safe_image.php?d=AQADDfGggIw1hlms&w=50&h=50&url=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F45%2FRedditch_015.jpg%2F720px-Redditch_015.jpg&fallback=hub_city',
          'likes':8774,
          'category':'City',
          'location':'Redditch, United Kingdom',
          'verified':false
        },
        {
          'profileId':'394628833903112',
          'screenName':'',
          'displayName':'Redditch Advertiser',
          'link':'https://www.facebook.com/TheRedditchAdvertiser/',
          'icon':'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/1507741_958562310843092_5576055505895477039_n.jpg?oh=b2b1085f5311c03506e03b1607ce4014&oe=5888DDD1',
          'likes':4677,
          'category':'Media/News/Publishing',
          'location':'Redditch, United Kingdom',
          'verified':false
        },
        {
          'profileId':'333695420135234',
          'screenName':'',
          'displayName':'Redditch Standard',
          'link':'https://www.facebook.com/TheRedditchStandard2/',
          'icon':'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/11825794_448528501985258_6157774130394997267_n.png?oh=9a4d5e3eab6953e99545eaee447df88b&oe=58CAD24C',
          'likes':10257,
          'category':'Media/News/Publishing',
          'location':'20A Church Green East, Redditch, United Kingdom B98 8BP',
          'verified':false
        },
        {
          'profileId':'216466788372871',
          'screenName':'',
          'displayName':'Reddit Gifts',
          'link':'https://www.facebook.com/redditgifts/',
          'icon':'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/1451447_683985331621012_633620728_n.jpg?oh=03d42a65ee921a6b77dde6fad3b401ca&oe=58C1F109',
          'likes':42588,
          'category':'Website',
          'location':'',
          'verified':false
        }
      ],
      'nextActions':{},
      'uri':'/ws/company/2/campaign/4/mentions',
      'permissions':[],
      'verbs':[]
    });
}

type StubGetMentionsQuertRequestParam = {
  companyId: string;
  initiativeId: string;
  service: string;
  query: string;
  credentialUniqueId: string;
  date?: number;
  status?: number;
  response?: MentionQueryResponse;
}
export function stubGetMentionsQueryRequest({ companyId, initiativeId, service, query, credentialUniqueId = '', date = _.now(), status, response }: StubGetMentionsQuertRequestParam): void {
  const url = `/ws/company/${companyId}/campaign/${initiativeId}/mentions?q=${encodeURIComponent(query)}&service=${service}&credentialUniqueId=${credentialUniqueId}&_=${date}`;
  mentionsApiRequest(url, { status, response });
}
