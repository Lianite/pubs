/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

import type { Note } from 'adapters/types';

type StubNoteApiRequestParam = {|
  companyId: string;
  campaignId: string;
  messageId: number;
  note: Note;
  status?: number;
  response?: Object;
|};

function noteApiRequest(url: string, { companyId, campaignId, messageId, note, status, response }: StubNoteApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

export function mockCreateNoteResponse(): Note {
  return {
    noteText: 'test text',
    authorFirstName: 'fake',
    authorLastName: 'person',
    authorAvatarThumbnail: 'http://image.com',
    noteCreationTimestamp: 1234
  };
}

export function stubCreateNoteRequest({ companyId, campaignId, messageId, note, status, response }: StubNoteApiRequestParam): void {
  const noteToUse = note || {};
  const url = `/ws/multichannel/company/${companyId}/campaign/${campaignId}/message/note?messageId=${messageId}&note=${encodeURIComponent(noteToUse.noteText)}`;
  noteApiRequest(url, { companyId, campaignId, messageId, note, status, response });
}
