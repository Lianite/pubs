/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

type StubLabelsApiRequestParam = {
  campaignId?: string;
  companyId?: string;
  status?: number;
  response?: Object;
};
function lablesApiRequest(url: string, { companyId, status, response }: StubLabelsApiRequestParam): void {
  stubConvApiRequest(url)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}


type mockGetLabelsParam = {
  companyId: string,
  campaignId: string
};
export function mockGetLabels({ companyId, campaignId }: mockGetLabelsParam) {
  return {
    data:{
      contentLabels:[
        {id: 36, contentLabelGroupId: 1, title: 'x1', status: 'ACTIVE'},
        {id: 36, contentLabelGroupId: 2, title: 'x2', status: 'ACTIVE'},
        {id: 36, contentLabelGroupId: 3, title: 'x3', status: 'ACTIVE'},
        {id: 36, contentLabelGroupId: 4, title: 'x4', status: 'ACTIVE'},
        {id: 36, contentLabelGroupId: 5, title: 'x5', status: 'ACTIVE'},
        {id: 36, contentLabelGroupId: 6, title: 'x6', status: 'ACTIVE'}
      ]
    }
  };
}
export function stubGetLabels({ companyId, campaignId, status, response }: StubLabelsApiRequestParam): void {
  companyId = companyId || '';
  campaignId = campaignId || '';
  const url = `/ws/company/${companyId}/campaign/${campaignId}/contentLabel?`;
  lablesApiRequest(url, { companyId, campaignId, status, response });
}

type mockCreateLabelParam = {
  companyId: string,
  labelName: string
};
export function mockCreateLabel({companyId, labelName}: mockCreateLabelParam) {
  return {
    data:{
      companyId: 1,
      contentLabelScope:'GLOBAL',
      createdDate:{
        date: 1479931322588,
        text:'2016/11/23 02:02:02 PM',
        timezone:{name: 'America/Chicago', abbreviation: 'CST', offset: '-0600'}
      },
      id:42,
      priority:false,
      status:'ACTIVE',
      title:'asdfasdf'
    }
  };
}
export function stubCreateLabel({ companyId, labelName, status, response }: StubLabelsApiRequestParam): void {
  companyId = companyId || '';
  const url = `/ws/company/${companyId}/contentLabel`;
  lablesApiRequest(url, { companyId, status, response });
}
