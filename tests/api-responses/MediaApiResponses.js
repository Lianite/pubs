/* @flow */
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubConvApiRequest } from 'tests/test-utils/stubRequest';

import type {
  PostSocialImageResponse,
  GetSessionNonceToken,
  PostSocialVideoResponse,
  TranscodingProgressResponse,
  TranscodingResultsResponse } from 'apis/MediaApi';

type StubMediaApiRequestParam = {
  companyId?: string;
  campaignId?: string;
  status?: number;
  response?: any;
  videoKey?: string;
};

type StubUploadVideoApiRequestParam = {
  companyId?: string;
  campaignId?: string;
  status?: number;
  response?: Object;
  file: File,
  networks: Array<string>
};

type StubVideoTranscodingResultsRequestParam = {
  companyId?: string;
  campaignId?: string;
  status?: number;
  response?: TranscodingResultsResponse;
  videoKey?: string;
}

function mediaApiRequest(url: string, { companyId, campaignId, status, response }: StubMediaApiRequestParam, endpoint: ?string): void {
  stubConvApiRequest(url, endpoint)
    .andReturn({
      status: status ? status : HTTPSTATUS.OK,
      responseText: response ? JSON.stringify(response) : ''
    });
}

export function mockImageUploadResponse(): PostSocialImageResponse {
  return {
    data: {
      assetID: null,
      assetVersion: null,
      displayName: 'displayName',
      fullImage: 'fullImage.png',
      id: 123,
      image: 'thumbImage.png',
      imageHeight: 1080,
      imageWidth: 1920,
      imageSizeAfterResizing: 21175,
      mimeType: 'image/png',
      spredfastSummary: null,
      spredfastThumbnailUrls: ['thumbImage.png'],
      spredfastTitle: '',
      summary: null,
      type: 'IMAGE'
    },
    nextActions: {},
    uri: '',
    permissions: [''],
    verbs: ['']
  };
};

export function stubImageUploadRequest({ companyId, campaignId, status, response }: StubMediaApiRequestParam): void {
  let companyIdToUse = companyId || '2';
  let campaignIdToUse = campaignId || '2';
  const url = `/ws/company/${companyIdToUse}/campaign/${campaignIdToUse}/message/socialImage/`;
  mediaApiRequest(url, {companyId, campaignId, status, response});
}

export function mockSessionNonceTokenResponse() : GetSessionNonceToken {
  return {
    data: 'aHash',
    nextActions: {},
    uri: 'someUrl',
    permissions: [],
    verbs: []
  };
};

export function stubSessionNonceTokenRequest({companyId, campaignId, status, response }: StubMediaApiRequestParam) {
  let companyIdToUse = companyId || '2';
  const url = `/ws/company/${companyIdToUse}/session/nonce`;
  mediaApiRequest(url, {companyId, campaignId, status, response});
};

export function mockVideoUploadResponse() : PostSocialVideoResponse {
  return {
    socialVideos: [{
      displayName: 'video name',
      duration: 0,
      id: 1,
      size: 1,
      summary: 'video summary',
      type: 'video/mp4',
      videoStatus: 'uploaded',
      videoThumbnail: {},
      targetService: 'FACEBOOK'
    }],
    videoKey: 'someKey'
  };
};

export function stubVideoUploadRequest({companyId, campaignId, file, networks, status, response}: StubUploadVideoApiRequestParam): void {
  let companyIdToUse = companyId || '2';
  let campaignIdToUse = campaignId || '2';
  const services = networks.length && `&service=${networks.join('&service=')}` || '';
  const url = `/ws/multichannel/company/${companyIdToUse}/campaign/${campaignIdToUse}/message/socialVideo?fileType=${encodeURI(file.type)}&fileSize=${file.size}${services}`;
  mediaApiRequest(url, {companyId, campaignId, status, response}, 'https://upload-mock.spredfast.com:8443');
}

export function mockVideoTranscodingProgressResponse() : TranscodingProgressResponse {
  return {
    '1': 100
  };
}

export function stubVideoTranscodingProgressRequest({companyId, campaignId, status, response, videoKey}: StubMediaApiRequestParam) {
  let companyIdToUse = companyId || '2';
  let campaignIdToUse = campaignId || '2';
  let videoKeyToUse = videoKey || 'videoKey';
  const url = `/ws/multichannel/company/${companyIdToUse}/campaign/${campaignIdToUse}/message/socialVideo/transcodingProgress/?videoKey=${videoKeyToUse}`;
  mediaApiRequest(url, {companyId, campaignId, status, response});
}

export function mockVideoTranscodingResultsResponse() : TranscodingResultsResponse {
  return [{
    videoId: 1,
    videoPreviewUrl: 'someUrl',
    videoStatus: 'transcoded',
    thumbnails: [],
    size: 1
  }];
}

export function stubVideoTranscodingResultsRequest({companyId, campaignId, status, response, videoKey} : StubVideoTranscodingResultsRequestParam) {
  let companyIdToUse = companyId || '2';
  let campaignIdToUse = campaignId || '2';
  let videoKeyToUse = videoKey || 'videoKey';
  const url = `/ws/multichannel/company/${companyIdToUse}/campaign/${campaignIdToUse}/message/socialVideo/transcodingResults/?videoKey=${videoKeyToUse}`;
  mediaApiRequest(url, {companyId, campaignId, status, response});
}
