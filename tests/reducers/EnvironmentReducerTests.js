/* @flow */
import * as environmentActions from 'actions/EnvironmentActions';
import { createStore } from 'redux';
import reducer, { EnvironmentState, TimezoneRecord } from 'reducers/EnvironmentReducer';
import { RequestStatus } from 'constants/ApplicationConstants';
import { List as ImmutableList, Map as ImmutableMap } from 'immutable';

import type { Store } from 'redux';


describe('Environment reducer', () => {
  let store: Store<EnvironmentState>;
  let state: EnvironmentState;

  beforeEach(() => {
    store = createStore(reducer);
    state = store.getState();
  });

  describe('initial state', () => {
    it('should have expected defaults', () => {
      expect(state.csrfToken).toEqual('');
      expect(state.csrfTokenStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.csrfTokenRequestError).toEqual('');
      expect(state.userId).toEqual(-1);
      expect(state.userDataStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.userDataRequestError).toEqual('');
      expect(state.plansData).toEqual(ImmutableList());
      expect(state.plansDataStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.plansDataError).toEqual('');
    });
  });

  describe('clear state', () => {
    it('should have expected defaults', () => {
      store.dispatch(environmentActions.clearEnvironment());
      expect(state.csrfToken).toEqual('');
      expect(state.csrfTokenStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.csrfTokenRequestError).toEqual('');
      expect(state.userId).toEqual(-1);
      expect(state.userDataStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.userDataRequestError).toEqual('');
      expect(state.plansData).toEqual(ImmutableList());
      expect(state.plansDataStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.plansDataError).toEqual('');
    });
  });

  describe('csrf token', () => {
    it('should change status on request', () => {
      store.dispatch(environmentActions.requestCsrfToken());
      expect(store.getState().csrfTokenStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store token', () => {
      store.dispatch(environmentActions.csrfTokenReceived('token_value'));
      expect(store.getState().csrfTokenStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().csrfToken).toEqual('token_value');
    });

    it('should change status on error and store the error', () => {
      store.dispatch(environmentActions.csrfTokenError('test error'));
      expect(store.getState().csrfTokenStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().csrfTokenRequestError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });
  });

  describe('user data', () => {
    it('should change status on request', () => {
      store.dispatch(environmentActions.requestUserData());
      expect(store.getState().userDataStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store id', () => {
      const userDataPayload = {
        id: 3,
        avatar: 'http://test-url.com',
        timezone: {
          abbreviation: 'CDT',
          name: 'American/Chicago',
          offset: '-0500'
        },
        viewAccess: true
      };

      store.dispatch(environmentActions.userDataReceived(userDataPayload));
      expect(store.getState().userDataStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().userId).toEqual(3);
      expect(store.getState().userAvatar).toEqual('http://test-url.com');
      expect(store.getState().userTimezone).toEqual(new TimezoneRecord(userDataPayload.timezone));
      expect(store.getState().viewAccess).toEqual(true);
    });

    it('should filter out the default avatar url', () => {
      const userDataPayload = {
        id: 3,
        avatar: '/images/default-avatar.jpg',
        timezone: {
          abbreviation: 'CDT',
          name: 'American/Chicago',
          offset: '-0500'
        },
        viewAccess: true
      };

      store.dispatch(environmentActions.userDataReceived(userDataPayload));
      expect(store.getState().userAvatar).toEqual('');
    });

    it('should change status on error and store the error', () => {
      store.dispatch(environmentActions.userDataError('test error'));
      expect(store.getState().userDataStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().userDataRequestError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });

    it('should set the user\'s ability to edit a message on action dispatch', () => {
      store.dispatch(environmentActions.userCanEditPermission(false));
      expect(store.getState().editAccess).toEqual(false);
      store.dispatch(environmentActions.userCanEditPermission(true));
      expect(store.getState().editAccess).toEqual(true);
    });
  });

  describe('campaign Id', () => {
    it('should change status on request', () => {
      store.dispatch(environmentActions.requestCampaignId());
      expect(store.getState().campaignIdStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received', () => {
      store.dispatch(environmentActions.campaignIdReceived());
      expect(store.getState().campaignIdStatus).toEqual(RequestStatus.STATUS_LOADED);
    });

    it('should change status on error and store the error', () => {
      store.dispatch(environmentActions.campaignIdError('test error'));
      expect(store.getState().campaignIdStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().campaignIdRequestError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });
  });

  describe('capabilities', () => {
    it('should change status on request', () => {
      store.dispatch(environmentActions.requestCapabilities());
      expect(store.getState().capabilitiesStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store id', () => {
      store.dispatch(environmentActions.capabilitiesReceived({foo:'bar'}));
      expect(store.getState().capabilitiesStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().capabilities.get('foo')).toEqual('bar');
    });

    it('should change status on error and store the error', () => {
      store.dispatch(environmentActions.capabilitiesError('test error'));
      expect(store.getState().capabilitiesStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().capabilitiesRequestError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });
  });

  describe('featureFlags', () => {
    it('should pick feature flags from the default state', () => {
      expect(store.getState().featureFlags).toDeepEqual(ImmutableMap({
        googleplus: true,
        linkTags: true,
        facebookTargeting: true
      }));
    });
  });

  describe('company', () => {
    it('should change status on request', () => {
      store.dispatch(environmentActions.requestCompanyData());
      expect(store.getState().companyStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store id', () => {
      store.dispatch(environmentActions.companyReceived({id:1}));
      expect(store.getState().companyStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().company.get('id')).toEqual(1);
    });

    it('should change status on error and store the error', () => {
      store.dispatch(environmentActions.companyDataError('test error'));
      expect(store.getState().companyStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().companyRequestError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });

    it('should add feature flag values to the store', () => {
      store.dispatch(environmentActions.multiChannelFeatureFlagReceived(true));
      expect(store.getState().multiChannelEnabled).toEqual(true);
    });
  });

  describe('unscheduled draft time', () => {
    it('should set the unscheduled draft time', () => {
      store.dispatch(environmentActions.setUnscheduledDraftDatetime(670896000));
      expect(store.getState().unscheduledDraftDatetime).toEqual(670896000);
    });
  });
});
