/* @flow */
import * as messageActions from 'actions/MessageActions';
import { createStore } from 'redux';
import { EditorState, ContentState } from 'draft-js';
import reducer, { MessageReducerState } from 'reducers/MessageReducer';
import { RequestStatus, DefaultNetwork } from 'constants/ApplicationConstants';
import { Map as ImmutableMap } from 'immutable';
import { getPlainTextFromEditorState } from 'utils/MessageUtils';
import { createMessageStateFromPayload } from 'adapters/MessageAdapter';
import { mockMessageResponse } from 'tests/api-responses/MessageApiResponses';
import moment from 'moment';

import type { Store } from 'redux';

describe('Message Reducers', () => {
  let store: Store<MessageReducerState>;

  beforeEach(() => {
    store = createStore(reducer);
  });

  //This will need to be changed when the message state is refactored
  describe('Add new messageTitle reducer', () => {
    it('should add a new messageTitle', () => {
      store.dispatch(messageActions.updateMessageTitle('Test'));
      const state: MessageReducerState = store.getState();
      expect(state.messageTitle).toBe('Test');//Specifically here
    });
  });

  describe('Add new messageEditorState reducer', () => {
    it('should add a new messageEditorState', () => {
      let tempEditorMap = ImmutableMap({
        [DefaultNetwork]: EditorState.createEmpty()
      });
      store.dispatch(messageActions.updateMessageBody(tempEditorMap));

      const state = store.getState();
      expect(state.messageEditorStateMap.get(DefaultNetwork).getCurrentContent().getPlainText()
        === EditorState.createEmpty().getCurrentContent().getPlainText()).toBeTruthy();
    });
  });

  describe('Add new requestSaveMessage reducer', () => {
    it('sets the saving flag correctly', () => {
      store.dispatch(messageActions.requestSaveMessage());
      expect(store.getState().saving).toBe(true);
    });
  });

  describe('Add new saveMessageCompleted reducer', () => {
    it('sets the saving flag correctly', () => {
      const tempEditorMap = store.getState().get('messageEditorStateMap').set('FACEBOOK', EditorState.createEmpty());
      store.dispatch(messageActions.requestSaveMessage());
      store.dispatch(messageActions.updateMessageBody(tempEditorMap));

      expect(store.getState().saving).toBe(true);

      expect(getPlainTextFromEditorState(store.getState().get('messageEditorStateMap').get('FACEBOOK'))
        === EditorState.createEmpty().getCurrentContent().getPlainText()).toBeTruthy();
    });
  });

  describe('Read message started reducer', () => {
    it('sets the status flag indicating reading the message has started', () => {
      store.dispatch(messageActions.readMessageStarted());
      expect(store.getState().loadingMessageStatus).toBe(RequestStatus.STATUS_REQUESTED);
    });
  });

  describe('Read message failed reducer', () => {
    it('sets the status flag indicating reading the message has started', () => {
      const errorMessage = 'Forbidden';
      store.dispatch(messageActions.readMessageFailed(errorMessage));
      expect(store.getState().loadingMessageStatus).toBe(RequestStatus.STATUS_ERROR);
      expect(store.getState().loadingMessageError).toBe(errorMessage);
    });
  });

  describe('Read message completed reducer', () => {
    it('sets the status flag indicating reading the message has completed', () => {
      const mockReadMessagePayload = mockMessageResponse({
        companyId: '1',
        initiativeId: '1',
        eventId: 123,
        planId: '1',
        userId: 1,
        messageStatus: 'DRAFT',
        scheduledDatetime: 123213123123
      });
      const messageActionPayload = createMessageStateFromPayload(mockReadMessagePayload, -500);
      store.dispatch(messageActions.readMessageCompleted(messageActionPayload.message));
      const state = store.getState();

      expect(state.eventId).toBe(messageActionPayload.message.eventId);
      expect(state.eventAuthorUserId).toBe(messageActionPayload.message.eventAuthorUserId);
      expect(state.messageStatus).toBe(messageActionPayload.message.status);
      expect(state.messageTitle).toBe(messageActionPayload.message.messageTitle);
      expect(state.messageEditorStateMap).toEqual(messageActionPayload.message.messageEditorStateMap);
      expect(state.loadingMessageStatus).toBe(RequestStatus.STATUS_LOADED);
      expect(state.scheduledDatetime).toEqual(messageActionPayload.message.scheduledDatetime);
      expect(state.activities).toEqual(messageActionPayload.message.activities);
    });
  });

  describe('Undo Apply All reducer', () => {
    it('should undo the apply to all', () => {
      let state = store.getState();

      const textToCopy = 'All channels text';
      let newMessageEditorStateMap = state.get('messageEditorStateMap');

      newMessageEditorStateMap = newMessageEditorStateMap.set('FACEBOOK', EditorState.createEmpty())
                                                         .set('TWITTER', EditorState.createWithContent(ContentState.createFromText(textToCopy)));

      store.dispatch(messageActions.updateMessageBody(newMessageEditorStateMap));
      state = store.getState();

      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get(DefaultNetwork))).toBe('');
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('FACEBOOK'))).toBe('');
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('TWITTER'))).toBe(textToCopy);

      store.dispatch(messageActions.applyToAllChannels({network: 'TWITTER', editorState: EditorState.createWithContent(ContentState.createFromText(textToCopy))}));

      state = store.getState();

      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get(DefaultNetwork))).toBe(textToCopy);
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('FACEBOOK'))).toBe(textToCopy);
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('TWITTER'))).toBe(textToCopy);

      store.dispatch(messageActions.undoApplyToAllToAlert('TWITTER'));

      state = store.getState();

      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get(DefaultNetwork))).toBe('');
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('FACEBOOK'))).toBe('');
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('TWITTER'))).toBe(textToCopy);
    });
  });

  describe('Apply to all channels reducer', () => {
    it('applys the same text to all editorState instances', () => {
      let state = store.getState();

      const textToCopy = 'All channels text';
      let newMessageEditorStateMap = state.get('messageEditorStateMap');

      newMessageEditorStateMap = newMessageEditorStateMap.set('FACEBOOK', EditorState.createEmpty())
                                                         .set('TWITTER', EditorState.createWithContent(ContentState.createFromText(textToCopy)));

      store.dispatch(messageActions.updateMessageBody(newMessageEditorStateMap));
      state = store.getState();

      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get(DefaultNetwork))).toBe('');
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('FACEBOOK'))).toBe('');
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('TWITTER'))).toBe(textToCopy);

      store.dispatch(messageActions.applyToAllChannels({network: 'TWITTER', editorState: EditorState.createWithContent(ContentState.createFromText(textToCopy))}));

      state = store.getState();

      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get(DefaultNetwork))).toBe(textToCopy);
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('FACEBOOK'))).toBe(textToCopy);
      expect(getPlainTextFromEditorState(state.get('messageEditorStateMap').get('TWITTER'))).toBe(textToCopy);
    });
  });

  describe('Add scheduledDatetime reducer', () => {
    it('Adds a scheduledtime and error to state', () => {
      const exampleMoment = moment('04/06/1991');
      store.dispatch(messageActions.setScheduledTime({
        scheduledDatetime: exampleMoment,
        scheduledDatetimeHasError: false
      }));

      expect(store.getState().scheduledDatetime.scheduledDatetime).toEqual(exampleMoment);
      expect(store.getState().scheduledDatetime.scheduledDatetimeHasError).toBe(false);
    });
  });
});
