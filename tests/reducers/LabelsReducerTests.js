/* @flow */
import * as labelActions from 'actions/LabelActions';
import { clearMessage } from 'actions/MessageActions';
import { createStore } from 'redux';
import { is, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import reducer, { LabelsReducerState } from 'reducers/LabelsReducer';
import type { Store } from 'redux';

describe('Labels State reducer', () => {
  let store: Store<LabelsReducerState>;
  let state: LabelsReducerState;
  const label = ImmutableMap({
    title: 'Test',
    id: '1'
  });
  let sLabels = ImmutableList([label, label]);
  let aLabels = sLabels.toMap();

  beforeEach(() => {
    store = createStore(reducer);
    state = store.getState();
  });

  describe('initial state', () => {
    it('should have expected defaults', () => {
      expect(state.toJS()).toEqual({selectedLabels:[], availableLabels:{}, currentLabelText: ''});
    });
  });

  describe('ADD_LABEL', () => {
    it('should add a new label to selectedLabels', () => {
      store.dispatch(labelActions.addLabelToMessage(label.toJS()));
      state = store.getState();
      expect(state.selectedLabels.some((slabel) => {
        return is(label, slabel);
      })).toEqual(true);
    });
  });

  describe('REMOVE_LABEL', () => {
    beforeEach(() => {
      store.dispatch(labelActions.addLabelToMessage(label));
    });

    it('should remove label from selectedLabels', () => {
      store.dispatch(labelActions.removeLabel(label.get('title')));
      state = store.getState();
      expect(state.selectedLabels.isEmpty()).toEqual(true);
    });
  });

  describe('LABELS_LOADED', () => {
    it('should load all availableLabels', () => {
      store.dispatch(labelActions.labelsLoaded(aLabels));
      state = store.getState();
      expect(is(state.availableLabels, aLabels)).toEqual(true);
    });
  });

  describe('LABELS_READ_COMPLETE', () => {
    it('should load all selectedLabels', () => {
      store.dispatch(labelActions.readContentLabelsCompleted(sLabels.toJS()));
      state = store.getState();
      expect(is(state.selectedLabels, sLabels)).toEqual(true);
    });
  });

  describe('CLEAR_MESSAGE', () => {
    it('should return availableLabels to its defaultState', () => {
      store.dispatch(clearMessage());
      state = store.getState();
      expect(state.availableLabels.isEmpty()).toEqual(true);
    });
  });

});
