/* @flow */
import * as accountsActions from 'actions/AccountsActions';
import { updateCurrentAssignee } from 'actions/AssigneeActions';
import { AssigneeRecord } from 'records/AssigneeRecords';
import createStore from 'createStore';
import { SupportedNetworks, VoiceFilteringConstants } from 'constants/ApplicationConstants';
import { FacebookActionConstants } from 'constants/ActionConstants';
import { CredentialRecord } from 'records/AccountsRecord';
import { List as ImmutableList, Map as ImmutableMap } from 'immutable';
import { clearMessage } from 'actions/MessageActions';
import { AccountsState } from 'records/AccountsRecord';

import type { Store } from 'redux';
import _ from 'lodash';


describe('Accounts Reducers', () => {
  let store: Store<AccountsState>;

  let actionQueue = [];

  const mockDispatch = (action) => actionQueue.push(action);

  const getCredentialWithId = (id: string) => store.getState().accounts.checkedCredentials.get(id);

  beforeEach(() => {
    store = createStore();
    actionQueue = [];
  });

  describe('Add new fetchVoicesSuccessful reducer', () => {
    it('should populate the voices state element', () => {
      store.dispatch(accountsActions.fetchVoicesSuccessful([
        {
          data: {
            id: 1,
            name: 'Test 1',
            credentials: [
              {
                data: {
                  id: 1,
                  name: 'Page: Facebook Test',
                  service: 'FACEBOOK',
                  targetingSupported: true,
                  authenticated: false,
                  uniqueId: 'unique1'
                }
              },
              {
                data: {
                  id: 2,
                  name: 'Twitter Test',
                  service: 'TWITTER',
                  targetingSupported: false,
                  authenticated: true,
                  uniqueId: 'unique2'
                }
              }
            ]
          }
        }
      ]));
      store.dispatch(updateCurrentAssignee(new AssigneeRecord({
        lastName: 'McTest',
        id: '1',
        firstName: 'Test',
        voices: [{data:{id:1}}, {data:{id:2}}],
        conflictingVoices: []
      })));
      const state = store.getState().accounts;

      expect(state.voices[0].data.id).toBe(1);
      expect(state.voices[0].data.name).toBe('Test 1');
      expect(state.voices[0].data.credentials[0].data.id).toBe(1);
      expect(state.voices[0].data.credentials[0].data.name).toBe('Page: Facebook Test');
      expect(state.voices[0].data.credentials[0].data.service).toBe('FACEBOOK');
      expect(state.voices[0].data.credentials[0].data.targetingSupported).toBe(true);
      expect(state.voices[0].data.credentials[0].data.authenticated).toBe(false);
      expect(state.voices[0].data.credentials[1].data.id).toBe(2);
      expect(state.voices[0].data.credentials[1].data.name).toBe('Twitter Test');
      expect(state.voices[0].data.credentials[1].data.service).toBe('TWITTER');
      expect(state.voices[0].data.credentials[1].data.targetingSupported).toBe(false);
      expect(state.voices[0].data.credentials[1].data.authenticated).toBe(true);
    });
  });

  describe('Add new voiceCredentialCheckToggle reducer', () => {
    it('should check and uncheck tree correctly', () => {
      store.dispatch(accountsActions.fetchVoicesSuccessful([
        {
          data: {
            id: 1,
            name: 'Test 1',
            credentials: [
              {
                data: {
                  id: 1,
                  name: 'Page: Facebook Test',
                  service: 'FACEBOOK',
                  authenticated: false,
                  targetingSupported: true,
                  uniqueId: 'unique1'
                }
              },
              {
                data: {
                  id: 2,
                  name: 'Twitter Test',
                  service: 'TWITTER',
                  authenticated: true,
                  targetingSupported: false,
                  uniqueId: 'unique2'
                }
              }
            ]
          }
        }
      ]));
      store.dispatch(updateCurrentAssignee(new AssigneeRecord({
        lastName: 'McTest',
        id: '1',
        firstName: 'Test',
        voices: [{data:{id:1}}, {data:{id:2}}],
        conflictingVoices: []
      })));

      // Single Credential Checked
      store.dispatch(accountsActions.voiceChecked(1, 'unique1'));
      expect(getCredentialWithId('unique1') !== undefined).toBe(true);
      expect(getCredentialWithId('unique2') === undefined).toBe(true);

      // Single Voice Checked - Make sure that all credentials are checked for that voice
      store.dispatch(accountsActions.voiceChecked(1));
      expect(getCredentialWithId('unique1') !== undefined).toBe(true);
      expect(getCredentialWithId('unique1').get('authenticated')).toBe(false);
      expect(getCredentialWithId('unique1').get('targetingSupported')).toBe(true);
      expect(getCredentialWithId('unique2') !== undefined).toBe(true);
      expect(getCredentialWithId('unique2').get('authenticated')).toBe(true);
      expect(getCredentialWithId('unique2').get('targetingSupported')).toBe(false);

      // Single Credential Unchecked - Make sure that only that credential is unchecked
      store.dispatch(accountsActions.voiceChecked(1, 'unique1'));
      expect(getCredentialWithId('unique1')).toBe(undefined);
      expect(getCredentialWithId('unique2') !== undefined).toBe(true);

      // Check and uncheck single Voice - Make sure voice is undefined
      store.dispatch(accountsActions.voiceChecked(1));
      store.dispatch(accountsActions.voiceChecked(1));
      expect(getCredentialWithId('unique1')).toBe(undefined);
    });

    it('should clear facebook targeting if no facebook targeting accounts remain', () => {
      store.getState().accounts = store.getState().accounts.set('voices', [{
        data: {
          id: 1,
          name: 'Test 1',
          credentials: [
            {
              data: {
                id: 1,
                name: 'Page: Facebook Test',
                service: 'FACEBOOK',
                authenticated: false,
                targetingSupported: true,
                uniqueId: 'unique1'
              }
            },
            {
              data: {
                id: 2,
                name: 'Twitter Test',
                service: 'TWITTER',
                authenticated: true,
                targetingSupported: false,
                uniqueId: 'unique2'
              }
            }
          ]
        }
      }]);
      store.getState().assignee = store.getState().assignee.set('currentAssignee',
        new AssigneeRecord({
          lastName: 'McTest',
          id: 1,
          firstName: 'Test',
          voices: [{data:{id:1}}, {data:{id:2}}],
          conflictingVoices: []
        })
      );
      store.getState().accounts = store.getState().accounts.set('checkedCredentials', ImmutableList([
        new CredentialRecord({
          id: 1,
          name: 'Page: FacebookTest',
          targetingSupported: false,
          authenticated: true,
          socialNetwork: 'FACEBOOK',
          uniqueId: 'unique1'
        })
      ]));

      accountsActions.voiceChecked(1, 'unique1')(mockDispatch, store.getState);

      expect(actionQueue.filter((action: Object) => action.type === FacebookActionConstants.CLEAR_TARGETING).length).toEqual(1);
    });

    it('should not clear facebook targeting if valid facebook targeting accounts remain', () => {
      store.getState().accounts = store.getState().accounts.set('voices', [{
        data: {
          id: 1,
          name: 'Test 1',
          credentials: [
            {
              data: {
                id: 1,
                name: 'Page: Facebook Test',
                service: 'FACEBOOK',
                authenticated: true,
                targetingSupported: true,
                uniqueId: 'unique1'
              }
            },
            {
              data: {
                id: 2,
                name: 'Page Test 2',
                service: 'FACEBOOK',
                authenticated: true,
                targetingSupported: true,
                uniqueId: 'unique2'
              }
            }
          ]
        }
      }]);
      store.getState().accounts = store.getState().accounts.set('checkedCredentials', ImmutableList([
        new CredentialRecord({
          id: 1,
          name: 'Page: FacebookTest',
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK',
          uniqueId: 'unique1'
        }),
        new CredentialRecord({
          id: 2,
          name: 'Page test 2',
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK',
          uniqueId: 'unique2'
        })
      ]));

      mockDispatch(accountsActions.voiceChecked(1, 'unique1'));

      expect(actionQueue.filter((action: Object) => action.type === FacebookActionConstants.CLEAR_TARGETING).length).toEqual(0);
    });
  });

  describe('Add new quickSelectAllVoices reducer', () => {
    it('check and uncheck full list of credentials correctly', () => {
      store.dispatch(accountsActions.fetchVoicesSuccessful([
        {
          data: {
            id: 1,
            name: 'Test 1',
            credentials: [
              {
                data: {
                  id: 1,
                  name: 'Page: Facebook Test',
                  service: 'FACEBOOK',
                  uniqueId: 'unique1'
                }
              },
              {
                data: {
                  id: 2,
                  name: 'Twitter Test',
                  service: 'TWITTER',
                  uniqueId: 'unique2'
                }
              }
            ]
          }
        }
      ]));
      store.dispatch(updateCurrentAssignee(new AssigneeRecord({
        lastName: 'McTest',
        id: '1',
        firstName: 'Test',
        voices: [{data:{id:1}}, {data:{id:2}}],
        conflictingVoices: []
      })));

      // Select All check
      store.dispatch(accountsActions.quickSelectAllOrNone(true));
      expect(getCredentialWithId('unique1') !== undefined).toBe(true);
      expect(getCredentialWithId('unique2') !== undefined).toBe(true);

      // Clear All check
      store.dispatch(accountsActions.quickSelectAllOrNone(false));
      expect(getCredentialWithId('unique1') === undefined).toBe(true);
      expect(getCredentialWithId('unique2') === undefined).toBe(true);
    });
  });

  describe('Filtering accounts reducers', () => {
    it('should update the filter text used to filter displayed accounts', () => {
      const filter = 'test filter';
      store.dispatch(accountsActions.updateFilterText(filter));
      expect(store.getState().accounts.currentAccountFilterText).toBe(filter);
    });

    it('should update the channels used to filter displayed accounts', () => {
      const filteredAccounts = [SupportedNetworks.FACEBOOK, SupportedNetworks.TWITTER];

      store.dispatch(accountsActions.updateFilterChannels(SupportedNetworks.FACEBOOK));
      store.dispatch(accountsActions.updateFilterChannels(SupportedNetworks.TWITTER));
      expect(
        _.isEqual(store.getState().accounts.currentAccountChannelFilter.toJS(), filteredAccounts)
      ).toBe(true);

      // Remove all of the filters
      store.dispatch(accountsActions.updateFilterChannels());
      expect(
        _.isEqual(store.getState().accounts.currentAccountChannelFilter.toJS(), [])
      ).toBe(true);
    });

    it('should remove the filter channel when a preexisting one is passed in', () => {
      const filteredAccounts = [SupportedNetworks.FACEBOOK, SupportedNetworks.TWITTER];

      store.dispatch(accountsActions.updateFilterChannels(SupportedNetworks.FACEBOOK));
      store.dispatch(accountsActions.updateFilterChannels(SupportedNetworks.TWITTER));
      expect(
        _.isEqual(store.getState().accounts.currentAccountChannelFilter.toJS(), filteredAccounts)
      ).toBe(true);

      store.dispatch(accountsActions.updateFilterChannels(SupportedNetworks.FACEBOOK));
      expect(
        _.isEqual(store.getState().accounts.currentAccountChannelFilter.toJS(), [SupportedNetworks.TWITTER])
      ).toBe(true);
    });

    it('should update the selection filter used to filter displayed accounts', () => {
      const filteredSelection = VoiceFilteringConstants.FILTER_SELECTED;
      store.dispatch(accountsActions.updateFilterSelection(filteredSelection));
      expect(store.getState().accounts.currentAccountSelectionFilter).toEqual(VoiceFilteringConstants.FILTER_SELECTED);
    });

    it('should update the selection filter used to filter displayed accounts', () => {
      const filteredSelection = VoiceFilteringConstants.FILTER_SELECTED;
      store.dispatch(accountsActions.updateExclusiveFilterChannel(filteredSelection));
      expect(store.getState().accounts.currentAccountChannelFilter).toEqual(new ImmutableList([VoiceFilteringConstants.FILTER_SELECTED]));
    });
  });

  describe('Search on voices toggle', () => {
    it('should update the store to toggle on the flag for searching accounts by voice name', () => {
      expect(store.getState().accounts.searchOnVoices).toBeFalsy();
      store.dispatch(accountsActions.searchOnVoicesToggle());
      expect(store.getState().accounts.searchOnVoices).toBeTruthy();
    });
  });

  describe('Clear accounts reducer', () => {
    it('should clear the state when clear state is called', () => {
      store.getState().accounts.set('checkedCredentials', ImmutableList([
        new CredentialRecord({
          id: 1,
          name: 'Page: FacebookTest',
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK'
        }),
        new CredentialRecord({
          id: 2,
          name: 'Page test 2',
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK'
        })
      ]));
      store.dispatch(clearMessage());
      expect(store.getState().accounts.checkedCredentials.toJS()).toDeepEqual(new ImmutableMap().toJS());
    });

    it('should remove the networks checked credential', () => {
      const checkedCredentials = [
        new CredentialRecord({
          id: 1,
          name: 'Page: FacebookTest',
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'TWITTER',
          uniqueId: 'unique1'
        }),
        new CredentialRecord({
          id: 2,
          name: 'Page test 2',
          targetingSupported: true,
          authenticated: true,
          socialNetwork: 'FACEBOOK',
          uniqueId: 'unique2'
        })
      ];

      store.getState().accounts = store.getState().accounts.set('checkedCredentials', ImmutableList([checkedCredentials[0], checkedCredentials[1]]));
      store.dispatch(accountsActions.removeNetworkVoices(SupportedNetworks.FACEBOOK));
      expect(store.getState().accounts.checkedCredentials.toJS()).toDeepEqual([checkedCredentials[0].toJS()]);
    });
  });
});
