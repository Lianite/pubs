/* @flow */
import * as mediaActions from 'actions/MediaActions';
import { clearMessage } from 'actions/MessageActions';
import { createStore } from 'redux';
import reducer from 'reducers/ImageReducer';
import { ImageReducerState } from 'records/ImageRecords';
import { RequestStatus } from 'constants/ApplicationConstants';

import type { Store } from 'redux';

describe('Media reducer', () => {
  let store: Store<ImageReducerState>;
  let state: ImageReducerState;

  beforeEach(() => {
    store = createStore(reducer);
    state = store.getState();
  });

  describe('initial state', () => {
    it('should have expected defaults', () => {
      expect(state.imageUploadStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(state.imageUploadError).toEqual('');
      expect(state.imageId).toEqual(null);
      expect(state.imageFullUrl).toEqual('');
      expect(state.imageThumbUrl).toEqual('');
    });
  });

  describe('image upload', () => {
    it('should change status on request', () => {
      store.dispatch(mediaActions.requestUploadImage());
      expect(store.getState().imageUploadStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store token', () => {
      const sample_payload = {
        id: 3,
        fullUrl: 'fullUrl',
        thumbUrl: 'thumbUrl'
      };
      store.dispatch(mediaActions.uploadImageSucceeded(sample_payload));
      expect(store.getState().imageId).toEqual(sample_payload.id);
      expect(store.getState().imageFullUrl).toEqual(sample_payload.fullUrl);
      expect(store.getState().imageThumbUrl).toEqual(sample_payload.thumbUrl);
    });

    it('should change status on error and store the error', () => {
      store.dispatch(mediaActions.uploadImageFailed('test error'));
      expect(store.getState().imageUploadStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().imageUploadError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });
  });

  describe('image read', () => {
    it('update the image state when an images is read in on a message', () => {
      const mockImagesPayload = [{
        id: 1,
        originalUrl: 'spredfast-mock-image-url.com',
        thumbnailUrl: 'spredfast-mock-image-url-thumb.com',
        imageSizeAfterResizing: 10000,
        fileType: 'IMAGE',
        mimeType: 'image/jpeg',
        width: 100,
        height: 100
      }];
      store.dispatch(mediaActions.readImagesCompleted(mockImagesPayload));
      expect(store.getState().imageId).toEqual(mockImagesPayload[0].id);
      expect(store.getState().imageFullUrl).toEqual(mockImagesPayload[0].originalUrl);
      expect(store.getState().imageThumbUrl).toEqual(mockImagesPayload[0].thumbnailUrl);
      expect(store.getState().imageMimeType).toEqual(mockImagesPayload[0].mimeType);
      expect(store.getState().imageFilesize).toEqual(mockImagesPayload[0].imageSizeAfterResizing);
      expect(store.getState().imageUploadStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().imageWidth).toEqual(100);
      expect(store.getState().imageHeight).toEqual(100);
    });
  });

  describe('image save', () => {
    it('clears the list of images on a successful message save', () => {
      const mockImagesPayload = [{
        id: 1,
        originalUrl: 'spredfast-mock-image-url.com',
        thumbnailUrl: 'spredfast-mock-image-url-thumb.com'
      }];
      store.dispatch(mediaActions.readImagesCompleted(mockImagesPayload));
      store.dispatch(clearMessage());
      expect(store.getState().imageId).toEqual(null);
      expect(store.getState().imageFullUrl).toEqual('');
      expect(store.getState().imageThumbUrl).toEqual('');
      expect(store.getState().imageUploadStatus).toEqual(RequestStatus.STATUS_NEVER_REQUESTED);
    });
  });
});
