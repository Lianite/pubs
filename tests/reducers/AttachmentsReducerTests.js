/* @flow */
import * as mediaActions from 'actions/MediaActions';
import * as messageActions from 'actions/MessageActions';
import { createStore } from 'redux';
import reducer from 'reducers/VideosReducer';
import { SocialVideoRecord, VideosState } from 'records/VideosRecords';
import { RequestStatus } from 'constants/ApplicationConstants';
import { List as ImmutableList, Set as ImmutableSet } from 'immutable';
import type { Store } from 'redux';
import {
  mockVideoUploadResponse,
  mockVideoTranscodingProgressResponse,
  mockVideoTranscodingResultsResponse } from 'tests/api-responses/MediaApiResponses';

describe('Videos Reducers', () => {
  let store: Store<VideosState>;

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('Update the upload status when the video uploading starts', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.requestUploadVideo());
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          uploadStatus: RequestStatus.STATUS_REQUESTED
        })]),
        videoKey: ''
      });
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Update the upload percent', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoProgress(36));
      const state: VideosState = store.getState();
      expect(state.socialVideos.first().uploadPercent).toBe(36);
    });
  });

  describe('Update the uploaded video when the video is uploaded', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          displayName: 'video name',
          duration: 0,
          id: 1,
          videoSize: 1,
          summary: 'video summary',
          type: 'video/mp4',
          targetService: 'FACEBOOK',
          uploadStatus: RequestStatus.STATUS_LOADED
        })]),
        videoKey: 'someKey'
      });
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Update the uploaded video when the video upload fails', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoFailed('FAILED'));
      const state: VideosState = store.getState();
      expect(state.videoUploadAndTranscodeError).toBe('FAILED');
      expect(state.socialVideos.first().uploadStatus).toBe(RequestStatus.STATUS_ERROR);
    });
  });

  describe('Update the uploaded video when the video upload is canceled', () => {
    it('should update the properties', () => {
      store.dispatch(mediaActions.uploadVideoCanceled());
      const state: VideosState = store.getState();
      expect(state.videoUploadAndTranscodeError).toBe('');
      expect(state.socialVideos.first().uploadStatus).toBe(RequestStatus.STATUS_CANCELED);
      expect(state.socialVideos.first().transcodeProgressStatus).toBe(RequestStatus.STATUS_CANCELED);
      expect(state.socialVideos.first().transcodeResultsStatus).toBe(RequestStatus.STATUS_CANCELED);
    });
  });

  describe('Update the transcode progress status when the video transcoding starts', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      store.dispatch(mediaActions.requestTranscodeVideoProgress());
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          displayName: 'video name',
          duration: 0,
          id: 1,
          videoSize: 1,
          summary: 'video summary',
          type: 'video/mp4',
          targetService: 'FACEBOOK',
          uploadStatus: RequestStatus.STATUS_LOADED,
          transcodeProgressStatus: RequestStatus.STATUS_REQUESTED
        })]),
        videoKey: 'someKey'
      });
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Update the transcode progress when the video is transcoding', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      store.dispatch(mediaActions.requestTranscodeVideoProgress());
      store.dispatch(mediaActions.updateTranscodeVideoProgress(mockVideoTranscodingProgressResponse()));
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          displayName: 'video name',
          duration: 0,
          id: 1,
          videoSize: 1,
          summary: 'video summary',
          type: 'video/mp4',
          targetService: 'FACEBOOK',
          uploadStatus: RequestStatus.STATUS_LOADED,
          transcodeProgressStatus: RequestStatus.STATUS_REQUESTED,
          transcodeProgress: 100
        })]),
        videoKey: 'someKey'
      });
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Update the transcode progress status when the video transcoding ends', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      store.dispatch(mediaActions.transcodeVideoSucceeded());
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          displayName: 'video name',
          duration: 0,
          id: 1,
          videoSize: 1,
          summary: 'video summary',
          type: 'video/mp4',
          targetService: 'FACEBOOK',
          uploadStatus: RequestStatus.STATUS_LOADED,
          transcodeProgressStatus: RequestStatus.STATUS_LOADED
        })]),
        videoKey: 'someKey'
      });
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Update the transcode results status when the video transcoding results are requested', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      store.dispatch(mediaActions.requestTranscodeVideoResults());
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableList([new SocialVideoRecord({
          displayName: 'video name',
          duration: 0,
          id: 1,
          videoSize: 1,
          summary: 'video summary',
          type: 'video/mp4',
          targetService: 'FACEBOOK',
          uploadStatus: RequestStatus.STATUS_LOADED,
          transcodeResultsStatus: RequestStatus.STATUS_REQUESTED,
          thumbnails: ImmutableList(),
          thumbnailIndexSelected: -1
        })]),
        videoKey: 'someKey'
      });
      expect(state.toJS()).toDeepEqual(expectedVideo.toJS());
    });
  });

  describe('Update the transcode results status when the video transcoding results request ends', () => {
    it('should update the property', () => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      store.dispatch(mediaActions.requestTranscodeVideoProgress());
      store.dispatch(mediaActions.updateTranscodeVideoProgress(mockVideoTranscodingProgressResponse()));
      store.dispatch(mediaActions.transcodeVideoSucceeded());
      store.dispatch(mediaActions.transcodeVideoResultsSucceeded(mockVideoTranscodingResultsResponse()));
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState({
        socialVideos: ImmutableSet([new SocialVideoRecord({
          displayName: 'video name',
          duration: 0,
          id: 1,
          videoSize: 1,
          summary: 'video summary',
          type: 'video/mp4',
          targetService: 'FACEBOOK',
          uploadStatus: RequestStatus.STATUS_LOADED,
          transcodeProgressStatus: RequestStatus.STATUS_LOADED,
          transcodeResultsStatus: RequestStatus.STATUS_LOADED,
          transcodeProgress: 100,
          transcodedVideoSize: 1,
          thumbnails: ImmutableList([]),
          videoPreviewUrl: 'someUrl'
        })]),
        videoKey: 'someKey',
        videoUploadAndTranscodeError: ''
      });
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Update the video record when reset is required', () => {
    beforeEach(() => {
      store.dispatch(mediaActions.uploadVideoSucceeded(mockVideoUploadResponse()));
      store.dispatch(mediaActions.requestTranscodeVideoProgress());
      store.dispatch(mediaActions.updateTranscodeVideoProgress(mockVideoTranscodingProgressResponse()));
      store.dispatch(mediaActions.transcodeVideoSucceeded());
      store.dispatch(mediaActions.transcodeVideoResultsSucceeded(mockVideoTranscodingResultsResponse()));
    });

    it('should update on remove', () => {
      store.dispatch(mediaActions.removeUploadedVideo());
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState();
      expect(state.equals(expectedVideo)).toBeTruthy();
    });

    it('should update on clear message', () => {
      store.dispatch(messageActions.clearMessage());
      const state: VideosState = store.getState();
      const expectedVideo = new VideosState();
      expect(state.equals(expectedVideo)).toBeTruthy();
    });
  });

  describe('Read videos completed', () => {
    it('should store the action payload', () => {
      const socialVideoSet = ImmutableSet([
        new SocialVideoRecord({
          displayName: 'display name',
          duration: 123,
          id: 321,
          videoSize: 333,
          transcodedVideoSize: 222,
          summary: 'summary here',
          type: 'type',
          videoPreviewUrl: 'url',
          thumbnails: ImmutableList(),
          uploadStatus: RequestStatus.STATUS_LOADED,
          targetService: 'FACEBOOK',
          transcodeProgressStatus: RequestStatus.STATUS_LOADED,
          transcodeProgress: 100,
          transcodeResultsStatus: RequestStatus.STATUS_LOADED
        })
      ]);
      const payload = new VideosState({
        socialVideos: socialVideoSet,
        videoKey: ''
      });
      store.dispatch(mediaActions.readVideosCompleted(payload));
      expect(store.getState().equals(payload));
    });
  });

  describe('Update selected thumbnail', () => {
    it('should update selected thumbnail on change', () => {
      //First, hydrate some state
      const socialVideoSet = ImmutableSet([
        new SocialVideoRecord({
          displayName: 'display name',
          duration: 123,
          id: 321,
          videoSize: 333,
          transcodedVideoSize: 222,
          summary: 'summary here',
          type: 'type',
          videoPreviewUrl: 'url',
          thumbnails: ImmutableList([
            {
              uri: 'uri0',
              id: 'id0'
            },
            {
              uri: 'uri1',
              id: 'id1'
            }
          ]),
          uploadStatus: RequestStatus.STATUS_LOADED,
          targetService: 'FACEBOOK',
          transcodeProgressStatus: RequestStatus.STATUS_LOADED,
          transcodeProgress: 100,
          transcodeResultsStatus: RequestStatus.STATUS_LOADED
        })
      ]);

      const payload = new VideosState({
        socialVideos: socialVideoSet,
        videoKey: ''
      });
      store.dispatch(mediaActions.readVideosCompleted(payload));

      expect(store.getState().socialVideos.first().thumbnailIndexSelected).toBe(-1);

      store.dispatch(mediaActions.videoThumbnailFrameChanged({id: 'id1', uri: 'uri1'}));

      expect(store.getState().socialVideos.first().thumbnailIndexSelected).toBe(1);
    });
  });
});
