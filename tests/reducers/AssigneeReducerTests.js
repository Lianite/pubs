/* @flow */
import * as assigneeActions from 'actions/AssigneeActions';
import { createStore } from 'redux';
import { is, List as ImmutableList } from 'immutable';
import { AssigneeRecord, AssigneeReducerState } from 'records/AssigneeRecords';
import reducer from 'reducers/AssigneeReducer';
import { RequestStatus } from 'constants/ApplicationConstants';
import type { Store } from 'redux';

describe('Assign State reducer', () => {
  let store: Store<AssigneeReducerState>;
  let assignee = new AssigneeRecord({
    firstName: 'Bob',
    id: 1,
    lastName: 'McTest',
    conflictingVoices: []
  });

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('initial state', () => {
    let defaultAssignee = new AssigneeRecord();
    it('should have expected defaults', () => {
      expect(is(store.getState().currentAssignee, defaultAssignee)).toEqual(true);
    });
  });

  describe('update current assignee', () => {
    it('should update to the new currentAssignee when one is selected', () => {
      store.dispatch(assigneeActions.updateCurrentAssignee(assignee));
      expect(is(store.getState().currentAssignee, assignee)).toEqual(true);
    });
  });

  describe('all assign data', () => {
    it('should change status on request', () => {
      store.dispatch(assigneeActions.requestAllAssignees());
      expect(store.getState().allAssigneesStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store assignees', () => {
      const assigneeDataPayload = ImmutableList([assignee]);

      store.dispatch(assigneeActions.allAssigneesReceived(assigneeDataPayload));
      expect(store.getState().allAssigneesStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().allAssignees).toEqual(assigneeDataPayload);
    });

    it('should change status on error and store the error', () => {
      store.dispatch(assigneeActions.allAssigneesError('test error'));
      expect(store.getState().allAssigneesStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().allAssigneesError;
      expect(requestError).not.toBe('');
      expect(requestError).toEqual('test error');
    });
  });

  describe('valid assign data', () => {
    it('should change status on request', () => {
      store.dispatch(assigneeActions.requestValidAssignees());
      expect(store.getState().validAssigneesStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store assignees', () => {
      const assigneeDataPayload = ImmutableList([assignee]);

      store.dispatch(assigneeActions.validAssigneesReceived(assigneeDataPayload));
      expect(store.getState().validAssigneesStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().validAssignees).toEqual(assigneeDataPayload);
    });

    it('should change status on error and store the error', () => {
      store.dispatch(assigneeActions.valideAssigneesError('test error'));
      expect(store.getState().validAssigneesStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().valideAssigneesError;
      expect(requestError).not.toBe('');
      expect(requestError).toEqual('test error');
    });
  });

  describe('fetch assignee voices', () => {
    it('should change status on request', () => {
      store.dispatch(assigneeActions.fetchAssigneesVoicesStarted());
      expect(store.getState().fetchAssigneesVoicesStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store assignees', () => {
      const assigneeDataPayload = ImmutableList([assignee]);

      store.dispatch(assigneeActions.fetchConflictingAssigneesVoicesSuccessful(assigneeDataPayload));
      expect(store.getState().fetchAssigneesVoicesStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().allAssignees).toEqual(ImmutableList());
    });

    it('should change status on error and store the error', () => {
      store.dispatch(assigneeActions.fetchAssigneesVoicesError('test error'));
      expect(store.getState().fetchAssigneesVoicesStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().fetchAssigneesVoicesError;
      expect(requestError).not.toBe('');
      expect(requestError).toEqual('test error');
    });
  });
});
