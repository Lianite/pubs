/* @flow */
import * as linkTagActions from 'actions/LinkTagsActions';
import { createStore } from 'redux';
import reducer from 'reducers/LinkTagsReducer';
import {
  LinkTagsState,
  LinkTagPropertiesRecord,
  TagVariableValue } from 'records/LinkTagsRecords';
import {
  RequestStatus,
  DefaultNetwork } from 'constants/ApplicationConstants';
import {
  fromJS,
  Map as ImmutableMap,
  List as ImmutableList } from 'immutable';
import {
  getMockLinkTag } from 'tests/api-responses/LinkTagsApiResponses';

import type { Store } from 'redux';

describe('Link Tags Reducers', () => {
  let store: Store<LinkTagsState>;

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('Create link tag start reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      const state: LinkTagsState = store.getState();
      expect(state.equals(new LinkTagsState({
        urls: ImmutableMap({
          'foo.com': ImmutableMap({
            [DefaultNetwork]: new LinkTagPropertiesRecord({})
          })
        })
      }))).toBeTruthy();
    });
  });

  describe('Create link tag completed reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork,
        linkTag: getMockLinkTag()
      }));
      const state: LinkTagsState = store.getState();
      expect(state.equals(new LinkTagsState({
        urls: ImmutableMap({
          'foo.com': ImmutableMap({
            [DefaultNetwork]: new LinkTagPropertiesRecord({
              ...getMockLinkTag(),
              enabled: true
            })
          })
        })
      })));
    });
  });

  describe('Create link tag failed reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagFailed({
        link: 'foo.com',
        network: DefaultNetwork,
        error: 'some error'
      }));
      const state: LinkTagsState = store.getState();
      expect(state.equals(new LinkTagsState({
        urls: ImmutableMap({
          'foo.com': ImmutableMap({
            [DefaultNetwork]: new LinkTagPropertiesRecord({
              ...getMockLinkTag(),
              enabled: false
            })
          })
        })
      })));
    });
  });

  describe('Update Link Tag Variable reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork,
        linkTag: getMockLinkTag()
      }));
      store.dispatch(linkTagActions.updateLinkTagVariable({
        link: 'foo.com',
        tagVariableId: 89,
        tagVariableValue: [{value: 'SD'}],
        network: DefaultNetwork
      }));

      const state: LinkTagsState = store.getState();
      const exptectedLinkTag = new LinkTagPropertiesRecord({
        ...getMockLinkTag(),
        variableValues: ImmutableList([new TagVariableValue({
          variableId: 89,
          values: ImmutableList([{value: 'SD'}])
        })]),
        enabled: true,
        saved: false
      });

      const expectedState = new LinkTagsState({
        urls: ImmutableMap({
          'foo.com': ImmutableMap({
            [DefaultNetwork]: exptectedLinkTag
          })
        })
      });

      expect(state
        .getIn(['urls', 'foo.com', DefaultNetwork])
        .equals(expectedState.getIn(['urls', 'foo.com', DefaultNetwork])));
    });
  });

  describe('Update Link Tag Variable Values reducer', () => {
    it('should update the property', () => {
      const newValues = fromJS({
        variableId: 100,
        values: [{value: 'FOO'}]
      });
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork,
        linkTag: getMockLinkTag()
      }));
      store.dispatch(linkTagActions.updateLinkTagVariable({
        link: 'foo.com',
        tagVariableId: 89,
        tagVariableValue: [{value: 'SD'}],
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.updateLinkTagVariable({
        link: 'foo.com',
        tagVariableId: 90,
        tagVariableValue: [{value: 'AS'}],
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.updateLinkTagVariableValues({
        link: 'foo.com',
        newValues,
        network: DefaultNetwork
      }));

      const state: LinkTagsState = store.getState();
      const exptectedLinkTag = new LinkTagPropertiesRecord({
        ...getMockLinkTag(),
        variableValues: newValues,
        enabled: true,
        saveStatus: RequestStatus.STATUS_NEVER_REQUESTED
      });

      const expectedState = new LinkTagsState({
        urls: ImmutableMap({
          'foo.com': ImmutableMap({
            [DefaultNetwork]: exptectedLinkTag
          })
        })
      });

      expect(state
        .getIn(['urls', 'foo.com', DefaultNetwork])
        .equals(expectedState.getIn(['urls', 'foo.com', DefaultNetwork])));
    });
  });

  describe('Save link tag start reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork,
        linkTag: getMockLinkTag()
      }));
      store.dispatch(linkTagActions.saveLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      const state: LinkTagsState = store.getState();
      const expectedLinkTag = new LinkTagPropertiesRecord(fromJS({
        ...getMockLinkTag(),
        enabled: true,
        saveStatus: RequestStatus.STATUS_REQUESTED
      }));

      const linkTag = state.getIn(['urls', 'foo.com', DefaultNetwork]);
      expect(linkTag.equals(expectedLinkTag)).toBeTruthy();
    });
  });

  describe('Save link tag completed reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork,
        linkTag: getMockLinkTag()
      }));
      store.dispatch(linkTagActions.saveLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.saveLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      const state: LinkTagsState = store.getState();
      const expectedLinkTag = new LinkTagPropertiesRecord(fromJS({
        ...getMockLinkTag(),
        enabled: true,
        saveStatus: RequestStatus.STATUS_LOADED
      }));
      const linkTag = state.getIn(['urls', 'foo.com', DefaultNetwork]);
      expect(linkTag.equals(expectedLinkTag)).toBeTruthy();
    });
  });

  describe('Save link tag failed reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkTagActions.createLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.createLinkTagCompleted({
        link: 'foo.com',
        network: DefaultNetwork,
        linkTag: getMockLinkTag()
      }));
      store.dispatch(linkTagActions.saveLinkTagStart({
        link: 'foo.com',
        network: DefaultNetwork
      }));
      store.dispatch(linkTagActions.saveLinkTagFailed({
        error: 'some error',
        link: 'foo.com',
        network: DefaultNetwork
      }));
      const state: LinkTagsState = store.getState();
      const expectedLinkTag = new LinkTagPropertiesRecord(fromJS({
        ...getMockLinkTag(),
        enabled: true,
        saveStatus: RequestStatus.STATUS_ERROR
      }));

      const linkTag = state.getIn(['urls', 'foo.com', DefaultNetwork]);
      expect(linkTag.equals(expectedLinkTag)).toBeTruthy();
    });
  });
});
