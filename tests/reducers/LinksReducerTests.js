/* @flow */
import * as linksActions from 'actions/LinksActions';
import { createStore } from 'redux';
import reducer from 'reducers/LinksReducer';
import { LinksState } from 'records/LinksRecords';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';

import type { Store } from 'redux';

describe('Links Reducers', () => {
  let store: Store<LinksState>;

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('Update Url Properties reducer', () => {
    it('should update the property', () => {
      let urlProperties = ImmutableMap({
        'www.google.com': ImmutableMap({
          'shortenOnPublish': true,
          'addLinkTags': false
        })
      });

      store.dispatch(linksActions.updateUrlProperties(urlProperties));
      const state: LinksState = store.getState();
      expect(state.urlProperties.get('www.google.com').get('shortenOnPublish')).toBeTruthy();
    });
  });

  describe('Update Found Urls reducer', () => {
    it('should update the property', () => {
      let foundUrls = ImmutableMap({
        'FACEBOOK': ImmutableList(['www.google.com', 'www.reddit.com'])
      });

      store.dispatch(linksActions.updateFoundUrls(foundUrls));
      const state: LinksState = store.getState();
      expect(state.urls).toEqual(foundUrls);
    });
  });
});
