/* @flow */
import * as mentionsActions from 'actions/MentionsActions';
import reducer, { MentionsReducerState } from 'reducers/MentionsReducer';
import { createStore } from 'redux';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { MentionsActionConstants } from 'constants/ActionConstants';
import { MentionsRecord } from 'types/MentionsTypes.js';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';

import type { Store } from 'redux';

describe('Message Reducers', () => {
  let store: Store<MentionsReducerState>;
  let state: MentionsReducerState;

  beforeEach(() => {
    store = createStore(reducer);
    state = store.getState();
  });

  describe('initial state', () =>{
    it('should have expected defaults', () => {
      expect(state.listOfMentions).toEqual(ImmutableMap());
      expect(state.mentionsQueryList).toEqual(ImmutableList());
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.FACEBOOK)).toEqual(false);
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.TWITTER)).toEqual(false);
      expect(state.loadMentionsQueryStatus).toEqual(MentionsActionConstants.MENTIONS_QUERY_NOT_REQUESTED);
    });
  });

  describe('openMentionsQuery reducer', () => {
    it('should open the mentions', () => {
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.FACEBOOK)).not.toBeTruthy();
      store.dispatch(mentionsActions._openMentionsQuery(SupportedNetworks.FACEBOOK));

      state = store.getState();
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.FACEBOOK)).toBeTruthy();
    });
  });

  describe('closeMentionsQuery reducer', () => {
    it('should close the mentions', () => {
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.FACEBOOK)).not.toBeTruthy();
      store.dispatch(mentionsActions._openMentionsQuery(SupportedNetworks.FACEBOOK));

      state = store.getState();
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.FACEBOOK)).toBeTruthy();
      store.dispatch(mentionsActions._closeMentionsQuery(SupportedNetworks.FACEBOOK));

      state = store.getState();
      expect(state.isMentionsQueryOpen.get(SupportedNetworks.FACEBOOK)).not.toBeTruthy();
    });
  });

  describe('requestMentionsQueryCompleted reducer', () => {
    it('should receive mentions', () => {
      let exampleMentions = [
        new MentionsRecord()
      ];
      store.dispatch(mentionsActions.requestMentionsQueryCompleted(exampleMentions));

      state = store.getState();
      expect(state.mentionsQueryList.size).toEqual(1);
      expect(state.loadMentionsQueryStatus).toEqual(MentionsActionConstants.MENTIONS_RECEIVED);
    });
  });

  describe('requestMentionsQueryFailed reducer', () => {
    it('should set loadMentionsQueryStatus to failed', () => {

      store.dispatch(mentionsActions.requestMentionsQueryFailed());

      state = store.getState();
      expect(state.loadMentionsQueryStatus).toEqual(MentionsActionConstants.MENTIONS_DATA_ERROR);
    });
  });

  describe('requestMentionsQuery reducer', () => {
    it('should set loadMentionsQueryStatus to requested', () => {

      store.dispatch(mentionsActions.requestMentionsQuery());

      state = store.getState();
      expect(state.loadMentionsQueryStatus).toEqual(MentionsActionConstants.REQUEST_MENTIONS_DATA);
    });
  });

  describe('updateMentions reducer', () => {
    it('should update the received mentions', () => {

      let exampleMentions = ImmutableMap({
        [SupportedNetworks.FACEBOOK]: ImmutableList([new MentionsRecord()])
      });
      store.dispatch(mentionsActions._updateMentions(exampleMentions));

      state = store.getState();

      expect(state.listOfMentions.get(SupportedNetworks.FACEBOOK).size).toEqual(1);

      expect(state.loadMentionsQueryStatus).toEqual(MentionsActionConstants.MENTIONS_QUERY_NOT_REQUESTED);
    });
  });
});
