/* @flow */
import * as linkPreviewActions from 'actions/LinkPreviewActions';
import * as messageActions from 'actions/MessageActions';
import { createStore } from 'redux';
import reducer from 'reducers/LinkPreviewReducer';
import { LinkPreviewState } from 'records/LinkPreviewRecords';
import { RequestStatus } from 'constants/ApplicationConstants';

import type { Store } from 'redux';

describe('Link Preview Reducers', () => {
  let store: Store<LinkPreviewState>;

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('Update Link Preview reducer', () => {
    it('should update the property', () => {
      const linkPreview = {
        caption: 'www.spredfast.com',
        description: 'Spredfast is an enterprise social media marketing platform.',
        sourceMedia: [],
        networks: [],
        title: 'Spredfast',
        thumbnailUrl: ''
      };

      store.dispatch(linkPreviewActions.updateLinkPreview(linkPreview));
      const state: LinkPreviewState = store.getState();
      const expectedState = new LinkPreviewState(linkPreview);
      expect(state).toDeepEqual(expectedState);
    });
  });

  describe('Remove Link Preview reducer', () => {
    it('should update the property', () => {
      store.dispatch(linkPreviewActions.removeLinkPreview());
      const state: LinkPreviewState = store.getState();
      expect(state.removed).toEqual(true);
    });
  });

  describe('Edit Link Preview reducer', () => {
    it('should update the property', () => {
      const linkPreview = {
        caption: 'www.spredfast.com',
        description: 'Spredfast is an enterprise social media marketing platform.',
        sourceMedia: [],
        networks: [],
        title: 'Spredfast',
        thumbnailUrl: ''
      };
      store.dispatch(linkPreviewActions.updateLinkPreview(linkPreview));

      store.dispatch(linkPreviewActions.editLinkPreview({
        description: 'foo',
        title: 'bar'
      }));

      const state: LinkPreviewState = store.getState();

      expect(state.description).toEqual('foo');
      expect(state.title).toEqual('bar');
    });
  });

  describe('Request upload custom thumb reducer', () => {
    it('is correct default values', () => {
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_NEVER_REQUESTED);
      expect(store.getState().customUploadedMedia.size).toBe(0);
    });

    it('updates currentlyUploadMedia on request', () => {
      store.dispatch(linkPreviewActions.requestUploadCustomThumb());
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_REQUESTED);
    });

    it('updates currentlyUploadMedia on cancel', () => {
      store.dispatch(linkPreviewActions.requestUploadCustomThumb());
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_REQUESTED);

      store.dispatch(linkPreviewActions.cancelLinkPreviewCustomUpload());
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_CANCELED);
    });

    it('updates currentlyUploadMedia on request, then succeed', () => {
      store.dispatch(linkPreviewActions.requestUploadCustomThumb());
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_REQUESTED);

      store.dispatch(linkPreviewActions.uploadCustomThumbSucceeded());
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_LOADED);
    });

    it('updates currentlyUploadMedia on request, then error', () => {
      store.dispatch(linkPreviewActions.requestUploadCustomThumb());
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_REQUESTED);

      store.dispatch(linkPreviewActions.uploadCustomThumbFailed('error here'));
      expect(store.getState().uploadingCustomThumbStatus).toBe(RequestStatus.STATUS_ERROR);
      expect(store.getState().uploadingCustomThumbError).toBe('error here');
    });

    it('updates thumbnail', () =>{
      store.dispatch(linkPreviewActions.updateImageThumbnail('thumbnail'));
      expect(store.getState().thumbnailUrl).toBe('thumbnail');
    });
  });

  describe('Message lifecycle', () => {

    beforeEach(() => {
      store.dispatch(linkPreviewActions.editLinkPreview({
        description: 'description',
        caption: 'caption',
        title: 'title',
        thumbnailUrl: 'thumb',
        linkUrl: 'google.com'
      }));
    });

    it('should clear on clear message', () => {
      const expectedOutput = new LinkPreviewState();
      store.dispatch(messageActions.clearMessage());
      expect(store.getState()).toDeepEqual(expectedOutput);
    });

    it('should clear on read link preview', () => {
      const linkDetails = {
        linkUrl: 'readlinkurl.com',
        thumbnailUrl: 'readthumburl.com',
        description: 'read description',
        caption: 'read caption',
        title: 'read title'
      };
      const expectedOutput = new LinkPreviewState(linkDetails);
      store.dispatch(linkPreviewActions.readLinkPreviewCompleted(linkDetails));
      expect(store.getState()).toDeepEqual(expectedOutput);
    });
  });

  describe('Link preview scraping', () => {
    it('has correct default values', () => {
      expect(store.getState().currentlyScraping).toBe(false);
    });

    it('updates when scraping starts', () => {
      store.dispatch(linkPreviewActions.scrapingLinkPreview());
      expect(store.getState().currentlyScraping).toBe(true);
    });

    it('updates when scraping cancels', () => {
      store.dispatch(linkPreviewActions.scrapingLinkPreview());
      expect(store.getState().currentlyScraping).toBe(true);

      store.dispatch(linkPreviewActions.cancelLinkPreviewScrape());
      expect(store.getState().currentlyScraping).toBe(false);
    });
  });

});
