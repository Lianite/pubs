/* @flow */
import reducer from 'reducers/FacebookReducer';
import * as Actions from 'actions/FacebookActions';
import {
  is,
  List as ImmutableList
} from 'immutable';
import { createStore } from 'redux';
import { FacebookState } from 'records/FacebookRecords';
import {
  LanguageState,
  LanguagesState,
  LocationState,
  TargetingState,
  GenderState
} from 'records/TargetingRecords';
import { clearMessage } from 'actions/MessageActions';
import { GENDER_TARGETING_TYPES } from 'constants/ApplicationConstants';

import type { Store } from 'redux';


describe('Facebook Reducers', () => {
  let store: Store<FacebookState>;

  const createStoreWithLanguage = (language) => {
    store = createStore(reducer, new FacebookState({
      targeting: new TargetingState({
        language: new LanguagesState({
          selectedLanguages: ImmutableList([
            new LanguageState(language)
          ])
        }),
        gender: new GenderState(GENDER_TARGETING_TYPES['Men'])
      })
    }));
  };

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('Initial State', () => {
    it('should set the correct initial state', () => {
      expect(store.getState()).toDeepEqual(new FacebookState());
    });

    it('should clear the state when clear state is called', () => {
      store.dispatch(Actions.addFacebookTargetingLanguage({
        value: '1',
        description: 'English'
      }));
      store.dispatch(clearMessage());
      expect(store.getState().toJS()).toDeepEqual(new FacebookState().toJS());
    });
  });

  describe('Targeting', () => {

    describe('Location', () => {
      it('should add an included location', () => {
        store.dispatch(Actions.addFacebookTargetingIncludedLocation({
          value: '1',
          description: 'United States',
          type: 'COUNTRY'
        }));
        expect(store.getState().toJS()).toDeepEqual(new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'United States',
            type: 'COUNTRY'
          })
        ])).toJS());
      });

      it('should not add a duplicate included location', () => {
        store = createStore(reducer, new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'United States',
            type: 'COUNTRY'
          })
        ])));

        store.dispatch(Actions.addFacebookTargetingIncludedLocation({
          value: '1',
          description: 'United States',
          type: 'COUNTRY'
        }));
        expect(store.getState().toJS()).toDeepEqual(new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'United States',
            type: 'COUNTRY'
          })
        ])).toJS());
      });

      it('should remove an included location', () => {
        store = createStore(reducer, new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'United States',
            type: 'COUNTRY'
          })
        ])));

        store.dispatch(Actions.removeFacebookTargetingIncludedLocation('United States'));
        expect(store.getState().toJS()).toDeepEqual(new FacebookState().toJS());
      });

      it('should gracefully handle the case where a location to be removed is not found in state', () => {
        store = createStore(reducer, new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'United States',
            type: 'COUNTRY'
          })
        ])));

        store.dispatch(Actions.removeFacebookTargetingIncludedLocation('United Arab Emirates'));
        expect(store.getState().toJS()).toDeepEqual(new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], new ImmutableList([
          new LocationState({
            value: '1',
            description: 'United States',
            type: 'COUNTRY'
          })
        ])).toJS());
      });

      it('should remove an entire country\'s sub-locations', () => {
        store = createStore(reducer, new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'Germany',
            type: 'GEOGRAPHY',
            subType: 'COUNTRY'
          }),
          new LocationState({
            value: '2',
            description: 'Alabama',
            type: 'GEOGRAPHY',
            subType: 'REGION',
            parentTargetingValue: {
              value: '1',
              description: 'United States',
              type: 'GEOGRAPHY',
              subType: 'COUNTRY'
            }
          }),
          new LocationState({
            value: '3',
            description: 'New York',
            type: 'GEOGRAPHY',
            subType: 'METRO',
            parentTargetingValue: {
              value: '1',
              description: 'United States',
              type: 'GEOGRAPHY',
              subType: 'COUNTRY'
            }
          })
        ])));

        store.dispatch(Actions.removeFacebookTargetingIncludedCountry('United States'));

        expect(store.getState().toJS()).toDeepEqual(new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'Germany',
            type: 'GEOGRAPHY',
            subType: 'COUNTRY'
          })
        ])).toJS());
      });

      it('should remove a country included locations', () => {
        store = createStore(reducer, new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '1',
            description: 'Germany',
            type: 'GEOGRAPHY',
            subType: 'COUNTRY'
          }),
          new LocationState({
            value: '2',
            description: 'Alabama',
            type: 'GEOGRAPHY',
            subType: 'REGION',
            parentTargetingValue: {
              value: '1',
              description: 'United States',
              type: 'GEOGRAPHY',
              subType: 'COUNTRY'
            }
          }),
          new LocationState({
            value: '3',
            description: 'New York',
            type: 'GEOGRAPHY',
            subType: 'METRO',
            parentTargetingValue: {
              value: '1',
              description: 'United States',
              type: 'GEOGRAPHY',
              subType: 'COUNTRY'
            }
          })
        ])));

        store.dispatch(Actions.removeFacebookTargetingIncludedCountry('Germany'));

        expect(store.getState().toJS()).toDeepEqual(new FacebookState().setIn([
          'targeting',
          'location',
          'includedLocations'
        ], ImmutableList([
          new LocationState({
            value: '2',
            description: 'Alabama',
            type: 'GEOGRAPHY',
            subType: 'REGION',
            parentTargetingValue: {
              value: '1',
              description: 'United States',
              type: 'GEOGRAPHY',
              subType: 'COUNTRY'
            }
          }),
          new LocationState({
            value: '3',
            description: 'New York',
            type: 'GEOGRAPHY',
            subType: 'METRO',
            parentTargetingValue: {
              value: '1',
              description: 'United States',
              type: 'GEOGRAPHY',
              subType: 'COUNTRY'
            }
          })
        ])).toJS());
      });

      describe('Gender', () => {
        it('should set a new gender', () => {
          store.dispatch(Actions.setGenderTargeting(GENDER_TARGETING_TYPES['Women']));
          expect(store.getState().toJS()).toDeepEqual(new FacebookState({
            targeting: new TargetingState({
              language: new LanguagesState(),
              gender: new GenderState(GENDER_TARGETING_TYPES['Women'])
            })
          }).toJS());
        });

        it('should clear the gender targeting', () => {
          store.dispatch(Actions.clearGenderTargeting({
            value: '1',
            description: 'English'
          }));
          expect(store.getState().toJS()).toDeepEqual(new FacebookState({
            targeting: new TargetingState({
              language: new LanguagesState(),
              gender: new GenderState()
            })
          }).toJS());
        });
      });

      describe('Language', () => {
        it('should add a language', () => {
          store.dispatch(Actions.addFacebookTargetingLanguage({
            value: '1',
            description: 'English'
          }));
          expect(store.getState().toJS()).toDeepEqual(new FacebookState({
            targeting: new TargetingState({
              language: new LanguagesState({
                selectedLanguages: ImmutableList([
                  new LanguageState({
                    value: '1',
                    description: 'English'
                  })
                ])
              }),
              gender: new GenderState()
            })
          }).toJS());
        });

        it('should not add a duplicate language', () => {
          createStoreWithLanguage({
            value: '1',
            description: 'English'
          });
          expect(store.getState().targeting.language.selectedLanguages.toJS()).toDeepEqual([{
            value: '1',
            description: 'English',
            type: 'LOCALE'
          }]);
          store.dispatch(Actions.addFacebookTargetingLanguage({
            value: '1',
            description: 'English'
          }));
          expect(store.getState().toJS()).toDeepEqual(new FacebookState({
            targeting: new TargetingState({
              language: new LanguagesState({
                selectedLanguages: ImmutableList([
                  new LanguageState({
                    value: '1',
                    description: 'English'
                  })
                ])
              }),
              gender: new GenderState(GENDER_TARGETING_TYPES['Men'])
            })
          }).toJS());
        });

        it('should remove a language', () => {
          createStoreWithLanguage({
            value: '1',
            description: 'English'
          });
          expect(store.getState().targeting.language.selectedLanguages.toJS()).toDeepEqual([{
            value: '1',
            description: 'English',
            type: 'LOCALE'
          }]);
          store.dispatch(Actions.removeFacebookTargetingLanguage('English'));
          expect(store.getState().toJS()).toDeepEqual(new FacebookState({
            targeting: new TargetingState({
              language: new LanguagesState({
                selectedLanguages: ImmutableList()
              }),
              gender: new GenderState(GENDER_TARGETING_TYPES['Men'])
            })
          }).toJS());
        });

        it('should gracefully handle the case where a language to be removed is not found in state', () => {
          createStoreWithLanguage({
            value: '1',
            description: 'English'
          });
          expect(store.getState().targeting.language.selectedLanguages.toJS()).toDeepEqual([{
            value: '1',
            description: 'English',
            type: 'LOCALE'
          }]);
          store.dispatch(Actions.removeFacebookTargetingLanguage('Spanish'));
          expect(store.getState().toJS()).toDeepEqual(new FacebookState({
            targeting: new TargetingState({
              language: new LanguagesState({
                selectedLanguages: ImmutableList([
                  new LanguageState({
                    value: '1',
                    description: 'English'
                  })
                ])
              }),
              gender: new GenderState(GENDER_TARGETING_TYPES['Men'])
            })
          }).toJS());
        });
      });

      it('should clear targeting', () => {
        createStoreWithLanguage({
          value: '1',
          description: 'English'
        });
        expect(store.getState().targeting.language.selectedLanguages.toJS()).toDeepEqual([{
          value: '1',
          description: 'English',
          type: 'LOCALE'
        }]);
        store.dispatch(Actions.clearTargeting());
        expect(is(store.getState().targeting, new TargetingState())).toEqual(true);
      });
    });

    describe('Read Facebook State', () => {
      it('should set the facebook state correctly', () => {
        createStoreWithLanguage({
          value: '1',
          description: 'English'
        });
        const facebookState = new FacebookState({
          targeting: new TargetingState({
            language: new LanguagesState({
              selectedLanguages: ImmutableList([
                new LanguageState({
                  value: '2',
                  description: 'Arabic'
                })
              ])
            }),
            gender: new GenderState(GENDER_TARGETING_TYPES['Men'])
          })
        });
        store.dispatch(Actions.readFacebookCompleted(facebookState));
        expect(is(store.getState(), facebookState)).toBe(true);
      });
    });

    describe('Clear Message', () => {
      it('should clear the facebook state', () => {
        createStoreWithLanguage({
          value: '1',
          description: 'English'
        });
        expect(
          is(
            store.getState(),
            new FacebookState()
          )
        ).toBe(false);

        store.dispatch(clearMessage());
        expect(
          is(
            store.getState(),
            new FacebookState()
          )
        ).toBe(true);
      });
    });
  });
});
