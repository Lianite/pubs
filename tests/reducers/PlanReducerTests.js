/* @flow */
import * as planActions from 'actions/PlanActions';
import { createStore } from 'redux';
import { is, Map as ImmutableMap, List as ImmutableList } from 'immutable';
import reducer, { PlanReducerState } from 'reducers/PlanReducer';
import { RequestStatus } from 'constants/ApplicationConstants';
import type { Store } from 'redux';

describe('Plan State reducer', () => {
  let store: Store<PlanReducerState>;
  let state: PlanReducerState;

  beforeEach(() => {
    store = createStore(reducer);
    state = store.getState();
  });

  describe('initial state', () => {
    it('should have expected defaults', () => {
      expect(is(state.currentPlan, ImmutableMap({
        name: '',
        id: '',
        color: ''
      }))).toEqual(true);
    });
  });

  describe('update current Plan', () => {
    it('should update to the new currentPlan when one is selected', () => {
      let plan = ImmutableMap({
        color: '#FFFF',
        id: '1',
        name: 'Test'
      });
      store.dispatch(planActions.updateCurrentPlan(plan));
      state = store.getState();
      expect(is(state.currentPlan, plan)).toEqual(true);
    });
  });

  describe('plans data', () => {
    it('should change status on request', () => {
      store.dispatch(planActions.requestPlanData());
      expect(store.getState().plansDataStatus).toEqual(RequestStatus.STATUS_REQUESTED);
    });

    it('should change status on received and store plans', () => {
      const plansDataPayload = ImmutableList([{
        id: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
        name: 'Test',
        color: '#119146'
      }]);

      store.dispatch(planActions.planDataReceived(plansDataPayload));
      expect(store.getState().plansDataStatus).toEqual(RequestStatus.STATUS_LOADED);
      expect(store.getState().plansData).toEqual(plansDataPayload);
    });

    it('should change status on error and store the error', () => {
      store.dispatch(planActions.planDataError('test error'));
      expect(store.getState().plansDataStatus).toEqual(RequestStatus.STATUS_ERROR);
      let requestError: string = store.getState().plansDataError;
      expect(requestError).not.toBe('');
      if (requestError) {
        expect(requestError).toEqual('test error');
      }
    });
  });
});
