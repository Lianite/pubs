/* @flow */
import * as notesActions from 'actions/NotesActions';
import {
  clearMessage
} from 'actions/MessageActions';
import { createStore } from 'redux';
import reducer from 'reducers/NotesReducer';
import { NoteState, NotesState } from 'records/NotesRecords';
import { List as ImmutableList } from 'immutable';

import type { Store } from 'redux';

describe('Notes Reducers', () => {
  let store: Store<NotesState>;
  let state: NotesState;

  beforeEach(() => {
    store = createStore(reducer);
  });

  describe('Read Notes Completed reducer', () => {
    const notes = [
      {
        authorFirstName: 'John',
        authorLastName: 'Test',
        noteCreationTimestamp: 1234,
        noteText: 'hello',
        authorAvatarThumbnail: 'test.com'
      },
      {
        authorFirstName: 'Example',
        authorLastName: 'Person',
        noteCreationTimestamp: 9876,
        noteText: 'test',
        authorAvatarThumbnail: ''
      }
    ];
    it('should update the state correctly', () => {
      store.dispatch(notesActions.readNotesCompleted({ notesList: notes }));
      const expectedNotes: Array<NoteState> = notes.map((note) => {
        return new NoteState({
          authorFirstName: note.authorFirstName,
          authorLastName: note.authorLastName,
          authorAvatarThumbnail: note.authorAvatarThumbnail,
          noteCreationTimestamp: note.noteCreationTimestamp,
          noteText: note.noteText
        });
      });
      const expectedOutput: NotesState = new NotesState({
        notesList: ImmutableList(expectedNotes)
      });
      state = store.getState();
      expect(state.get('notesList').size).toEqual(2);
      expect(state.get('notesList')).toDeepEqual(expectedOutput.get('notesList'));
    });
  });

  describe('Add Note reducer', () => {
    it('should update the state correctly', () => {
      state = store.getState();
      expect(state.get('notesList').size).toEqual(0);
      store.dispatch(notesActions.addNote({
        noteText: 'test',
        authorFirstName: 'test',
        authorLastName: 'user',
        authorAvatarThumbnail: 'http://image.com',
        noteCreationTimestamp: 1234
      }));
      const expectedOutput = new NotesState({
        notesList: ImmutableList([new NoteState({
          noteText: 'test',
          authorFirstName: 'test',
          authorLastName: 'user',
          authorAvatarThumbnail: 'http://image.com',
          noteCreationTimestamp: 1234
        })])
      });
      state = store.getState();
      expect(state.get('notesList').size).toEqual(1);
      expect(state.get('notesList').first()).toDeepEqual(expectedOutput.get('notesList').first());
    });
  });

  describe('Current Note Text Changed reducer', () => {
    it('should update the state correctly', () => {
      store.dispatch(notesActions.currentNoteTextChanged({
        text: 'this is the current text'
      }));
      state = store.getState();
      expect(state.get('currentNoteText')).toEqual('this is the current text');
    });
  });

  describe('Clear Message reducer', () => {
    beforeEach(() => {
      store = createStore(reducer, new NotesState({currentNoteText: 'test'}));
    });
    it('should update the state correctly', () => {
      expect(store.getState().currentNoteText).toEqual('test');
      store.dispatch(clearMessage());
      expect(store.getState()).toDeepEqual(new NotesState());
    });
  });

  describe('Saving Note reducer', () => {
    it('should set "savingMessge" to true', () => {
      store = createStore(reducer, new NotesState({savingNote: false}));
      expect(store.getState().savingNote).toEqual(false);
      store.dispatch(notesActions.saveNoteRequestStarted());
      expect(store.getState().savingNote).toEqual(true);
    });
    it('should set "savingMessge" to false', () => {
      store = createStore(reducer, new NotesState({savingNote: true}));
      expect(store.getState().savingNote).toEqual(true);
      store.dispatch(notesActions.saveNoteRequestCompleted());
      expect(store.getState().savingNote).toEqual(false);
    });
  });
});
