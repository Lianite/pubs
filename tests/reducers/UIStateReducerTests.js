/* @flow */
import * as uiStateActions from 'actions/UIStateActions';
import { createStore } from 'redux';
import reducer, { UIStateReducerState } from 'reducers/UIStateReducer';

import { SidebarPages } from 'constants/UIStateConstants';

import type { Store } from 'redux';

describe('UI State reducer', () => {
  let store: Store<UIStateReducerState>;
  let state: UIStateReducerState;

  beforeEach(() => {
    store = createStore(reducer);
    state = store.getState();
  });

  describe('initial state', () => {
    it('should have expected defaults', () => {
      expect(state.sidebarOpen).toEqual(true);
      expect(state.activePage).toEqual(SidebarPages.BASICS);
    });
  });

  describe('close sidebar', () => {
    it('should close if opened', () => {
      store.dispatch(uiStateActions.closeSidebar());
      state = store.getState();
      expect(state.sidebarOpen).toEqual(false);
      expect(state.activePage).toEqual('');
    });

    it('should not change if closed', () => {
      //First, close it
      store.dispatch(uiStateActions.closeSidebar());
      state = store.getState();
      expect(state.sidebarOpen).toEqual(false);

      //close it again
      store.dispatch(uiStateActions.closeSidebar());
      state = store.getState();
      expect(state.sidebarOpen).toEqual(false);
    });
  });

  describe('open sidebar', () => {
    it('should not change if opened, active page should update', () => {
      store.dispatch(uiStateActions.openSidebar(SidebarPages.WORKFLOW));
      state = store.getState();
      expect(state.sidebarOpen).toEqual(true);
      expect(state.activePage).toEqual(SidebarPages.WORKFLOW);
    });

    it('should change if closed', () => {
        //First, close
      store.dispatch(uiStateActions.closeSidebar());
      state = store.getState();
      expect(state.sidebarOpen).toEqual(false);
      expect(state.activePage).toEqual('');

      //Now, open
      store.dispatch(uiStateActions.openSidebar(SidebarPages.NOTES));
      state = store.getState();
      expect(state.sidebarOpen).toEqual(true);
      expect(state.activePage).toEqual(SidebarPages.NOTES);
    });
  });

  describe('Add new voiceDropdownToggle reducer', () => {
    it('toggle dropdown open/close', () => {
      store.dispatch(uiStateActions.voiceDropdownToggle());
      expect(store.getState().voiceSelectorOpen).toBe(true);

      store.dispatch(uiStateActions.voiceDropdownToggle());
      expect(store.getState().voiceSelectorOpen).toBe(false);

      // Making sure that it sets to true when false
      store.dispatch(uiStateActions.voiceDropdownToggle('focus'));
      expect(store.getState().voiceSelectorOpen).toBe(true);

      // Making sure it stays as true even when already true
      store.dispatch(uiStateActions.voiceDropdownToggle('focus'));
      expect(store.getState().voiceSelectorOpen).toBe(true);

      // Making sure that it sets to false when true
      store.dispatch(uiStateActions.voiceDropdownToggle('blur'));
      expect(store.getState().voiceSelectorOpen).toBe(false);

      // Making sure it stays as false even when already false
      store.dispatch(uiStateActions.voiceDropdownToggle('blur'));
      expect(store.getState().voiceSelectorOpen).toBe(false);
    });
  });

  describe('update dragging state', () => {
    it('should default to false', () => {
      state = store.getState();
      expect(state.currentlyDraggingMedia).toEqual(false);
    });

    it('to dragging, then to not dragging', () => {
        //First, drag
      store.dispatch(uiStateActions.dragStateChanged(true));
      state = store.getState();
      expect(state.currentlyDraggingMedia).toEqual(true);

      //Then, stop drag
      store.dispatch(uiStateActions.dragStateChanged(false));
      state = store.getState();
      expect(state.currentlyDraggingMedia).toEqual(false);
    });
  });

  describe('update preview visibility state', () => {
    it('should default to false', () => {
      state = store.getState();
      expect(state.previewVisible).toEqual(false);
    });

    it('should set to visible and then back to not visible', () => {
      store.dispatch(uiStateActions.previewVisibilityChanged(true));
      state = store.getState();
      expect(state.previewVisible).toEqual(true);

      store.dispatch(uiStateActions.previewVisibilityChanged(false));
      state = store.getState();
      expect(state.previewVisible).toEqual(false);
    });
  });
});
