import React from 'react';
import { shallow } from 'enzyme';
import MentionLinkDecorator from 'decorators/components/MentionLinkDecorator';

import styles from 'styles/modules/components/MentionsQuery.scss';

describe('MentionLinkDecorator Component', () => {
  let shallowWrapper;

  const mainProps = {
    children: ['testText']
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<MentionLinkDecorator {...props} />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const decorator = shallowWrapper.find(`.${styles.mentionsLinkDecorator}`);
    expect(decorator.length).toEqual(1);

    expect(decorator.prop('children')).toEqual(mainProps.children);
  });
});
