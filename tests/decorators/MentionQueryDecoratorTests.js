import React from 'react';
import MentionQueryDecorator from 'decorators/components/MentionQueryDecorator';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/MentionsQuery.scss';

describe('MentionQueryDecorator Component', () => {
  let shallowWrapper;

  let mainProps = {
    decoratedText: '@testText',
    queryForMentions: jasmine.createSpy('queryForMentions'),
    recordCurrentMentionOffset: jasmine.createSpy('recordCurrentMentionOffset'),
    children: ['testText'],
    network: 'FACEBOOK',
    offsetKey: '1',
    currentMentionOffset: '2'
  };

  const render = (props = mainProps) => {
    props = {
      ... mainProps,
      ...props
    };
    shallowWrapper = shallow(<MentionQueryDecorator {...props} >test</MentionQueryDecorator>);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.queryForMentions.calls.reset();
    mainProps.recordCurrentMentionOffset.calls.reset();
  });

  it('should render correctly', () => {
    const mention = shallowWrapper.find(`.${styles.mentionsQueryDecorator}`);
    expect(mention.length).toEqual(1);

    expect(mention.children()).toEqual(shallowWrapper.children());
  });

  describe('queryForMentions callback', () => {
    it('should call the queryForMentions callback when a proper mentions candidate is passed in', () => {
      shallowWrapper.setProps({
        ...mainProps,
        decoratedText: '@testTextZZZ'
      });
      expect(mainProps.queryForMentions).toHaveBeenCalledWith(
        '@testTextZZZ',
        '1',
        'FACEBOOK'
      );
    });

    it('should not call the queryForMentions callback when a mentions candidate is too short in length', () => {
      shallowWrapper.setProps({
        ...mainProps,
        decoratedText: '@'
      });
      expect(mainProps.queryForMentions).not.toHaveBeenCalled();
    });

    it('should not call the queryForMentions callback when a mentions candidate has not changed', () => {
      shallowWrapper.setProps({
        ...mainProps,
        decoratedText: '@testText'
      });
      expect(mainProps.queryForMentions).not.toHaveBeenCalled();
    });
  });

  describe('recordCurrentMentionOffset callback', () => {
    it('should not call the callback if the mentions candidate has not changed', () => {
      shallowWrapper.setProps({
        decoratedText: '@testText'
      });
      expect(mainProps.recordCurrentMentionOffset).not.toHaveBeenCalled();
    });

    it('should not call the callback if the offsetKey equals the currentMentionOffset', () => {
      render({
        currentMentionOffset: '1'
      });
      shallowWrapper.setProps({
        decoratedText: '@somethingDifferent'
      });
      expect(mainProps.recordCurrentMentionOffset).not.toHaveBeenCalled();
    });

    it('should cal the callback if the mention candidate has changed and the offsetKey does not equal the currentMentionOffset', () => {
      shallowWrapper.setProps({
        decoratedText: '@somethingDifferent'
      });
      expect(mainProps.recordCurrentMentionOffset).toHaveBeenCalledWith('1');
    });
  });
});
