import React from 'react';
import { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import LinkDecorator from 'decorators/components/LinkDecorator';

function renderStateless(props = {}) {
  return ReactTestUtils.renderIntoDocument(<div><LinkDecorator {...props}/></div>);
}

let mockFunc = () => {
  return true;
};

describe('LinkDecorator Component', () => {
  let statelessComponent;

  let props = {
    children: ['testText'],
    sidebarOpen: false,
    activePage: 'Test',
    onSidebarOpened: mockFunc
  };

  beforeEach(() => {
    spyOn(props, 'onSidebarOpened');
    statelessComponent = renderStateless(props);
  });

  // a basic test for looking at outputted elements in the DOM and verifing the results
  it('should render a link', () => {
    let domNode = findDOMNode(statelessComponent).children[0];
    expect(ReactTestUtils.isDOMComponent(domNode)).toBeTruthy();
  });

  it('should render a link', () => {
    let domNode = findDOMNode(statelessComponent).children[0];
    ReactTestUtils.Simulate.click(domNode);

    expect(props.onSidebarOpened).toBeTruthy();
  });
});
