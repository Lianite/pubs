/* @flow */
import { createAction } from 'redux-actions';
import { LabelActionConstants } from 'constants/ActionConstants';

import type { Dispatch } from 'redux';
import type { rootState } from 'reducers/index';

import * as LabelsApi from 'apis/LabelsApi';

import { getCompanyId, getCampaignId } from 'selectors/RouterSelector';

export const addLabelToMessage  = createAction(LabelActionConstants.ADD_LABEL);
export const removeLabelFromMessage  = createAction(LabelActionConstants.REMOVE_LABEL);
export const labelsLoaded = createAction(LabelActionConstants.LABELS_LOADED);
export const readContentLabelsCompleted = createAction(LabelActionConstants.LABELS_READ_COMPLETE);
export const addNewLabelToAvailableLabels = createAction(LabelActionConstants.APPEND_NEW_LABEL);

export const fetchLabelsAction = () => {
  return (dispatch: Dispatch, getState: () => rootState) => {
    const state = getState();
    return LabelsApi.getLabels(getCompanyId(state), getCampaignId(state))
      .then(res => {
        dispatch(labelsLoaded(res));
      });
  };
};
