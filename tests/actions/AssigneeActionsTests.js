/* @flow */
import * as actions from 'actions/AssigneeActions';
import { AssigneeActionConstants } from 'constants/ActionConstants';
import { is, List as ImmutableList } from 'immutable';
import { AssigneeRecord } from 'records/AssigneeRecords';
import { mockGetAllAssignDataRequest,
  mockGetValidAssignDataRequest,
  stubGetAllAssigneeDataRequest,
  stubGetValidAssigneeDataRequest,
  stubGetAssigneeVoices,
  mockGetAssigneeVoicesRequest } from 'tests/api-responses/AssigneeApiResponses';
import mockState from 'tests/test-utils/MockPublishingState';
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { assigneeErrors } from 'constants/AssigneeConstants';

describe('Assignee Actions', () => {
  describe('update current plan', () => {
    it('should create assignee from payload', () => {
      const assignee = new AssigneeRecord({
        lastName: 'McTest',
        id: '1',
        firstName: 'Test',
        voices: [],
        conflictingVoices: [],
        invalid: false
      });
      expect(actions.updateCurrentAssignee(assignee)).toEqual({
        type: AssigneeActionConstants.UPDATE_CURRENT_ASSIGNEE,
        payload: assignee
      });
    });
  });

  describe('requestAllAssignees', () => {
    it('should create expected structure', () => {
      expect(actions.requestAllAssignees()).toEqual({
        type: AssigneeActionConstants.REQUEST_ALL_ASSIGNEES,
        payload: undefined
      });
    });
  });

  describe('allAssigneesReceived', () => {
    it('should create expected structure', () => {
      expect(actions.allAssigneesReceived(1)).toEqual({
        type: AssigneeActionConstants.ALL_ASSIGNEES_RECEIVED,
        payload: 1
      });
    });
  });

  describe('allAssigneesError', () => {
    it('should create expected structure', () => {
      const error = new Error('test error');
      expect(actions.allAssigneesError(error)).toEqual({
        type: AssigneeActionConstants.ALL_ASSIGNEES_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('fetchAssigneesVoicesStarted', () => {
    it('should create expected structure', () => {
      expect(actions.fetchAssigneesVoicesStarted()).toEqual({
        type: AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_STARTED,
        payload: undefined
      });
    });
  });

  describe('fetchAssigneesVoicesSuccessful', () => {
    it('should create expected structure', () => {
      expect(actions.fetchConflictingAssigneesVoicesSuccessful(1)).toEqual({
        type: AssigneeActionConstants.FETCH_CONFLICTING_ASSIGNEE_VOICES,
        payload: 1
      });
    });
  });

  describe('fetchAssigneesVoicesError', () => {
    it('should create expected structure', () => {
      const error = new Error('test error');
      expect(actions.fetchAssigneesVoicesError(error)).toEqual({
        type: AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_FAILED,
        payload: error,
        error: true
      });
    });
  });

  describe('requestValidAssignees', () => {
    it('should create expected structure', () => {
      expect(actions.requestValidAssignees()).toEqual({
        type: AssigneeActionConstants.REQUEST_VALID_ASSIGNEES,
        payload: undefined
      });
    });
  });

  describe('validAssigneesReceived', () => {
    it('should create expected structure', () => {
      expect(actions.validAssigneesReceived(1)).toEqual({
        type: AssigneeActionConstants.VALID_ASSIGNEES_RECEIVED,
        payload: 1
      });
    });
  });

  describe('valideAssigneesError', () => {
    it('should create expected structure', () => {
      const error = new Error('test error');
      expect(actions.valideAssigneesError(error)).toEqual({
        type: AssigneeActionConstants.VALID_ASSIGNEES_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('clearAssignee', () => {
    it('should create clear', () => {
      expect(actions.clearAssignee()).toEqual({
        type: AssigneeActionConstants.CLEAR_ASSIGNEE,
        payload: undefined
      });
    });
  });

  describe('fetch all possible assignees', () => {

    let savedActions = [];

    const mockDispatch  = (action) => {
      savedActions.push(action);
    };

    const mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '3'
        }
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const campaignId = mockGetState().router.params.campaignId;
    const response = mockGetAllAssignDataRequest({companyId, campaignId});

    beforeEach(() => {
      savedActions = [];
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetAllAssigneeDataRequest({companyId, campaignId, response});

      actions.sendAllAssigneesRequest(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.REQUEST_ALL_ASSIGNEES,
          payload: undefined
        });

        expect(savedActions[1].type).toEqual(AssigneeActionConstants.ALL_ASSIGNEES_RECEIVED);
        expect(is(savedActions[1].payload, ImmutableList([
          new AssigneeRecord({ 'id': '3', 'firstName': 'x1', 'lastName': 'ACTIVE',  'invalid': false}),
          new AssigneeRecord({ 'id': '6', 'firstName': 'x2', 'lastName': 'ACTIVE',  'invalid': false }),
          new AssigneeRecord({ 'id': '5', 'firstName': 'x3', 'lastName': 'ACTIVE',  'invalid': false }),
          new AssigneeRecord({ 'id': '4', 'firstName': 'x4', 'lastName': 'ACTIVE',  'invalid': false }),
          new AssigneeRecord({ 'id': '2', 'firstName': 'x5', 'lastName': 'ACTIVE',  'invalid': false }),
          new AssigneeRecord({ 'id': '1', 'firstName': 'x6', 'lastName': 'ACTIVE',  'invalid': false })
        ]))).toEqual(true);
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubGetAllAssigneeDataRequest({companyId, campaignId, status: HTTPSTATUS.FORBIDDEN});

      actions.sendAllAssigneesRequest(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.REQUEST_ALL_ASSIGNEES,
          payload: undefined
        });
        expect(savedActions[1]).toEqual({
          type: AssigneeActionConstants.ALL_ASSIGNEES_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubGetAllAssigneeDataRequest({companyId, campaignId, response: {notAKeyIExpect: 'wtf'}});

      actions.sendAllAssigneesRequest(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.REQUEST_ALL_ASSIGNEES,
          payload: undefined
        });
        expect(savedActions[1]).toEqual({
          type: AssigneeActionConstants.ALL_ASSIGNEES_ERROR,
          payload: assigneeErrors.allAssigneesErrorText
        });
        done();
      });
    });
  });

  describe('fetch all valid assignees', () => {
    let savedActions = [];

    const mockDispatch  = (action) => {
      savedActions.push(action);
    };

    const mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '3'
        }
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const campaignId = mockGetState().router.params.campaignId;
    const response = mockGetValidAssignDataRequest({companyId, campaignId});

    beforeEach(() => {
      savedActions = [];
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetValidAssigneeDataRequest({companyId, campaignId, response});

      actions.sendValidAssigneesRequest(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.REQUEST_VALID_ASSIGNEES,
          payload: undefined
        });

        expect(savedActions[1]).toEqual({
          type: AssigneeActionConstants.VALID_ASSIGNEES_RECEIVED,
          payload: ImmutableList([
            new AssigneeRecord({ 'id': undefined, 'firstName': undefined, 'lastName': undefined, 'invalid': false})
          ])
        });
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubGetValidAssigneeDataRequest({companyId, campaignId, status: HTTPSTATUS.FORBIDDEN});

      actions.sendValidAssigneesRequest(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.REQUEST_VALID_ASSIGNEES,
          payload: undefined
        });
        expect(savedActions[1]).toEqual({
          type: AssigneeActionConstants.VALID_ASSIGNEES_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubGetValidAssigneeDataRequest({companyId, campaignId, response: {notAKeyIExpect: 'wtf'}});

      actions.sendValidAssigneesRequest(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.REQUEST_VALID_ASSIGNEES,
          payload: undefined
        });
        expect(savedActions[1].type).toEqual(AssigneeActionConstants.VALID_ASSIGNEES_RECEIVED);
        expect(is(savedActions[1].payload, ImmutableList([new AssigneeRecord({id: undefined, firstName: undefined, lastName: undefined})]))).toBe(true);
        done();
      });
    });
  });

  describe('fetch all assignee voices', () => {
    let savedActions = [];

    const mockDispatch  = (action) => {
      savedActions.push(action);
    };

    const mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '3'
        }
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const campaignId = mockGetState().router.params.campaignId;
    const userId = 1;
    const response = mockGetAssigneeVoicesRequest({companyId, campaignId, userId});

    beforeEach(() => {
      savedActions = [];
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetAssigneeVoices({companyId, campaignId, userId, response});

      actions.fetchAssigneesVoices()(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_STARTED,
          payload: undefined
        });
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubGetAssigneeVoices({companyId, campaignId, userId, status: HTTPSTATUS.FORBIDDEN});

      actions.fetchAssigneesVoices()(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_STARTED,
          payload: undefined
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubGetAssigneeVoices({companyId, campaignId, userId, response: {notAKeyIExpect: 'wtf'}});

      actions.fetchAssigneesVoices()(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: AssigneeActionConstants.FETCH_ASSIGNEE_VOICES_REQUEST_STARTED,
          payload: undefined
        });
        done();
      });
    });
  });

  describe('create assignee from user data', () => {
    let savedActions = [];

    const mockDispatch  = (action: Object) => {
      savedActions.push(action);
    };

    let mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '3'
        }
      }
    });

    it('should create a user from default data', () => {
      const assignee = new AssigneeRecord({
        lastName: '',
        id: -1,
        firstName: '',
        voices: [],
        conflictingVoices: [],
        invalid: false
      });

      mockDispatch(actions.createCurrentAssignee(mockGetState));
      expect(savedActions[0].type).toEqual(AssigneeActionConstants.UPDATE_CURRENT_ASSIGNEE);
      expect(savedActions[0].payload.toObject()).toDeepEqual(assignee.toObject());
    });
  });
});
