/* @flow */
import * as actions from 'actions/PlanActions';
import { PlanActionConstants } from 'constants/ActionConstants';
import { Map as ImmutableMap } from 'immutable';
import { stubGetPlanDataRequest } from 'tests/api-responses/EnvironmentApiResponses';
import { mockGetPlanDataRequest } from 'tests/api-responses/PlanApiResponses';
import { List as ImmutableList } from 'immutable';
import mockState from 'tests/test-utils/MockPublishingState';
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { StoredPlan } from 'records/PlanRecords';

describe('Plan Actions', () => {
  describe('update current plan', () => {
    it('should create plan from payload', () => {
      let plan = ImmutableMap({
        color: '#FFFF',
        id: '1',
        name: 'Test'
      });
      expect(actions.updateCurrentPlan(plan)).toEqual({
        type: PlanActionConstants.UPDATE_CURRENT_PLAN,
        payload: plan
      });
    });
  });

  describe('requestPlanData', () => {
    it('should create expected structure', () => {
      expect(actions.requestPlanData()).toEqual({
        type: PlanActionConstants.REQUEST_PLAN_DATA,
        payload: undefined
      });
    });
  });

  describe('planDataReceived', () => {
    it('should create expected structure', () => {
      expect(actions.planDataReceived(1)).toEqual({
        type: PlanActionConstants.PLAN_DATA_RECEIVED,
        payload: 1
      });
    });
  });

  describe('planDataError', () => {
    it('should create expected structure', () => {
      let error = new Error('test error');
      expect(actions.planDataError(error)).toEqual({
        type: PlanActionConstants.PLAN_DATA_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('plan data fetch', () => {
    let actionQueue = [];

    const mockDispatch  = (action) => {
      actionQueue.push(action);
    };


    let stateShape = {
      router: {
        params: {
          companyId: '2',
          campaignId: '3'
        },
        location: {
          query: {
            plan: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01' //This is a mock id from the mock payload stubGetPlanDataRequest
          }
        }
      },
      plan: {
        plansData: ImmutableList([
          new StoredPlan({
            id: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
            name: 'Test 1',
            color: '#119146'
          })
        ])
      }
    };
    let mockGetState;

    let companyId;
    let campaignId;
    let response;

    beforeEach(() => {
      actionQueue = [];
      mockGetState = mockState(stateShape);
      companyId = mockGetState().router.params.companyId;
      campaignId = mockGetState().router.params.campaignId;
      response = mockGetPlanDataRequest({companyId, campaignId});
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetPlanDataRequest({companyId, campaignId, response});

      actions.sendPlanDataRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(actionQueue.shift()).toEqual({
          type: PlanActionConstants.REQUEST_PLAN_DATA,
          payload: undefined
        });

        expect(actionQueue.shift()).toEqual({
          type: PlanActionConstants.PLAN_DATA_RECEIVED,
          payload: [{
            id: 'debfba3a-7dfc-49f6-a3fd-c05eb2d90e01',
            name: 'Test 1',
            color: '#119146'
          },
          {
            id: 'debfba3a-7dfc-49f6-a3fd-c05eb2d92e01',
            name: 'Test 2',
            color: '#119146'
          },
          {
            id: 'debfba3a-7dfc-49f6-aafd-c05eb2d90e01',
            name: 'Test 3',
            color: '#119146'
          },
          {
            id: 'debfba3a-7dfc-49f6-a3fd-c05eb34390e01',
            name: 'Test 4',
            color: '#119146'
          }] //from fetch-mock spec above
        });
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubGetPlanDataRequest({companyId, campaignId, status: HTTPSTATUS.FORBIDDEN});

      actions.sendPlanDataRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(actionQueue.shift()).toEqual({
          type: PlanActionConstants.REQUEST_PLAN_DATA,
          payload: undefined
        });
        expect(actionQueue.shift()).toEqual({
          type: PlanActionConstants.PLAN_DATA_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubGetPlanDataRequest({companyId, campaignId, response: {notAKeyIExpect: 'wtf'}});

      actions.sendPlanDataRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(actionQueue.shift()).toEqual({
          type: PlanActionConstants.REQUEST_PLAN_DATA,
          payload: undefined
        });
        expect(actionQueue.shift()).toEqual({
          type: PlanActionConstants.PLAN_DATA_ERROR,
          payload: 'The required plan data was not found on the payload for getPlanData'
        });
        done();
      });
    });

    it('should update the current plan on load if the plan data is passed to the app from Conversations', (done) => {
      stubGetPlanDataRequest({companyId, campaignId, response});

      actions.sendPlanDataRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(actionQueue[2]).toEqual({
          type: PlanActionConstants.UPDATE_CURRENT_PLAN,
          payload: mockGetState().plan.plansData.get('0')
        });
        done();
      });
    });

    it('should not update the current plan on load if the message is being edited', (done) => {
      stateShape.router.params = stateShape.router.params = {
        companyId: '2',
        campaignId: '3',
        eventId: '123'
      };
      mockGetState = mockState(stateShape);
      stubGetPlanDataRequest({companyId, campaignId, response});

      actions.sendPlanDataRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(actionQueue[2]).toEqual(undefined);
        done();
      });
    });
  });
});
