import * as actions from 'actions/EnvironmentActions';
import { EnvironmentActionConstants } from 'constants/ActionConstants';
import { PermissionConstants } from 'constants/ApplicationConstants';
import { mockCsrfTokenResponse,
  stubCsrfTokenRequest,
  mockGetUserDataRequest,
  stubGetUserDataRequest,
  mockGetCampaignIdRequest,
  stubGetCampaignIdRequest,
  mockCapabilitiesResponse,
  stubGetCapabilitiesRequest,
  mockGetCompanyDataRequest,
  stubGetCompanyRequest } from 'tests/api-responses/EnvironmentApiResponses';
import { REDUX_ROUTER_HISTORY_ACTION_TYPE } from 'tests/test-utils/PublishingTestConstants';

import mockState from 'tests/test-utils/MockPublishingState';
import { HTTPSTATUS } from 'constants/ApplicationConstants';

describe('Environment State Actions', () => {

  describe('requestCsrfToken', () => {
    it('should create expected structure', () => {
      expect(actions.requestCsrfToken()).toEqual({
        type: EnvironmentActionConstants.REQUEST_CSRF_TOKEN,
        payload: undefined
      });
    });
  });

  describe('csrfTokenReceived', () => {
    it('should create expected structure', () => {
      expect(actions.csrfTokenReceived('token_value')).toEqual({
        type: EnvironmentActionConstants.CSRF_TOKEN_RECEIVED,
        payload: 'token_value'
      });
    });
  });

  describe('csrfTokenError', () => {
    it('should create expected structure', () => {
      let error = new Error('test error');
      expect(actions.csrfTokenError(error)).toEqual({
        type: EnvironmentActionConstants.CSRF_TOKEN_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('csrf token fetch', () => {
    let savedAction1, savedAction2;

    let mockDispatch  = (action) => {
      if (savedAction1) {
        savedAction2 = action;
      } else {
        savedAction1 = action;
      }
    };

    let mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const response = mockCsrfTokenResponse({ companyId });

    beforeEach(() => {
      savedAction1 = undefined;
      savedAction2 = undefined;
    });

    it('should send fetch requests and store as expected', (done) => {
      stubCsrfTokenRequest({ companyId, response });
      actions.sendCsrfTokenRequestAction()(mockDispatch, mockGetState).then(res => {
        expect(savedAction1).toEqual({
          type: EnvironmentActionConstants.REQUEST_CSRF_TOKEN,
          payload: undefined
        });

        expect(savedAction2).toEqual({
          type: EnvironmentActionConstants.CSRF_TOKEN_RECEIVED,
          payload: 'csrf-token-here' //from stub spec above
        });
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubCsrfTokenRequest({ companyId, status: HTTPSTATUS.INTERNAL_SERVER_ERROR });

      actions.sendCsrfTokenRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedAction1).toEqual({
          type: EnvironmentActionConstants.REQUEST_CSRF_TOKEN,
          payload: undefined
        });
        expect(savedAction2).toEqual({
          type: EnvironmentActionConstants.CSRF_TOKEN_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubCsrfTokenRequest({ companyId, response: {notAKeyIExpect: 'wtf'} });

      actions.sendCsrfTokenRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedAction1).toEqual({
          type: EnvironmentActionConstants.REQUEST_CSRF_TOKEN,
          payload: undefined
        });
        expect(savedAction2).toEqual({
          type: EnvironmentActionConstants.CSRF_TOKEN_ERROR,
          payload: 'csrfToken was not found on the payload for getCsrfToken'
        });
        done();
      });
    });
  });

  describe('multiChannelFeatureFlagReceived', () => {
    it('should add the multi-channel publihsing feature flag value to the store', () => {
      expect(actions.multiChannelFeatureFlagReceived(true)).toEqual({
        type: EnvironmentActionConstants.FEATURES_RECEIVED,
        payload: true
      });
    });
  });

  describe('requestUserData', () => {
    it('should create expected structure', () => {
      expect(actions.requestUserData()).toEqual({
        type: EnvironmentActionConstants.REQUEST_USER_DATA,
        payload: undefined
      });
    });
  });

  describe('userDataReceived', () => {
    it('should create expected structure', () => {
      expect(actions.userDataReceived('3')).toEqual({
        type: EnvironmentActionConstants.USER_DATA_RECEIVED,
        payload: '3'
      });
    });
  });

  describe('userDataError', () => {
    it('should create expected structure', () => {
      let error = new Error('test error');
      expect(actions.userDataError(error)).toEqual({
        type: EnvironmentActionConstants.USER_DATA_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('userCanEditPermission', () => {
    it('should create expected structure', () => {
      expect(actions.userCanEditPermission(false)).toEqual({
        type: EnvironmentActionConstants.USER_CAN_EDIT_PERMISSION,
        payload: false
      });
    });
  });

  describe('user and company async actions', () => {
    let savedActions = [];

    let mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      }
    });

    const mockDispatch  = (action) => {
      if (typeof action === 'function') {
        return action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    const companyId = mockGetState().router.params.companyId;
    const userResponse = mockGetUserDataRequest({
      companyId,
      userId: 3,
      permissionLevel: PermissionConstants.VIEW_PUBLISHING_ACCESS_KEY,
      role: {
        name: '',
        privileges: []
      },
      userFirstName: 'test',
      userLastName: 'user',
      userAvatar: 'http://avatar.com',
      email: 'testuser@spredfast.com',
      directoryUserId: 12345
    });
    const companyResponse = mockGetCompanyDataRequest({
      directoryId: 1234,
      companyId: 2,
      companyName: 'Spredfast Test Company',
      features: ['publishing_multichannel', 'link_tagging']
    });

    beforeEach(() => {
      savedActions = [];
    });

    it('should fire the getUser and getCompany api calls and dispatch correct actions', (done) => {
      stubGetUserDataRequest({companyId, response: userResponse});

      actions.sendUserDataRequestAction()(mockDispatch, mockGetState).then(res => {
        expect(savedActions[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_USER_DATA,
          payload: undefined
        });

        expect(savedActions[1]).toEqual({
          type: EnvironmentActionConstants.USER_CAN_EDIT_PERMISSION,
          payload: false
        });

        expect(savedActions[2]).toEqual({
          type: EnvironmentActionConstants.USER_DATA_RECEIVED,
          payload: {
            id: userResponse.data.id,
            timezone: {
              abbreviation: userResponse.data.timezone.abbreviation,
              name: userResponse.data.timezone.name,
              offset: parseInt(userResponse.data.timezone.offset, 10)
            },
            viewAccess: false,
            role: {
              name: '',
              privileges: []
            },
            firstName: 'test',
            lastName: 'user',
            avatar: 'http://avatar.com'
          }
        });
        done();
      });
    });

    it('should send fetch request and then error on 403 for a user', (done) => {
      stubGetUserDataRequest({companyId, status: HTTPSTATUS.FORBIDDEN});

      actions.sendUserDataRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_USER_DATA,
          payload: undefined
        });
        expect(savedActions[1]).toEqual({
          type: EnvironmentActionConstants.USER_DATA_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should fetch company data', (done) => {
      stubGetCompanyRequest({companyId, response: companyResponse});

      actions.sendCompanyRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedActions[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_COMPANY_DATA,
          payload: undefined
        });

        expect(savedActions[1]).toEqual({
          type: EnvironmentActionConstants.FEATURES_RECEIVED,
          payload: true
        });

        expect(savedActions[2]).toEqual({
          type: EnvironmentActionConstants.COMPANY_RECEIVED,
          payload: {
            companyMetadata: {
              contentLabelCreationRestricted: res.data.companyMetadata.contentLabelCreationRestricted,
              salesforceEnabled: res.data.companyMetadata.salesforceEnabled,
              shortenLinksByDefault: res.data.companyMetadata.shortenLinksByDefault
            },
            mediaConfigDTO: {
              imageAllowedMimeTypes: res.data.mediaConfigDTO
            },
            name: res.data.name,
            enabledFeatures: ['publishing_multichannel', 'link_tagging']
          }
        });
        done();
      });
    });

    it('should send fetch request on company and then error on invalid data', (done) => {
      stubGetCompanyRequest({companyId, status: HTTPSTATUS.FORBIDDEN});

      actions.sendCompanyRequestAction()(mockDispatch, mockGetState).then(res => {
        expect(savedActions[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_COMPANY_DATA,
          payload: undefined
        });
        expect(savedActions[1]).toEqual({
          type: EnvironmentActionConstants.COMPANY_DATA_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });
  });

  describe('requestCampaignId', () => {
    it('should create expected structure', () => {
      expect(actions.requestCampaignId()).toEqual({
        type: EnvironmentActionConstants.REQUEST_CAMPAIGN_ID,
        payload: undefined
      });
    });
  });

  describe('campaignIdReceived', () => {
    it('should create expected structure', () => {
      expect(actions.campaignIdReceived()).toEqual({
        type: EnvironmentActionConstants.CAMPAIGN_ID_RECEIVED,
        payload: undefined
      });
    });
  });

  describe('campaignIdError', () => {
    it('should create expected structure', () => {
      let error = new Error('test error');
      expect(actions.campaignIdError(error)).toEqual({
        type: EnvironmentActionConstants.CAMPAIGN_ID_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('campaign id fetch', () => {
    let actionQueue = [];

    let mockGetStateWithoutCampaign = mockState({
      router: {
        location: {
          pathname: '/company/2'
        },
        params: {
          companyId: '2'
        }
      }
    });

    let mockGetStateWithCampaign = mockState({
      router: {
        location: {
          pathname: '/company/2/campaign/6'
        },
        params: {
          companyId: '2',
          campaignId: '6'
        }
      }
    });

    const mockDispatchWithoutCampaign  = (action) => {
      actionQueue.push(action);
    };

    const mockDispatchWithCampaign  = (action) => {
      actionQueue.push(action);
      if (typeof action === 'function') {
        return action(mockDispatchWithCampaign, mockGetStateWithCampaign);
      }
    };

    const sampleReturnedCampaignId = 5;
    const response = mockGetCampaignIdRequest({campaignId:sampleReturnedCampaignId});

    beforeEach(() => {
      actionQueue = [];
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetCampaignIdRequest({response});

      actions.sendCampaignIdRequestAction()(mockDispatchWithoutCampaign, mockGetStateWithoutCampaign).then( res => {
        expect(actionQueue.length).toEqual(3); //request, success, history push, and fetch thunk

        expect(actionQueue[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_CAMPAIGN_ID,
          payload: undefined
        });

        expect(actionQueue[1]).toEqual({
          type: EnvironmentActionConstants.CAMPAIGN_ID_RECEIVED,
          payload: undefined
        });

        expect(actionQueue[2].type).toEqual(REDUX_ROUTER_HISTORY_ACTION_TYPE);
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubGetCampaignIdRequest({status: HTTPSTATUS.FORBIDDEN});

      actions.sendCampaignIdRequestAction()(mockDispatchWithoutCampaign, mockGetStateWithoutCampaign).then( res => {
        expect(actionQueue[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_CAMPAIGN_ID,
          payload: undefined
        });
        expect(actionQueue[1]).toEqual({
          type: EnvironmentActionConstants.CAMPAIGN_ID_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubGetCampaignIdRequest({response: {notAKeyIExpect: 'wtf'}});

      actions.sendCampaignIdRequestAction()(mockDispatchWithoutCampaign, mockGetStateWithoutCampaign).then( res => {
        expect(actionQueue[0]).toEqual({
          type: EnvironmentActionConstants.REQUEST_CAMPAIGN_ID,
          payload: undefined
        });
        expect(actionQueue[1]).toEqual({
          type: EnvironmentActionConstants.CAMPAIGN_ID_ERROR,
          payload: 'campaignId was not found on the payload for getLastCampaign'
        });
        done();
      });
    });
  });

  describe('requestCapabilities', () => {
    it('should create expected structure', () => {
      expect(actions.requestCapabilities()).toEqual({
        type: EnvironmentActionConstants.REQUEST_CAPABILITIES,
        payload: undefined
      });
    });
  });

  describe('capabilitiesReceived', () => {
    it('should create expected structure', () => {
      expect(actions.capabilitiesReceived({foo:'bar'})).toEqual({
        type: EnvironmentActionConstants.CAPABILITIES_RECEIVED,
        payload: {foo:'bar'}
      });
    });
  });

  describe('capabilitiesError', () => {
    it('should create expected structure', () => {
      let error = new Error('test error');
      expect(actions.capabilitiesError(error)).toEqual({
        type: EnvironmentActionConstants.CAPABILITIES_ERROR,
        payload: error,
        error: true
      });
    });
  });

  describe('capabilities fetch', () => {
    let savedAction1, savedAction2;

    let mockDispatch  = (action) => {
      if (savedAction1) {
        savedAction2 = action;
      } else {
        savedAction1 = action;
      }
    };

    let mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const response = mockCapabilitiesResponse({companyId});

    beforeEach(() => {
      savedAction1 = undefined;
      savedAction2 = undefined;
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetCapabilitiesRequest({companyId, response});

      actions.sendCapabilitiesRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedAction1).toEqual({
          type: EnvironmentActionConstants.REQUEST_CAPABILITIES,
          payload: undefined
        });

        expect(savedAction2).toEqual({
          type: EnvironmentActionConstants.CAPABILITIES_RECEIVED,
          payload: {foo:'bar'}
        });
        done();
      });
    });

    it('should send fetch request and then error on 403', (done) => {
      stubGetCapabilitiesRequest({companyId, status: HTTPSTATUS.FORBIDDEN});

      actions.sendCapabilitiesRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedAction1).toEqual({
          type: EnvironmentActionConstants.REQUEST_CAPABILITIES,
          payload: undefined
        });
        expect(savedAction2).toEqual({
          type: EnvironmentActionConstants.CAPABILITIES_ERROR,
          payload: 'Unsuccessful HTTP response'
        });
        done();
      });
    });

    it('should send fetch request and then error on invalid data', (done) => {
      stubGetCapabilitiesRequest({companyId, response: {notAKeyIExpect: 'wtf'}});

      actions.sendCapabilitiesRequestAction()(mockDispatch, mockGetState).then( res => {
        expect(savedAction1).toEqual({
          type: EnvironmentActionConstants.REQUEST_CAPABILITIES,
          payload: undefined
        });
        expect(savedAction2).toEqual({
          type: EnvironmentActionConstants.CAPABILITIES_ERROR,
          payload: 'data was not found on the payload for getCapabilities'
        });
        done();
      });
    });
  });

  describe('setUnscheduledDraftDatetime', () => {
    it('should dispatch an action to set the unscheduledDraftDatetime', () => {
      expect(actions.setUnscheduledDraftDatetime(670896000)).toEqual({
        type: EnvironmentActionConstants.SET_UNSCHEDULED_DRAFT_DATETIME,
        payload: 670896000
      });
    });
  });
});
