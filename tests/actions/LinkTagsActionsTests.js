/* @flow */
import _ from 'lodash';
import * as actions from 'actions/LinkTagsActions';
import { LinkTagsActionConstants } from 'constants/ActionConstants';
import {
  mockCreateLinkTag,
  stubCreateLinkTag,
  mockCreateLinkTagError,
  mockSaveLinkTag,
  stubSaveLinkTag,
  mockSaveLinkTagError
} from 'tests/api-responses/LinkTagsApiResponses';
import mockState from 'tests/test-utils/MockPublishingState';
import {
  SupportedNetworks,
  RequestStatus,
  LinkTags,
  DefaultNetwork } from 'constants/ApplicationConstants';
import {
  LinkTagPropertiesRecord,
  TagVariable } from 'records/LinkTagsRecords';
import {
  LabelsReducerState } from 'reducers/LabelsReducer';
import {
  fromJS,
  Map as ImmutableMap,
  List as ImmutableList
} from 'immutable';
import { CompanyRecord } from 'reducers/EnvironmentReducer';
import { EditorState } from 'draft-js';

export const defaultEnvironment = ImmutableMap({
  company: new CompanyRecord({
    enabledFeatures: ['link_tagging']
  })
});

const defaultRouter = {
  params: {
    companyId: '1',
    campaignId: '1'
  }
};

describe('LinkTags Actions', () => {

  describe('createLinkTagStart', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.createLinkTagStart(payload)).toEqual({
        type: LinkTagsActionConstants.CREATE_LINK_TAG_START,
        payload: payload
      });
    });
  });

  describe('createLinkTagCompleted', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.createLinkTagCompleted(payload)).toEqual({
        type: LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED,
        payload: payload
      });
    });
  });

  describe('createLinkTagFailed', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.createLinkTagFailed(payload)).toEqual({
        type: LinkTagsActionConstants.CREATE_LINK_TAG_FAILED,
        payload: payload
      });
    });
  });

  describe('updateLinkTagVariable', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.updateLinkTagVariable(payload)).toEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: payload
      });
    });
  });

  describe('saveLinkTagCompleted', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.saveLinkTagCompleted(payload)).toEqual({
        type: LinkTagsActionConstants.SAVE_LINK_TAG_COMPLETED,
        payload: payload
      });
    });
  });

  describe('saveLinkTagFailed', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.saveLinkTagFailed(payload)).toEqual({
        type: LinkTagsActionConstants.SAVE_LINK_TAG_FAILED,
        payload: payload
      });
    });
  });

  describe('saveLinkTagStart', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.saveLinkTagStart(payload)).toEqual({
        type: LinkTagsActionConstants.SAVE_LINK_TAG_START,
        payload: payload
      });
    });
  });

  describe('updateLinkTagVariableValues', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.updateLinkTagVariableValues(payload)).toEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE_VALUES,
        payload: payload
      });
    });
  });

  describe('createLinkTag', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                variableDescription: {
                  required: false
                }
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action) => {
      savedActions.push(action);
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should call createLinkTagStart action before add a link tag', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', DefaultNetwork)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[0]).toEqual({
            type: LinkTagsActionConstants.CREATE_LINK_TAG_START,
            payload: {
              link: 'foo.com',
              network: DefaultNetwork
            }
          });
          done();
        });
    });

    it('should call createLinkTagCompleted when get the link tag from the server', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', DefaultNetwork)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[1]).toEqual({
            type: LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED,
            payload: {
              linkTag: response,
              link: 'foo.com',
              network: DefaultNetwork
            }
          });
          done();
        });
    });

    it('should call createLinkTagCompleted when get the link tag from the server', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', DefaultNetwork)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[1]).toEqual({
            type: LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED,
            payload: {
              linkTag: response,
              link: 'foo.com',
              network: DefaultNetwork
            }
          });
          done();
        });
    });

    it('should call createLinkTagFailed when get an error from the server', (done) => {
      const response = mockCreateLinkTagError();
      stubCreateLinkTag({response, status: 422});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', DefaultNetwork)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[1]).toEqual({
            type: LinkTagsActionConstants.CREATE_LINK_TAG_FAILED,
            payload: {
              error: 'TAG_NO_DOMAIN_CONFIG_FOUND_FOR_URL',
              link: 'foo.com',
              network: DefaultNetwork
            }
          });
          done();
        });
    });
  });

  describe('createLinkTagsFromUrls', () => {
    let defaultState = {
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [DefaultNetwork]: new LinkTagPropertiesRecord()
          },
          'bar.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord()
          }
        })
      },
      environment: defaultEnvironment
    };
    let mockGetState = mockState(defaultState);

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should not create link tags for only the new urls when the feature flag linkTags is disable', () => {
      let aMockGetState = mockState({
        ...defaultState,
        environment: ImmutableMap({
          featureFlags: ImmutableMap({
            linkTags: false
          })
        })
      });

      expect(savedActions[0]).toBeUndefined();

      const urlsByNetworks = ImmutableMap({
        [DefaultNetwork]: ImmutableList(['foo.com', 'bar.com']),
        [SupportedNetworks.TWITTER]: ImmutableList(['foo.com'])
      });

      actions.createLinkTagsFromUrls(urlsByNetworks)(mockDispatch, aMockGetState);
      expect(savedActions[0]).toBeUndefined();
    });

    it('should create link tags for only the new urls', (done) => {
      expect(savedActions[0]).toBeUndefined();

      const urlsByNetworks = ImmutableMap({
        [DefaultNetwork]: ImmutableList(['foo.com', 'bar.com']),
        [SupportedNetworks.TWITTER]: ImmutableList(['foo.com'])
      });

      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      actions.createLinkTagsFromUrls(urlsByNetworks)(mockDispatch, mockGetState);

      _.defer(() => {
        expect(savedActions[0]).toEqual({
          type: LinkTagsActionConstants.CREATE_LINK_TAG_START,
          payload: {
            link: 'bar.com',
            network: DefaultNetwork
          }
        });
        expect(savedActions[1]).toEqual({
          type: LinkTagsActionConstants.CREATE_LINK_TAG_START,
          payload: {
            link: 'foo.com',
            network: SupportedNetworks.TWITTER
          }
        });
        done();
      });
    });
  });

  describe('saveLinkTags', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [DefaultNetwork]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                variableDescription: {
                  required: false
                }
              })])
            })
          },
          'bar.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 2,
              enabled: true,
              saveStatus: RequestStatus.STATUS_NEVER_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                variableDescription: {
                  required: false
                }
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should call saveLinkTagStart action before save a link tag', (done) => {
      const responseLinkTag = mockSaveLinkTag(2);
      stubSaveLinkTag(2, {response: responseLinkTag});

      expect(savedActions[0]).toBeUndefined();
      actions.saveLinkTags()(mockDispatch, mockGetState);
      _.defer(() => {
        expect(savedActions[0]).toEqual({
          type: LinkTagsActionConstants.SAVE_LINK_TAG_START,
          payload: {
            link: 'bar.com',
            network: SupportedNetworks.TWITTER
          }
        });
        done();
      });
    });

    it('should call saveLinkTagCompleted when the link tag has been saved', (done) => {
      const responseLinkTag = mockSaveLinkTag(2);
      stubSaveLinkTag(2, {response: responseLinkTag});

      expect(savedActions[0]).toBeUndefined();
      actions.saveLinkTags()(mockDispatch, mockGetState);
      _.defer(() => {
        expect(savedActions[1]).toEqual({
          type: LinkTagsActionConstants.SAVE_LINK_TAG_COMPLETED,
          payload: {
            link: 'bar.com',
            network: SupportedNetworks.TWITTER
          }
        });
        done();
      });
    });

    it('should call saveLinkTagFailed when the link tag could not been saved', (done) => {
      const responseLinkTag = mockSaveLinkTagError();
      stubSaveLinkTag(2, {response: responseLinkTag, status: 422});

      expect(savedActions[0]).toBeUndefined();
      actions.saveLinkTags()(mockDispatch, mockGetState);
      _.defer(() => {
        expect(savedActions[1]).toEqual({
          type: LinkTagsActionConstants.SAVE_LINK_TAG_FAILED,
          payload: {
            error: 'TAG_INVALID_VARIABLE_VALUE',
            link: 'bar.com',
            network: SupportedNetworks.TWITTER
          }
        });
        done();
      });
    });
  });

  describe('updateLinkTagVariable', () => {
    const defaultState = {
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [DefaultNetwork]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                variableDescription: {
                  required: false
                }
              })])
            })
          },
          'bar.com': {
            [DefaultNetwork]: new LinkTagPropertiesRecord({
              id: 2,
              enabled: true,
              saveStatus: RequestStatus.STATUS_NEVER_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                variableDescription: {
                  required: false
                }
              })])
            }),
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 2,
              enabled: true,
              saveStatus: RequestStatus.STATUS_NEVER_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                variableDescription: {
                  required: false
                }
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    };
    let mockGetState = mockState(defaultState);

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should not call updateLinkTagVariable action if linkTags feature flag is turned off', (done) => {
      let aMockGetState = mockState({
        ...defaultState,
        environment: ImmutableMap({
          featureFlags: ImmutableMap({
            linkTags: false
          })
        })
      });

      expect(savedActions[0]).toBeUndefined();

      actions.updateLinkTagVariableForAllNetworks('bar.com', 89, [{value: 'DH'}])(mockDispatch, aMockGetState);
      _.defer(() => {
        expect(savedActions[0]).toBeUndefined();
        done();
      });
    });

    it('should call updateLinkTagVariable action when update a link tag', (done) => {
      const tagVariableValue = [{value: 'DH'}];
      const tagVariableId = 89;
      const link =  'bar.com';
      const responseLinkTag = mockSaveLinkTag(2);
      stubSaveLinkTag(2, {response: responseLinkTag});

      expect(savedActions[0]).toBeUndefined();

      actions.updateLinkTagVariableForAllNetworks(link, tagVariableId, tagVariableValue)(mockDispatch, mockGetState);
      _.defer(() => {
        expect(savedActions[0]).toEqual({
          type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
          payload: { link, network: DefaultNetwork, tagVariableId, tagVariableValue }
        });
        expect(savedActions[1]).toEqual({
          type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
          payload: { link, network: SupportedNetworks.TWITTER, tagVariableId, tagVariableValue }
        });
        done();
      });
    });

    it('should call saveLinkTagsStart after update the link tag', (done) => {
      const tagVariableValue = [{value: 'DH'}];
      const tagVariableId = 89;
      const link =  'bar.com';
      const network = DefaultNetwork;
      const responseLinkTag = mockSaveLinkTag(2);
      stubSaveLinkTag(2, {response: responseLinkTag});

      expect(savedActions[0]).toBeUndefined();

      actions.updateLinkTagVariableForAllNetworks(link, tagVariableId, tagVariableValue)(mockDispatch, mockGetState);
      _.defer(() => {
        expect(savedActions[2]).toEqual({
          type: LinkTagsActionConstants.SAVE_LINK_TAG_START,
          payload: {link, network}
        });
        done();
      });
    });
  });

  describe('initialize link tag variable values', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                name: 'fillInTheBlank',
                type: 'text',
                variableDescription: fromJS({
                  required: false,
                  fieldName: 'Some text'
                })
              })]),
              variableValues: fromJS([{
                variableId: 103,
                values: [{value: 'Some text'}]
              }])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should copy the link tag variable values from a link tag of another network if there is any', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});
      const expectedValues = fromJS([{
        variableId: 103,
        values: [{value: 'Some text'}]
      }]);

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', DefaultNetwork)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[2]).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE_VALUES,
            payload: {
              newValues: expectedValues,
              link: 'foo.com',
              network: DefaultNetwork
            }
          });
          done();
        });
    });
  });

  describe('updateChannelTagVariableValue', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: 'channel',
                variableDescription: ImmutableMap({
                  TWITTER: 'tw'
                })
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set channel tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[2]).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 103,
              tagVariableValue: [{value: 'tw'}],
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });
  });

  describe('updateSfTrackingIdTagVariableValue', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.SPREDFAST_TRACKING_ID_TAG_VARIABLE_TYPE
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set spredfast tracking id tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[2]).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 103,
              tagVariableValue: [{value: LinkTags.SF_TRACKING_ID_TOKEN}],
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });
  });

  describe('updateSfTrackingIdTagVariableValue', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.DATETIME_TAG_VARIABLE_TYPE
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set datetime tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[2]).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 103,
              tagVariableValue: [{value: LinkTags.DATETIME_TOKEN}],
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });
  });

  describe('updateSingleValueTagVariableValue', () => {
    let mockGetState = mockState({
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 98,
                type: LinkTags.SINGLE_VALUE_TAG_VARIABLE_TYPE,
                variableDescription: ImmutableMap({
                  value: 'some value'
                })
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    });

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set single value tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[2]).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 98,
              tagVariableValue: [{value: 'some value'}],
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });
  });

  describe('updateSocialTagVariableValue', () => {
    const defaultState = {
      accounts: {
        checkedCredentials: ImmutableList([{
          socialNetwork: SupportedNetworks.TWITTER,
          uniqueId: 'someUniqueIdHash',
          name: 'Page'
        }, {
          socialNetwork: SupportedNetworks.TWITTER,
          uniqueId: 'someUniqueIdHash_2',
          name: 'Account'
        }, {
          socialNetwork: SupportedNetworks.FACEBOOK,
          uniqueId: 'someUniqueIdHash_3',
          name: 'Account 2'
        }])
      },
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.ACCOUNT_TAG_VARIABLE_TYPE,
                variableDescription: fromJS({
                  someUniqueIdHash: 'customValue'
                })
              })])
            })
          },
          'bar.com': {
            [SupportedNetworks.FACEBOOK]: new LinkTagPropertiesRecord({
              id: 2,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.ACCOUNT_TAG_VARIABLE_TYPE,
                variableDescription: fromJS({
                  someUniqueIdHash: 'customValue'
                })
              })])
            }),
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 3,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.ACCOUNT_TAG_VARIABLE_TYPE,
                variableDescription: fromJS({
                  someUniqueIdHash: 'customValue'
                })
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    };
    let mockGetState = mockState(defaultState);

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set social account tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions[2]).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 103,
              tagVariableValue: ImmutableList([{
                accountUniqueId: 'someUniqueIdHash',
                value: 'customValue'
              }, {
                accountUniqueId: 'someUniqueIdHash_2',
                value: 'Account'
              }]),
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });

    it('should not update all the social account tag variables when call updateSocialAccountTagVariables if the feature flag linkTags is turned off', () => {
      let aMockGetState = mockState({
        ...defaultState,
        environment: ImmutableMap({
          featureFlags: ImmutableMap({
            linkTags: false
          })
        })
      });

      expect(savedActions[0]).toBeUndefined();

      actions.updateSocialAccountTagVariables()(mockDispatch, aMockGetState);
      expect(savedActions[0]).toBeUndefined();
    });


    it('should update all the social account tag variables after call updateSocialAccountTagVariables', () => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions[0]).toBeUndefined();

      actions.updateSocialAccountTagVariables()(mockDispatch, mockGetState);
      expect(savedActions[0]).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: ImmutableList([{
            accountUniqueId: 'someUniqueIdHash',
            value: 'customValue'
          }, {
            accountUniqueId: 'someUniqueIdHash_2',
            value: 'Account'
          }])
        }
      });
      expect(savedActions[1]).toEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          link: 'bar.com',
          network: SupportedNetworks.FACEBOOK,
          tagVariableId: 103,
          tagVariableValue: ImmutableList([{
            accountUniqueId: 'someUniqueIdHash_3',
            value: 'Account 2'
          }])
        }
      });
      expect(savedActions[2]).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'bar.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: ImmutableList([{
            accountUniqueId: 'someUniqueIdHash',
            value: 'customValue'
          }, {
            accountUniqueId: 'someUniqueIdHash_2',
            value: 'Account'
          }])
        }
      });
    });
  });

  describe('updateLabelsTagVariableValue', () => {
    const defaultState = {
      labels: new LabelsReducerState({
        selectedLabels: ImmutableList([ImmutableMap({
          title: 'foo'
        })])
      }),
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.LABELS_TAG_VARIABLE_TYPE
              })])
            })
          },
          'bar.com': {
            [SupportedNetworks.FACEBOOK]: new LinkTagPropertiesRecord({
              id: 2,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.LABELS_TAG_VARIABLE_TYPE
              })])
            }),
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 3,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.LABELS_TAG_VARIABLE_TYPE
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    };
    let mockGetState = mockState(defaultState);

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set label tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions.shift()).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions.shift().type).toEqual(LinkTagsActionConstants.CREATE_LINK_TAG_START);
          expect(savedActions.shift().type).toEqual(LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED);
          expect(savedActions.shift()).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 103,
              tagVariableValue: fromJS([{
                value: 'foo'
              }]),
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });

    it('should not update all the labels tag variables when call updateLabelsTagVariables if the feature flag linkTags is turned off', () => {
      let aMockGetState = mockState({
        ...defaultState,
        environment: ImmutableMap({
          featureFlags: ImmutableMap({
            linkTags: false
          })
        })
      });

      expect(savedActions.shift()).toBeUndefined();

      actions.updateLabelsTagVariables()(mockDispatch, aMockGetState);
      expect(savedActions.shift()).toBeUndefined();
    });


    it('should update all the labels tag variables after call updateLabesTagVariables', () => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions.shift()).toBeUndefined();

      actions.updateLabelsTagVariables()(mockDispatch, mockGetState);
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: fromJS([{
            value: 'foo'
          }])
        }
      });
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          link: 'bar.com',
          network: SupportedNetworks.FACEBOOK,
          tagVariableId: 103,
          tagVariableValue: fromJS([{
            value: 'foo'
          }])
        }
      });
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'bar.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: fromJS([{
            value: 'foo'
          }])
        }
      });
    });
  });

  describe('updateTitleTagVariableValue', () => {
    const defaultState = {
      message: {
        messageEditorStateMap: ImmutableMap({
          [SupportedNetworks.TWITTER]: EditorState.createEmpty(),
          [SupportedNetworks.FACEBOOK]: EditorState.createEmpty()
        }),
        messageTitle: 'foo'
      },
      router: defaultRouter,
      linkTags: {
        urls: fromJS({
          'foo.com': {
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 1,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.TITLE_TAG_VARIABLE_TYPE
              })])
            })
          },
          'bar.com': {
            [SupportedNetworks.FACEBOOK]: new LinkTagPropertiesRecord({
              id: 2,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.TITLE_TAG_VARIABLE_TYPE
              })])
            }),
            [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
              id: 3,
              enabled: true,
              saveStatus: RequestStatus.STATUS_REQUESTED,
              tagVariables: ImmutableList([new TagVariable({
                id: 103,
                type: LinkTags.TITLE_TAG_VARIABLE_TYPE
              })])
            })
          }
        })
      },
      environment: defaultEnvironment
    };
    let mockGetState = mockState(defaultState);

    let savedActions;

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should set title tag variable after link tag creation', (done) => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions.shift()).toBeUndefined();
      actions.createLinkTag('foo.com', SupportedNetworks.TWITTER)(mockDispatch, mockGetState)
        .then(() => {
          expect(savedActions.shift().type).toEqual(LinkTagsActionConstants.CREATE_LINK_TAG_START);
          expect(savedActions.shift().type).toEqual(LinkTagsActionConstants.CREATE_LINK_TAG_COMPLETED);
          expect(savedActions.shift()).toEqual({
            type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
            payload: {
              tagVariableId: 103,
              tagVariableValue: fromJS([{
                value: 'foo'
              }]),
              link: 'foo.com',
              network: SupportedNetworks.TWITTER
            }
          });
          done();
        });
    });

    it('should not update all the title tag variables when call updateTitleTagVariables if the feature flag linkTags is turned off', () => {
      let aMockGetState = mockState({
        ...defaultState,
        environment: ImmutableMap({
          featureFlags: ImmutableMap({
            linkTags: false
          })
        }),
        message: {
          messageTitle: 'foo'
        }
      });

      expect(savedActions.shift()).toBeUndefined();

      actions.updateTitleTagVariables()(mockDispatch, aMockGetState);
      expect(savedActions.shift()).toBeUndefined();
    });


    it('should update all the title tag variables after call updateTitleTagVariables', () => {
      const response = mockCreateLinkTag();
      stubCreateLinkTag({response});

      expect(savedActions.shift()).toBeUndefined();

      actions.updateTitleTagVariables()(mockDispatch, mockGetState);
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: fromJS([{
            value: 'foo'
          }])
        }
      });
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          link: 'bar.com',
          network: SupportedNetworks.FACEBOOK,
          tagVariableId: 103,
          tagVariableValue: fromJS([{
            value: 'foo'
          }])
        }
      });
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'bar.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: fromJS([{
            value: 'foo'
          }])
        }
      });
    });
  });
});
