/* @flow */
import * as messageActions from 'actions/MessageActions';
import { EditorState, ContentState } from 'draft-js';
import mockState from 'tests/test-utils/MockPublishingState';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { createMessageStateFromPayload } from 'adapters/MessageAdapter';
import { MessageActionConstants,
  MediaActionConstants,
  MentionsActionConstants,
  EnvironmentActionConstants,
  LinkPreviewActionConstants,
  NotesActionConstants,
  LinksActionConstants,
  FacebookActionConstants,
  AssigneeActionConstants,
  AccountsActionConstants,
  LinkTagsActionConstants } from 'constants/ActionConstants';
import {
  stubCreateMessagesRequest,
  stubEditMessagesRequest,
  mockMessageResponse,
  stubReadMessagesRequest,
  mockGetLinkPreviewResponse,
  stubGetLinkPreviewRequest } from 'tests/api-responses/MessageApiResponses';
import {
  fromJS,
  Map as ImmutableMap,
  Set as ImmutableSet,
  List as ImmutableList
} from 'immutable';
import { HTTPSTATUS,
  MessageConstants,
  RequestStatus,
  DefaultNetwork,
  MAX_MESSAGE_TITLE_CHARS } from 'constants/ApplicationConstants';
import { buildMessageCompletedNotification } from 'utils/NotificationUtils';
import { TimezoneRecord, CompanyRecord } from 'reducers/EnvironmentReducer';
import { PlanActionConstants } from 'constants/ActionConstants';
import { ADD_NOTIFICATION } from '@spredfast/react-lib/lib/growler-notification/redux/actions';
import _ from 'lodash';
import { defaultEnvironment as linkTagsDefaultEnvironment} from 'tests/actions/LinkTagsActionsTests';
import {
  SupportedNetworks,
  LinkTags } from 'constants/ApplicationConstants';
import {
  LinkTagPropertiesRecord,
  TagVariable } from 'records/LinkTagsRecords';

const defaultLinkTags = {
  urls: fromJS({
    'foo.com': {
      [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
        id: 1,
        enabled: true,
        saveStatus: RequestStatus.STATUS_REQUESTED,
        tagVariables: ImmutableList([new TagVariable({
          id: 103,
          type: LinkTags.TITLE_TAG_VARIABLE_TYPE
        })])
      })
    }
  })
};

const defaultMockState = {
  router: {
    params: {
      companyId: '2',
      campaignId: '2',
      eventId: '2'
    }
  },
  environment: {
    userId: 1,
    userTimezone: new TimezoneRecord(),
    capabilities: fromJS({
      [SupportedNetworks.FACEBOOK]: {
        NEW_MESSAGE: {
          NOTE: {
            linkPreview: {
              supported: true
            }
          }
        }
      },
      [SupportedNetworks.TWITTER]: {
        NEW_MESSAGE: {
          NOTE: {
            linkPreview: {
              supported: false
            }
          }
        }
      }
    }),
    company: new CompanyRecord({
      companyMetadata: {
        shortenLinksByDefault: true
      }
    })
  },
  message: {
    messageStatus: MessageConstants.PENDING,
    messageEditorStateMap: ImmutableMap({
      [DefaultNetwork]: EditorState.createWithContent(ContentState.createFromText('this is a test www.google.com of the link detection system www.reddit.com'))
    })
  },
  assignee: {
    firstName: 'Bob',
    lastName: 'Kelso',
    id: 1,
    conflictingVoices: [],
    voices: [],
    invalid: false
  },
  images: {
    imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
    imageUploadError: '',
    imageId: null,
    imageFullUrl: '',
    imageThumbUrl: ''
  },
  linkPreview: {
    linkUrl: 'www.google.com'
  },
  links: {
    urlProperties: ImmutableMap({
      'www.test.com': new UrlPropertiesRecord({
        shortenOnPublish: true,
        addLinkTags: true,
        associatedNetworks: ImmutableSet()
      })
    })
  },
  plan:{
    currentPlan: ImmutableMap({
      name: 'test',
      id: '1',
      color: 'blue'
    })
  }
};

describe('Message Actions', () => {
  let actionQueue;
  const mockDispatch = (action) => {
    actionQueue.push(action);
  };

  beforeEach(() => {
    actionQueue = [];
  });

  describe('addMessageTitle Action', () => {
    it('should create an action to add a new message title', () => {
      expect(messageActions.updateMessageTitle('TEST')).toEqual({
        type: MessageActionConstants.UPDATE_MESSAGE_TITLE,
        payload: 'TEST'
      });
    });
  });

  describe('messageTitleChanged', () => {
    let mockGetState = mockState({
      ...defaultMockState,
      environment: linkTagsDefaultEnvironment,
      linkTags: defaultLinkTags,
      message: {
        messageEditorStateMap: ImmutableMap({
          [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText(''))
        }),
        messageTitle: 'foo'
      }
    });
    let actionQueue = [];

    const mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        actionQueue.push(action);
      }
    };

    beforeEach(() => {
      actionQueue = [];
    });

    it('should not truncate when length is less than max', () => {
      messageActions.messageTitleChanged('new title')(mockDispatch);
      expect(actionQueue.shift()).toEqual({
        type: MessageActionConstants.UPDATE_MESSAGE_TITLE,
        payload: 'new title'
      });
    });

    it('should truncate when length is more than max', () => {
      //Create a test title with the max length. Use +1 since join is in between array elements
      const longTitle = new Array(MAX_MESSAGE_TITLE_CHARS + 1).join( 'f' );

      //now add an extra char
      const invalidTitle = longTitle + 'u';
      messageActions.messageTitleChanged(invalidTitle)(mockDispatch);
      expect(actionQueue.shift()).toEqual({
        type: MessageActionConstants.UPDATE_MESSAGE_TITLE,
        payload: longTitle
      });
    });

    it('should call updateTitleTagVariables after the message was changed', () => {
      messageActions.messageTitleChanged('foo')(mockDispatch);

      expect(actionQueue.shift().type).toEqual(MessageActionConstants.UPDATE_MESSAGE_TITLE);

      expect(actionQueue.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: fromJS([{value: 'foo'}])
        }
      });
    });
  });

  describe('updateMessageBody Action', () => {
    it('should create an action to add a new message text', () => {
      let action = messageActions.updateMessageBody(EditorState.createEmpty());
      const stateText = action.payload.getCurrentContent().getPlainText();
      const editorStateText = EditorState.createEmpty().getCurrentContent().getPlainText();

      expect(action.type).toBe(MessageActionConstants.UPDATE_MESSAGE_BODY);
      expect(stateText === editorStateText).toBeTruthy();
    });
  });

  describe('sendUpdatedMessageBody Action', () => {
    let mockGetState = mockState({
      ...defaultMockState,
      environment: linkTagsDefaultEnvironment,
      linkTags: defaultLinkTags,
      message: {
        messageEditorStateMap: ImmutableMap({
          [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText(''))
        }),
        messageTitle: 'foo'
      },
      links: {
        urlProperties: new ImmutableMap()
      }
    });
    let savedActions = [];

    const mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should create an action when network is in state', () => {
      messageActions.sendUpdatedMessageBody({
        editorState: EditorState.createEmpty(),
        network: SupportedNetworks.TWITTER
      })(mockDispatch, mockGetState);

      expect((savedActions[0] || {}).type).toBe(MessageActionConstants.UPDATE_MESSAGE_BODY);
      expect((savedActions[0] || {}).payload.get(SupportedNetworks.TWITTER).getCurrentContent().getPlainText()).toEqual('');
    });

    it('should NOT create an action when network is not in state', () => {
      const mockGetState = mockState({
        message:{
          messageEditorStateMap: ImmutableMap({
            [DefaultNetwork]: EditorState.createEmpty()
          })
        }
      });

      messageActions.sendUpdatedMessageBody({
        editorState: EditorState.createEmpty(),
        network: SupportedNetworks.TWITTER
      })(mockDispatch, mockGetState);

      expect(savedActions[0]).toBeUndefined();
    });

    it('should not call updateTitleTagVariables if a message title is defined', () => {
      messageActions.sendUpdatedMessageBody({
        editorState: EditorState.createEmpty(),
        network: SupportedNetworks.TWITTER
      })(mockDispatch, mockGetState);

      expect(savedActions.find(action => action.type === LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE)).toBeUndefined();
    });

    it('should call updateTitleTagVariables if a message title is not defined', () => {
      mockGetState = mockState({
        ...defaultMockState,
        environment: linkTagsDefaultEnvironment,
        linkTags: defaultLinkTags,
        message: {
          messageEditorStateMap: ImmutableMap({
            [SupportedNetworks.TWITTER]: EditorState.createWithContent(ContentState.createFromText('some text'))
          })
        }
      });

      messageActions.sendUpdatedMessageBody({
        editorState: EditorState.createEmpty(),
        network: SupportedNetworks.TWITTER
      })(mockDispatch, mockGetState);

      expect(savedActions.find(action => action.type === LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE)).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: fromJS([{value: 'some text'}])
        }
      });
    });
  });

  describe('applyToAllChannels Action', () => {
    describe('applyToAllChannels Helper', () => {
      const network = 'TEST';
      const messageEditorState = EditorState.createEmpty();

      it('should pass through the supplied values as a payload', () => {
        messageActions.applyToAllChannelsWithNotification({messageEditorState, network})(mockDispatch);

        expect(actionQueue[0]).toEqual({
          type: MessageActionConstants.APPLY_TO_ALL_CHANNELS,
          payload: {messageEditorState, network}
        });
      });

    });
  });

  describe('undoApplyToAllToAlertAndRemoveNotification Action', () => {
    describe('undoApplyToAllToAlertAndRemoveNotification Helper', () => {
      const messageId = 'messageId';

      it('should pass through the supplied values as a payload', () => {
        messageActions.undoApplyToAllToAlertAndRemoveNotification(messageId)(mockDispatch);

        expect(actionQueue[0]).toEqual({
          type: MessageActionConstants.UNDO_APPLY_ALL,
          payload: messageId
        });

        expect(actionQueue[1]).toEqual({
          type: 'REMOVE_NOTIFICATION',
          payload: messageId
        });
      });

    });
  });

  describe('undoApplyToAllToAlert Action', () => {
    it('should pass through the supplied value as a payload', () => {
      expect(messageActions.undoApplyToAllToAlert('TEST')).toEqual({
        type: MessageActionConstants.UNDO_APPLY_ALL,
        payload: 'TEST'
      });
    });
  });

  describe('async actions', () => {
    let currentAction;

    const mockGetState = mockState(defaultMockState);

    let mockGetStateForMockDispatch = mockGetState;

    const mockDispatch  = (action) => {
      if (typeof action === 'function') {
        return action(mockDispatch, mockGetStateForMockDispatch);
      } else {
        actionQueue.push(action);
      }
    };

    describe('saveMessage action', () => {
      const companyId = mockGetState().router.params.companyId;
      const initiativeId = mockGetState().router.params.campaignId;
      const userId = mockGetState().environment.userId;
      const messageStatus = MessageConstants.PENDING;
      const assignee = mockGetState().assignee;
      const response = mockMessageResponse({companyId, initiativeId, assignee, userId, messageStatus});

      describe('creating a message', () => {
        let mockGetState = mockState({
          message: {
            messageStatus: MessageConstants.PENDING
          },
          router: {
            params: {
              companyId: '2',
              campaignId: '2'
            }
          },
          environment: {
            userId: 1
          },
          assignee: {
            firstName: 'Bob',
            lastName: 'Kelso',
            id: 1,
            conflictingVoices: [],
            voices: [],
            invalid: false
          },
          labels: {}
        });

        it('dispatches a start and complete action on successful api call', (done) => {
          stubCreateMessagesRequest({companyId, initiativeId, response});
          messageActions.saveMessage(MessageConstants.PENDING)(mockDispatch, mockGetState)
          .then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.REQUEST_SAVE_MESSAGE,
              payload: mockGetState().message.messageStatus
            });
            done();
          });
        });

        it('dispatches a start and complete action on successful api call with copy', (done) => {
          let mockGetState = mockState({
            message: {
              messageStatus: MessageConstants.PENDING
            },
            router: {
              params: {
                companyId: '2',
                campaignId: '2',
                copyId: '2'
              }
            },
            environment: {
              userId: 1
            }
          });
          stubCreateMessagesRequest({companyId, initiativeId, response, isCopy: true});
          messageActions.saveMessage(MessageConstants.PENDING)(mockDispatch, mockGetState)
          .then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.REQUEST_SAVE_MESSAGE,
              payload: mockGetState().message.messageStatus
            });
            done();
          });
        });

        it('dispatches a start but does dispatch a fail on failed api call', (done) => {
          stubCreateMessagesRequest({companyId, initiativeId, status: HTTPSTATUS.INTERNAL_SERVER_ERROR});
          messageActions.saveMessage(MessageConstants.PENDING)(mockDispatch, mockGetState)
          .then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.REQUEST_SAVE_MESSAGE,
              payload: mockGetState().message.messageStatus
            });
            done();
          });
        });
      });

      describe('edit a message', () => {
        const eventId = mockGetState().router.params.eventId;

        it('dispatches a start and complete action on successful api call', (done) => {
          stubEditMessagesRequest({companyId, initiativeId, eventId, response});
          messageActions.saveMessage(MessageConstants.PENDING)(mockDispatch, mockGetState)
          .then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.REQUEST_SAVE_MESSAGE,
              payload: mockGetState().message.messageStatus
            });
            done();
          });
        });

        it('dispatches a start but does dispatch a fail on failed api call', (done) => {
          stubEditMessagesRequest({companyId, initiativeId, eventId, status: HTTPSTATUS.INTERNAL_SERVER_ERROR});
          messageActions.saveMessage(MessageConstants.PENDING)(mockDispatch, mockGetState)
          .then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.REQUEST_SAVE_MESSAGE,
              payload: mockGetState().message.messageStatus
            });
            expect(actionQueue[1].type).toEqual(ADD_NOTIFICATION);
            expect(actionQueue[2]).toEqual({
              type: MessageActionConstants.SAVE_MESSAGE_REQUEST_FAILED,
              payload: undefined
            });
            done();
          });
        });
      });
    });

    describe('readMessage action', () => {
      const companyId = mockGetState().router.params.companyId;
      const initiativeId = mockGetState().router.params.campaignId;
      const eventId = mockGetState().router.params.eventId;
      const userId = mockGetState().environment.userId;
      const userOffset = mockGetState().environment.userTimezone.offset;
      const planId = mockGetState().plan.currentPlan.get('id');
      const messageStatus = MessageConstants.PENDING;
      const scheduledPublishDate = 675993600;
      const assignee = mockGetState().assignee;

      const response = mockMessageResponse({companyId, initiativeId, assignee, userId, eventId, planId, messageStatus, scheduledPublishDate});
      const linkPreviewResponse = mockGetLinkPreviewResponse();

      it('dispatches a start and complete action on successful api call', (done) => {
        stubReadMessagesRequest({companyId, initiativeId, eventId, response});
        stubGetLinkPreviewRequest(
          {
            companyId: companyId,
            campaignId: initiativeId,
            url: 'www.nfl.com',
            response: linkPreviewResponse
          });
        const readMessagePromise = messageActions.readMessageAction()(mockDispatch, mockGetState);

        expect(readMessagePromise).toBeTruthy();
        if (readMessagePromise) {
          readMessagePromise.then(res => {

            currentAction = actionQueue.shift();
            expect(currentAction).toEqual({
              type: MessageActionConstants.READ_MESSAGE_STARTED,
              payload: undefined
            });

            const transformedPayload = createMessageStateFromPayload(response, userOffset);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(MessageActionConstants.READ_MESSAGE_COMPLETED);
            expect(currentAction.payload).toEqualMessage(transformedPayload.message);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(LinksActionConstants.UPDATE_FOUND_URLS);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(LinksActionConstants.UPDATE_URL_PROPERTIES);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(MediaActionConstants.READ_IMAGES_COMPLETED);
            expect(currentAction.payload).toEqual(transformedPayload.images);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(LinkPreviewActionConstants.READ_LINK_PREVIEW_COMPLETED);
            expect(currentAction.payload).toEqual(transformedPayload.linkPreview);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(NotesActionConstants.READ_NOTES_COMPLETED);
            expect(currentAction.payload).toEqual(transformedPayload.notes);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(FacebookActionConstants.READ_FACEBOOK_COMPLETED);
            expect(currentAction.payload).toEqual(transformedPayload.facebook);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(PlanActionConstants.UPDATE_CURRENT_PLAN);
            expect(currentAction.payload).toEqual(fromJS({ name: '', id: '', color: '' }));

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(MentionsActionConstants.UPDATE_MENTIONS);
            expect(currentAction.payload).toEqual(transformedPayload.message.mentions);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(AssigneeActionConstants.UPDATE_CURRENT_ASSIGNEE);
            expect(currentAction.payload.toJS()).toEqual(response.assignee);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(EnvironmentActionConstants.USER_CAN_EDIT_PERMISSION);
            expect(currentAction.payload).toEqual(response.userAuthorizedForEdit);

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(AccountsActionConstants.VOICE_CRED_TOGGLE);
            expect(currentAction.payload.equals(transformedPayload.checkedCredentials)).toBeTruthy();

            currentAction = actionQueue.shift();
            expect(currentAction.type).toBe(LinkPreviewActionConstants.UPDATE_LINK_PREVIEW_IMAGES);
            expect(currentAction.payload.sourceMedia.size).toBe(2);
            expect(currentAction.payload.sourceMedia.first().id).toBe(-1);
            expect(currentAction.payload.sourceMedia.first().src).toBe('https://www.facebook.com/images/fb_icon_325x325.png');
            expect(currentAction.payload.sourceMedia.last().id).toBe(-1);
            expect(currentAction.payload.sourceMedia.last().src).toBe('https://scontent.xx.fbcdn.net/t39.2365-6/851565_602269956474188_918638970_n.png');
            expect(currentAction.payload.customUploadedMedia.size).toBe(1);
            expect(currentAction.payload.customUploadedMedia.first().id).toBe(-1);
            expect(currentAction.payload.customUploadedMedia.first().src).toBe('http://i.nflcdn.com/static/content/public/static/img/share/shield.jpg');

            done();
          });
        }
      });

      it('dispatches a start and complete action on successful api call for copy', (done) => {
        let mockGetState = mockState({
          router: {
            params: {
              companyId: '2',
              campaignId: '2',
              copyId: '2'
            }
          },
          environment: {
            userId: 1,
            userTimezone: new TimezoneRecord()
          }
        });
        stubReadMessagesRequest({companyId, initiativeId, eventId, response, isCopy: true});
        stubGetLinkPreviewRequest(
          {
            companyId: companyId,
            campaignId: initiativeId,
            url: 'www.nfl.com',
            response: linkPreviewResponse
          });
        const readMessagePromise = messageActions.readMessageAction()(mockDispatch, mockGetState);

        expect(readMessagePromise).toBeTruthy();
        if (readMessagePromise) {
          readMessagePromise.then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.READ_MESSAGE_STARTED,
              payload: undefined
            });

            const transformedPayload = createMessageStateFromPayload(response, userOffset);

            expect(actionQueue[1].type).toBe(MessageActionConstants.READ_MESSAGE_COMPLETED);
            expect(actionQueue[1].payload).toEqualMessage(transformedPayload.message);

            expect(actionQueue[2].type).toBe(LinksActionConstants.UPDATE_FOUND_URLS);

            expect(actionQueue[3].type).toBe(LinksActionConstants.UPDATE_URL_PROPERTIES);

            expect(actionQueue[4].type).toBe(MediaActionConstants.READ_IMAGES_COMPLETED);
            expect(actionQueue[4].payload).toEqual(transformedPayload.images);

            expect(actionQueue[5].type).toBe(LinkPreviewActionConstants.READ_LINK_PREVIEW_COMPLETED);
            expect(actionQueue[5].payload).toEqual(transformedPayload.linkPreview);

            expect(actionQueue[6].type).toBe(NotesActionConstants.READ_NOTES_COMPLETED);
            expect(actionQueue[6].payload).toEqual(transformedPayload.notes);

            expect(actionQueue[7].type).toBe(FacebookActionConstants.READ_FACEBOOK_COMPLETED);
            expect(actionQueue[7].payload).toEqual(transformedPayload.facebook);

            expect(actionQueue[8].type).toBe(PlanActionConstants.UPDATE_CURRENT_PLAN);
            expect(actionQueue[8].payload).toEqual(fromJS({ name: '', id: '', color: '' }));

            expect(actionQueue[9].type).toBe(MentionsActionConstants.UPDATE_MENTIONS);
            expect(actionQueue[9].payload).toEqual(transformedPayload.message.mentions);

            expect(actionQueue[10].type).toBe(AssigneeActionConstants.UPDATE_CURRENT_ASSIGNEE);
            expect(actionQueue[10].payload.toJS()).toEqual(response.assignee);

            expect(actionQueue[11].type).toBe(EnvironmentActionConstants.USER_CAN_EDIT_PERMISSION);
            expect(actionQueue[11].payload).toEqual(response.userAuthorizedForEdit);

            expect(actionQueue[12].type).toBe(AccountsActionConstants.VOICE_CRED_TOGGLE);
            expect(actionQueue[12].payload.equals(transformedPayload.checkedCredentials)).toBeTruthy();

            expect(actionQueue[13].type).toBe(LinkPreviewActionConstants.UPDATE_LINK_PREVIEW_IMAGES);
            expect(actionQueue[13].payload.sourceMedia.size).toBe(2);
            expect(actionQueue[13].payload.sourceMedia.first().id).toBe(-1);
            expect(actionQueue[13].payload.sourceMedia.first().src).toBe('https://www.facebook.com/images/fb_icon_325x325.png');
            expect(actionQueue[13].payload.sourceMedia.last().id).toBe(-1);
            expect(actionQueue[13].payload.sourceMedia.last().src).toBe('https://scontent.xx.fbcdn.net/t39.2365-6/851565_602269956474188_918638970_n.png');
            expect(actionQueue[13].payload.customUploadedMedia.size).toBe(1);
            expect(actionQueue[13].payload.customUploadedMedia.first().id).toBe(-1);
            expect(actionQueue[13].payload.customUploadedMedia.first().src).toBe('http://i.nflcdn.com/static/content/public/static/img/share/shield.jpg');

            done();
          });
        }
      });

      it('dispatches a start and failed action on failed api call', (done) => {
        stubReadMessagesRequest({companyId, initiativeId, eventId, status: HTTPSTATUS.INTERNAL_SERVER_ERROR});

        const readMessagePromise = messageActions.readMessageAction(companyId, initiativeId, eventId)(mockDispatch, mockGetState);
        expect(readMessagePromise).toBeTruthy();
        if (readMessagePromise) {
          readMessagePromise.then(res => {
            expect(actionQueue[0]).toEqual({
              type: MessageActionConstants.READ_MESSAGE_STARTED,
              payload: undefined
            });
            expect(actionQueue[1]).toEqual({
              type: MessageActionConstants.READ_MESSAGE_FAILED,
              payload: 'Unsuccessful HTTP response'
            });
            done();
          });
        }
      });
    });

    describe('notifyMessageCompletion action', () => {
      it('should dispatch an action to trigger a growler on the resolution of a message save (save or fail)', () => {
        messageActions.notifyMessageCompletion(mockGetState(), MessageConstants.PENDING, true)(mockDispatch);
        expect(actionQueue[0]).toEqual({
          type: ADD_NOTIFICATION,
          payload: buildMessageCompletedNotification('Your message has been published.', 'PUBLISHED', true)
        });
      });
    });
  });
});
