/* @flow */
import * as actions from 'actions/MediaActions';
import { MediaActionConstants } from 'constants/ActionConstants';
import {
  mockImageUploadResponse,
  stubImageUploadRequest,
  mockSessionNonceTokenResponse,
  stubSessionNonceTokenRequest,
  mockVideoUploadResponse,
  stubVideoUploadRequest,
  mockVideoTranscodingProgressResponse,
  stubVideoTranscodingProgressRequest,
  mockVideoTranscodingResultsResponse,
  stubVideoTranscodingResultsRequest } from 'tests/api-responses/MediaApiResponses';
import { RequestStatus } from 'constants/ApplicationConstants';
import { createAsyncDispatch } from 'tests/test-utils/PublishingReduxTestUtils';
import mockStore from 'tests/test-utils/MockPublishingStore';
import mockState from 'tests/test-utils/MockPublishingState';
import { EditorState} from 'draft-js';
import { fromJS, Map as ImmutableMap, Set as ImmutableSet } from 'immutable';
import { SocialVideoRecord } from 'records/VideosRecords';

const defaultCapabilities = fromJS({
  FACEBOOK: {
    NEW_MESSAGE: {
      VIDEO: {
        videoCapabilityDTO: {}
      }
    }
  },
  TWITTER: {
    NEW_MESSAGE: {
      VIDEO: {}
    }
  }
});

const defaultMessageEditorStateMap = ImmutableMap({
  FACEBOOK: EditorState.createEmpty(),
  TWITTER: EditorState.createEmpty()
});


describe('Media Actions', () => {

  describe('requestUploadImage', () => {
    it('should create expected structure', () => {
      expect(actions.requestUploadImage()).toEqual({
        type: MediaActionConstants.REQUEST_UPLOAD_IMAGE,
        payload: undefined
      });
    });
  });

  describe('uploadImageSucceeded', () => {
    it('should create expected structure', () => {
      expect(actions.uploadImageSucceeded({foo:'bar'})).toEqual({
        type: MediaActionConstants.UPLOAD_IMAGE_SUCCEEDED,
        payload: {foo:'bar'}
      });
    });
  });

  describe('uploadImageFailed', () => {
    it('should create expected structure', () => {
      let error = new Error('test error');
      expect(actions.uploadImageFailed(error)).toEqual({
        type: MediaActionConstants.UPLOAD_IMAGE_FAILED,
        payload: error,
        error: true
      });
    });
  });

  describe('readImagesCompleted', () => {
    it('should create expected structure', () => {
      expect(actions.readImagesCompleted()).toEqual({
        type: MediaActionConstants.READ_IMAGES_COMPLETED,
        payload: undefined
      });
    });
  });

  describe('upload image async', () => {
    // define a mock initial state
    let newMockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      },
      images: {
        imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
      }
    });

    // setup a mock store that records actions
    let store = mockStore(newMockGetState, {
      reduxSaga: true
    });

    const asyncDispatch = createAsyncDispatch(store);
    const response = mockImageUploadResponse();
    //NOTE: this doesn't work in all browsers....it's fine in Chrome, which
    //      is what we're using for karma right now. Not sure what we should
    //      do if we want to run these tests on other browsers.
    const dummyFile = new File([''], 'dummy.png', {type: 'image/png', lastModified: ''});
    let mockActions;

    beforeEach(() => {
      store.clearActions();
      mockActions = store.getActions();
    });

    it('should request to upload an image, and on success give a successful reply', (done) => {
      stubImageUploadRequest({ companyId: '2', campaignId: '2', response });

      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.sendUploadImageRequest(dummyFile))
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: MediaActionConstants.UPLOAD_IMAGE,
            payload: dummyFile
          });
          expect(mockActions[1]).toEqual({
            type: MediaActionConstants.REQUEST_UPLOAD_IMAGE,
            payload: undefined
          });
          expect(mockActions[2]).toEqual({
            type: MediaActionConstants.UPLOAD_IMAGE_SUCCEEDED,
            payload: {
              id: response.data.id,
              fullUrl: response.data.fullImage,
              thumbUrl: response.data.image,
              filesize: response.data.imageSizeAfterResizing,
              mimeType: response.data.mimeType,
              imageWidth: response.data.imageWidth,
              imageHeight: response.data.imageHeight
            }
          });

          done();
        });
    });

    it('should request to upload an image, and on success give a successful reply', (done) => {
      stubImageUploadRequest({ companyId: '2', campaignId: '2', status: 400 });

      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.sendUploadImageRequest(dummyFile))
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: MediaActionConstants.UPLOAD_IMAGE,
            payload: dummyFile
          });
          expect(mockActions[1]).toEqual({
            type: MediaActionConstants.REQUEST_UPLOAD_IMAGE,
            payload: undefined
          });
          expect(mockActions[2]).toEqual({
            type: MediaActionConstants.UPLOAD_IMAGE_FAILED,
            payload: 'Unsuccessful HTTP response'
          });

          done();
        });
    });

    it('should request to upload an image, and on success give a successful reply', () => {
      stubImageUploadRequest({ companyId: '2', campaignId: '2', response });

      expect(mockActions[0]).toBeUndefined();
      store.dispatch(actions.sendUploadImageRequest(dummyFile));
      store.dispatch(actions.uploadImageCanceled());

      expect(mockActions[0]).toEqual({
        type: MediaActionConstants.UPLOAD_IMAGE,
        payload: dummyFile
      });
      //We can only check that the final action is Cancel, since a
      // bunch of actions can happen at the same time before cancel
      expect(mockActions[mockActions.length - 1]).toEqual({
        type: MediaActionConstants.UPLOAD_IMAGE_CANCELED,
        payload: undefined
      });
    });
  });


  describe('removeUploadedVideo', () => {
    it('should create expected structure', () => {
      expect(actions.removeUploadedVideo()).toEqual({
        type: MediaActionConstants.REMOVE_UPLOADED_VIDEO,
        payload: undefined
      });
    });
  });

  describe('requestUploadVideo', () => {
    it('should create expected structure', () => {
      expect(actions.requestUploadVideo()).toEqual({
        type: MediaActionConstants.REQUEST_UPLOAD_VIDEO,
        payload: undefined
      });
    });
  });

  describe('uploadVideoSucceeded', () => {
    it('should create expected structure', () => {
      const uploadVideoResponse = mockVideoUploadResponse();
      expect(actions.uploadVideoSucceeded(uploadVideoResponse)).toEqual({
        type: MediaActionConstants.UPLOAD_VIDEO_SUCCEEDED,
        payload: uploadVideoResponse
      });
    });
  });

  describe('uploadVideoFailed', () => {
    it('should create expected structure', () => {
      expect(actions.uploadVideoFailed()).toEqual({
        type: MediaActionConstants.UPLOAD_VIDEO_FAILED,
        payload: undefined
      });
    });
  });

  describe('requestTranscodeVideoProgress', () => {
    it('should create expected structure', () => {
      expect(actions.requestTranscodeVideoProgress()).toEqual({
        type: MediaActionConstants.REQUEST_TRANSCODE_VIDEO_PROGRESS,
        payload: undefined
      });
    });
  });

  describe('updateTranscodeVideoProgress', () => {
    const videoProgress = mockVideoTranscodingProgressResponse();
    it('should create expected structure', () => {
      expect(actions.updateTranscodeVideoProgress(videoProgress)).toEqual({
        type: MediaActionConstants.UPDATE_TRANSCODE_VIDEO_PROGRESS,
        payload: videoProgress
      });
    });
  });

  describe('transcodeVideoSucceeded', () => {
    it('should create expected structure', () => {
      expect(actions.transcodeVideoSucceeded()).toEqual({
        type: MediaActionConstants.TRANSCODE_VIDEO_SUCCEEDED,
        payload: undefined
      });
    });
  });

  describe('requestTranscodeVideoResults', () => {
    it('should create expected structure', () => {
      expect(actions.requestTranscodeVideoResults()).toEqual({
        type: MediaActionConstants.REQUEST_TRANSCODE_VIDEO_RESULTS,
        payload: undefined
      });
    });
  });

  describe('transcodeVideoResultsSucceeded', () => {
    it('should create expected structure', () => {
      const transcodeVideoResults = mockVideoTranscodingResultsResponse();
      expect(actions.transcodeVideoResultsSucceeded(transcodeVideoResults)).toEqual({
        type: MediaActionConstants.TRANSCODE_VIDEO_RESULTS_SUCCEEDED,
        payload: transcodeVideoResults
      });
    });
  });

  describe('upload video async', () => {
    const videoKey = 'key';
    const newMockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      },
      environment: {
        capabilities: defaultCapabilities
      },
      message: {
        messageEditorStateMap: defaultMessageEditorStateMap
      },
      videos: {
        socialVideos: ImmutableSet([
          new SocialVideoRecord({
            uploadStatus: RequestStatus.STATUS_LOADED
          })
        ]),
        videoKey: videoKey
      }
    });

    const store = mockStore(newMockGetState, {reduxSaga: true});
    const asyncDispatch = createAsyncDispatch(store);
    const sessionNonceTokenResponse = mockSessionNonceTokenResponse();
    const videoUploadResponse = mockVideoUploadResponse();
    const videoTranscodingProgressResponse = mockVideoTranscodingProgressResponse();
    const videoTranscodingResultsResponse = mockVideoTranscodingResultsResponse();
    const dummyFile = new File([''], 'dummy.mp4', {
      type: 'video/mp4',
      lastModified: '',
      size: 123
    });
    let mockActions;

    beforeEach(() => {
      store.clearActions();
      mockActions = store.getActions();
    });

    it('should request to upload a video and transcode it', (done) => {
      stubSessionNonceTokenRequest({
        companyId: '2',
        response: sessionNonceTokenResponse
      });
      stubVideoUploadRequest({
        companyId: '2',
        campaignId: '2',
        file: dummyFile,
        networks: ['FACEBOOK'],
        sessionNonceToken: sessionNonceTokenResponse,
        response: videoUploadResponse
      });
      stubVideoTranscodingProgressRequest({
        companyId: '2',
        campaignId: '2',
        response: videoTranscodingProgressResponse,
        videoKey: videoKey
      });
      stubVideoTranscodingResultsRequest({
        companyId: '2',
        campaignId: '2',
        response: videoTranscodingResultsResponse,
        videoKey: videoKey
      });

      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.sendUploadVideoRequest([dummyFile]))
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: MediaActionConstants.UPLOAD_VIDEO,
            payload: [dummyFile]
          });
          expect(mockActions[1]).toEqual({
            type: MediaActionConstants.REQUEST_UPLOAD_VIDEO,
            payload: dummyFile
          });
          expect(mockActions[2]).toEqual({
            type: MediaActionConstants.UPLOAD_VIDEO_SUCCEEDED,
            payload: videoUploadResponse
          });
          expect(mockActions[3]).toEqual({
            type: MediaActionConstants.REQUEST_TRANSCODE_VIDEO_PROGRESS,
            payload: undefined
          });
          expect(mockActions[4]).toEqual({
            type: MediaActionConstants.UPDATE_TRANSCODE_VIDEO_PROGRESS,
            payload: videoTranscodingProgressResponse
          });
          expect(mockActions[5]).toEqual({
            type: MediaActionConstants.TRANSCODE_VIDEO_SUCCEEDED,
            payload: undefined
          });
          expect(mockActions[6]).toEqual({
            type: MediaActionConstants.REQUEST_TRANSCODE_VIDEO_RESULTS,
            payload: undefined
          });
          expect(mockActions[7]).toEqual({
            type: MediaActionConstants.TRANSCODE_VIDEO_RESULTS_SUCCEEDED,
            payload: videoTranscodingResultsResponse
          });
          expect(mockActions.length).toEqual(8);
          done();
        });
    });
  });

  describe('upload media async', () => {
    let savedAction;

    const mockDispatch = (action) => {
      savedAction = action;
    };

    beforeEach(() => {
      savedAction = undefined;
    });

    it('should call upload video', () => {
      const mockGetState = mockState({});
      const videoFile = new File([''], 'dummy.mp4', {
        type: 'video/mp4',
        lastModified: '',
        size: 123
      });

      actions.sendUploadMediaRequest([videoFile])(mockDispatch, mockGetState);
      expect(savedAction).toEqual({
        type: MediaActionConstants.UPLOAD_VIDEO,
        payload: [videoFile]
      });
    });

    it('should call upload image', () => {
      const mockGetState = mockState({});
      const imageFile = new File([''], 'dummy.mp4', {
        type: 'image/png',
        lastModified: '',
        size: 123
      });

      actions.sendUploadMediaRequest([imageFile])(mockDispatch, mockGetState);
      expect(savedAction).toEqual({
        type: MediaActionConstants.UPLOAD_IMAGE,
        payload: [imageFile]
      });
    });
  });

});
