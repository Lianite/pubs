/* @flow */
import * as actions from 'actions/FacebookActions';
import { FacebookActionConstants } from 'constants/ActionConstants';
import {
  mockFetchLanguagesApiResponse,
  stubFetchLanguagesApiResponse,
  mockFetchLocationsApiResponse,
  stubFetchLocationsApiResponse
} from 'tests/api-responses/FacebookApiResponses';
import mockState from 'tests/test-utils/MockPublishingState';//import { fromJS, Map as ImmutableMap, Set as ImmutableSet } from 'immutable';
import { CredentialRecord } from 'records/AccountsRecord';
import {
  Map as ImmutableMap,
  List as ImmutableList
} from 'immutable';
import { FacebookState } from 'records/FacebookRecords';

import { AssigneeRecord } from 'records/AssigneeRecords';
import {
  LanguageState,
  LanguagesState,
  TargetingState,
  GenderState
} from 'records/TargetingRecords';
import { GENDER_TARGETING_TYPES } from 'constants/ApplicationConstants';

let mockGetState;
let actionQueue;
let currentAction;
const mockDispatch  = (action) => {
  actionQueue.push(action);
};

describe('Facebook Actions', () => {

  beforeEach(() => {
    mockGetState = mockState({
      accounts: {
        voices: [
          {
            data: {
              id: 1,
              name: 'Test 1',
              credentials: [
                {
                  data: {
                    id: 1,
                    name: 'Page: Facebook Test',
                    service: 'FACEBOOK',
                    accountType: 'PAGE',
                    socialNetworkId: '1234',
                    uniqueId: '12345',
                    authenticated: true,
                    targetingSupported: true,
                    organicVisibilitySupported: true
                  }
                },
                {
                  data: {
                    id: 2,
                    name: 'Do not target',
                    service: 'FACEBOOK',
                    accountType: 'NOT A PAGE?',
                    socialNetworkId: '1234',
                    uniqueId: '12345',
                    authenticated: false,
                    targetingSupported: false,
                    organicVisibilitySupported: true
                  }
                },
                {
                  data: {
                    id: 3,
                    name: 'Page: Target',
                    service: 'FACEBOOK',
                    accountType: 'PAGE',
                    socialNetworkId: '1234',
                    uniqueId: '12345',
                    authenticated: true,
                    targetingSupported: true,
                    organicVisibilitySupported: true
                  }
                }
              ]
            }
          }
        ],
        checkedCredentials: ImmutableMap({
          '1': new CredentialRecord({
            id: 1,
            name: 'Page: Facebook Test',
            socialNetwork: 'FACEBOOK',
            accountType: 'PAGE',
            authenticated: true,
            targetingSupported: true,
            organicVisibilitySupported: true,
            uniqueId: 1
          })
        })
      },
      facebook: new FacebookState({
        targeting: new TargetingState({
          language: new LanguagesState({
            selectedLanguages: ImmutableList([
              new LanguageState({
                value: '1',
                description: 'English'
              })
            ])
          }),
          gender: new GenderState({
            value: '0',
            description: 'All'
          })
        })
      }),
      darkPostStatus: false,
      assignee: {
        currentAssignee: new AssigneeRecord({
          lastName: 'McTest',
          id: '1',
          firstName: 'Test',
          voices: [{data:{id:1}}, {data:{id:2}}],
          conflictingVoices: []
        })
      }
    });
    actionQueue = [];
    currentAction = undefined;
  });

  describe('selectGenderTargeting', () => {
    it('should dispatch the correct action when a new gender has been selected', () => {
      actions.selectGenderTargeting('Men')(mockDispatch, mockGetState);

      expect(actionQueue[0].type).toBe(FacebookActionConstants.SET_GENDER_TARGETING);
      expect(actionQueue[0].payload).toBe(GENDER_TARGETING_TYPES['Men']);
    });

    it('should dispatch a clear when All is selected for gender', () => {
      actions.selectGenderTargeting('All')(mockDispatch, mockGetState);

      expect(actionQueue[0].type).toBe(FacebookActionConstants.CLEAR_GENDER_TARGETING);
      expect(actionQueue[0].payload).toBe(undefined);
    });
  });

  describe('fetchFacebookTargetingLanguages', () => {

    const response = mockFetchLanguagesApiResponse();

    it('should dispatch the correct action when facebook targeting languages have been loaded', (done) => {
      stubFetchLanguagesApiResponse({credentialUniqueIds: ['1'], companyId: '1', campaignId: '1', query: 'a', response });

      expect(actionQueue[0]).toBeUndefined();
      actions.fetchFacebookTargetingLanguages('a')(mockDispatch, mockGetState)
      .then(() => {
        currentAction = actionQueue.shift();
        expect(currentAction.type).toEqual(FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LANGUAGES_COMPLETED);
        done();
      });
    });

    it('should not dispatch any action when facebook targeting is disabled', (done) => {
      mockGetState = mockState({});
      stubFetchLanguagesApiResponse({credentialUniqueIds: ['1'], companyId: '1', campaignId: '1', query: 'a', response });

      expect(actionQueue[0]).toBeUndefined();
      actions.fetchFacebookTargetingLanguages('a')(mockDispatch, mockGetState)
      .then(() => {
        expect(actionQueue.length).toEqual(0);
        done();
      });
    });
  });

  describe('fetchFacebookTargetingLocations', () => {

    const response = mockFetchLocationsApiResponse();

    it('should dispatch the correct action when facebook targeting languages have been loaded', (done) => {
      stubFetchLocationsApiResponse({
        credentialUniqueIds: ['1'],
        companyId: '1',
        campaignId: '1',
        subType: 'COUNTRY',
        query: 'e',
        response
      });

      expect(actionQueue[0]).toBeUndefined();
      actions.fetchFacebookTargetingLocations('e')(mockDispatch, mockGetState)
      .then(() => {
        currentAction = actionQueue.shift();
        expect(currentAction.type).toEqual(FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LOCATIONS_COMPLETED);
        done();
      });
    });

    it('should dispatch the correct action when facebook locations are fetched without a query', () => {
      actions.fetchFacebookTargetingLocations('')(mockDispatch, mockGetState);

      currentAction = actionQueue.shift();
      expect(currentAction.type).toEqual(FacebookActionConstants.FETCH_FACEBOOK_TARGETING_LOCATIONS_COMPLETED);
      expect(currentAction.payload).toEqual([]);
    });

    it('should not dispatch any action when facebook targeting is disabled', (done) => {
      mockGetState = mockState({});
      stubFetchLocationsApiResponse({
        credentialUniqueIds: ['1'],
        companyId: '1',
        campaignId: '1',
        subType: 'COUNTRY',
        query: 'e',
        response
      });

      expect(actionQueue[0]).toBeUndefined();
      actions.fetchFacebookTargetingLocations('e')(mockDispatch, mockGetState)
      .then(() => {
        expect(actionQueue.length).toEqual(0);
        done();
      });
    });
  });
});
