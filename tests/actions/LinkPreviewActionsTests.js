import * as actions from 'actions/LinkPreviewActions';
import { LinkPreviewActionConstants } from 'constants/ActionConstants';
import {
  mockGetLinkPreviewResponse,
  stubGetLinkPreviewRequest
} from 'tests/api-responses/MessageApiResponses';
import mockState from 'tests/test-utils/MockPublishingState';
import { OrderedSet as ImmutableOrderedSet } from 'immutable';
import { extractUrlFromSourceMedia } from 'utils/LinkUtils';
import { RequestStatus } from 'constants/ApplicationConstants';
import { convertToMediaRecord } from 'adapters/LinkPreviewAdapter';
import mockStore from 'tests/test-utils/MockPublishingStore';
import { createAsyncDispatch } from 'tests/test-utils/PublishingReduxTestUtils';
import { mockImageUploadResponse,
  stubImageUploadRequest } from 'tests/api-responses/MediaApiResponses';

describe('LinkPreview Actions', () => {
  let savedAction;

  let mockDispatch  = (action) => {
    savedAction = action;
  };

  beforeEach(() => {
    savedAction = undefined;
  });

  describe('updateLinkPreview', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.updateLinkPreview(payload)).toEqual({
        type: LinkPreviewActionConstants.UPDATE_LINK_PREVIEW,
        payload: payload
      });
    });
  });

  describe('removeLinkPreview', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.removeLinkPreview(payload)).toEqual({
        type: LinkPreviewActionConstants.REMOVE_LINK_PREVIEW,
        payload: payload
      });
    });
  });

  describe('editLinkPreview', () => {
    it('should create expected structure', () => {
      const payload = {};
      expect(actions.editLinkPreview(payload)).toEqual({
        type: LinkPreviewActionConstants.EDIT_LINK_PREVIEW,
        payload: payload
      });
    });
  });

  describe('cycle selected thumbnail url', () => {
    let savedAction;
    const companyId = '1';
    const campaignId = '1';
    const mockGetState = mockState({
      router: {
        params: {companyId, campaignId}
      },
      linkPreview: {
        caption: 'worldofwarcraft.com',
        description:'World of Warcraft',
        linkUrl:'www.worldofwarcraft.com',
        previewLinkValidationNeeded:false,
        thumbnailUrl:'http://bnetcmsus-a.akamaihd.net/cms/template_resource/fh/FHSCSCG9CXOC1462229977849.png',
        title:'World of Warcraft',
        customUploadedMedia: ImmutableOrderedSet(),
        sourceMedia: ImmutableOrderedSet([
          {
            id: -1,
            src:'http://bnetcmsus-a.akamaihd.net/cms/template_resource/fh/FHSCSCG9CXOC1462229977849.png'
          },
          {
            id: -1,
            src:'https://bnetus-a.akamaihd.net/api/legal/static/images/legal/ratings/esrb/en/t.1qiE8.png'
          },
          {
            id: -1,
            src:'http://dummy.dummy.com'
          }
        ])
      }
    });

    beforeEach(() => {
      savedAction = undefined;
    });

    const mockDispatch = (action) => {
      savedAction = action;
    };

    it('should change the thumbnail url when cycleLinkPreviewImageCarousel is fired', () => {
      actions.cycleLinkPreviewImageCarousel('FORWARD')(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: 'UPDATE_IMAGE_THUMBNAIL',
        payload: 'https://bnetus-a.akamaihd.net/api/legal/static/images/legal/ratings/esrb/en/t.1qiE8.png'
      });
    });

    it('should change the thumbnail url when cycleLinkPreviewImageCarousel is fired', () => {
      actions.cycleLinkPreviewImageCarousel('BACKWARD')(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: 'UPDATE_IMAGE_THUMBNAIL',
        payload: 'http://dummy.dummy.com'
      });
    });
  });

  describe('sync link preview', () => {
    it('should not update link preview if it was removed', () => {
      const mockGetState = mockState({
        linkPreview: {
          removed: true
        }
      });

      actions.syncLinkPreview('anything.com')(mockDispatch, mockGetState);
      expect(savedAction).toBeFalsy();
    });

    it('should update link preview ', () => {
      const mockGetState = mockState({
        linkPreview: {
          linkUrl: 'www.facebook.com'
        }
      });
      actions.syncLinkPreview('www.google.com')(mockDispatch, mockGetState);

      expect(savedAction.type).toEqual('ADD_LINK_PREVIEW');
      expect(savedAction.payload).toEqual('www.google.com');
    });
  });

  describe('upload image async', () => {
    // define a mock initial state
    let newMockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      },
      images: {
        imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
      }
    });

    // setup a mock store that records actions
    let store = mockStore(newMockGetState, {
      reduxSaga: true
    });

    const asyncDispatch = createAsyncDispatch(store);
    const response = mockImageUploadResponse();
    //NOTE: this doesn't work in all browsers....it's fine in Chrome, which
    //      is what we're using for karma right now. Not sure what we should
    //      do if we want to run these tests on other browsers.
    const dummyFile = new File([''], 'dummy.png', {type: 'image/png', lastModified: ''});
    let mockActions;

    beforeEach(() => {
      store.clearActions();
      mockActions = store.getActions();
    });

    it('should request to upload an image, and on success give a successful reply', (done) => {
      stubImageUploadRequest({ companyId: '2', campaignId: '2', response });

      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.uploadCustomThumb(dummyFile))
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB,
            payload: dummyFile
          });
          expect(mockActions[1]).toEqual({
            type: LinkPreviewActionConstants.REQUEST_UPLOAD_CUSTOM_THUMB,
            payload: undefined
          });
          expect(mockActions[2]).toEqual({
            type: LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_SUCCEEDED,
            payload: undefined
          });

          done();
        });
    });

    it('should request to upload an image, and on success give a successful reply', (done) => {
      stubImageUploadRequest({ companyId: '2', campaignId: '2', status: 400 });

      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.uploadCustomThumb(dummyFile))
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB,
            payload: dummyFile
          });
          expect(mockActions[1]).toEqual({
            type: LinkPreviewActionConstants.REQUEST_UPLOAD_CUSTOM_THUMB,
            payload: undefined
          });
          expect(mockActions[2]).toEqual({
            type: LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_FAILED,
            payload: 'Unsuccessful HTTP response'
          });

          done();
        });
    });

    it('should request to upload an image, and on cancel give a cancel reply', () => {
      stubImageUploadRequest({ companyId: '2', campaignId: '2', response });

      expect(mockActions[0]).toBeUndefined();
      store.dispatch(actions.uploadCustomThumb(dummyFile));
      store.dispatch(actions.cancelLinkPreviewCustomUpload());

      expect(mockActions[0]).toEqual({
        type: LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB,
        payload: dummyFile
      });
      expect(mockActions[1]).toEqual({
        type: LinkPreviewActionConstants.REQUEST_UPLOAD_CUSTOM_THUMB,
        payload: undefined
      });
      expect(mockActions[2]).toEqual({
        type: LinkPreviewActionConstants.UPLOAD_CUSTOM_THUMB_CANCELED,
        payload: undefined
      });
      expect(mockActions[3]).toBeUndefined();
    });
  });

  describe('add link preview', () => {
    // define a mock initial state
    let newMockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2'
        }
      },
      images: {
        imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
      }
    });

    // setup a mock store that records actions
    let store = mockStore(newMockGetState, {
      reduxSaga: true
    });

    const asyncDispatch = createAsyncDispatch(store);
    // const response = mockImageUploadResponse();
    let mockActions;

    beforeEach(() => {
      store.clearActions();
      mockActions = store.getActions();
    });

    it('should call remove link preview if an url is not provided', (done) => {
      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.removeLinkPreview())
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: LinkPreviewActionConstants.REMOVE_LINK_PREVIEW,
            payload: undefined
          });

          done();
        });
    });

    it('should request to upload an image, and on success give a successful reply', (done) => {
      const response = mockGetLinkPreviewResponse();
      const url = 'www.facebook.com';
      stubGetLinkPreviewRequest({
        companyId: '2',
        campaignId: '2',
        url,
        response
      });

      expect(mockActions[0]).toBeUndefined();
      asyncDispatch(actions.addLinkPreview(url))
        .then(() => {
          expect(mockActions[0]).toEqual({
            type: LinkPreviewActionConstants.ADD_LINK_PREVIEW,
            payload: url
          });
          expect(mockActions[1]).toEqual({
            type: LinkPreviewActionConstants.SCRAPING_LINK_PREVIEW,
            payload: undefined
          });
          expect(mockActions[2]).toEqual({
            type: LinkPreviewActionConstants.LINK_SCRAPE_SUCCESSFUL,
            payload: undefined
          });
          expect(mockActions[3]).toEqual({
            type: LinkPreviewActionConstants.UPDATE_LINK_PREVIEW,
            payload: {
              ...response,
              linkUrl: url,
              thumbnailUrl: response.sourceMedia[0].src,
              customUploadedMedia: ImmutableOrderedSet(),
              currentlyUploadingMedia: false,
              sourceMedia: ImmutableOrderedSet(convertToMediaRecord(extractUrlFromSourceMedia(response.sourceMedia)))
            }
          });

          done();
        });
    });

    it('should request to upload an image, and on success give a successful reply', (done) => {
      const url = 'www.facebook.com';

      expect(mockActions[0]).toBeUndefined();

      let newMockGetState = mockState({
        router: {
          params: {
            companyId: '2',
            campaignId: '2'
          }
        },
        images: {
          imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED
        }
      });

      // setup a mock store that records actions
      let store = mockStore(newMockGetState, {
        reduxSaga: true
      });

      const asyncDispatch = createAsyncDispatch(store);

      asyncDispatch(actions.addLinkPreview(url))
        .then(() => {
          expect(mockActions[0]).toEqual(undefined);

          done();
        });
    });
  });
});
