import * as actions from 'actions/UIStateActions';
import { UIStateActionConstants,
  MessageActionConstants } from 'constants/ActionConstants';
import { SidebarPages } from 'constants/UIStateConstants';
import mockState from 'tests/test-utils/MockPublishingState';

describe('UI State Actions', () => {
  describe('toggleSidebar Helper', () => {
    let savedAction;

    let mockDispatch  = (action) => {
      savedAction = action;
    };

    beforeEach(() => {
      savedAction = undefined;
    });


    it('should open if closed', () => {
      actions.toggleSidebar(false /*sidebar closed*/, '', SidebarPages.BASICS)(mockDispatch);
      expect(savedAction).toEqual({
        type: UIStateActionConstants.OPEN_SIDEBAR,
        payload: SidebarPages.BASICS
      });
    });

    it('should close if open and same page is active', () => {
      actions.toggleSidebar(true /*sidebar open*/, SidebarPages.PLANNER, SidebarPages.PLANNER)(mockDispatch);
      expect(savedAction).toEqual({
        type: UIStateActionConstants.CLOSE_SIDEBAR,
        payload: undefined
      });
    });

    it('should switch pages if open and new page clicked', () => {
      actions.toggleSidebar(true /*sidebar open*/, SidebarPages.PLANNER, SidebarPages.NOTES)(mockDispatch);
      expect(savedAction).toEqual({
        type: UIStateActionConstants.OPEN_SIDEBAR,
        payload: SidebarPages.NOTES
      });
    });

  });

  describe('closeSidebarIfOpen Helper', () => {
    let savedAction;

    let mockDispatch  = (action) => {
      savedAction = action;
    };

    beforeEach(() => {
      savedAction = undefined;
    });


    it('should close if open', () => {
      actions.closeSidebarIfOpen(true /*sidebar open*/)(mockDispatch);
      expect(savedAction).toEqual({
        type: UIStateActionConstants.CLOSE_SIDEBAR,
        payload: undefined
      });
    });

    it('should not close if closed', () => {
      actions.closeSidebarIfOpen(false /*sidebar closed*/)(mockDispatch);
      expect(savedAction).toEqual(undefined);
    });

  });

  describe('voiceDropdownToggle Action', () => {
    it('should create the proper action to toggle the voice dropdown', () => {
      expect(actions.voiceDropdownToggle()).toEqual({
        type: UIStateActionConstants.TOGGLE_VOICE_SELECTOR,
        payload: undefined
      });
    });
  });

  describe('toggleVoiceSelectorIfNecessary Action', () => {
    let savedAction;
    const mockDispatch = (action:any) => {
      savedAction = action;
    };

    beforeEach(() => {
      savedAction = undefined;
    });

    it('should toggle if focus and currently closed', () => {
      const mockGetState = mockState({
        uiState:{
          voiceSelectorOpen: false
        }
      });
      actions.toggleVoiceSelectorIfNecessary('focus')(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: UIStateActionConstants.TOGGLE_VOICE_SELECTOR,
        payload: 'focus'
      });
    });

    it('should NOT toggle if focus and currently open', () => {
      const mockGetState = mockState({
        uiState:{
          voiceSelectorOpen: true
        }
      });
      actions.toggleVoiceSelectorIfNecessary('focus')(mockDispatch, mockGetState);

      expect(savedAction).toBe(undefined);
    });

    it('should toggle if blur and currently open', () => {
      const mockGetState = mockState({
        uiState:{
          voiceSelectorOpen: true
        }
      });
      actions.toggleVoiceSelectorIfNecessary('blur')(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: UIStateActionConstants.TOGGLE_VOICE_SELECTOR,
        payload: 'blur'
      });
    });

    it('should NOT toggle if blur and currently close', () => {
      const mockGetState = mockState({
        uiState:{
          voiceSelectorOpen: false
        }
      });
      actions.toggleVoiceSelectorIfNecessary('blur')(mockDispatch, mockGetState);

      expect(savedAction).toBe(undefined);
    });

    it('should pass through toggle if no payload', () => {
      const mockGetState = mockState({
        uiState:{
          voiceSelectorOpen: false
        }
      });
      actions.toggleVoiceSelectorIfNecessary()(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: UIStateActionConstants.TOGGLE_VOICE_SELECTOR,
        payload: undefined
      });
    });
  });

  describe('toggleVoiceSelectorIfNecessary Action', () => {
    let savedAction;
    const mockDispatch = (action:any) => {
      savedAction = action;
    };

    beforeEach(() => {
      savedAction = undefined;
    });

    it('should toggle if dragging and currently not dragging', () => {
      const mockGetState = mockState({
        uiState:{
          currentlyDraggingMedia: false
        }
      });
      actions.updateDragStateIfNecessary(true)(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: UIStateActionConstants.DRAG_STATE_CHANGED,
        payload: true
      });
    });

    it('should toggle if not dragging and currently dragging', () => {
      const mockGetState = mockState({
        uiState:{
          currentlyDraggingMedia: true
        }
      });
      actions.updateDragStateIfNecessary(false)(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type: UIStateActionConstants.DRAG_STATE_CHANGED,
        payload: false
      });
    });

    it('should NOT toggle if dragging and currently dragging', () => {
      const mockGetState = mockState({
        uiState:{
          currentlyDraggingMedia: true
        }
      });
      actions.updateDragStateIfNecessary(true)(mockDispatch, mockGetState);

      expect(savedAction).toEqual(undefined);
    });

    it('should toggle if preview is visible', () => {
      const mockGetState = mockState({
        uiState:{
          previewVisible: false
        }
      });
      actions.updatePreviewVisibility(true)(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type:UIStateActionConstants.PREVIEW_VISIBILITY_CHANGED,
        payload: true
      });
    });

    it('should toggle if preview is invisible', () => {
      const mockGetState = mockState({
        uiState:{
          previewVisible: true
        }
      });
      actions.updatePreviewVisibility(false)(mockDispatch, mockGetState);

      expect(savedAction).toEqual({
        type:UIStateActionConstants.PREVIEW_VISIBILITY_CHANGED,
        payload: false
      });
    });
  });

  describe('cancelCloseButtonClicked', () => {
    let savedActions = [];
    const mockDispatch = (action:any) => {
      savedActions.push(action);
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should dispatch a clearMessage and clearMedia call after check lock and unlock calls are made', (done) => {
      const mockGetState = mockState({
        message: {
          eventId: 1
        },
        router: {
          params: {
            companyId: '1',
            campaignId: '1',
            copyId: '1'
          }
        }
      });
      const checkLock = actions.cancelCloseButtonClicked()(mockDispatch, mockGetState);

      checkLock.finally(res => {
        expect(savedActions[0]).toEqual({
          type:MessageActionConstants.CLEAR_MESSAGE,
          payload: false
        });
      });
      done();
    });
  });
});
