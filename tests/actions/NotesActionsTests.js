import * as actions from 'actions/NotesActions';
import { NotesActionConstants } from 'constants/ActionConstants';
import mockState from 'tests/test-utils/MockPublishingState';
import { HTTPSTATUS } from 'constants/ApplicationConstants';
import { stubCreateNoteRequest, mockCreateNoteResponse } from 'tests/api-responses/NotesApiResponses';
import { ADD_NOTIFICATION } from '@spredfast/react-lib/lib/growler-notification/redux/actions';
import { NOTE_SAVE_FAIL_NOTIFICATION_EXPIRE } from 'constants/NotesMenuConstants';
import { shallow } from 'enzyme';

import type { Note } from 'adapters/types';

const note: Note = {
  noteText: 'test text',
  authorFirstName: 'fake',
  authorLastName: 'person',
  authorAvatarThumbnail: 'http://image.com',
  noteCreationTimestamp: 1234
};

describe('Notes Actions', () => {

  describe('read notes completed', () => {
    it('should create expected structure', () => {
      expect(actions.readNotesCompleted()).toEqual({
        type: NotesActionConstants.READ_NOTES_COMPLETED,
        payload: undefined
      });
    });
  });

  describe('add note', () => {
    it('should create expected structure', () => {
      expect(actions.addNote(note)).toEqual({
        type: NotesActionConstants.ADD_NOTE,
        payload: note
      });
    });
  });

  describe('current note text changed', () => {
    it('should create the expected structure', () => {
      expect(actions.currentNoteTextChanged('test')).toEqual({
        type: NotesActionConstants.CURRENT_NOTE_TEXT_CHANGED,
        payload: 'test'
      });
    });
  });

  describe('create note for saved message', () => {
    let actionQueue = [];
    let mockDispatch  = (action) => {
      actionQueue.push(action);
    };

    let mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2',
          eventId: '2'
        }
      }
    });
    const companyId = 2;
    const campaignId = 2;
    const messageId = 2;

    afterEach(() => {
      actionQueue = [];
    });
    it('should send a request to save the note', (done) => {
      stubCreateNoteRequest({
        companyId,
        campaignId,
        messageId,
        note,
        response: mockCreateNoteResponse()
      });
      actions.createNoteForSavedMessage(note.noteText)(mockDispatch, mockGetState)
      .then(res => {
        expect(actionQueue.length).toEqual(3);
        expect(actionQueue[0]).toEqual({
          type: NotesActionConstants.SAVE_NOTE_REQUEST_STARTED,
          payload: undefined
        });
        expect(actionQueue[1]).toEqual({
          type: NotesActionConstants.ADD_NOTE,
          payload: note
        });
        expect(actionQueue[2]).toEqual({
          type: NotesActionConstants.SAVE_NOTE_REQUEST_COMPLETED,
          payload: undefined
        });
        done();
      });
    });
    it('should error out and dispatch a notification with the correct message', (done) => {
      stubCreateNoteRequest({
        companyId,
        campaignId,
        messageId,
        note,
        status: HTTPSTATUS.INTERNAL_SERVER_ERROR,
        response: mockCreateNoteResponse()
      });
      actions.createNoteForSavedMessage(note.noteText)(mockDispatch, mockGetState)
      .then(res => {
        expect(actionQueue.length).toEqual(3);
        expect(actionQueue[0]).toEqual({
          type: NotesActionConstants.SAVE_NOTE_REQUEST_STARTED,
          payload: undefined
        });
        expect(actionQueue[1].type).toEqual(ADD_NOTIFICATION);
        expect(actionQueue[1].payload).toEqual(jasmine.objectContaining({
          sfType: 'warning',
          timed: true,
          timeOverride: NOTE_SAVE_FAIL_NOTIFICATION_EXPIRE
        }));
        expect(shallow(actionQueue[1].payload.children).text()).toEqual('Note cannot be savedPlease try again shortly');
        expect(actionQueue[2]).toEqual({
          type: NotesActionConstants.SAVE_NOTE_REQUEST_COMPLETED,
          payload: undefined
        });
        done();
      });
    });
  });
});
