/* @flow */
import * as mentionsActions from 'actions/MentionsActions';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { EditorState, ContentState } from 'draft-js';
import mockState from 'tests/test-utils/MockPublishingState';
// import { UrlPropertiesRecord } from 'records/LinksRecords';
// import { createMessageStateFromPayload } from 'adapters/MessageAdapter';
import { MentionsActionConstants /*EnvironmentActionConstants, LinkPreviewActionConstants*/ } from 'constants/ActionConstants';
import { stubGetMentionsQueryRequest, mockMentionsQueryResponse } from 'tests/api-responses/MentionsApiResponses';
import { List as ImmutableList, Map as ImmutableMap, fromJS } from 'immutable';
//import _ from 'lodash';
import { TimezoneRecord, CompanyRecord } from 'reducers/EnvironmentReducer';
import {   RequestStatus,
  HTTPSTATUS,
  DefaultNetwork } from 'constants/ApplicationConstants';

describe('Mentions Actions', () => {
  describe('openMentionsQuery Action', () => {
    let savedActions = [];
    const mockDispatch = (action:any) => {
      savedActions.push(action);
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should create an action to open the query menu if it is closed', () => {
      const mockGetState = mockState({
        mentions:{
          isMentionsQueryOpen: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: false,
            [SupportedNetworks.TWITTER]: true
          })
        }
      });

      mentionsActions.openMentionsQuery(SupportedNetworks.FACEBOOK)(mockDispatch, mockGetState);

      expect(savedActions[0].type).toBe(MentionsActionConstants.OPEN_MENTIONS_QUERY);
      expect(savedActions[0].payload).toBe(SupportedNetworks.FACEBOOK);
    });

    it('should NOT create an action to open the query menu if it is open', () => {
      const mockGetState = mockState({
        mentions:{
          isMentionsQueryOpen: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: false,
            [SupportedNetworks.TWITTER]: true
          })
        }
      });

      mentionsActions.openMentionsQuery(SupportedNetworks.TWITTER)(mockDispatch, mockGetState);

      expect(savedActions[0]).toBeUndefined();
    });
  });

  describe('closeMentionsQuery Action', () => {
    let savedActions = [];
    const mockDispatch = (action:any) => {
      savedActions.push(action);
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should create an action to close the query menu if it is opened', () => {
      const mockGetState = mockState({
        mentions:{
          isMentionsQueryOpen: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: true,
            [SupportedNetworks.TWITTER]: false
          })
        }
      });

      mentionsActions.closeMentionsQuery(SupportedNetworks.FACEBOOK)(mockDispatch, mockGetState);

      expect(savedActions[0].type).toBe(MentionsActionConstants.CLOSE_MENTIONS_QUERY);
      expect(savedActions[0].payload).toBe(SupportedNetworks.FACEBOOK);
    });

    it('should NOT create an action to close the query menu if it is closed', () => {
      const mockGetState = mockState({
        mentions:{
          isMentionsQueryOpen: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: true,
            [SupportedNetworks.TWITTER]: false
          })
        }
      });

      mentionsActions.closeMentionsQuery(SupportedNetworks.TWITTER)(mockDispatch, mockGetState);

      expect(savedActions[0]).toBeUndefined();
    });
  });

  describe('updateMentions Action', () => {
    let savedActions = [];
    const mockDispatch = (action:any) => {
      savedActions.push(action);
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should not return an action when mentions have not changed', () => {
      const mockGetState = mockState({
        mentions:{
          isMentionsQueryOpen: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: false,
            [SupportedNetworks.TWITTER]: false
          }),
          listOfMentions: ImmutableMap({
            'FACEBOOK': ImmutableList([])
          }),
          mentionsQueryList: ImmutableList([]),
          loadMentionsQueryStatus: ''
        }
      });
      const editorStateMap = ImmutableMap({
        'FACEBOOK': EditorState.createEmpty()
      });

      mentionsActions.updateMentions(editorStateMap)(mockDispatch, mockGetState);

      expect(savedActions[0]).toBeUndefined();
    });

    it('should return an action when mentions have changed', () => {
      const mockGetState = mockState({
        mentions:{
          isMentionsQueryOpen: ImmutableMap({
            [SupportedNetworks.FACEBOOK]: false,
            [SupportedNetworks.TWITTER]: false
          }),
          listOfMentions: ImmutableMap({}),
          mentionsQueryList: ImmutableList([]),
          loadMentionsQueryStatus: ''
        }
      });
      const editorStateMap = ImmutableMap({
        'FACEBOOK': EditorState.createEmpty()
      });

      mentionsActions.updateMentions(editorStateMap)(mockDispatch, mockGetState);

      expect(savedActions[0].type).toBe(MentionsActionConstants.UPDATE_MENTIONS);
    });
  });

  describe('queryForMentions Action', () => {
    let savedActions = [];

    const mockDispatch = (action:any) => {
      savedActions.push(action);
    };

    beforeEach(() => {
      savedActions = [];
    });

    const mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2',
          eventId: '2'
        }
      },
      environment: {
        userId: 1,
        userTimezone: new TimezoneRecord(),
        capabilities: fromJS({
          FACEBOOK: {
            NEW_MESSAGE: {
              NOTE: {
                linkPreview: {
                  supported: true
                }
              }
            }
          },
          TWITTER: {
            NEW_MESSAGE: {
              NOTE: {
                linkPreview: {
                  supported: false
                }
              }
            }
          }
        }),
        company: new CompanyRecord({
          companyMetadata: {
            shortenLinksByDefault: true
          }
        })
      },
      message: {
        messageEditorStateMap: ImmutableMap({
          [DefaultNetwork]: EditorState.createWithContent(ContentState.createFromText('this is a test www.google.com of the link detection system www.reddit.com'))
        })
      },
      images: {
        imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
        imageUploadError: '',
        imageId: null,
        imageFullUrl: '',
        imageThumbUrl: ''
      },
      mentions:{
        isMentionsQueryOpen: ImmutableMap({
          [SupportedNetworks.FACEBOOK]: true,
          [SupportedNetworks.TWITTER]: false
        }),
        listOfMentions: ImmutableMap({
          'FACEBOOK': ImmutableList([])
        }),
        mentionsQueryList: ImmutableList([]),
        loadMentionsQueryStatus: ''
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const initiativeId = mockGetState().router.params.campaignId;
    const response = mockMentionsQueryResponse({companyId, initiativeId});

    it('should receive mentions from MentionsApi', (done) => {
      stubGetMentionsQueryRequest({companyId, initiativeId, service: SupportedNetworks.FACEBOOK, query: '@reddit', credentialUniqueId: '', date: 1000000, response});

      mentionsActions.sendOutQuery(SupportedNetworks.FACEBOOK, '@reddit', 'aaaaa-0-0', mockGetState(), 1000000)(mockDispatch, mockGetState)
      .then(res => {
        expect(savedActions[0].type).toBe(MentionsActionConstants.MENTIONS_RECEIVED);
        expect(savedActions[0].payload[0].profileId).toBe(response.data[0].profileId);
        expect(savedActions[0].payload[1].profileId).toBe(response.data[1].profileId);
        expect(savedActions[0].payload[2].profileId).toBe(response.data[2].profileId);
        expect(savedActions[0].payload[3].profileId).toBe(response.data[3].profileId);
        expect(savedActions[0].payload[4].profileId).toBe(response.data[4].profileId);

        done();
      });
    });

    it('should dispatch a failed action on a failed request', (done) => {
      stubGetMentionsQueryRequest({companyId, initiativeId, service: SupportedNetworks.FACEBOOK, query: '@reddit', credentialUniqueId: '', date: 1000000, status: HTTPSTATUS.INTERNAL_SERVER_ERROR});

      mentionsActions.sendOutQuery(SupportedNetworks.FACEBOOK, '@reddit', 'aaaaa-0-0', mockGetState(), 1000000, response)(mockDispatch, mockGetState)
      .then(res => {
        expect(savedActions[0].type).toBe(MentionsActionConstants.MENTIONS_DATA_ERROR);
        done();
      });
    });
  });
});
