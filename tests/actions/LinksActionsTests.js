/* @flow */
import _ from 'lodash';
import * as linksActions from 'actions/LinksActions';
import { EditorState, ContentState } from 'draft-js';
import mockState from 'tests/test-utils/MockPublishingState';
import {
  LinksActionConstants,
  LinkTagsActionConstants } from 'constants/ActionConstants';
import { RequestStatus, DefaultNetwork } from 'constants/ApplicationConstants';
import { fromJS, Map as ImmutableMap, Set as ImmutableSet, List as ImmutableList } from 'immutable';
import { TimezoneRecord, CompanyRecord } from 'reducers/EnvironmentReducer';
import { UrlPropertiesRecord } from 'records/LinksRecords';

const defaultCompany = new CompanyRecord({
  companyMetadata: {
    shortenLinksByDefault: true
  },
  enabledFeatures: ['link_tagging']
});

describe('Links Actions tests', () => {
  let actionQueue = [];

  const mockGetState = mockState({
    router: {
      params: {
        companyId: '2',
        campaignId: '2',
        eventId: '2'
      }
    },
    environment: {
      userId: 1,
      userTimezone: new TimezoneRecord(),
      capabilities: fromJS({
        FACEBOOK: {
          NEW_MESSAGE: {
            NOTE: {
              linkPreview: {
                supported: true
              }
            }
          }
        },
        TWITTER: {
          NEW_MESSAGE: {
            NOTE: {
              linkPreview: {
                supported: false
              }
            }
          }
        }
      }),
      company: defaultCompany
    },
    message: {
      messageEditorStateMap: ImmutableMap({
        [DefaultNetwork]: EditorState.createWithContent(ContentState.createFromText('this is a test www.google.com of the link detection system www.reddit.com'))
      })
    },
    images: {
      imageUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
      imageUploadError: '',
      imageId: null,
      imageFullUrl: '',
      imageThumbUrl: ''
    },
    linkPreview: {
      linkUrl: 'www.google.com'
    },
    links: {
      urlProperties: ImmutableMap({
        'www.test.com': new UrlPropertiesRecord({
          shortenOnPublish: true,
          addLinkTags: true,
          associatedNetworks: ImmutableSet()
        })
      })
    }
  });

  const mockDispatch = (action:any) => {
    if (_.isFunction(action)) {
      action(mockDispatch, mockGetState);
    } else {
      actionQueue.push(action);
    }
  };

  describe('updateUrlList action', () => {
    beforeEach(() => {
      actionQueue = [];
    });

    it('should correctly parse urls from all the message editors in the store', () => {
      linksActions.updateUrlList()(mockDispatch, mockGetState);
      expect(actionQueue[0].type).toEqual(LinksActionConstants.UPDATE_URL_PROPERTIES);
      expect(actionQueue[0].payload.toJS()).toEqual({
        'www.google.com': {
          associatedNetworks: [DefaultNetwork],
          shortenOnPublish: true,
          addLinkTags: true
        },
        'www.reddit.com': {
          associatedNetworks: [DefaultNetwork],
          shortenOnPublish: true,
          addLinkTags: true
        }
      });

      expect(actionQueue[1].type).toEqual(LinksActionConstants.UPDATE_FOUND_URLS);
      expect(actionQueue[1].payload.equals(ImmutableMap({[DefaultNetwork]: ImmutableList(['www.google.com', 'www.reddit.com'])}))).toBeTruthy();
    });

    it('should sync link tags urls', () => {
      linksActions.updateUrlList()(mockDispatch, mockGetState);
      expect(actionQueue[2]).toEqual({
        type: LinkTagsActionConstants.CREATE_LINK_TAG_START,
        payload: {
          link: 'www.google.com',
          network: DefaultNetwork
        }
      });
      expect(actionQueue[3]).toEqual({
        type: LinkTagsActionConstants.CREATE_LINK_TAG_START,
        payload: {
          link: 'www.reddit.com',
          network: DefaultNetwork
        }
      });
    });

    it('should correctly add new urls from the editors', () => {
      linksActions.updateUrlList()(mockDispatch, mockGetState);

      const newGetState = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [DefaultNetwork]: EditorState.createWithContent(ContentState.createFromText('this is a test www.google.com of the link detection system www.reddit.com www.newLink.com'))
          })
        },
        environment: {
          company: defaultCompany
        },
        links: {
          urlProperties: ImmutableMap({
            'www.google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            }),
            'www.reddit.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            })
          })
        }
      });
      actionQueue = [];
      linksActions.updateUrlList()(mockDispatch, newGetState);
      expect(actionQueue[0].type).toEqual(LinksActionConstants.UPDATE_URL_PROPERTIES);
      expect(actionQueue[1].type).toEqual(LinksActionConstants.UPDATE_FOUND_URLS);
      expect(actionQueue[1].payload.equals(ImmutableMap({[DefaultNetwork]: ImmutableList(['www.google.com', 'www.reddit.com', 'www.newLink.com'])}))).toBeTruthy();
    });

    it('should correctly add link preview to urls if not present', () => {
      const newGetState = mockState({
        message: {
          messageEditorStateMap: ImmutableMap({
            [DefaultNetwork]: EditorState.createEmpty()
          })
        },
        environment: {
          company: defaultCompany
        },
        links: {
          urlProperties: ImmutableMap({
            'www.google.com': new UrlPropertiesRecord({
              shortenOnPublish: true,
              addLinkTags: true,
              associatedNetworks: ImmutableSet([DefaultNetwork])
            })
          })
        },
        linkPreview: {
          linkUrl: 'www.google.com'
        }
      });
      linksActions.updateUrlList()(mockDispatch, newGetState);
      expect(actionQueue[0].type).toEqual(LinksActionConstants.UPDATE_URL_PROPERTIES);
      expect(actionQueue[0].payload.get('www.google.com')).toBeTruthy();
      expect(actionQueue[1].type).toEqual(LinksActionConstants.UPDATE_FOUND_URLS);
      expect(actionQueue[1].payload.equals(ImmutableMap({[DefaultNetwork]: ImmutableList()}))).toBeTruthy();
    });
  });

  describe('updateSingleUrlProperty action', () => {
    beforeEach(() => {
      actionQueue = [];
    });

    it('should update the url property successfully', (done) => {
      linksActions.updateSingleUrlProperty('www.test.com', 'shortenOnPublish', false)(mockDispatch, mockGetState);

      expect(actionQueue[0].type).toEqual(LinksActionConstants.UPDATE_URL_PROPERTIES);

      expect(actionQueue[0].payload.get('www.test.com').get('shortenOnPublish')).toBe(false);

      done();
    });
  });

  describe('Calculating Link Preview Helper', () => {
    it('Should return the link if we went from no links to one without existing url', () => {
      const oldUrls = ImmutableList();
      const newUrls = ImmutableList(['www.google.com']);
      const curr = '';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.google.com');
    });

    it('Should return the existing link if we went from no links to one with existing url', () => {
      const oldUrls = ImmutableList();
      const newUrls = ImmutableList(['www.google.com']);
      const curr = 'www.abc.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.abc.com');
    });

    it('Should return the existing link if we added a link', () => {
      const oldUrls = ImmutableList(['www.google.com']);
      const newUrls = ImmutableList(['www.google.com', 'www.abc.com']);
      const curr = 'www.cnn.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.cnn.com');
    });

    it('Should return the existing link if we removed a link', () => {
      const oldUrls = ImmutableList(['www.google.com', 'www.abc.com']);
      const newUrls = ImmutableList(['www.google.com']);
      const curr = 'www.cnn.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.cnn.com');
    });

    it('Should return the existing link if we changed multiple', () => {
      const oldUrls = ImmutableList(['www.google.com', 'www.abc.com']);
      const newUrls = ImmutableList(['a.com', 'b.com']);
      const curr = 'www.google.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.google.com');
    });

    it('Should return the existing link if current is not in the old list', () => {
      const oldUrls = ImmutableList(['fun.golf', 'www.abc.com']);
      const newUrls = ImmutableList(['fun.golf.com', 'www.abc.com']);
      const curr = 'www.notthere.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.notthere.com');
    });

    it('Should return the existing link if change is not a substring', () => {
      const oldUrls = ImmutableList(['fun.golf, www.abc.com']);
      const newUrls = ImmutableList(['totallynew.com', 'www.abc.com']);
      const curr = 'fun.golf';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('fun.golf');
    });

    it('Should return the new link if it was the only one that changed', () => {
      const oldUrls = ImmutableList(['fun.golf', 'www.abc.com']);
      const newUrls = ImmutableList(['fun.golf.com', 'www.abc.com']);
      const curr = 'fun.golf';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('fun.golf.com');
    });

    it('Should return the new link if it was the only one that changed (not first index)', () => {
      const oldUrls = ImmutableList(['fun.golf', 'www.abc.com']);
      const newUrls = ImmutableList(['fun.golf', 'www.abc.com/fun']);
      const curr = 'www.abc.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.abc.com/fun');
    });

    it('Should return the new link if any of multiple changed', () => {
      const oldUrls = ImmutableList(['fun.golf', 'google.com', 'google.com', 'google.com']);
      const newUrls = ImmutableList(['fun.golf', 'google.com', 'www.google.com', 'google.com']);
      const curr = 'google.com';

      expect(linksActions.calculateLinkPreviewUrl(oldUrls, newUrls, curr)).toEqual('www.google.com');
    });
  });
});
