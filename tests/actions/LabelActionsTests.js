/* @flow */
import * as actions from 'actions/LabelActions';
import {
  LabelActionConstants,
  LinkTagsActionConstants } from 'constants/ActionConstants';
import {
  fromJS,
  Map as ImmutableMap,
  List as ImmutableList } from 'immutable';
import { mockGetLabels, stubGetLabels } from 'tests/api-responses/LabelsApiResponses';
import _ from 'lodash';
import mockState from 'tests/test-utils/MockPublishingState';
import { Label } from 'reducers/LabelsReducer';
import { defaultEnvironment as linkTagsDefaultEnvironment} from 'tests/actions/LinkTagsActionsTests';
import {
  SupportedNetworks,
  RequestStatus,
  LinkTags } from 'constants/ApplicationConstants';
import {
  LinkTagPropertiesRecord,
  TagVariable } from 'records/LinkTagsRecords';

const defaultMockState = {
  environment: linkTagsDefaultEnvironment,
  linkTags: {
    urls: fromJS({
      'foo.com': {
        [SupportedNetworks.TWITTER]: new LinkTagPropertiesRecord({
          id: 1,
          enabled: true,
          saveStatus: RequestStatus.STATUS_REQUESTED,
          tagVariables: ImmutableList([new TagVariable({
            id: 103,
            type: LinkTags.LABELS_TAG_VARIABLE_TYPE
          })])
        })
      }
    })
  }
};

describe('label Actions', () => {
  let label = new Label({id: '1', name: 'test'});
  let labelMap = ImmutableMap({label1: label, label2: label});
  let labelList = ImmutableList([label, label]);

  describe('addLabelToMessage', () => {
    it('should bring in a new label', () => {
      expect(actions.addLabelToMessage(label)).toEqual({
        type: LabelActionConstants.ADD_LABEL,
        payload: label
      });
    });
  });
  describe('labelsLoaded', () => {
    it('should give a map of all availableLabels', () => {
      expect(actions.labelsLoaded(labelMap)).toEqual({
        type: LabelActionConstants.LABELS_LOADED,
        payload: labelMap
      });
    });
  });
  describe('readContentLabelsCompleted', () => {
    it('should give a list of all labels already selected', () => {
      expect(actions.readContentLabelsCompleted(labelList)).toEqual({
        type: LabelActionConstants.LABELS_READ_COMPLETE,
        payload: labelList
      });
    });
  });
  describe('addNewLabelToAvailableLabels', () => {
    it('should give a new lable to add to then of availableLabels', () => {
      expect(actions.addNewLabelToAvailableLabels(label)).toEqual({
        type: LabelActionConstants.APPEND_NEW_LABEL,
        payload: label
      });
    });
  });

  describe('addLabel', () => {
    let savedActions;

    let mockGetState = mockState(defaultMockState);

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should call addLabelToMessage action after execute addLabel action', () => {
      actions.addLabel('foo')(mockDispatch, mockGetState);
      expect(savedActions.shift()).toDeepEqual({
        payload: {
          title: 'foo'
        },
        type: LabelActionConstants.ADD_LABEL
      });
    });

    it('should call updateLabelTagVariables action after execute addLabelToMessage action', () => {
      actions.addLabel('foo')(mockDispatch, mockGetState);
      savedActions.shift();
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: ImmutableList()
        }
      });
    });

  });

  describe('fetchLabelsAction', () => {
    let savedAction1;

    let mockDispatch  = (action) => {
      if (savedAction1) {
        savedAction1 = action;
      }
    };

    let mockGetState = mockState({
      router: {
        params: {
          companyId: '1',
          campaignId: '1'
        }
      }
    });

    const companyId = mockGetState().router.params.companyId;
    const campaignId = mockGetState().router.params.campaignId;
    const response = mockGetLabels({companyId, campaignId});

    beforeEach(() => {
      savedAction1 = undefined;
    });

    it('should send fetch requests and store as expected', (done) => {
      stubGetLabels({companyId, campaignId, response});

      actions.fetchLabelsAction()(mockDispatch, mockGetState).then( res => {
        expect(savedAction1).toEqual({
          type: LabelActionConstants.LABELS_LOADED,
          payload: {
            companyId: 1,
            contentLabelScope: 'GLOBAL',
            createdDate: {
              date: 1479931322588,
              text: '2016/11/23 02:02:02 PM',
              timezone: {
                name: 'America/Chicago',
                abbreviation: 'CST',
                offset: '-0600'
              }
            },
            id: 42,
            priority: false,
            status: 'ACTIVE',
            title: 'asdfasdf'
          }
        });
      });
      done();
    });
  });

  describe('removeLabelFromMessage', () => {
    let savedActions;

    let mockGetState = mockState(defaultMockState);

    let mockDispatch = (action:any) => {
      if (_.isFunction(action)) {
        action(mockDispatch, mockGetState);
      } else {
        savedActions.push(action);
      }
    };

    beforeEach(() => {
      savedActions = [];
    });

    it('should call the action removeLabel', () => {
      actions.removeLabelFromMessage('1')(mockDispatch, mockGetState);
      expect(savedActions.shift()).toDeepEqual({
        payload: '1',
        type: LabelActionConstants.REMOVE_LABEL
      });
    });

    it('should call updateLabelTagVariables action after execute removeLabel action', () => {
      actions.addLabel('foo')(mockDispatch, mockGetState);
      savedActions.shift();
      expect(savedActions.shift()).toDeepEqual({
        type: LinkTagsActionConstants.UPDATE_LINK_TAG_VARIABLE,
        payload: {
          tagVariableId: 103,
          link: 'foo.com',
          network: SupportedNetworks.TWITTER,
          tagVariableValue: ImmutableList()
        }
      });
    });
  });
});
