/* @flow */
import * as accountsActions from 'actions/AccountsActions';
import { getVoices } from 'apis/AccountsApi';
import mockState from 'tests/test-utils/MockPublishingState';
import { AssigneeRecord } from 'records/AssigneeRecords';
import { SupportedNetworks,
  VoiceFilteringConstants,
  DefaultNetwork,
  RequestStatus } from 'constants/ApplicationConstants';
import { FacebookState } from 'records/FacebookRecords';
import {
  LanguageState,
  LanguagesState,
  TargetingState
} from 'records/TargetingRecords';
import {
  AccountsActionConstants,
  FacebookActionConstants
} from 'constants/ActionConstants';
import * as FacebookActions from 'actions/FacebookActions';
import { CredentialRecord } from 'records/AccountsRecord';
import { mockGetVoicesResponse,
  stubGetVoicesRequest } from 'tests/api-responses/AccountsApiResponses';
import { TimezoneRecord, CompanyRecord } from 'reducers/EnvironmentReducer';
import {
  is,
  fromJS,
  Map as ImmutableMap,
  Set as ImmutableSet,
  List as ImmutableList
} from 'immutable';
import { UrlPropertiesRecord } from 'records/LinksRecords';
import { EditorState, ContentState } from 'draft-js';

describe('Accounts Actions', () => {

  let baseState = {
    accounts: {
      voices: [
        {
          data: {
            id: '1',
            name: 'Test 1',
            credentials: [
              {
                data: {
                  id: 1,
                  name: 'Page: Facebook Test',
                  service: 'FACEBOOK',
                  accountType: 'PAGE',
                  socialNetworkId: '1234',
                  uniqueId: 'unique1',
                  authenticated: false,
                  targetingSupported: false,
                  organicVisibilitySupported: false
                }
              },
              {
                data: {
                  id: 2,
                  name: 'Twitter Test',
                  service: 'TWITTER',
                  accountType: 'NOT A PAGE?',
                  socialNetworkId: '1234',
                  uniqueId: 'unique2',
                  authenticated: false,
                  targetingSupported: false,
                  organicVisibilitySupported: false
                }
              }
            ]
          }
        }
      ]
    }
  };
  let mockGetState;

  let fetchFacebookTargetingLanguagesSpy;

  beforeAll(() => {
    fetchFacebookTargetingLanguagesSpy = spyOn(FacebookActions, 'fetchFacebookTargetingLanguages').and.callThrough();
  });

  beforeEach(() => {
    mockGetState = mockState(baseState);
  });

  afterEach(() => {
    fetchFacebookTargetingLanguagesSpy.calls.reset();
  });

  describe('getVoices api call', () => {
    const companyId = '1';
    const initiativeId = '1';

    it('should return a list of accounts on successful api call', (done) => {
      const response = mockGetVoicesResponse({companyId, initiativeId});
      stubGetVoicesRequest({companyId, initiativeId, response});

      getVoices(companyId, initiativeId, [SupportedNetworks.FACEBOOK, SupportedNetworks.TWITTER, SupportedNetworks.GOOGLEPLUS])
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('fetchVoicesSuccessful Action', () => {
    it('should pass through the supplied value as a payload', () => {
      expect(accountsActions.fetchVoicesSuccessful('TEST')).toEqual({
        type: 'FETCH_VOICES',
        payload: 'TEST'
      });
    });
  });

  describe('quickSelectAllOrNone Action', () => {
    let actionQueue = [];
    const mockDispatch = (action) => actionQueue.push(action);

    beforeEach(() => {
      actionQueue = [];
    });
    it('should trigger fetching of facebook  targeting languages', () => {
      accountsActions.quickSelectAllOrNone(false)(() => {}, mockGetState);
      expect(fetchFacebookTargetingLanguagesSpy.calls.count()).toEqual(1);
    });
    it('should trigger no voices to select', () => {
      const tempState = mockState({
        ...baseState,
        accounts: {
          currentAccountFilterText: 'TEST',
          currentAccountChannelFilter: ImmutableList([])
        }
      });
      accountsActions.quickSelectAllOrNone(false)(mockDispatch, tempState);
      expect(actionQueue[0].payload).toEqual(new ImmutableMap());
    });
    it('should clear facebook targeting if all voices are being unchecked', () => {
      accountsActions.quickSelectAllOrNone(false)(mockDispatch, mockGetState);
      expect(actionQueue.find(action => action.type === FacebookActionConstants.CLEAR_TARGETING)).toBeTruthy();
    });
    it('should not clear facebook targeting if all voices are being checked', () => {
      accountsActions.quickSelectAllOrNone(true)(mockDispatch, mockGetState);
      expect(actionQueue.find(action => action.type === FacebookActionConstants.CLEAR_TARGETING)).toBeFalsy();
    });
  });

  describe('removeCredentialsForSelectedNetwork Action', () => {

    let actionQueue = [];
    const mockDispatch = (action) => actionQueue.push(action);

    beforeEach(() => {
      actionQueue = [];
    });

    it('should trigger fetching of facebook  targeting languages', () => {
      accountsActions.removeCredentialsForSelectedNetwork('FACEBOOK')(() => {}, mockGetState);
      expect(fetchFacebookTargetingLanguagesSpy.calls.count()).toEqual(1);
    });

    it('should clear facebook targeting if facebook is being cleared', () => {
      accountsActions.removeCredentialsForSelectedNetwork('FACEBOOK')(mockDispatch, mockGetState);
      expect(actionQueue.find(action => action.type === FacebookActionConstants.CLEAR_TARGETING)).toBeTruthy();
    });
    it('should not clear facebook targeting if a network other than facebook is being cleared', () => {
      accountsActions.removeCredentialsForSelectedNetwork('TWITTER')(mockDispatch, mockGetState);
      expect(actionQueue.find(action => action.type === FacebookActionConstants.CLEAR_TARGETING)).toBeFalsy();
    });
  });

  describe('quickSelectAllVoices Action', () => {
    it('should pass through with no payload', () => {
      expect(accountsActions.quickSelectAllVoices(false)).toEqual({
        type: 'QUICK_CREDENTIAL_SELECT',
        payload: false
      });
    });
  });

  describe('voiceCredentialCheckToggle Action', () => {
    it('should create the proper action to add new voices', () => {
      expect(accountsActions.voiceCredentialCheckToggle({voiceId: 1})).toEqual({
        type: 'VOICE_CRED_TOGGLE',
        payload: {
          voiceId: 1
        }
      });
    });
  });

  describe('searchOnVoicesToggle Action', () => {
    it('should pass through the supplied value as a payload', () => {
      expect(accountsActions.searchOnVoicesToggle()).toEqual({
        type: 'SEARCH_ON_VOICES_TOGGLE',
        payload: undefined
      });
    });
  });

  describe('voiceChecked Action', () => {
    let actionQueue = [];

    const mockDispatch  = (action) => {
      actionQueue.push(action);
    };

    const mockGetState = mockState({
      assignee: {
        currentAssignee: new AssigneeRecord({
          lastName: 'McTest',
          id: 1,
          firstName: 'Test',
          voices: [{data:{id:1}}, {data:{id:2}}],
          conflictingVoices: []
        })
      },
      accounts: {
        voices: [
          {
            data: {
              id: 1,
              name: 'Test 1',
              credentials: [
                {
                  data: {
                    id: 1,
                    name: 'Page: Facebook Test',
                    service: 'FACEBOOK',
                    accountType: 'PAGE',
                    socialNetworkId: '1234',
                    uniqueId: 'unique1',
                    authenticated: false,
                    organicVisibilitySupported: false,
                    targetingSupported: false
                  }
                },
                {
                  data: {
                    id: 2,
                    name: 'Twitter Test',
                    service: 'TWITTER',
                    accountType: 'NOT A PAGE?',
                    socialNetworkId: '1234',
                    uniqueId: 'unique2',
                    authenticated: false,
                    organicVisibilitySupported: false,
                    targetingSupported: false
                  }
                }
              ]
            }
          }
        ]
      }
    });

    beforeEach(() => {
      actionQueue = [];
    });

    it('should create the proper action to check voices', () => {
      accountsActions.voiceChecked(1, 'unique1')(mockDispatch, mockGetState);
      expect(actionQueue[0].type).toEqual('VOICE_CRED_TOGGLE');
      expect(is(actionQueue[0].payload,
        ImmutableMap({
          'unique1': new CredentialRecord({
            id: 1,
            name: 'Page: Facebook Test',
            socialNetwork: 'FACEBOOK',
            socialCredentialType: 'PAGE',
            socialNetworkId: '1234',
            uniqueId: 'unique1',
            authenticated: false,
            organicVisibilitySupported: false,
            targetingSupported: false
          })
        })
      )).toBeTruthy();
      expect(fetchFacebookTargetingLanguagesSpy.calls.count()).toBe(1);
    });
  });

  describe('updateFilterText action', () => {
    it('should send an action to update the filtering text for accounts', () => {
      expect(accountsActions.updateFilterText('test filter')).toEqual({
        type: AccountsActionConstants.UPDATE_FILTER_TEXT,
        payload: 'test filter'
      });
    });
  });

  describe('updateFilterChannels action', () => {
    it('should update filtered channels for accounts', () => {
      expect(accountsActions.updateFilterChannels(SupportedNetworks.FACEBOOK)).toEqual({
        type: AccountsActionConstants.UPDATE_FILTER_CHANNELS,
        payload: SupportedNetworks.FACEBOOK
      });
    });
  });

  describe('updateFilterSelection action', () => {
    it('should update the selection filtering value for accounts', () => {
      expect(accountsActions.updateFilterSelection(VoiceFilteringConstants.FILTER_SELECTED)).toEqual({
        type: AccountsActionConstants.UPDATE_FILTER_SELECTION,
        payload: VoiceFilteringConstants.FILTER_SELECTED
      });
    });
  });

  describe('async actions', () => {
    let actionQueue = [];

    const mockGetState = mockState({
      router: {
        params: {
          companyId: '2',
          campaignId: '2',
          eventId: '2'
        }
      },
      environment: {
        userId: 1,
        userTimezone: new TimezoneRecord(),
        capabilities: fromJS({
          FACEBOOK: {
            NEW_MESSAGE: {
              NOTE: {
                linkPreview: {
                  supported: true
                }
              }
            }
          },
          TWITTER: {
            NEW_MESSAGE: {
              NOTE: {
                linkPreview: {
                  supported: false
                }
              }
            }
          }
        }),
        company: new CompanyRecord({
          companyMetadata: {
            shortenLinksByDefault: true
          }
        })
      },
      message: {
        messageEditorStateMap: ImmutableMap({
          [DefaultNetwork]: EditorState.createWithContent(ContentState.createFromText('this is a test www.google.com of the link detection system www.reddit.com'))
        })
      },
      media: {
        mediaUploadStatus: RequestStatus.STATUS_NEVER_REQUESTED,
        mediaUploadError: '',
        mediaId: null,
        mediaFullUrl: '',
        mediaThumbUrl: ''
      },
      linkPreview: {
        linkUrl: 'www.google.com'
      },
      links: {
        urlProperties: ImmutableMap({
          'www.test.com': new UrlPropertiesRecord({
            shortenOnPublish: true,
            addLinkTags: true,
            associatedNetworks: ImmutableSet()
          })
        })
      },
      plan:{
        currentPlan: ImmutableMap({
          name: 'test',
          id: '1',
          color: 'blue'
        })
      }
    });

    let mockGetStateForMockDispatch = mockGetState;

    const mockDispatch  = (action) => {
      if (typeof action === 'function') {
        return action(mockDispatch, mockGetStateForMockDispatch);
      } else {
        actionQueue.push(action);
      }
    };

    describe('fetchVoices action', () => {
      const companyId = mockGetState().router.params.companyId;
      const initiativeId = mockGetState().router.params.campaignId;
      const response = mockGetVoicesResponse({companyId, initiativeId});

      beforeEach(() => {
        actionQueue = [];
      });

      it('should dispatch fetchVoicesSuccessful action on successful async action api call', (done) => {
        stubGetVoicesRequest({companyId, initiativeId, response});
        accountsActions.fetchVoices('')(mockDispatch, mockGetState)
          .then(res => {
            expect(actionQueue[0]).toEqual({
              type: AccountsActionConstants.FETCH_VOICES_REQUEST_STARTED,
              payload: undefined
            });
            expect(actionQueue[1]).toEqual({
              type: AccountsActionConstants.FETCH_VOICES,
              payload: response
            });
            done();
          });
      });
    });
  });

  describe('targeting logic', () => {
    let actionQueue = [];

    const mockDispatch  = (action) => {
      actionQueue.push(action);
    };

    beforeEach(() => {
      actionQueue = [];
    });
    const targetingState =  {
      accounts: {
        voices: [
          {
            data: {
              id: 1,
              name: 'Test 1',
              credentials: [
                {
                  data: {
                    id: 1,
                    name: 'Page: Facebook Test',
                    service: 'FACEBOOK',
                    accountType: 'PAGE',
                    socialNetworkId: '1234',
                    uniqueId: 'unique1',
                    authenticated: true,
                    targetingSupported: true,
                    organicVisibilitySupported: true
                  }
                },
                {
                  data: {
                    id: 2,
                    name: 'Do not target',
                    service: 'FACEBOOK',
                    accountType: 'NOT A PAGE?',
                    socialNetworkId: '1234',
                    uniqueId: 'unique2',
                    authenticated: false,
                    targetingSupported: false,
                    organicVisibilitySupported: true
                  }
                },
                {
                  data: {
                    id: 3,
                    name: 'Page: Target',
                    service: 'FACEBOOK',
                    accountType: 'PAGE',
                    socialNetworkId: '1234',
                    uniqueId: 'unique3',
                    authenticated: true,
                    targetingSupported: true,
                    organicVisibilitySupported: true
                  }
                }
              ]
            }
          }
        ],
        checkedCredentials: ImmutableMap({
          'unique1': new CredentialRecord({
            id: 1,
            name: 'Page: Facebook Test',
            socialNetwork: 'FACEBOOK',
            accountType: 'PAGE',
            authenticated: true,
            targetingSupported: true,
            organicVisibilitySupported: true,
            uniqueId: 'unique1'
          })
        })
      },
      facebook: new FacebookState({
        targeting: new TargetingState({
          language: new LanguagesState({
            selectedLanguages: ImmutableList([
              new LanguageState({
                value: '1',
                description: 'English'
              })
            ])
          })
        })
      }),
      darkPostStatus: false,
      assignee: {
        currentAssignee: new AssigneeRecord({
          lastName: 'McTest',
          id: '1',
          firstName: 'Test',
          voices: [{data:{id:1}}, {data:{id:2}}],
          conflictingVoices: []
        })
      }
    };
    beforeEach(() => {
      mockGetState = mockState(targetingState);
    });

    it('should only select accounts that are valid for facebook targeting if targeting is set when an entire voice is checked', () => {
      accountsActions.voiceChecked(1)(mockDispatch, mockGetState);
      expect(actionQueue[0].type).toEqual('VOICE_CRED_TOGGLE');
      expect(Object.keys(actionQueue[0].payload.toJS())).toDeepEqual(['unique1', 'unique3']);
    });

    it('should select all accounts if targeting is possible but not set', () => {
      const newState = {
        ...targetingState,
        facebook: new FacebookState()
      };
      mockGetState = mockState(newState);
      accountsActions.voiceChecked(1)(mockDispatch, mockGetState);
      expect(actionQueue[0].type).toEqual('VOICE_CRED_TOGGLE');
      expect(Object.keys(actionQueue[0].payload.toJS())).toDeepEqual(['unique1', 'unique2', 'unique3']);
    });

    it('should unselect all accounts if all valid targeting accounts have already been selected and a voice is unchecked', () => {
      mockGetState = mockState({
        ...targetingState,
        accounts: {
          ...targetingState.accounts,
          checkedCredentials: ImmutableMap({
            'unique1': new CredentialRecord({
              id: 1,
              name: 'Page: Facebook Test',
              socialNetwork: 'FACEBOOK',
              accountType: 'PAGE',
              authenticated: true,
              targetingSupported: true,
              organicVisibilitySupported: true,
              uniqueId: 'unique1'
            }),
            'unique3': new CredentialRecord({
              id: 3,
              name: 'Page: Target',
              socialNetwork: 'FACEBOOK',
              accountType: 'PAGE',
              authenticated: true,
              targetingSupported: true,
              organicVisibilitySupported: true,
              uniqueId: 'unique3'
            })
          })
        }
      });
      accountsActions.voiceChecked(1)(mockDispatch, mockGetState);
      expect(actionQueue[0].type).toEqual('VOICE_CRED_TOGGLE');
      expect(Object.keys(actionQueue[0].payload.toJS())).toDeepEqual([]);
    });

    it('should only select accounts that are valid for facebook targeting if targeting is set when all voices are checked', () => {
      accountsActions.quickSelectAllOrNone(true)(mockDispatch, mockGetState);
      expect(actionQueue[0].type).toEqual(AccountsActionConstants.QUICK_CREDENTIAL_SELECT);
      expect(Object.keys(actionQueue[0].payload.toJS())).toDeepEqual(['unique1', 'unique3']);
    });

    it('should unselect all accounts if targeting is set when all voices are unchecked', () => {
      accountsActions.quickSelectAllOrNone(false)(mockDispatch, mockGetState);
      const uncheckAction: ?Object = actionQueue.find(action => action.type === AccountsActionConstants.QUICK_CREDENTIAL_SELECT);
      expect(uncheckAction && uncheckAction.type).toEqual(AccountsActionConstants.QUICK_CREDENTIAL_SELECT);
      expect(Object.keys(uncheckAction && uncheckAction.payload.toJS() || {})).toDeepEqual([]);
    });
  });
});