import React from 'react';
import { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import DatetimePicker from 'components/DatetimePicker';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import { adjustUtcMomentDate,
  adjustUtcMomentTime } from 'utils/DateUtils';
import moment from 'moment';

// complete 100% clean re-render with fresh local state
function cleanRender(props = {}) {
  const component = ReactTestUtils.renderIntoDocument((
    <DatetimePicker {...props} />
  ));
  return findDOMNode(component);
}

describe('Scheduler Calendar Component', () => {
  let datetimePickerComponent;
  let datetimeInputProps;

  beforeEach(() => {
    const defaultMoment = moment('2050-01-15T10:00:00.000Z');
    datetimeInputProps = {
      scheduledDatetime: null,
      calendarCurrentDate: defaultMoment.toDate(),
      calendarDate: defaultMoment,
      userTimezone: new TimezoneRecord({
        abbreviation: 'CDT',
        name: 'American/Chicago',
        offset: -500
      }),
      timeString: '',
      timeError: false,
      debounceDelayMs: 100, //very short for tests
      setDate: () => true,
      setTime: () => true
    };
    spyOn(datetimeInputProps, 'setTime');
    spyOn(datetimeInputProps, 'setDate');
    datetimePickerComponent = cleanRender(datetimeInputProps);
  });

  it('should render combobox and calendar', () => {
    const calendar = datetimePickerComponent.querySelector('.DatetimePicker-datePicker');
    const comboBox = datetimePickerComponent.querySelector('.DatetimePicker-timePickerContainer');

    expect(ReactTestUtils.isDOMComponent(calendar)).toBeTruthy();
    expect(ReactTestUtils.isDOMComponent(comboBox)).toBeTruthy();
  });

  it('should render combobox and calendar', () => {
    const calendar = datetimePickerComponent.querySelector('.DatetimePicker-datePicker');
    const comboBox = datetimePickerComponent.querySelector('.DatetimePicker-timePickerContainer');

    expect(ReactTestUtils.isDOMComponent(calendar)).toBeTruthy();
    expect(ReactTestUtils.isDOMComponent(comboBox)).toBeTruthy();
  });

  it('should call respond to choosing a date from the calendar', () => {
    const calendar = datetimePickerComponent.querySelector('.DatetimePicker-datePicker');
    // Choosing all accurate dates that have click events that will trigger setting the date
    const dateToClick = calendar.querySelector('table tbody .rw-btn:not(.rw-off-range) .calendarDay:not(.outOfDate)').parentElement.parentElement;
    ReactTestUtils.Simulate.click(dateToClick);

    const badDateToClick = calendar.querySelector('table tbody .rw-btn:not(.rw-off-range) .calendarDay.outOfDate').parentElement.parentElement;
    ReactTestUtils.Simulate.click(badDateToClick);

    const clickedDate = moment(dateToClick.parentElement.title);
    const adjustedDate = adjustUtcMomentDate(datetimeInputProps.calendarDate, clickedDate, datetimeInputProps.userTimezone.offset, moment(datetimeInputProps.calendarCurrentDate));

    expect(datetimeInputProps.setDate).toHaveBeenCalledWith(adjustedDate);
  });

  it('should not respond to choosing a bad date from the calendar', () => {
    const calendar = datetimePickerComponent.querySelector('.DatetimePicker-datePicker');
    const badDateToClick = calendar.querySelector('table tbody .rw-btn:not(.rw-off-range) .calendarDay.outOfDate').parentElement.parentElement;
    ReactTestUtils.Simulate.click(badDateToClick);
    expect(datetimeInputProps.setDate).not.toHaveBeenCalled();
  });

  it('should toggle opening the combobox on click', () => {
    const comboBoxInput = datetimePickerComponent.querySelector('.DatetimePicker-timePickerContainer input');
    let popupOptions = datetimePickerComponent.querySelector('.rw-popup-container');

    expect(ReactTestUtils.isDOMComponent(popupOptions)).toBeFalsy();
    ReactTestUtils.Simulate.click(comboBoxInput);
    popupOptions = datetimePickerComponent.querySelector('.rw-popup-container');
    expect(ReactTestUtils.isDOMComponent(popupOptions)).toBeTruthy();
  });

  it('should select a time if clicked on in the options for the combobox', () => {
    const comboBoxInput = datetimePickerComponent.querySelector('.DatetimePicker-timePickerContainer input');
    ReactTestUtils.Simulate.click(comboBoxInput);
    const popupTimeOption = datetimePickerComponent.querySelector('.rw-popup-container ul li');

    let popupOptions = datetimePickerComponent.querySelector('.rw-popup-container');
    expect(ReactTestUtils.isDOMComponent(popupOptions)).toBeTruthy();

    ReactTestUtils.Simulate.click(popupTimeOption);

    expect(datetimeInputProps.setTime.calls.mostRecent().args).toDeepEqual(
      [popupTimeOption.textContent, adjustUtcMomentTime(datetimeInputProps.scheduledDatetime, popupTimeOption.textContent, datetimeInputProps.userTimezone.offset, moment(datetimeInputProps.calendarCurrentDate))]
    );

    // list closes on selecting something
    popupOptions = datetimePickerComponent.querySelector('.rw-popup-container');
    expect(ReactTestUtils.isDOMComponent(popupOptions)).toBeFalsy();
  });

  it('should change time if manually typing in time in the time input', () => {
    const comboBoxInput = datetimePickerComponent.querySelector('.DatetimePicker-timePickerContainer input');

    expect(datetimeInputProps.setTime).not.toHaveBeenCalled();
    ReactTestUtils.Simulate.change(comboBoxInput, {target: {value: '12:00pm'}});

    expect(datetimeInputProps.setTime).toHaveBeenCalledWith(
      '12:00pm',
      adjustUtcMomentTime(datetimeInputProps.scheduledDatetime, '12:00pm', datetimeInputProps.userTimezone.offset, moment(datetimeInputProps.calendarCurrentDate))
    );
  });

  it('should send errored time on blur of time input', () => {
    const comboBoxInput = datetimePickerComponent.querySelector('.DatetimePicker-timePickerContainer input');
    ReactTestUtils.Simulate.change(comboBoxInput, {target: {value: 'not a time'}});

    ReactTestUtils.Simulate.blur(comboBoxInput);

    expect(datetimeInputProps.setTime.calls.allArgs()).toDeepEqual([
      ['not a time', moment.invalid()]
    ]);
  });
});
