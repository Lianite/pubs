import React from 'react';
import { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import DatetimeInput from 'components/DatetimeInput';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import * as DateUtils from 'utils/DateUtils';

// rerendering onto the same dom node, aka respects prop changes and persists local state
function render(props = {}) {
  return ReactTestUtils.renderIntoDocument(<div><DatetimeInput {...props}/></div>);
}

let mockDatetimeSting;
let mockDatetime;
function mockSetDatetime(datetimeString, datetime): void {
  mockDatetimeSting = datetimeString;
  mockDatetime = datetime;
}

describe('DatetimeInput Component', () => {
  let datetimeInputComponent;
  let datetimeInputProps;

  beforeEach(() => {
    mockDatetimeSting = '';
    mockDatetime = null;
    datetimeInputProps = {
      headerTitle: 'Schedule',
      datetimeString: '',
      datetimeError: false,
      errorMessage: '',
      userTimezone: new TimezoneRecord({
        abbreviation: 'CDT',
        name: 'American/Chicago',
        offset: -500
      }),
      debounceDelayMs: 100, //very short for tests
      setDatetime: mockSetDatetime
    };
    datetimeInputComponent = findDOMNode(render(datetimeInputProps)).children[0];
  });

  it('should render correct input header', () => {
    const inputHeaderString = datetimeInputComponent.querySelector('.DatetimeInput-scheduleCalendarHeader');
    expect(inputHeaderString.textContent).toEqual(datetimeInputProps.headerTitle);
  });

  it('should render no errors when no errors are passed in', () => {
    const inputHeaderString = datetimeInputComponent.querySelector('.DatetimeInput-validationIcon.DatetimeInput-error');
    expect(inputHeaderString).toBe(null);
  });

  it('should render errors when errors are passed in', () => {
    datetimeInputProps.datetimeError = true;
    datetimeInputProps.errorMessage = 'There is an error';
    datetimeInputComponent = findDOMNode(render(datetimeInputProps)).children[0];

    const inputHeaderString = datetimeInputComponent.querySelector('.DatetimeInput-errorText').textContent;
    expect(inputHeaderString).toBe('There is an error');

    const inputWithError = datetimeInputComponent.querySelector('.DatetimeInput-scheduleCalendarDateTimeInput.DatetimeInput-error');
    expect(inputWithError).toBeDefined();
  });

  it('should render clear button when there is text', () => {
    datetimeInputProps.datetimeString = '01/02/2003';
    datetimeInputComponent = findDOMNode(render(datetimeInputProps)).children[0];

    const clearButton = datetimeInputComponent.querySelector('.DatetimeInput-clearButton');
    expect(clearButton).toBeDefined();
  });

  it('should call the setDatetime function on change of input', () => {
    const datetimeInput = datetimeInputComponent.querySelector('.DatetimeInput-scheduleCalendarDateTimeInput');
    ReactTestUtils.Simulate.change(datetimeInput, {target: {value: 'August 1st, 3000'}});
    expect(mockDatetimeSting).toBe('August 1st, 3000');
    expect(mockDatetime.local().format('MMMM Do, YYYY')).toBe('August 1st, 3000');
  });

  it('should call the setDatetime function on blur of input', () => {
    datetimeInputProps.datetimeString = 'August 1st, 3000';
    datetimeInputComponent = findDOMNode(render(datetimeInputProps)).children[0];

    const datetimeInput = datetimeInputComponent.querySelector('.DatetimeInput-scheduleCalendarDateTimeInput');
    ReactTestUtils.Simulate.blur(datetimeInput);
    expect(mockDatetimeSting).toBe('08/01/3000, 12:00am');
  });

  it('should set the default date on click of the input', () => {
    const datetimeInput = datetimeInputComponent.querySelector('.DatetimeInput-scheduleCalendarDateTimeInput');
    ReactTestUtils.Simulate.click(datetimeInput);

    const expectedDate = DateUtils.getDefaultScheduleTime(datetimeInputProps.userTimezone.offset);
    const expectedDateString = DateUtils.formatDatetimeForTextInput(expectedDate, datetimeInputProps.userTimezone.offset);
    expect(mockDatetimeSting).toBe(expectedDateString);
  });

  it('should clear the value of the datetime on clicking the clear button', () => {
    datetimeInputProps.datetimeString = 'August 1st, 3000';
    datetimeInputComponent = findDOMNode(render(datetimeInputProps)).children[0];

    const clearButton = datetimeInputComponent.querySelector('.DatetimeInput-clearButton');
    ReactTestUtils.Simulate.click(clearButton);

    expect(mockDatetimeSting).toBe('');
    expect(mockDatetime).toBe(null);
  });
});
