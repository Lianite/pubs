/*global spyOn:true*/

import React from 'react';
import Credential from 'components/Credential';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import { shallow } from 'enzyme';
import styles from 'styles/modules/components/Credential.scss';

let shallowWrapper;

let mockfunc = () => {
  return true;
};

const mainProps = {
  uniqueId: 'unique1',
  name: 'TEST',
  service: 'FACEBOOK',
  handleChecked: mockfunc,
  checked: false,
  authenticated: true
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<Credential {...props} />);
}

describe('Credential Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const credential = shallowWrapper.find(`.${styles.credential}`);
    expect(credential.length).toEqual(1);

    const checkbox = credential.find(Checkbox);
    expect(checkbox.length).toEqual(1);
    expect(checkbox.prop('checked')).toEqual(false);

    const icon = credential.find(`.rad-icon-network-facebook.${styles.networkIcon}.${styles.facebook}`);
    expect(icon.length).toEqual(1);

    const name = credential.find(`.${styles.credentialName}`);
    expect(name.length).toEqual(1);
    expect(name.text()).toEqual('TEST');

    expect(shallowWrapper.find(`.${styles.authenticationWarning}`).length).toEqual(0);
    expect(credential.find(`.${styles.credentialWarningIcon}`).length).toEqual(0);
  });

  it('should render correctly when the credential is not authenticated', () => {
    render({
      authenticated: false
    });
    const credential = shallowWrapper.find(`.${styles.credential}.${styles.authenticationWarning}`);
    expect(credential.length).toEqual(1);
    expect(credential.hasClass(styles.disabled)).toEqual(false);

    const warning = credential.children().find(`.${styles.warning}`);
    expect(warning.length).toEqual(1);
    expect(warning.text()).toEqual('Account requires reauthentication.');
    expect(credential.find(`.${styles.credentialWarningIcon}`).length).toEqual(1);
  });

  it('should render correctly when disabled', () => {
    const handleChecked = jasmine.createSpy('handleChecked');
    render({
      disabled: true,
      disabledMessage: 'This account cannot be selected.',
      handleChecked
    });
    const credential = shallowWrapper.find(`.${styles.credential}.${styles.disabled}`);

    const warning = credential.children().find(`.${styles.warning}`);
    expect(warning.length).toEqual(1);
    expect(warning.text()).toEqual('This account cannot be selected.');
  });

  it('should render correctly when disabled and the credential is not authenticated', () => {
    render({
      disabled: true,
      disabledMessage: 'This account cannot be selected.',
      authenticated: false
    });
    const credential = shallowWrapper.find(`.${styles.credential}.${styles.disabled}.${styles.authenticationWarning}`);

    expect(credential.find(`.${styles.credentialWarningIcon}`).length).toEqual(1);

    const warning = credential.children().find(`.${styles.warning}`);
    expect(warning.length).toEqual(1);
    expect(warning.text()).toEqual('Account requires reauthentication.This account cannot be selected.');
  });

  it('should call handleChecked when appropriate', () => {
    const handleChecked = jasmine.createSpy('handleChecked');
    render({
      handleChecked
    });

    let credential = shallowWrapper.find(`.${styles.credential}`);
    credential.simulate('click');

    expect(handleChecked).toHaveBeenCalledWith('unique1');

    render({
      disabled: true,
      handleChecked
    });

    credential = shallowWrapper.find(`.${styles.credential}`);
    credential.simulate('click');

    expect(handleChecked).not.toHaveBeenCalledWith();
  });

});
