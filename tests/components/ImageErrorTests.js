/* @flow */
import React from 'react';
import ImageError from 'components/media/ImageError';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/media/ImageError.scss';

let shallowWrapper;

describe('Image Error', () => {

  const props = {
    title: 'error title',
    description: 'error description'
  };

  const render = (componentProps = props) => {
    shallowWrapper = shallow(<ImageError {...componentProps}/>);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    expect(shallowWrapper.find(`.${styles.imageErrorContainer}`).length).toEqual(1);
    expect(shallowWrapper.find(`.${styles.mediaErrorTitle}`).first().text()).toEqual('error title');
    expect(shallowWrapper.find(`.${styles.mediaErrorDescription}`).first().text()).toEqual('error description');
  });

});
