import React, { Component } from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import LinkPreview from 'components/LinkPreview';
import ReactTestUtils from 'react-addons-test-utils';
import { PublishingError } from 'records/ErrorRecords';
import { List as ImmutableList } from 'immutable';

const Wrap = class Wrap extends Component {
  render() {
    return <LinkPreview {...this.props} />;
  }
};

const node = document.createElement('div');

const mainProps = {
  caption: 'www.spredfast.com',
  captionUrl: 'www.spredfast.com',
  description: 'Spredfast is an enterprise social media marketing platform.',
  networks: [],
  title: 'Spredfast',
  thumbnailUrl: '',
  onRemove: () => {}
};

const img = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((<Wrap {...props} />), node);
}

describe('Link Preview Component', () => {
  let linkPreviewComponent;
  let linkPreviewElement;

  beforeEach(() => {
    linkPreviewComponent = render();
    linkPreviewElement = findDOMNode(linkPreviewComponent);
  });

  it('should render a thumbnail holder if there is no thumbnailUrl', () => {
    const thumbnailHolder = linkPreviewElement.querySelector('.LinkPreview-thumbnailHolder');
    expect(thumbnailHolder).toBeTruthy();
    expect(thumbnailHolder.querySelector('p').textContent).toEqual('No image available');
    expect(thumbnailHolder.querySelector('.icon-no-image')).toBeTruthy();
  });

  it('should render a thumbnail if there is a thumbnailUrl', () => {
    linkPreviewComponent = render({
      thumbnailUrl: img
    });
    linkPreviewElement = findDOMNode(linkPreviewComponent);
    const thumbnail = linkPreviewElement.querySelector('.LinkPreview-thumbnail');
    expect(thumbnail).toBeTruthy();
    expect(thumbnail.querySelector('img').src).toEqual(img);
  });

  it('should render the customize thumbnail title', () => {
    const customizeThumbnail = linkPreviewElement.querySelector('.LinkPreview-customizeThumbnail');
    expect(customizeThumbnail).toBeTruthy();
    expect(customizeThumbnail.textContent).toEqual('Customize Thumbnail');
  });

  it('should render link preview details', () => {
    const linkPreviewDetails = linkPreviewElement.querySelector('.LinkPreview-linkPreviewDetails');
    expect(linkPreviewDetails).toBeTruthy();
    expect(linkPreviewDetails.querySelector('h3').textContent).toEqual('Spredfast');
    expect(linkPreviewDetails.querySelector('p').textContent).toEqual('Spredfast is an enterprise social media marketing platform.');
    expect(linkPreviewDetails.querySelectorAll('.rad-icon-edit').length).toEqual(2);
  });

  it('should call onEdit when change the link preview title', () => {
    const onEdit = jasmine.createSpy('onEdit');
    linkPreviewComponent = render({onEdit});

    const linkPreviewDetails = linkPreviewElement.querySelector('.LinkPreview-linkPreviewDetails');
    const title = linkPreviewDetails.querySelector('h3');
    title.textContent = 'new title';
    ReactTestUtils.Simulate.blur(title);

    expect(onEdit).toHaveBeenCalledWith({
      title: 'new title'
    });
  });

  it('should call onEdit when change the link preview description', () => {
    const onEdit = jasmine.createSpy('onEdit');
    linkPreviewComponent = render({onEdit});

    const linkPreviewDetails = linkPreviewElement.querySelector('.LinkPreview-linkPreviewDetails');
    const description = linkPreviewDetails.querySelector('p');
    description.textContent = 'new description';
    ReactTestUtils.Simulate.blur(description);

    expect(onEdit).toHaveBeenCalledWith({
      description: 'new description'
    });
  });


  it('should render a close button', () => {
    const closeButton = linkPreviewElement.querySelector('.LinkPreview-closeButton');
    expect(closeButton).toBeTruthy();
    expect(closeButton.querySelector('.icon-modal_close')).toBeTruthy();
  });

  it('should call onRemove handler when click close button', () => {
    const onRemove = jasmine.createSpy('onRemove');
    linkPreviewComponent = render({onRemove});
    const closeButton = linkPreviewElement.querySelector('.LinkPreview-closeButton');
    expect(closeButton).toBeTruthy();
    ReactTestUtils.Simulate.click(closeButton);
    expect(onRemove).toHaveBeenCalled();
  });

  it('should render the link preview caption', () => {
    const caption = linkPreviewElement.querySelector('.LinkPreview-caption');
    expect(caption).toBeTruthy();
    expect(caption.innerHTML).toEqual('<a href="http://www.spredfast.com" target="_blank">www.spredfast.com</a>');
  });

  it('should render the network list', () => {
    let networks = linkPreviewElement.querySelector('.LinkPreview-networks');
    expect(networks).toBeTruthy();
    expect(networks.querySelector('i')).toBeFalsy();

    linkPreviewComponent = render({
      networks: ['facebook', 'twitter']
    });
    linkPreviewElement = findDOMNode(linkPreviewComponent);

    networks = linkPreviewElement.querySelector('.LinkPreview-networks');
    expect(networks.querySelectorAll('.LinkPreview-icon').length).toEqual(2);
    expect(networks.querySelector('.LinkPreview-facebook')).toBeTruthy();
    expect(networks.querySelector('.LinkPreview-twitter')).toBeTruthy();
  });

  it('should display error when custom thumb upload fails', () => {
    linkPreviewComponent = render({
      error: new PublishingError({
        title: 'error title',
        description: 'error description',
        networks: ImmutableList()
      })
    });

    linkPreviewElement = findDOMNode(linkPreviewComponent);
    const errorTextContainer = linkPreviewElement.querySelector('.ImageError-mediaErrorText');
    expect(errorTextContainer).toBeTruthy();

    const errorIcon = errorTextContainer.querySelector('.ImageError-validationIcon');
    expect(errorIcon).toBeTruthy();

    const errorTitle = errorTextContainer.querySelector('.ImageError-mediaErrorTitle');
    expect(errorTitle.textContent).toEqual('error title');

    const errorDescription = errorTextContainer.querySelector('.ImageError-mediaErrorDescription');
    expect(errorDescription.textContent).toEqual('error description');

    const mediaErrorOverlay = linkPreviewElement.querySelector('.ImageError-mediaErrorOverlay');
    expect(mediaErrorOverlay).toBeTruthy();
  });
});
