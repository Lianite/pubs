import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import MessagePublishBar from 'components/MessagePublishBar';
import { publishBarText } from 'constants/UITextConstants';
import { MessageConstants } from 'constants/ApplicationConstants';

function renderStateless(props) {
  return ReactTestUtils.renderIntoDocument(<div><MessagePublishBar {...props}/></div>);
}

let mockfunc = () => {
  return true;
};

describe('Message Publish Bar Component', () => {
  let statelessComponent;
  let publishBarProps;

  beforeEach(() => {
    publishBarProps = {
      publishImmediately: true,
      onScheduleClicked: mockfunc,
      canPublish: true,
      canSaveDraft: true,
      cancelVerbage: 'Clear',
      hasEditAccess: true,
      canUseMultichannelPubs: true
    };
    spyOn(publishBarProps, 'onScheduleClicked');
    statelessComponent = renderStateless(publishBarProps);
  });

  // a basic test for looking at outputted elements in the DOM and verifing the results
  it('should render publish and save draft buttons that are not disabled', () => {
    let domNode = statelessComponent.children[0];
    expect(ReactTestUtils.isDOMComponent(domNode)).toBeTruthy();

    const publishButton = domNode.querySelector('.MessagePublishBar-publishBarButtonSchedule');
    expect(publishButton.textContent).toBe(publishBarText.immediatelyPublishText);
    expect(publishButton.attributes.disabled).toBeUndefined();

    const draftButton = domNode.querySelector('.MessagePublishBar-publishBarButtonSaveDraft');
    expect(draftButton.textContent).toBe(publishBarText.saveDraftButtonText);
    expect(draftButton.attributes.disabled).toBeUndefined();
  });

  it('should render schedule text on publish button when there is a date', () => {
    publishBarProps.publishImmediately = false;
    statelessComponent = renderStateless(publishBarProps);
    let domNode = statelessComponent.children[0];

    const publishButton = domNode.querySelector('.MessagePublishBar-publishBarButtonSchedule');
    expect(publishButton.textContent).toBe(publishBarText.schedulePublishText);
  });

  it('should render publish and save draft buttons that are disabled', () => {
    publishBarProps.canPublish = false;
    publishBarProps.canSaveDraft = false;
    statelessComponent = renderStateless(publishBarProps);
    let domNode = statelessComponent.children[0];

    const publishButton = domNode.querySelector('.MessagePublishBar-publishBarButtonSchedule');
    expect(publishButton.attributes.disabled).toBeDefined();

    const draftButton = domNode.querySelector('.MessagePublishBar-publishBarButtonSaveDraft');
    expect(draftButton.attributes.disabled).toBeDefined();
  });

  it('should not render the publish and save draft buttons if the user does not have permissions for these actions', () => {
    publishBarProps.hasEditAccess = true;
    statelessComponent = renderStateless(publishBarProps);
    let actionButtons = statelessComponent.querySelector('.MessagePublishBar-saveButtonsContainer');
    expect(ReactTestUtils.isDOMComponent(actionButtons)).toBeTruthy();

    publishBarProps.hasEditAccess = false;
    statelessComponent = renderStateless(publishBarProps);
    actionButtons = statelessComponent.querySelector('.MessagePublishBar-saveButtonsContainer');
    expect(ReactTestUtils.isDOMComponent(actionButtons)).toBeFalsy();

    publishBarProps.hasEditAccess = true;
    publishBarProps.canUseMultichannelPubs = false;
    statelessComponent = renderStateless(publishBarProps);
    statelessComponent = renderStateless(publishBarProps);
    actionButtons = statelessComponent.querySelector('.MessagePublishBar-saveButtonsContainer');
    expect(ReactTestUtils.isDOMComponent(actionButtons)).toBeFalsy();
  });

  it('should call the onScheduleClicked function on clicking of an action', () => {
    statelessComponent = renderStateless(publishBarProps);
    let actionButtons = statelessComponent.querySelectorAll('.MessagePublishBar-saveButtonsContainer button');
    ReactTestUtils.Simulate.click(actionButtons[0]);
    expect(publishBarProps.onScheduleClicked).toHaveBeenCalledWith(MessageConstants.PENDING);
    ReactTestUtils.Simulate.click(actionButtons[1]);
    expect(publishBarProps.onScheduleClicked).toHaveBeenCalledWith(MessageConstants.DRAFT);
  });
});
