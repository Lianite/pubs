/* @flow */
import React from 'react';
import NetworkOptionsBar from 'components/message/NetworkOptionsBar';
import { TextInput } from '@spredfast/react-lib';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/message/NetworkOptionsBar.scss';

describe('Network Options Bar', () => {

  let shallowWrapper;

  const mainProps = {
    customVideoTitle: 'custom title, yo!',
    setCustomVideoTitle: jasmine.createSpy('setCustomVideoTitle')
  };

  function render(props = mainProps) {
    const finalProps = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<NetworkOptionsBar {...finalProps} />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const bar = shallowWrapper.find(`.${styles.barWrapper}`);
    expect(bar.length).toEqual(1);
    expect(bar.text()).toContain('Video Title');
    expect(bar.text()).not.toContain('UPDATE FACEBOOK VIDEO TITLE');

    const input = shallowWrapper.find(TextInput);
    expect(input.prop('value')).toEqual('custom title, yo!');
    expect(input.prop('placeholder')).toEqual('Add a title for this video (optional)...');

    input.simulate('debouncedChange', { target: { value: 'a' } });
    expect(mainProps.setCustomVideoTitle).toHaveBeenCalledWith('a');

    expect(shallowWrapper.find(`.${styles.expanded}`).length).toEqual(0);
  });

  it('should pass change handler to the input appropriately', () => {
    const input = shallowWrapper.find(TextInput);

    input.simulate('debouncedChange', { target: { value: 'a' } });
    expect(mainProps.setCustomVideoTitle).toHaveBeenCalledWith('a');
  });

  it('should render the correct message when expanded', () => {
    const button = shallowWrapper.find('button');
    button.simulate('click');

    const bar = shallowWrapper.find(`.${styles.barWrapper}`);
    expect(bar.text()).toContain('UPDATE FACEBOOK VIDEO TITLE');

    expect(shallowWrapper.find(`.${styles.expanded}`).length).toBeGreaterThan(0);
  });
});
