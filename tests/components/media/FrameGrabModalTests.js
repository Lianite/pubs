/* @flow */
import React from 'react';
import { shallow } from 'enzyme';
import FrameGrabModal from 'components/media/FrameGrabModal';
import FrameGrabContainer from 'containers/media/FrameGrabContainer';
import { Modal, Button } from '@spredfast/react-lib';

let shallowWrapper;

const mainProps = {
  close: () => {},
  respond: () => {}
};

const render = (props = {}) => {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FrameGrabModal {...props} />);
};

let respond;

describe('FrameGrabModal Component', () => {

  beforeEach(() => {
    respond = jasmine.createSpy('respond');
    render({
      respond
    });
  });

  it('should render FrameGrab component', () => {
    const modal = shallowWrapper.find(Modal.Modal);
    const body = modal.dive().find(Modal.Body);
    const frameGrab = body.dive().find(FrameGrabContainer);
    expect(frameGrab.length).toBe(1);
  });

  it('should call respond with false when cancel clicked', () => {
    const modal = shallowWrapper.find(Modal.Modal);
    const footer = modal.dive().find(Modal.Footer);
    const footerButtons = footer.dive().find(Button);
    expect(footerButtons.length).toBe(2);
    footerButtons.first().prop('onClick')();
    expect(respond).toHaveBeenCalledWith(false);
  });

  it('should call respond with false when close button clicked', () => {
    const modal = shallowWrapper.find(Modal.Modal);
    const header = modal.dive().find(Modal.Header);
    const closeButton = header.dive().find('.FrameGrabModal-closeButton');
    expect(closeButton.length).toBe(1);
    closeButton.simulate('click');
    expect(respond).toHaveBeenCalled();
  });

  it('should call respond with false when done clicked and no thumbnail change', () => {
    const modal = shallowWrapper.find(Modal.Modal);
    const footer = modal.dive().find(Modal.Footer);
    const footerButtons = footer.dive().find(Button);
    expect(footerButtons.length).toBe(2);
    footerButtons.last().prop('onClick')();
    expect(respond).toHaveBeenCalledWith(false);
  });

  it('should call respond with thumbnail when done clicked and thumbnail changed', () => {
    const dummyThumb = {id: 'id', uri: 'uri'};
    shallowWrapper.instance().selectedThumbnailChanged(dummyThumb);
    const modal = shallowWrapper.find(Modal.Modal);
    const footer = modal.dive().find(Modal.Footer);
    const footerButtons = footer.dive().find(Button);
    expect(footerButtons.length).toBe(2);
    footerButtons.last().prop('onClick')();
    expect(respond).toHaveBeenCalledWith(dummyThumb);
  });
});
