/* @flow */
import React from 'react';
import { shallow } from 'enzyme';
import FrameGrab, { PotentialThumbnail } from 'components/media/FrameGrab';

let shallowWrapper;

const mainProps = {
  initialThumbnail: {id: 'id0', uri: 'uri0'},
  possibleThumbnails: [
    {
      id: 'id0',
      uri: 'uri0'
    },
    {
      id: 'id1',
      uri: 'uri1'
    },
    {
      id: 'id2',
      uri: 'uri2'
    },
    {
      id: 'id3',
      uri: 'uri3'
    },
    {
      id: 'id4',
      uri: 'uri4'
    },
    {
      id: 'id5',
      uri: 'uri5'
    },
    {
      id: 'id6',
      uri: 'uri6'
    },
    {
      id: 'id7',
      uri: 'uri7'
    },
    {
      id: 'id8',
      uri: 'uri8'
    },
    {
      id: 'id9',
      uri: 'uri9'
    },
    {
      id: 'id10',
      uri: 'uri10'
    },
    {
      id: 'id11',
      uri: 'uri11'
    }
  ],
  selectedThumbnailChanged: (thumb) => {}
};

const render = (props = {}) => {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FrameGrab {...props} />);
};

let selectedThumbnailChanged;

describe('FrameGrab Component', () => {

  beforeEach(() => {
    selectedThumbnailChanged = jasmine.createSpy('selectedThumbnailChanged');
    render({
      selectedThumbnailChanged
    });
  });

  it('should render 12 PotentialThumbnail components', () => {
    expect(shallowWrapper.find(PotentialThumbnail).length).toBe(12);
  });

  it('should have the first PotentialThumbnail selected', () => {
    expect(shallowWrapper.state().currentlySelectedThumb).toDeepEqual({id: 'id0', uri: 'uri0'});
    const firstPotentialThumbnail = shallowWrapper.find(PotentialThumbnail).first();
    expect(firstPotentialThumbnail.prop('selected')).toBe(true);
    expect(firstPotentialThumbnail.prop('id')).toBe('id0');
    expect(firstPotentialThumbnail.prop('uri')).toBe('uri0');
  });

  it('should have the first PotentialThumbnail selected only', () => {
    shallowWrapper.find(PotentialThumbnail).forEach((node) => {
      if (node.prop('id') === 'id0') {
        expect(node.prop('selected')).toBe(true);
      } else {
        expect(node.prop('selected')).toBe(false);
      }
    });
  });

  it('should update state, and selected, and call callback when new thumbnail clicked', () => {
    shallowWrapper.find(PotentialThumbnail).last().dive().find('.FrameGrab-potentialThumbnail').simulate('click');
    expect(shallowWrapper.state().currentlySelectedThumb).toDeepEqual({id: 'id11', uri: 'uri11'});
    shallowWrapper.find(PotentialThumbnail).forEach((node) => {
      if (node.prop('id') === 'id11') {
        expect(node.prop('selected')).toBe(true);
      } else {
        expect(node.prop('selected')).toBe(false);
      }
    });
    expect(selectedThumbnailChanged).toHaveBeenCalledWith({id: 'id11', uri: 'uri11'});
  });

  it('should NOT update and call callback when same thumbnail clicked', () => {
    shallowWrapper.find(PotentialThumbnail).first().dive().find('.FrameGrab-potentialThumbnail').simulate('click');
    expect(shallowWrapper.state().currentlySelectedThumb).toDeepEqual({id: 'id0', uri: 'uri0'});
    shallowWrapper.find(PotentialThumbnail).forEach((node) => {
      if (node.prop('id') === 'id0') {
        expect(node.prop('selected')).toBe(true);
      } else {
        expect(node.prop('selected')).toBe(false);
      }
    });
    expect(selectedThumbnailChanged).not.toHaveBeenCalled();
  });
});
