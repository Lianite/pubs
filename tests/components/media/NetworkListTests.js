import React, { Component } from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import { DefaultNetwork, networkIcons} from 'constants/ApplicationConstants';
import NetworkList from 'components/media/NetworkList';

const Wrap = class Wrap extends Component {
  render() {
    return <NetworkList {...this.props} />;
  }
};

const node = document.createElement('div');

const mainProps = {
  networks: ['FACEBOOK', 'TWITTER']
};

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((<Wrap {...props} />), node);
}

describe('NetworkList Component', () => {
  let networkListComponent;
  let networkListElement;

  beforeEach(() => {
    networkListComponent = render();
    networkListElement = findDOMNode(networkListComponent);
  });

  it('should render network icon list', () => {
    expect(networkListElement.querySelectorAll('.NetworkList-icon').length).toEqual(2);
    expect(networkListElement.querySelector(`.${networkIcons.FACEBOOK}`)).toBeTruthy();
    expect(networkListElement.querySelector(`.${networkIcons.TWITTER}`)).toBeTruthy();
  });

  it('should render no network selected message when the only network is the default one', () => {
    networkListComponent = render({
      networks: [DefaultNetwork]
    });
    networkListElement = findDOMNode(networkListComponent);
    expect(networkListElement.textContent).toEqual('No Network Selected');
  });

  it('should render no network selected message when there is no network selected', () => {
    networkListComponent = render({
      networks: []
    });
    networkListElement = findDOMNode(networkListComponent);
    expect(networkListElement.textContent).toEqual('No Network Selected');
  });
});
