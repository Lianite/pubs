import React, { Component } from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import Image from 'components/media/Image';

const Wrap = class Wrap extends Component {
  render() {
    return <Image {...this.props} />;
  }
};

const node = document.createElement('div');
const imageSrc = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

const mainProps = {
  networks: ['FACEBOOK'],
  uploading: true,
  image: null,
  onRemove: () => {},
  mediaFilesizeError: null
};

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((<Wrap {...props} />), node);
}

describe('Image Component', () => {
  let imageComponent;
  let imageElement;

  beforeEach(() => {
    imageComponent = render();
    imageElement = findDOMNode(imageComponent);
  });

  it('should render the loading animation when is uploading the image', () => {
    expect(imageElement.querySelector('.sf-LoadingAnimation')).toBeTruthy();
    imageComponent = render({
      uploading: false
    });
    imageElement = findDOMNode(imageComponent);
    expect(imageElement.querySelector('.sf-LoadingAnimation')).toBeFalsy();
  });

  it('should render the uploaded image', () => {
    imageComponent = render({
      uploading: false,
      image: imageSrc
    });
    imageElement = findDOMNode(imageComponent);
    const placeHolder = imageElement.querySelector('.Image-mediaPlaceholderImage');
    const image = placeHolder.style.backgroundImage;
    expect(image === `url("${imageSrc}")` || image ===  `url(${imageSrc})`).toBeTruthy();
  });

  it('should render the image errors if there are any', () => {
    imageComponent = render({
      uploading: false,
      mediaFilesizeError: {
        title: 'foo',
        description: 'bar'
      }
    });
    imageElement = findDOMNode(imageComponent);
    const errorTextContainer = imageElement.querySelector('.ImageError-mediaErrorText');
    expect(errorTextContainer).toBeTruthy();

    const errorIcon = errorTextContainer.querySelector('.ImageError-validationIcon');
    expect(errorIcon).toBeTruthy();

    const errorTitle = errorTextContainer.querySelector('.ImageError-mediaErrorTitle');
    expect(errorTitle.textContent).toEqual('foo');

    const errorDescription = errorTextContainer.querySelector('.ImageError-mediaErrorDescription');
    expect(errorDescription.textContent).toEqual('bar');

    const mediaErrorOverlay = imageElement.querySelector('.ImageError-mediaErrorOverlay');
    expect(mediaErrorOverlay).toBeTruthy();
  });

  it('should call remove handler when click over remove button', () => {
    const onRemove = jasmine.createSpy('onRemove');
    imageComponent = render({onRemove});
    imageElement = findDOMNode(imageComponent);

    const closeButton = imageElement.querySelector('.Image-mediaRemoveButton');
    expect(closeButton).toBeTruthy();
    ReactTestUtils.Simulate.click(closeButton);
    expect(onRemove).toHaveBeenCalled();
  });

  it('should render the networks when the image is uploaded', () => {
    imageComponent = render({
      uploading: false,
      image: imageSrc
    });
    imageElement = findDOMNode(imageComponent);
    const icon = imageElement.querySelector('.NetworkList-networks .NetworkList-facebook');
    expect(icon).toBeTruthy();
  });
});
