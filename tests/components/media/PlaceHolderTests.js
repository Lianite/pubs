import React, { Component } from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import PlaceHolder from 'components/media/PlaceHolder';

const Wrap = class Wrap extends Component {
  render() {
    return <PlaceHolder {...this.props} />;
  }
};

const node = document.createElement('div');

const mainProps = {
  text: 'foo',
  dragging: false,
  onLoadFiles: () => {}
};

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((<Wrap {...props} />), node);
}

describe('PlaceHolder Component', () => {
  let placeHolderComponent;
  let placeHolderElement;

  beforeEach(() => {
    placeHolderComponent = render();
    placeHolderElement = findDOMNode(placeHolderComponent);
  });

  it('should render upload image icon', () => {
    const icon = placeHolderElement.querySelector('.PlaceHolder-mediaBarIcon');
    expect(icon).toBeTruthy();
  });

  it('should display upload text message', () => {
    const text = placeHolderElement.querySelector('span');
    expect(text.textContent).toEqual('foo');
  });

  it('should display upload button when is not dragging', () => {
    let uploadButton = placeHolderElement.querySelector('button');
    expect(uploadButton).toBeTruthy();

    placeHolderComponent = render({
      dragging: true
    });
    placeHolderElement = findDOMNode(placeHolderComponent);
    uploadButton = placeHolderElement.querySelector('button');
    expect(uploadButton).toBeFalsy();
  });

  it('should call onLoadFiles function when click over upload button', () => {
    const onLoadFiles = jasmine.createSpy('onLoadFiles');
    placeHolderComponent = render({
      dragging: false,
      onLoadFiles
    });
    placeHolderElement = findDOMNode(placeHolderComponent);
    const uploadButton = placeHolderElement.querySelector('button');
    ReactTestUtils.Simulate.click(uploadButton);
    expect(onLoadFiles).toHaveBeenCalled();
  });
});
