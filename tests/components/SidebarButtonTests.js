import React from 'react';
import SidebarButton from 'components/SidebarButton';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/SidebarButton.scss';

describe('Sidebar Button Tests', () => {
  let shallowWrapper;

  const mainProps = {
    buttonPageName: 'test',
    buttonImage: 'buttonImageUrl',
    active: false,
    onClick: jasmine.createSpy('onClick')
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<SidebarButton {...props} />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const container = shallowWrapper.find(`.${styles.sidebarButtonContainer}`);
    expect(container.length).toEqual(1);
    expect(container.text()).toEqual('test');

    const button = container.find(`.${styles.sidebarButton}`);
    expect(button.length).toEqual(1);
    expect(button.hasClass(styles.activeButton)).toBe(false);

    const image = button.find('.buttonImageUrl');
    expect(image.length).toEqual(1);

    const tooltip = button.find(`.${styles.tooltiptext}`);
    expect(tooltip.length).toEqual(1);
  });

  it('should render correctly', () => {
    render({
      active: true
    });
    const container = shallowWrapper.find(`.${styles.sidebarButtonContainer}`);

    const button = container.find(`.${styles.sidebarButton}.${styles.activeButton}`);
    expect(button.length).toEqual(1);
  });

  it('should add the appropriate class on enter and invoke the callback after 600ms', (done) => {
    const callback = jasmine.createSpy('callback');
    shallowWrapper.instance().rootNode = {};
    shallowWrapper.instance().componentWillEnter(callback);

    expect(shallowWrapper.instance().rootNode.className).toEqual(`${styles.sidebarButtonContainer} ${styles.sfSidebarButtonEnter}`);

    setTimeout(() => {
      expect(callback).toHaveBeenCalled();
      expect(shallowWrapper.instance().rootNode.className).toEqual(styles.sidebarButtonContainer);
      done();
    }, 600);
  });

  it('should add the appropriate class on leave and invoke the callback after 500ms', (done) => {
    const callback = jasmine.createSpy('callback');
    shallowWrapper.instance().rootNode = {};
    shallowWrapper.instance().componentWillLeave(callback);

    expect(shallowWrapper.instance().rootNode.className).toEqual(`${styles.sidebarButtonContainer} ${styles.sfSidebarButtonExit}`);

    setTimeout(() => {
      expect(callback).toHaveBeenCalled();
      done();
    }, 500);
  });

  describe('error rendering', () => {
    it('should not render an error', () => {
      expect(shallowWrapper.find(`.${styles.error}`).length).toEqual(0);
    });

    it('should render an error', () => {
      render({
        error: true
      });
      expect(shallowWrapper.find(`.${styles.error}`).length).toEqual(1);
    });
  });
});
