/*global spyOn:true*/

import React from 'react';
import { shallow } from 'enzyme';
import Voice from 'components/Voice';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';

import styles from 'styles/modules/components/Voice.scss';

describe('Voice Component', () => {
  let shallowWrapper;

  const mainProps = {
    id: 1,
    name: 'TEST',
    handleChecked: jasmine.createSpy('handleChecked'),
    credentials: [
      {
        id: 1,
        name: 'Test',
        service: 'FACEBOOK'
      },
      {
        id: 2,
        name: 'Example',
        service: 'TWITTER'
      }
    ],
    unfilteredCredentialCount: 2,
    checkedEntry: {}
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<Voice {...props} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.handleChecked.calls.reset();
  });

  it('should render correctly', () => {
    const voice = shallowWrapper.find(`.${styles.voice}`);
    expect(voice.length).toEqual(1);

    const holder = voice.find(`.${styles.uncheckedVoiceHolder}`);
    expect(holder.length).toEqual(1);

    const checkbox = holder.find(Checkbox);
    expect(checkbox.prop('checked')).toEqual(false);

    expect(voice.text()).toContain('TEST');
  });

  it('should render correctly with a checked entry', () => {
    render({
      checkedEntry: {
        '2': true
      }
    });

    const checkbox = shallowWrapper.find(Checkbox);
    expect(checkbox.prop('checked')).toEqual('indeterminate');
  });

  it('should render correctly when all entries are checked', () => {
    render({
      checkedEntry: {
        '2': true,
        '1': true
      }
    });

    const checkbox = shallowWrapper.find(Checkbox);
    expect(checkbox.prop('checked')).toEqual(true);
  });

  it('should call handleChecked', () => {
    const holder = shallowWrapper.find(`.${styles.uncheckedVoiceHolder}`);
    holder.simulate('click');

    expect(mainProps.handleChecked).toHaveBeenCalledWith(1);
  });
});
