import React from 'react';
import MentionsQuery from 'components/MentionsQuery';
import MentionQueryRow from 'components/MentionQueryRow.js';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { EditorState } from 'draft-js';
import { List as ImmutableList } from 'immutable';
import { MentionsActionConstants } from 'constants/ActionConstants';
import { mentionsQueryMenuText } from 'constants/UITextConstants';
import { getCompositeDecorator } from 'decorators/EditorDecorator';
import { shallow } from 'enzyme';
import { MentionsRecord } from 'types/MentionsTypes';

import styles from 'styles/modules/components/MentionsQuery.scss';

let shallowWrapper;

const mainProps = {
  mentionsQueryList: ImmutableList([]),
  sendUpdatedEditorState: jasmine.createSpy('sendUpdatedEditorState'),
  getMentionsQueryUiState: true,
  editorState: EditorState.createEmpty(),
  network: 'FACEBOOK',
  decorators: getCompositeDecorator('', ''),
  onBlur: jasmine.createSpy('onBlur'),
  loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_QUERY_NOT_REQUESTED,
  closeMentionsQueryDialog: jasmine.createSpy('closeMentionsQueryDialog')
};

const render = (props = mainProps) => {
  props = {
    ...mainProps,
    ...props
  };
  shallowWrapper = shallow(<MentionsQuery {...props} />);
};

describe('MentionsQuery Component', () => {
  beforeEach(() => {
    render();
  });
  describe('Query Not Requested', () => {

    const notRequestedMentionsProps = {
      loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_QUERY_NOT_REQUESTED
    };

    beforeEach(() => {
      render(notRequestedMentionsProps);
    });

    it('should render correctly when mentions haven\'t been requested', () => {
      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.text()).toBeFalsy();
    });
  });

  describe('Query Requested. Not Returned', () => {
    const requestedMentionsProps = {
      loadMentionsQueryStatus: MentionsActionConstants.REQUEST_MENTIONS_DATA
    };

    beforeEach(() => {
      render(requestedMentionsProps);
    });

    it('should render correctly when mentions have been requested', () => {
      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.length).toEqual(1);

      const spinner = query.find(`.${styles.loadingQuerySuggestions}`);
      expect(spinner.length).toEqual(1);
      expect(spinner.text()).toContain(mentionsQueryMenuText.retrievingAccounts);
    });
  });

  describe('Query Requested, Error Returned', () => {
    const errorMentionsProps = {
      loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_DATA_ERROR
    };

    beforeEach(() => {
      render(errorMentionsProps);
    });

    it('should render correctly when there is an error', () => {
      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.length).toEqual(1);

      const error = query.find(`.${styles.errorQuerying}`);
      expect(error.length).toEqual(1);
      expect(error.text()).toEqual(mentionsQueryMenuText.errorQuerying);
    });
  });

  describe('Facebook Query Returned', () => {
    const facebookMentionsProps = {
      mentionsQueryList: ImmutableList([
        new MentionsRecord(
          {
            'profileId':'7177913734',
            'screenName':'testScreenName',
            'displayName':'Reddit',
            'link':'https://www.facebook.com/reddit/',
            'icon':'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
            'likes':1026470,
            'category':'News/Media Website',
            'location':'Redditch, United Kingdom',
            'verified':true
          }),
        new MentionsRecord(
          {
            'profileId':'112236622122442',
            'screenName':'',
            'displayName':'Redditch',
            'link':'https://www.facebook.com/pages/Redditch/112236622122442',
            'icon':'https://external.xx.fbcdn.net/safe_image.php?d=AQADDfGggIw1hlms&w=50&h=50&url=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F45%2FRedditch_015.jpg%2F720px-Redditch_015.jpg&fallback=hub_city',
            'likes':-1,
            'category':'',
            'location':'',
            'verified':false
          })
      ]),
      loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_RECEIVED
    };

    beforeEach(() => {
      render(facebookMentionsProps);
    });

    it('should render mentions results correctly', () => {
      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.length).toEqual(1);

      const mentions = query.find(MentionQueryRow);
      expect(mentions.length).toEqual(2);
      mentions.forEach((mention, index) => {
        expect(mention.props()).toEqual(jasmine.objectContaining({
          mention: facebookMentionsProps.mentionsQueryList.get(index),
          network: 'FACEBOOK',
          editorState: mainProps.editorState,
          decorators: mainProps.decorators,
          sendUpdatedEditorState: mainProps.sendUpdatedEditorState,
          onBlur: mainProps.onBlur
        }));
      });
    });

    it('should render correctly when there are no results', () => {
      render({
        ...facebookMentionsProps,
        mentionsQueryList: ImmutableList([])
      });

      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.length).toEqual(1);

      const mentions = query.find(MentionQueryRow);
      expect(mentions.length).toEqual(0);
      expect(query.text()).toContain(mentionsQueryMenuText.noResultsFound);
    });
  });

  describe('Twitter Query Returned', () => {
    const twitterMentionsProps = {
      mentionsQueryList: ImmutableList([
        new MentionsRecord(
          {
            'profileId':'7177913734',
            'screenName':'testScreenName',
            'displayName':'Reddit',
            'link':'https://www.facebook.com/reddit/',
            'icon':'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12072823_10153682419958735_2551945902722283373_n.png?oh=f0f4fe5e1a2c8cec0b60ea3db865ed20&oe=588D4505',
            'likes':-1,
            'category':'News/Media Website',
            'location':'Redditch, United Kingdom',
            'verified':true
          }),
        new MentionsRecord(
          {
            'profileId':'112236622122442',
            'screenName':'',
            'displayName':'Redditch',
            'link':'https://www.facebook.com/pages/Redditch/112236622122442',
            'icon':'https://external.xx.fbcdn.net/safe_image.php?d=AQADDfGggIw1hlms&w=50&h=50&url=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F45%2FRedditch_015.jpg%2F720px-Redditch_015.jpg&fallback=hub_city',
            'likes':-1,
            'category':'',
            'location':'',
            'verified':false
          })
      ]),
      network: SupportedNetworks.TWITTER,
      loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_RECEIVED
    };

    beforeEach(() => {
      render(twitterMentionsProps);
    });

    it('should render mentions results correctly', () => {
      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.length).toEqual(1);

      const mentions = query.find(MentionQueryRow);
      expect(mentions.length).toEqual(2);
      mentions.forEach((mention, index) => {
        expect(mention.props()).toEqual(jasmine.objectContaining({
          mention: twitterMentionsProps.mentionsQueryList.get(index),
          network: 'TWITTER',
          editorState: mainProps.editorState,
          decorators: mainProps.decorators,
          sendUpdatedEditorState: mainProps.sendUpdatedEditorState,
          onBlur: mainProps.onBlur
        }));
      });
    });

    it('should render correctly when there are no results', () => {
      render({
        ...twitterMentionsProps,
        mentionsQueryList: ImmutableList([])
      });

      const query = shallowWrapper.find(`.${styles.mentionsQuery}`);
      expect(query.length).toEqual(1);

      const mentions = query.find(MentionQueryRow);
      expect(mentions.length).toEqual(0);
      expect(query.text()).toContain(mentionsQueryMenuText.noResultsFound);
    });
  });
});
