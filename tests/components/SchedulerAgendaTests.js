import React from 'react';
import { shallow } from 'enzyme';
import SchedulerAgenda from 'components/SchedulerAgenda';

import styles from 'styles/modules/components/SchedulerAgenda.scss';

describe('Scheduler Agenda Component', () => {
  let shallowWrapper;

  const render = () => {
    shallowWrapper = shallow(<SchedulerAgenda />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const container = shallowWrapper.find(`.${styles.scheduleAgendaContainer}`);
    expect(container.length).toEqual(1);

    const icon = container.find(`.${styles.comingSoonSvg}`);
    expect(icon.length).toEqual(1);

    expect(container.text()).toEqual('Calendar is arriving soon!');
  });
});
