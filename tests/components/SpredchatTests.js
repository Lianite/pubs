import React from 'react';
import { findDOMNode } from 'react-dom';
import Spredchat from 'components/Spredchat';
import ReactTestUtils from 'react-addons-test-utils';

function renderStateless(props) {
  return ReactTestUtils.renderIntoDocument(<div><Spredchat {...props}/></div>);
}

describe('Spredchat Component', () => {
  let props;
  let statelessComponent;

  beforeEach(() => {
    props = {
      id: '12',
      name:'foo',
      companyId: '3',
      campaignId: '4'
    };
    statelessComponent = renderStateless(props);
  });

  it('renders SpredchatNoteMeta component when name is truthy', () => {
    let domNode = findDOMNode(statelessComponent).children[0];
    expect(ReactTestUtils.isDOMComponent(domNode)).toBeTruthy();
  });

  it('does not render when truthy name is not provided', () => {
    props.name = '';
    statelessComponent = renderStateless(props);
    let domNode = findDOMNode(statelessComponent).children[0];
    expect(ReactTestUtils.isDOMComponent(domNode)).toBeFalsy();
  });

  it('does not render when truthy id is not provided', () => {
    props.id = '';
    statelessComponent = renderStateless(props);
    let domNode = findDOMNode(statelessComponent).children[0];
    expect(ReactTestUtils.isDOMComponent(domNode)).toBeFalsy();
  });

});