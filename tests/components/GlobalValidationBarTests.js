import React from 'react';
import { shallow } from 'enzyme';
import GlobalValidationBar from 'components/GlobalValidationBar';
import { ValidationType } from 'constants/UIStateConstants';

import styles from 'styles/modules/components/GlobalValidationBar.scss';

let shallowWrapper;

const mainProps = {
  validation: {
    type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
    validationText: 'this is validation text',
    errorsPerNetwork: {}
  }
};

const render = (props = mainProps) => {
  props = {
    ...mainProps,
    ...props
  };
  shallowWrapper = shallow(<GlobalValidationBar {...props} />);
};

describe('Global Validation Bar', () => {

  // the before each function is ran before every test (it function) and should be used
  // to setup data for our tests. tests (again, it functions) aren't always ran in the order
  // you list them as when the server executes them, so by using beforeEach we gaurentee our
  // beforehand data will be there.
  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const bar = shallowWrapper.find(`.${styles.globalValidationBar}`);
    expect(bar.length).toEqual(1);
    expect(bar.hasClass(styles.active)).toBe(false);
    expect(bar.hasClass(styles.error)).toBe(false);
    expect(bar.hasClass(styles.improvement)).toBe(false);
    expect(bar.hasClass(styles.permissionError)).toBe(false);

    const wrapper = bar.find(`.${styles.contentWrapper}`);
    expect(wrapper.length).toEqual(1);

    const icon = wrapper.find(`.${styles.validationIcon}`);
    expect(icon.length).toEqual(1);
    expect(icon.hasClass('rad-icon-warning-dot')).toBe(false);
    expect(icon.hasClass(styles.visible)).toBe(false);
    expect(icon.hasClass(styles.error)).toBe(false);
    expect(icon.hasClass(styles.improvement)).toBe(false);
    expect(icon.hasClass('icon-no-edit')).toBe(false);

    expect(shallowWrapper.text()).toEqual('this is validation text');
  });

  it('should render correctly with an error', () => {
    render({
      validation: {
        type: ValidationType.VALIDATION_TYPE_ERROR
      }
    });
    const bar = shallowWrapper.find(`.${styles.globalValidationBar}`);
    expect(bar.length).toEqual(1);
    expect(bar.hasClass(styles.active)).toBe(true);
    expect(bar.hasClass(styles.error)).toBe(true);
    expect(bar.hasClass(styles.improvement)).toBe(false);
    expect(bar.hasClass(styles.permissionError)).toBe(false);

    const wrapper = bar.find(`.${styles.contentWrapper}`);
    expect(wrapper.length).toEqual(1);

    const icon = wrapper.find(`.${styles.validationIcon}`);
    expect(icon.length).toEqual(1);
    expect(icon.hasClass('rad-icon-warning-dot')).toBe(true);
    expect(icon.hasClass(styles.visible)).toBe(true);
    expect(icon.hasClass(styles.error)).toBe(true);
    expect(icon.hasClass(styles.improvement)).toBe(false);
    expect(icon.hasClass('icon-no-edit')).toBe(false);
  });

  it('should render correctly with an improvement validation issue', () => {
    render({
      validation: {
        type: ValidationType.VALIDATION_TYPE_IMPROVEMENT
      }
    });
    const bar = shallowWrapper.find(`.${styles.globalValidationBar}`);
    expect(bar.length).toEqual(1);
    expect(bar.hasClass(styles.active)).toBe(true);
    expect(bar.hasClass(styles.error)).toBe(false);
    expect(bar.hasClass(styles.improvement)).toBe(true);
    expect(bar.hasClass(styles.permissionError)).toBe(false);

    const wrapper = bar.find(`.${styles.contentWrapper}`);
    expect(wrapper.length).toEqual(1);

    const icon = wrapper.find(`.${styles.validationIcon}`);
    expect(icon.length).toEqual(1);
    expect(icon.hasClass('rad-icon-warning-dot')).toBe(false);
    expect(icon.hasClass(styles.visible)).toBe(true);
    expect(icon.hasClass(styles.error)).toBe(false);
    expect(icon.hasClass(styles.improvement)).toBe(true);
    expect(icon.hasClass('icon-no-edit')).toBe(false);
  });

  it('should render correctly with a permissions error', () => {
    render({
      validation: {
        type: ValidationType.VALIDATION_TYPE_EDIT_PERMISSION_ERROR
      }
    });
    const bar = shallowWrapper.find(`.${styles.globalValidationBar}`);
    expect(bar.length).toEqual(1);
    expect(bar.hasClass(styles.active)).toBe(true);
    expect(bar.hasClass(styles.error)).toBe(false);
    expect(bar.hasClass(styles.improvement)).toBe(false);
    expect(bar.hasClass(styles.permissionError)).toBe(true);

    const wrapper = bar.find(`.${styles.contentWrapper}`);
    expect(wrapper.length).toEqual(1);

    const icon = wrapper.find(`.${styles.validationIcon}`);
    expect(icon.length).toEqual(1);
    expect(icon.hasClass('rad-icon-warning-dot')).toBe(false);
    expect(icon.hasClass(styles.visible)).toBe(true);
    expect(icon.hasClass(styles.error)).toBe(false);
    expect(icon.hasClass(styles.improvement)).toBe(false);
    expect(icon.hasClass('icon-no-edit')).toBe(true);
  });
});
