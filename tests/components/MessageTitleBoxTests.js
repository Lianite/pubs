import React from 'react';
import { shallow } from 'enzyme';
import MessageTitleBox from 'components/MessageTitleBox';
import { TextInput } from '@spredfast/react-lib';

import styles from 'styles/modules/components/MessageTitleBox.scss';

describe('Message Title Box Component', () => {
  let shallowWrapper;

  const mainProps = {
    onChange: jasmine.createSpy('onChange'),
    messageTitle: 'test',
    currentPlanColor: 'blue',
    closeButtonClicked: () => true, //this will only be triggered on embedded conditions
    canUseMultichannelPubs: true //this only effects the styles of something that appears iff the app is embedded
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<MessageTitleBox {...props} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.onChange.calls.reset();
  });

  it('should render correctly', () => {
    const box = shallowWrapper.find(`.${styles.headerBar}`);
    expect(box.length).toEqual(1);

    const colorBar = shallowWrapper.findWhere((wrapper) => wrapper.prop('style') && wrapper.prop('style').backgroundColor === 'blue');
    expect(colorBar.length).toEqual(1);

    const input = shallowWrapper.find(TextInput);
    expect(input.props()).toEqual(jasmine.objectContaining({
      maxLength: 64,
      value: 'test',
      placeholder: 'Untitled Message'
    }));
  });

  it('should handle debouncedChange events correctly', () => {
    const input = shallowWrapper.find(TextInput);
    input.simulate('debouncedChange', { target: { value: 'a' } });

    expect(mainProps.onChange).toHaveBeenCalledWith('a');
  });
});
