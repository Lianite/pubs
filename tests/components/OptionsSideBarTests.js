import React from 'react';
import OptionsSideBar from 'components/OptionsSideBar';
import SidebarButton from 'components/SidebarButton';
import LinksContextMenu from 'components/optionsbar/LinksContextMenu';
import { SidebarPages } from 'constants/UIStateConstants';
import NotesMenuContainer from 'containers/optionsbar/NotesMenuContainer';
import FacebookMenuContainer from 'containers/optionsbar/networks/FacebookMenuContainer';
import { Set as ImmutableSet, Map as ImmutableMap } from 'immutable';
import { shallow } from 'enzyme';
import _ from 'underscore';

import styles from 'styles/modules/components/OptionsSideBar.scss';

let mockfunc = () => {
  return true;
};

let shallowWrapper;

describe('Options Sidebar Component', () => {

  const mainProps = {
    sidebarOpen: true,
    onSidebarTabClicked: mockfunc,
    onSidebarClosed:mockfunc,
    activePage: SidebarPages.BASICS,
    urls: ImmutableSet(['www.test.com']),
    activeNetworks: ['FACEBOOK', 'TWITTER'],
    addLinkPreview: () => {},
    selectedLinkPreviewUrl: 'www.test.com',
    linkPreviewUrls: ImmutableSet(['www.test.com']),
    linkTagsByLink: ImmutableMap(),
    linkTagsEnable: false,
    updateLinkTagVariable: () => {},
    canAddAttachmentByType: {linkPreview: true, images: true, video: true}
  };

  const notesProps = _.extend({}, mainProps, {activePage: SidebarPages.NOTES});

  const linksProps = {
    sidebarOpen: true,
    onSidebarTabClicked: mockfunc,
    onSidebarClosed:mockfunc,
    activePage: SidebarPages.LINKS,
    urls: ImmutableSet(['www.test.com']),
    activeNetworks: ['FACEBOOK', 'TWITTER'],
    urlProperties: ImmutableMap({
      'www.test.com': ImmutableMap({
        fullUrl: 'www.test.com',
        shortenOnPublish: true,
        addLinkTags: true
      })
    }),
    onChangeLinkProperty: mockfunc,
    addLinkPreview: () => {},
    selectedLinkPreviewUrl: 'www.test.com',
    linkPreviewUrls: ImmutableSet(['www.test.com']),
    canAddAttachmentByType: {linkPreview: true, images: true, video: true}
  };

  const facebookProps = {
    sidebarOpen: true,
    onSidebarTabClicked: mockfunc,
    onSidebarClosed:mockfunc,
    activePage: SidebarPages.FACEBOOK,
    urls: ImmutableSet(),
    activeNetworks: ['FACEBOOK', 'TWITTER'],
    urlProperties: ImmutableMap(),
    onChangeLinkProperty: mockfunc,
    addLinkPreview: () => {},
    selectedLinkPreviewUrl: 'www.test.com',
    linkPreviewUrls: ImmutableSet(['www.test.com']),
    canAddAttachmentByType: {linkPreview: true, images: true, video: true}
  };

  const twitterProps = {
    sidebarOpen: true,
    onSidebarTabClicked: mockfunc,
    onSidebarClosed:mockfunc,
    activePage: SidebarPages.TWITTER,
    urls: ImmutableSet(),
    activeNetworks: ['FACEBOOK', 'TWITTER'],
    urlProperties: ImmutableMap(),
    onChangeLinkProperty: mockfunc,
    addLinkPreview: () => {},
    selectedLinkPreviewUrl: 'www.test.com',
    linkPreviewUrls: ImmutableSet(['www.test.com']),
    canAddAttachmentByType: {linkPreview: true, images: true, video: true}
  };

  function render(props = {}) {
    const finalProps = _.extend({}, mainProps, props);
    shallowWrapper = shallow(<OptionsSideBar {...finalProps} />);
  }

  beforeEach(() => {
    spyOn(mainProps, 'onSidebarClosed');
    render(mainProps);
  });

  it('should not call method when sidebar button clicked and component is open', () => {
    const sidebarButton = shallowWrapper.find('button').at(1);
    sidebarButton.simulate('click');
    expect(mainProps.onSidebarClosed).not.toHaveBeenCalled();
  });

  it('should render the links context menu', () => {
    render(linksProps);
    const linksMenu = shallowWrapper.find(LinksContextMenu);
    expect(linksMenu.length).toEqual(1);
  });

  it('should render the facebook context menu', () => {
    render(facebookProps);
    const facebookMenu = shallowWrapper.find(FacebookMenuContainer);
    expect(facebookMenu.length).toEqual(1);

    const buttonList = shallowWrapper.find(`.${styles.optionButtonsContainer}`).dive();
    const buttons = buttonList.find(SidebarButton);
    const facebookButton = buttons.findWhere((wrapper) => wrapper.prop('buttonPageName') === 'Facebook');
    expect(facebookButton.prop('error')).not.toBe(true);
  });

  it('should render an error in the facebook menu button', () => {
    render({
      ...facebookProps,
      facebookHasError: true
    });
    const buttonList = shallowWrapper.find(`.${styles.optionButtonsContainer}`).dive();
    const buttons = buttonList.find(SidebarButton);
    const facebookButton = buttons.findWhere((wrapper) => wrapper.prop('buttonPageName') === 'Facebook');
    expect(facebookButton.prop('error')).toBe(true);
  });

  it('should render the twitter context menu', () => {
    render(twitterProps);
    const twitterMenu = shallowWrapper.find(`.${styles.comingSoonPlaceholder}`);
    expect(twitterMenu.length).toEqual(1);
  });

  it('should render the notes menu', () => {
    render(notesProps);
    expect(shallowWrapper.find(NotesMenuContainer).length).toEqual(1);
  });
});
