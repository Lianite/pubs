/* @flow */
/*global spyOn:true*/

import React from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import MessageEditor from 'components/MessageEditor';
import { EditorState } from 'draft-js';
import { Provider } from 'react-redux';
import createStore from 'createStore';
import { List as ImmutableList, Map as ImmutableMap } from 'immutable';
import { DefaultNetwork } from 'constants/ApplicationConstants';
// we need this store for testing, because the main component renders
// a smart container as a child.
const store = createStore();

const mainProps = {
  messageEditorStateMap: ImmutableMap({
    [DefaultNetwork]: EditorState.createEmpty()
  }),
  voiceDropdownToggle: () => true,
  selectedAccountList: ImmutableMap(),
  removeSelectedAccount: () => true,
  removeSelectedAccountsPerNetwork: () => undefined,
  resendValidAssigneesRequest: () => {
    return () => {};
  },
  updateFilterText: () => true,
  filterText: '',
  linkPreview: {
    linkPreviewInfo: {
      caption: 'www.spredfast.com',
      description: 'Spredfast is an enterprise social media marketing platform.',
      title: 'Spredfast',
      thumbnailUrl: '',
      currentlyScraping: false,
      linkUrl: 'www.spredfast.com',
      videoSrc: '',
      videoLink: '',
      removed: false
    },
    linkPreviewNetworks: [],
    linkPreviewLoading: false,
    enableCarousel: false,
    linkPreviewAlreadyUploadingMedia: false,
    linkPreviewCustomThumbError: undefined
  },
  updateExclusiveFilterChannel: () => undefined,
  updateFilterSelection: () => undefined,
  currentAccountChannelFilter: ImmutableList(),
  editLinkPreview: () => {},
  removeLinkPreview: () => {},
  videoUploadNetworks: [],
  canShowLinkPreview: false,
  canShowMediaBar: true,
  linkRegexString: '',
  removeAttachment: () => {},
  removeUploadedVideo: () => {},
  currentlyDraggingMedia: false,
  mediaStatus: {
    waiting: true
  },
  image: {
    waiting: true
  },
  video: {
    waiting: true
  },
  cancelImageUpload: () => {},
  cancelVideoUpload: () => {},
  cycleThumbnailForward: () => {},
  cycleThumbnailBackward: () => {},
  onCancelUpload: () => {},
  onAcknowledgeError: () => {},
  onScrapeCancel: () => {},
  onRefreshClicked: () => {},
  voiceSelectorOpen: false
};


const node = document.createElement('div');

function render(props = {}) {
  props = {...mainProps, ...props};
  return findDOMNode(
    ReactDOM.render(<Provider store={store}><MessageEditor {...props} /></Provider>, node)
  );
}

describe('Message Editor Component', () => {
  let messageEditorComponent;

  beforeEach(() => {
    messageEditorComponent = render();
  });

  it('should render AccountSelector', () => {
    const AccountSelector = messageEditorComponent.querySelector('.AccountSelector-accountSelectorBar');
    expect(AccountSelector).toBeTruthy();
  });

  it('should render a Message', () => {
    const Message = messageEditorComponent.querySelector('.Message-message');
    expect(Message).toBeTruthy();
  });

  it('should render MediaBar if there is no LinkPreview', () => {
    const MediaBar = messageEditorComponent.querySelector('.MediaBar-mediaBar');
    expect(MediaBar).toBeTruthy();

    const LinkPreview = messageEditorComponent.querySelector('.LinkPreview-linkPreview');
    expect(LinkPreview).toBeFalsy();
  });

  it('should render LinkPreview', () => {
    messageEditorComponent = render({
      canShowLinkPreview: true,
      canShowMediaBar: false
    });
    const MediaBar = messageEditorComponent.querySelector('.MediaBar-mediaBar');
    expect(MediaBar).toBeFalsy();

    const LinkPreview = messageEditorComponent.querySelector('.LinkPreview-linkPreview');
    expect(LinkPreview).toBeTruthy();
  });
});
