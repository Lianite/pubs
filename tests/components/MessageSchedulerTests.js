import React from 'react';
import { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import MessageScheduler from 'components/MessageScheduler';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import { createUtcMomentFromString,
  adjustUtcMomentTime,
  getDefaultScheduleTime } from 'utils/DateUtils';
import moment from 'moment';

// complete 100% clean re-render with fresh local state
function cleanRender(props = {}) {
  const component = ReactTestUtils.renderIntoDocument((
    <MessageScheduler {...props} />
  ));
  return findDOMNode(component);
}

describe('Scheduler Calendar Component', () => {
  let messageSchedulerComponent;
  let messageSchedulerProps;

  beforeEach(() => {
    const defaultMoment = moment('2050-01-15T10:00:00.000Z');
    messageSchedulerProps = {
      calendarCurrentDatetime: defaultMoment.toDate(),
      scheduledDatetime: null,
      userTimezone: new TimezoneRecord({
        abbreviation: 'CDT',
        name: 'American/Chicago',
        offset: -500
      }),
      loadingMessageHasCompleted: false,
      savingMessageHasCompleted: false,
      debounceWaitTime: 10,
      setScheduledTime: () => true
    };
    spyOn(messageSchedulerProps, 'setScheduledTime');
    messageSchedulerComponent = cleanRender(messageSchedulerProps);
  });

  it('should render combobox and calendar', () => {
    const calendar = messageSchedulerComponent.querySelector('.DatetimePicker-datePicker');
    const comboBox = messageSchedulerComponent.querySelector('.DatetimePicker-timePickerContainer');

    expect(ReactTestUtils.isDOMComponent(calendar)).toBeTruthy();
    expect(ReactTestUtils.isDOMComponent(comboBox)).toBeTruthy();
  });

  it('should render combobox and calendar', () => {
    const calendar = messageSchedulerComponent.querySelector('.DatetimePicker-datePicker');
    const comboBox = messageSchedulerComponent.querySelector('.DatetimePicker-timePickerContainer');

    expect(ReactTestUtils.isDOMComponent(calendar)).toBeTruthy();
    expect(ReactTestUtils.isDOMComponent(comboBox)).toBeTruthy();
  });

  it('changing the datetime should update the state', () => {
    const datetimeInput = messageSchedulerComponent.querySelector('.DatetimeInput-scheduleCalendarDateTimeInput');
    const utcDate = createUtcMomentFromString('04/06/2050, 12:00am');

    ReactTestUtils.Simulate.change(datetimeInput, {target: {value: '04/06/2050, 12:00am'}});
    ReactTestUtils.Simulate.change(datetimeInput, {target: {value: '04/06/1991, 12:00am'}});
    ReactTestUtils.Simulate.change(datetimeInput, {target: {value: ''}});
    ReactTestUtils.Simulate.change(datetimeInput, {target: {value: 'not a date, man'}});
    expect(messageSchedulerProps.setScheduledTime.calls.allArgs()).toDeepEqual([
      [{
        scheduledDatetime: utcDate,
        scheduledDatetimeHasError: false
      }],
      [{
        scheduledDatetime: createUtcMomentFromString('04/06/1991, 12:00am'),
        scheduledDatetimeHasError: true
      }],
      [{
        scheduledDatetime: null,
        scheduledDatetimeHasError: false
      }],
      [{
        scheduledDatetime: createUtcMomentFromString('not a date, man'),
        scheduledDatetimeHasError: true
      }]
    ]);
  });

  it('changing the calendar should update the state', () => {
    const dateToClick = messageSchedulerComponent.querySelector('.DatetimePicker-datePicker table tbody .rw-btn:not(.rw-off-range) .calendarDay:not(.outOfDate)').parentElement.parentElement;
    const dateToClickOutOfDate = messageSchedulerComponent.querySelector('.DatetimePicker-datePicker table tbody .rw-btn:not(.rw-off-range) .calendarDay.outOfDate').parentElement.parentElement;

    ReactTestUtils.Simulate.click(dateToClick);
    ReactTestUtils.Simulate.click(dateToClickOutOfDate);

    expect(messageSchedulerProps.setScheduledTime.calls.allArgs()).toDeepEqual([
      [{
        scheduledDatetime: getDefaultScheduleTime(messageSchedulerProps.userTimezone.offset, moment(messageSchedulerProps.calendarCurrentDatetime)),
        scheduledDatetimeHasError: false
      }]
    ]);
  });

  it('changing the timepicker should update the state', () => {
    const comboBoxInput = messageSchedulerComponent.querySelector('.DatetimePicker-timePickerContainer input');
    let popupTimeOptions;

    ReactTestUtils.Simulate.click(comboBoxInput);

    popupTimeOptions = messageSchedulerComponent.querySelectorAll('.rw-popup-container ul li');
    ReactTestUtils.Simulate.click(popupTimeOptions[0]);

    // unsure on how I would test the time changing here for valid, since it will depend on the time
    // per the users timezone...

    ReactTestUtils.Simulate.change(comboBoxInput, {target: {value: 'not a date, man'}});

    expect(messageSchedulerProps.setScheduledTime.calls.allArgs()).toDeepEqual([
      [{
        scheduledDatetime: adjustUtcMomentTime(messageSchedulerProps.scheduledDatetime, popupTimeOptions[0].textContent, messageSchedulerProps.userTimezone.offset, moment(messageSchedulerProps.calendarCurrentDatetime)),
        scheduledDatetimeHasError: true
      }],
      [{
        scheduledDatetime: adjustUtcMomentTime(messageSchedulerProps.scheduledDatetime, popupTimeOptions[0].textContent, messageSchedulerProps.userTimezone.offset, moment(messageSchedulerProps.calendarCurrentDatetime)),
        scheduledDatetimeHasError: true
      }],
      [{
        scheduledDatetime: adjustUtcMomentTime(messageSchedulerProps.scheduledDatetime, 'not a date, man', messageSchedulerProps.userTimezone.offset, moment(messageSchedulerProps.calendarCurrentDatetime)),
        scheduledDatetimeHasError: true
      }]
    ]);
  });
});
