import React, { Component } from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import MediaBar from 'components/MediaBar';

const Wrap = class Wrap extends Component {
  render() {
    return <MediaBar {...this.props} />;
  }
};

const node = document.createElement('div');

const mainProps = {
  currentlyDraggingMedia: false,
  networks: ['FACEBOOK'],
  onDrop: () => {},
  mediaStatus: {
    waiting: true,
    uploading: false,
    completed: false,
    error: false
  },
  removeAttachment: () => {},
  removeUploadedVideo: () => {},
  cancelImageUpload: () => {},
  cancelVideoUpload: () => {},
  videoUploadNetworks: ['FACEBOOK'],
  image: {
    waiting: true
  },
  video: {
    waiting: true
  }
};

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((<Wrap {...props} />), node);
}

function hasClass(el, classname) {
  return el.className.indexOf(classname) >= 0;
}

describe('Media Bar Component', () => {
  let mediaBarComponent;
  let mediaBarElement;

  beforeEach(() => {
    mediaBarComponent = render(mainProps);
    mediaBarElement = findDOMNode(mediaBarComponent);
  });

  it('should update container when is dragging', () => {
    expect(hasClass(mediaBarElement, 'dragging')).toBeFalsy();

    mediaBarComponent = render({
      currentlyDraggingMedia: true
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    expect(hasClass(mediaBarElement, 'dragging')).toBeTruthy();
  });

  it('should update media place holder depending of media status', () => {
    let mediaPlaceHolder = mediaBarElement.querySelector('.MediaBar-mediaPlaceholder');
    expect(hasClass(mediaPlaceHolder, 'waiting')).toBeTruthy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    mediaPlaceHolder = mediaBarElement.querySelector('.MediaBar-mediaPlaceholder');
    expect(hasClass(mediaPlaceHolder, 'working')).toBeTruthy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: false,
        completed: true,
        error: false
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    mediaPlaceHolder = mediaBarElement.querySelector('.MediaBar-mediaPlaceholder');
    expect(hasClass(mediaPlaceHolder, 'completed')).toBeTruthy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: false,
        completed: false,
        error: true
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    mediaPlaceHolder = mediaBarElement.querySelector('.MediaBar-mediaPlaceholder');
    expect(hasClass(mediaPlaceHolder, 'error')).toBeTruthy();

    expect(hasClass(mediaPlaceHolder, 'image')).toBeFalsy();
    expect(hasClass(mediaPlaceHolder, 'video')).toBeFalsy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      },
      image: {
        waiting: false,
        uploading: true
      },
      video: {
        waiting: false,
        uploading: true
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    mediaPlaceHolder = mediaBarElement.querySelector('.MediaBar-mediaPlaceholder');
    expect(hasClass(mediaPlaceHolder, 'image')).toBeTruthy();
    expect(hasClass(mediaPlaceHolder, 'video')).toBeTruthy();
  });

  it('should render place holder when media status is waiting', () => {
    let placeHolder = mediaBarElement.querySelector('.PlaceHolder-placeHolder');
    expect(placeHolder).toBeTruthy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    placeHolder = mediaBarElement.querySelector('.PlaceHolder-placeHolder');
    expect(placeHolder).toBeFalsy();
  });

  it('should update place holder text when is dragging', () => {
    let placeHolder = mediaBarElement.querySelector('.PlaceHolder-placeHolder');
    expect(placeHolder.querySelector('span').textContent).toEqual('Add Media from your ');

    mediaBarComponent = render({
      currentlyDraggingMedia: true
    });
    placeHolder = mediaBarElement.querySelector('.PlaceHolder-placeHolder');
    expect(placeHolder.querySelector('span').textContent).toEqual('Drop media files here');
  });

  it('should render image when its status is not waiting', () => {
    expect(mediaBarElement.querySelector('.Image-image')).toBeFalsy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      },
      image: {
        waiting: false,
        uploading: true
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    expect(mediaBarElement.querySelector('.Image-image')).toBeTruthy();
  });

  it('should call removeAttachment or cancelImageUpload when click over remove image button', () => {
    const onRemove = jasmine.createSpy('onRemove');
    let onCancel = jasmine.createSpy('onCancel');
    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      },
      image: {
        waiting: false,
        uploading: true
      },
      removeAttachment: onRemove,
      cancelImageUpload: onCancel
    });
    mediaBarElement = findDOMNode(mediaBarComponent);


    let closeButton = mediaBarElement.querySelector('.Image-mediaRemoveButton');
    expect(closeButton).toBeTruthy();
    ReactTestUtils.Simulate.click(closeButton);
    expect(onRemove).not.toHaveBeenCalled();
    expect(onCancel).toHaveBeenCalled();

    onCancel = jasmine.createSpy('onCancel');

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: false,
        completed: true,
        error: false
      },
      image: {
        waiting: false,
        uploading: false,
        completed: true
      },
      removeAttachment: onRemove,
      cancelImageUpload: onCancel
    });
    mediaBarElement = findDOMNode(mediaBarComponent);


    closeButton = mediaBarElement.querySelector('.Image-mediaRemoveButton');
    expect(closeButton).toBeTruthy();
    ReactTestUtils.Simulate.click(closeButton);
    expect(onRemove).toHaveBeenCalled();
    expect(onCancel).not.toHaveBeenCalled();
  });

  it('should render video when its status is not waiting', () => {
    expect(mediaBarElement.querySelector('.Video-video')).toBeFalsy();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      },
      video: {
        waiting: false,
        uploading: true
      }
    });
    mediaBarElement = findDOMNode(mediaBarComponent);
    expect(mediaBarElement.querySelector('.Video-video')).toBeTruthy();
  });

  it('should call removeUploadedVideo after click the remove button when the video is uploaded', () => {
    const onRemove = jasmine.createSpy('onRemove');
    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      },
      video: {
        waiting: false,
        uploading: true
      },
      removeUploadedVideo: onRemove
    });
    mediaBarElement = findDOMNode(mediaBarComponent);

    let closeButton = mediaBarElement.querySelector('.Video-closeButton');
    expect(closeButton).toBeTruthy();
    ReactTestUtils.Simulate.click(closeButton);
    expect(onRemove).not.toHaveBeenCalled();

    mediaBarComponent = render({
      mediaStatus: {
        waiting: false,
        uploading: true,
        completed: false,
        error: false
      },
      video: {
        waiting: false,
        uploading: false,
        completed: true
      },
      removeUploadedVideo: onRemove
    });
    mediaBarElement = findDOMNode(mediaBarComponent);

    closeButton = mediaBarElement.querySelector('.Video-closeButton');
    expect(closeButton).toBeTruthy();
    ReactTestUtils.Simulate.click(closeButton);
    expect(onRemove).toHaveBeenCalled();
  });
});
