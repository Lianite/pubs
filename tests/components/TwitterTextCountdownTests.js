import React from 'react';
import { shallow } from 'enzyme';
import TwitterTextCountdown from 'components/TwitterTextCountdown';

import styles from 'styles/modules/components/TwitterTextCountdown.scss';

describe('Twitter Text Countdown component', () => {

  let shallowWrapper;

  const mainProps = {
    text: 'this is a test'
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<TwitterTextCountdown {...props} />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const countdown = shallowWrapper.find(`.${styles.twitterTextCountdownContainer}`);
    expect(countdown.length).toEqual(1);
    expect(countdown.text()).toEqual('126');
    expect(countdown.hasClass(styles.warning)).toBe(false);
  });

  it('should render a warning correctly', () => {
    render({
      text: new Array(150).join()
    });
    const countdown = shallowWrapper.find(`.${styles.twitterTextCountdownContainer}`);
    expect(countdown.hasClass(styles.warning)).toBe(true);
  });
});
