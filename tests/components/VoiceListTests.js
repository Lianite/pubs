import React from 'react';
import { SupportedNetworks, VoiceFilteringConstants } from 'constants/ApplicationConstants';
import VoiceList, { VoiceListHeader } from 'components/VoiceList';
import { is, List as ImmutableList, Set as ImmutableSet } from 'immutable';
import InfiniteScroll from 'react-infinite';
import Credential from 'components/Credential';
import Voice from 'components/Voice';
import { OptionPicker } from 'components/optionsbar/LinkPreviewUrlPicker';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/VoiceList.scss';

let shallowWrapper;

describe('VoiceList component', () => {

  const mainProps = {
    voiceChecked: jasmine.createSpy('voiceChecked'),
    voices: [
      {
        id: 1,
        name: 'Test',
        credentials: [
          {
            id: 1,
            name: 'Test Cred',
            service: SupportedNetworks.FACEBOOK,
            targetingSupported: true,
            authenticated: true,
            organicVisibilitySupported: true
          },
          {
            id: 2,
            name: 'Test Cred 2',
            service: SupportedNetworks.TWITTER,
            targetingSupported: false,
            authenticated: true
          },
          {
            id: 3,
            name: 'Test Cred 3',
            service: SupportedNetworks.FACEBOOK,
            targetingSupported: false,
            authenticated: false,
            organicVisibilitySupported: false
          }
        ]
      }
    ],
    filteredAccountCount: 2,
    totalAccountCount: 2,
    checkedCount: 0,
    isOpen: true,
    quickSelectAllVoices: jasmine.createSpy('quickSelectAllVoices'),
    checkedVoices: {},
    searchOnVoices: false,
    currentAccountChannelFilter: ImmutableList(),
    currentAccountSelectionFilter: '',
    updateFilterChannels: jasmine.createSpy('updateFilterChannels'),
    updateFilterSelection: jasmine.createSpy('updateFilterSelection'),
    resendValidAssigneesRequest: jasmine.createSpy('resendValidAssigneesRequest'),
    searchOnVoicesToggle: jasmine.createSpy('searchOnVoicesToggle')
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<VoiceList {...props} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.voiceChecked.calls.reset();
    mainProps.quickSelectAllVoices.calls.reset();
    mainProps.updateFilterChannels.calls.reset();
    mainProps.updateFilterSelection.calls.reset();
    mainProps.resendValidAssigneesRequest.calls.reset();
    mainProps.searchOnVoicesToggle.calls.reset();
  });

  it('should render correctly', () => {
    const container = shallowWrapper.find(`.${styles.voiceListContainer}`);
    expect(container.length).toEqual(1);

    const header = container.find(VoiceListHeader);
    expect(header.length).toEqual(1);
    expect(header.props()).toEqual(jasmine.objectContaining({
      currentAccountChannelFilter: mainProps.currentAccountChannelFilter,
      currentAccountSelectionFilter: mainProps.currentAccountSelectionFilter,
      filteredAccountCount: mainProps.filteredAccountCount,
      totalAccountCount: mainProps.totalAccountCount,
      updateFilterChannels: mainProps.updateFilterChannels,
      updateFilterSelection: mainProps.updateFilterSelection,
      checkedCount: mainProps.checkedCount,
      searchOnVoicesToggle: mainProps.searchOnVoicesToggle,
      searchOnVoices: mainProps.searchOnVoices
    }));

    const scroll = container.find(InfiniteScroll);
    const voices = scroll.children().find(Voice);
    expect(voices.length).toEqual(1);
    const credentials = scroll.children().find(Credential);
    expect(credentials.length).toEqual(3);
  });

  it('should render correctly when closed', () => {
    render({
      isOpen: false
    });
    const container = shallowWrapper.find(`.${styles.voiceListContainer}`);
    expect(container.length).toEqual(0);
    expect(shallowWrapper.find(Voice).length).toEqual(0);
    expect(shallowWrapper.find(Credential).length).toEqual(0);
    expect(shallowWrapper.text()).toEqual('');
  });

  describe('Voice List Header', () => {

    describe('independent tests', () => {
      const mainHeaderProps = {
        filteredAccountCount: 2,
        totalAccountCount: 2,
        checkedCount: 1,
        quickSelectAllVoices: jasmine.createSpy('quickSelectAllVoices'),
        quickSelectAndFocusReset: jasmine.createSpy('quickSelectAndFocusReset'),
        toggleAdvancedOptions: jasmine.createSpy('toggleAdvancedOptions'),
        searchOnVoices: false,
        currentAccountChannelFilter: ImmutableList(),
        currentAccountSelectionFilter: '',
        advancedOptionsOpen: false,
        updateFilterChannels: jasmine.createSpy('updateFilterChannels'),
        updateFilterSelection: jasmine.createSpy('updateFilterSelection'),
        resendValidAssigneesRequest: jasmine.createSpy('resendValidAssigneesRequest'),
        searchOnVoicesToggle: jasmine.createSpy('searchOnVoicesToggle')
      };

      const renderHeader = (props = mainHeaderProps) => {
        props = {
          ...mainHeaderProps,
          ...props
        };
        shallowWrapper = shallow(<VoiceListHeader {...props} />);
      };

      beforeEach(() => {
        renderHeader();
      });

      afterEach(() => {
        mainHeaderProps.quickSelectAllVoices.calls.reset();
        mainHeaderProps.quickSelectAndFocusReset.calls.reset();
        mainHeaderProps.toggleAdvancedOptions.calls.reset();
        mainHeaderProps.updateFilterChannels.calls.reset();
        mainHeaderProps.updateFilterSelection.calls.reset();
        mainHeaderProps.resendValidAssigneesRequest.calls.reset();
        mainHeaderProps.searchOnVoicesToggle.calls.reset();
      });

      it('should render and behave correctly', () => {
        const filters = shallowWrapper.find(`.${styles.channelFilters}`);
        const channelButtons = filters.find('button');

        channelButtons.forEach((button) => {
          button.simulate('click');
        });
        expect(mainHeaderProps.updateFilterChannels).toHaveBeenCalledWith();
        expect(mainHeaderProps.updateFilterChannels).toHaveBeenCalledWith('TWITTER');
        expect(mainHeaderProps.updateFilterChannels).toHaveBeenCalledWith('FACEBOOK');

        const selection = shallowWrapper.find(`.${styles.selectionFilters}`);
        const selectionButtons = selection.find('button');

        selectionButtons.forEach((button) => {
          button.simulate('click');
        });

        expect(mainHeaderProps.updateFilterSelection).toHaveBeenCalledWith();
        expect(mainHeaderProps.updateFilterSelection).toHaveBeenCalledWith(VoiceFilteringConstants.FILTER_SELECTED);
        expect(mainHeaderProps.updateFilterSelection).toHaveBeenCalledWith(VoiceFilteringConstants.FILTER_UNSELECTED);

        const advanced = shallowWrapper.find(`.${styles.quickSelectAndAdvancedOptions}`);
        const advancedButtons = advanced.find('button');

        advancedButtons.forEach((button) => {
          button.simulate('click');
        });

        expect(mainHeaderProps.quickSelectAndFocusReset).toHaveBeenCalledWith(true);
        expect(mainHeaderProps.quickSelectAndFocusReset).toHaveBeenCalledWith(false);

        shallowWrapper.find('button').last().simulate('click');
        expect(mainHeaderProps.toggleAdvancedOptions).toHaveBeenCalledWith(false);
      });

      it('should have all buttons enabled', () => {
        shallowWrapper.find('button').forEach((button) => {
          expect(button.prop('disabled')).toBeFalsy();
        });
      });

      it('should disable buttons correctly', () => {
        renderHeader({
          checkedCount: 2,
          filteredAccountCount: 2
        });

        expect(shallowWrapper.find(`.${styles.quickSelectAndAdvancedOptions} button`).at(0).prop('disabled')).toBe(true);

        renderHeader({
          checkedCount: 0
        });

        expect(shallowWrapper.find(`.${styles.quickSelectAndAdvancedOptions} button`).at(1).prop('disabled')).toBe(true);
      });

      describe('advanced options controls', () => {
        it('should render the correct text', () => {
          renderHeader({
            advancedOptionsOpen: false
          });

          expect(shallowWrapper.text()).toContain('Show');
          expect(shallowWrapper.text()).toContain('Account & Search Options');

          renderHeader({
            advancedOptionsOpen: true
          });

          expect(shallowWrapper.text()).toContain('Hide');
          expect(shallowWrapper.text()).toContain('Account & Search Options');
        });

        it('should pass the correct props to the options picker', () => {
          renderHeader({
            advancedOptionsOpen: true
          });
          let options = shallowWrapper.find(OptionPicker);

          expect(is(options.prop('options'), ImmutableSet(['Search Account Name', 'Search Account Set Name']))).toBe(true);
          expect(options.prop('selectedOption')).toEqual('Search Account Name');

          renderHeader({
            searchOnVoices: true,
            advancedOptionsOpen: true
          });

          options = shallowWrapper.find(OptionPicker);
          expect(options.prop('selectedOption')).toEqual('Search Account Set Name');

          options.simulate('select', 'example');
          expect(mainHeaderProps.searchOnVoicesToggle).toHaveBeenCalledWith('example');
        });
      });
    });

    describe('property tests', () => {
      beforeEach(() => {
        shallowWrapper.instance().refs = {
          filteredAccountCount: {
            focus: () => {}
          }
        };
      });
      it('should call the appropriate props on quickSelectAndFocusReset', () => {
        const header = shallowWrapper.find(VoiceListHeader);
        header.prop('quickSelectAndFocusReset')(true);
        expect(mainProps.quickSelectAllVoices).toHaveBeenCalledWith(true);
      });

      it('should toggle advanced options correctly', () => {
        let header = shallowWrapper.find(VoiceListHeader);
        expect(header.prop('advancedOptionsOpen')).toEqual(false);

        header.prop('toggleAdvancedOptions')(false);

        header = shallowWrapper.find(VoiceListHeader);
        expect(header.prop('advancedOptionsOpen')).toEqual(true);
      });
    });
  });

  describe('disabling credentials', () => {
    it('should enable all facebook accounts when no facebook targeting data has been set', () => {
      render({
        ...mainProps,
        facebookTargetingSet: false
      });

      const credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(2);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);
    });

    it('should disable facebook appropriately when facebook targeting data has been set', () => {
      render({
        ...mainProps,
        facebookTargetingSet: true,
        darkPostStatus: false
      });
      let credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabled') &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Facebook Targeting.'
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);

      render({
        ...mainProps,
        voices: [{
          ...mainProps.voices[0],
          credentials: [
            ...mainProps.voices[0].credentials, {
              id: 4,
              name: 'Test  cred 4',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: false,
              authenticated: true,
              organicVisibilitySupported: true
            }, {
              id: 5,
              name: 'Test  cred 5',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: true,
              authenticated: false,
              organicVisibilitySupported: true
            }
          ]
        }],
        facebookTargetingSet: true
      });
      credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabled')
      )).length).toEqual(3);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Facebook Targeting.'
      )).length).toEqual(3);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);
    });


    it('should enable all facebook accounts when no facebook dark post data has been set', () => {
      render({
        ...mainProps,
        darkPostStatus: false
      });

      const credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(2);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);
    });

    it('should disable facebook appropriately when facebook dark post data has been set', () => {
      render({
        ...mainProps,
        darkPostStatus: true
      });
      let credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabled') &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Dark Posting.'
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);

      render({
        ...mainProps,
        voices: [{
          ...mainProps.voices[0],
          credentials: [
            ...mainProps.voices[0].credentials, {
              id: 4,
              name: 'Test  cred 4',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: true,
              authenticated: true,
              organicVisibilitySupported: true
            }, {
              id: 5,
              name: 'Test  cred 5',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: true,
              authenticated: false,
              organicVisibilitySupported: true
            }
          ]
        }],
        darkPostStatus: true
      });
      credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabled')
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Dark Posting.'
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(3);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);
    });

    it('should disable facebook appropriately when facebook dark post and targeting data has been set', () => {
      render({
        ...mainProps,
        darkPostStatus: true
      });
      let credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabled') &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Dark Posting.'
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);

      render({
        ...mainProps,
        voices: [{
          ...mainProps.voices[0],
          credentials: [
            ...mainProps.voices[0].credentials, {
              id: 4,
              name: 'Test  cred 4',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: true,
              authenticated: true,
              organicVisibilitySupported: false
            }, {
              id: 5,
              name: 'Test  cred 5',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: false,
              authenticated: true,
              organicVisibilitySupported: false
            },
            {
              id: 6,
              name: 'Test Cred 6',
              service: SupportedNetworks.FACEBOOK,
              targetingSupported: false,
              authenticated: true,
              organicVisibilitySupported: true
            }
          ]
        }],
        darkPostStatus: true,
        facebookTargetingSet: true
      });
      credentials = shallowWrapper.find(Credential);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabled')
      )).length).toEqual(4);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Dark Posting.'
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Facebook Targeting.'
      )).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => (
        credentialWrapper.prop('service') === 'FACEBOOK' &&
        credentialWrapper.prop('disabledMessage') === 'Account does not support Facebook Targeting and Dark Posting.'
      )).length).toEqual(2);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'FACEBOOK' && !credentialWrapper.prop('disabled')).length).toEqual(1);
      expect(credentials.findWhere((credentialWrapper) => credentialWrapper.prop('service') === 'TWITTER' && !credentialWrapper.prop('disabled')).length).toEqual(1);
    });
  });
});
