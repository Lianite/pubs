/*global spyOn:true*/

import React from 'react';
import { shallow } from 'enzyme';
import SelectedAccount from 'components/SelectedAccount';
import { List as ImmutableList } from 'immutable';
import { CredentialRecord } from 'records/AccountsRecord';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import RADIcon from '@spredfast/react-lib/lib/RADIcon';

import styles from 'styles/modules/components/SelectedAccount.scss';

let mockfunc = () => {
  return true;
};

describe('SelectedAccount Component', () => {
  let shallowWrapper;
  let props;
  let rollupProps;

  const render = function(overrideProps = props) {
    const propsToUse = {
      ...props,
      ...overrideProps
    };
    shallowWrapper = shallow(<SelectedAccount {...propsToUse} />);
  };

  beforeEach(() => {

    props = {
      remove: mockfunc,
      resendValidAssigneesRequest: mockfunc,
      accountName: 'Test',
      service: 'FACEBOOK',
      accountId: 1,
      authenticated: true
    };

    rollupProps = {
      removeSelectedAccountsPerNetwork: mockfunc,
      remove: mockfunc,
      accountName: '5 accounts',
      service: 'FACEBOOK',
      accountId: 1,
      credentials: [
        new CredentialRecord({
          id: 1,
          name: 'Spredfast Testin 1',
          socialNetwork: SupportedNetworks.FACEBOOK,
          authenticated: true
        }),
        new CredentialRecord({
          id: 2,
          name: 'Spredfast Testin 2',
          socialNetwork: SupportedNetworks.FACEBOOK,
          authenticated: true
        }),
        new CredentialRecord({
          id: 3,
          name: 'Spredfast Testin 3',
          socialNetwork: SupportedNetworks.FACEBOOK,
          authenticated: true
        }),
        new CredentialRecord({
          id: 4,
          name: 'Spredfast Testin 4',
          socialNetwork: SupportedNetworks.FACEBOOK,
          authenticated: true
        }),
        new CredentialRecord({
          id: 5,
          name: 'Spredfast Testin 5',
          socialNetwork: SupportedNetworks.FACEBOOK,
          authenticated: true
        })
      ],
      authenticated: true,
      updateExclusiveFilterChannel: mockfunc,
      updateFilterSelection: mockfunc,
      resendValidAssigneesRequest: mockfunc,
      currentAccountChannelFilter: ImmutableList(),
      voiceSelectorOpen: false,
      openAccountSelector: mockfunc
    };

    spyOn(props, 'remove');

    spyOn(rollupProps, 'removeSelectedAccountsPerNetwork');
    spyOn(rollupProps, 'updateExclusiveFilterChannel');
    spyOn(rollupProps, 'updateFilterSelection');
    spyOn(rollupProps, 'openAccountSelector');

    shallowWrapper = shallow(<SelectedAccount {...props} />);
  });

  // a basic test for looking at outputted elements in the DOM and verifing the results
  it('should render standard account button', () => {
    const accountName = shallowWrapper.find(`.${styles.accountName}`).text();
    expect(accountName).toBe(props.accountName);
  });

  it('should call the closing on click handler upon clicking the close link', () => {
    shallowWrapper.find(`.${styles.closeButton}`).simulate('click', new Event('click'));
    expect(props.remove).toHaveBeenCalled();
  });

  it('should render a tooltip for a rolled up account selector', () => {
    render(rollupProps);

    const tooltip = shallowWrapper.find(Tooltip);
    expect(typeof tooltip.prop('overlay')).toEqual('object');
  });

  it('should change filters when a rolled account button is clicked', () => {
    let rolledAccountComponent = shallow(<SelectedAccount {...rollupProps} />);
    const selectedAccount = rolledAccountComponent.find(`.${styles.selectedAccount}`);

    expect(rollupProps.updateExclusiveFilterChannel).not.toHaveBeenCalled();
    expect(rollupProps.updateFilterSelection).not.toHaveBeenCalled();

    selectedAccount.simulate('click');

    expect(rollupProps.updateExclusiveFilterChannel).toHaveBeenCalled();
    expect(rollupProps.updateFilterSelection).toHaveBeenCalled();
  });

  it('should remove accounts, but not change filters when removing an account rollup', () => {
    let rolledAccountComponent = shallow(<SelectedAccount {...rollupProps} />);
    const removeButton = rolledAccountComponent.find(`.${styles.closeButton}`);

    expect(rollupProps.removeSelectedAccountsPerNetwork).not.toHaveBeenCalled();

    removeButton.simulate('click', new Event('click'));

    expect(rollupProps.updateExclusiveFilterChannel).not.toHaveBeenCalled();
    expect(rollupProps.updateFilterSelection).not.toHaveBeenCalled();
    expect(rollupProps.removeSelectedAccountsPerNetwork).toHaveBeenCalled();
  });

  it('should render correctly when an account is authenticated and we are not rolled up', () => {
    const tooltip = shallowWrapper.find(Tooltip);
    expect(tooltip
      .findWhere(wrapper => {
        wrapper.children().find(RADIcon)
        .findWhere(icon => icon.prop('className') === styles.authenticationWarning);
      }).length).toEqual(0);
    expect(tooltip.prop('visible')).toEqual(false);
  });

  it('should render correctly when all accounts are authenticated and we are rolled up', () => {
    render({
      ...rollupProps,
      authenticated: undefined
    });
    shallowWrapper.find(`.${styles.selectedAccount}`).simulate('mouseEnter');
    const tooltip = shallowWrapper.find(Tooltip);
    expect(tooltip.length).toEqual(1);
    expect(tooltip.prop('visible')).toEqual(undefined);
    expect(tooltip.findWhere(wrapper => {
      return wrapper.type() === RADIcon && wrapper.prop('className') === styles.authenticationWarning;
    }).length).toEqual(1);

    const overlay = tooltip.prop('overlay');
    expect(overlay.props.children[1].props.children[0].props.children).toEqual('Spredfast Testin 1');
    expect(overlay.props.children[1].props.children[1].props.children).toEqual('Spredfast Testin 2');
  });

  it('should render correctly when an account is not authenticated and we are not rolled up', () => {
    render({
      ...props,
      authenticated: false
    });
    const tooltip = shallowWrapper.find(Tooltip);
    expect(tooltip.length).toEqual(1);
    expect(tooltip.prop('visible')).toEqual(undefined);
    expect(tooltip.findWhere(wrapper => {
      return wrapper.type() === RADIcon && wrapper.prop('className') === styles.authenticationWarning;
    }).length).toEqual(1);
  });

  it('should render correctly when an account is not authenticated and we are rolled up', () => {
    rollupProps.credentials[0] = rollupProps.credentials[0].set('authenticated', false);
    render({
      ...rollupProps,
      authenticated: undefined
    });
    shallowWrapper.find(`.${styles.selectedAccount}`).simulate('mouseEnter');
    const tooltip = shallowWrapper.find(Tooltip);
    expect(tooltip.length).toEqual(1);
    expect(tooltip.prop('visible')).toEqual(undefined);
    expect(tooltip.findWhere(wrapper => {
      return wrapper.type() === RADIcon && wrapper.prop('className') === styles.authenticationWarning;
    }).length).toEqual(1);

    const overlay = tooltip.prop('overlay');
    expect(overlay.props.children[1].props.children[0].props.children).toEqual('Spredfast Testin 1 (not authenticated)');
    expect(overlay.props.children[1].props.children[1].props.children).toEqual('Spredfast Testin 2');
  });
});
