import React from 'react';
import AccountSelector from 'components/AccountSelector';
import SelectedAccount from 'components/SelectedAccount';
import VoiceListContainer from 'containers/VoiceListContainer';
import { TextInput } from '@spredfast/react-lib';
import { Map as ImmutableMap } from 'immutable';
import { CredentialRecord } from 'records/AccountsRecord';
import { shallow } from 'enzyme';

let shallowWrapper;

const mockfunc = () => {
  return true;
};

const mainProps = {
  onClick: jasmine.createSpy('onClick'),
  removeSelectedAccount: mockfunc,
  removeSelectedAccountsPerNetwork: mockfunc,
  accounts: ImmutableMap({
    '1': new CredentialRecord({
      id: 1,
      uniqueId: 'unique1',
      name: 'Test',
      socialNetwork: 'FACEBOOK'
    }),
    '2': new CredentialRecord({
      id: 2,
      uniqueId: 'unique2',
      name: 'Example',
      socialNetwork: 'TWITTER'
    })
  }),
  updateFilterText: jasmine.createSpy('onClick'),
  updateFilterSelection: mockfunc,
  resendValidAssigneesRequest: mockfunc,
  filterText: 'test',
  voices: ['asd'],
  voiceSelectorOpen: false,
  accountSelectorHeight: 10,
  currentAccountChannelFilter: 'test'
};

const render = (props = mainProps) => {
  props = {
    ...mainProps,
    ...props
  };
  shallowWrapper = shallow(<AccountSelector {...props} />);
};

describe('Account Selector Component', () => {

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.onClick.calls.reset();
    mainProps.updateFilterText.calls.reset();
  });

  it('should render correctly', () => {
    const textInput = shallowWrapper.find(TextInput);
    expect(textInput.length).toEqual(1);
    expect(textInput.prop('placeholder')).toEqual('Add accounts... ');
    expect(textInput.prop('value')).toEqual('test');

    const voiceList = shallowWrapper.find(VoiceListContainer);
    expect(voiceList.length).toEqual(1);
    expect(voiceList.prop('accountSelectorHeight')).toEqual(10);
  });

  it('should call onClick with the appropriate argument on focus', () => {
    const textInput = shallowWrapper.find(TextInput);
    textInput.simulate('focus');
    expect(mainProps.onClick).toHaveBeenCalledWith('focus');
  });

  it('should call updateFilterText and onClick when the text input is clicked and the selector is closed', () => {
    const textInput = shallowWrapper.find(TextInput);
    textInput.simulate('debouncedChange', { target: {} });
    expect(mainProps.onClick).toHaveBeenCalled();
    expect(mainProps.updateFilterText).toHaveBeenCalled();
  });

  it('should call updateFilterText and not call onClick when the text input is clicked and the selector is open', () => {
    render({
      voiceSelectorOpen: true
    });
    const textInput = shallowWrapper.find(TextInput);
    textInput.simulate('debouncedChange', { target: {} });
    expect(mainProps.onClick).not.toHaveBeenCalled();
    expect(mainProps.updateFilterText).toHaveBeenCalled();
  });

  it('should display individual accounts when there is enough width to display them', () => {
    render({
      accountSelectorWidth: 1000
    });
    const accounts = shallowWrapper.find(SelectedAccount);
    accounts.forEach((account) => {
      expect(account.prop('credentials')).toBeFalsy();
      expect(accounts.at(0).prop('remove')).toEqual(mainProps.removeSelectedAccount);
      expect(accounts.at(0).prop('resendValidAssigneesRequest')).toEqual(mainProps.resendValidAssigneesRequest);
    });

    expect(accounts.at(0).prop('accountName')).toEqual('Test');
    expect(accounts.at(0).prop('service')).toEqual('FACEBOOK');
    expect(accounts.at(0).prop('accountId')).toEqual('unique1');

    expect(accounts.at(1).prop('accountName')).toEqual('Example');
    expect(accounts.at(1).prop('service')).toEqual('TWITTER');
    expect(accounts.at(1).prop('accountId')).toEqual('unique2');
  });

  it('should display rolled up accounts when there is enough width to display them', () => {
    render({
      accountSelectorWidth: 10
    });
    const accounts = shallowWrapper.find(SelectedAccount);
    accounts.forEach((account => {
      expect(account.prop('accountName')).toEqual('1 account');
      expect(account.prop('removeSelectedAccountsPerNetwork')).toEqual(mainProps.removeSelectedAccountsPerNetwork);
      expect(account.prop('currentAccountChannelFilter')).toEqual('test');
      expect(account.prop('resendValidAssigneesRequest')).toEqual(mainProps.resendValidAssigneesRequest);
      expect(account.prop('openAccountSelector')).toEqual(mainProps.onClick);
      expect(account.prop('voiceSelectorOpen')).toEqual(false);
    }));

    expect(accounts.at(0).prop('credentials')).toDeepEqual([mainProps.accounts.get('1')]);
    expect(accounts.at(1).prop('credentials')).toDeepEqual([mainProps.accounts.get('2')]);
  });
  it('should specify that a selected account is authenticated when its credential is authenticated', () => {
    render({
      accounts: ImmutableMap({
        '1': new CredentialRecord({
          id: 1,
          authenticated: true,
          name: 'Test',
          socialNetwork: 'FACEBOOK'
        })
      })
    });
    const account = shallowWrapper.find(SelectedAccount);
    expect(account.prop('credentials')).toBeFalsy();
    expect(account.prop('authenticated')).toEqual(true);
  });

  it('should specify that rolled up accounts are authenticated when all credentials are authenticated', () => {
    render({
      accounts: ImmutableMap({
        '1': new CredentialRecord({
          id: 1,
          name: 'Test',
          socialNetwork: 'FACEBOOK',
          authenticated: true
        }),
        '2': new CredentialRecord({
          id: 2,
          name: 'Test',
          socialNetwork: 'FACEBOOK',
          authenticated: true
        })
      }),
      accountSelectorWidth: 1
    });
    const rolledUpAccounts = shallowWrapper.find(SelectedAccount);
    expect(rolledUpAccounts.prop('credentials').length).toEqual(2);
    expect(rolledUpAccounts.prop('authenticated')).toEqual(true);
  });

  it('should specify that rolled up accounts are authenticated when all credentials are authenticated', () => {
    render({
      accounts: ImmutableMap({
        '1': new CredentialRecord({
          id: 1,
          name: 'Test',
          socialNetwork: 'FACEBOOK',
          authenticated: true
        }),
        '2': new CredentialRecord({
          id: 2,
          name: 'Test',
          socialNetwork: 'FACEBOOK',
          authenticated: false
        })
      }),
      accountSelectorWidth: 1
    });
    const rolledUpAccounts = shallowWrapper.find(SelectedAccount);
    expect(rolledUpAccounts.prop('credentials').length).toEqual(2);
    expect(rolledUpAccounts.prop('authenticated')).toEqual(false);
  });
});
