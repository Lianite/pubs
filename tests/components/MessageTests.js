import React from 'react';
import { shallow } from 'enzyme';
import Message from 'components/Message';
import {
    Editor,
    EditorState,
    ContentState
} from 'draft-js';
import { ValidationType } from 'constants/UIStateConstants';
import TwitterTextCountdown from 'components/TwitterTextCountdown';
import MessageBoxHeaderContainer from 'containers/MessageBoxHeaderContainer';
import NetworkOptionsBar from 'components/message/NetworkOptionsBar';
import MentionsQuery from 'components/MentionsQuery';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import { List as ImmutableList } from 'immutable';
import { MentionsActionConstants } from 'constants/ActionConstants';

let mockfunc = () => {
  return true;
};

describe('Message Component', () => {
  let shallowWrapper;

  const mainProps = {
    messageEditorState: EditorState.moveFocusToEnd(EditorState.createEmpty()),
    onChange: jasmine.createSpy('onChange'),
    onBlur: jasmine.createSpy('onBlur'),
    network: SupportedNetworks.FACEBOOK,
    applyToAllChannels: jasmine.createSpy('applyToAllChannels'),
    updateMessageDebounceTime: 500,
    messageEditorDefaultPlaceholder: 'TEST',
    numberOfNetworks: 2,
    validation: {
      type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
      validationText:'',
      errorsPerNetwork: {}
    },
    mentionsQueryList: ImmutableList([]),
    loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_RECEIVED,
    closeMentionsQueryDialog: jasmine.createSpy('closeMentionsQueryDialog'),
    getMentionsQueryUIState: false,
    currentMentionOffset: '2'
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<Message {...props} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.onBlur.calls.reset();
    mainProps.closeMentionsQueryDialog.calls.reset();
    mainProps.onChange.calls.reset();
    mainProps.applyToAllChannels.calls.reset();
  });

  it('should render correctly', () => {
    const header = shallowWrapper.find(MessageBoxHeaderContainer);
    expect(header.length).toEqual(1);
    expect(header.props()).toEqual(jasmine.objectContaining({
      numberOfNetworks: 2,
      network: 'FACEBOOK',
      disableApplyToAll: true
    }));
    expect(header.prop('validation')).toEqual(mainProps.validation);

    const editor = shallowWrapper.find(Editor);
    expect(editor.length).toEqual(1);
    expect(editor.prop('editorState')).toEqual(mainProps.messageEditorState);
    expect(editor.prop('placeholder')).toEqual('TEST');
    expect(editor.props()).toEqual(jasmine.objectContaining({
      spellCheck: true,
      stripPastedStyles: true
    }));

    const mentions = shallowWrapper.find(MentionsQuery);
    expect(mentions.length).toEqual(1);
    expect(mentions.props()).toEqual(jasmine.objectContaining({
      loadMentionsQueryStatus: MentionsActionConstants.MENTIONS_RECEIVED,
      network: 'FACEBOOK',
      mentionsQueryList: mainProps.mentionsQueryList,
      sendUpdatedEditorState: mainProps.onChange,
      getMentionsQueryUiState: false,
      currentMentionOffset: '2',
      closeMentionsQueryDialog: mainProps.closeMentionsQueryDialog
    }));

    expect(shallowWrapper.find(TwitterTextCountdown).length).toEqual(0);

    const bar = shallowWrapper.find(NetworkOptionsBar);
    expect(bar.length).toEqual(0);
  });

  it('should render a twitter countdown when necessary', () => {
    render({
      network: 'TWITTER',
      messageEditorState: EditorState.createWithContent(ContentState.createFromText('this is a test'))
    });
    const countdown = shallowWrapper.find(TwitterTextCountdown);
    expect(countdown.length).toEqual(1);
    expect(countdown.prop('text')).toEqual('this is a test');
  });

  describe('Message Header', () => {
    it('should handle applyToAllChannels events correctly', () => {
      const header = shallowWrapper.find(MessageBoxHeaderContainer);
      header.prop('applyToAllChannels')();

      expect(mainProps.applyToAllChannels).toHaveBeenCalled();
    });
  });

  describe('Network Options Bar', () => {
    it('should render when a facebook video has been requested', () => {
      render({
        hasVideoBeenRequested: true,
        customVideoTitle: 'custom',
        setCustomVideoTitle: mockfunc
      });

      const bar = shallowWrapper.find(NetworkOptionsBar);
      expect(bar.length).toEqual(1);
      expect(bar.props()).toDeepEqual({
        customVideoTitle: 'custom',
        setCustomVideoTitle: mockfunc
      });
    });

    it('should not render when a twitter video has been requested', () => {
      render({
        network: 'TWITTER',
        hasVideoBeenRequested: true,
        customVideoTitle: 'custom',
        setCustomVideoTitle: mockfunc
      });

      const bar = shallowWrapper.find(NetworkOptionsBar);
      expect(bar.length).toEqual(0);
    });
  });

  describe('Mentions Query', () => {
    it('should handle blur events appropriately', () => {
      const mentions = shallowWrapper.find(MentionsQuery);
      mentions.simulate('blur', {});

      expect(mainProps.onBlur).toHaveBeenCalled();
    });

    it('should handle keyUp events appropriately', () => {
      const mentions = shallowWrapper.find(MentionsQuery);
      mentions.simulate('keyUp', { keyCode: 27 });

      expect(mainProps.closeMentionsQueryDialog).toHaveBeenCalledWith('FACEBOOK');

      mainProps.closeMentionsQueryDialog.calls.reset();

      mentions.simulate('keyUp', { keyCode: 11 });

      expect(mainProps.closeMentionsQueryDialog).not.toHaveBeenCalled();
    });
  });

  describe('Editor', () => {
    it('should handle blur events appropriately', () => {
      const editor = shallowWrapper.find(Editor);
      editor.simulate('blur', {});

      expect(mainProps.closeMentionsQueryDialog).toHaveBeenCalledWith('FACEBOOK');
      expect(mainProps.onBlur).toHaveBeenCalled();
    });

    it('should handle change events appropriately', () => {
      const editor = shallowWrapper.find(Editor);
      editor.simulate('change');

      expect(mainProps.onChange).toHaveBeenCalled();
    });

    it('should handle keyUp events appropriately', () => {
      const editor = shallowWrapper.find(Editor);
      editor.simulate('keyUp', { keyCode: 27 });

      expect(mainProps.closeMentionsQueryDialog).toHaveBeenCalledWith('FACEBOOK');

      mainProps.closeMentionsQueryDialog.calls.reset();

      editor.simulate('keyUp', { keyCode: 11 });

      expect(mainProps.closeMentionsQueryDialog).not.toHaveBeenCalled();
    });
  });
});
