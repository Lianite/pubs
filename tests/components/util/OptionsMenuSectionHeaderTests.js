import React from 'react';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import { mount } from 'enzyme';

import styles from 'styles/modules/components/util/OptionsMenuSectionHeader.scss';

let mountedWrapper;

const mainProps = {
  title: 'test title',
  children: <span>test</span>
};

function render(props = {}) {
  props = {...mainProps, ...props};
  mountedWrapper = mount(<OptionsMenuSectionHeader {...props}>{props.children}</OptionsMenuSectionHeader>);
}

describe('Options Menu Section Header Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const wrapper = mountedWrapper.find(`.${styles.wrapper}`);
    expect(wrapper.length).toEqual(1);
    expect(wrapper.hasClass(styles.subheading)).toEqual(false);

    const content = wrapper.children();

    expect(content.text()).toEqual('test test title');
  });

  it('should set the correct class for a subheading', () => {
    render({
      subheading: true
    });
    expect(mountedWrapper.find(`.${styles.wrapper}.${styles.subheading}`).length).toEqual(1);
  });

  it('should render the icon to the left', () => {
    render({
      iconRight: true
    });
    expect(mountedWrapper.text()).toEqual('test title test');
  });

  it('should pass the style prop to the wrapper', () => {
    render({
      style: { test :123 }
    });
    expect(mountedWrapper.find(`.${styles.wrapper}`).prop('style')).toDeepEqual({
      test: 123
    });
  });
});
