/* @flow */
import React from 'react';
import DropdownButton from 'components/util/DropdownButton';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/util/DropdownButton.scss';

describe('Dropdown Button Component', () => {

  let shallowWrapper;

  const mainProps = {
    onClick: () => {},
    onBlur: () => {},
    open: false
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<DropdownButton {...props} />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly when closed', () => {
    const button = shallowWrapper.find(`.${styles.button}`);
    expect(button.length).toEqual(1);
    expect(button.props()).toEqual(jasmine.objectContaining({
      onClick: mainProps.onClick,
      onBlur: mainProps.onBlur
    }));

    const icon = shallowWrapper.find('i');
    expect(icon.hasClass('icon-caret-down')).toBe(true);
  });

  it('should render correctly when open', () => {
    render({
      open: true
    });
    const button = shallowWrapper.find(`.${styles.button}`);
    expect(button.length).toEqual(1);
    expect(button.props()).toEqual(jasmine.objectContaining({
      onClick: mainProps.onClick,
      onBlur: mainProps.onBlur
    }));

    const icon = shallowWrapper.find('i');
    expect(icon.hasClass('icon-caret-up')).toBe(true);
  });
});
