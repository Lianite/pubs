/* @flow */
import React from 'react';
import {DisabledAccountsList} from 'components/util/DisabledAccountsList';
import { shallow } from 'enzyme';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import { CredentialRecord } from 'records/AccountsRecord';

describe('Invalid Targeting Accounts component', () => {
  let shallowInvalidAccountsWrapper;
  const mainInvalidAccountsProps = {
    invalidAccounts: [
      new CredentialRecord({
        name: 'planet express'
      }),
      new CredentialRecord({
        name: 'fry'
      })
    ]
  };
  const renderInvalidAccounts = (props = mainInvalidAccountsProps) => {
    const propsToUse = {
      ...mainInvalidAccountsProps,
      ...props
    };
    shallowInvalidAccountsWrapper = shallow(<DisabledAccountsList {...propsToUse} />);
  };

  beforeEach(() => {
    renderInvalidAccounts();
  });

  it('should render multiple invalid targeting accounts correctly', () => {
    const tooltip = shallowInvalidAccountsWrapper.find(Tooltip);
    expect(tooltip.children().text()).toEqual('View Unsupported Accounts');
    const overlay = shallow(tooltip.prop('overlay'));
    expect(overlay.text()).toEqual('planet expressfry');
  });

  it('should render one invalid targeting account correctly', () => {
    renderInvalidAccounts({
      invalidAccounts: mainInvalidAccountsProps.invalidAccounts.slice(1)
    });
    const tooltip = shallowInvalidAccountsWrapper.find(Tooltip);
    expect(tooltip.children().text()).toEqual('View Unsupported Account');
    const overlay = shallow(tooltip.prop('overlay'));
    expect(overlay.text()).toEqual('fry');
  });
});
