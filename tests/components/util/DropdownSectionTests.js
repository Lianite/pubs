import React from 'react';
import {
  default as DropdownSection,
  SelectedData
} from 'components/util/DropdownSection';
import AlertBox from 'components/util/AlertBox';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import Label from 'components/util/Label';
import { Combobox } from 'react-widgets';
import { shallow, mount } from 'enzyme';

import styles from 'styles/modules/components/util/DropdownSection.scss';
import dropdownButtonStyles from 'styles/modules/components/util/DropdownButton.scss';
import labelStyles from 'styles/modules/components/optionsbar/Label.scss';

import type { DropdownSectionProps } from 'components/util/DropdownSection';

let shallowWrapper;
let mountedWrapper;

const findByClassName = (className: string, parent? : Object) => {
  const shallowParent = parent || shallowWrapper;
  const mountedParent = parent || mountedWrapper;
  return {
    shallow: shallowParent.find(`.${className}`),
    mounted: mountedParent.find(`.${className}`)
  };
};

const mainProps: DropdownSectionProps = {
  title: 'test',
  icon: <span>icon</span>,
  iconRight: true,
  subheading: true,
  description: 'this is a section',
  comboBoxConfig: {}
};

function render(props: DropdownSectionProps = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<DropdownSection {...props} />);
  mountedWrapper = mount(<DropdownSection {...props} />);
}

describe('Dropdown Section Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const header = shallowWrapper.find(OptionsMenuSectionHeader);
    expect(header.length).toEqual(1);
    expect(header.prop('title')).toEqual('test');
    expect(header.prop('children')).toDeepEqual(<span>icon</span>);
    expect(header.prop('iconRight')).toEqual(true);
    expect(header.prop('subheading')).toEqual(true);

    const description = findByClassName(styles.description).shallow;
    expect(description.text()).toEqual('this is a section');

    expect(shallowWrapper.find(Label).length).toEqual(0);
    expect(findByClassName(styles.errorWrapper).shallow.length).toEqual(0);

    const container = findByClassName(styles.comboBoxContainer).shallow;
    expect(container.length).toEqual(1);

    expect(findByClassName(styles.colorBar, container).shallow.length).toEqual(0);

    const comboBox = shallowWrapper.find(Combobox);
    expect(comboBox.length).toEqual(1);

    const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
    expect(dropdownButton.length).toEqual(1);

    expect(findByClassName(styles.clearSelectedDatum, container).shallow.length).toEqual(0);
  });

  it('should set the correct class based on whether the clear button should display', () => {
    let container = findByClassName(styles.comboBoxContainer).shallow;
    expect(container.hasClass(styles.clearButtonVisible)).toEqual(false);

    render({
      singleSelect: true,
      selectedData: [{}]
    });
    container = findByClassName(styles.comboBoxContainer).shallow;
    expect(container.hasClass(styles.clearButtonVisible)).toEqual(true);
  });

  it('should render an error correctly', () => {
    render({
      errorText: 'this is an error'
    });
    const error = shallowWrapper.find(AlertBox);
    expect(error.prop('text')).toEqual('this is an error');
  });

  it('should open and close the list correctly', () => {
    const onToggle = jasmine.createSpy('onToggle');
    render({
      onToggle
    });
    let comboBox = mountedWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(false);

    let dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
    dropdownButton.simulate('click');

    comboBox = mountedWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(true);
    expect(onToggle.calls.mostRecent().args).toEqual([true]);

    dropdownButton.simulate('click');
    comboBox = mountedWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(false);
    expect(onToggle.calls.mostRecent().args).toEqual([false]);

    render({
      shouldToggleOpen: false
    });
    dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
    dropdownButton.simulate('click');

    comboBox = shallowWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(false);
    expect(onToggle.calls.mostRecent().args).toEqual([false]);
  });

  it('should handle blur events correctly', () => {
    const onToggle = jasmine.createSpy('onToggle');
    const onBlur = jasmine.createSpy('onBlur');
    render({
      comboBoxConfig: {
        onBlur
      },
      onToggle
    });
    let comboBox = shallowWrapper.find(Combobox);
    comboBox.simulate('focus', {});

    comboBox = shallowWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(true);

    shallowWrapper.instance().topLevelNode = {
      contains: value => value
    };

    comboBox.simulate('blur', { relatedTarget: false });
    comboBox = shallowWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(false);
    expect(onToggle).toHaveBeenCalledWith(false);
    expect(onBlur).toHaveBeenCalled();

    comboBox.simulate('focus', {});
    comboBox = shallowWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(true);

    onToggle.calls.reset();
    onBlur.calls.reset();

    comboBox.simulate('blur', { relatedTarget: true });
    comboBox = shallowWrapper.find(Combobox);
    expect(comboBox.prop('open')).toEqual(true);
    expect(onToggle.calls.count()).toEqual(0);
    expect(onBlur).toHaveBeenCalled();
  });

  it('should call on toggle open on toggle open', () => {
    const onToggle = jasmine.createSpy('onToggle');
    render({
      onToggle
    });

    shallowWrapper.instance().toggleOpen();

    expect(onToggle).toHaveBeenCalled();
  });

  describe('clear input on select', () => {
    it('should not clear input on select', () => {
      render({
        comboBoxConfig: {
          data: [{
            text: 'test'
          }, {
            text: 'example'
          }],
          valueField: 'text',
          textField: 'text'
        }
      });
      mountedWrapper.setState({
        comboBoxValue: 'test'
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);
      const firstItem = comboBox.find('li').first();

      firstItem.simulate('click');
      expect(mountedWrapper.find(Combobox).find('input').prop('value')).toEqual('test');
    });

    it('should not clear input on select', () => {
      render({
        comboBoxConfig: {
          data: [{
            text: 'test'
          }, {
            text: 'example'
          }],
          valueField: 'text',
          textField: 'text'
        },
        clearInputOnSelect: true
      });
      mountedWrapper.setState({
        comboBoxValue: 'test'
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);
      const firstItem = comboBox.find('li').first();

      firstItem.simulate('click');
      expect(mountedWrapper.find(Combobox).find('input').prop('value')).toEqual('');
    });
  });

  describe('multi-select', () => {
    it('should render selected data correctly', () => {
      const onRemoveDatum = jasmine.createSpy('onRemoveDatum');
      const selectedData = [{
        text: 'example'
      }, {
        text: 'data point',
        error: true
      }, {
        text: 'hello'
      }];
      render({
        selectedData,
        onRemoveDatum
      });
      const data = shallowWrapper.find(SelectedData).dive().find(Label);
      expect(data.length).toEqual(3);
      data.forEach((dataLabel, index) => {
        expect(dataLabel.key()).toEqual(`${index}`);
        expect(typeof dataLabel.prop('onRemove')).toEqual('function');
        expect(dataLabel.prop('error')).toEqual(!!selectedData[index].error);
        expect(dataLabel.prop('children')).toEqual(selectedData[index].text);
      });

      const renderedDatum = data.at(0).dive();
      const datumButton = renderedDatum.find(`.${labelStyles.closeButton}`);
      datumButton.simulate('click');
      expect(onRemoveDatum).toHaveBeenCalledWith('example');
    });

    it('should handle focus events correctly', () => {
      const onToggle = jasmine.createSpy('onToggle');
      render({
        onToggle
      });
      let comboBox = shallowWrapper.find(Combobox);

      comboBox.simulate('focus', {});

      comboBox =  shallowWrapper.find(Combobox);
      expect(onToggle).toHaveBeenCalledWith(true);
      expect(comboBox.prop('open')).toEqual(true);

      onToggle.calls.reset();

      render({
        onToggle,
        shouldToggleOpen: false
      });
      comboBox = shallowWrapper.find(Combobox);

      comboBox.simulate('focus', {});

      comboBox =  shallowWrapper.find(Combobox);
      expect(onToggle.calls.count()).toEqual(0);
      expect(comboBox.prop('open')).toEqual(false);
    });

    it('should select data correctly from a click', () => {
      const addDatum = jasmine.createSpy('addDatum');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      const fetchData = jasmine.createSpy('fetchData');

      render({
        comboBoxConfig: {
          data: [{
            text: 'test'
          }, {
            text: 'example'
          }],
          valueField: 'text',
          textField: 'text'
        },
        addDatum,
        onInputValueChange,
        fetchData
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);
      const firstItem = comboBox.find('li').first();

      firstItem.simulate('click');
      expect(addDatum).toHaveBeenCalledWith('test');
      expect(fetchData).toHaveBeenCalled();
      expect(mountedWrapper.find(Combobox).prop('open')).toEqual(true);
    });

    it('should select data correctly from a keypress', () => {
      const addDatum = jasmine.createSpy('addDatum');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      const fetchData = jasmine.createSpy('fetchData');

      render({
        comboBoxConfig: {
          data: [{
            text: 'test'
          }, {
            text: 'example'
          }],
          valueField: 'text',
          textField: 'text'
        },
        addDatum,
        onInputValueChange,
        fetchData
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);

      comboBox.simulate('keyDown', {
        key: 'Enter'
      });
      expect(addDatum).toHaveBeenCalledWith('test');
      expect(fetchData).toHaveBeenCalled();
      expect(mountedWrapper.find(Combobox).prop('open')).toEqual(true);
    });

    it('should handle changes by keypress appropriately', () => {
      const onChange = jasmine.createSpy('onChange');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      render({
        comboBoxConfig: {
          onChange
        },
        onInputValueChange
      });

      const comboBox = mountedWrapper.find(Combobox);
      const input = comboBox.find('input');

      input.simulate('change', 'a');
      expect(onChange).toHaveBeenCalled();
      expect(onInputValueChange).toHaveBeenCalled();
    });
  });

  describe('single-select', () => {
    it('should not render selected data', () => {
      const selectedData = [{
        text: 'example'
      }, {
        text: 'data point',
        error: true
      }, {
        text: 'hello'
      }];
      render({
        selectedData,
        singleSelect: true
      });
      const data = shallowWrapper.find(Label);
      expect(data.length).toEqual(0);
    });

    it('should handle focus events correctly', () => {
      const onToggle = jasmine.createSpy('onToggle');
      render({
        onToggle,
        singleSelect: true
      });
      let comboBox = shallowWrapper.find(Combobox);

      comboBox.simulate('focus', {});

      comboBox =  shallowWrapper.find(Combobox);
      expect(onToggle).toHaveBeenCalledWith(true);
      expect(comboBox.prop('open')).toEqual(true);

      onToggle.calls.reset();

      render({
        onToggle,
        shouldToggleOpen: false,
        singleSelect: true
      });
      comboBox = shallowWrapper.find(Combobox);

      comboBox.simulate('focus', {});

      comboBox =  shallowWrapper.find(Combobox);
      expect(onToggle.calls.count()).toEqual(0);
      expect(comboBox.prop('open')).toEqual(false);
    });

    it('should select data correctly from a click', () => {
      const addDatum = jasmine.createSpy('addDatum');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      const fetchData = jasmine.createSpy('fetchData');

      render({
        comboBoxConfig: {
          data: [{
            text: 'test'
          }, {
            text: 'example'
          }],
          valueField: 'text',
          textField: 'text'
        },
        addDatum,
        onInputValueChange,
        fetchData,
        singleSelect: true
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);
      const firstItem = comboBox.find('li').first();

      firstItem.simulate('click');
      expect(addDatum).toHaveBeenCalledWith({ text: 'test' });
      expect(onInputValueChange).toHaveBeenCalledWith({ text: 'test' });
      expect(fetchData).toHaveBeenCalled();
      expect(mountedWrapper.find(Combobox).prop('open')).toEqual(false);
    });

    it('should select data correctly from a keypress', () => {
      const addDatum = jasmine.createSpy('addDatum');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      const fetchData = jasmine.createSpy('fetchData');

      render({
        comboBoxConfig: {
          data: [{
            text: 'test'
          }, {
            text: 'example'
          }],
          valueField: 'text',
          textField: 'text'
        },
        addDatum,
        onInputValueChange,
        fetchData,
        singleSelect: true
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);

      comboBox.simulate('keyDown', {
        key: 'Enter'
      });
      expect(addDatum).toHaveBeenCalledWith({ text: 'test' });
      expect(onInputValueChange).toHaveBeenCalledWith({ text: 'test' });
      expect(fetchData).toHaveBeenCalled();
      expect(mountedWrapper.find(Combobox).prop('open')).toEqual(false);
    });

    it('should handle disabledField correctly from a select', () => {
      const addDatum = jasmine.createSpy('addDatum');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      const fetchData = jasmine.createSpy('fetchData');

      render({
        comboBoxConfig: {
          data: [{
            text: 'test',
            disable: true
          }, {
            text: 'example',
            disable: true
          }],
          valueField: 'text',
          textField: 'text',
          disabledField: 'disable'
        },
        addDatum,
        onInputValueChange,
        fetchData,
        singleSelect: true
      });
      const dropdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
      dropdownButton.simulate('click');

      const comboBox = mountedWrapper.find(Combobox);

      comboBox.simulate('keyDown', {
        key: 'Enter'
      });
      expect(addDatum).not.toHaveBeenCalled();
      expect(onInputValueChange).not.toHaveBeenCalled();
      expect(fetchData).not.toHaveBeenCalled();
      expect(mountedWrapper.find(Combobox).prop('open')).toEqual(true);
    });

    it('should handle changes by keypress appropriately', () => {
      const onChange = jasmine.createSpy('onChange');
      const onInputValueChange = jasmine.createSpy('onInputValueChange');
      render({
        comboBoxConfig: {
          onChange
        },
        onInputValueChange,
        singleSelect: true
      });

      const comboBox = mountedWrapper.find(Combobox);
      const input = comboBox.find('input');

      input.simulate('change', 'a');
      expect(onChange).toHaveBeenCalled();
      expect(onInputValueChange).toHaveBeenCalled();
    });

    it('should render a color bar', () => {
      render({
        selectedData: [{
          text: 'test',
          error: false,
          color: '#fff'
        }],
        colorSelected: true
      });

      const colorBar = findByClassName(styles.colorBar).shallow;
      expect(colorBar.length).toEqual(1);
      expect(colorBar.prop('style').backgroundColor).toEqual('#fff');
    });
  });
});
