import React from 'react';
import AlertBox from 'components/util/AlertBox';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/util/AlertBox.scss';

import type { Props } from 'components/util/AlertBox';

let shallowWrapper;

const mainProps: Props = {
  text: 'this is an error',
  additionalText: 'description',
  link: <div>link text</div>
};

function render(props: Props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<AlertBox {...props} />);
}

describe('Alert Box Component', () => {
  beforeEach(() => {
    render();
  });
  it('should render correctly', () => {
    const wrapper = shallowWrapper.find(`.${styles.alertWrapper}`);
    expect(wrapper.length).toEqual(1);
    expect(shallowWrapper.find(`.${styles.soft}`).length).toEqual(0);

    const icon = wrapper.find(`.${styles.alertIcon}`);
    expect(icon.length).toEqual(1);

    const alertText = wrapper.find(`.${styles.alertText}`);
    expect(alertText.text()).toContain('this is an error');

    const additionalText = wrapper.find(`.${styles.additionalText}`);
    expect(additionalText.text()).toEqual('description');

    const link = shallowWrapper.find(`.${styles.link}`);
    expect(link.children().text()).toEqual('link text');
  });

  it('should render a soft warning correctly', () => {
    render({
      soft: true
    });
    expect(shallowWrapper.find(`.${styles.soft}.${styles.alertWrapper}`).length).toEqual(1);
  });
});
