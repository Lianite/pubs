import React from 'react';
import ReactDOM from 'react-dom';
import MessageHeaderBox, {
  ErrorMessage,
  ApplyToAllButton
} from 'components/MessageBoxHeader';
import { ValidationType } from 'constants/UIStateConstants';
import _ from 'lodash';
import { shallow, mount } from 'enzyme';
import Tooltip from '@spredfast/react-lib/lib/Tooltip';
import { SupportedNetworks } from 'constants/ApplicationConstants';
import PostPreview from '@spredfast/social-media-preview';

import type MessageAttachments from 'selectors/MessageAttachmentsSelectors';

import styles from 'styles/modules/components/MessageBoxHeader.scss';

let shallowWrapper, mountedWrapper;

describe('Message Box Header', () => {

  const props = {
    disableApplyToAll: false,
    numberOfNetworks: 1,
    network: SupportedNetworks.TWITTER,
    applyToAllChannels: jasmine.createSpy('applyToAllChannels'),
    onPreviewVisibilityChange: jasmine.createSpy('onPreviewVisibilityChange'),
    validation: {
      type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
      inlineText: '',
      hoverText: ''
    }
  };

  const linkPreviewPreviewProps: MessageAttachments = {
    type: 'link',
    attachment_type: 'link',
    attachment_title: 'title',
    attachment_caption: 'caption',
    attachment_link_image: 'http://url-image.com',
    attachment_description: 'description'
  };

  const videoPreviewProps: MessageAttachments = {
    type: 'video',
    attachment_type: 'video',
    attachment_title: 'title',
    attachment_video_thumbnail: 'http://video-thumbnail.com',
    attachment_video_uri: 'http://video-uri.com'
  };

  const imagePreviewProps: MessageAttachments = {
    type: 'image',
    attachment_type: 'image',
    attachment_images: ['http://image-uri.com']
  };

  const render = (componentProps = props) => {
    componentProps = {
      ...props,
      ...componentProps
    };
    shallowWrapper = shallow(<MessageHeaderBox {...componentProps}/>);
    mountedWrapper = mount(<MessageHeaderBox {...componentProps}/>);
  };

  beforeEach(() => {
    render({
      numberOfNetworks: 1
    });
  });

  it('should render correctly', () => {
    const header = shallowWrapper.find(`.${styles.messageHeader}`);
    expect(header.text()).toContain('TWITTER');

    const icon = header.find('.icon-network-twitter-bounds');
    expect(icon.length).toEqual(1);

    const applyToAll = header.find(ApplyToAllButton);
    expect(applyToAll.length).toEqual(1);
    expect(applyToAll.props()).toEqual(jasmine.objectContaining({
      disableApplyToAll: false,
      numberOfNetworks: 1
    }));
    applyToAll.simulate('applyToAllChannels');
    expect(props.applyToAllChannels).toHaveBeenCalled();

    const error = header.find(ErrorMessage);
    expect(error.length).toEqual(1);
    expect(error.prop('validation')).toEqual(props.validation);
  });

  describe('Error Message', () => {
    let shallowErrorWrapper;
    const mainErrorProps = {
      validation: {
        type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
        inlineText: '',
        hoverText: ''
      }
    };
    const renderError = (errorProps = mainErrorProps) => {
      errorProps = {
        ...mainErrorProps,
        ...errorProps
      };
      shallowErrorWrapper = shallow(<ErrorMessage {...errorProps} />);
    };
    beforeEach(() => {
      renderError();
    });

    it('should render nothing when there are no problems', () => {
      expect(shallowErrorWrapper.text()).toBeFalsy();
    });

    it('should render correctly when there is an error', () => {
      renderError({
        validation: {
          type: ValidationType.VALIDATION_TYPE_ERROR,
          inlineText: 'this is a problem',
          hoverText: ''
        }
      });
      const errorLine = shallowErrorWrapper.find(`.${styles.messageHeaderErrorLine}`);
      expect(errorLine.length).toEqual(1);

      const validation = shallowErrorWrapper.find(`.${styles.error}.${styles.visible}`);
      expect(validation.length).toEqual(2);
      expect(shallowErrorWrapper.find(`.${styles.improvement}`).length).toEqual(0);
      expect(shallowErrorWrapper.find(`.${styles.validationIcon}`).length).toEqual(1);
      expect(shallowErrorWrapper.find(`.${styles.validationText}`).length).toEqual(1);
      expect(shallowErrorWrapper.text()).toEqual('this is a problem');
    });

    it('should render correctly when there is an improvement', () => {
      renderError({
        validation: {
          type: ValidationType.VALIDATION_TYPE_IMPROVEMENT,
          inlineText: 'this is an improvement',
          hoverText: ''
        }
      });
      const errorLine = shallowErrorWrapper.find(`.${styles.messageHeaderErrorLine}`);
      expect(errorLine.length).toEqual(1);

      const validation = shallowErrorWrapper.find(`.${styles.improvement}.${styles.visible}`);
      expect(validation.length).toEqual(2);
      expect(shallowErrorWrapper.find(`.${styles.error}`).length).toEqual(0);
      expect(shallowErrorWrapper.find(`.${styles.validationIcon}`).length).toEqual(1);
      expect(shallowErrorWrapper.find(`.${styles.validationText}`).length).toEqual(1);
      expect(shallowErrorWrapper.text()).toEqual('this is an improvement');
    });
  });

  describe('post preview', () => {
    it('should not render a post preview button when there is no associated network', () => {
      const  updatedProps = _.assign({}, props, linkPreviewPreviewProps, {
        network: ''
      });
      render(updatedProps);

      expect(shallowWrapper.find(Tooltip).length).toEqual(0);
      expect(shallowWrapper.find(`.${styles.previewIcon}`).length).toEqual(0);
    });
    it('should pass the appropriate maximum width to a facebook post preview', () => {
      const  updatedProps = _.assign({}, props, linkPreviewPreviewProps, {
        network: SupportedNetworks.FACEBOOK
      });
      render(updatedProps);

      const tooltip = shallowWrapper.find(Tooltip);
      expect(tooltip.prop('overlayStyle')).toDeepEqual({
        maxWidth: '500px',
        width: '500px'
      });
    });
    it('should pass the appropriate maximum width to a twitter post preview', () => {
      const  updatedProps = _.assign({}, props, linkPreviewPreviewProps, {
        network: SupportedNetworks.TWITTER
      });
      render(updatedProps);

      const tooltip = shallowWrapper.find(Tooltip);
      expect(tooltip.prop('overlayStyle')).toDeepEqual({
        maxWidth: '600px',
        width: '600px'
      });
    });
    it('should successfully call the preview visibility change callback', () => {
      const previewVisibilitySpy = jasmine.createSpy('previewVisibilitySpy');
      const  updatedProps = _.assign({}, props, linkPreviewPreviewProps, {
        network: SupportedNetworks.FACEBOOK,
        onPreviewVisibilityChange: previewVisibilitySpy
      });
      render(updatedProps);

      const previewButton = mountedWrapper.find(`.${styles.previewIcon}`);
      previewButton.simulate('click');

      expect(previewVisibilitySpy).toHaveBeenCalled();
    });
    it('should render a facebook post preview when the network is facebook', () => {
      const  updatedProps = _.assign({}, props, {
        postPreview: linkPreviewPreviewProps
      }, {
        network: SupportedNetworks.FACEBOOK
      });
      render(updatedProps);

      const tooltip = shallowWrapper.find(Tooltip);
      expect(tooltip.length).toEqual(1);
      expect(tooltip.prop('overlay').type).toEqual(PostPreview.Facebook);
      expect(shallowWrapper.find(`.${styles.previewIcon}`).length).toEqual(1);
    });
    it('should render a twitter post preview when the network is twitter', () => {
      const  updatedProps = _.assign({}, props, {
        postPreview: linkPreviewPreviewProps
      }, {
        network: SupportedNetworks.TWITTER
      });
      render(updatedProps);

      const tooltip = shallowWrapper.find(Tooltip);
      expect(tooltip.length).toEqual(1);
      expect(tooltip.prop('overlay').type).toEqual(PostPreview.Twitter);
      expect(shallowWrapper.find(`.${styles.previewIcon}`).length).toEqual(1);
    });
    it('should pass the appropriate props to a link preview', () => {
      const  updatedProps = _.assign({}, props, {
        postPreview: linkPreviewPreviewProps
      }, {
        network: SupportedNetworks.FACEBOOK
      });
      render(updatedProps);

      const postPreview = shallowWrapper.find(Tooltip).prop('overlay');
      expect(postPreview.props).toEqual(jasmine.objectContaining({
        type: 'link',
        attachment_type: 'link',
        attachment_title: 'title',
        attachment_caption: 'caption',
        attachment_description: 'description',
        attachment_link_image: 'http://url-image.com',
        showSeeMore: true
      }));
    });
    it('should pass the appropriate props to a video preview', () => {
      const  updatedProps = _.assign({}, props, {
        postPreview: videoPreviewProps
      }, {
        network: SupportedNetworks.FACEBOOK
      });
      render(updatedProps);

      const postPreview = shallowWrapper.find(Tooltip).prop('overlay');
      expect(postPreview.props).toEqual(jasmine.objectContaining({
        type: 'video',
        attachment_type: 'video',
        attachment_title: 'title',
        attachment_video_thumbnail: 'http://video-thumbnail.com',
        attachment_video_uri: 'http://video-uri.com',
        showSeeMore: true
      }));
    });
    it('should pass the appropriate props to an image preview', () => {
      const  updatedProps = _.assign({}, props, {
        postPreview: imagePreviewProps
      }, {
        network: SupportedNetworks.FACEBOOK
      });
      render(updatedProps);

      const postPreview = shallowWrapper.find(Tooltip).prop('overlay');
      expect(postPreview.props).toEqual(jasmine.objectContaining({
        type: 'image',
        attachment_type: 'image',
        attachment_images: ['http://image-uri.com'],
        showSeeMore: true
      }));
    });
    it('should pass the correct class names to the tooltip based on tooltip position and size', () => {
      const targetRect = {
        top: 100,
        bottom: 150,
        left: 100,
        right: 150
      };
      let popupRect = {
        top: 150,
        bottom: 200,
        left: 150,
        right: 200
      };

      render({
        ...props,
        postPreview: linkPreviewPreviewProps,
        network: SupportedNetworks.FACEBOOK
      });

      const originalOnPopupAlign = mountedWrapper.instance().onPreviewPopupAlign;
      spyOn(ReactDOM.findDOMNode(mountedWrapper.ref('previewTarget').nodes[0]), 'getBoundingClientRect').and.callFake(() => targetRect);
      spyOn(mountedWrapper.instance(), 'onPreviewPopupAlign').and.callFake(function() {
        return originalOnPopupAlign.bind(this)({
          getBoundingClientRect: () => popupRect
        });
      });
      mountedWrapper.instance().onPreviewPopupAlign();
      const tooltip = mountedWrapper.find(Tooltip);
      expect(tooltip.prop('overlayClassName')).toContain('rc-tooltip-placement-bottom');
      expect(tooltip.prop('overlayClassName')).not.toContain('post-preview-arrow-hidden');

      popupRect.top = 50;
      mountedWrapper.instance().onPreviewPopupAlign();
      expect(tooltip.prop('overlayClassName')).not.toContain('rc-tooltip-placement-bottom');
      expect(tooltip.prop('overlayClassName')).not.toContain('post-preview-arrow-hidden');

      popupRect = {
        top: 0,
        bottom: 500,
        left: 0,
        right: 500
      };
      mountedWrapper.instance().onPreviewPopupAlign();
      expect(tooltip.prop('overlayClassName')).not.toContain('rc-tooltip-placement-bottom');
      expect(tooltip.prop('overlayClassName')).toContain('post-preview-arrow-hidden');

      popupRect = {
        top: 0,
        bottom: 50,
        left: 0,
        right: 50
      };
      mountedWrapper.instance().onPreviewPopupAlign();
      expect(tooltip.prop('overlayClassName')).not.toContain('rc-tooltip-placement-bottom');
      expect(tooltip.prop('overlayClassName')).not.toContain('post-preview-arrow-hidden');
    });
    it('should pass the appropriate platforms to a preview', () => {
      const  updatedProps = _.assign({}, props, {
        postPreview: imagePreviewProps
      }, {
        network: SupportedNetworks.FACEBOOK
      });
      render(updatedProps);

      const postPreview = shallowWrapper.find(Tooltip).prop('overlay');
      expect(postPreview.props).toEqual(jasmine.objectContaining({
        platforms: ['DESKTOP', 'MOBILE'],
        showNetworkHeader: true
      }));
    });
  });
  describe('apply to all channels', () => {
    let shallowApplyWrapper;
    const mainApplyProps = {
      disableApplyToAll: false,
      numberOfNetworks: 2,
      onApplyToAllChannels: jasmine.createSpy('onApplyToAllChannels')
    };
    const renderApply = (props = mainApplyProps) => {
      props = {
        ...mainApplyProps,
        ...props
      };
      shallowApplyWrapper = shallow(<ApplyToAllButton {...props} />);
    };

    beforeEach(() => {
      renderApply();
    });

    it('should render correctly', () => {
      const applyToAll = shallowApplyWrapper.find(`.${styles.applyToAllButton}`);
      expect(applyToAll.length).toEqual(1);
      expect(applyToAll.hasClass(styles.disabledApplyAll)).toBe(false);
      expect(applyToAll.prop('disabled')).toBe(false);
      expect(applyToAll.text()).toEqual('Apply To All Channels');

      applyToAll.simulate('click');
      expect(mainApplyProps.onApplyToAllChannels).toHaveBeenCalled();
    });

    it('should disable correctly', () => {
      renderApply({
        disableApplyToAll: true
      });
      const applyToAll = shallowApplyWrapper.find(`.${styles.applyToAllButton}`);
      expect(applyToAll.length).toEqual(1);
      expect(applyToAll.hasClass(styles.disabledApplyAll)).toBe(true);
      expect(applyToAll.prop('disabled')).toBe(true);
    });

    it('should not render when there are fewer than 2 networks', () => {
      renderApply({
        numberOfNetworks: 1
      });
      const applyToAll = shallowApplyWrapper.find(`.${styles.applyToAllButton}`);
      expect(applyToAll.length).toEqual(0);
    });
  });
});
