import React from 'react';
import { shallow } from 'enzyme';
import Main from 'components/Main';
import Dropzone from 'react-dropzone';
import Spredchat from 'components/Spredchat';
import GrowlerReduxHost from '@spredfast/react-lib/lib/growler-notification/redux/GrowlerReduxHost';
import GlobalValidationBar from 'components/GlobalValidationBar';
import MessageEditorContainer from 'containers/MessageEditorContainer';
import MessageTitleBox from 'components/MessageTitleBox';
import MessagePublishBar from 'components/MessagePublishBar';
import OptionsSidebarContainer from 'containers/OptionsSidebarContainer';
import ScheduleContainer from 'containers/ScheduleContainer';
import { ValidationType } from 'constants/UIStateConstants';
import assign from 'lodash/assign';
import styles from 'styles/modules/components/Main.scss';

const mockfunc = () => {};

describe('Main component', () => {
  let shallowWrapper;

  const mainProps = {
    onTitleChange: mockfunc,
    eventId: 1234,
    messageTitle: 'TEST',
    sidebarOpen: true,
    messageHasEventId: true,
    companyId: '33',
    campaignId: '54',
    userDataLoaded: false,
    validation: {
      type: ValidationType.VALIDATION_TYPE_NO_PROBLEMS,
      validationText:''
    },
    dragStateChanged: jasmine.createSpy('dragStateChanged'),
    cancelVerbage: 'Clear',
    onCancelClicked: mockfunc,
    previewVisible: false,
    hasViewAccess: true,
    sendAllAssigneesRequest: mockfunc,
    sendValidAssigneesRequest: mockfunc,
    createCurrentAssignee: mockfunc,
    currentPlan: {name: '', id: '', color: 'blue'},
    fetchLabelsAction: mockfunc,
    canUseMultichannelPubs: true,
    canPublish: true,
    canSaveDraft: true,
    hasEditAccess: true,
    initializeAppData: mockfunc,
    scheduledDatetime: true,
    onScheduleClicked: mockfunc
  };

  const render = (componentProps = mainProps) => {
    const finalProps = assign({}, mainProps, componentProps);
    shallowWrapper = shallow(<Main {...finalProps} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.dragStateChanged.calls.reset();
  });

  // a basic test for looking at outputted elements in the DOM and verifing the results
  it('should render correctly', () => {
    const container = shallowWrapper.find('#publishing-ui-app-container');
    expect(container.length).toEqual(1);

    const dropzone = container.find(Dropzone);
    expect(dropzone.length).toEqual(1);

    const spredchat = container.find(Spredchat);
    expect(spredchat.length).toEqual(1);
    expect(spredchat.props()).toEqual({
      id: '1234',
      name: 'TEST',
      companyId: '33',
      campaignId: '54'
    });

    const growlerHost = container.find(GrowlerReduxHost);
    expect(growlerHost.length).toEqual(1);

    const globalValidationBar = container.find(GlobalValidationBar);
    expect(globalValidationBar.length).toEqual(1);
    expect(globalValidationBar.prop('validation')).toEqual(mainProps.validation);

    const title = container.find(MessageTitleBox);
    expect(title.length).toEqual(1);
    expect(title.props()).toDeepEqual({
      messageTitle: 'TEST',
      onChange: mainProps.onTitleChange,
      currentPlanColor: 'blue',
      closeButtonClicked: mainProps.onCancelClicked,
      canUseMultichannelPubs: mainProps.canUseMultichannelPubs
    });

    const sidebar = container.find(OptionsSidebarContainer);
    expect(sidebar.length).toEqual(1);

    const editor = shallowWrapper.find(MessageEditorContainer);
    expect(editor.length).toEqual(1);

    const schedule = shallowWrapper.find(ScheduleContainer);
    expect(schedule.length).toEqual(1);

    const publish = shallowWrapper.find(MessagePublishBar);
    expect(publish.length).toEqual(1);
    expect(publish.props()).toDeepEqual({
      publishImmediately: false,
      onScheduleClicked: mainProps.onScheduleClicked,
      canUseMultichannelPubs: true,
      canPublish: true,
      canSaveDraft: true,
      onCancelClicked: mainProps.onCancelClicked,
      cancelVerbage: 'Clear',
      hasEditAccess: true
    });

    expect(shallowWrapper.find(`.${styles.postPreviewOverlay}`).length).toEqual(0);

    expect(shallowWrapper.find(`.${styles.permissionBlockingOverlay}`).length).toEqual(0);
  });

  describe('Spredchat', () => {
    it('should not render spredchat when appropriate', () => {
      render({
        eventId: undefined
      });
      expect(shallowWrapper.find(Spredchat).length).toEqual(0);
    });

    it('should not render spredchat when appropriate', () => {
      render({
        eventId: 1,
        messageTitle: undefined
      });
      expect(shallowWrapper.find(Spredchat).length).toEqual(0);
    });
  });

  describe('Dropzone', () => {
    let dropzone;
    it('should handle dragEnter events correctly', () => {
      dropzone = shallowWrapper.find(Dropzone);
      dropzone.simulate('dragEnter');
      expect(mainProps.dragStateChanged).toHaveBeenCalledWith(true);
    });

    it('should handle dragLeave events correctly', () => {
      dropzone = shallowWrapper.find(Dropzone);
      dropzone.simulate('dragLeave');
      expect(mainProps.dragStateChanged).toHaveBeenCalledWith(false);
    });

    it('should handle drop events correctly', () => {
      dropzone = shallowWrapper.find(Dropzone);
      dropzone.simulate('drop');
      expect(mainProps.dragStateChanged).toHaveBeenCalledWith(false);
    });
  });

  describe('Permission Blocking Overlay', () => {
    it('should display when user cannot use the app', () => {
      render({
        canUseMultichannelPubs: false
      });

      const permission = shallowWrapper.find(`.${styles.permissionBlockingOverlay}`);
      expect(permission.length).toEqual(1);
      expect(permission.text()).toContain('Sorry, you don\'t have access to publishing');
      expect(permission.text()).toContain('Have any questions? Contact Customer Support');
    });
  });

  it('should not render a post preview overlay when preview is not visible', () => {
    render({
      previewVisible: false
    });
    expect(shallowWrapper.find(`.${styles.postPreviewOverlay}`).length).toEqual(0);
  });

  it('should render a post preview overlay when preview is visible', () => {
    render({
      previewVisible: true
    });
    expect(shallowWrapper.find(`.${styles.postPreviewOverlay}`).length).toEqual(1);
  });
});
