/* @flow */
import React from 'react';
import AssigneeDropDown from 'components/optionsbar/AssigneeDropDown';
import { Combobox } from 'react-widgets';
import DropdownSection from 'components/util/DropdownSection';
import styles from 'styles/modules/components/optionsbar/AssigneeDropDown.scss';
import { shallow } from 'enzyme';

const mainProps = {
  currentAssignee: {firstName:'Bob', lastName:'Kelso', id:1},
  onSelectAssignee: () => true,
  resendValidAssigneesRequest: () => true,
  assignees: [{firstName:'Bob', lastName:'Kelso', id:1}],
  invalidAssignees: [],
  unavailableVoices: [
    {
      data: {
        id: 1,
        name: 'Test 1',
        credentials: []
      },
      nextActions: {},
      uri: '',
      permissions: [],
      verbs: []
    }
  ]
};

let wrapper;

function render(props = {}) {
  props = {...mainProps, ...props};
  wrapper = shallow(<AssigneeDropDown {...props}/>);
}

describe('AssigneeDropDown Component', () => {

  beforeEach(() => {
    render(mainProps);
  });

  it('should render a Combobox and a chevronButton', () => {
    expect(wrapper.find(DropdownSection).length).not.toEqual(0);
  });

  describe('should handle onBlur correctly', () => {
    it('should return currentAssignee if Combobox cleared out', () => {
      wrapper.setState({comboBoxValue: {}});
      const dropdownSection = wrapper.find(DropdownSection).dive();
      const comboBox = dropdownSection.find(Combobox);
      comboBox.simulate('blur', {});
      expect(wrapper.state('comboBoxValue')).toEqual(mainProps.currentAssignee);
      expect(wrapper.state('comboBoxOpen')).toEqual(false);
      expect(wrapper.find(DropdownSection).props().comboBoxConfig.value).toEqual(mainProps.currentAssignee);
    });

    it('should select the assignee if a valid one is filled out', () => {
      wrapper.setState({comboBoxValue: {firstName:'Bob', lastName:'Kelso', id:2}});
      const dropdownSection = wrapper.find(DropdownSection).dive();
      const comboBox = dropdownSection.find(Combobox);
      comboBox.simulate('blur', {});
      expect(wrapper.state('comboBoxValue')).toEqual({firstName:'Bob', lastName:'Kelso', id:2});
      expect(wrapper.state('comboBoxOpen')).toEqual(false);
      expect(wrapper.find(DropdownSection).props().comboBoxConfig.value).toEqual({firstName:'Bob', lastName:'Kelso', id:2});
    });
  });

  it('should create a warning text if there are invalidAssignees', () => {
    expect(wrapper.find(`.${styles.warningWrapper}`).length).toEqual(1);
  });

});
