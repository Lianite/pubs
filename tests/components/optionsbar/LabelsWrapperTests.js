/* @flow */
import React from 'react';
import { shallow, mount } from 'enzyme';
import LabelsWrapper from 'components/optionsbar/LabelsWrapper';
import DropdownSection from 'components/util/DropdownSection';
import { Combobox } from 'react-widgets';
import { List as ImmutableList } from 'immutable';
import { LabelsRecord } from 'records/LabelsRecords';

let shallowWrapper, mountedWrapper;

import dropdownButtonStyles from 'styles/modules/components/util/DropdownButton.scss';

const mockFunc = () => {};

const mainProps = {
  availableLabels: [
    {
      title: 'Test 1',
      id: 1
    },
    {
      title: 'Test 2'
    },
    {
      title: 'Test 3'
    },
    {
      title: 'Test 4'
    }
  ],
  selectedLabels: [
    {
      title: 'Test 2'
    }
  ],
  labelErrorGroup: ImmutableList(),
  updateLabelText: mockFunc
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<LabelsWrapper {...props} />);
  mountedWrapper = mount(<LabelsWrapper {...props} />);
}

describe('LabelsWrapper Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render and pass props correctly', () => {
    const dropdownSection = shallowWrapper.find(DropdownSection);
    expect(dropdownSection.length).toEqual(1);
    expect(dropdownSection.props()).toEqual(jasmine.objectContaining({
      title: 'Labels',
      icon: <span className='icon-label-thick'></span>,
      description: 'Add labels that will be associated with your publishing message.',
      errorText: '',
      onInputValueChange: mainProps.updateLabelText
    }));
    expect(dropdownSection.prop('selectedData')).toDeepEqual([{
      text: 'Test 2',
      error :false
    }]);

    const comboBoxConfig = dropdownSection.prop('comboBoxConfig');
    expect(comboBoxConfig).toEqual(jasmine.objectContaining({
      data: mainProps.availableLabels,
      placeholder: 'Select Labels...'
    }));
    expect(typeof comboBoxConfig.itemComponent).toEqual('function');
  });

  it('should respond correctly to selecting a label', () => {
    const addLabel = jasmine.createSpy('addLabel');
    const fetchLabelsAction = jasmine.createSpy('fetchLabelsAction');
    render({
      addLabel,
      fetchLabelsAction
    });

    const dropwdownButton = mountedWrapper.find(`.${dropdownButtonStyles.button}`);
    dropwdownButton.simulate('click');

    const comboBox = mountedWrapper.find(Combobox);
    const firstItem = comboBox.find('li').first();

    firstItem.simulate('click');
    expect(addLabel).toHaveBeenCalledWith(mainProps.availableLabels[0].title);
    expect(fetchLabelsAction).toHaveBeenCalled();
  });

  it('should pass in error text when appropriate', () => {
    let dropdownSection = shallowWrapper.find(DropdownSection);

    expect(dropdownSection.prop('errorText')).toEqual('');

    render({
      labelErrorGroup: ImmutableList([new LabelsRecord({ foo: 'bar' })])
    });

    dropdownSection = shallowWrapper.find(DropdownSection);
    expect(dropdownSection.prop('errorText')).toEqual('These labels belong to same label group and can’t both be used. Issue must be resolved to publish.');
  });

  it('should specify which labels have errors correctly', () => {
    render({
      labelErrorGroup: ImmutableList([new LabelsRecord({id: 1})]),
      selectedLabels: mainProps.availableLabels
    });

    const dropdownSection = shallowWrapper.find(DropdownSection);
    expect(dropdownSection.prop('selectedData')).toDeepEqual([{
      text: 'Test 1',
      error: true
    }, {
      text: 'Test 2',
      error: false
    }, {
      text: 'Test 3',
      error: false
    }, {
      text: 'Test 4',
      error: false
    }]);
  });
});
