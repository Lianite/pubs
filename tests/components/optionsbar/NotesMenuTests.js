import React from 'react';
import NotesMenu from 'components/optionsbar/NotesMenu';
import Note from 'components/optionsbar/Note';
import NoteInput from 'components/optionsbar/NoteInput';
import { shallow, mount } from 'enzyme';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import styles from 'styles/modules/components/optionsbar/NotesMenu.scss';
import { default as ProductNav, ProductNavItem } from '@spredfast/react-lib/lib/ProductNav';
import noNotesImage from 'images/nonotes.svg';
import { MAX_NOTE_CHARS } from 'constants/NotesMenuConstants';
import { NO_NOTES_OR_ACTIVITY } from 'constants/UITextConstants';

import type { NotesMenuProps } from 'components/NotesMenu';

let shallowWrapper;
let mountedWrapper;

const mockFunc = () => {};

const mainProps: NotesMenuProps = {
  notes: [
    {
      noteText: 'hello this is a note',
      authorFirstName: 'john',
      authorLastName: 'smith',
      authorAvatarThumbnail: 'http://image-url.com'
    },
    {
      noteText: 'this is an amazing note',
      authorFirstName: 'bert',
      authorLastName: 'johnson',
      authorAvatarThumbnail: 'http://test-url.com'
    },
    {
      noteText: 'welcome to this note',
      authorFirstName: 'alvin',
      authorLastName: 'chipmunk',
      authorAvatarThumbnail: 'http://image-test.com'
    }
  ],
  activities: [],
  messageIsSaved: true,
  userTimezone: new TimezoneRecord(),
  userFirstName: 'test',
  userLastName: 'user',
  initialNoteText: 'this is a note',
  createNoteForSavedMessage: mockFunc,
  createPendingNote: mockFunc,
  disableInput: false
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<NotesMenu {...props} />);
  mountedWrapper = mount(<NotesMenu {...props} />);
}

describe('Notes Menu Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const menuWrapper = shallowWrapper.find(`.${styles.notesMenu}`);
    expect(menuWrapper.length).toEqual(1);

    const menu = menuWrapper.find(ProductNav);
    expect(menu.length).toEqual(1);

    const inputArea = menuWrapper.find(`.${styles.noteInputArea}`);
    expect(inputArea.find(NoteInput).length).toEqual(1);

    const menuItems = menu.find(ProductNavItem);
    expect(menuItems.length).toEqual(2);
    expect(menuItems.at(0).children().text().toLowerCase()).toEqual('notes');
  });

  it('should render notes', () => {
    render({
      messageIsSaved: false
    });
    const notesList = shallowWrapper.find(`.${styles.notesList}`);
    expect(notesList.length).toEqual(1);
    expect(notesList.hasClass(styles.scrolledToTop)).toEqual(true);

    const notes = notesList.find(Note);
    expect(notes.length).toEqual(3);
  });

  it('should render an element to indicate the top of the notes list', () => {
    expect(mountedWrapper.instance().notesListTop).toBeDefined();
  });

  it('should render correctly when the notes list is scrolled down and then back up', () => {
    const notesList = shallowWrapper.find(`.${styles.notesList}`);

    notesList.simulate('scroll', { target: { scrollTop: 10 } });
    expect(shallowWrapper.find(`.${styles.notesList}.${styles.scrolledToTop}`).length).toEqual(0);

    notesList.simulate('scroll', { target: { scrollTop: 0 } });
    expect(shallowWrapper.find(`.${styles.notesList}.${styles.scrolledToTop}`).length).toEqual(1);
  });

  it('should render a placeholder image when no notes are present', () => {
    render({
      notes: []
    });
    const notes = shallowWrapper.find(Note);
    expect(notes.length).toEqual(0);

    const placeholder = shallowWrapper.find(`.${styles.noNotesPlaceholder}`);
    expect(placeholder.length).toEqual(1);

    const placeholderImage = placeholder.find('img');
    expect(placeholderImage.length).toEqual(1);
    expect(placeholderImage.prop('src')).toEqual(noNotesImage);

    const placeholderBanner = placeholder.find(`.${styles.noNotesBanner}`);
    expect(placeholderBanner.text()).toEqual(NO_NOTES_OR_ACTIVITY);
  });

  it('should pass appropriate props to notes', () => {
    const notes = shallowWrapper.find(Note);
    notes.forEach((note, index) => {
      // expect(note.prop('note')).toDeepEqual(mainProps.notes[index]);
      expect(note.props()).toDeepEqual({
        authorFirstName: mainProps.notes[index].authorFirstName,
        authorLastName: mainProps.notes[index].authorLastName,
        authorAvatarThumbnail: mainProps.notes[index].authorAvatarThumbnail,
        timestamp: mainProps.notes[index].noteCreationTimestamp,
        authoredContent: mainProps.notes[index].noteText,
        authoredContentMaxChars: MAX_NOTE_CHARS, //the default prop
        userTimezone: mainProps.userTimezone
      });
      expect(note.prop('userTimezone')).toEqual(mainProps.userTimezone);
    });
  });

  it('should pass appropriate props to the note input', () => {
    const noteInput = shallowWrapper.find(NoteInput);
    expect(typeof noteInput.prop('onCurrentNoteTextChange')).toEqual('function');
    expect(noteInput.prop('createPendingNote')).toEqual(mockFunc);
    expect(noteInput.prop('createNoteForSavedMessage')).toEqual(mockFunc);
    expect(noteInput.prop('messageIsSaved')).toEqual(true);
    expect(noteInput.prop('userFirstName')).toEqual('test');
    expect(noteInput.prop('userLastName')).toEqual('user');
    expect(noteInput.prop('currentNoteText')).toEqual('this is a note');
    expect(typeof noteInput.prop('onTextAreaBlur')).toEqual('function');
    expect(typeof noteInput.prop('onTextAreaFocus')).toEqual('function');
    expect(noteInput.prop('showFooter')).toEqual(true);
    expect(typeof noteInput.prop('onAddNote')).toEqual('function');
  });

  it('should show the input footer when the input area is focused', () => {
    render({
      initialNoteText: ''
    });
    const noteInput = mountedWrapper.find(NoteInput);
    expect(mountedWrapper.find(`.${styles.notesList}.${styles.noteInputFooterVisible}`).length).toEqual(0);
    expect(mountedWrapper.find(`.${styles.noteInputArea}.${styles.noteInputFooterVisible}`).length).toEqual(0);
    expect(noteInput.prop('showFooter')).toEqual(false);

    const textArea = noteInput.find('textarea');

    textArea.simulate('focus');

    expect(mountedWrapper.find(`.${styles.noteInputArea}.${styles.noteInputFooterVisible}`).length).toEqual(1);
    expect(noteInput.prop('showFooter')).toEqual(true);
  });

  it('should show the input footer when there is note text', () => {
    render({
      initialNoteText: 'test'
    });
    const noteInput = mountedWrapper.find(NoteInput);

    expect(mountedWrapper.find(`.${styles.noteInputArea}.${styles.noteInputFooterVisible}`).length).toEqual(1);
    expect(noteInput.prop('showFooter')).toEqual(true);
  });

  it('should show the input footer when the input area is focused and there is note text', () => {
    render({
      initialNoteText: 'test'
    });
    const noteInput = mountedWrapper.find(NoteInput);
    const textArea = noteInput.find('textarea');

    textArea.simulate('focus');

    expect(mountedWrapper.find(`.${styles.noteInputArea}.${styles.noteInputFooterVisible}`).length).toEqual(1);
    expect(noteInput.prop('showFooter')).toEqual(true);
  });

  it('should disable the input based on the "disableInput" prop', () => {
    render({
      disableInput: false
    });
    expect(shallowWrapper.find(NoteInput).prop('disabled')).toEqual(false);

    render({
      disableInput: true
    });
    expect(shallowWrapper.find(NoteInput).prop('disabled')).toEqual(true);
  });

  it('should reset text when initial note text changes', () => {
    render({
      initialNoteText: 'test'
    });
    expect(shallowWrapper.state('currentNoteText')).toEqual('test');
    let noteInput = shallowWrapper.find(NoteInput);
    expect(noteInput.prop('currentNoteText')).toEqual('test');
    shallowWrapper.setProps({
      initialNoteText: 'example'
    });
    expect(shallowWrapper.state('currentNoteText')).toEqual('example');
    noteInput = shallowWrapper.find(NoteInput);
    expect(noteInput.prop('currentNoteText')).toEqual('example');
  });
});
