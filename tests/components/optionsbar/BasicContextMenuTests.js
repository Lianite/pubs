/* @flow */
import React from 'react';
import { shallow } from 'enzyme';
import BasicsContextMenu from 'components/optionsbar/BasicsContextMenu';
import PlanPicker from 'components/optionsbar/PlanPicker';
import LabelsWrapper from 'components/optionsbar/LabelsWrapper';
import { List as ImmutableList } from 'immutable';

import styles from 'styles/modules/components/optionsbar/BasicsContextMenu.scss';

const mockFunc = () => {};

const mainProps = {
  currentPlan: {
    id: '3',
    name: 'this plan',
    color: '#ddd'
  },
  onSelectPlan: mockFunc,
  plans: [{
    id: '1',
    name: 'plan',
    color: '#fff'
  }],
  selectedLabels: [{
    id: 1,
    title: 'name'
  }],
  availableLabels: [{
    id: 2,
    title: 'title'
  }],
  onRemoveLabel: mockFunc,
  onLabelSelect: mockFunc,
  fetchLabelsAction: mockFunc,
  addLabel: mockFunc,
  updateLabelText: mockFunc,
  currentLabelText: 'test',
  canCreateLabels: true,
  labelErrorGroup: ImmutableList([])
};

describe('Basics Context Menu Component', () => {
  let shallowWrapper;

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<BasicsContextMenu {...props} />);
  };

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const menu = shallowWrapper.find(`.${styles.basicsContextMenu}`);
    expect(menu.length).toEqual(1);

    const title = menu.find(`.${styles.title}`);
    expect(title.text().toLowerCase()).toEqual('basics');

    const labels = menu.find(LabelsWrapper);
    expect(labels.props()).toDeepEqual({
      selectedLabels: mainProps.selectedLabels,
      availableLabels: mainProps.availableLabels,
      onRemoveLabel: mockFunc,
      addLabel: mockFunc,
      updateLabelText: mockFunc,
      fetchLabelsAction: mockFunc,
      labelErrorGroup: mainProps.labelErrorGroup,
      canCreateLabels: true,
      currentLabelText: 'test'
    });

    const plans = menu.find(PlanPicker);
    expect(plans.props()).toDeepEqual({
      currentPlan: mainProps.currentPlan,
      onSelectPlan: mockFunc,
      plans: mainProps.plans
    });
  });

});
