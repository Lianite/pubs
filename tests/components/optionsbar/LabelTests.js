/* @flow */
import React from 'react';
import Label from 'components/util/Label';
import { shallow } from 'enzyme';

type Props = {
  children?: any,
  id?: number,
  onRemove: Function,
  error: bool
};

const mainProps: Props = {
  children: 'Test',
  id: 1,
  onRemove: () => {},
  error: false
};

let wrapper;

function render(props: Props) {
  wrapper = shallow(<Label {...props}/>);
}

describe('Label Component', () => {

  beforeEach(() => {
    render(mainProps);
  });

  it('should render a Label', () => {
    expect(!!wrapper).toBe(true);
    expect(wrapper.html()).toEqual('<div class="Label-label"><div class="Label-labelName">Test</div><span class="Label-closeButton icon-modal_close"></span></div>');
  });
});
