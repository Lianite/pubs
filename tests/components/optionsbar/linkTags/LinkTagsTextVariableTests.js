/* @flow */
/*global spyOn:true*/

import React from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import LinkTagsTextVariable from 'components/optionsbar/linkTags/LinkTagsTextVariable';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';

const Wrap = class Wrap extends React.Component {
  render() {
    return <LinkTagsTextVariable {...this.props} />;
  }
};

const node = document.createElement('div');

const mainProps = {
  link: 'google.com',
  tagVariable: ImmutableMap({
    id: 0,
    name: 'aTextVar',
    type: 'text',
    variableDescription: ImmutableMap({
      fieldName: 'Da Field',
      required: false
    })
  }),
  variableValue: ImmutableMap({
    variableId: 0,
    values: ImmutableList([ImmutableMap({
      value: 'Field Value'
    })])
  }),
  updateLinkTagVariable: () => {}
};

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((
    <Wrap {...props} />
    ), node);
}

describe('Link Tag Text Variable Component', () => {
  let component;

  beforeEach(() => {
    spyOn(mainProps, 'updateLinkTagVariable');
    component = render(mainProps);
  });

  it('should render some elements', () => {
    expect(ReactTestUtils.isDOMComponent(findDOMNode(component))).toBeTruthy();
  });

  it('should render Text Variable component', () => {
    const el = findDOMNode(component);
    expect(el).toBeTruthy();

    const variableLabel = el.querySelector('label');
    expect(variableLabel).toBeTruthy();

    const variableInput = el.querySelector('input[type=text]');
    expect(variableInput).toBeTruthy();

  });

  it('should render a variable with "Da Field" as label', () => {
    const el = findDOMNode(component);
    const variableLabel = el.querySelector('label');
    expect(variableLabel).toBeTruthy();
    expect(variableLabel.textContent).toEqual('Da Field:');
  });

  it('should render variable with a required indicator', () => {
    let component = render({
      tagVariable: ImmutableMap({
        id: 0,
        name: 'aTextVar',
        type: 'text',
        variableDescription: ImmutableMap({
          fieldName: 'Da Field',
          required: true
        })
      })
    });
    const el = findDOMNode(component);
    const variableLabel = el.querySelector('label');
    expect(variableLabel).toBeTruthy();
    expect(variableLabel.textContent).toEqual('Da Field *:');
  });

  it('should call "updateLinkTagVariable" when the text changes', (done) => {
    let component = render({
      updateLinkTagVariable: (link, id, value) => {
        let val = value.pop();
        expect(val.value).toEqual('var value');
        done();
      }
    });

    const el = findDOMNode(component);
    const variableInput = el.querySelector('input[type=text]');
    ReactTestUtils.Simulate.change(variableInput, {target: {value: 'var value'}});
  });
});
