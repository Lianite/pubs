/* @flow */
/*global spyOn:true*/

import React from 'react';
import ReactDOM, { findDOMNode } from 'react-dom';
import LinkTagsVariables from 'components/optionsbar/linkTags/LinkTagsVariables';
import { Map as ImmutableMap, List as ImmutableList } from 'immutable';

const Wrap = class Wrap extends React.Component {
  render() {
    return <LinkTagsVariables {...this.props} />;
  }
};

const node = document.createElement('div');

const mainProps = {
  link: 'google.com',
  linkTag: {
    tagVariables: ImmutableList([
      ImmutableMap({
        id: 0,
        name: 'aTextVar',
        type: 'text',
        variableDescription: ImmutableMap({
          fieldName: 'Da Field',
          required: false
        })
      })
    ]),
    variableValues: ImmutableList([
      ImmutableMap({
        variableId: 0,
        values: ImmutableList([ImmutableMap({
          value: 'Field Value'
        })])
      })
    ])
  },
  updateLinkTagVariable: () => {}
};

function render(props = {}) {
  props = {...mainProps, ...props};
  return ReactDOM.render((
    <Wrap {...props} />
  ), node);
}

describe('Link Tag Variables Component', () => {
  let component;

  beforeEach(() => {
    component = render();
  });

  it('should render text tag variable', () => {
    const el = findDOMNode(component);
    expect(el).toBeTruthy();

    const textVariable = el.querySelector('.LinkTagsVariables-linkTagVariable');
    expect(textVariable).toBeTruthy();
  });
});
