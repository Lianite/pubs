/* @flow  */
import React from 'react';
import NoteInput from 'components/optionsbar/NoteInput';
import { shallow } from 'enzyme';
import styles from 'styles/modules/components/optionsbar/NoteInput.scss';

import type { NoteInputProps } from 'components/optionsbar/NoteInput';

let shallowWrapper;

const mockFunc = () => {};

const mainProps : NoteInputProps = {
  onCurrentNoteTextChange: mockFunc,
  createPendingNote: mockFunc,
  createNoteForSavedMessage: mockFunc,
  messageIsSaved: true,
  userFirstName: 'test',
  userLastName: 'user',
  userAvatar: 'http://avatar.com',
  showFooter: false,
  onTextAreaFocus: mockFunc,
  onTextAreaBlur: mockFunc,
  currentNoteText: '',
  onAddNote: mockFunc,
  disabled: false
};

const testRenderFundamentals = () => {
  const noteInputWrapper = shallowWrapper.find(`.${styles.noteInputWrapper}`);
  expect(noteInputWrapper.length).toEqual(1);

  const noteInput = noteInputWrapper.find(`.${styles.noteInput}`);
  expect(noteInput.length).toEqual(1);
  expect(noteInput.prop('placeholder')).toEqual('Add a note...');
  expect(noteInput.prop('disabled')).toEqual(false);

  return noteInputWrapper;
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<NoteInput {...props} />);
}

describe('Notes Menu Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const noteInputWrapper = testRenderFundamentals();
    expect(noteInputWrapper.find(`.${styles.noteInput}.${styles.footerVisible}`).length).toEqual(0);
    expect(noteInputWrapper.find(`.${styles.footer}`).length).toEqual(0);
  });

  it('should render correctly when footer is visible', () => {
    render({
      showFooter: true
    });
    const noteInputWrapper = testRenderFundamentals();

    const footer = noteInputWrapper.find(`.${styles.footer}`);
    expect(footer.length).toEqual(1);

    const addNoteButton = footer.find(`.${styles.addNote}`);
    expect(addNoteButton.length).toEqual(1);
    expect(addNoteButton.prop('disabled')).toEqual(true);
    expect(addNoteButton.text()).toEqual('Save Note');
  });

  it('should call "onCurrentNoteTextChange" when the text area changes', () => {
    const onCurrentNoteTextChangeSpy = jasmine.createSpy('onCurrentNoteTextChange');
    render({
      onCurrentNoteTextChange: onCurrentNoteTextChangeSpy
    });
    shallowWrapper.find(`.${styles.noteInput}`).simulate('change', { target: { value: 'note text' } });
    expect(onCurrentNoteTextChangeSpy).toHaveBeenCalled();
    const spyArgs = onCurrentNoteTextChangeSpy.calls.mostRecent().args;
    expect(spyArgs[0]).toEqual('note text');
  });

  it('should call "onTextAreaFocus" when the text area changes', () => {
    const onTextAreaFocus = jasmine.createSpy('onTextAreaFocus');
    render({
      onTextAreaFocus
    });
    shallowWrapper.find(`.${styles.noteInput}`).simulate('focus');
    expect(onTextAreaFocus).toHaveBeenCalled();
  });

  it('should call "onTextAreaBlur" when the text area changes', () => {
    const onTextAreaBlur = jasmine.createSpy('onTextAreaBlur');
    render({
      onTextAreaBlur
    });
    shallowWrapper.find(`.${styles.noteInput}`).simulate('blur');
    expect(onTextAreaBlur).toHaveBeenCalled();
  });

  it('should call "createPendingNote" and "onAddNote" when button is clicked and message is not saved', () => {
    const createPendingNoteSpy = jasmine.createSpy('createPendingNote');
    const onAddNoteSpy = jasmine.createSpy('onAddNote');
    render({
      showFooter: true,
      messageIsSaved: false,
      currentNoteText: 'this is the current text',
      createPendingNote: createPendingNoteSpy,
      onAddNote: onAddNoteSpy
    });

    shallowWrapper.find(`.${styles.addNote}`).simulate('click');
    expect(createPendingNoteSpy).toHaveBeenCalled();
    const spyArgs = createPendingNoteSpy.calls.mostRecent().args;
    expect(spyArgs[0]).toEqual(jasmine.objectContaining({
      authorFirstName: 'test',
      authorLastName: 'user',
      authorAvatarThumbnail: 'http://avatar.com',
      noteText: 'this is the current text'
    }));
    expect(typeof spyArgs[0].noteCreationTimestamp).toEqual('number');
    expect(onAddNoteSpy).toHaveBeenCalled();
  });

  it('should call "createNoteForSavedMessage" when button is clicked and message is saved', () => {
    const createNoteForSavedMessageSpy = jasmine.createSpy('createNoteForSavedMessage');
    const onAddNoteSpy = jasmine.createSpy('onAddNote');
    render({
      showFooter: true,
      messageIsSaved: true,
      createNoteForSavedMessage: createNoteForSavedMessageSpy,
      currentNoteText: 'test text',
      onAddNote: onAddNoteSpy
    });

    shallowWrapper.find(`.${styles.noteInput}`).simulate('change', { target: { value: 'note text' } });
    shallowWrapper.find(`.${styles.addNote}`).simulate('click');
    expect(createNoteForSavedMessageSpy).toHaveBeenCalled();
    const spyArgs = createNoteForSavedMessageSpy.calls.mostRecent().args;
    expect(spyArgs[0]).toEqual('test text');
    expect(onAddNoteSpy).toHaveBeenCalled();
  });

  it('should disable the textarea and button when component is disabled', () => {
    render({
      disabled: true,
      currentNoteText: 'test',
      showFooter: true
    });
    expect(shallowWrapper.find(`.${styles.noteInput}`).prop('disabled')).toEqual(true);
    expect(shallowWrapper.find(`.${styles.addNote}`).prop('disabled')).toEqual(true);
  });
});
