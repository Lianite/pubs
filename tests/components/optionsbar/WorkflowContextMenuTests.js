/* @flow */
import React from 'react';
import WorkflowContextMenu from 'components/optionsbar/WorkflowContextMenu';
import AssigneeDropDown from 'components/optionsbar/AssigneeDropDown';
import FeaturePlaceholder from 'components/util/FeaturePlaceholder';
import approvalsPlaceholderImage from 'images/approvals-placeholder.svg';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/optionsbar/WorkflowContextMenu.scss';

let mockfunc = () => {
  return true;
};

const mainProps = {
  currentAssignee: {
    id: 0,
    firstName: 'Test',
    lastName: 'McTest'
  },
  onSelectAssignee: mockfunc,
  resendValidAssigneesRequest: mockfunc,
  assignees: [],
  invalidAssignees: [],
  unavailableVoices: []
};

let wrapper;

function render(props = mainProps) {
  props = {...mainProps, ...props};
  wrapper = shallow(<WorkflowContextMenu {...props}/>);
}

describe('Workflow Context Menu Component', () => {
  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    expect(wrapper.find(`.${styles.title}`).text().toLowerCase()).toEqual('workflow');
    expect(wrapper.find(AssigneeDropDown).length).toEqual(1);
    expect(wrapper.find(`.${styles.divider}`).length).toEqual(1);
  });

  it('should pass props correctly to AssigneeDropDown', () => {
    expect(wrapper.find(AssigneeDropDown).props()).toEqual(mainProps);
  });

  it('should render a placeholder for Approvals', () => {
    const approvals = wrapper.find(FeaturePlaceholder);
    expect(approvals.props()).toDeepEqual({
      image: approvalsPlaceholderImage,
      header: 'Approvals are arriving soon!',
      body: 'Send your message into an approval path and more - coming soon!'
    });
  });
});
