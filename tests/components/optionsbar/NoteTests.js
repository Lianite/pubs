import React from 'react';
import {
  default as Note,
  AuthorAvatar,
  ReadMoreToggle,
  NoteBody
} from 'components/optionsbar/Note';
import { shallow } from 'enzyme';
import {
  default as ReactTestUtils,
  findRenderedDOMComponentWithClass,
  Simulate
} from 'react-addons-test-utils';
import { findDOMNode } from 'react-dom';
import { TimezoneRecord } from 'reducers/EnvironmentReducer';
import styles from 'styles/modules/components/optionsbar/Note.scss';

import type {
  AuthorAvatarProps,
  ReadMoreToggleProps,
  NoteProps,
  NoteBodyProps
} from 'components/optionsbar/Note';

let shallowWrapper;
let shallowAvatarWrapper;
let shallowReadMoreWrapper;
let shallowNoteBodyWrapper;

let fullRenderedComponent;

const mockFunc = () => {};

const longNoteText = 'this is a very long note that I am writing it is a test it does not have any meaningful content but it still holds information that is only for testing and things';

const mainProps: NoteProps = {
  authoredContent: 'hello this is a note',
  authorFirstName: 'john',
  authorLastName: 'smith',
  authorAvatarThumbnail: 'http://image-url.com',
  timestamp: 1479252304,
  userTimezone: new TimezoneRecord({
    offset: -600
  })
};

const mainAvatarProps: AuthorAvatarProps = {
  authorFirstName: 'john',
  authorLastName: 'smith',
  authorAvatarThumbnail: 'http://image-url.com'
};

const mainReadMoreToggleProps: ReadMoreToggleProps = {
  onClick: mockFunc,
  expanded: false
};

const mainNoteBodyProps: NotebodyProps = {
  authoredContent: mainProps.authoredContent,
  onReadMoreClick: mockFunc,
  expanded: false
};

function render(props: NoteProps = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<Note {...props} />);
}

function renderAvatar(props: AuthorAvatarProps = {}) {
  props = {...mainAvatarProps, ...props};
  shallowAvatarWrapper = shallow(<AuthorAvatar {...props} />);
}

function renderReadMoreToggle(props: ReadMoreToggleProps = {}) {
  props = {...mainReadMoreToggleProps, ...props};
  shallowReadMoreWrapper = shallow(
    <ReadMoreToggle {...props} />);
}

function renderNoteBody(props: NoteBodyProps = {}) {
  props = {...mainNoteBodyProps, ...props};
  shallowNoteBodyWrapper = shallow(<NoteBody {...props} />);
}

function renderFullComponent(props: NoteProps = {}) {
  props = {...mainProps, ...props};
  fullRenderedComponent = ReactTestUtils.renderIntoDocument(<Note {...props}/>);
}

describe('Saved Note Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const noteWrapper = shallowWrapper.find(`.${styles.note}`);
    expect(noteWrapper.length).toEqual(1);

    const noteHeader = noteWrapper.find(`.${styles.header}`);
    expect(noteHeader.length).toEqual(1);

    const avatarWrapper = noteHeader.find(AuthorAvatar);
    expect(avatarWrapper.length).toEqual(1);
    expect(avatarWrapper.prop('authorFirstName')).toEqual('john');
    expect(avatarWrapper.prop('authorLastName')).toEqual('smith');
    expect(avatarWrapper.prop('authorAvatarThumbnail')).toEqual('http://image-url.com');

    const info = noteHeader.find(`.${styles.info}`);
    expect(info.length).toEqual(1);

    const authorName = info.find(`.${styles.authorName}`);
    expect(authorName.length).toEqual(1);
    expect(authorName.text()).toEqual('john smith');

    const timestamp = info.find(`.${styles.timestamp}`);
    expect(timestamp.length).toEqual(1);
    expect(timestamp.text()).toEqual('Nov 15, 5:25pm');

    const body = noteWrapper.find(NoteBody);
    expect(body.length).toEqual(1);
    expect(body.prop('expanded')).toEqual(false);
    expect(body.prop('authoredContent')).toEqual(mainProps.authoredContent);
    expect(body.prop('onReadMoreClick')).toEqual(jasmine.any(Function));

    expect(body.find(ReadMoreToggle).length).toEqual(0);
  });

  describe('author avatar component', () => {
    beforeEach(() => {
      renderAvatar();
    });
    it('should render an avatar correctly', () => {
      const avatar = shallowAvatarWrapper.find(`.${styles.avatar}`);
      expect(avatar.length).toEqual(1);
      expect(avatar.prop('style').backgroundImage).toEqual('url(http://image-url.com)');
    });
    it('should render correctly when no avatar is passed in', () => {
      renderAvatar({
        authorAvatarThumbnail: ''
      });
      const avatar = shallowAvatarWrapper.find(`.${styles.avatarInitials}`);
      expect(avatar.length).toEqual(1);
      expect(avatar.text()).toEqual('j s');
    });
  });

  describe('read more component', () => {
    beforeEach(() => {
      renderReadMoreToggle();
    });
    it('should render correctly when not expanded', () => {
      expect(shallowReadMoreWrapper.find(`.${styles.readMore}`).length).toEqual(1);
      expect(shallowReadMoreWrapper.text()).toEqual('Read More');
    });
    it('should render correctly when expanded', () => {
      renderReadMoreToggle({
        expanded: true
      });
      expect(shallowReadMoreWrapper.find(`.${styles.readMore}`).length).toEqual(1);
      expect(shallowReadMoreWrapper.text()).toEqual('Read Less');
    });
  });

  describe('note body component', () => {
    beforeEach(() => {
      renderNoteBody();
    });
    it('should render correctly when the note is short', () => {
      expect(shallowNoteBodyWrapper.find(`.${styles.body}`).length).toEqual(1);
      expect(shallowNoteBodyWrapper.text()).toEqual('hello this is a note');
      expect(shallowNoteBodyWrapper.find(ReadMoreToggle).length).toEqual(0);
    });
    it('should render correctly when the note is long and not expanded', () => {
      renderNoteBody({
        authoredContent: longNoteText,
        expanded: false
      });
      expect(shallowNoteBodyWrapper.find(`.${styles.body}`).length).toEqual(1);
      expect(shallowNoteBodyWrapper.text()).toContain(`${longNoteText.slice(0, 150)} ...`);
      const readMoreToggle = shallowNoteBodyWrapper.find(ReadMoreToggle);
      expect(readMoreToggle.length).toEqual(1);
      expect(readMoreToggle.prop('expanded')).toEqual(false);
    });
    it('should render correctly when the note is long and not expanded', () => {
      renderNoteBody({
        authoredContent: longNoteText,
        expanded: true
      });
      expect(shallowNoteBodyWrapper.find(`.${styles.body}`).length).toEqual(1);
      expect(shallowNoteBodyWrapper.text()).toContain(longNoteText);
      const readMoreToggle = shallowNoteBodyWrapper.find(ReadMoreToggle);
      expect(readMoreToggle.length).toEqual(1);
      expect(readMoreToggle.prop('expanded')).toEqual(true);
    });
  });

  describe('read more/less toggling', () => {
    beforeEach(() => {
      renderFullComponent({
        authoredContent: longNoteText,
        expanded: false
      });
    });
    it('should expand/collapse the note when read more toggle is clicked', () => {
      const readMoreToggle = findRenderedDOMComponentWithClass(fullRenderedComponent, styles.readMore);
      const readMoreToggleNode = findDOMNode(readMoreToggle);
      const noteBody = findRenderedDOMComponentWithClass(fullRenderedComponent, styles.body);
      const noteBodyNode = findDOMNode(noteBody);

      expect(readMoreToggleNode.textContent).toEqual('Read More');
      expect(noteBodyNode.textContent).toEqual(`${longNoteText.slice(0, 150)} ...Read More`);

      Simulate.click(readMoreToggleNode);

      expect(readMoreToggleNode.textContent).toEqual('Read Less');
      expect(noteBodyNode.textContent).toEqual(`${longNoteText}Read Less`);

      Simulate.click(readMoreToggleNode);

      expect(readMoreToggleNode.textContent).toEqual('Read More');
      expect(noteBodyNode.textContent).toEqual(`${longNoteText.slice(0, 150)} ...Read More`);
    });
  });
});
