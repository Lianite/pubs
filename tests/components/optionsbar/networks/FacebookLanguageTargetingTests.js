/* @flow */
import React from 'react';
import FacebookLanguageTargeting from 'components/optionsbar/networks/FacebookLanguageTargeting';
import { shallow } from 'enzyme';
import DropdownSection from 'components/util/DropdownSection';
import { Combobox } from 'react-widgets';

let shallowWrapper;

const mainProps = {
  onInputChange: jasmine.createSpy('onInputChange'),
  targetingSelectedLanguages: [],
  targetingLanguageOptions: [{
    value: '1',
    description: 'English',
    type: 'LOCALE'
  }, {
    value: '2',
    description: 'Chinese',
    type: 'LOCALE'
  }],
  addTargetingLanguage: () => {},
  removeTargetingLanguage: () => {}
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FacebookLanguageTargeting {...props} />);
}

describe('Facebook Language Targeting Component', () => {

  afterEach(() => {
    mainProps.onInputChange.calls.reset();
  });

  it('should pass selected languages to the dropdown section', () => {
    render({
      targetingSelectedLanguages: [{
        value: '1',
        description: 'English',
        type: 'LOCALE'
      }, {
        value: '2',
        description: 'Chinese',
        type: 'LOCALE'
      }]
    });
    const dropdown = shallowWrapper.find(DropdownSection);
    expect(dropdown.prop('selectedData')).toDeepEqual([{
      text: 'English'
    }, {
      text: 'Chinese'
    }]);
  });

  it('should pass languages as options for the dropdown, in the order they are provided as props', () => {
    expect(shallowWrapper.find(DropdownSection).prop('comboBoxConfig')).toEqual(jasmine.objectContaining({
      data: [{
        value: '1',
        description: 'English',
        type: 'LOCALE'
      }, {
        value: '2',
        description: 'Chinese',
        type: 'LOCALE'
      }]
    }));
  });

  it('should specify the correct behavior for the dropdown', () => {
    const comboBoxConfig = shallowWrapper.find(DropdownSection).prop('comboBoxConfig');
    expect(comboBoxConfig).toEqual(jasmine.objectContaining({
      valueField: 'value',
      textField: 'description',
      placeholder: 'Enter a Language...'
    }));
    expect(comboBoxConfig.messages).toDeepEqual({
      emptyList: 'No languages match your search'
    });
  });

  describe('adding languages', () => {
    it('should correctly add a language', () => {
      const addTargetingLanguage = jasmine.createSpy('addTargetingLanguage');
      render({
        addTargetingLanguage
      });
      const comboBox = shallowWrapper.find(DropdownSection).dive().find(Combobox);
      comboBox.simulate('select', '1');
      expect(addTargetingLanguage).toHaveBeenCalledWith({
        value: '1',
        description: 'English',
        type: 'LOCALE'
      });
    });
  });

  describe('input changes', () => {
    it('should handle change events correctly', () => {
      const dropdown = shallowWrapper.find(DropdownSection);

      expect(mainProps.onInputChange.calls.count()).toEqual(0);

      dropdown.simulate('inputValueChange', 'string');
      expect(mainProps.onInputChange).toHaveBeenCalledWith('string');

      mainProps.onInputChange.calls.reset();

      dropdown.simulate('inputValueChange', {});
      expect(mainProps.onInputChange).not.toHaveBeenCalled();
    });
  });
});
