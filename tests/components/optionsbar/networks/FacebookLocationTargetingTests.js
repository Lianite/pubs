/* @flow */
import React from 'react';
import FacebookLocationTargeting from 'components/optionsbar/networks/FacebookLocationTargeting';
import Location from 'components/optionsbar/networks/targeting/Location';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import DropdownButton from 'components/util/DropdownButton';
import { Combobox } from 'react-widgets';
import { shallow } from 'enzyme';
import _ from 'lodash';

import styles from 'styles/modules/components/optionsbar/networks/FacebookLocationTargeting.scss';

describe('Facebook Location Targeting Component', () => {

  let shallowWrapper;

  const mainProps = {
    onInputChange: jasmine.createSpy('onInputChange'),
    locationOptions: [],
    selectedLocations: {},
    addIncludedLocation: jasmine.createSpy('addIncludedLocation'),
    removeIncludedLocation: () => {},
    removeIncludedCountry: jasmine.createSpy('removeIncludedCountry')
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<FacebookLocationTargeting {...props} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.onInputChange.calls.reset();
    mainProps.addIncludedLocation.calls.reset();
    mainProps.removeIncludedCountry.calls.reset();
  });

  it('should render correctly', () => {
    const header = shallowWrapper.find(OptionsMenuSectionHeader);
    expect(header.length).toEqual(1);
    expect(header.prop('title')).toEqual('Locations');
    expect(header.prop('subheading')).toEqual(true);

    const locations = shallowWrapper.find(`.${styles.locations}`);
    expect(locations.length).toEqual(1);
    expect(locations.hasClass(styles.notEmpty)).toBeFalsy();

    const countries = shallowWrapper.find(`.${styles.country}`);
    expect(countries.length).toEqual(0);

    const input = shallowWrapper.find(`.${styles.inputWrapper}`);
    expect(input.length).toEqual(1);

    const button = input.find(DropdownButton);
    expect(button.length).toEqual(1);
    expect(button.prop('open')).toEqual(false);

    const combobox = input.find(Combobox);
    expect(combobox.length).toEqual(1);
    expect(combobox.props()).toEqual(jasmine.objectContaining({
      value: '',
      data: mainProps.locationOptions,
      valueField: 'value',
      textField: 'description',
      open: false,
      placeholder: 'Enter a Location...'
    }));
    expect(combobox.prop('messages')).toDeepEqual({
      emptyList: 'You can add a country, state/region, city, or postal code.',
      emptyFilter: 'You can add a country, state/region, city, or postal code.'
    });
  });

  it('should handle input button events correctly', () => {
    let combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(false);

    const button = shallowWrapper.find(DropdownButton);
    button.simulate('click');

    combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(true);

    button.simulate('click');

    combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(false);
  });

  it('should handle combobox focus events correctly', () => {
    let combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(false);

    combobox.simulate('focus');

    combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(true);
  });

  it('should handle combobox blur events correctly', () => {
    let combobox = shallowWrapper.find(Combobox);
    combobox.simulate('focus');

    combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(true);

    combobox.simulate('blur');

    combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('open')).toEqual(false);
  });

  it('should pass location options correctly', () => {
    render({
      locationOptions: [{
        value: 'a',
        description: 'b',
        type: 'GEOGRAPHY'
      }, {
        value: 'x',
        description: 'y',
        type: 'GEOGRAPHY'
      }]
    });
    const combobox = shallowWrapper.find(Combobox);
    expect(combobox.prop('data')).toDeepEqual([{
      value: 'a',
      description: 'b',
      type: 'GEOGRAPHY'
    }, {
      value: 'x',
      description: 'y',
      type: 'GEOGRAPHY'
    }]);
  });

  it('should render selected locations', () => {
    const selectedLocations = {
      'United States': {
        description: 'United States',
        children: []
      },
      'United Kingdom': {
        description: 'United Kingdom',
        children: [{
          value: 'LDN',
          description: 'London',
          type: 'GEOGRAPHY',
          subType: 'METRO'
        }]
      }
    };
    render({
      selectedLocations
    });

    expect(shallowWrapper.find(`.${styles.locations}`).hasClass(styles.notEmpty)).toBe(true);

    const selectedLocationsArray = _.map(selectedLocations);

    const countries = shallowWrapper.find(`.${styles.country}`);
    expect(countries.length).toEqual(2);
    countries.forEach((country, countryIndex) => {
      expect(country.text()).toContain(selectedLocationsArray[countryIndex].description);

      country.find(`.${styles.remove}`).simulate('click');
      expect(mainProps.removeIncludedCountry).toHaveBeenCalledWith(selectedLocationsArray[countryIndex].description);

      const locations = country.find(Location);
      if (!selectedLocationsArray[countryIndex].children.length) {
        expect(locations.length).toEqual(1);
        expect(locations.prop('description')).toEqual(selectedLocationsArray[countryIndex].description);
        expect(locations.prop('onRemove')).toEqual(mainProps.removeIncludedLocation);
      } else {
        expect(locations.length).toEqual(selectedLocationsArray[countryIndex].children.length);
        locations.forEach((location, locationIndex) => {
          expect(location.prop('description')).toEqual(selectedLocationsArray[countryIndex].children[locationIndex].description);
          expect(location.prop('onRemove')).toEqual(mainProps.removeIncludedLocation);
        });
      }
    });
  });

  describe('events', () => {
    it('should handle change events correctly when the value is a string', () => {
      let combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('');

      combobox.simulate('change', 'value');

      expect(mainProps.onInputChange).toHaveBeenCalledWith('value');
      combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('value');
    });

    it('should handle change events correctly when the value is a TargetingEntity', () => {
      let combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('');

      combobox.simulate('change', {
        value: 'a',
        description: 'b',
        type: 'GEOGRAPHY'
      });

      expect(mainProps.onInputChange).not.toHaveBeenCalled();
      combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('b');
    });

    it('should handle select events correctly', () => {
      let combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('');

      combobox.simulate('change', 'foo');

      combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('foo');

      combobox.simulate('select', {
        value: 'a',
        description: 'b',
        type: 'GEOGRAPHY'
      });

      expect(mainProps.addIncludedLocation.calls.argsFor(0)).toDeepEqual([{
        value: 'a',
        description: 'b',
        type: 'GEOGRAPHY'
      }]);
      combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('');
    });

    it('should handle a combination of select and change events correctly', () => {
      let combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('');

      combobox.simulate('change', 'test');

      combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('test');

      combobox.simulate('select', {
        value: 'a',
        description: 'b',
        type: 'GEOGRAPHY'
      });

      combobox = shallowWrapper.find(Combobox);
      expect(combobox.prop('value')).toEqual('');
    });
  });
});
