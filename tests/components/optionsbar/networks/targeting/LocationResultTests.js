import React from 'react';
import LocationResult from 'components/optionsbar/networks/targeting/LocationResult';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/optionsbar/networks/targeting/LocationResult.scss';

let shallowWrapper;

describe('Location Result component', () => {
  const mainProps = {
    item: {
      description: 'location',
      subType: 'COUNTRY',
      type: 'GEOGRAPHY',
      value: '1'
    }
  };
  const renderLocationResult = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<LocationResult {...props} />);
  };

  beforeEach(() => {
    renderLocationResult();
  });

  it('should render a country correctly', () => {
    expect(shallowWrapper.find(`.${styles.locationResultDescription}`).text()).toEqual('location');
    expect(shallowWrapper.find(`.${styles.locationResultSubType}`).text()).toEqual('Country');
  });

  it('should render a region correctly', () => {
    renderLocationResult({
      item: {
        description: 'location',
        subType: 'REGION',
        type: 'GEOGRAPHY',
        value: '1'
      }
    });
    expect(shallowWrapper.find(`.${styles.locationResultDescription}`).text()).toEqual('location');
    expect(shallowWrapper.find(`.${styles.locationResultSubType}`).text()).toEqual('Region');
  });

  it('should render a city correctly', () => {
    renderLocationResult({
      item: {
        description: 'location',
        subType: 'METRO',
        type: 'GEOGRAPHY',
        value: '1'
      }
    });
    expect(shallowWrapper.find(`.${styles.locationResultDescription}`).text()).toEqual('location');
    expect(shallowWrapper.find(`.${styles.locationResultSubType}`).text()).toEqual('City');
  });

  it('should render a zip correctly', () => {
    renderLocationResult({
      item: {
        description: 'location',
        subType: 'ZIP_CODE',
        type: 'GEOGRAPHY',
        value: '1'
      }
    });
    expect(shallowWrapper.find(`.${styles.locationResultDescription}`).text()).toEqual('location');
    expect(shallowWrapper.find(`.${styles.locationResultSubType}`).text()).toEqual('Zip Code');
  });
});
