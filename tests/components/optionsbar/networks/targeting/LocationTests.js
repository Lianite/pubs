/* @flow */
import React from 'react';
import Location from 'components/optionsbar/networks/targeting/Location';
import { shallow } from 'enzyme';

import styles from 'styles/modules/components/optionsbar/networks/targeting/Location.scss';

describe('Facebook Location Targeting Component', () => {

  let shallowWrapper;

  const mainProps = {
    description: 'My Location',
    onRemove: jasmine.createSpy('onRemove')
  };

  const render = (props = mainProps) => {
    props = {
      ...mainProps,
      ...props
    };
    shallowWrapper = shallow(<Location {...props} />);
  };

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.onRemove.calls.reset();
  });

  it('should render correctly', () => {
    expect(shallowWrapper.text()).toEqual('My Location');

    const icon = shallowWrapper.find('i');
    expect(icon.hasClass('icon-location')).toBe(true);

    const remove = shallowWrapper.find(`.${styles.remove}`);
    expect(remove.length).toEqual(1);
  });

  it('should handle removals correctly', () => {
    const remove = shallowWrapper.find(`.${styles.remove}`);
    remove.simulate('click');

    expect(mainProps.onRemove).toHaveBeenCalledWith('My Location');
  });
});
