/* @flow */
import React from 'react';
import FacebookDarkpost from 'components/optionsbar/networks/FacebookDarkpost';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import { shallow } from 'enzyme';
import AlertBox from 'components/util/AlertBox';
import { CredentialRecord } from 'records/AccountsRecord';

let shallowWrapper;

const mainProps = {
  canDarkPost: true,
  darkPostStatus: false,
  invalidDarkPostAccounts: [],
  setDarkPostVisibility: () => {}
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FacebookDarkpost {...props} />);
}

describe('Facebook Darkpost Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const darkpostCheckbox = shallowWrapper.find(Checkbox);
    expect(darkpostCheckbox.length).toEqual(1);
    expect(darkpostCheckbox.prop('disabled')).toEqual(!mainProps.canDarkPost);
    expect(darkpostCheckbox.prop('checked')).toEqual(mainProps.darkPostStatus);
  });

  it('should render correctly when dark posting is disabled and there is one invalid account', () => {
    const invalidDarkPostAccounts = [
      new CredentialRecord({
        name: 'planet express'
      })
    ];
    render({
      canDarkPost: false,
      invalidDarkPostAccounts
    });

    const darkpostCheckbox = shallowWrapper.find(Checkbox);
    expect(darkpostCheckbox.length).toEqual(0);

    const alert = shallowWrapper.find(AlertBox);
    expect(alert.prop('text')).toEqual('Dark Posting Not Available');
    expect(alert.prop('additionalText')).toEqual('You\'ve selected 1 account that doesn\'t support Dark Posting.');
    expect(alert.prop('soft')).toEqual(true);
  });

  it('should render correctly when dark posting is disabled and there are multiple invalid accounts', () => {
    const invalidDarkPostAccounts = [
      new CredentialRecord({
        name: 'planet express'
      }),
      new CredentialRecord({
        name: 'fry'
      })
    ];
    render({
      canDarkPost: false,
      invalidDarkPostAccounts
    });
    const darkpostCheckbox = shallowWrapper.find(Checkbox);
    expect(darkpostCheckbox.length).toEqual(0);

    const alert = shallowWrapper.find(AlertBox);
    expect(alert.prop('text')).toEqual('Dark Posting Not Available');
    expect(alert.prop('additionalText')).toEqual('You\'ve selected 2 accounts that don\'t support Dark Posting.');
    expect(alert.prop('soft')).toEqual(true);
  });
});
