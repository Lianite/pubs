/* @flow */
import React from 'react';
import FacebookMenu from 'components/optionsbar/networks/FacebookMenu';
import FacebookTargeting from 'components/optionsbar/networks/FacebookTargeting';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import FeaturePlaceholder from 'components/util/FeaturePlaceholder';
import { shallow } from 'enzyme';
import styles from 'styles/modules/components/optionsbar/networks/FacebookMenu.scss';
import { GenderState } from 'records/TargetingRecords';

import targetingPlaceholderImage from 'images/targeting-placeholder.svg';

let shallowWrapper;

const mainProps = {
  onLanguageTargetingInputChange: () => {},
  targetingSelectedLanguages: [],
  targetingLanguageOptions: [{
    value: '1',
    description: 'English',
    type: 'LOCALE'
  }, {
    value: '2',
    description: 'Chinese',
    type: 'LOCALE'
  }],
  targetingSelectedLocations: {},
  targetingLocationOptions: [],
  onLocationTargetingInputChange: () => {},
  addTargetingIncludedLocation: () => {},
  removeTargetingIncludedLocation: () => {},
  removeTargetingIncludedCountry: () => {},
  canTarget: true,
  targetingSet: false,
  invalidTargetingAccounts: [],
  invalidDarkPostAccounts: [],
  darkPostStatus: false,
  canDarkPost: true,
  setDarkPostVisibility: () => {},
  removeTargetingLanguage: () => {},
  clearTargeting: () => {},
  addTargetingLanguage: () => {},
  targetingIsEnabled: true,
  selectGenderTargeting: () => {},
  targetingSelectedGender: new GenderState()
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FacebookMenu {...props} />);
}

describe('Facebook Menu Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const header = shallowWrapper.find(`.${styles.header}`);
    expect(header.length).toEqual(1);
    expect(header.text().toLowerCase()).toEqual('facebook');

    const targeting = shallowWrapper.find(FacebookTargeting);
    expect(targeting.props()).toDeepEqual({
      canTarget: true,
      targetingSet: false,
      onLanguageTargetingInputChange: mainProps.onLanguageTargetingInputChange,
      targetingSelectedLanguages: mainProps.targetingSelectedLanguages,
      targetingLanguageOptions: mainProps.targetingLanguageOptions,
      addTargetingLanguage: mainProps.addTargetingLanguage,
      removeTargetingLanguage: mainProps.removeTargetingLanguage,
      invalidTargetingAccounts: mainProps.invalidTargetingAccounts,
      clearTargeting: mainProps.clearTargeting,
      onLocationTargetingInputChange: mainProps.onLocationTargetingInputChange,
      addIncludedLocation: mainProps.addTargetingIncludedLocation,
      removeIncludedLocation: mainProps.removeTargetingIncludedLocation,
      removeIncludedCountry: mainProps.removeTargetingIncludedCountry,
      locationOptions: mainProps.targetingLocationOptions,
      selectedLocations: mainProps.targetingSelectedLocations,
      selectGenderTargeting: mainProps.selectGenderTargeting,
      targetingSelectedGender: mainProps.targetingSelectedGender
    });
  });

  describe('Targeting', () => {

    it('should render correctly when targeting is disabled entirely', () => {
      render({
        targetingIsEnabled: false
      });

      const placeholder = shallowWrapper.find(FeaturePlaceholder);
      expect(placeholder.length).toEqual(1);
      expect(placeholder.props()).toDeepEqual({
        image: targetingPlaceholderImage,
        header: 'Targeting is arriving soon!',
        body: 'Target your audience by interest, location, and other demographics.'
      });

      expect(shallowWrapper
        .find(OptionsMenuSectionHeader)
        .findWhere((wrapper) => wrapper.prop('title') && wrapper.prop('title').toLowerCase() === 'targeting')
      .length).toEqual(0);
    });
  });
});
