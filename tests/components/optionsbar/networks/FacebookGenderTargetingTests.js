/* @flow */
import React from 'react';
import FacebookGenderTargeting from 'components/optionsbar/networks/FacebookGenderTargeting';
import { shallow } from 'enzyme';
import styles from 'styles/modules/components/optionsbar/networks/FacebookGenderTargeting.scss';
import LabeledRadioButton from '@spredfast/react-lib/lib/LabeledRadioButton';
import { FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS } from 'constants/UITextConstants';
import { GenderState } from 'records/TargetingRecords';

let shallowWrapper;

const mainProps = {
  selectGenderTargeting: () => {},
  targetingSelectedGender: new GenderState({
    value: '1',
    description: 'Male'
  })
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FacebookGenderTargeting {...props} />);
}

describe('Facebook Gender Targeting Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const header = shallowWrapper.find(`.${styles.facebookGenderTargetingContainer}`);
    expect(header.length).toEqual(1);

    const radioButtons = shallowWrapper.find(LabeledRadioButton);
    expect(radioButtons.length).toEqual(3);
  });

  it('should trigger a selectGenderTargeting when a radiobutton is clicked', () => {
    const selectGenderTargeting = jasmine.createSpy('selectGenderTargeting');
    render({
      selectGenderTargeting,
      targetingSelectedGender: new GenderState({
        value: '0',
        description: 'All'
      })
    });

    const radioButtons = shallowWrapper.find(LabeledRadioButton);
    expect(selectGenderTargeting).not.toHaveBeenCalled();

    // RadioButton 0 is targeting All
    radioButtons.at(0).simulate('change');
    expect(selectGenderTargeting).toHaveBeenCalledWith(FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS.All);
    // RadioButton 1 is targeting Men
    radioButtons.at(1).simulate('change');
    expect(selectGenderTargeting).toHaveBeenCalledWith(FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS.Male);
    // RadioButton 2 is targeting Women
    radioButtons.at(2).simulate('change');
    expect(selectGenderTargeting).toHaveBeenCalledWith(FACEBOOK_TARGETING_GENDER_RADIO_OPTIONS.Female);
  });
});
