/* @flow */
import React from 'react';
import FacebookTargeting from 'components/optionsbar/networks/FacebookTargeting';
import {DisabledAccountsList} from 'components/util/DisabledAccountsList';
import FacebookLanguageTargeting from 'components/optionsbar/networks/FacebookLanguageTargeting';
import FacebookGenderTargeting from 'components/optionsbar/networks/FacebookGenderTargeting';
import { shallow } from 'enzyme';
import styles from 'styles/modules/components/optionsbar/networks/FacebookTargeting.scss';
import OptionsMenuSectionHeader from 'components/util/OptionsMenuSectionHeader';
import AlertBox from 'components/util/AlertBox';
import { CredentialRecord } from 'records/AccountsRecord';
import { GenderState } from 'records/TargetingRecords';

let shallowWrapper;

const mainProps = {
  onLanguageTargetingInputChange: () => {},
  targetingSelectedLanguages: [],
  targetingLanguageOptions: [{
    value: '1',
    description: 'English',
    type: 'LOCALE'
  }, {
    value: '2',
    description: 'Chinese',
    type: 'LOCALE'
  }],
  selectedLocations: {},
  locationOptions: [],
  addIncludedLocation: () => {},
  removeIncludedLocation: () => {},
  removeIncludedCountry: () => {},
  onLocationTargetingInputChange: () => {},
  canTarget: true,
  targetingSet: false,
  invalidTargetingAccounts: [],
  clearTargeting: () => {},
  addTargetingLanguage: () => {},
  removeTargetingLanguage: () => {},
  selectGenderTargeting: () => {},
  targetingSelectedGender: new GenderState()
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<FacebookTargeting {...props} />);
}

describe('Facebook Targeting Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {

    const targetingHeader = shallowWrapper
      .find(OptionsMenuSectionHeader)
      .findWhere((wrapper) => wrapper.prop('title') && wrapper.prop('title').toLowerCase() === 'targeting');
    expect(targetingHeader.length).toEqual(1);
    expect(targetingHeader.prop('children')).toBeDefined();

    const language = shallowWrapper.find(FacebookLanguageTargeting);
    expect(language.length).toEqual(1);
    expect(language.props()).toDeepEqual({
      onInputChange: mainProps.onLanguageTargetingInputChange,
      addTargetingLanguage: mainProps.addTargetingLanguage,
      removeTargetingLanguage: mainProps.removeTargetingLanguage,
      targetingSelectedLanguages: mainProps.targetingSelectedLanguages,
      targetingLanguageOptions: mainProps.targetingLanguageOptions
    });

    const gender  = shallowWrapper.find(FacebookGenderTargeting);
    expect(gender.length).toEqual(1);
    expect(gender.props()).toDeepEqual({
      selectGenderTargeting: mainProps.selectGenderTargeting,
      targetingSelectedGender: mainProps.targetingSelectedGender
    });
  });

  it('should render correctly when targeting is disabled and there is one invalid account', () => {
    const invalidTargetingAccounts = [
      new CredentialRecord({
        name: 'planet express'
      })
    ];
    render({
      canTarget: false,
      invalidTargetingAccounts
    });
    const targetingHeader = shallowWrapper
      .find(OptionsMenuSectionHeader)
      .findWhere((wrapper) => wrapper.prop('title') && wrapper.prop('title').toLowerCase() === 'targeting');
    expect(targetingHeader.length).toEqual(1);
    expect(targetingHeader.prop('children')).toBeDefined();

    const language = shallowWrapper.find(FacebookLanguageTargeting);
    expect(language.length).toEqual(0);

    const alert = shallowWrapper.find(AlertBox);
    expect(alert.prop('text')).toEqual('Targeting Not Available');
    expect(alert.prop('additionalText')).toEqual('You\'ve selected 1 account that doesn\'t support Targeting.');
    expect(alert.prop('soft')).toEqual(true);

    const link = alert.prop('link');
    expect(link.type).toEqual(DisabledAccountsList);
    expect(link.props.invalidAccounts).toDeepEqual(invalidTargetingAccounts);
  });

  it('should render correctly when targeting is set but also disabled', () => {
    const clearTargeting = jasmine.createSpy('clearTargeting');
    const invalidTargetingAccounts = [
      new CredentialRecord({
        name: 'planet express'
      }),
      new CredentialRecord({
        name: 'fry'
      })
    ];
    render({
      canTarget: false,
      targetingSet: true,
      targetingSelectedLanguages: [{
        value: '1',
        description: 'English',
        type: 'LOCALE'
      }, {
        value: '2',
        description: 'Chinese',
        type: 'LOCALE'
      }],
      invalidTargetingAccounts,
      clearTargeting
    });

    const targetingHeader = shallowWrapper
      .find(OptionsMenuSectionHeader)
      .findWhere((wrapper) => wrapper.prop('title') && wrapper.prop('title').toLowerCase() === 'targeting');
    expect(targetingHeader.length).toEqual(1);
    expect(targetingHeader.prop('children')).toBeDefined();

    const language = shallowWrapper.find(FacebookLanguageTargeting);
    expect(language.length).toEqual(0);

    const alert = shallowWrapper.find(AlertBox);
    expect(alert.prop('text')).toEqual('Targeting Not Available');
    expect(alert.prop('additionalText')).toEqual('Not all of your selected Facebook accounts support Targeting. Remove problematic accounts or remove targeting.');
    expect(alert.prop('soft')).not.toEqual(true);

    const link = shallow(alert.prop('link'));
    const invalidAccounts = link.find(DisabledAccountsList);
    expect(invalidAccounts.prop('invalidAccounts')).toEqual(invalidTargetingAccounts);
    const clearTargetingLink = link.find(`.${styles.link}`);
    clearTargetingLink.simulate('click');
    expect(clearTargeting).toHaveBeenCalled();
  });

  it('should render correctly when targeting is disabled and there are multiple invalid accounts', () => {
    const invalidTargetingAccounts = [
      new CredentialRecord({
        name: 'planet express'
      }),
      new CredentialRecord({
        name: 'fry'
      })
    ];
    render({
      canTarget: false,
      invalidTargetingAccounts
    });
    const targetingHeader = shallowWrapper
      .find(OptionsMenuSectionHeader)
      .findWhere((wrapper) => wrapper.prop('title') && wrapper.prop('title').toLowerCase() === 'targeting');
    expect(targetingHeader.length).toEqual(1);
    expect(targetingHeader.prop('children')).toBeDefined();

    const language = shallowWrapper.find(FacebookLanguageTargeting);
    expect(language.length).toEqual(0);

    const alert = shallowWrapper.find(AlertBox);
    expect(alert.prop('text')).toEqual('Targeting Not Available');
    expect(alert.prop('additionalText')).toEqual('You\'ve selected 2 accounts that don\'t support Targeting.');
    expect(alert.prop('soft')).toEqual(true);

    const link = alert.prop('link');
    expect(link.type).toEqual(DisabledAccountsList);
    expect(link.props.invalidAccounts).toDeepEqual(invalidTargetingAccounts);
  });
});
