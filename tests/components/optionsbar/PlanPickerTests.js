/* @flow */
import React from 'react';
import { shallow, mount } from 'enzyme';
import PlanPicker from 'components/optionsbar/PlanPicker';
import DropdownSection from 'components/util/DropdownSection';
import { Combobox } from 'react-widgets';
import { emptyPlan } from 'constants/PlanConstants';

import dropdownStyles from 'styles/modules/components/util/DropdownSection.scss';
import dropdownbuttonStyles from 'styles/modules/components/util/DropdownButton.scss';


let shallowWrapper, mountedWrapper;

const mainProps = {
  plans: [
    {
      id: '1',
      name: 'Test 1',
      color: 'blue'
    },
    {
      id: '2',
      name: 'Test 2',
      color: 'red'
    },
    {
      id: '3',
      name: 'Test 3',
      color: 'green'
    },
    {
      id: '4',
      name: 'Test 4',
      color: 'yellow'
    }
  ],
  currentPlan: {
    id: '3',
    name: 'Test 3',
    color: 'green'
  },
  onSelectPlan: () => {}
};

function render(props = {}) {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<PlanPicker {...props} />);
  mountedWrapper = mount(<PlanPicker {...props} />);
}

describe('PlanPicker Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render and pass props correctly', () => {
    const dropdownSection = shallowWrapper.find(DropdownSection);
    expect(dropdownSection.length).toEqual(1);
    expect(dropdownSection.props()).toEqual(jasmine.objectContaining({
      colorSelected: true,
      singleSelect: true,
      title: 'Plans',
      icon: <span className='icon-planner'></span>,
      description: 'Select a Plan to associate with your message',
      selectedData: [mainProps.currentPlan]
    }));
    const comboBoxConfig = dropdownSection.prop('comboBoxConfig');
    expect(comboBoxConfig).toEqual(jasmine.objectContaining({
      data: mainProps.plans,
      placeholder: 'Select Plan...'
    }));
    expect(typeof comboBoxConfig.itemComponent).toEqual('function');
  });

  it('should not pass in an empty plan', () => {
    render({
      currentPlan: {}
    });
    const dropdownSection = shallowWrapper.find(DropdownSection);
    expect(dropdownSection.prop('selectedData')).toDeepEqual([]);
  });

  it('should respond correctly to selecting a plan', () => {
    const onSelectPlan = jasmine.createSpy('onSelectPlan');
    render({
      onSelectPlan
    });

    const dropdownButton = mountedWrapper.find(`.${dropdownbuttonStyles.button}`);
    dropdownButton.simulate('click');

    const comboBox = mountedWrapper.find(Combobox);
    const firstItem = comboBox.find('li').first();

    firstItem.simulate('click');
    expect(onSelectPlan).toHaveBeenCalledWith(mainProps.plans[0]);
  });

  it('should respond correctly to a blur when a valid plan is not selected', () => {
    const onSelectPlan = jasmine.createSpy('onSelectPlan');
    render({
      onSelectPlan,
      currentPlan: undefined
    });
    const dropdownSection = shallowWrapper.find(DropdownSection).dive();
    const comboBox = dropdownSection.find(Combobox);
    comboBox.simulate('blur', {});

    expect(onSelectPlan).toHaveBeenCalledWith(emptyPlan);
  });

  it('should respond correctly to a blur when a valid plan is selected', () => {
    const onSelectPlan = jasmine.createSpy('onSelectPlan');
    render({
      onSelectPlan
    });
    const dropdownSection = shallowWrapper.find(DropdownSection).dive();
    const comboBox = dropdownSection.find(Combobox);
    comboBox.simulate('blur', {});

    expect(onSelectPlan).toHaveBeenCalledWith(mainProps.currentPlan);
  });

  it('should respond correctly when the dropdown is cleared', () => {
    const onSelectPlan = jasmine.createSpy('onSelectPlan');
    render({
      onSelectPlan
    });

    const clearButton = mountedWrapper.find(`.${dropdownStyles.clearSelectedDatum}`);
    clearButton.simulate('click');

    expect(onSelectPlan).toHaveBeenCalledWith(emptyPlan);
  });
});
