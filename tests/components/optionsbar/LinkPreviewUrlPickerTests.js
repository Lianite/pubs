import React from 'react';
import LinkPreviewUrlPicker, {
  OptionPicker,
  Option
} from 'components/optionsbar/LinkPreviewUrlPicker';
import { Set as ImmutableSet } from 'immutable';
import { shallow } from 'enzyme';
import _ from 'lodash';

import styles from 'styles/modules/components/optionsbar/LinkPreviewUrlPicker.scss';

const mainProps = {
  urls: ImmutableSet(['google.com', 'facebook.com']),
  selectedUrl: 'google.com',
  onSelect: () => {}
};

let shallowWrapper;

const render = (props = mainProps) => {
  props = {...mainProps, ...props};
  shallowWrapper = shallow(<LinkPreviewUrlPicker {...props} />);
};

describe('Link Preview Url Picker Component', () => {

  beforeEach(() => {
    render();
  });

  it('should render correctly', () => {
    const urlPicker = shallowWrapper.find(`.${styles.linkPreviewUrlPicker}`);
    expect(urlPicker.length).toEqual(1);
    expect(urlPicker.text()).toContain('Link Preview');

    const optionPicker = shallowWrapper.find(OptionPicker);
    expect(optionPicker.length).toEqual(1);
    expect(optionPicker.props()).toDeepEqual({
      noneOption: true,
      onSelect: mainProps.onSelect,
      selectedOption: 'google.com',
      options: mainProps.urls
    });
  });

  describe('OptionPicker component', () => {

    const mainOptionPickerProps = {
      options: ImmutableSet(['google.com', 'facebook.com']),
      selectedOption: 'google.com',
      onSelect: (jasmine.createSpy('onSelect')),
      noneOption: true,
      inline: false
    };

    const renderOptionPicker = (props = mainOptionPickerProps) => {
      props = {
        ...mainOptionPickerProps,
        ...props
      };
      shallowWrapper = shallow(<OptionPicker {...props} />);
    };

    beforeEach(() => {
      renderOptionPicker();
    });

    afterEach(() => {
      mainOptionPickerProps.onSelect.calls.reset();
    });

    it('should render correctly', () => {

      const pickerOptions = _.map(mainOptionPickerProps.options.toJS(), option => option);

      const picker = shallowWrapper.find(`.${styles.linkPreviewContainer}`);
      expect(picker.length).toEqual(1);

      expect(shallowWrapper.find(`.${styles.inline}`).length).toEqual(0);

      const options = shallowWrapper.find(Option);
      expect(options.length).toEqual(3);

      options.forEach((option, index) => {
        if (index === 0) {
          expect(option.prop('value')).toEqual('None');
          expect(option.prop('checked')).toEqual(false);

          option.simulate('select', 'value');
          expect(mainOptionPickerProps.onSelect).toHaveBeenCalledWith();
        } else {
          expect(option.prop('value')).toEqual(pickerOptions[index - 1]);
          expect(option.prop('checked')).toEqual(option.prop('value') === mainOptionPickerProps.selectedOption);

          option.simulate('select', 'value');
          expect(mainOptionPickerProps.onSelect).toHaveBeenCalledWith('value');
        }
      });
    });

    it('should not render an option for \'None\'', () => {
      renderOptionPicker({
        noneOption: false
      });

      const options = shallowWrapper.find(Option);
      expect(options.length).toEqual(2);
      expect(options.findWhere((option) => option.prop('value') === 'None').length).toEqual(0);
    });

    it('should render inline when specified', () => {
      renderOptionPicker({
        inline: true
      });
      expect(shallowWrapper.find(`.${styles.inline}`).length).toEqual(1);
    });
  });

  describe('Option component', () => {

    const mainOptionProps = {
      value: 'test',
      onSelect: jasmine.createSpy('onSelect'),
      checked: false
    };

    const renderOptions = (props = mainOptionProps) => {
      props = {
        ...mainOptionProps,
        ...props
      };
      shallowWrapper = shallow(<Option {...props} />);
    };

    beforeEach(() => {
      renderOptions();
    });

    afterEach(() => {
      mainOptionProps.onSelect.calls.reset();
    });

    it('should render correctly', () => {

      const button = shallowWrapper.find('button');
      expect(button.prop('aria-checked')).toEqual(false);

      shallowWrapper.find('div').simulate('click');
      expect(mainOptionProps.onSelect).toHaveBeenCalledWith('test');
    });

    it('should render correctly when checked', () => {
      renderOptions({
        checked: true
      });

      const button = shallowWrapper.find('button');
      expect(button.prop('aria-checked')).toEqual(true);
    });
  });
});
