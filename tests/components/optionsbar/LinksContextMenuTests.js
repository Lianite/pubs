/* @flow */
import React from 'react';
import LinksContextMenu from 'components/optionsbar/LinksContextMenu';
import LinkPreviewUrlPicker from 'components/optionsbar/LinkPreviewUrlPicker';
import LinkTagsVariables from 'components/optionsbar/linkTags/LinkTagsVariables';
import { shallow } from 'enzyme';
import { Map as ImmutableMap, Set as ImmutableSet, List as ImmutableList } from 'immutable';
import Checkbox from '@spredfast/react-lib/lib/Checkbox';
import _ from 'lodash';

import styles from 'styles/modules/components/optionsbar/LinksContextMenu.scss';

const mainProps = {
  urls: ImmutableSet(['www.google.com', 'www.reddit.com']),
  urlProperties: ImmutableMap({
    'www.google.com': ImmutableMap({
      'shortenOnPublish': false,
      'addLinkTags': false
    }),
    'www.reddit.com': ImmutableMap({
      'shortenOnPublish': true,
      'addLinkTags': true
    })
  }),
  onChangeLinkProperty: jasmine.createSpy('onChangeLinkProperty'),
  addLinkPreview: jasmine.createSpy('addLinkPreview'),
  selectedLinkPreviewUrl: 'www.google.com',
  linkTagsByLink: ImmutableMap(),
  linkTagsEnable: true,
  updateLinkTagVariable: () => {},
  canAddAttachmentByType: {linkPreview: true, images: true, video: true}
};

let shallowWrapper;

const render = (props = mainProps) => {
  props = {
    ...mainProps,
    ...props
  };
  shallowWrapper = shallow(<LinksContextMenu {...props} />);
};

describe('Links Context Menu Component', () => {

  beforeEach(() => {
    render();
  });

  afterEach(() => {
    mainProps.onChangeLinkProperty.calls.reset();
    mainProps.addLinkPreview.calls.reset();
  });

  // a basic test for looking at outputted elements in the DOM and verifying the results
  it('should render correctly', () => {
    const menu = shallowWrapper.find(`.${styles.linksContextMenu}`);
    expect(menu.length).toEqual(1);

    const title = menu.find(`.${styles.title}`);
    expect(title.text()).toContain('LINKS');

    const picker = menu.find(LinkPreviewUrlPicker);
    expect(picker.length).toEqual(1);
    expect(picker.props()).toDeepEqual({
      urls: mainProps.urls,
      selectedUrl: 'www.google.com',
      onSelect: mainProps.addLinkPreview
    });

    const links = shallowWrapper.find(`.${styles.linkShorten}`);
    expect(links.length).toEqual(2);
  });

  it('should render all links correctly', () => {
    const urls = mainProps.urls.toJS();
    const urlProps = _.map(mainProps.urlProperties.toJS(), urlProps => urlProps);
    const links = shallowWrapper.find(`.${styles.linkShorten}`);

    expect(links.length).toEqual(2);
    links.forEach((link, index) => {
      expect(link.text()).toContain(urls[index]);
      expect(link.text()).toContain('Shorten on Publish');
      const checkbox = link.find(Checkbox);
      expect(checkbox.length).toEqual(1);
      expect(checkbox.prop('checked')).toEqual(urlProps[index].shortenOnPublish);

      checkbox.simulate('change');
      expect(mainProps.onChangeLinkProperty).toHaveBeenCalledWith(
        urls[index],
        'shortenOnPublish',
        !urlProps[index].shortenOnPublish
      );
      mainProps.onChangeLinkProperty.calls.reset();
    });
  });

  it('should not render any urls or a url picker if none are passed in', () => {
    render({
      urls: ImmutableSet([])
    });
    const links = shallowWrapper.find(`.${styles.linkShorten}`);
    expect(links.length).toEqual(0);
    expect(shallowWrapper.find(LinkPreviewUrlPicker).length).toEqual(0);
  });

  it('should not render an url picker if link previews cannot be attached', () => {
    render({
      canAddAttachmentByType: {
        ...mainProps.canAddAttachmentByType,
        linkPreview: false
      }
    });
    expect(shallowWrapper.find(LinkPreviewUrlPicker).length).toEqual(0);
  });

  it('should render link tag variables only when: there is a link tag, shortenOnPublish and addLinkTags are turned on', () => {
    render();
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(0);

    render({
      urls: ImmutableSet(['www.google.com']),
      urlProperties: ImmutableMap({
        'www.google.com': ImmutableMap({
          'shortenOnPublish': true,
          'addLinkTags': true
        })
      }),
      linkTagsByLink: ImmutableMap({
        'www.google.com': {
          tagVariables: ImmutableList(),
          variableValues: ImmutableList()
        }
      })
    });
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(1);
  });

  it('should not render link tag variables when there is a link tag but shortenOnPublish is turned off', () => {
    render();
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(0);

    render({
      urls: ImmutableSet(['www.google.com']),
      urlProperties: ImmutableMap({
        'www.google.com': ImmutableMap({
          'shortenOnPublish': false,
          'addLinkTags': true
        })
      }),
      linkTagsByLink: ImmutableMap({
        'www.google.com': {
          tagVariables: ImmutableList(),
          variableValues: ImmutableList()
        }
      })
    });
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(0);
  });

  it('should not render link tag variables when there is a link tag but addLinkTags is turned off', () => {
    render();
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(0);

    render({
      urls: ImmutableSet(['www.google.com']),
      urlProperties: ImmutableMap({
        'www.google.com': ImmutableMap({
          'shortenOnPublish': false,
          'addLinkTags': false
        })
      }),
      linkTagsByLink: ImmutableMap({
        'www.google.com': {
          tagVariables: ImmutableList(),
          variableValues: ImmutableList()
        }
      })
    });
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(0);
  });

  it('should not render link tag variables if linkTags is not enable', () => {
    render({
      urls: ImmutableSet(['www.google.com']),
      urlProperties: ImmutableMap({
        'www.google.com': ImmutableMap({
          'shortenOnPublish': true,
          'addLinkTags': true
        })
      }),
      linkTagsByLink: ImmutableMap({
        'www.google.com': {
          tagVariables: ImmutableList(),
          variableValues: ImmutableList()
        }
      }),
      linkTagsEnable: false
    });
    expect(shallowWrapper.find(LinkTagsVariables).length).toEqual(0);
  });
});
