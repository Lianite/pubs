import Promise from 'bluebird';

export function createAsyncDispatch(store): Function {
  return (action) => {
    store.dispatch(action);
    return new Promise(function(resolve) {
      setTimeout(() => {
        resolve(store.getState());
      });
    });
  };
}
