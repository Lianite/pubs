/* @flow */

import { EnvironmentConstants } from 'constants/ApplicationConstants';

const defaultEnvDetails = {
  environmentUrl: `https://${EnvironmentConstants.TEST_CONV_HOSTNAME}`,
  productEnvironment: 'MOCK05',
  productName: 'Conversations'
};

export const setProdConvDetails = () => {
  window.PublishingApp = {
    envDetails: [defaultEnvDetails],
    lh: EnvironmentConstants.PROD_LOGIN_HOSTNAME
  };
};

export const setTestConvDetails = () => {
  window.PublishingApp = {
    envDetails: [defaultEnvDetails]
  };
};

export const setStagingConvDetails = () => {
  window.PublishingApp = {
    envDetails: [{
      ...defaultEnvDetails,
      environmentUrl: 'https://qa.spredfast.com'
    }]
  };
};

export const setDevConvDetails = () => {
  window.PublishingApp = {
    envDetails: [{
      ...defaultEnvDetails,
      environmentUrl: `https://${EnvironmentConstants.LOCAL_DEVELOPMENT_CONV_HOSTNAME}`
    }]
  };
};
