/* @flow */
import { CONV_API } from 'services/HttpService';

export function stubApiRequest(baseUrl: string, path: string | RegExp): Object {
  if (typeof path === 'string') {
    return jasmine.Ajax.stubRequest(`${baseUrl}${path}`);
  }
  return jasmine.Ajax.stubRequest(path);
}

export function stubConvApiRequest(path: string | RegExp, endpoint: ?string): Object {
  return stubApiRequest(endpoint ? endpoint : CONV_API, path);
}
