import configureMockStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'sagas/rootSaga';

import type { rootState } from 'reducers/index';

type MiddlewareConfig = {
  reduxSaga?: bool,
  thunk?: bool
};

export default function createMockPublishingStore(initialState: rootState, middlewareConfig?: MiddlewareConfig) {
  let middleware = [];
  let store;

  if (middlewareConfig && middlewareConfig.reduxSaga) {
    const sagaMiddleware = createSagaMiddleware();
    middleware.push(sagaMiddleware);

    const mockStore = configureMockStore(middleware);

    store = mockStore(initialState);

    sagaMiddleware.run(rootSaga);
  } else {
    const mockStore = configureMockStore(middleware);

    store = mockStore(initialState);
  }

  return store;
}
