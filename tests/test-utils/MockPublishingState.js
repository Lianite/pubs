/* @flow */
import { VideosState } from 'records/VideosRecords';
import { MessageReducerState } from 'reducers/MessageReducer';
import { EnvironmentState } from 'reducers/EnvironmentReducer';
import { UIStateReducerState } from 'reducers/UIStateReducer';
import { AssigneeReducerState } from 'records/AssigneeRecords';
import { ImageReducerState } from 'records/ImageRecords';
import { PlanReducerState } from 'reducers/PlanReducer';
import { MentionsReducerState } from 'reducers/MentionsReducer';
import { LinkPreviewState } from 'records/LinkPreviewRecords';
import { LinkTagsState } from 'records/LinkTagsRecords';
import { LinksState } from 'records/LinksRecords';
import { LabelsReducerState } from 'reducers/LabelsReducer';
import { NotesState } from 'records/NotesRecords';
import { FacebookState } from 'records/FacebookRecords';
import { AccountsState } from 'records/AccountsRecord';
import type { rootState } from 'reducers/index';

const defaultRouter = {
  params: {
    companyId: '1',
    campaignId: '1'
  }
};

/**
 * This function creates a simple rootState representing the state of our app's
 * store. This is then used in testing, primarily actions where the full functionality
 * of a store isn't needed.
 */
type MockRootStateParam = {
  assignee?: Object,
  videos?: Object,
  router?: Object,
  message?: Object,
  uiState?: Object,
  environment?: Object,
  images?: Object,
  plan?: Object,
  label?: Object,
  linkPreview?: Object,
  linkTags?: Object,
  links?: Object,
  labels?: Object,
  growlerNotification?: Object,
  mentions?: Object,
  linkPreview? : Object,
  links?: Object,
  notes?: Object,
  facebook?: Object,
  accounts?: Object
};

const mockGetState = ({videos, router, message, environment, uiState, images, linkPreview, linkTags, links, notes, plan, labels, mentions, facebook, accounts, assignee}: MockRootStateParam): () => rootState => {
  return () => {
    return {
      assignee: new AssigneeReducerState(assignee),
      videos: new VideosState(videos),
      router: router || defaultRouter,
      message: new MessageReducerState(message),
      environment: new EnvironmentState(environment),
      uiState: new UIStateReducerState(uiState),
      images: new ImageReducerState(images),
      plan: new PlanReducerState(plan),
      mentions: new MentionsReducerState(mentions),
      linkPreview: new LinkPreviewState(linkPreview),
      linkTags: new LinkTagsState(linkTags),
      links: new LinksState(links),
      labels: new LabelsReducerState(labels),
      growlerNotification: [],
      notes: new NotesState(notes),
      facebook: new FacebookState(facebook),
      accounts: new AccountsState(accounts)
    };
  };
};

export default mockGetState;
