import _ from 'lodash';

export const toEqualMessage = (util, customEqualityTesters) => {
  return {
    compare: function(actual, expected) {
      var result = {};

      const eventIdPassed = actual.eventId === expected.eventId;
      const messageTitlePassed = actual.messageTitle === expected.messageTitle;
      const checkedCredentialsPassed = _.isEqual(actual.checkedCredentials, expected.checkedCredentials);

      /**
       * The follow compares messageEditorStateMaps at the moment only on text content.
       * In the future this will likely expand as we add more complexity to the
       * messageEditorStateMap
       */

      const messageEditorStatePassed = actual.messageEditorStateMap.reduce((result, val, key) => {
        const textIsEqual = _.isEqual(val.getCurrentContent().getPlainText(), expected.messageEditorStateMap.get(key).getCurrentContent().getPlainText());
        return result && textIsEqual;
      }, true);

      result.pass = (eventIdPassed && messageTitlePassed && checkedCredentialsPassed && messageEditorStatePassed);
      return result;
    }
  };
};
