import _ from 'lodash';

export const toDeepEqual = (util, customEqualityTesters) => {
  return {
    compare: function(actual, expected) {
      var result = {};
      result.pass = _.isEqual(actual, expected);
      return result;
    }
  };
};
