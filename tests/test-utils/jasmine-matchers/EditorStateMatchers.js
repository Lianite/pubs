import _ from 'lodash';

export const toEqualEditorStateText = (util, customEqualityTesters) => {
  return {
    compare: function(actual, expected) {
      var result = {};
      result.pass = _.isEqual(actual.getCurrentContent().getPlainText(), expected.getCurrentContent().getPlainText());
      return result;
    }
  };
};
