/* @flow */
import { createMessage,
  editMessage,
  readMessage,
  getLinkPreview } from 'apis/MessageApi';
import { stubCreateMessagesRequest,
  stubEditMessagesRequest,
  mockMessageResponse,
  mockGetLinkPreviewResponse,
  stubReadMessagesRequest,
  stubGetLinkPreviewRequest } from 'tests/api-responses/MessageApiResponses';
import { MessageConstants, HTTPSTATUS } from 'constants/ApplicationConstants';

describe('MessageApi', () => {
  describe('createMessage api call', () => {
    const companyId = '1';
    const initiativeId = '1';
    const messageStatus = MessageConstants.PENDING;
    const userId = 1;
    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1
    };
    const planId = '1';

    it('should return a message confirmation on successful api call', (done) => {
      const response = mockMessageResponse({companyId, planId, userId, assignee, initiativeId, messageStatus});
      stubCreateMessagesRequest({companyId, initiativeId, response});

      createMessage(companyId, initiativeId, response, false)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('editMessage api call', () => {
    const companyId = '1';
    const initiativeId = '1';
    const eventId = '1';
    const messageStatus = MessageConstants.PENDING;
    const userId = 1;
    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1
    };
    const planId = '1';

    it('should return a message confirmation on successful api call', (done) => {
      const response = mockMessageResponse({companyId, planId, userId, assignee, initiativeId, messageStatus});
      stubEditMessagesRequest({companyId, initiativeId, eventId, response});
      editMessage(companyId, initiativeId, eventId, response)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('readMessage api call', () => {
    const companyId = '1';
    const initiativeId = '1';
    const eventId = '1';
    const messageStatus = MessageConstants.PENDING;
    const userId = 1;
    const assignee = {
      firstName: 'Bob',
      lastName: 'Kelso',
      id: 1
    };
    const planId = '1';

    it('should return a message confirmation on successful api call', (done) => {
      const eventIdNum = parseInt(eventId, 10);
      const response = mockMessageResponse({companyId, planId, userId, assignee, initiativeId, eventId: eventIdNum, messageStatus});
      stubReadMessagesRequest({companyId, initiativeId, eventId, response});

      readMessage(companyId, initiativeId, eventId, false)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('getLinkPreview api call', () => {
    const companyId = '1';
    const campaignId = '1';

    it('should return a link preview details', (done) => {
      const response = mockGetLinkPreviewResponse();
      const url = 'http://www.facebook.com';
      stubGetLinkPreviewRequest({companyId, campaignId, url, response});

      getLinkPreview(companyId, campaignId, url)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });

    it('should return a link preview without details on failure', (done) => {
      const url = 'http://www.facebook.com';
      stubGetLinkPreviewRequest({companyId, campaignId, url, status: HTTPSTATUS.INTERNAL_SERVER_ERROR});
      getLinkPreview(companyId, campaignId, url)
        .catch(error => {
          expect(error.message).toEqual('Unsuccessful HTTP response');
          done();
        });
    });
  });
});
