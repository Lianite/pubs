/* @flow */
import { PermissionConstants } from 'constants/ApplicationConstants';
import { getCsrfToken, getUserData } from 'apis/EnvironmentApi';
import { mockCsrfTokenResponse,
  mockGetUserDataRequest,
  stubCsrfTokenRequest,
  stubGetUserDataRequest } from 'tests/api-responses/EnvironmentApiResponses';

describe('EnvironemntApi', () => {
  describe('getCsrfToken api call', () => {
    const companyId = '2';

    it('should return a csrf token as a string on successful api call', (done) => {
      const response = mockCsrfTokenResponse();
      stubCsrfTokenRequest({ companyId, response });

      getCsrfToken(companyId)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('getUserData api call', () => {
    const getUserMockParam = {
      companyId: '2',
      userId: 2,
      userFirstName: 'test',
      userLastName: 'user',
      userAvatar: 'http://avatar.com',
      permissionLevel: PermissionConstants.FULL_PUBLISHING_ACCESS_KEY,
      email: 'testuser@spredfast.com',
      directoryUserId: 12345
    };

    it('should return the active user on successful api call', (done) => {
      const response = mockGetUserDataRequest(getUserMockParam);
      stubGetUserDataRequest({ companyId: getUserMockParam.companyId, response });

      getUserData(getUserMockParam.companyId)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });
});
