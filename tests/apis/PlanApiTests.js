/* @flow */
import { getPlansData } from 'apis/PlanApi';
import { stubGetPlanDataRequest } from 'tests/api-responses/EnvironmentApiResponses';
import { mockGetPlanDataRequest } from 'tests/api-responses/PlanApiResponses';

describe('PlanApi', () => {
  describe('getPlansData api call', () => {
    const getPlansMockParam = {
      companyId: '2',
      campaignId: '2'
    };

    it('should return the avaliable plans on a successful api call', (done) => {
      const response = mockGetPlanDataRequest(getPlansMockParam);
      stubGetPlanDataRequest({ companyId: getPlansMockParam.companyId, campaignId: getPlansMockParam.campaignId, response });

      getPlansData(getPlansMockParam.companyId, getPlansMockParam.campaignId)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });
});
