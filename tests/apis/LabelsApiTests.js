/* @flow */
import { getLabels } from 'apis/LabelsApi';
import { mockGetLabels,
         stubGetLabels } from 'tests/api-responses/LabelsApiResponses';

describe('LabelsApi', () => {
  describe('getLabels api call', () => {
    const getLabelsMockParam = {
      companyId: '1',
      campaignId: '1'
    };

    it('should return the labels on a successful api call', (done) => {
      const response = mockGetLabels(getLabelsMockParam);
      stubGetLabels({ companyId: getLabelsMockParam.companyId, campaignId: getLabelsMockParam.campaignId, response });

      getLabels(getLabelsMockParam.companyId, getLabelsMockParam.campaignId)
        .then(res => {
          expect(res).toEqual(response);
        });
      done();
    });
  });
});
