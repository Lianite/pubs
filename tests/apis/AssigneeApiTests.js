/* @flow */
import { getAllAssignees, getValidAssignees, getAssigneeVoices } from 'apis/AssigneeApi';
import { mockGetAllAssignDataRequest,
  mockGetValidAssignDataRequest,
  stubGetAllAssigneeDataRequest,
  stubGetValidAssigneeDataRequest,
  mockGetAssigneeVoicesRequest,
  stubGetAssigneeVoices} from 'tests/api-responses/AssigneeApiResponses';

import { SupportedNetworks } from 'constants/ApplicationConstants';

describe('AssigneeApi', () => {
  describe('getAllAssignees api call', () => {
    const getAssignMockParam = {
      companyId: '2',
      campaignId: '2'
    };

    it('should return the avaliable plans on a successful api call', (done) => {
      const response = mockGetAllAssignDataRequest(getAssignMockParam);
      stubGetAllAssigneeDataRequest({ companyId: getAssignMockParam.companyId, campaignId: getAssignMockParam.campaignId, response });

      getAllAssignees(getAssignMockParam.companyId, getAssignMockParam.campaignId)
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('getValidAssignees api call', () => {
    const getAssignMockParam = {
      companyId: '2',
      campaignId: '2'
    };

    it('should return the avaliable plans on a successful api call', (done) => {
      const response = mockGetValidAssignDataRequest(getAssignMockParam);
      stubGetValidAssigneeDataRequest({ companyId: getAssignMockParam.companyId, campaignId: getAssignMockParam.campaignId, response });

      getValidAssignees(getAssignMockParam.companyId, getAssignMockParam.campaignId, [])
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });

  describe('getAssigneeVoices api call', () => {
    const getAssignMockParam = {
      companyId: '2',
      campaignId: '2',
      userId: 2
    };

    it('should return the avaliable plans on a successful api call', (done) => {
      const response = mockGetAssigneeVoicesRequest(getAssignMockParam);
      stubGetAssigneeVoices(
        {
          companyId: getAssignMockParam.companyId,
          campaignId: getAssignMockParam.campaignId,
          userId: getAssignMockParam.userId,
          response });

      getAssigneeVoices(getAssignMockParam.companyId, getAssignMockParam.campaignId, getAssignMockParam.userId, [SupportedNetworks.FACEBOOK, SupportedNetworks.TWITTER, SupportedNetworks.GOOGLEPLUS])
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });
});
