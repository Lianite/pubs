/* @flow */
import { getVoices } from 'apis/AccountsApi';
import { mockGetVoicesResponse,
  stubGetVoicesRequest } from 'tests/api-responses/AccountsApiResponses';
import { SupportedNetworks } from 'constants/ApplicationConstants';

describe('MessageApi', () => {
  describe('getVoices api call', () => {
    const companyId = '1';
    const initiativeId = '1';

    it('should return a list of accounts on successful api call', (done) => {
      const response = mockGetVoicesResponse({companyId, initiativeId});
      stubGetVoicesRequest({companyId, initiativeId, response});

      getVoices(companyId, initiativeId, [SupportedNetworks.FACEBOOK, SupportedNetworks.TWITTER, SupportedNetworks.GOOGLEPLUS])
        .then(res => {
          expect(res).toEqual(response);
          done();
        });
    });
  });
});
