/* @flow */
import { LinkTags } from 'constants/ApplicationConstants';
import { getLinkTagsEndpoint } from 'apis/LinkTagsApi';
import {
  setProdConvDetails,
  setTestConvDetails,
  setStagingConvDetails,
  setDevConvDetails
} from 'tests/test-utils/EnvironmentTestUtils';

describe('LinkTagsApi', () => {
  describe('getLinkTagsEndpoint', () => {

    afterEach(setTestConvDetails);

    it('should return prod endpoint', () => {
      setProdConvDetails();
      expect(getLinkTagsEndpoint()).toEqual(`//${LinkTags.PROD_HOSTNAME}`);
    });

    it('should return development endpoint: test environment', () => {
      expect(getLinkTagsEndpoint()).toEqual(`//${LinkTags.DEV_HOSTNAME}`);
    });

    it('should return development endpoint: local environment', () => {
      setDevConvDetails();
      expect(getLinkTagsEndpoint()).toEqual(`//${LinkTags.DEV_HOSTNAME}`);
    });

    it('should return staging endpoint', () => {
      setStagingConvDetails();
      expect(getLinkTagsEndpoint()).toEqual(`//${LinkTags.STAGE_HOSTNAME}`);
    });
  });
});
