import fs from 'fs';
import path from 'path';
import resolve from 'resolve';
import nodeResolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath)
    .filter(file => fs.statSync(path.join(srcpath, file)).isDirectory());
}

const directories = getDirectories('./app/scripts/');

const customResolver = {
  name: '@spredfast/publishing-ui/custom-resolver',
  resolveId(importee, importer) {
    // ignore IDs with null character, these belong to other plugins
    if (/\0/.test(importee)) {
      return null;
    }

    // disregard entry module
    if (!importer) {
      return null;
    }

    const parts = importee.split(/[\/\\]/);
    let id = parts.shift();
    if (directories.indexOf(id) >= 0) {
      return resolve.sync(importee, {
        moduleDirectory: './app/scripts/'
      });
    }
  }
};

export default {
  entry: 'app/scripts/main.native.js',
  format: 'es',
  dest: 'build/publishing/scripts/bundle.native.js',
  external: [
    '@spredfast/sf-intents',
    '@spredfast/sf-segment-js',
    '@spredfast/react-lib',
    '@spredfast/react-lib/lib/growler-notification/redux/actions',
    '@spredfast/react-lib/lib/growler-notification/redux/reducer',
    'bluebird',
    'draft-js',
    'immutable',
    'moment',
    'lodash',
    'react',
    'react-redux',
    'react-router',
    'redux',
    'redux-actions',
    'redux-router',
    'reselect',
    'twitter-text',
    'decorators/components/ExtraCharsDecorator',
    'decorators/components/MentionLinkDecorator',
    'decorators/components/MentionQueryDecorator',
    'selectors/RouterSelector',
    'services/HttpService',
    'services/GlobalsWrapper',
    'services/SentryService',
    'utils/ApiUtils'
  ],
  plugins: [
    customResolver,
    nodeResolve(),
    babel({
      babelrc: false,
      presets: ['react', 'stage-0'],
      plugins: ['external-helpers', 'transform-flow-strip-types'],
      externalHelpers: true
    })
  ]
};
