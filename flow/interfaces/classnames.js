type classes = ?string | { [key:string]: boolean } | string[];

declare module 'classnames' {
	declare function exports (...classes: classes[]): string;
}

declare module 'classnames/bind' {
  declare function bind(styles:any): any;
}
