type Request = {
	abort: Function;
	accept: Function;
	end: Function;
	send: Function;
	set: Function;
	type: Function;
  attach: Function;
  on: Function;
	url: string;
};

declare module 'superagent' {
	declare function get(url: string): Request;
	declare function post(url: string): Request;
}

declare module 'react-hot-loader/Injection' {
	declare var RootInstanceProvider: Object;
}
