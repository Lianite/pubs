type reducerFn<T> = (state: T, action: any) => T;

declare module 'redux-actions' {
  declare function createAction (type: string, makePayload?: (..._: any) => any, makeMeta?: (..._: any) => any): (...args: any) => any
  declare function handleActions<T> (reducers: { [key:string]: reducerFn<T> }, initialState: T): reducerFn<T>
}
