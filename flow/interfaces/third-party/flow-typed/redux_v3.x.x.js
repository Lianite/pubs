declare module 'redux' {
  declare type State = any;
  declare type Action = Object;
  declare type AsyncAction = any;
  declare type Reducer<S, A> = (state: S, action: A) => S;
  declare type BaseDispatch = (a: Action) => Action;
  declare type Dispatch = (a: Action | AsyncAction) => any;
  declare type ActionCreator = (...args: any) => Action | AsyncAction;
  declare type MiddlewareAPI = { dispatch: Dispatch, getState: () => State };
  declare type Middleware = (api: MiddlewareAPI) => (next: Dispatch) => Dispatch;
  declare type Store<T> = {
    dispatch: Dispatch,
    getState: () => T,
    subscribe: (listener: () => void) => () => void,
    replaceReducer: (reducer: Reducer<any, any>) => void
  };
  declare type StoreCreator<T> = (reducer: Reducer<T, any>, initialState: ?State) => Store<T>;
  declare type StoreEnhancer<T> = (next: StoreCreator<T>) => StoreCreator<T>;
  declare type ActionCreatorOrObjectOfACs = ActionCreator | { [key: string]: ActionCreator };
  declare type Reducers = { [key: string]: Reducer<any, any> };
  declare class Redux {
    bindActionCreators<actionCreators: ActionCreatorOrObjectOfACs>(actionCreators: actionCreators, dispatch: Dispatch): actionCreators;
    combineReducers(reducers: Reducers): Reducer<any, any>;
    createStore<T: any>(reducer: Reducer<T, any>, initialState?: T, enhancer?: StoreEnhancer<T>): Store<T>;
    applyMiddleware<T: any>(...middlewares: Array<Middleware>): StoreEnhancer<T>;
    compose<T: any>(...functions: Array<Function | StoreEnhancer<T>>): Function;
  }
  declare var exports: Redux;
}
