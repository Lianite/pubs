/* eslint-disable no-console */
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const PackageConfig = require('./package.json');
const webpack = require('webpack');
const child = require('child_process');
const fs = require('fs');

const Promise = require('bluebird');
Promise.promisifyAll($.git);

$.runSequence = require('run-sequence');

const date = new Date();
const DeployConfig = {
  AppTitle: 'Spredfast Publishing',
  AppName: 'publishing-ui',
  appDescription: 'Multichannel publishing application for Spredfast',
  cacheControl: 'max-age=120, s-maxage=2592000',

  // (Optional) You app can support a beta channel feature flag that offers the ability to launch the contents of your
  // master branch to an audience that has enabled said feature flag.
  betaChannelKey: '',
  sentryEndpoint: 'https://app.getsentry.com/api/0/projects/publishing-ui/publishing-ui/releases',
  tag: [PackageConfig.version, 'audience-deploy', date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes()].join('-')
};

const shamonDefaultPort = 3000;
const shamonDefaultSecurePort = 4000;

const S3_BUCKET = 'massrel-pub';

const publish = require('@spredfast/gulp-deploy')(DeployConfig, PackageConfig).publish;
const getTargetPath = require('@spredfast/gulp-deploy')(DeployConfig, PackageConfig).getTargetPath;

gulp.task('dev', function(cb) {
  return $.runSequence(['dev:server', 'html', 'cp-public-assets'], cb);
});

gulp.task('dev:server', function(cb) {
  const devConfig = require('./webpack.config.dev.js');
  const shamon = require('@spredfast/shamon');
  const app = shamon.app;

  const compiler = webpack(devConfig, function(/*err, stats*/) {
    cb(); // Callback after compile so webpack continues runSequence
  });

  app.use(function(req, res, next) {
  // http://stackoverflow.com/a/2068407
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.header('Pragma', 'no-cache');
    res.header('Expires', '0');
    next();
  });

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: $.util.env.noinfo,
    publicPath: devConfig.output.publicPath,
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  }));

  app.use(require('webpack-hot-middleware')(compiler));

  shamon.getPublicKeyAndStart({
    buildDir: './build/server',
    config: `./config/config.${$.util.env.env}.json`,
    port: $.util.env.port || shamonDefaultPort,
    securePort: $.util.env.securePort || shamonDefaultSecurePort
  });

  console.log('server listening at: https://publishing-local.spredfast.com:' + $.util.env.securePort || shamonDefaultSecurePort);
});

gulp.task('html', function() {
  const replacements = {
    // Generic items:
    '<PackageVersion>': PackageConfig.version,
    '<App Name>': DeployConfig.AppName,
    '<S3 Bucket>': S3_BUCKET,
    '<App Title>': DeployConfig.AppTitle,
    '<App Description>': DeployConfig.appDescription
  };

  const patterns = [];
  for (var key in replacements) {
    patterns.push({
      match: key,
      replacement: replacements[key]
    });
  }

  return gulp.src('app/index.hbs')
    .pipe($.replaceTask({
      patterns: patterns,
      usePrefix: false
    }))
    .pipe(gulp.dest('build/server/'));
});

gulp.task('cp-public-assets', function() {
  return gulp.src('app/styles/images/*')
    .pipe(gulp.dest('build/server/public'));
});

// Server deploy processes
gulp.task('deploy-server', function(cb) {
  if (!$.util.env.env) {
    return cb(new $.util.PluginError('deploy-server', 'No env config provided.'));
  }

  return $.runSequence('clean',
    ['cp-server', 'cp-public-assets', 'html'],
    cb);
});

gulp.task('server', function(cb) {
  if (!$.util.env.env) {
    return cb(new $.util.PluginError('server', 'No env config provided.'));
  }

  $.git.fetchAsync('origin', '')
  .then(function() {
    return $.git.revParseAsync({ args: '--short origin/master' });
  })
  .then(function(hash) {
    return $.git.execAsync({ args: `tag server-${$.util.env.env}-${hash} origin/master` });
  })
  .then(function() {
    return $.git.pushAsync('origin', 'master', { args: '--tags' });
  })
  .catch(function(err) {
    cb(new $.util.PluginError('server', err));
  });
});

gulp.task('deploy-client:prod', function(cb) {
  $.git.fetchAsync('origin', '')
  .then(function() {
    return $.git.revParseAsync({ args: '--short origin/master' });
  })
  .then(function(hash) {
    return $.git.execAsync({ args: `tag release-${PackageConfig.version}-${hash} origin/master` });
  })
  .then(function() {
    return $.git.pushAsync('origin', 'master', { args: '--tags' });
  })
  .catch(function(err) {
    cb(new $.util.PluginError('deploy-client:prod', err));
  });
});

gulp.task('cp-server', function() {
  gulp.src(['node_modules/@spredfast/shamon/server.js'])
    .pipe(gulp.dest('build/server'));

  gulp.src([`config/config.${$.util.env.env}.json`])
    .pipe($.rename('config.json'))
    .pipe(gulp.dest('build/server'));

  return gulp.src(['node_modules/**/*'])
    .pipe(gulp.dest('build/server/node_modules'));
});

gulp.task('scripts:dist', function(cb) {
  const distConfig = require('./webpack.config.build.js');

  distConfig.output.publicPath = getTargetPath();

  webpack(distConfig, function(err, stats) {
    if (err) {
      throw new $.util.PluginError('scripts:dist', err);
    }

    const jsonStats = stats.toJson();
    if (jsonStats.errors.length > 0) {
      return cb(jsonStats.errors);
    }

    $.util.log('[scripts:dist]', stats.toString({
      colors: true
    }));

    cb();
  });
});

gulp.task('clean', function() {
  return gulp.src('build')
    .pipe($.rimraf());
});

gulp.task('deploy', function(cb) {
  return $.runSequence('scripts:dist',
    'publish',
    'sentry',
    cb);
});

gulp.task('publish', function() {
  return gulp.src('build/publishing/**/*').pipe(publish());
});

// upload js bundle with sourcemap to enable sourcemaps in sentry errors
gulp.task('sentry', function() {
  const env = $.util.env.target;
  const releaseTag = DeployConfig.tag;
  const sentryEndpoint = DeployConfig.sentryEndpoint;
  const destPath = `${DeployConfig.appName}/${env}-${PackageConfig.version}`;
  const awsRoot = 'https://massrel-pub.a.ssl.fastly.net';
  const sentryKey = '1b68877edb854830b89ef2a90ef904cd';

  return fs.appendFile('build/publishing/scripts/bundle.js', `\n//# sourceMappingURL=${awsRoot}/${destPath}/publishing/scripts/bundle.js.map`, function(err) {
    if (err) {
      throwError(err);
    }

    const curlRelease = `curl ${sentryEndpoint}/ \
      -u ${sentryKey}: \
      -X POST \
      -d '{"version": "${releaseTag}"}' \
      -H 'Content-Type: application/json'`;

    const curlSrcmap = `curl ${sentryEndpoint}/${encodeURIComponent(releaseTag)}/files/ \
      -X POST \
      -u ${sentryKey}: \
      -F file=@build/publishing/scripts/bundle.js.map \
      -F name=${awsRoot}/${destPath}/publishing/scripts/bundle.js.map \
      -H "Content-Type: multipart/form-data"`;

    const curlBundle = `curl ${sentryEndpoint}/${encodeURIComponent(releaseTag)}/files/ \
      -X POST \
      -u ${sentryKey}: \
      -F file=@build/publishing/scripts/bundle.js \
      -F name=${awsRoot}/${destPath}/scripts/publishing/build.js \
      -H "Content-Type: multipart/form-data"`;

    child.exec(curlRelease, function(err, stdout, stderr) {
      if (err) {
        throwError(err);
      } else {
        logStatus(stdout, `New release created in Sentry: ${releaseTag}`, curlRelease);
        child.exec(curlSrcmap, function(err, stdout, stderr) {
          if (err) {
            throwError(err);
          } else {
            logStatus(stdout, 'Sourcemap uploaded to Sentry', curlRelease);
            child.exec(curlBundle, function(err, stdout, stderr) {
              if (err) {
                throwError(err);
              } else {
                logStatus(stdout, 'Bundle uploaded to Sentry', curlRelease);
              }
            });
          }
        });
      }
    });

    function logStatus(stdout, title) {
      if (stdout === '{"detail": ""}') {
        console.log(`Failed: ${title}`);
      } else {
        console.log(`Sucess: ${title}`);
      }
    };

    function throwError(err) {
      throw new $.util.PluginError('sentry', err);
    }
  });
});
