const path = require('path');
const webpack = require('webpack');
const SRC_FOLDER = path.join(__dirname, 'app/scripts');
const hmrPlugin = new webpack.HotModuleReplacementPlugin();

/* eslint-disable no-process-env */
const definePlugin = new webpack.DefinePlugin({
  'window.__DEV__': 'true'
});
/* eslint-enable no-process-env */

module.exports = {
  entry: [
    'babel-polyfill',
    'webpack-hot-middleware/client?reload=true',
    path.join(SRC_FOLDER, 'main.js')
  ],
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: '/publishing'
  },
  resolve: {
    root: [
      path.join(__dirname, 'app/scripts')
    ],
    fallback: [
      path.join(__dirname, '/node_modules')
    ],
    alias: {
      'bootstrap_sass': 'bootstrap-sass',
      'reactLibStyles': '@spredfast/react-lib/styles',
      'rad': '@spredfast/rad/dist',
      'styles': path.join(__dirname, 'app/styles'),
      'socialMediaPreview': path.join(__dirname, 'node_modules/@spredfast/social-media-preview'),
      'scripts': path.join(__dirname, 'app/scripts'),
      'tests': path.join(__dirname, 'tests'),
      'images': path.join(__dirname, 'app/styles/images'),
      'fixtures': path.join(__dirname, 'app/fixtures')
    }
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-0'],
          plugins: [
            'transform-runtime',
            'transform-decorators-legacy',
            'lodash',
            ['react-transform', {
              'transforms': [{
                transform: 'react-transform-hmr',
                imports: ['react'],
                locals: ['module']
              }, {
                transform: 'react-transform-catch-errors',
                imports: ['react', 'redbox-react']
              }]
            }]
          ]
        }
      },
      // Sass
      {
        test: /\.scss$/,
        include: [
          path.join(__dirname, 'app/styles/modules')
        ],
        loader: 'style!css?modules&importLoaders=1&localIdentName=[path][name]---[local]---[hash:base64:5]!postcss!sass'
      },
      {
        test: /(\.scss|\.css)$/,
        exclude: [
          path.join(__dirname, 'app/styles/modules')
        ],
        loader: 'style!css!postcss!sass'
      },
      // Fonts & Images
      {
        test: /\.woff(2)?(\?v=[0-9].[0-9].[0-9])?$/,
        include: [
          path.join(__dirname, 'node_modules/react-widgets/lib')
        ],
        loader: 'url-loader?mimetype=application/font-woff'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9].[0-9].[0-9])?$/,
        include: [
          path.join(__dirname, 'node_modules/react-widgets/lib')
        ],
        loader: 'file-loader?name=[name].[ext]'
      },
      {
        test: /(\.woff|\.woff2|\.ttf|\.eot|\.svg|\.png|\.gif|\.jpg)$/,
        loader: 'url'
      },
      // json
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  },
  plugins: [
    hmrPlugin,
    definePlugin
  ],
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  }
};
