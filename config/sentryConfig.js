/*
Have your sentry admin setup a new project for your app.  Then visit
"app.getsentry.com/spredfast/<YOUR_PROJECT_NAME>/settings/keys" and grab the PUBLIC
DSN and update the config below. Then on this same webpage click the Details button and
grab the Secret Key and update the config below. Once you've done this, your project will
automatically log all errors to sentry caused by redux actions.
NOTE: logging only happens for non-localhost.
*/

export default {
  dsn: 'https://1b68877edb854830b89ef2a90ef904cd@app.getsentry.com/81889',
  secretKey: '03ad30a9d4b8475fb9340d7d79436499'
};