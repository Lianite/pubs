module.exports = {
  githubRepo: 'spredfast/publishing-ui',
  publicAppUrl: 'https://publishing.spredfast.com',
  publicQaAppUrl: 'https://publishing-qa.spredfast.com'
};
