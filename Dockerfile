FROM spredfast/dandy-node

RUN /etc/my_init.d/00_regen_ssh_host_keys.sh && mkdir -p /var/www/app/public

ADD ./build/server /var/www/app

WORKDIR /var/www/app

RUN apt-get -y autoclean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/

EXPOSE 80

CMD ["node", "server.js", "--buildDir", "./"]
